package cdc;

import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.CheckpointingMode;
import org.apache.flink.streaming.api.TimeCharacteristic;
import org.apache.flink.streaming.api.environment.ExecutionCheckpointingOptions;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.table.api.EnvironmentSettings;
import org.apache.flink.table.api.bridge.java.StreamTableEnvironment;
import java.time.Duration;

/**
 * https://ci.apache.org/projects/flink/flink-docs-release-1.12/dev/table/types.html
 * <p>
 * https://ci.apache.org/projects/flink/flink-docs-release-1.13/docs/dev/table/sql/overview/
 */
public class FlinkMySQLCDCSQL {

    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment streamEnv = StreamExecutionEnvironment.createLocalEnvironmentWithWebUI(new Configuration());
        streamEnv.setStreamTimeCharacteristic(TimeCharacteristic.ProcessingTime);
        streamEnv.setParallelism(2);

        // ENV Setings
        EnvironmentSettings tableEnvSettings = EnvironmentSettings.newInstance()
                .inStreamingMode()
                .build();
        // 创建Table Env
        StreamTableEnvironment tableEnv = StreamTableEnvironment.create(streamEnv, tableEnvSettings);
        tableEnv.getConfig().getConfiguration().set(ExecutionCheckpointingOptions.CHECKPOINTING_MODE, CheckpointingMode.EXACTLY_ONCE);
        tableEnv.getConfig().getConfiguration().set(ExecutionCheckpointingOptions.CHECKPOINTING_INTERVAL, Duration.ofSeconds(20));


        tableEnv.executeSql("CREATE DATABASE IF NOT EXISTS stream_tmp");

        tableEnv.executeSql("CREATE TABLE stream_tmp.iot_device_flink (" +
                " id BIGINT PRIMARY KEY , " +
                " tenant_id BIGINT," +
                " name STRING ," +
                " code STRING ," +
                " tag STRING " +
                "  " +
                ") WITH (" +
                "  'connector' = 'mysql-cdc'," +
                "  'hostname' = '10.70.40.27'," +
                "  'port' = '30306'," +
                "  'username' = 'root'," +
                "  'password' = 'test123'," +
                "  'database-name' = 'test'," +
                "  'table-name' = 'connector' " +
                ")");

        tableEnv.executeSql("CREATE TABLE stream_tmp.iot_device_print (" +
                " id BIGINT, " +
                " tenant_id BIGINT," +
                " name STRING ," +
                " code STRING ," +
                " tag STRING " +
                ") WITH (" +
                "  'connector' = 'print'" +
                ")");

        tableEnv.executeSql("INSERT INTO stream_tmp.iot_device_print SELECT * FROM stream_tmp.iot_device_flink").print();


    }


}
