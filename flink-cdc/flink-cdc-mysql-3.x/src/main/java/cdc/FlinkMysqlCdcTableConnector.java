package cdc;

import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.cdc.connectors.mysql.source.MySqlSource;
import org.apache.flink.cdc.connectors.mysql.table.StartupOptions;
import org.apache.flink.cdc.debezium.table.RowDataDebeziumDeserializeSchema;
import org.apache.flink.streaming.api.CheckpointingMode;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.table.data.RowData;
import org.apache.flink.table.types.logical.RowType;
import org.apache.flink.types.RowKind;
import org.apache.flink.util.Collector;

import java.time.ZoneOffset;

import static org.apache.flink.table.api.DataTypes.*;

/**
 * 可实现mysql 8.0  cdc
 * @author manmao
 * @since 2020-03-17
 */
public class FlinkMysqlCdcTableConnector {

    public static void main(String[] args) throws Exception {

        RowType schema = (RowType) ROW(
                FIELD("id", BIGINT().notNull()),
                FIELD("tenant_id", BIGINT()),
                FIELD("name", STRING()),
                FIELD("code", STRING()),
                FIELD("tag", STRING())
        ).getLogicalType();

        RowDataDebeziumDeserializeSchema.ValueValidator valueValidator = new RowDataDebeziumDeserializeSchema.ValueValidator() {

            @Override
            public void validate(RowData rowData, RowKind rowKind) throws Exception {
            }
        };

        RowDataDebeziumDeserializeSchema deserializationSchema =  RowDataDebeziumDeserializeSchema.newBuilder()
                .setPhysicalRowType(schema)
                .setResultTypeInfo( TypeInformation.of(RowData.class))
                .setValueValidator(valueValidator)
                .setServerTimeZone(ZoneOffset.UTC)
                .build();

        MySqlSource<RowData> source = MySqlSource.<RowData>builder()
                .hostname("10.70.40.27")
                .port(30306)
                // monitor all tables under inventory database
                .databaseList("test")
                .username("root")
                .password("test123")
                .tableList("test.connector")
                // converts SourceRecord to String
                .deserializer(deserializationSchema)
                .startupOptions(StartupOptions.latest())
                .includeSchemaChanges(true)
                .build();

        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.getCheckpointConfig().setCheckpointInterval(5 * 60 * 1000);
        env.getCheckpointConfig().setCheckpointingMode(CheckpointingMode.EXACTLY_ONCE);


        // use parallelism 1 for sink to keep message ordering
        DataStream<RowData> stream = env.fromSource(source, WatermarkStrategy.noWatermarks(), "Kafka Source");

        stream.flatMap(new FlatMapFunction<RowData, Object>() {

            @Override
            public void flatMap(RowData rowData, Collector<Object> collector) throws Exception {
                // 打印操作类型和数据
                System.out.println(rowData.getRowKind().toString() +":"+rowData);
            }
        });

        env.execute();

    }


}
