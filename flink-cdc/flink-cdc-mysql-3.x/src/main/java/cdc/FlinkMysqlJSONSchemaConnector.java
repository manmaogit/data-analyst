package cdc;

import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.cdc.connectors.mysql.source.MySqlSource;
import org.apache.flink.cdc.connectors.mysql.table.StartupOptions;
import org.apache.flink.cdc.debezium.JsonDebeziumDeserializationSchema;
import org.apache.flink.streaming.api.CheckpointingMode;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.util.Collector;

/**
 * 可实现mysql 8.0  cdc
 * @author manmao
 * @since 2020-03-17
 */
public class FlinkMysqlJSONSchemaConnector {

    public static void main(String[] args) throws Exception {

        MySqlSource<String> source = MySqlSource.<String>builder()
                .hostname("10.10.110.224")
                .port(13306)
                // monitor all tables under inventory database
                .databaseList("lms_common")
                .username("root")
                .password("67KbkwyGNx!ZER3f")
                .tableList("lms_common.lms_order_division_config")
                // 消费存量数据+增量数据，当snapshot数据消费完之后，需要等待checkpoint完成才会进入binlog增量数据消费
                .startupOptions(StartupOptions.initial())
                // 消费增量数据
                //.startupOptions(StartupOptions.latest())
                .includeSchemaChanges(true)
                // converts SourceRecord to String
                .deserializer(new JsonDebeziumDeserializationSchema())
                .build();

        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        env.getCheckpointConfig().setCheckpointInterval( 5* 30 * 1000);
        env.getCheckpointConfig().setCheckpointingMode(CheckpointingMode.EXACTLY_ONCE);

        // use parallelism 1 for sink to keep message ordering
        DataStream<String> stream = env.fromSource(source, WatermarkStrategy.noWatermarks(), "MySQL Source").setParallelism(4);

        /**
         * JSON数据格式
         * {
         *     "before": {
         *         "id": 56,
         *         "operation_type": "order",
         *         "last_modifyor_time": 1698856686000
         *     },
         *     "after": {
         *         "id": 56,
         *         "operation_type": "order"
         *     },
         *     "source": {
         *         "version": "1.9.7.Final",
         *         "connector": "mysql",
         *         "name": "mysql_binlog_source",
         *         "ts_ms": 1698827913000,
         *         "snapshot": "false",
         *         "db": "lms_common",
         *         "sequence": null,
         *         "table": "lms_order_division_config",
         *         "server_id": 110224,
         *         "gtid": "018331e0-3801-11ed-8681-005056a74259:18799424",
         *         "file": "mysql-bin.000786",
         *         "pos": 808172758,
         *         "row": 0,
         *         "thread": 2960539,
         *         "query": null
         *     },
         *     "op": "u",  // c、u、d 创建，更新，删除
         *     "ts_ms": 1698827913637,
         *     "transaction": null
         * }
         */
        stream.flatMap(new FlatMapFunction<String, Object>() {

            @Override
            public void flatMap(String rowData, Collector<Object> collector) throws Exception {
                // 打印操作类型和数据
                System.out.println("消费到变更数据："+rowData);
            }
        });

        env.execute();

    }


}
