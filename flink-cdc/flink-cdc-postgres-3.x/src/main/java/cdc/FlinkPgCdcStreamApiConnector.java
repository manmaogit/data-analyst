package cdc;

import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.cdc.connectors.postgres.PostgreSQLSource;
import org.apache.flink.cdc.connectors.postgres.table.PostgresValueValidator;
import org.apache.flink.cdc.debezium.table.RowDataDebeziumDeserializeSchema;
import org.apache.flink.streaming.api.CheckpointingMode;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.source.SourceFunction;
import org.apache.flink.table.data.RowData;
import org.apache.flink.table.types.logical.RowType;
import org.apache.flink.util.Collector;

import java.time.ZoneOffset;

import static org.apache.flink.table.api.DataTypes.*;

/**
 * @author manmao
 * @since 2020-03-17
 */
public class FlinkPgCdcStreamApiConnector {

    public static void main(String[] args) throws Exception {

        RowType schema = (RowType) ROW(
                FIELD("id", BIGINT().notNull()),
                FIELD("tenant_id", BIGINT()),
                FIELD("name", STRING()),
                FIELD("create_date", DATE())
        ).getLogicalType();

        RowDataDebeziumDeserializeSchema deserializationSchema =  RowDataDebeziumDeserializeSchema.newBuilder()
                .setResultTypeInfo(TypeInformation.of(RowData.class))
                .setValueValidator(new PostgresValueValidator("public","test"))
                .setServerTimeZone(ZoneOffset.UTC)
                .build();

        SourceFunction<RowData> sourceFunction = PostgreSQLSource.<RowData>builder()
                .hostname("10.70.40.27")
                .port(30795)
                // monitor all tables under inventory database
                .database("ipaas_config")
                .username("postgres")
                .password("qyPcfj782yR")
                .tableList("public.project")
                // converts SourceRecord to String
                .deserializer(deserializationSchema)
                .build();

        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.getCheckpointConfig().setCheckpointInterval(5 * 60 * 1000);
        env.getCheckpointConfig().setCheckpointingMode(CheckpointingMode.EXACTLY_ONCE);

        // use parallelism 1 for sink to keep message ordering
        DataStream<RowData> stream = env.addSource(sourceFunction);

        stream.flatMap(new FlatMapFunction<RowData, Object>() {

            @Override
            public void flatMap(RowData rowData, Collector<Object> collector) throws Exception {
                // 打印操作类型和数据
                System.out.println(rowData.getRowKind().toString() + ":" + rowData.getString(4));
            }
        });
        env.execute();

    }


}
