package com.data.flink.window;

import org.apache.flink.api.common.state.MapState;
import org.apache.flink.api.common.state.MapStateDescriptor;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.functions.windowing.RichAllWindowFunction;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.flink.util.Collector;

import java.util.Iterator;

/**
 * 窗口
 */
public class TrigerAlertWindow extends RichAllWindowFunction<TruckBean, TruckBean, TimeWindow> {

    private static final long serialVersionUID = 3592205012540586044L;

    MapState<String, TruckBean> mapState = null;


    @Override
    public void open(Configuration parameters) {
        MapStateDescriptor<String, TruckBean> desc = new MapStateDescriptor<String, TruckBean>("history_sh", String.class, TruckBean.class);
        mapState = getRuntimeContext().getMapState(desc);

        System.out.println("init open function");

    }

    @Override
    public void apply(TimeWindow window, Iterable<TruckBean> values, Collector<TruckBean> out) throws Exception {


        System.out.println("thread:" + Thread.currentThread().getId());

        for (TruckBean truckBean : values) {
            if (mapState.contains(truckBean.imei)) {

                if (mapState.get(truckBean.imei).time < truckBean.time) {
                    mapState.put(truckBean.imei, truckBean);
                }
            } else {
                mapState.put(truckBean.imei, truckBean);
            }
        }

        int count = 0;
        for (String key : mapState.keys()) {
            count++;
        }
        System.out.println("mapstate size:" + count);


        Iterator<TruckBean> iterator = mapState.values().iterator();
        long currentTime = System.currentTimeMillis();
        while (iterator.hasNext()) {
            TruckBean truckBean = iterator.next();
            if (currentTime - truckBean.time > 8000) {
                System.out.println("triger alert with imei:" + truckBean.imei + ",interval time:" + (currentTime - truckBean.time));
                out.collect(truckBean);
            }
        }
    }
}
