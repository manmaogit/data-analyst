package com.data.flink.topn;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrderDetail {

    /**
     * 下单用户id
     */
    private Long userId;
    /**
     * 商品id
     */
    private Long itemId;
    /**
     * 用户所在城市
     */
    private String citeName;

    /**
     * 订单金额
     */
    private Double price;

    /**
     * 下单时间
     */
    private Long timeStamp;

}