package com.data.flink.source;

import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.api.common.serialization.SimpleStringSchema;
import org.apache.flink.connector.base.DeliveryGuarantee;
import org.apache.flink.connector.kafka.sink.KafkaRecordSerializationSchema;
import org.apache.flink.connector.kafka.sink.KafkaSink;
import org.apache.flink.connector.kafka.source.KafkaSource;
import org.apache.flink.connector.kafka.source.KafkaSourceBuilder;
import org.apache.flink.connector.kafka.source.enumerator.initializer.OffsetsInitializer;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.connectors.kafka.*;
import org.apache.kafka.clients.consumer.OffsetResetStrategy;
import org.apache.kafka.common.TopicPartition;

import java.util.*;

/**
 * @author manmao
 * @since 2019-01-15
 */
public class KafkaStreamDemo {

    public static void main(String[] args) {
        final StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        // checkpoint every 5000 msecs
        env.enableCheckpointing(5000);

        DataStream<String> stream = env.fromSource(flinkKafkaConsume(), WatermarkStrategy.noWatermarks(), "Kafka Source");

        /*
         * kafka sink
         */
        KafkaSink<String> sink = KafkaSink.<String>builder()
                .setBootstrapServers("127.0.0.1")
                .setRecordSerializer(KafkaRecordSerializationSchema.builder()
                        .setTopic("topic-name")
                        .setValueSerializationSchema(new SimpleStringSchema())
                        .build()
                )
                .setDeliveryGuarantee(DeliveryGuarantee.AT_LEAST_ONCE)
                .build();
        stream.sinkTo(sink);
        // versions 0.10+ allow attaching the records' event timestamp when writing them to Kafka;
        // this method is not available for earlier Kafka versions
        //myProducer.setWriteTimestampToKafka(true);

    }



    /**
     * 设置 kafka source 从指定的offset开始消费
     *
     * @param kafkaSourceBuilder kafka消费者构造器
     */
    public static void setStartFromSpecificOffsets(KafkaSourceBuilder<String> kafkaSourceBuilder) {
        final HashSet<TopicPartition> partitionSet = new HashSet<>(Arrays.asList(
                // Partition 0 of topic "topic-a"
                new TopicPartition("topic-a", 0),
                // Partition 5 of topic "topic-b"
                new TopicPartition("topic-b", 5)));
        kafkaSourceBuilder.setPartitions(partitionSet);

        // 从指定的位置消费出来
        KafkaSource.builder()
                // Start from committed offset of the consuming group, without reset strategy
                .setStartingOffsets(OffsetsInitializer.committedOffsets())
                // Start from committed offset, also use EARLIEST as reset strategy if committed offset doesn't exist
                .setStartingOffsets(OffsetsInitializer.committedOffsets(OffsetResetStrategy.EARLIEST))
                // Start from the first record whose timestamp is greater than or equals a timestamp
                .setStartingOffsets(OffsetsInitializer.timestamp(1592323200L))
                // Start from earliest offset
                .setStartingOffsets(OffsetsInitializer.earliest())
                // Start from latest offset
                .setStartingOffsets(OffsetsInitializer.latest());

    }

    /**
     * Flink Kafka数据源
     *
     * @return Flink Kafka数据源
     */
    private static KafkaSource<String> flinkKafkaConsume() {
        return KafkaSource.<String>builder()
                .setProperty("flink.poll-timeout", "50")
                .setProperty(FlinkKafkaConsumerBase.KEY_PARTITION_DISCOVERY_INTERVAL_MILLIS, "60000")
                .setProperty("security.protocol", "SASL_PLAINTEXT")
                .setProperty("sasl.kerberos.service.name", "kafka")
                .setBootstrapServers("127.0.0.1")
                .setTopics("input-topic")
                .setGroupId("consumer_group")
                .setStartingOffsets(OffsetsInitializer.earliest())
                .setValueOnlyDeserializer(new SimpleStringSchema())
                .build();
    }


}
