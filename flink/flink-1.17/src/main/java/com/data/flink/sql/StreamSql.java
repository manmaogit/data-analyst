package com.data.flink.sql;

import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.api.common.serialization.SimpleStringSchema;
import org.apache.flink.connector.kafka.source.KafkaSource;
import org.apache.flink.connector.kafka.source.enumerator.initializer.OffsetsInitializer;
import org.apache.flink.streaming.api.TimeCharacteristic;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumerBase;
import org.apache.flink.table.api.EnvironmentSettings;
import org.apache.flink.table.api.Schema;
import org.apache.flink.table.api.Table;
import org.apache.flink.table.api.TableResult;
import org.apache.flink.table.api.bridge.java.StreamTableEnvironment;
import org.apache.flink.table.types.logical.TimestampType;
import org.apache.flink.table.types.logical.VarCharType;
import org.apache.flink.types.Row;
import org.apache.flink.util.CloseableIterator;


/**
 * @author lx
 */
public class StreamSql {

    private static final String KAFKA_TOPIC = "topic_test";

    private static final String KAFKA_TOPIC_TABLE_NAME = "topic_test";

    private static final String OUTPUT_FILE_PATH="test";

    public static void main(String[] args) throws Exception {

        // streaming env
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setStreamTimeCharacteristic(TimeCharacteristic.ProcessingTime);

        // 创建tableEnv
        EnvironmentSettings fsSettings = EnvironmentSettings.newInstance().inStreamingMode().build();
        StreamExecutionEnvironment fsEnv = StreamExecutionEnvironment.getExecutionEnvironment();
        StreamTableEnvironment tableEnv = StreamTableEnvironment.create(fsEnv, fsSettings);

        DataStream<String> stream =  env.fromSource(flinkKafkaConsume(), WatermarkStrategy.noWatermarks(), "Kafka Source");

        // tableEnv关联到kafka $("imei"), $("truckno"), $("proctime")
        // stream 注册成临时Table
        tableEnv.createTemporaryView(KAFKA_TOPIC_TABLE_NAME, stream,
                Schema.newBuilder()
                        .column("imei", new VarCharType(32).asSerializableString())
                        .column("truckno", new VarCharType(64).asSerializableString())
                        .column("proctime", new TimestampType().asSerializableString())
                        .build());

        // 使用tableEnv执行SQL
        Table sqlResult = tableEnv.sqlQuery("SELECT TUMBLE_START(proctime, INTERVAL '1' MINUTE) as procTime,imei FROM "
                        + KAFKA_TOPIC_TABLE_NAME + " GROUP BY TUMBLE(proctime, INTERVAL '1' MINUTE) ");

        // table转换成 Data Stream
        DataStream<Row>  rowDataStream = tableEnv.toDataStream(sqlResult);

        // 返回结果
        CloseableIterator<Row> resultRow = sqlResult.execute().collect();
        env.execute();

    }

    /**
     * Flink Kafka数据源
     *
     * @return Flink Kafka数据源
     */
    private static KafkaSource<String> flinkKafkaConsume() {
        return KafkaSource.<String>builder()
                .setProperty("flink.poll-timeout", "50")
                .setProperty(FlinkKafkaConsumerBase.KEY_PARTITION_DISCOVERY_INTERVAL_MILLIS, "60000")
                .setProperty("security.protocol", "SASL_PLAINTEXT")
                .setProperty("sasl.kerberos.service.name", "kafka")
                .setBootstrapServers("127.0.0.1")
                .setTopics(KAFKA_TOPIC)
                .setGroupId("consumer_group")
                .setStartingOffsets(OffsetsInitializer.earliest())
                .setValueOnlyDeserializer(new SimpleStringSchema())
                .build();
    }

}
