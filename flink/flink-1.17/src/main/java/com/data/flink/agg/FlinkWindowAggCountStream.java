package com.data.flink.agg;

import com.alibaba.fastjson.JSON;
import com.data.flink.entity.UserBehavior;
import com.data.flink.source.LocalFileSource;
import lombok.extern.slf4j.Slf4j;
import org.apache.flink.api.common.functions.AggregateFunction;
import org.apache.flink.api.common.functions.RichFlatMapFunction;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.CheckpointingMode;
import org.apache.flink.streaming.api.TimeCharacteristic;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.CheckpointConfig;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.sink.RichSinkFunction;
import org.apache.flink.streaming.api.functions.source.SourceFunction;
import org.apache.flink.streaming.api.windowing.assigners.SlidingEventTimeWindows;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.util.Collector;

/**
 *
 * 简单的将两个流按照key hash 合并到一起
 *
 * @author manmao
 */
@Slf4j
public class FlinkWindowAggCountStream {

    private static final int PARALLELISM = 4;

    private static final Long CHECK_POINT_TIME = 5 * 60 * 1000L;


    public static void main(String[] args) throws Exception {
        SourceFunction<String> source1 = new LocalFileSource("/Users/g7/Desktop/user_behavior.log");
        initContainer(source1);
    }


    private static void initContainer(SourceFunction<String> source1) throws Exception {
        StreamExecutionEnvironment env = initExecutionEnvironment();

        env.setStreamTimeCharacteristic(TimeCharacteristic.ProcessingTime);


        DataStream<UserBehavior> objectStream1 = env.addSource(source1).name("source1").uid("source1").flatMap(new FilterMap());

        DataStream<AggCount> aggregateStream = objectStream1
                .windowAll(SlidingEventTimeWindows.of(Time.seconds(5),Time.seconds(3)))
                .aggregate(new AggregateFunction<UserBehavior,AggCount , AggCount>() {
            @Override
            public AggCount createAccumulator() {
                return new AggCount(0);
            }

            @Override
            public AggCount add(UserBehavior value, AggCount accumulator) {
                 accumulator.setCount(accumulator.getCount() + 1);
                return accumulator;
            }

            @Override
            public AggCount getResult(AggCount accumulator) {
                return accumulator;
            }

            @Override
            public AggCount merge(AggCount a, AggCount b) {
                a.setCount(a.getCount()+b.getCount());
                return a;
            }
        });


        aggregateStream.addSink(new SimpleSinkFunction()).name("sink").uid("sink");

        env.execute("local stream test");
    }


    /**
     * Flink执行环境
     *
     * @return Flink流处理的执行环境
     */
    private static StreamExecutionEnvironment initExecutionEnvironment() {
        StreamExecutionEnvironment streamExecutionEnvironment = StreamExecutionEnvironment.createLocalEnvironmentWithWebUI(new Configuration());
        streamExecutionEnvironment.setParallelism(PARALLELISM);
        streamExecutionEnvironment.enableCheckpointing(CHECK_POINT_TIME);
        final CheckpointConfig checkpointConfig = streamExecutionEnvironment.getCheckpointConfig();
        checkpointConfig.setCheckpointTimeout(CHECK_POINT_TIME);
        checkpointConfig.setMinPauseBetweenCheckpoints(CHECK_POINT_TIME);
        checkpointConfig.setCheckpointingMode(CheckpointingMode.AT_LEAST_ONCE);
        checkpointConfig.setExternalizedCheckpointCleanup(CheckpointConfig.ExternalizedCheckpointCleanup.RETAIN_ON_CANCELLATION);
        return streamExecutionEnvironment;
    }


    public static class FilterMap extends RichFlatMapFunction<String, UserBehavior> {

        @Override
        public void flatMap(String s, Collector<UserBehavior> collector) throws Exception {
            UserBehavior userBehavior = JSON.parseObject(s, UserBehavior.class);
            collector.collect(userBehavior);
        }
    }




    public static class SimpleSinkFunction extends RichSinkFunction<AggCount> {

        @Override
        public void open(Configuration parameters) throws Exception {
            super.open(parameters);
        }

        @Override
        public void invoke(AggCount value, Context context) throws Exception {
            log.info(value.getCount() + "");
        }
    }


}
