package com.data.flink.async.io;

import com.alibaba.fastjson.JSON;
import com.data.flink.entity.UserBehavior;
import com.data.flink.source.LocalFileSource;
import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.datastream.AsyncDataStream;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.async.AsyncRetryStrategy;
import org.apache.flink.streaming.api.functions.async.ResultFuture;
import org.apache.flink.streaming.api.functions.async.RichAsyncFunction;
import org.apache.flink.streaming.api.functions.sink.SinkFunction;
import org.apache.flink.streaming.util.retryable.AsyncRetryStrategies;
import org.apache.flink.streaming.util.retryable.RetryPredicates;
import org.apache.flink.util.Collector;

import java.util.Collections;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;

/**
 * @author manmao
 * @since 2020-08-30
 */
public class AsyncRequestDemo {

    public static void main(String[] args) throws Exception {

        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(2);
        env.getCheckpointConfig().setCheckpointInterval(1000 * 20);

        DataStream<String> stream = env.addSource(new LocalFileSource("/Users/g7/Desktop/user_behavior.log", 100));

        DataStream<UserBehavior> mapStream = stream.flatMap(new FlatMapFunction<String, UserBehavior>() {
            @Override
            public void flatMap(String value, Collector<UserBehavior> out) throws Exception {
                out.collect(JSON.parseObject(value,UserBehavior.class));
            }
        });

        // 应用异步 I/O 转换操作并启用重试 或者 通过工具类创建一个异步重试策略, 或用户实现自定义的策略  重试3次，每次退避时间100ms
        AsyncRetryStrategy<String> asyncRetryStrategy =
                new AsyncRetryStrategies.FixedDelayRetryStrategyBuilder<String>(3, 100L) // maxAttempts=3, fixedDelay=100ms
                        .ifResult(RetryPredicates.EMPTY_RESULT_PREDICATE)
                        .ifException(RetryPredicates.HAS_EXCEPTION_PREDICATE)
                        .build();

        // apply the async I/O transformation
        // 异步算子超时时间两分钟
        DataStream<String> resultStream =
                AsyncDataStream.orderedWaitWithRetry(mapStream, new AsyncDatabaseRequest(), 3 * 1000, TimeUnit.MILLISECONDS, 10,asyncRetryStrategy);

        resultStream.addSink(new SinkFunction<String>() {
            @Override
            public void invoke(String value, Context context) throws Exception {
                System.out.println("sink:" + value);
            }
        });

        env.execute("start");


    }




    static class AsyncDatabaseRequest extends RichAsyncFunction<UserBehavior, String> {


        @Override
        public void open(Configuration parameters) throws Exception {
            super.open(parameters);
        }

        @Override
        public void close() throws Exception {
            super.close();
        }


        /**
         * flink所谓的异步IO，并不是只要实现了asyncInvoke方法就是异步了，这个方法并不是异步的，而是要依靠这个方法里面所写的查询是异步的才可以。
         *  UserBehavior数据在checkpoint的时候会写入state。
         * @param input        输入
         * @param resultFuture 输出
         * @throws Exception
         */
        @Override
        public void asyncInvoke(UserBehavior input, ResultFuture<String> resultFuture) throws Exception {
            System.out.println("异步算子:" + input);
            CompletableFuture.supplyAsync(new Supplier<String>() {
                @Override
                public String get() {
                    try {
                        // 模拟超时操作
                        Thread.sleep(1000 * 2);
                    } catch (InterruptedException e) {
                        return "";
                    }
                    return input.getUserId()+"";
                }
            }).thenAccept((String dbResult) -> {
                resultFuture.complete(Collections.singleton(dbResult));
            });
        }

        @Override
        public void timeout(UserBehavior input, ResultFuture<String> resultFuture) throws Exception {
            // 超时处理  如果不调用complete方法,数据无法提交,直到 asyncInvoke 异步算子执行完成。
            resultFuture.complete(null);
        }

    }

}
