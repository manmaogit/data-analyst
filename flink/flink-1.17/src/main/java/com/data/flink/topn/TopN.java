package com.data.flink.topn;

import com.alibaba.fastjson.JSON;
import com.data.flink.source.LocalFileSource;
import org.apache.flink.api.common.eventtime.SerializableTimestampAssigner;
import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.api.common.functions.ReduceFunction;
import org.apache.flink.api.common.serialization.SimpleStringSchema;
import org.apache.flink.api.java.functions.KeySelector;
import org.apache.flink.connector.kafka.source.KafkaSource;
import org.apache.flink.connector.kafka.source.enumerator.initializer.OffsetsInitializer;
import org.apache.flink.streaming.api.CheckpointingMode;
import org.apache.flink.streaming.api.TimeCharacteristic;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.windowing.ProcessAllWindowFunction;
import org.apache.flink.streaming.api.windowing.assigners.SlidingEventTimeWindows;
import org.apache.flink.streaming.api.windowing.assigners.TumblingEventTimeWindows;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.flink.streaming.connectors.redis.RedisSink;
import org.apache.flink.streaming.connectors.redis.common.config.FlinkJedisPoolConfig;
import org.apache.flink.streaming.connectors.redis.common.mapper.RedisCommand;
import org.apache.flink.streaming.connectors.redis.common.mapper.RedisCommandDescription;
import org.apache.flink.streaming.connectors.redis.common.mapper.RedisMapper;
import org.apache.flink.util.Collector;

import java.time.Duration;
import java.util.Comparator;
import java.util.Map;
import java.util.TreeMap;

/**
 * 计算订单金额前十的用户
 *
 * @author manmao
 */
public class TopN {

    public static void main(String[] args) throws Exception {

        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime);
        env.enableCheckpointing(60 * 1000, CheckpointingMode.EXACTLY_ONCE);
        env.getCheckpointConfig().setCheckpointTimeout(30 * 1000);

        DataStream<String> stream = env.addSource(new LocalFileSource("/Users/g7/Desktop/untitled.json", 500));

        DataStream<OrderDetail> orderStream = stream.map(message -> {
            OrderDetail orderDetail = JSON.parseObject(message, OrderDetail.class);
            orderDetail.setTimeStamp(System.currentTimeMillis());
            return orderDetail;
        });

        // 生成时间水印
        DataStream<OrderDetail> dataStream = orderStream.assignTimestampsAndWatermarks((WatermarkStrategy.<OrderDetail>forBoundedOutOfOrderness(Duration.ofSeconds(2))
                .withTimestampAssigner(new SerializableTimestampAssigner<OrderDetail>() {
                    @Override
                    public long extractTimestamp(OrderDetail element, long recordTimestamp) {
                        //指定EventTime对应的字段
                        return element.getTimeStamp();
                    }
                })));

        // 计算10s的数据,每5秒滑动一次，将每个用户支付的金额，累加起来
        DataStream<OrderDetail> reduceStream = dataStream
                .keyBy((KeySelector<OrderDetail, Long>) OrderDetail::getUserId)
                .window(SlidingEventTimeWindows.of(Time.seconds(10), Time.seconds(5)))
                .reduce(new ReduceFunction<OrderDetail>() {
                    @Override
                    public OrderDetail reduce(OrderDetail value1, OrderDetail value2) throws Exception {
                        return new OrderDetail(value1.getUserId(), value1.getItemId(), value1.getCiteName(), value1.getPrice() + value2.getPrice(), value1.getTimeStamp());
                    }
                });



        // 计算topN
        DataStream<TreeMap<Double, OrderDetail>> process =
                reduceStream
                        .windowAll(TumblingEventTimeWindows.of(Time.seconds(5)))
                        .process(new ProcessAllWindowFunction<OrderDetail, TreeMap<Double, OrderDetail>, TimeWindow>() {
                            @Override
                            public void process(Context context, Iterable<OrderDetail> iterable, Collector<TreeMap<Double, OrderDetail>> collector) throws Exception {

                                TreeMap<Double, OrderDetail> treeMap = new TreeMap<Double, OrderDetail>(new Comparator<Double>() {
                                    @Override
                                    public int compare(Double x, Double y) {
                                        return (x > y) ? -1 : 1;
                                    }
                                });

                                for (OrderDetail orderDetail : iterable) {
                                    treeMap.put(orderDetail.getPrice(), orderDetail);
                                    if (treeMap.size() > 3) {
                                        treeMap.pollLastEntry();
                                    }
                                }

                                for (Map.Entry<Double, OrderDetail> entry : treeMap.entrySet()) {
                                    System.out.println(entry.getValue());
                                }
                                System.out.println("--------------------------------------------");
                                collector.collect(treeMap);
                            }
                        });


        FlinkJedisPoolConfig conf = new FlinkJedisPoolConfig.Builder().setHost("172.22.35.241").setPort(6379).setPassword("dsptest-redis").build();

        process.addSink(new RedisSink<>(conf, new RedisMapper<TreeMap<Double, OrderDetail>>() {

            // HASH SORTSET 需要设置KEY
            private final String TOPN = "TOPN:";

            @Override
            public RedisCommandDescription getCommandDescription() {
                return new RedisCommandDescription(RedisCommand.SET, TOPN);
            }

            @Override
            public String getKeyFromData(TreeMap<Double, OrderDetail> data) {
                // SET 操作的key
                return String.valueOf("topn");
            }

            @Override
            public String getValueFromData(TreeMap<Double, OrderDetail> data) {
                return String.valueOf(JSON.toJSONString(data));
            }
        }));

        env.execute("execute topn");

    }


    /**
     *  https://nightlies.apache.org/flink/flink-docs-release-1.15/docs/connectors/datastream/kafka/
     */
    private static KafkaSource<String> initConsumer() {
        return KafkaSource.<String>builder()
                .setBootstrapServers("localhost:9092")
                .setTopics("input-topic")
                .setGroupId("my-group")
                .setStartingOffsets(OffsetsInitializer.earliest())
                .setValueOnlyDeserializer(new SimpleStringSchema())
                .build();
    }
}
