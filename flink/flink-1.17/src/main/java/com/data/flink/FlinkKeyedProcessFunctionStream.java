package com.data.flink;

import com.alibaba.fastjson.JSON;
import com.data.flink.entity.UserBehavior;
import com.data.flink.source.LocalFileSource;
import lombok.extern.slf4j.Slf4j;
import org.apache.flink.api.common.functions.RichFlatMapFunction;
import org.apache.flink.api.common.state.MapState;
import org.apache.flink.api.common.state.MapStateDescriptor;
import org.apache.flink.api.common.state.StateTtlConfig;
import org.apache.flink.api.common.time.Time;
import org.apache.flink.api.java.functions.KeySelector;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.CheckpointingMode;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.CheckpointConfig;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.KeyedProcessFunction;
import org.apache.flink.streaming.api.functions.sink.RichSinkFunction;
import org.apache.flink.streaming.api.functions.source.SourceFunction;
import org.apache.flink.util.Collector;

import java.util.Iterator;
import java.util.Map;

/**
 * @author manmao
 */
@Slf4j
public class FlinkKeyedProcessFunctionStream {

    private static final int PARALLELISM = 4;

    private static final Long CHECK_POINT_TIME = 5 * 60 * 1000L;


    public static void main(String[] args) throws Exception {
        initContainer(new LocalFileSource("/Users/lx/Desktop/user_behavior.log"));
    }


    private static void initContainer(SourceFunction<String> source) throws Exception {
        StreamExecutionEnvironment env = initExecutionEnvironment();

        DataStream<UserBehavior> objectStream =
                env.addSource(source).name("source").uid("source").flatMap(new FilterMap());

        DataStream<Tuple2<String, Integer>> result = objectStream
                .keyBy((KeySelector<UserBehavior, Long>) UserBehavior::getUserId)
                .process(new CustomKeyedProcessFunction()).name("process").uid("process");

        result.addSink(new SimpleSinkFunction()).name("sink").uid("sink");

        env.execute("local stream test");
    }


    /**
     * Flink执行环境
     *
     * @return Flink流处理的执行环境
     */
    private static StreamExecutionEnvironment initExecutionEnvironment() {
        StreamExecutionEnvironment streamExecutionEnvironment = StreamExecutionEnvironment.getExecutionEnvironment();
        streamExecutionEnvironment.setParallelism(PARALLELISM);
        streamExecutionEnvironment.enableCheckpointing(CHECK_POINT_TIME);
        final CheckpointConfig checkpointConfig = streamExecutionEnvironment.getCheckpointConfig();
        checkpointConfig.setCheckpointTimeout(CHECK_POINT_TIME);
        checkpointConfig.setMinPauseBetweenCheckpoints(CHECK_POINT_TIME);
        checkpointConfig.setCheckpointingMode(CheckpointingMode.AT_LEAST_ONCE);
        checkpointConfig.setExternalizedCheckpointCleanup(CheckpointConfig.ExternalizedCheckpointCleanup.RETAIN_ON_CANCELLATION);
        return streamExecutionEnvironment;
    }


    public static class FilterMap extends RichFlatMapFunction<String, UserBehavior> {

        @Override
        public void flatMap(String s, Collector<UserBehavior> collector) throws Exception {
            UserBehavior userBehavior = JSON.parseObject(s, UserBehavior.class);
            collector.collect(userBehavior);
        }
    }


    public static class CustomKeyedProcessFunction extends KeyedProcessFunction<Long, UserBehavior, Tuple2<String, Integer>> {

        MapState<String, Integer> mapState;

        @Override
        public void open(Configuration parameters) throws Exception {
            super.open(parameters);

            MapStateDescriptor<String, Integer> mapStateDescriptor = new MapStateDescriptor<>("map", String.class, Integer.class);

            // 设置state时间
            StateTtlConfig ttlConfig = StateTtlConfig
                    .newBuilder(Time.seconds(10))
                    .cleanupFullSnapshot()
                    .build();
            mapStateDescriptor.enableTimeToLive(ttlConfig);

            mapState = this.getRuntimeContext().getMapState(mapStateDescriptor);

        }

        @Override
        public void processElement(UserBehavior userBehavior, Context context, Collector<Tuple2<String, Integer>> collector) throws Exception {
            String key = userBehavior.userId + "";
            Integer count = mapState.get(key);
            if (count == null) {
                count = 0;
            }
            mapState.put(key, count + 1);
            context.timerService().registerProcessingTimeTimer(System.currentTimeMillis() + 1000 * 6);
        }

        @Override
        public void onTimer(long timestamp, OnTimerContext ctx, Collector<Tuple2<String, Integer>> out) throws Exception {
            super.onTimer(timestamp, ctx, out);
            Iterator<Map.Entry<String, Integer>> irater = mapState.entries().iterator();
            while (irater.hasNext()) {
                Map.Entry<String, Integer> item = irater.next();
                // 删除
                irater.remove();
                out.collect(Tuple2.of(item.getKey(), item.getValue()));
            }

        }

    }


    public static class SimpleSinkFunction extends RichSinkFunction<Tuple2<String, Integer>> {

        @Override
        public void open(Configuration parameters) throws Exception {
            super.open(parameters);
        }

        @Override
        public void invoke(Tuple2<String, Integer> value, Context context) throws Exception {
            log.info(value.f0 + "-x-" + value.f1);
        }
    }


}
