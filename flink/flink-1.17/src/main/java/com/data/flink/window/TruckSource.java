package com.data.flink.window;

import com.google.common.collect.Lists;
import org.apache.flink.api.java.tuple.Tuple4;
import org.apache.flink.streaming.api.functions.source.SourceFunction;

import java.util.List;
import java.util.Random;
import java.util.UUID;

/**
 * 数据源
 */
public  class TruckSource implements SourceFunction<Tuple4<String, Double, Double, Long>> {

    private static final long serialVersionUID = 1L;

    Random locationRandom = new Random(360);
    Random random = new Random();

    private volatile boolean isRunning = true;

    private double lat;
    private double lng;

    private int numOfCars = 100;

    private List<String> imeiListFirst = Lists.newArrayList();
    private List<String> imeiListSecond = Lists.newArrayList();


    public TruckSource(){
        this(100);
    }

    public TruckSource(int numOfCars) {

        for (int i = 0; i < numOfCars; i++) {
            imeiListFirst.add(UUID.randomUUID().toString());
            imeiListSecond.add(UUID.randomUUID().toString());
        }
        this.numOfCars = numOfCars;
    }

    public static TruckSource create(int cars) {
        return new TruckSource(cars);
    }

    @Override
    public void run(SourceContext<Tuple4<String, Double, Double, Long>> ctx) throws Exception {

        int count = 0;

        while (isRunning) {
            Thread.sleep(1000);

            List<String> seekList = null;
            if (count % 20 < 10) {
                seekList = imeiListFirst;
            } else {
                seekList = imeiListSecond;
            }
            count++;

            for (int i = 0; i < numOfCars; i++) {

                if (random.nextBoolean()) {
                    Tuple4<String, Double, Double, Long> record = new Tuple4<>(seekList.get(i),
                            locationRandom.nextDouble(), locationRandom.nextDouble(), System.currentTimeMillis());
                    ctx.collect(record);
                } else {
                    //System.out.println("dont output location for imei:" + seekList.get(i));
                }
            }
        }
    }

    @Override
    public void cancel() {
        isRunning = false;
    }
}
