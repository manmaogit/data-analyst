package com.data.flink.hotitem;

import com.alibaba.fastjson.JSON;
import com.data.flink.entity.ItemViewCount;
import com.data.flink.entity.UserBehavior;
import com.data.flink.source.LocalFileSource;
import org.apache.flink.api.common.eventtime.SerializableTimestampAssigner;
import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.api.common.functions.AggregateFunction;
import org.apache.flink.api.common.functions.RichFlatMapFunction;
import org.apache.flink.api.common.state.ListState;
import org.apache.flink.api.common.state.ListStateDescriptor;
import org.apache.flink.api.java.functions.KeySelector;
import org.apache.flink.api.java.io.PojoCsvInputFormat;
import org.apache.flink.api.java.typeutils.PojoTypeInfo;
import org.apache.flink.api.java.typeutils.TypeExtractor;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.core.fs.Path;
import org.apache.flink.streaming.api.TimeCharacteristic;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

import org.apache.flink.streaming.api.functions.KeyedProcessFunction;
import org.apache.flink.streaming.api.functions.source.SourceFunction;
import org.apache.flink.streaming.api.functions.windowing.WindowFunction;
import org.apache.flink.streaming.api.windowing.assigners.SlidingEventTimeWindows;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.flink.util.Collector;
import org.apache.flink.util.OutputTag;

import java.io.File;
import java.sql.Timestamp;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * 热门商品
 * 求每5分钟，求过去60分钟内，商品点击次数前三的商品。输出格式【统计时间，商品id, 商品点击次数】
 */
public class HotItems {

    public static void main(String[] args) throws Exception {

        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);
        env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime);

        SourceFunction<String> source1 = new LocalFileSource("/Users/lx/Desktop/user_behavior.log",10);
        /*
         * 创建data source
         */
        DataStream<UserBehavior> dataSource = env.addSource(source1).name("source1").uid("source1").flatMap(new FilterMap());


        //  延迟时间，一分钟，生成 Watermark
        DataStream<UserBehavior> timedData = dataSource.assignTimestampsAndWatermarks((WatermarkStrategy.<UserBehavior>forBoundedOutOfOrderness(Duration.ofSeconds(10))
                .withTimestampAssigner(new SerializableTimestampAssigner<UserBehavior>() {
                    @Override
                    public long extractTimestamp(UserBehavior element, long recordTimestamp) {
                        // 如果时间延迟超过 currentTimeMillis-3000, 则数据会丢弃
                        return element.getTimestamp().getTime();
                    }
                })));

        // 过滤出点击事件
        DataStream<UserBehavior> pvData = timedData.filter(userBehavior -> {
            // 过滤出只有点击的数据
            return "pv".equals(userBehavior.behavior);
        });

        // 窗口统计点击量 ; 由于要每隔 5s 统计一次最近 3分钟 每个商品的点击量
        // 这个写法可以通过聚合aggregate算子,拿到一个时间窗口内,每个商品的点击次数，商品名，窗口的结束时间
        // windows 开始时间是  timestamp - (timestamp - offset + windowSize) % windowSize;
        // window 触发计算的时间是：windowEnd + lateness 的时间点
        // 根据时间时间确定 窗口触发
        DataStream<ItemViewCount> windowedData = pvData.keyBy((KeySelector<UserBehavior, Long>) UserBehavior::getItemId)
                .window(SlidingEventTimeWindows.of(Time.minutes(3), Time.seconds(10)))
                .aggregate(new CountAgg(), new WindowResultFunction());

        // 聚合
        // 将所有keyed流数据，按照窗口结束时间，分流到一起，可以得到【商品名，点击次数】
        DataStream<String> topItems = windowedData
                .keyBy((KeySelector<ItemViewCount, Long>) ItemViewCount::getWindowEnd)
                .process(new TopnHotItemsProcessFunction(3));

        topItems.print();
        env.execute("Hot Items Job");
    }


    /**
     * COUNT 统计的聚合函数实现，每出现一条记录加
     */
    public static class CountAgg implements AggregateFunction<UserBehavior, Long, Long> {

        @Override
        public Long createAccumulator() {
            return 0L;
        }

        @Override
        public Long add(UserBehavior userBehavior, Long acc) {
            return acc + 1;
        }

        @Override
        public Long getResult(Long acc) {
            return acc;
        }

        @Override
        public Long merge(Long acc, Long acc1) {
            return acc + acc1;
        }
    }

    public static class WindowResultFunction implements WindowFunction<Long, ItemViewCount, Long, TimeWindow> {

        @Override
        public void apply(Long key, // 窗口的主键，即 itemId
                          TimeWindow timeWindow, // 窗口
                          Iterable<Long> aggregateResult,// // 聚合函数的结果，即 count 值
                          Collector<ItemViewCount> collector // 输出类型为 ItemViewCount
        ) throws Exception {
            Long itemId = key;
            Long count = aggregateResult.iterator().next();
            collector.collect(ItemViewCount.of(itemId, timeWindow.getEnd(), count));
        }
    }

    /**
     * 前N个
     */
    public static class TopnHotItemsProcessFunction extends KeyedProcessFunction<Long, ItemViewCount, String> {

        private final int topSize;

        /**
         * 用于存储商品与点击数的状态，待收齐同一个窗口的数据后，再触发 TopN 计算
         */
        private ListState<ItemViewCount> itemState;

        public TopnHotItemsProcessFunction(int topSize) {
            this.topSize = topSize;
        }

        @Override
        public void open(Configuration parameters) throws Exception {
            super.open(parameters);
            ListStateDescriptor<ItemViewCount> itemStateDesc = new ListStateDescriptor<ItemViewCount>("itemState-state", ItemViewCount.class);
            itemState = getRuntimeContext().getListState(itemStateDesc);

        }

        @Override
        public void processElement(ItemViewCount input, Context context, Collector<String> collector) throws Exception {

            itemState.add(input);

            // 注册 windowEnd+1 的 EventTime Timer, 当触发时，说明收齐了属于 windowEnd 窗口的所有商品数据
            context.timerService().registerEventTimeTimer(input.windowEnd + 1);
        }

        @Override
        public void onTimer(long timestamp, OnTimerContext ctx, Collector<String> out) throws Exception {
            super.onTimer(timestamp, ctx, out);

            // 获取收到的所有商品点击量
            List<ItemViewCount> allItems = new ArrayList<>();
            for (ItemViewCount item : itemState.get()) {
                allItems.add(item);
            }
            // 清除状态，释放空间
            itemState.clear();

            // 按照点击量从大到小排序
            allItems.sort(new Comparator<ItemViewCount>() {
                @Override
                public int compare(ItemViewCount o1, ItemViewCount o2) {
                    return (int) (o2.viewCount - o1.viewCount);
                }
            });

            StringBuilder result = new StringBuilder();
            result.append("====================================\n");
            result.append("时间: ").append(new Timestamp(timestamp - 1)).append("\n");
            for (int i = 0; i < topSize && i < allItems.size(); i++) {
                ItemViewCount currentItem = allItems.get(i);
                //No1: 商品ID=12224 浏览量=2413
                result.append("No")
                        .append(i)
                        .append(":")
                        .append(" 商品ID=")
                        .append(currentItem.itemId)
                        .append(" 浏览量=")
                        .append(currentItem.viewCount)
                        .append("\n");
            }

            result.append("====================================\n\n");
            out.collect(result.toString());
        }

    }

    public static class FilterMap extends RichFlatMapFunction<String, UserBehavior> {

        @Override
        public void flatMap(String s, Collector<UserBehavior> collector) throws Exception {
            UserBehavior userBehavior = JSON.parseObject(s, UserBehavior.class);
            collector.collect(userBehavior);
        }
    }


    public DataStream<UserBehavior> CSVSource(StreamExecutionEnvironment env) {
        Path filePath = Path.fromLocalFile(new File("/Users/g7/Desktop/user_behavior.log"));

        // 抽取 UserBehavior 的 TypeInformation，是一个 PojoTypeInfo
        PojoTypeInfo<UserBehavior> pojoType = (PojoTypeInfo<UserBehavior>) TypeExtractor.createTypeInfo(UserBehavior.class);

        // 由于 Java 反射抽取出的字段顺序是不确定的，需要显式指定下文件中字段的顺序
        String[] fieldOrder = new String[]{"userId", "itemId", "categoryId", "behavior", "timestamp"};

        // 创建 PojoCsvInputFormat
        PojoCsvInputFormat<UserBehavior> csvInput = new PojoCsvInputFormat<>(filePath, pojoType, fieldOrder);

        DataStream<UserBehavior> dataSource = env.createInput(csvInput, pojoType);
        return dataSource;
    }
}
