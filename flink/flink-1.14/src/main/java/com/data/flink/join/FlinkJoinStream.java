package com.data.flink.join;

import com.alibaba.fastjson.JSON;
import com.data.flink.entity.UserBehavior;
import com.data.flink.source.LocalFileSource;
import lombok.extern.slf4j.Slf4j;
import org.apache.flink.api.common.eventtime.SerializableTimestampAssigner;
import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.api.common.functions.CoGroupFunction;
import org.apache.flink.api.common.functions.JoinFunction;
import org.apache.flink.api.common.functions.RichFlatMapFunction;
import org.apache.flink.api.common.state.CheckpointListener;
import org.apache.flink.api.java.functions.KeySelector;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.CheckpointingMode;
import org.apache.flink.streaming.api.TimeCharacteristic;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.CheckpointConfig;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.AssignerWithPeriodicWatermarks;
import org.apache.flink.streaming.api.functions.co.ProcessJoinFunction;
import org.apache.flink.streaming.api.functions.source.SourceFunction;
import org.apache.flink.streaming.api.windowing.assigners.SlidingEventTimeWindows;
import org.apache.flink.streaming.api.windowing.assigners.TumblingEventTimeWindows;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.util.Collector;

import javax.annotation.Nullable;
import java.time.Duration;

/**
 * @author manmao
 */
@Slf4j
public class FlinkJoinStream {

    private static final int PARALLELISM = 4;

    private static final Long CHECK_POINT_TIME = 5 * 60 * 1000L;


    public static void main(String[] args) throws Exception {
        SourceFunction<String> source1 = new LocalFileSource("/Users/lx/Desktop/user_behavior.log", 1000);
        SourceFunction<String> source2 = new LocalFileSource("/Users/lx/Desktop/user_behavior.log", 3000);
        initContainer(source1, source2);
    }


    private static void initContainer(SourceFunction<String> source1, SourceFunction<String> source2) throws Exception {
        StreamExecutionEnvironment env = initExecutionEnvironment();

        env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime);
        //设置周期性的产生水位线的时间间隔。当数据流很大的时候，如果每个事件都产生水位线，会影响性能。
        //默认100毫秒
        env.getConfig().setAutoWatermarkInterval(100);


        DataStream<UserBehavior> objectStream1 = env.addSource(source1).name("source1").uid("source1").flatMap(new FilterMap());

        DataStream<UserBehavior> objectStream2 = env.addSource(source2).name("source2").uid("source2").flatMap(new FilterMap());

        // >= flink 1.10 后 提取 watermark
        DataStream<UserBehavior> eventTimeStream1 = objectStream1.assignTimestampsAndWatermarks(WatermarkStrategy.<UserBehavior>forBoundedOutOfOrderness(Duration.ofSeconds(3))
                .withTimestampAssigner(new SerializableTimestampAssigner<UserBehavior>() {
                    @Override
                    public long extractTimestamp(UserBehavior element, long recordTimestamp) {
                        //指定EventTime对应的字段
                        return element.getTimestamp().getTime();
                    }
                }));

        // flink 1.10 前提取 watermark
        DataStream<UserBehavior> eventTimeStream2 = objectStream2.assignTimestampsAndWatermarks(new AssignerWithPeriodicWatermarks<UserBehavior>() {
            private long currentTime = 0;

            @Override
            public long extractTimestamp(UserBehavior element, long previousElementTimestamp) {
                currentTime = element.getTimestamp().getTime();
                return element.getTimestamp().getTime();
            }

            @Nullable
            @Override
            public org.apache.flink.streaming.api.watermark.Watermark getCurrentWatermark() {
                return new org.apache.flink.streaming.api.watermark.Watermark(currentTime);
            }
        });

        // interval join和window join 的区别
        // 官方文档 https://ci.apache.org/projects/flink/flink-docs-release-1.12/dev/stream/operators/joining.html

        // window join 操作,根据key 来进行join
        eventTimeStream1.join(eventTimeStream2)
                .where((KeySelector<UserBehavior, Long>) UserBehavior::getUserId)
                .equalTo((KeySelector<UserBehavior, Long>) UserBehavior::getUserId)
                .window(SlidingEventTimeWindows.of(Time.seconds(10), Time.seconds(1)))
                .apply(new JoinFunction<UserBehavior, UserBehavior, String>() {
                    @Override
                    public String join(UserBehavior left, UserBehavior right) {
                        System.out.println(left.getUserId() + ":" + left.getTimestamp() + "--" + right.getUserId() + ":" + right.getTimestamp());
                        return "";
                    }
                });

        /* Interval Join目前只支持Event Time */
       eventTimeStream1.keyBy((KeySelector<UserBehavior, Long>) UserBehavior::getUserId)
                .intervalJoin(eventTimeStream2.keyBy((KeySelector<UserBehavior, Long>) UserBehavior::getUserId))
                .between(Time.seconds(-5), Time.seconds(5))
                .process(new ProcessJoinFunction<UserBehavior, UserBehavior, String>() {

                             @Override
                             public void processElement(UserBehavior left, UserBehavior right, Context ctx, Collector<String> out) throws Exception {
                                 System.out.println(left.getUserId() + ":" + left.getBehavior() + "--" + right.getUserId() + ":" + right.getBehavior());
                             }
                         }
                );

        // coGroup join 实现两个流的 join
        eventTimeStream1.coGroup(eventTimeStream2)
                .where((KeySelector<UserBehavior, Long>) UserBehavior::getUserId)
                .equalTo((KeySelector<UserBehavior, Long>) UserBehavior::getUserId)
                .window(TumblingEventTimeWindows.of(Time.seconds(1)))
                .apply(new CoGroupFunction<UserBehavior, UserBehavior, Object>() {
                    @Override
                    public void coGroup(Iterable<UserBehavior> first, Iterable<UserBehavior> second, Collector<Object> out) throws Exception {
                        // left join
                        if (first.iterator().hasNext()) {
                            out.collect("left join: " + first + " => " + second);
                        }

                        //right join
                        if (second.iterator().hasNext()) {
                            out.collect("right join: " + first + " => " + second);
                        }

                        //inner join
                        if (first.iterator().hasNext() && second.iterator().hasNext()) {
                            out.collect("join: " + first + " => " + second);
                        }

                        // full join
                        out.collect("full outer join: " + first + " => " + second);

                    }
                });

        env.execute("local stream test");
    }


    /**
     * Flink执行环境
     *
     * @return Flink流处理的执行环境
     */
    private static StreamExecutionEnvironment initExecutionEnvironment() {
        StreamExecutionEnvironment streamExecutionEnvironment = StreamExecutionEnvironment.createLocalEnvironmentWithWebUI(new Configuration());
        streamExecutionEnvironment.setParallelism(PARALLELISM);
        streamExecutionEnvironment.enableCheckpointing(CHECK_POINT_TIME);
        final CheckpointConfig checkpointConfig = streamExecutionEnvironment.getCheckpointConfig();
        checkpointConfig.setCheckpointTimeout(CHECK_POINT_TIME);
        checkpointConfig.setMinPauseBetweenCheckpoints(CHECK_POINT_TIME);
        checkpointConfig.setCheckpointingMode(CheckpointingMode.AT_LEAST_ONCE);
        checkpointConfig.setExternalizedCheckpointCleanup(CheckpointConfig.ExternalizedCheckpointCleanup.RETAIN_ON_CANCELLATION);
        return streamExecutionEnvironment;
    }


    public static class FilterMap extends RichFlatMapFunction<String, UserBehavior> implements CheckpointListener {

        @Override
        public void flatMap(String s, Collector<UserBehavior> collector) throws Exception {
            UserBehavior userBehavior = JSON.parseObject(s, UserBehavior.class);
            collector.collect(userBehavior);
        }

        @Override
        public void notifyCheckpointComplete(long checkpointId) throws Exception {
            //..  checkpoint 完成
        }

        @Override
        public void notifyCheckpointAborted(long checkpointId) throws Exception {
            CheckpointListener.super.notifyCheckpointAborted(checkpointId);
            // ... checkpoint 终止
        }
    }


}
