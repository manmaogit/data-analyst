package com.data.flink.window;

import com.data.flink.entity.UserBehavior;
import org.apache.flink.api.common.eventtime.SerializableTimestampAssigner;
import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.functions.RichFilterFunction;
import org.apache.flink.api.java.tuple.Tuple4;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.TimeCharacteristic;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.sink.RichSinkFunction;
import org.apache.flink.streaming.api.windowing.assigners.TumblingEventTimeWindows;
import org.apache.flink.streaming.api.windowing.time.Time;

import java.time.Duration;

public class MNApplication {

    public static void main(String[] args) throws Exception {

        final StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        //定义了水位线的时间属性是从消息中获取
        env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime);



        // 定义数据源
        env.addSource(new TruckSource()).name("truckSource")
                //定义数据转换函数（可选），可以将元组转换成bean对象
                .map(new SourceMapFunction()).name("sourceMapFunction").setParallelism(4)
                //定义过滤器（可选），可以将某些无用数据过滤掉
                .filter(new MyFilterFunction()).name("myFilterFunction").setParallelism(4)
                .assignTimestampsAndWatermarks(WatermarkStrategy.<TruckBean>forMonotonousTimestamps().withTimestampAssigner(new TruckTimestamp()))
                //时间窗口（可选）
                .windowAll(TumblingEventTimeWindows.of(Time.seconds(10)))
                .allowedLateness(Time.seconds(10))
                //允许的最大延迟xx（可选），超过此延迟数据可丢失
                //使用时间窗口函数做处理
                .apply(new TrigerAlertWindow()).name("trigerAlertWindow")
                //sink数据做持久化操作
                .addSink(new MySinkFunction()).name("mySinkFunction");
        env.execute("test");
    }


    public static class SourceMapFunction implements MapFunction<Tuple4<String, Double, Double, Long>, TruckBean> {


        @Override
        public TruckBean map(Tuple4<String, Double, Double, Long> element) throws Exception {
            TruckBean bean = new TruckBean();
            bean.imei = element.f0;
            bean.lat = element.f1;
            bean.lng = element.f2;
            bean.time = element.f3;

            return bean;
        }
    }

    public static class MyFilterFunction extends RichFilterFunction<TruckBean> {

        @Override
        public void open(Configuration parameters) throws Exception {

            System.out.println("begin init rich filter function" + Thread.currentThread().getId());
        }

        @Override
        public boolean filter(TruckBean value) throws Exception {
            return true;
        }
    }

    /**
     * 保存数据
     */
    public static class MySinkFunction extends RichSinkFunction<TruckBean> {

        @Override
        public void open(Configuration parameters) throws Exception {

        }

        @Override
        public void invoke(TruckBean value, Context context) throws Exception {

        }
    }

}

