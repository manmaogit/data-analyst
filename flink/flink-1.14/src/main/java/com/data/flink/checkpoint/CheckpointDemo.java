package com.data.flink.checkpoint;

import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.common.state.CheckpointListener;
import org.apache.flink.api.common.state.ListState;
import org.apache.flink.api.common.state.ListStateDescriptor;
import org.apache.flink.api.common.typeinfo.TypeHint;
import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.runtime.state.FunctionInitializationContext;
import org.apache.flink.runtime.state.FunctionSnapshotContext;
import org.apache.flink.streaming.api.checkpoint.CheckpointedFunction;
import org.apache.flink.util.Collector;

/**
 * 检查点 机制API  <br>
 * CheckpointListener checkpoint 完成时的回调
 * <p>
 * CheckpointedFunction Operator State 操作
 *
 * @author mao.man@rootcloud.com<br>
 * @version 1.0<br>
 * @date 2022/06/19 <br>
 */

public class CheckpointDemo implements FlatMapFunction<String, String>, CheckpointListener, CheckpointedFunction {

    private transient ListState<Tuple2<String, Integer>> checkpointState;

    @Override
    public void flatMap(String value, Collector<String> out) throws Exception {

    }

    @Override
    public void initializeState(FunctionInitializationContext context) throws Exception {
        ListStateDescriptor<Tuple2<String, Integer>> descriptor =
                new ListStateDescriptor<>(
                        "buffered-elements",
                        TypeInformation.of(new TypeHint<Tuple2<String, Integer>>() {}));
        checkpointState = context.getOperatorStateStore().getListState(descriptor);
    }

    @Override
    public void snapshotState(FunctionSnapshotContext context) throws Exception {
        // checkpoint 之前调用
        // checkpointState.add(...)
    }


    @Override
    public void notifyCheckpointComplete(long checkpointId) throws Exception {
        System.out.println("checkpoint  完成");
    }

    @Override
    public void notifyCheckpointAborted(long checkpointId) throws Exception {
        System.out.println("checkpoint  中断");
    }
}


