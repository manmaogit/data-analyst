package com.data.flink.sql;

import org.apache.flink.api.common.serialization.SimpleStringSchema;
import org.apache.flink.core.fs.FileSystem;
import org.apache.flink.streaming.api.TimeCharacteristic;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer;
import org.apache.flink.table.api.EnvironmentSettings;
import org.apache.flink.table.api.Schema;
import org.apache.flink.table.api.Table;
import org.apache.flink.table.api.bridge.java.StreamTableEnvironment;
import org.apache.flink.table.sinks.CsvTableSink;
import org.apache.flink.table.types.logical.LogicalType;
import org.apache.flink.table.types.logical.TimestampType;
import org.apache.flink.table.types.logical.VarCharType;

import java.util.Properties;

import static org.apache.flink.table.api.Expressions.$;

/**
 * @author lx
 */
public class StreamSql {

    private static final String KAFKA_TOPIC = "topic_test";

    private static final String KAFKA_TOPIC_TABLE_NAME = "topic_test";

    private static final String OUTPUT_FILE_PATH="test";

    public static void main(String[] args) throws Exception {

        // streaming env
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setStreamTimeCharacteristic(TimeCharacteristic.ProcessingTime);

        // 创建tableEnv
        EnvironmentSettings fsSettings = EnvironmentSettings.newInstance().useOldPlanner().inStreamingMode().build();
        StreamExecutionEnvironment fsEnv = StreamExecutionEnvironment.getExecutionEnvironment();
        StreamTableEnvironment tableEnv = StreamTableEnvironment.create(fsEnv, fsSettings);

        // kafka 流数据
        // 最新版本代码 https://nightlies.apache.org/flink/flink-docs-release-1.15/docs/connectors/datastream/kafka/
        FlinkKafkaConsumer<String> kafkaTableSource = new FlinkKafkaConsumer<>(KAFKA_TOPIC, new SimpleStringSchema(), initProps());
        DataStream<String> stream = env.addSource(kafkaTableSource).name("StreamSQL");

        // tableEnv关联到kafka $("imei"), $("truckno"), $("proctime")
        tableEnv.createTemporaryView(KAFKA_TOPIC_TABLE_NAME, stream,
                Schema.newBuilder()
                        .column("imei", new VarCharType(32).asSerializableString())
                        .column("truckno", new VarCharType(64).asSerializableString())
                        .column("proctime", new TimestampType().asSerializableString())
                        .build());

        // 使用tableEnv执行SQL
        Table sqlResult = tableEnv.sqlQuery("SELECT TUMBLE_START(proctime, INTERVAL '1' MINUTE) as procTime,imei FROM "
                        + KAFKA_TOPIC_TABLE_NAME + " GROUP BY TUMBLE(proctime, INTERVAL '1' MINUTE) ");

        env.execute();

    }

    public static Properties initProps() {
        Properties props = new Properties();

        /** 要连接的Broker */
        props.put("bootstrap.servers", "127.0.0.1:9092");
        props.put("zookeeper.connect", "127.0.0.1:2181,127.0.0.2:2181,127.0.0.3:2181");
        /**当前的分组id*/
        props.put("group.id", "flink-test");
        props.put("enable.auto.commit", "true");

        /**用于序列化 key 和 value。*/
        props.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        props.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        return props;
    }
}
