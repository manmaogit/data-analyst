package com.data.flink;

import com.alibaba.fastjson.JSON;
import com.data.flink.entity.UserBehavior;
import com.data.flink.source.LocalFileSource;
import lombok.extern.slf4j.Slf4j;
import org.apache.flink.api.common.functions.RichFlatMapFunction;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.CheckpointingMode;
import org.apache.flink.streaming.api.TimeCharacteristic;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.CheckpointConfig;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.co.CoFlatMapFunction;
import org.apache.flink.streaming.api.functions.sink.RichSinkFunction;
import org.apache.flink.streaming.api.functions.source.SourceFunction;
import org.apache.flink.util.Collector;

/**
 *
 * 简单的将两个流按照key hash 合并到一起
 *
 * @author manmao
 */
@Slf4j
public class FlinkConnectedStream {

    private static final int PARALLELISM = 4;

    private static final Long CHECK_POINT_TIME = 5 * 60 * 1000L;


    public static void main(String[] args) throws Exception {
        SourceFunction<String> source1 = new LocalFileSource("/Users/lx/Desktop/user_behavior.log");
        SourceFunction<String> source2 = new LocalFileSource("/Users/lx/Desktop/user_behavior.log");
        initContainer(source1, source2);
    }


    private static void initContainer(SourceFunction<String> source1, SourceFunction<String> source2) throws Exception {
        StreamExecutionEnvironment env = initExecutionEnvironment();

        env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime);


        DataStream<UserBehavior> objectStream1 = env.addSource(source1).name("source1").uid("source1").flatMap(new FilterMap());

        DataStream<UserBehavior> objectStream2 = env.addSource(source2).name("source2").uid("source2").flatMap(new FilterMap());

        // connect 操作,连接两个不同的流
        DataStream<String> result = objectStream1.connect(objectStream2).keyBy("userId", "userId").flatMap(new ConnectCoMap());

        result.addSink(new SimpleSinkFunction()).name("sink").uid("sink");

        env.execute("local stream test");
    }


    /**
     * Flink执行环境
     *
     * @return Flink流处理的执行环境
     */
    private static StreamExecutionEnvironment initExecutionEnvironment() {
        StreamExecutionEnvironment streamExecutionEnvironment = StreamExecutionEnvironment.getExecutionEnvironment();
        streamExecutionEnvironment.setParallelism(PARALLELISM);
        streamExecutionEnvironment.enableCheckpointing(CHECK_POINT_TIME);
        final CheckpointConfig checkpointConfig = streamExecutionEnvironment.getCheckpointConfig();
        checkpointConfig.setCheckpointTimeout(CHECK_POINT_TIME);
        checkpointConfig.setMinPauseBetweenCheckpoints(CHECK_POINT_TIME);
        checkpointConfig.setCheckpointingMode(CheckpointingMode.AT_LEAST_ONCE);
        checkpointConfig.setExternalizedCheckpointCleanup(CheckpointConfig.ExternalizedCheckpointCleanup.RETAIN_ON_CANCELLATION);
        return streamExecutionEnvironment;
    }


    public static class FilterMap extends RichFlatMapFunction<String, UserBehavior> {

        @Override
        public void flatMap(String s, Collector<UserBehavior> collector) throws Exception {
            UserBehavior userBehavior = JSON.parseObject(s, UserBehavior.class);
            collector.collect(userBehavior);
        }
    }


    public static class ConnectCoMap implements CoFlatMapFunction<UserBehavior, UserBehavior, String> {

        @Override
        public void flatMap1(UserBehavior userBehavior, Collector<String> collector) throws Exception {
            System.out.println("1:" + userBehavior.getUserId());
        }

        @Override
        public void flatMap2(UserBehavior userBehavior, Collector<String> collector) throws Exception {
            System.out.println("2:" + userBehavior.getUserId());
        }
    }


    public static class SimpleSinkFunction extends RichSinkFunction<String> {

        @Override
        public void open(Configuration parameters) throws Exception {
            super.open(parameters);
        }

        @Override
        public void invoke(String value, Context context) throws Exception {
            log.info(value);
        }
    }


}
