package com.data.flink.window;


import org.apache.flink.api.common.eventtime.SerializableTimestampAssigner;

/**
 * 提取消息中的event 时间字段作为水位线字段
 *
 * @author lx
 */
public class TruckTimestamp implements SerializableTimestampAssigner<TruckBean> {

    private static final long serialVersionUID = 1L;

    @Override
    public long extractTimestamp(TruckBean element, long previousElementTimestamp) {
        return element.time;
    }

}