package com.data.flink.source;

import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.functions.source.RichSourceFunction;

import java.io.BufferedReader;
import java.io.FileReader;


/**
 * 自定义本地文件source
 *
 * @author manmao
 */
public class LocalFileSource extends RichSourceFunction<String> {

    private String filePath;

    private int sendInterval;

    public LocalFileSource(String filePath) {
        this.filePath = filePath;
        this.sendInterval = 1000;
    }

    public LocalFileSource(String filePath, int sendInterval) {
        this.filePath = filePath;
        this.sendInterval = sendInterval;
    }


    @Override
    public void open(Configuration parameters) throws Exception {
        super.open(parameters);
    }

    @Override
    public void run(SourceContext<String> ctx) {
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(filePath))) {
            String dataStream;
            dataStream = bufferedReader.readLine();
            while (true) {
                if (dataStream == null) {
                    Thread.sleep(10 * 1000);
                    continue;
                }
                if (Thread.interrupted()) {
                    break;
                }
                // 通过控制数据的发送频率来模拟实际的数据发送信息
                Thread.sleep(sendInterval);
                ctx.collect(dataStream);
                dataStream = bufferedReader.readLine();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void cancel() {

    }
}
