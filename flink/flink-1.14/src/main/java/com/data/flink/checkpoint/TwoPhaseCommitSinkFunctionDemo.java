package com.data.flink.checkpoint;

import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.streaming.api.functions.sink.TwoPhaseCommitSinkFunction;

/**
 * 两阶段提交 TwoPhaseCommitSinkFunction 实现了 CheckpointedFunction 和 CheckpointListener 接口   <br>
 * CheckpointListener checkpoint 完成时的回调
 * <p>
 * CheckpointedFunction Operator State 操作
 *
 * @author mao.man@rootcloud.com<br>
 * @version 1.0<br>
 * @date 2022/06/19 <br>
 */

public class TwoPhaseCommitSinkFunctionDemo extends TwoPhaseCommitSinkFunction<String, String, String> {

    public TwoPhaseCommitSinkFunctionDemo() {
        super(TypeInformation.of(String.class).createSerializer(null),
                TypeInformation.of(String.class).createSerializer(null));
    }

    @Override
    protected void invoke(String transaction, String value, Context context) throws Exception {

    }

    @Override
    protected String beginTransaction() throws Exception {
        return null;
    }

    @Override
    protected void preCommit(String transaction) throws Exception {
        //  snapshotState() ... 触发
    }

    @Override
    protected void commit(String transaction) {

    }

    @Override
    protected void abort(String transaction) {

    }
}


