package com.data.flink.topn;

import com.alibaba.fastjson.JSON;

import java.util.Random;

public class OrderDetailGenerate {

    public static void main(String[] args) {
        for (int i = 0; i < 10000; i++) {
            Random random = new Random();
            OrderDetail orderDetail = new OrderDetail();
            orderDetail.setUserId((long)random.nextInt(20));
            orderDetail.setPrice(random.nextDouble() * 100 + 10);
            orderDetail.setCiteName("成都");
            orderDetail.setItemId((long)random.nextInt(100000));
            orderDetail.setTimeStamp(System.currentTimeMillis());
            System.out.println(JSON.toJSONString(orderDetail));
        }
    }
}
