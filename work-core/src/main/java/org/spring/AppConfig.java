package org.spring;


import org.springframework.context.annotation.*;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @author K
 */
@Configuration
@EnableTransactionManagement
@ComponentScan(basePackages = "com.xxx.xxx.xx")
@PropertySource(ignoreResourceNotFound = true, value = {"classpath:jaguar-storage.properties"})
@ImportResource(locations = {"classpath:mybatis.xml", "classpath:dubbo-bean.xml"})
public class AppConfig {

    @Bean
    public void test(){

    }
}
