package org.spring;

import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.Resource;

import java.util.Map;
import java.util.Properties;

/**
 * 直接通过配置文件生成 Spring容器
 */
public class SpringClassPathXmlUtil {

    private static class SpringUtilHolder {
        private static ClassPathXmlApplicationContext ac = init();
    }

    public static final ClassPathXmlApplicationContext getContext() {
        return SpringUtilHolder.ac;
    }

    private static ClassPathXmlApplicationContext init() {
        Properties p = new Properties();
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        context.start();
        try {
            Resource[] re = context.getResources("classpath*:event-platform-*.properties");
            for (Resource r : re) {
                p.load(r.getInputStream());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return context;
    }

    private SpringClassPathXmlUtil() {}

    public static <T> T getBean(Class<T> c) {
        return getContext().getBean(c);
    }

    public static <T> T getBean(String name, Class<T> c){
        return getContext().getBean(name, c);
    }

    public static <T> Map<String, T> getBeansOfType(Class<T> c) {
        return getContext().getBeansOfType(c);
    }
}
