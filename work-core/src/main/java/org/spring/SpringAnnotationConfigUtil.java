package org.spring;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * 注解的方式初始化sping 容器
 *
 * @author manmao
 */
public class SpringAnnotationConfigUtil {

    private static volatile AnnotationConfigApplicationContext applicationContext = null;

    private SpringAnnotationConfigUtil() {
    }

    @SuppressWarnings("UnusedReturnValue")
    private static ApplicationContext getContext() {
        if (null == applicationContext) {
            synchronized (SpringAnnotationConfigUtil.class) {
                if (null == applicationContext) {
                    applicationContext = new AnnotationConfigApplicationContext(AppConfig.class);
                    applicationContext.start();
                }
            }
        }
        return applicationContext;
    }

    private static void init() {
        getContext();
    }

    public static <T> T getBean(Class<T> tClass) {
        init();
        return applicationContext.getBean(tClass);
    }

    public static <T> T getBean(String name, Class<T> tClass) {
        init();
        return applicationContext.getBean(name, tClass);
    }
}
