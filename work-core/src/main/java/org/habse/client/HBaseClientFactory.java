package org.habse.client;
import java.io.IOException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;


public class HBaseClientFactory {

    private  static Admin admin=null;
    private  static Connection connection=null;
    private  static Configuration config=null;

    static{
        config = HBaseConfiguration.create();
        config.addResource("hbase-site.xml");
        config.addResource("core-site.xml");
    }

    public static Admin getHTableAdminInstance() throws IOException {
        if (admin == null) {
            initConnection();
        }
        return admin;
    }

    /**
     * 生成htable
     * @param tableName
     * @return
     * @throws IOException
     */
    public static Table getTable(String tableName) throws IOException{
        if(connection==null){
           initConnection();
        }
        TableName tableNameO = TableName.valueOf(tableName);
        Table table=connection.getTable(tableNameO);
        return table;
    }



    

    /**
     * 初始化 和 hbase服务器的连接
     * @throws IOException
     */
    private static void initConnection()
        throws IOException {
        connection = ConnectionFactory.createConnection(config);
        admin = connection.getAdmin();
        
    }
}