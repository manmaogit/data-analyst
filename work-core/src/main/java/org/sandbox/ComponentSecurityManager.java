package org.sandbox;

import lombok.extern.slf4j.Slf4j;
import sun.security.util.SecurityConstants;

import java.io.FilePermission;
import java.net.SocketPermission;
import java.security.Permission;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * 组件运行沙箱权限校验 <br>
 *
 * @author mao.man<br>
 * @version 1.0<br>
 * @date 2021/11/05 <br>
 */
@Slf4j
public class ComponentSecurityManager extends SecurityManager {

    /**
     * 不允许访问的文件目录
     */
    private static final Set<String> SECURITY_DIRS = new HashSet<>();

    static {
        SECURITY_DIRS.addAll(Arrays.asList("/etc", "/sbin", "/proc", "/sys", "/bin", "/run"));
    }

    /**
     * 不允许写入数据的目录
     */
    private static final Set<String> WRITE_FORBID_DIRS = new HashSet<>();

    static {
        WRITE_FORBID_DIRS.addAll(Arrays.asList("/data/app/runtime-server/"));
    }

    private static final String EXIT_VM = "exitVM";

    private static final String GET_FILE_SYSTEM_ATTRIBUTES = "getFileSystemAttributes";

    private static final String SET_SECURITY_MANAGER = "setSecurityManager";


    @Override
    public void checkPermission(Permission perm) {
        checkPermissionCustom(perm);
    }

    @Override
    public void checkPermission(Permission perm, Object context) {
        checkPermissionCustom(perm);
    }

    private void checkPermissionCustom(Permission perm) {

        // runtime环境不校验权限
        if (ComponentRunSandboxManager.RUNTIME_ENV.get()) {
            return;
        }

        if (perm instanceof FilePermission) {
            // 不允许执行cmd命令
            if (perm.getActions().equals(SecurityConstants.FILE_EXECUTE_ACTION)) {
                log.warn("execute cmd SecurityException,action:{}", perm);
                throw new SecurityException("ComponentSecurityManager Permission not allowed execute cmd!!!");
            }
            // 不允许删除文件
            if (perm.getActions().equals(SecurityConstants.FILE_DELETE_ACTION)) {
                log.warn("delete file SecurityException,action:{}", perm);
                throw new SecurityException("ComponentSecurityManager Permission not allowed delete file!!!");
            }

            // 不允许写入数据到/data/app/ipaas-runtime-server/*
            if (perm.getActions().equals(SecurityConstants.FILE_WRITE_ACTION)) {
                for (String path : WRITE_FORBID_DIRS) {
                    if(perm.getName().startsWith(path)){
                        log.warn("write file /data/app/ipaas-runtime-server/** SecurityException,action:{}", perm);
                        throw new SecurityException("ComponentSecurityManager Permission not allowed write data to /data/app/ipaas-runtime-server/**  !!!");
                    }
                }
            }

            // 禁止访问系统文件目录
            for (String path : SECURITY_DIRS) {
                if (perm.getName().startsWith(path)) {
                    log.warn("ComponentSecurityManager Permission not allowed access security file,action:{}", perm);
                    throw new SecurityException("ComponentSecurityManager Permission not allowed access security file!!!");
                }
            }
        }

        // 不允许建立socket server
        if (perm instanceof SocketPermission) {
            if (perm.getActions().equals(SecurityConstants.SOCKET_LISTEN_ACTION) ||
                    perm.getActions().equals(SecurityConstants.SOCKET_ACCEPT_ACTION)) {
                log.warn("socket listen action SecurityException,action:{}", perm);
                throw new SecurityException("ComponentSecurityManager Permission not allowed create socket listen !!!");
            }
        }

        if (perm instanceof RuntimePermission) {
            // 不允许 System.exit() 方法
            if (perm.getName().startsWith(EXIT_VM)) {
                log.warn("ComponentSecurityManager Permission not allowed System.exit(x),action:{}", perm);
                throw new SecurityException("ComponentSecurityManager Permission not allowed System.exit(x) !!!");
            }
            // 不允许 getFileSystemAttributes 获取文件系统信息
            if (GET_FILE_SYSTEM_ATTRIBUTES.equals(perm.getName())) {
                log.warn("ComponentSecurityManager Permission not allowed getFileSystemAttributes,action:{}", perm);
                throw new SecurityException("ComponentSecurityManager Permission not allowed getFileSystemAttributes !!!");
            }
            // 不在Runtime运行时环境,不支持setSecurityManager操作
            if (SET_SECURITY_MANAGER.equals(perm.getName())) {
                log.warn("ComponentSecurityManager Permission not allowed setSecurityManager ,action:{}", perm);
                throw new SecurityException("ComponentSecurityManager Permission not allowed setSecurityManager !!!");
            }
        }
    }
}
