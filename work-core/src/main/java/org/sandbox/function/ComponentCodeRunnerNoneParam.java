package org.sandbox.function;

/**
 * 运行组件代码方法 函数式表达式
 *
 * @author mao.man<br>
 * @version 1.0<br>
 * @date 2021/07/16 <br>
 */
@FunctionalInterface
public interface ComponentCodeRunnerNoneParam {

    /**
     * 运行代码
     */
    void run();
}