package org.sandbox.groovy;

import groovy.lang.Binding;
import groovy.lang.GroovyShell;
import groovy.lang.Script;
import groovy.transform.ThreadInterrupt;
import groovy.transform.TimedInterrupt;
import org.codehaus.groovy.control.CompilerConfiguration;
import org.codehaus.groovy.control.customizers.ASTTransformationCustomizer;
import org.codehaus.groovy.control.customizers.ImportCustomizer;
import org.codehaus.groovy.runtime.InvokerHelper;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author mao.man@rootcloud.com<br>
 * @version 1.0<br>
 * @date 2022/07/17 <br>
 */
public class GroovyMain {

    public static void main(String[] args) {
        // 自定义配置
        CompilerConfiguration compilerConfiguration = new CompilerConfiguration();

        // 配置 安全管理器 SecureASTCustomizer
        compilerConfiguration.addCompilationCustomizers(GroovySandboxExpressionChecker.getSecureASTCustomizer());

        // 添加默认引入的包名，脚本中不用import
        compilerConfiguration.addCompilationCustomizers(new ImportCustomizer()
                // 引用包
                .addStarImports("org.apache.commons.lang3","org.spring")
                // 引用class
                .addImports("org.apache.commons.lang3.StringUtils"));

        //addThreadInterrupt(compilerConfiguration);

        // 编译运行
        GroovyShell groovyShell = new GroovyShell(new Binding(), compilerConfiguration);
        Script script = groovyShell.parse("def process(msg){return StringUtils.isNotBlank('123')}");
        System.out.println(InvokerHelper.invokeMethod(script, "process", null));
    }

    /**
     * 添加拦截器和超时时间，针对while,foreach,模块在循环 执行前后增加时间统计，自定义方法调用前后统计，超时则抛出异常。
     *
     * @param compilerConfiguration 编译配置
     */
    public static void addThreadInterrupt(CompilerConfiguration compilerConfiguration) {
        // 添加线程中断拦截器，可拦截循环体（for,while）、方法和闭包的首指令
        compilerConfiguration.addCompilationCustomizers(new ASTTransformationCustomizer(ThreadInterrupt.class));
        // 添加线程中断拦截器，可中断超时线程，当前定义超时时间为3s
        Map<String, Object> timeoutArgs = new HashMap<>(15);
        timeoutArgs.put("value", 3);
        // 3秒钟
        compilerConfiguration.addCompilationCustomizers(new ASTTransformationCustomizer(timeoutArgs, TimedInterrupt.class));
    }
}
