package org.sandbox.groovy;

import org.codehaus.groovy.ast.ClassNode;
import org.codehaus.groovy.ast.expr.ConstructorCallExpression;
import org.codehaus.groovy.ast.expr.Expression;
import org.codehaus.groovy.ast.expr.GStringExpression;
import org.codehaus.groovy.ast.expr.MethodCallExpression;
import org.codehaus.groovy.control.customizers.SecureASTCustomizer;
import sun.misc.Unsafe;

import java.io.File;
import java.util.*;

/**
 * https://github.com/elastic/elasticsearch/blob/4dc060527cd7d35817085a3926e65d071e3b1321/src/main/java/org/elasticsearch/script/groovy/GroovySandboxExpressionChecker.java
 * Groovy运行沙盒限制
 *
 * @author mao.man<br>
 * @version 1.0<br>
 * @date 2021/11/02 <br>
 */
public class GroovySandboxExpressionChecker implements SecureASTCustomizer.ExpressionChecker {

    private final Set<String> methodBlacklist;
    private final Set<String> packageBlacklist;
    private final Set<String> classBlacklist;

    public GroovySandboxExpressionChecker() {
        this.methodBlacklist = defaultMethodBlacklist;
        this.packageBlacklist = defaultPackageBlacklist;
        this.classBlacklist = defaultClassConstructionBlacklist;
    }

    /**
     * 不允许执行的方法
     */
    public static Set<String> defaultMethodBlacklist = new HashSet<>();

    static {
        defaultMethodBlacklist.addAll(Arrays.asList(
                "getClass",
                "wait",
                "notify",
                "notifyAll",
                "finalize",
                "execute",
                "exec",
                "cmd"));
    }

    /**
     * 不允许import的package
     */
    @Deprecated
    public static Set<String> defaultPackageBlacklist = new HashSet<>();

    /**
     * 不允许实例化的class
     */
    public static Set<String> defaultClassConstructionBlacklist = new HashSet<>();

    static {
        defaultClassConstructionBlacklist.addAll(Arrays.asList(
                Runtime.class.getName(),
                System.class.getName(),
                File.class.getName(),
                ProcessBuilder.class.getName()
        ));
    }

    /**
     * 不允许使用的Class
     */
    private final static Set<String> DEFAULT_RECEIVER_BLACKLIST = new HashSet<>();

    static {
        DEFAULT_RECEIVER_BLACKLIST.addAll(Arrays.asList(
                Runtime.class.getName(),
                System.class.getName(),
                Unsafe.class.getName(),
                File.class.getName()));
    }

    /**
     * Checks whether the expression to be compiled is allowed
     */
    @Override
    public boolean isAuthorized(Expression expression) {
        if (expression instanceof MethodCallExpression) {
            /*
             * 方法调用黑名单校验
             */
            MethodCallExpression mce = (MethodCallExpression) expression;
            String methodName = mce.getMethodAsString();
            if (methodBlacklist.contains(methodName)) {
                return false;
            } else if (methodName == null && mce.getMethod() instanceof GStringExpression) {
                // We do not allow GStrings for method invocation, they are a security risk
                return false;
            }
        } else if (expression instanceof ConstructorCallExpression) {
            /*
             * 对象构造函数 黑名单校验
             */
            ConstructorCallExpression cce = (ConstructorCallExpression) expression;
            ClassNode type = cce.getType();
            if (packageBlacklist.contains(type.getPackageName())) {
                return false;
            }
            return !classBlacklist.contains(type.getName());
        }
        return true;
    }

    /**
     * 返回一个自定义SecureASTCustomizer,设置了表达式检查
     *
     * @return 自定义SecureASTCustomizer
     */
    public static SecureASTCustomizer getSecureASTCustomizer() {
        SecureASTCustomizer scz = new SecureASTCustomizer();
        // 允许闭包
        scz.setClosuresAllowed(true);
        // 允许定义方法
        scz.setMethodDefinitionAllowed(true);
        // Package definitions are not allowed
        scz.setPackageAllowed(true);

        // 不允许 import package 黑名单
        List<String> importBlacklist = new ArrayList<>(GroovySandboxExpressionChecker.defaultClassConstructionBlacklist);
        scz.setImportsBlacklist(importBlacklist);

        // 不允许执行方法的class 黑名单
        scz.setReceiversBlackList(new ArrayList<>(GroovySandboxExpressionChecker.DEFAULT_RECEIVER_BLACKLIST));

        // Add the customized expression checker for finer-grained checking
        scz.addExpressionCheckers(new GroovySandboxExpressionChecker());
        return scz;
    }
}