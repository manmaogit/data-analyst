package org.sandbox;


import org.sandbox.function.ComponentCodeRunner;
import org.sandbox.function.ComponentCodeRunnerNoneParam;

/**
 * 组件代码运行安全沙箱 <br>
 *
 * @author mao.man<br>
 * @version 1.0<br>
 * @date 2021/11/05 <br>
 */
public class ComponentRunSandboxManager {

    /**
     * 标记是否当前代码执行在运行时环境, true：不用走沙箱权限校验 false：走沙箱权限校验
     */
    public final static InheritableThreadLocal<Boolean> RUNTIME_ENV = new InheritableThreadLocal<Boolean>() {
        @Override
        protected Boolean initialValue() {
            return true;
        }
    };

    /**
     * 自定义沙箱权限
     */
    private static final SecurityManager SECURITY_MANAGER = new ComponentSecurityManager();

    /**
     * 在Java沙箱中运行
     * 切换线程ContextClassLoader，然后运行代码，运行完后切换会原始类加载器
     *
     * @param threadContextClassLoader runner使用的线程上文类加载器
     * @param runner                   运行代码函数式表达
     * @param input                    输入数据
     * @param open                     是否打开沙箱运行
     * @return 执行结果
     */
    public static <T> T runInSandbox(ClassLoader threadContextClassLoader, ComponentCodeRunner<T> runner, T input, boolean open) {
        try {
            // 设置安全管理器
            beforeEnterComponent(open);
            // 进入组件代码运行阶段
            RUNTIME_ENV.set(false);
            return runner.run(input);
        } finally {
            // 进入runtime代码运行阶段
            RUNTIME_ENV.set(true);
        }
    }

    /**
     * 在Java沙箱中运行
     * 切换线程ContextClassLoader，然后运行代码，运行完后切换会原始类加载器
     *
     * @param threadContextClassLoader runner使用的线程上文类加载器
     * @param runner                   运行代码函数式表达
     * @param open                     是否打开沙箱运行
     */
    public static void runInSandbox(ClassLoader threadContextClassLoader, ComponentCodeRunnerNoneParam runner, boolean open) {
        try {
            // 设置安全管理器
            beforeEnterComponent(open);
            // 进入组件代码运行阶段
            RUNTIME_ENV.set(false);
            runner.run();
        } finally {
            // 进入runtime代码运行阶段
            RUNTIME_ENV.set(true);
        }
    }

    /**
     * 在进入组件代码之前判断是否开启沙箱或者关闭沙箱
     *
     * @param open 是否打开沙箱
     */
    private static void beforeEnterComponent(boolean open) {
        if (open) {
            // 设置安全管理器
            RUNTIME_ENV.set(true);
            if (System.getSecurityManager() != SECURITY_MANAGER) {
                System.setSecurityManager(SECURITY_MANAGER);
            }
        } else {
            // 关闭安全管理器
            RUNTIME_ENV.set(true);
            SecurityManager sm = System.getSecurityManager();
            if (sm != null) {
                System.setSecurityManager(null);
            }
        }
    }

}