package org.sandbox;

import groovy.lang.GroovyShell;
import groovy.lang.Script;
import org.codehaus.groovy.control.CompilerConfiguration;
import org.codehaus.groovy.runtime.InvokerHelper;
import org.sandbox.groovy.GroovySandboxExpressionChecker;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

/**
 * Groovy Shell <br>
 *
 * @author mao.man<br>
 * @version 1.0<br>
 * @date 2021/11/02 <br>
 */
public class SandboxTest {

    public void testJavaStandBox() throws IOException {

        SecurityManager sm = new ComponentSecurityManager();
        System.setSecurityManager(sm);

        System.out.println(Runtime.getRuntime().exec("whoami"));

        sm = System.getSecurityManager();
        if (sm != null) {
            System.setSecurityManager(null);
        }
    }

    private void execmd(){
        final GroovyShell sh = new GroovyShell();
        Script script  = sh.parse("def process(msg){def com = ['ls'];return com.execute().text}");
        System.out.println(InvokerHelper.invokeMethod(script,"process", null));

    }

    private void read() throws IOException {
        File file = new File("/etc/hosts");
        FileInputStream fileInputStream = new FileInputStream(file);
        fileInputStream.read();
    }


}
