package org.consistenthash;

import java.util.Collection;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 * 一致性hash算法实现
 *
 * @param <T> hash item类型
 * @author lx
 */
public class ConsistentHash<T> {

    private final ConsistentHash.HashAlgorithm hashAlgorithm;

    private final int numberOfReplicas;

    private final TreeMap<Integer, T> circle = new TreeMap<>();

    public ConsistentHash(int numberOfReplicas, Collection<T> nodes) {
        this.numberOfReplicas = numberOfReplicas;
        this.hashAlgorithm = new ConsistentHash.HashAlgorithm() {
            @Override
            public Integer hash(Object key) {
                String data = key.toString();
                int p = 16777619;
                int hash = -2128831035;

                for (int i = 0; i < data.length(); ++i) {
                    hash = (hash ^ data.charAt(i)) * 16777619;
                }

                hash += hash << 13;
                hash ^= hash >> 7;
                hash += hash << 3;
                hash ^= hash >> 17;
                hash += hash << 5;
                return hash;
            }
        };

        for (T node : nodes) {
            this.add(node);
        }

    }

    public ConsistentHash(ConsistentHash.HashAlgorithm hashAlgorithm, int numberOfReplicas, Collection<T> nodes) {
        this.numberOfReplicas = numberOfReplicas;
        this.hashAlgorithm = hashAlgorithm;

        for (T node : nodes) {
            this.add(node);
        }

    }

    public void add(T node) {
        for (int i = 0; i < this.numberOfReplicas; ++i) {
            this.circle.put(this.hashAlgorithm.hash(node.toString() + i), node);
        }

    }

    public void remove(T node) {
        for (int i = 0; i < this.numberOfReplicas; ++i) {
            this.circle.remove(this.hashAlgorithm.hash(node.toString() + i));
        }

    }

    public T get(Object key) {
        if (this.circle.isEmpty()) {
            return null;
        } else {
            int hash = this.hashAlgorithm.hash(key);
            if (!this.circle.containsKey(hash)) {
                SortedMap<Integer, T> tailMap = this.circle.tailMap(hash);
                hash = tailMap.isEmpty() ? (Integer) this.circle.firstKey() : (Integer) tailMap.firstKey();
            }
            //return circle.ceilingEntry(hash);
            return this.circle.get(hash);
        }
    }

    public interface HashAlgorithm {
        Integer hash(Object var1);
    }
}