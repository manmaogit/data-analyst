package org;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.Date;

/**
 * 时间类型转换工具类
 *
 * @author mao.man@rootcloud.com<br>
 * @version 1.0<br>
 * @date 2021/06/09 <br>
 */
public class LocalDateTimeUtils {

    public static final DateTimeFormatter TIME_FORMATTER = DateTimeFormatter.ofPattern("HH:mm:ss");

    public static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    public static final DateTimeFormatter DATETIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    public static final String DATETIME_FORMATTER_ALL = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";

    /**
     * 获取当前系统时间
     *
     * @return 当前系统时间
     */
    public static LocalTime getLocalTime() {
        return LocalTime.now();
    }

    /**
     * 获取当前系统日期
     *
     * @return 当前系统日期
     */
    public static LocalDate getLocalDate() {
        return LocalDate.now();
    }

    /**
     * 获取当前系统日期时间
     *
     * @return 当前系统日期时间
     */
    public static LocalDateTime getLocalDateTime() {
        return LocalDateTime.now();
    }

    /**
     * 获取当前系统时间字符串
     *
     * @return 当前系统时间字符串
     */
    public static String getLocalTimeString() {
        return LocalTime.now().format(TIME_FORMATTER);
    }

    /**
     * 获取当前系统日期字符串
     *
     * @return 当前系统日期字符串
     */
    public static String getLocalDateString() {
        return LocalDate.now().format(DATE_FORMATTER);
    }

    /**
     * 获取当前系统日期时间字符串
     *
     * @return 当前系统日期时间字符串
     */
    public static String getLocalDateTimeString() {
        return LocalDateTime.now().format(DATETIME_FORMATTER);
    }

    /**
     * 将 LocalDateTime 转换为时间字符串
     * @param dateTime
     * @return 该日期的时间字符串
     */
    public static String getLocalDateTimeToString(LocalDateTime dateTime) {
        return dateTime.format(DATETIME_FORMATTER);
    }

    /**
     * 字符串转LocalTime
     *
     * @param time 时间字符串 ，,默认 HH:mm:ss
     * @return 本地时间
     */
    public static LocalTime string2LocalTime(String time) {
        return LocalTime.parse(time, TIME_FORMATTER);
    }

    /**
     * 字符串转LocalDate
     *
     * @param date 日期字符串,默认 yyyy-MM-dd
     * @return 本地日期
     */
    public static LocalDate string2LocalDate(String date) {
        return LocalDate.parse(date, DATE_FORMATTER);
    }

    /**
     * 字符串转LocalDateTime
     *
     * @param dateTime 时间字符串
     * @return 本地时间对象，默认 yyyy-MM-dd HH:mm:ss
     */
    public static LocalDateTime string2LocalDateTime(String dateTime) {
        return LocalDateTime.parse(dateTime, DATETIME_FORMATTER);
    }

    /**
     * 字符串转LocalDateTime
     *
     * @param dateTime 时间字符转
     * @param pattern  自定义pattern
     * @return 返回本地时间
     */
    public static LocalDateTime string2LocalDateTime(String dateTime, String pattern) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
        return LocalDateTime.parse(dateTime, formatter);
    }

    /**
     * 将时间从一个时区转换为另一个时区
     * zoneId 解释
     * http://zzx-blog.com/java-8zhi-xin-ri-qi-shi-jian-apibi-ji-si-zoneid/
     *
     * @param from     时间
     * @param fromZone 时间的当前时区
     * @param toZone   目标时区
     * @return localtime
     */
    public static LocalDateTime convertTime(LocalDateTime from, ZoneId fromZone, ZoneId toZone) {
        if (fromZone == null) {
            fromZone = ZoneId.from(ZoneOffset.UTC);
        }
        if (toZone == null) {
            toZone = ZoneId.systemDefault();
        }
        final ZonedDateTime zonedTime = from.atZone(fromZone);
        final ZonedDateTime converted = zonedTime.withZoneSameInstant(toZone);
        return converted.toLocalDateTime();
    }



    /**
     * 字符串转LocalDateTime
     *
     * @param dateTime 时间字符转
     * @param pattern  自定义pattern
     * @return 返回本地时间
     */
    public static LocalDateTime string2LocalDateTime(String dateTime, String pattern, ZoneId zoneId) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern).withZone(zoneId);
        return LocalDateTime.parse(dateTime, formatter).atZone(ZoneId.systemDefault()).toLocalDateTime();
    }

    /**
     * Date转LocalDateTime
     *
     * @param date java date
     * @return 本地时间
     */
    public static LocalDateTime date2LocalDateTime(Date date) {
        //An instantaneous point on the time-line.(时间线上的一个瞬时点。)
        Instant instant = date.toInstant();
        //A time-zone ID, such as {@code Europe/Paris}.(时区)
        ZoneId zoneId = ZoneId.systemDefault();
        return instant.atZone(zoneId).toLocalDateTime();
    }

    /**
     * Date转LocalDate
     *
     * @param date 时间
     * @return 本地时间
     */
    public static LocalDate date2LocalDate(Date date) {
        //An instantaneous point on the time-line.(时间线上的一个瞬时点。)
        Instant instant = date.toInstant();
        //A time-zone ID, such as {@code Europe/Paris}.(时区)
        ZoneId zoneId = ZoneId.systemDefault();
        return instant.atZone(zoneId).toLocalDate();
    }

    /**
     * Date转LocalDate
     *
     * @param date 时间
     * @return 本地时间
     */
    public static LocalTime date2LocalTime(Date date) {
        //An instantaneous point on the time-line.(时间线上的一个瞬时点。)
        Instant instant = date.toInstant();
        //A time-zone ID, such as {@code Europe/Paris}.(时区)
        ZoneId zoneId = ZoneId.systemDefault();
        return instant.atZone(zoneId).toLocalTime();
    }

    /**
     * LocalDateTime转换为Date
     *
     * @param localDateTime 本地时间
     * @return Date数据
     */
    public static Date localDateTime2Date(LocalDateTime localDateTime) {
        ZoneId zoneId = ZoneId.systemDefault();
        //Combines this date-time with a time-zone to create a  ZonedDateTime.
        ZonedDateTime zdt = localDateTime.atZone(zoneId);
        return Date.from(zdt.toInstant());
    }

    public static void main(String[] args) {
        LocalDateTime time = LocalDateTimeUtils.string2LocalDateTime("2022-05-23T02:32:52.795Z", DATETIME_FORMATTER_ALL);
        final ZonedDateTime zonedTime = time.atZone(ZoneId.from(ZoneOffset.UTC));
        final ZonedDateTime converted = zonedTime.withZoneSameInstant(ZoneId.systemDefault());
        System.out.println(converted.toLocalDateTime());
    }
}
