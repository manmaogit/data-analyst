package org;

import cn.hutool.core.lang.Snowflake;
import cn.hutool.core.net.NetUtil;

/**
 * 雪花算法生成唯一id
 *
 * @author manmao
 * @since 2020-10-27
 */
public class SnowflakeSimple {

    private static Snowflake snowflake = null;

    public SnowflakeSimple() {
    }

    public static long nextId() {
        return snowflake.nextId();
    }

    public static String nextStrId() {
        return String.valueOf(nextId());
    }

    public static void main(String[] args) {
        for (int i = 0; i < 100; ++i) {
            System.out.println(nextStrId());
        }

    }

    static {
        String strIP = NetUtil.getLocalhostStr();
        long ip = NetUtil.ipv4ToLong(strIP);
        snowflake = new Snowflake(ip % 32L, ip / 32L % 32L);
    }
}
