package org.yarn.process.monitor;

/**
 * @author manmao
 */
public final class SystemClock implements Clock {

  private static final SystemClock INSTANCE = new SystemClock();

  public static SystemClock getInstance() {
    return INSTANCE;
  }

  public SystemClock() {
    // do nothing
  }

  @Override
  public long getTime() {
    return System.currentTimeMillis();
  }
}
