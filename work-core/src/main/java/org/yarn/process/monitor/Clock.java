package org.yarn.process.monitor;

public interface Clock {

  long getTime();
}