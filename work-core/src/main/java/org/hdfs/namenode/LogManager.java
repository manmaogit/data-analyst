package org.hdfs.namenode;

public class LogManager {

    private DoubleBuff doubleBuff = new DoubleBuff();
    private long txid = 0;

    private ThreadLocal<Long> txidThreadLoad = new ThreadLocal<>();


    public void writeEditLog(String logContent) {
        synchronized (this) {
            EditLog editLog = new EditLog(txid, logContent);
            txid++;
            txidThreadLoad.set(txid);

            doubleBuff.write(editLog);
        }
        logFlush();
    }


    private boolean isSyncRunning = false;
    private long maxTxid = 0;
    private boolean isWait = false;

    private void logFlush() {

        synchronized (this) {
            // 如果有线程正在刷盘
            if (isSyncRunning) {

                if (txidThreadLoad.get() < maxTxid) {
                    return;
                }

                if (isWait) {
                    return;
                }

                isWait = true;
                while (isSyncRunning) {
                    try {
                        this.wait(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                isWait = false;

            }
            doubleBuff.exChangeMemory();

            if (doubleBuff.buffReady.size() > 0) {
                maxTxid = doubleBuff.getMaxTxid();
            }
            isSyncRunning = true;
        }
        // flush
        doubleBuff.flush();
        synchronized (this) {
            isSyncRunning = false;
            this.notify();
        }
    }
}