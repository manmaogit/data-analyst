package org.hdfs.namenode;

import java.util.LinkedList;


public class DoubleBuff {

    public LinkedList<EditLog> currentMemory = new LinkedList<>();

    public LinkedList<EditLog> buffReady = new LinkedList<>();

    public void write(EditLog editLog) {
        currentMemory.add(editLog);
    }

    public void exChangeMemory() {
        LinkedList<EditLog> tmp;
        tmp = this.currentMemory;
        this.currentMemory = this.buffReady;
        this.buffReady = tmp;
    }

    public void flush() {
        for (EditLog editLog : buffReady) {
            //模拟将数据写到磁盘
            System.out.println(editLog);
        }
        buffReady.clear();
    }

    public long getMaxTxid() {
        return buffReady.getLast().getTxid();
    }
}

