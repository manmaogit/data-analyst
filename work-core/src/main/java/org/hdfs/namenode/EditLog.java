package org.hdfs.namenode;


public class EditLog {

    public long txid;

    public String log;

    public EditLog(long txid, String log) {
        this.txid = txid;
        this.log = log;
    }

    public long getTxid() {
        return txid;
    }

    public void setTxid(long txid) {
        this.txid = txid;
    }

    public String getLog() {
        return log;
    }

    public void setLog(String log) {
        this.log = log;
    }
}
