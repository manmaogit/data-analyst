package org.election.redission.election;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author AMDIN
 */
public class IPaasThreadFactory implements ThreadFactory {

    private final String threadNamePrefix;
    private static final AtomicInteger THREAD_IDX = new AtomicInteger();
    private final boolean daemon;

    public IPaasThreadFactory(String threadNamePrefix, boolean daemon) {
        this.threadNamePrefix = threadNamePrefix;
        this.daemon = daemon;
    }

    @Override
    public Thread newThread(Runnable runnable) {
        SecurityManager s = System.getSecurityManager();
        Thread thread = new Thread(s != null ? s.getThreadGroup() : new ThreadGroup("thread-group"), runnable, this.threadNamePrefix + THREAD_IDX.getAndIncrement());
        thread.setDaemon(this.daemon);
        return thread;
    }
}
