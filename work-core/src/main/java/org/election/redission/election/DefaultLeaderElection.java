package org.election.redission.election;

import cn.hutool.core.net.NetUtil;
import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * 领导选举
 * <p>
 *
 * @author mao.man<br>
 * @version 1.0<br>
 * @date 2022/04/20 <br>
 */
@Slf4j
public class DefaultLeaderElection implements LeaderElection {

    /**
     * 选举轮询时间间隔
     */
    private static final int WAIT_SECONDS = 5;

    /**
     *  Master 节点心跳上报Redis时间间隔
     */
    private static final int MASTER_WAIT_SECONDS = 60;

    /**
     * redis 客户端
     */
    private final RedissonClient redissonClient;

    /**
     * 领导锁
     */
    private RLock leaderLock;

    /**
     * 线程 while 循环标识，选举线程停止标识
     */
    private volatile boolean electionThreadStop = false;

    /**
     * 初始化完成标识
     */
    private volatile boolean isInit = false;

    private final Object masterLock = new Object();

    private final Object initLock = new Object();

    private final ElectionThread electionThread = new ElectionThread();

    private final List<ElectionListener> listeners = new ArrayList<>();

    private final MasterNodeManager masterNodeManager;

    public DefaultLeaderElection(RedissonClient redissonClient, MasterNodeManager masterNodeManager) {
        this.redissonClient = redissonClient;
        this.masterNodeManager = masterNodeManager;
    }


    /**
     * 开始启动领导选举，
     * 1.如果被选举为Master,则线程进入wait状态
     * 2.如果未被选中，则轮询，每个5秒参与一次选举
     *
     * @param leaderName 选举的内容，REDIS key
     */
    @Override
    public void startLeaderElection(String leaderName) {
        leaderLock = redissonClient.getLock(leaderName);
        electionThread.start();
        Runtime.getRuntime().addShutdownHook(new Thread(this::shutdown));
    }

    /**
     * 判断当前节点 是否是 Master节点
     *
     * @return true or false
     */
    @Override
    public boolean isMaster() {
        if (isInit) {
            return electionThread.isMaster();
        }
        // 等待初始化完成
        synchronized (initLock) {
            if (!isInit) {
                try {
                    initLock.wait();
                } catch (InterruptedException e) {
                    log.warn("interrupted exception in isMaster", e);
                }
            }
        }
        return electionThread.isMaster();
    }

    /**
     * 添加选举监听器
     *
     * @param electionListener 选举监听器
     */
    @Override
    public void addElectionListener(ElectionListener electionListener) {
        if (listeners.contains(electionListener)) {
            return;
        }
        listeners.add(electionListener);
    }

    /**
     * 关闭选举线程
     */
    @Override
    public void shutdown() {
        if (electionThreadStop) {
            return;
        }
        electionThreadStop = true;
        // 唤醒master 阻塞线程
        synchronized (masterLock) {
            masterLock.notifyAll();
        }
        listeners.clear();

        log.info("shutdown and give up leadership");
    }

    @Override
    public boolean isOldMasterOffline() {
        return masterNodeManager.isMasterNodeOffline();
    }

    /**
     * 获取leader锁
     * <p>
     * 如果获取到锁，需要检测之前的Master节点是否真的宕机。
     *
     * @return true 获取到leader权限
     * @throws InterruptedException tryLock 中断异常
     */
    private boolean getLeaderShipInternal() throws InterruptedException {
        // 尝试获取 leader 锁，如果锁住，则为master
        boolean locked = leaderLock.tryLock(WAIT_SECONDS, TimeUnit.SECONDS);
        // 如果获取到Master锁，并且原来的Master节点已经下线
        if (locked && isOldMasterOffline()) {
            return true;
        }
        // 如果Master未下线，证明Master节点因为网络抖动导致锁续期失败，当前节点放弃选举
        if (locked && leaderLock.isLocked() && leaderLock.isHeldByCurrentThread()) {
            leaderLock.unlock();
            Thread.sleep(5000);
        }
        return false;
    }

    /**
     * master节点 补偿操作
     * 当发现leader锁没有被持有，则尝试重新加锁，并且将本地ip:port更新到Redis
     * 如果加锁失败，则证明发生了脑裂，需要人工接入
     */
    private void masterRefreshLockAndHostInfo() {
        try {
            if (leaderLock.isHeldByCurrentThread()) {
                // 更新Master节点IP信息
                masterNodeManager.updateRemoteMasterUriWithSelfHost();
                return;
            }

            // 未持有锁，则尝试加锁
            boolean locked = leaderLock.tryLock(WAIT_SECONDS, TimeUnit.SECONDS);
            if (locked) {
                // 更新Master节点IP信息
                masterNodeManager.updateRemoteMasterUriWithSelfHost();
            } else {
                // 发生脑裂
                log.error("split brain error!!!,localhost:{}, remote master uri:{}", NetUtil.getLocalhostStr(), masterNodeManager.getMasterUri());
            }
            log.warn("get leadership but not held leader lock,try lock,result:{}", locked);
        } catch (Exception ex) {
            log.error("update master info to redis exception", ex);
        }
    }

    /**
     * 通知选举成功监听器
     */
    private void notifyElected() {
        try {
            for (ElectionListener listener : listeners) {
                listener.onElected();
            }
        } catch (Throwable ex) {
            log.error("trigger listener catch exception", ex);
        }
    }

    class ElectionThread extends Thread {

        private volatile boolean isMaster = false;

        public ElectionThread() {
            setName("Runtime-Leader-Election");
        }

        @Override
        public void run() {
            // 轮询，选举失败，每隔 5 秒,重新参与选举
            while (!electionThreadStop) {
                try {
                    if (isMaster) {
                        /* 已经成为Master节点 **/
                        // Master 节点补偿操作，防止网络抖动导致选举锁数据丢失
                        masterRefreshLockAndHostInfo();
                        // 已经成为master,休眠 60s 时间
                        synchronized (masterLock) {
                            masterLock.wait(Duration.ofSeconds(MASTER_WAIT_SECONDS).toMillis());
                        }
                    } else {
                        /* slave节点 选举操作 **/
                        isMaster = getLeaderShipInternal();
                        if (isMaster) {
                            log.info("get leadership in ElectionThread");
                            masterNodeManager.updateRemoteMasterUriWithSelfHost();
                            notifyElected();
                        } else {
                            // 更新远程Master地址到本地，防止Redis Master地址丢失后，出现脑裂
                            masterNodeManager.refreshLocalMasterUriCache();
                        }
                    }
                } catch (Exception e) {
                    log.warn("interrupted exception in while", e);
                } finally {
                    synchronized (initLock) {
                        // 标记已经初始化
                        if (!isInit) {
                            initLock.notifyAll();
                            isInit = true;
                        }
                    }
                }
            }

            // master选举失败
            if (leaderLock.isLocked() && leaderLock.isHeldByCurrentThread()) {
                leaderLock.unlock();
            }

            if (isMaster) {
                isMaster = false;
            }
        }

        public boolean isMaster() {
            return isMaster;
        }
    }


}