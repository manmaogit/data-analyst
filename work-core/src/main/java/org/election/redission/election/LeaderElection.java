package org.election.redission.election;


/**
 * 领导选举
 * <p>
 *
 * @author mao.man<br>
 * @version 1.0<br>
 * @date 2022/04/20 <br>
 */
public interface LeaderElection {

    /**
     * 开始启动领导选举，
     * 1.如果被选举为Master,则线程进入wait状态
     * 2.如果未被选中，则轮询，每个5秒参与一次选举
     *
     * @param leaderName 选举的内容，REDIS key
     */
    void startLeaderElection(String leaderName);

    /**
     * 判断当前节点 是否是 Master节点
     *
     * @return true or false
     */
    boolean isMaster();

    /**
     * 添加选举监听器
     *
     * @param electionListener 选举监听器
     */
    void addElectionListener(ElectionListener electionListener);

    /**
     * 关闭选举线程
     */
    void shutdown();

    /**
     * 判断老的Master是否存活中，存活则放弃锁选举
     *
     * @return 是否老的Master下线
     */
     boolean isOldMasterOffline();
}