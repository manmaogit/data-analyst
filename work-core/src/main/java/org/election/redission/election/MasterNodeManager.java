package org.election.redission.election;

import cn.hutool.core.net.NetUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;

/**
 * 主节点相关操作
 * 更新Master节点IP到Redis <br>
 * Master几点探活
 *
 * @author mao.man<br>
 * @version 1.0<br>
 * @date 2022/04/22 <br>
 */
@Service
@Slf4j
public class MasterNodeManager {
    
    private String serverPort;

    @Resource
    private StringRedisTemplate stringRedisTemplate;

    @Resource
    private RestTemplate restTemplate;
    

    /**
     * master host:port 地址本地缓存。当redis数据丢失时，使用本地
     */
    private String masterUriCache;

    private static final String RUNTIME_MASTER_POD = "RUNTIME_MASTER_POD";

    public void refreshLocalMasterUriCache() {
        // RUNTIME_MASTER_POD
        String uri = stringRedisTemplate.boundValueOps(RUNTIME_MASTER_POD).get();
        if (StringUtils.isNotBlank(uri)) {;
            // 更新本地Master地址
            masterUriCache = uri;
        }
    }

    /**
     * 更新 当前主机地址 为Master地址
     */
    public void updateRemoteMasterUriWithSelfHost() {
        // RUNTIME_MASTER_POD_${namespace}
        String key = RUNTIME_MASTER_POD;
        stringRedisTemplate.boundValueOps(key).set(NetUtil.getLocalhostStr() + ":" + serverPort);
    }

    /**
     * 获取Redis master 地址 RUNTIME_MASTER_POD_${namespace}，
     * 如果远程地址为空，使用本地缓存地址
     *
     * @return ip:port
     */
    public String getMasterUri() {
        String remoteMasterUri = null;
        try {
            // RUNTIME_MASTER_POD
            remoteMasterUri = stringRedisTemplate.boundValueOps(RUNTIME_MASTER_POD).get();
        } catch (Exception ex) {
            log.warn("get master uri from redis fail,exception:{}", ex.getMessage());
        }
        // 优先使用远程Master Uri, Redis数据不存在或者查询redis失败，则使用本地
        String masterUri = StringUtils.isNotBlank(remoteMasterUri) ? remoteMasterUri : masterUriCache;
        if (StringUtils.isBlank(masterUri)) {
            log.error("master uri is null,remoteMasterUri:{},localMasterUri:{}", remoteMasterUri, masterUriCache);
        }
        return masterUri;
    }

    /**
     * 判断Master节点是否存活
     *
     * @return true : master is offline , false: master is online
     */
    public boolean isMasterNodeOffline() {
        String url;
        try {
            String uri = getMasterUri();
            if (StringUtils.isBlank(uri)) {
                log.warn("master node uri is null,uri:{}", uri);
                return true;
            }
            url = "http://" + uri + "/actuator/health";
        } catch (Exception ex) {
            // 防止redis异常连接，导致判断Master下线
            log.error("create master url catch exception", ex);
            return false;
        }
        // 探活 /actuator/health
        return pingMasterNodeOffline(url);
    }

    /**
     * 探测Master 节点  /actuator/health 接口，如果响应失败，则判断master下线
     *
     * @param url 健康检查接口
     * @return true: master节点已下线， false: master节点未下线
     */
    private boolean pingMasterNodeOffline(String url) {
        int maxRetry = 3;
        for (int i = 0; i <= maxRetry; i++) {
            try {
                Thread.sleep(2000L * i);
                ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.GET,
                        new HttpEntity<String>(null, new HttpHeaders()), String.class);
                log.info("send api request to master node health api,url:{},result:{}", url, responseEntity);
                if (HttpStatus.OK.equals(responseEntity.getStatusCode())) {
                    return false;
                }
            } catch (Exception ex) {
                log.warn("ping master node health api exception,continue next retry,url:{},message:{}", url, ex.getMessage());
                try {
                    Thread.sleep(2000L * i);
                } catch (Exception ignored) {
                }
            }
        }
        log.warn("master node is offline,url:{},", url);
        return true;
    }
}
