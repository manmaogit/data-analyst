package org.election.redission.election;

/**
 * 领导选举监听器
 *
 * @author mao.man<br>
 * @version 1.0<br>
 * @date 2022/04/20 <br>
 */
public interface ElectionListener {

    /**
     * 当选master触发
     */
    void onElected();
}