package org.election.redission.election;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;


/**
 * 默认Master选举的监听器 <br>
 *
 * @author mao.man<br>
 * @version 1.0<br>
 * @date 2022/04/20 <br>
 */
@Slf4j
@Component
public class DefaultElectionListener implements ElectionListener, InitializingBean {

    @Override
    public void afterPropertiesSet() throws Exception {
    }

    @Override
    public void onElected() {
        log.info("get leadership in DefaultElectionListener");
    }

}
