package org.election.zk.election;

import org.apache.curator.framework.state.ConnectionState;

public class HaDemo extends SingleActiveNodeCluster {

    @Override
    protected void process() throws Exception {
        System.out.println("process");
    }

    @Override
    protected void onStateLost(ConnectionState state) {

    }

    @Override
    protected void onStateConnected(ConnectionState state) {

    }

    @Override
    protected String getName() {
        return "HaDemo";
    }

    public static void main(String[] args) {
        HaDemo demo = new HaDemo();
        demo.start();
        while (true){}
    }
}
