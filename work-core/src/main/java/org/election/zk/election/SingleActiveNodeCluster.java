package org.election.zk.election;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.framework.recipes.leader.LeaderSelector;
import org.apache.curator.framework.recipes.leader.LeaderSelectorListener;
import org.apache.curator.framework.state.ConnectionState;
import org.apache.curator.retry.RetryNTimes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * ZK HA 根据获取到leader角色执行
 *
 * @author manmao
 * @since 2020-11-13
 */
public abstract class SingleActiveNodeCluster implements LeaderSelectorListener {

    private Logger log = LoggerFactory.getLogger(getClass());

    private static final String ZK_PATH = "/leader/";

    private LeaderSelector leaderSelector;

    private static final int SESSION_TIMEOUT_MS = 15 * 1000;

    private static final int CONNECTION_TIMEOUT_MS = 10 * 1000;

    private static final String ZK_SERVER_LIST = "172.22.34.239:2181,172.22.35.220:2181,172.22.33.233:2181";

    /**
     * 开始整个服务，开始竞争任务执行权
     */
    @PostConstruct
    public void start() {
        CuratorFramework client = CuratorFrameworkFactory.builder().connectString(ZK_SERVER_LIST).namespace("dsp")
                .retryPolicy(new RetryNTimes(Integer.MAX_VALUE, 1000))
                .sessionTimeoutMs(SESSION_TIMEOUT_MS).connectionTimeoutMs(CONNECTION_TIMEOUT_MS)
                .build();
        client.start();
        leaderSelector = new LeaderSelector(client, ZK_PATH + getNode(), this);
        // 释放leader权限后，重新自动进入选举
        leaderSelector.autoRequeue();
        leaderSelector.start();
    }

    /**
     * 关闭整个服务
     */
    @PreDestroy
    public void stop() {
        try {
            leaderSelector.close();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }


    /**
     * 本节点成为leader,本方法返回后,释放leader权限。
     *
     * 如果设置了  leaderSelector.autoRequeue(); 则重新进出leader选举
     *
     * @param client 客户端
     * @throws Exception 异常
     */
    @Override
    public void takeLeadership(CuratorFramework client) throws Exception {
        log.info(getName() + " 成为Leader,开始执行任务 ");
        System.out.println(getName() + " 成为Leader,开始执行任务 ");
        executeTask();
    }

    @Override
    public void stateChanged(CuratorFramework client, ConnectionState newState) {
        if ((newState == ConnectionState.LOST)) {
            log.info(getName() + " - 断开与zookeeper连接，放弃任务竞争权或执行中的任务 ");
            onStateLost(newState);
        } else if (newState == ConnectionState.CONNECTED || newState == ConnectionState.RECONNECTED) {
            log.info(getName() + " - 连接到zookeeper，获得任务竞争权");
            onStateConnected(newState);
        }
    }

    private void executeTask() {
        try {
            process();
        } catch (Exception ex) {
            log.error("process throw exception", ex);
        }
        log.info("结束任务 " + getName());
    }


    /**
     * 获取leader权限后执行
     *
     * @throws Exception
     */
    protected abstract void process() throws Exception;


    /**
     * zk连接断开
     *
     * @param state zk状态
     */
    protected abstract void onStateLost(ConnectionState state);

    /**
     * zk连接
     *
     * @param state zk状态
     */
    protected abstract void onStateConnected(ConnectionState state);


    /**
     * 任务名字
     *
     * @return 任务名字
     */
    protected abstract String getName();

    /**
     * zookeeper节点名称
     */
    private String getNode() {
        return this.getClass().getName();
    }

}
