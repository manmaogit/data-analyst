package org.flink.classloader;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.function.Consumer;

/**
 * URLClassLoader扩展类，打破JVM双亲委派机制。
 * <p>
 * 加载class 它优先从URL对应的class中加载，加载失败，再从父类加载器 加载class。
 *
 * @author mao.man<br>
 * @version 1.0<br>
 * @date 2021/06/15 <br>
 */
public final class ChildFirstClassLoader extends AbstractPaasUserCodeClassLoader {

    /**
     * 默认下面报名开头的类，用父类加载器
     */
    private static final String[] DEFAULT_PARENT_FIRST_PATTERNS = new String[]{"java.", "scala.", "javax.annotation.",
            "org.slf4j", "org.apache.log4j", "org.apache.logging", "org.apache.commons.logging", "ch.qos.logback",
            "javax.servlet.", "org.apache.catalina."};

    /**
     * 配置class优先使用父类加载器加载，避免用户组件自定义class和IPaas Runtime系统自带类冲突
     */
    private final String[] alwaysParentFirstPatterns;

    /**
     * 类加载器名字，每个系统类加载器对应一个组件
     */
    private String name;

    /**
     * @param urls                         class资源路径
     * @param parent                       父类加载器
     * @param classLoadingExceptionHandler 异常处理
     */
    public ChildFirstClassLoader(
            URL[] urls,
            ClassLoader parent,
            Consumer<Throwable> classLoadingExceptionHandler) {
        this(urls, parent, null, classLoadingExceptionHandler);
    }

    /**
     * @param urls                         class资源路径
     * @param parent                       父类加载器
     * @param alwaysParentFirstPatterns    默认父类加载器优先加载的class
     * @param classLoadingExceptionHandler 异常处理
     */
    public ChildFirstClassLoader(
            URL[] urls,
            ClassLoader parent,
            String[] alwaysParentFirstPatterns,
            Consumer<Throwable> classLoadingExceptionHandler) {
        super(urls, parent, classLoadingExceptionHandler);
        if (alwaysParentFirstPatterns == null || alwaysParentFirstPatterns.length == 0) {
            this.alwaysParentFirstPatterns = DEFAULT_PARENT_FIRST_PATTERNS;
        } else {
            this.alwaysParentFirstPatterns = alwaysParentFirstPatterns;
        }
    }

    /**
     * @param urls                         class资源路径
     * @param parent                       父类加载器
     * @param alwaysParentFirstPatterns    默认父类加载器优先加载的class
     * @param classLoadingExceptionHandler 异常处理
     * @param name                          类加载器名字
     */
    public ChildFirstClassLoader(
            URL[] urls,
            ClassLoader parent,
            String[] alwaysParentFirstPatterns,
            Consumer<Throwable> classLoadingExceptionHandler,
            String name) {
        super(urls, parent, classLoadingExceptionHandler);
        if (alwaysParentFirstPatterns == null || alwaysParentFirstPatterns.length == 0) {
            this.alwaysParentFirstPatterns = DEFAULT_PARENT_FIRST_PATTERNS;
        } else {
            this.alwaysParentFirstPatterns = alwaysParentFirstPatterns;
        }
        this.name = name;
    }



    /**
     * 优先从URLClassLoader加载类，如果自定义的URL资源下类加载失败，则让父类加载器加载类
     *
     * @param name    class名字
     * @param resolve
     * @return 返回加载的class类
     * @throws ClassNotFoundException 类未找到
     */
    @Override
    protected Class<?> loadClassWithoutExceptionHandling(String name, boolean resolve)
            throws ClassNotFoundException {

        // First, check if the class has already been loaded
        Class<?> c = findLoadedClass(name);

        if (c == null) {
            // check whether the class should go parent-first
            for (String alwaysParentFirstPattern : alwaysParentFirstPatterns) {
                if (name.startsWith(alwaysParentFirstPattern)) {
                    return super.loadClassWithoutExceptionHandling(name, resolve);
                }
            }

            try {
                // check the URLs
                c = findClass(name);
            } catch (ClassNotFoundException e) {
                // let URLClassLoader do it, which will eventually call the parent
                c = super.loadClassWithoutExceptionHandling(name, resolve);
            }
        } else if (resolve) {
            resolveClass(c);
        }

        return c;
    }

    @Override
    public URL getResource(String name) {
        // first, try and find it via the URLClassloader
        URL urlClassLoaderResource = findResource(name);

        if (urlClassLoaderResource != null) {
            return urlClassLoaderResource;
        }

        // delegate to super
        return super.getResource(name);
    }

    @Override
    public Enumeration<URL> getResources(String name) throws IOException {
        // first get resources from URLClassloader
        Enumeration<URL> urlClassLoaderResources = findResources(name);

        final List<URL> result = new ArrayList<>();

        while (urlClassLoaderResources.hasMoreElements()) {
            result.add(urlClassLoaderResources.nextElement());
        }

        // get parent urls
        Enumeration<URL> parentResources = getParent().getResources(name);

        while (parentResources.hasMoreElements()) {
            result.add(parentResources.nextElement());
        }

        return new Enumeration<URL>() {
            Iterator<URL> iter = result.iterator();

            @Override
            public boolean hasMoreElements() {
                return iter.hasNext();
            }

            @Override
            public URL nextElement() {
                return iter.next();
            }
        };
    }

    public String getName() {
        return name;
    }

    /*
     * 支持类的并发加载的类加载器称为支持并行的类加载器，需要通过调用类加载器在类初始化时注册它们自己。registerAsParallelCapable方法。
     * 注意，默认情况下ClassLoader类被注册为支持并行的。但是，它的子类仍然需要注册它们自己，如果它们是并行的。
     * 在委托模型没有严格层次结构的环境中，类装入器需要具有并行能力，否则类装入可能会导致死锁，因为装入器锁在类装入过程期间一直持有(请参阅loadClass方法)。
     */
    static {
        ClassLoader.registerAsParallelCapable();
    }
}