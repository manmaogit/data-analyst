package org.flink.memory.demo;


import org.flink.memory.MemoryAllocationException;
import org.flink.memory.MemoryManager;
import org.flink.memory.core.MemorySegment;
import org.flink.memory.core.MemoryType;
import org.flink.memory.demo.proto.AddressBookBuf;

import java.io.IOException;
import java.util.List;

/**
 * <br>
 *
 * @author mao.man<br>
 * @version 1.0<br>
 * @date 2021/09/25 <br>
 */
public class ProtoBufDemo {
    public static void main(String[] args) throws IOException, MemoryAllocationException {
        // 序列化
        AddressBookBuf.AddressBook book =
                AddressBookBuf.AddressBook.newBuilder()
                        .setEmail("manmao@1994.98.com")
                        .setId(1234)
                        .setName("John Doe")
                        .setAddress("成都市")
                        .build();


        // 序列化
        AddressBookBuf.AddressBook book1 =
                AddressBookBuf.AddressBook.newBuilder()
                        .setEmail("man@1994.98.com")
                        .setId(123445)
                        .setName("John")
                        .setAddress("自贡市")
                        .build();

        MemoryManager memoryManager = new MemoryManager(512 * 1024 * 1024, 1, 32 * 1024, MemoryType.OFF_HEAP, true);
        List<MemorySegment> memorySegments = memoryManager.allocatePages("123", 1);
        int writeIndex = 0;
        if (writeIndex + book.toByteArray().length > memoryManager.getPageSize()) {
            // TODO
        }
        memorySegments.get(0).putInt(writeIndex,book.toByteArray().length);
        writeIndex  += 4;
        memorySegments.get(0).put(writeIndex, book.toByteArray());
        writeIndex  += book.toByteArray().length;

        memorySegments.get(0).putInt(writeIndex,book1.toByteArray().length);
        writeIndex  += 4;
        memorySegments.get(0).put(writeIndex, book1.toByteArray());
        writeIndex  += book1.toByteArray().length;

        // 反序列化
        // 对象1
        int readIndex  =  0;
        int length = memorySegments.get(0).getInt(readIndex);
        readIndex += 4;
        byte[] bytes =  new byte[length];
        memorySegments.get(0).get(readIndex, bytes, 0, length);
        readIndex += length;
        AddressBookBuf.AddressBook bookDeseria = AddressBookBuf.AddressBook.parseFrom(bytes);

        // 对象2
        int length1 = memorySegments.get(0).getInt(readIndex);
        readIndex += 4;
        byte[] bytes1 =  new byte[length1];
        memorySegments.get(0).get(readIndex, bytes1, 0, length1);
        readIndex += length;
        AddressBookBuf.AddressBook bookDeseria1 = AddressBookBuf.AddressBook.parseFrom(bytes1);

        System.out.println(bookDeseria);
        System.out.println(bookDeseria1);

        System.out.println(bookDeseria.getAddress());
        System.out.println(bookDeseria1.getAddress());

    }
}
