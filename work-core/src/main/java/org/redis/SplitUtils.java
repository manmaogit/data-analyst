package org.redis;

import com.google.common.collect.Lists;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class SplitUtils {
    public static final int MAX_KEYS_PKG_SIZE = 1000;
    public static final int FIRST_KYES_SPLIT_SIZE = 1400;
    
    public static List<Integer> splitToIntList(String value) {
        List<Integer> result = Lists.newArrayList();
        if (!org.apache.commons.lang3.StringUtils.isEmpty(value)) {
            String[] arr = value.split(",");
            for (String v : arr) {
                try {
                    Integer i = Integer.valueOf(v.trim());
                    result.add(i);
                } catch (Exception e) {
                }
            }
        }
        return result;
    }

    public static List<String> splitToStringList(String value) {
        List<String> result = Lists.newArrayList();
        if (!org.apache.commons.lang3.StringUtils.isEmpty(value)) {
            String[] arr = value.split(",");
            for (String v : arr) {
                result.add(v.trim());
            }
        }
        return result;
    }
    
    public static <T> List<List<T>> splitKeyList(List<T> keys) {
        return splitKeyList(keys, MAX_KEYS_PKG_SIZE, FIRST_KYES_SPLIT_SIZE);
    }
    
    /**
     * 关键字过多时，分隔成较小的包运行，提高效率
     * @param keys
     * @param maxPkgSize 最大包数量
     * @param firstSplitSize 首次分包数量，应该比maxPkgSize大，但小于maxPkgSize的两倍，
     *    这样允许小于firstSplitSize的关键字都在一个包里，使七八成的查询都可以一次解决
     * @return 分包后的列表，每个包的大小基本一致
     */
    public static <T> List<List<T>> splitKeyList(List<T> keys, int maxPkgSize, int firstSplitSize) {
        List<List<T>> result = Lists.newArrayList();
        int size = keys.size();
        if (size < firstSplitSize) {
            result.add(keys);
        } else {
            int pkgCount = size % maxPkgSize == 0 ? (size / maxPkgSize) : (size /maxPkgSize + 1);
            int pkgSize = size / pkgCount;
            for (int i = 0; i < pkgCount - 1; i++) {
                result.add(keys.subList(i * pkgSize, i * pkgSize + pkgSize));
            }
            result.add(keys.subList((pkgCount - 1) * pkgSize, size));
        }
        return result;
    }
    
    public static <K, V> List<Map<K, V>> splitKeyMap(Map<K, V> keys) {
        return splitKeyMap(keys, MAX_KEYS_PKG_SIZE, FIRST_KYES_SPLIT_SIZE);
    }
            
    public static <K, V> List<Map<K, V>> splitKeyMap(Map<K, V> keys, int maxPkgSize, int firstSplitSize) {
        List<Map<K, V>> result = Lists.newArrayList();
        int size = keys.size();
        if (size < firstSplitSize) {
            result.add(keys);
        } else {
            int pkgCount = size % maxPkgSize == 0 ? (size / maxPkgSize) : (size /maxPkgSize + 1);
            int pkgSize = size / pkgCount;
            int i = 0, c = 1;
            Map<K, V> pkgMap = new HashMap<>();
            for (Entry<K, V> entry : keys.entrySet()) {
                pkgMap.put(entry.getKey(), entry.getValue());
                i++;
                if (i == pkgSize && c < pkgCount) {
                    i = 0;
                    c++;
                    result.add(pkgMap);
                    pkgMap = new HashMap<>();
                }
            }
            result.add(pkgMap);
        }
        return result;
    }
}
