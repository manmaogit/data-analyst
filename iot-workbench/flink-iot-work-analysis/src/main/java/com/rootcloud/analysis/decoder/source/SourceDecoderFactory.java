/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.decoder.source;

import com.rootcloud.analysis.scanner.ClasspathPackageScanner;

import java.io.IOException;
import java.lang.reflect.Modifier;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SourceDecoderFactory {

  private static Logger logger = LoggerFactory.getLogger(SourceDecoderFactory.class);
  private static Map<String, AbstractSourceDecoder> decoderMap = new HashMap<>();

  static {
    ClasspathPackageScanner scanner = new ClasspathPackageScanner(
        "com.rootcloud.analysis.decoder.source.impl");
    try {
      List<String> classList = scanner.getFullyQualifiedClassNameList();

      for (String className : classList) {
        try {
          Class clazz = SourceDecoderFactory.class.getClassLoader()
              .loadClass(className.replace("com/rootcloud/analysis/decoder/source/impl/",
                  ""));
          if (Modifier.isAbstract(clazz.getModifiers()) || clazz.isInterface()) {
            continue;
          }
          Object obj = clazz.newInstance();
          if (!(obj instanceof AbstractSourceDecoder)) {
            continue;
          }
          AbstractSourceDecoder decoder = (AbstractSourceDecoder) obj;
          SourceDecoder anno = decoder.getClass().getAnnotation(SourceDecoder.class);
          if (anno == null) {
            continue;
          }
          decoderMap.put(anno.dataSourceType().name(), decoder);
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException e) {
          logger.warn("Exception occurred when initializing AbstractSourceDecoder instance", e);
        }
      }
    } catch (IOException e) {
      logger.error("Failed to init SourceDecoderFactory", e);
    }
  }

  /**
   * getDecoder.
   */
  public static AbstractSourceDecoder getDecoder(String inputType) {
    if (decoderMap.containsKey(inputType)) {
      return decoderMap.get(inputType);
    }

    logger.error("Source decoder not found: {}", inputType);
    return null;
  }
}
