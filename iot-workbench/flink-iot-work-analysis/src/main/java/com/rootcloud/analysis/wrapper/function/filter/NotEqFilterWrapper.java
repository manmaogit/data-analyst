/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.wrapper.function.filter;

import com.rootcloud.analysis.common.entity.RcUnit;
import lombok.Builder;
import lombok.ToString;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Builder
@ToString(callSuper = true)
public class NotEqFilterWrapper extends AbstractFilterWrapper {

  private static Logger logger = LoggerFactory.getLogger(NotEqFilterWrapper.class);

  private String attribute;

  public Object value;

  @Override
  public boolean filter(RcUnit record) {
    boolean result = false;
    try {
      if (record.getData().containsKey(attribute)) {
        Object attrValue = record.getAttr(attribute);
        if (attrValue instanceof String || attrValue instanceof Boolean) {
          if (attrValue != null && attrValue.toString().compareTo(String.valueOf(value)) != 0) {
            result = true;
          }
        } else {
          if (attrValue == null || Double
              .compare(Double.parseDouble(String.valueOf(attrValue)),
                  Double.parseDouble((String) value))
              == 0) {
            result = false;
          } else {
            return true;
          }
        }
      }
    } catch (Exception ex) {
      logger.error("Exception occurred when filtering data: {}， exception message: {}",
          record.toString(), ex.getMessage(), ex);
      result = false;
    }
    logger.debug("'NOT_EQ' filter result: {}, attribute: {}, record: {}, value: {}",
        result, attribute, record.toString(), value);
    return result;
  }

}
