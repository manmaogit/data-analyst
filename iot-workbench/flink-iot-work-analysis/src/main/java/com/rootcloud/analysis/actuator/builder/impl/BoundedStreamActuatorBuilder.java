/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.actuator.builder.impl;

import static com.rootcloud.analysis.core.enums.JobTypeEnum.OFFLINE;

import com.rootcloud.analysis.actuator.builder.ActuatorBuilder;
import com.rootcloud.analysis.core.exec.Actuator;
import com.rootcloud.analysis.core.graph.IGraph;

import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

@ActuatorBuilder(jobType = OFFLINE)
public class BoundedStreamActuatorBuilder extends DefaultActuatorBuilder {

  @Override
  public Actuator build(String jobName, String jobType, IGraph graph,
                        StreamExecutionEnvironment env) throws Exception {
    return super.build(jobName, jobType, graph, env);
  }
}
