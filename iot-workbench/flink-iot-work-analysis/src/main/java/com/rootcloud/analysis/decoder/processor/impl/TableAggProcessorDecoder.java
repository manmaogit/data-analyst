/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.decoder.processor.impl;

import com.alibaba.fastjson.JSONObject;
import com.rootcloud.analysis.core.constants.JobConstant;
import com.rootcloud.analysis.core.enums.ProcessTypeEnum;
import com.rootcloud.analysis.decoder.processor.AbstractProcessorDecoder;
import com.rootcloud.analysis.decoder.processor.ProcessorDecoder;
import com.rootcloud.analysis.exception.DecodeProcessorException;
import com.rootcloud.analysis.wrapper.processor.AbstractProcessorWrapper;
import com.rootcloud.analysis.wrapper.processor.TableAggProcessorWrapper;
import java.util.ArrayList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 表值聚合算子.
 *
 * @author: zexin.huang
 * @createdDate: 2022/8/15 10:47
 */
@ProcessorDecoder(processType = ProcessTypeEnum.UDT_AGG)
public class TableAggProcessorDecoder extends AbstractProcessorDecoder {

  private static Logger logger = LoggerFactory.getLogger(TableAggProcessorDecoder.class);

  @Override
  public AbstractProcessorWrapper decodeProcessParams(JSONObject conf) {
    try {
      TableAggProcessorWrapper.TableAggProcessorWrapperBuilder builder =
          TableAggProcessorWrapper.builder();
      builder.udtAggFuncName(conf.getString("udf_name"))
          .groupFields(new ArrayList(conf.getJSONArray(JobConstant.KEY_GROUP_BY_KEY)))
          .sourceAttribute(conf.getJSONArray(JobConstant.KEY_INPUT_SCHEMA))
          .targetAttribute(conf.getJSONArray(JobConstant.KEY_SCHEMA));
      TableAggProcessorWrapper result = builder.build();
      logger.debug("Decoding table agg processor: {}", result);
      return result;
    } catch (Exception ex) {
      logger.error("Exception occurred when decoding table agg processor: {}", conf);
      throw new DecodeProcessorException();
    }
  }
}
