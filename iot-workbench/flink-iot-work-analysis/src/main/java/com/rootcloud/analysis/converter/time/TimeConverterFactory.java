/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.converter.time;

import com.rootcloud.analysis.scanner.ClasspathPackageScanner;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TimeConverterFactory {
  private static Logger logger = LoggerFactory.getLogger(TimeConverterFactory.class);
  private static Map<String, ITimeConverter> timeConvertMap = new HashMap<>();

  static {
    ClasspathPackageScanner scanner = new ClasspathPackageScanner(
        "com.rootcloud.analysis.converter.time.impl");
    try {
      List<String> classList = scanner.getFullyQualifiedClassNameList();

      for (String clazz : classList) {
        ITimeConverter converter = (ITimeConverter) TimeConverterFactory.class
            .getClassLoader()
            .loadClass(clazz.replace("com/rootcloud/analysis/converter/time/impl/",
                "")).newInstance();
        TimeConvert anno = converter.getClass().getAnnotation(TimeConvert.class);
        timeConvertMap.put(anno.converterType().name(), converter);
      }
    } catch (IOException | ClassNotFoundException | InstantiationException
        | IllegalAccessException e) {
      logger.error("TimeConverterFactory init failed", e);
    }
  }

  /**
   * getConverter.
   */
  public static ITimeConverter getConverter(String converterType) {
    if (timeConvertMap.containsKey(converterType)) {
      return timeConvertMap.get(converterType);
    }

    logger.error("TimeConverterFactory getConverter converterType:{} no converter", converterType);
    return null;
  }

}
