/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.converter.frame;

import com.rootcloud.analysis.common.entity.RcUnitFrame;
import com.rootcloud.analysis.common.util.ClassUtil;
import com.rootcloud.analysis.converter.extractor.EventTimeExtractor;
import com.rootcloud.analysis.converter.map.RcUnitFrameToBeanMap;

import java.io.Serializable;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtMethod;
import org.apache.flink.streaming.api.datastream.DataStream;

public class RcUnitFrameToBeanConverter implements IFrameConverter<RcUnitFrame, Object> {
  private static final String BEAN_NAME = "com.rc.BeanEntity";
  private static final String CONTAINER_NAME = "com.rc.MapContainer";

  private List<String> keys = new ArrayList<>();
  private List<String> valueTypes = new ArrayList<>();

  public RcUnitFrameToBeanConverter(List<String> keys, List<String> valueTypes) {
    this.keys = keys;
    this.valueTypes = valueTypes;
  }

  @Override
  public DataStream convert(DataStream<RcUnitFrame> input) throws Exception {

    ClassPool pool = ClassPool.getDefault();
    CtClass clazz = ClassUtil.makeClass(pool, BEAN_NAME, keys, valueTypes);
    clazz.toClass();

    pool.importPackage(DataStream.class.getPackage().getName());
    pool.importPackage(RcUnitFrame.class.getPackage().getName());
    pool.importPackage(this.getClass().getPackage().getName());
    pool.importPackage(List.class.getPackage().getName());
    pool.importPackage(Class.class.getPackage().getName());
    pool.importPackage(RcUnitFrameToBeanMap.class.getPackage().getName());
    pool.importPackage(EventTimeExtractor.class.getPackage().getName());
    pool.importPackage(Serializable.class.getPackage().getName());

    CtClass container = ClassUtil.makeClass(pool, CONTAINER_NAME);

    CtClass interf = pool.getCtClass(Serializable.class.getName());
    CtClass[] interfaces = new CtClass[]{interf};
    container.setInterfaces(interfaces);

    String body = String.format("public DataStream/*<%s>*/ getStream(DataStream/*<RcUnitFrame>*/"
        + " input,List keys) {\n "
        + "DataStream/*<%s>*/ mapDataStream = input.map("
        + "new RcUnitFrameToBeanMap/*<%s>*/(keys,%s.class)).returns("
        + "%s.class);\n"
        + "return mapDataStream;}",
        BEAN_NAME,
        BEAN_NAME,
        BEAN_NAME,
        BEAN_NAME,
        BEAN_NAME);

    CtMethod gmethod = CtMethod.make(body, container);
    container.addMethod(gmethod);
    Class runner = container.toClass();

    Method method = runner.getMethod("getStream", DataStream.class, List.class);
    Object object = runner.newInstance();
    return (DataStream) method.invoke(object, input, keys);
  }

}
