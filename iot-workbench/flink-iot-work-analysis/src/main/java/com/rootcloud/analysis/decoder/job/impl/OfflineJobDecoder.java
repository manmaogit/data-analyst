/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.decoder.job.impl;

import com.alibaba.fastjson.JSONObject;
import com.rootcloud.analysis.core.enums.JobTypeEnum;
import com.rootcloud.analysis.core.graph.IGraph;
import com.rootcloud.analysis.decoder.job.JobDecoder;

@JobDecoder(jobType = JobTypeEnum.OFFLINE)
public class OfflineJobDecoder extends DefaultJobDecoder {

  @Override
  public IGraph decode(JSONObject conf) {
    return super.decode(conf);
  }
}
