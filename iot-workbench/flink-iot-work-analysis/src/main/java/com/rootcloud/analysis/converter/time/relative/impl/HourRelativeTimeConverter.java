/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.converter.time.relative.impl;

import com.rootcloud.analysis.converter.time.relative.IRelativeTimeConverter;
import com.rootcloud.analysis.converter.time.relative.RelativeTimeConvert;
import com.rootcloud.analysis.core.enums.TimeZoneTypeEnum;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;

@RelativeTimeConvert(timeUnit = "h")
public class HourRelativeTimeConverter implements IRelativeTimeConverter {

  @Override
  public long convert2EpochSecond(String datetimeStr, String pattern, TimeZoneTypeEnum timezone,
                                  int timeOffset) {
    LocalDateTime localDateTime = LocalDateTime
        .parse(datetimeStr, DateTimeFormatter.ofPattern(pattern));
    localDateTime = localDateTime.plusHours(timeOffset);
    return localDateTime.toEpochSecond(ZoneOffset.of(timezone.getTimeZoneValue().substring(3)));
  }

  @Override
  public long convert2EpochSecond(String datetimeStr, String pattern, ZoneOffset offset,
                                  int timeOffset) {
    LocalDateTime localDateTime = LocalDateTime
        .parse(datetimeStr, DateTimeFormatter.ofPattern(pattern));
    localDateTime = localDateTime.plusHours(timeOffset);
    return localDateTime.toEpochSecond(offset);
  }

  @Override
  public long convert2EpochSecond(String yyyyMMddHHmmss, ZoneOffset offset, int timeOffset) {
    return convert2EpochSecond(yyyyMMddHHmmss.substring(0, 10) + "0000",
        "yyyyMMddHHmmss", offset, timeOffset);
  }
}
