/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.wrapper.jdbc;

import com.alibaba.fastjson.JSONArray;

public class PostgresqlDynamicConnWrapper extends JdbcDynamicConnWrapper {

  /**
   * 构造方法.
   */
  public PostgresqlDynamicConnWrapper(String driverClassName, String url, String username,
                                      String password, String validationQuery, String table,
                                      JSONArray schema, String countSql, String selectSql) {
    super(driverClassName, url, username, password, validationQuery,
        table, schema, countSql, selectSql);
  }

}
