/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.decoder.source.impl;

import com.alibaba.fastjson.JSONObject;
import com.rootcloud.analysis.context.IotWorkRuntimeDecodeContext;
import com.rootcloud.analysis.core.constants.JobConstant;
import com.rootcloud.analysis.core.enums.DataSourceTypeEnum;
import com.rootcloud.analysis.core.enums.SqlModeEnum;
import com.rootcloud.analysis.core.util.RsaUtil;
import com.rootcloud.analysis.decoder.source.AbstractSourceDecoder;
import com.rootcloud.analysis.decoder.source.SourceDecoder;
import com.rootcloud.analysis.exception.DecodeSourceException;
import com.rootcloud.analysis.wrapper.source.AbstractSourceWrapper;
import com.rootcloud.analysis.wrapper.source.PostgresqlBatchSourceWrapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SourceDecoder(dataSourceType = DataSourceTypeEnum.PGSQL_BATCH)
public class PostgresqlBatchSourceDecoder extends AbstractSourceDecoder {

  private final Logger logger = LoggerFactory.getLogger(PostgresqlBatchSourceDecoder.class);

  @Override
  public AbstractSourceWrapper decodeSourceParams(JSONObject conf) {
    try {
      JSONObject params = conf.getJSONObject(JobConstant.KEY_PARAMETERS);

      PostgresqlBatchSourceWrapper.Builder builder = new PostgresqlBatchSourceWrapper.Builder();
      String sqlModeStr = conf.getString(JobConstant.KEY_SQL_MODE);
      builder
          .setUri(params.getString(JobConstant.KEY_URI))
          .setSqlMode(sqlModeStr == null ? SqlModeEnum.TABLE : SqlModeEnum.valueOf(sqlModeStr))
          .setTableSql(conf.getString(JobConstant.KEY_TABLE_SQL2))
          .setUsername(params.getString(JobConstant.KEY_USERNAME))
          .setFilterSql(conf.getString(JobConstant.KEY_SQL))
          .setPassword(
              RsaUtil.decryptDataOnJava(
                  params.getString(JobConstant.KEY_PASSWORD), IotWorkRuntimeDecodeContext.getPrivateKey()))
          .setTable(conf.getString(JobConstant.KEY_TABLE))
          .setLimit(
              conf.getInteger(JobConstant.KEY_LIMIT) == null
                  ? JobConstant.LIMIT_DEFAULT
                  : conf.getInteger(JobConstant.KEY_LIMIT));

      PostgresqlBatchSourceWrapper result = builder.build();
      logger.debug("Decoding Postgresql source: {}", result);
      return result;
    } catch (Exception ex) {
      logger.error("Exception occurred when decoding Postgresql source: {}", conf);
      throw new DecodeSourceException(conf.getString(JobConstant.KEY_NODE_ID), conf.toJSONString());
    }
  }
}
