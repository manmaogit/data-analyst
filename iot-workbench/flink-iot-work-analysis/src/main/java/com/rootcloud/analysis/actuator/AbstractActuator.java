/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.actuator;

import com.rootcloud.analysis.core.exec.Actuator;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public abstract class AbstractActuator implements Actuator {

  private String jobName;

  private String jobType;

}
