/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.decoder.processor.impl;

import com.alibaba.fastjson.JSONObject;
import com.rootcloud.analysis.core.constants.JobConstant;
import com.rootcloud.analysis.core.enums.ProcessTypeEnum;
import com.rootcloud.analysis.decoder.processor.AbstractProcessorDecoder;
import com.rootcloud.analysis.decoder.processor.ProcessorDecoder;
import com.rootcloud.analysis.exception.DecodeProcessorException;
import com.rootcloud.analysis.wrapper.processor.AbstractProcessorWrapper;
import com.rootcloud.analysis.wrapper.processor.SchemaProcessor02Wrapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@ProcessorDecoder(processType = ProcessTypeEnum.SCHEMA_02)
public class SchemaProcessor02Decoder extends AbstractProcessorDecoder {

  private static Logger logger = LoggerFactory.getLogger(
      SchemaProcessor02Decoder.class);

  @Override
  public AbstractProcessorWrapper decodeProcessParams(JSONObject conf) {
    try {
      return SchemaProcessor02Wrapper.builder()
          .schema(conf.getJSONArray(JobConstant.KEY_SCHEMA))
          .build();
    } catch (Exception ex) {
      logger.error("Exception occurred when decoding schema processor 02: {}", conf);
      throw new DecodeProcessorException();
    }
  }

}
