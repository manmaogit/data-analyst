/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.wrapper.jdbc;

import com.alibaba.fastjson.JSONArray;
import com.google.common.collect.Maps;
import com.rootcloud.analysis.common.entity.RcUnit;
import com.rootcloud.analysis.common.jdbc.ConnectionWrapper;
import com.rootcloud.analysis.exception.SourceReadDataException;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class JdbcDynamicConnWrapper implements IJdbcWrapper {

  private static final Logger logger = LoggerFactory.getLogger(JdbcDynamicConnWrapper.class);

  private static final long serialVersionUID = 1L;
  private ConnectionWrapper connectionWrapper;

  private final String table;
  private final JSONArray schema;
  private final String url;

  private final String countSql;
  private final String selectSql;

  private final int fetchSize = 1000;

  /**
   * 构造方法.
   */
  public JdbcDynamicConnWrapper(String driverClassName, String url, String username,
                                String password, String validationQuery, String table,
                                JSONArray schema, String countSql, String selectSql) {
    connectionWrapper = new ConnectionWrapper(driverClassName, url, username, password,
        validationQuery);
    this.table = table;
    this.schema = schema;
    this.url = url;
    this.countSql = countSql;
    this.selectSql = selectSql;
  }

  /**
   * 获取schema.
   */
  public JSONArray getSchema() {
    return this.schema;
  }

  /**
   * 获取table.
   */
  public String getTable() {
    return this.table;
  }

  /**
   * 获取url.
   */
  public String getUrl() {
    return this.url;
  }

  /**
   * 获取JDBC连接.
   */
  public ConnectionWrapper getConnectionWrapper() {
    return connectionWrapper;
  }

  /**
   * 销毁连接.
   */
  @Override
  public void close() {
    connectionWrapper.close();
    connectionWrapper = null;
  }

  /**
   * 查询数据.
   */
  @Override
  public Set<RcUnit> getData() throws Exception {
    Set<RcUnit> outPutSet = new HashSet<>();
    try {
      Statement statement = getConnectionWrapper().createStatement();
      statement.setFetchSize(fetchSize);
      statement.execute(selectSql);
      ResultSet resultSet = statement.getResultSet();
      if (resultSet != null) {
        ResultSetMetaData resultSetMetaData = resultSet.getMetaData();
        while (resultSet.next()) {
          Map<String, Object> data = Maps.newHashMap();
          for (int i = 1; i <= resultSetMetaData.getColumnCount(); i++) {
            String column = resultSetMetaData.getColumnName(i);
            resultSetMetaData.getColumnTypeName(i);
            data.put(column, resultSet.getObject(column));
          }
          if (getSchema() != null && !getSchema().isEmpty()) {
            outPutSet.add(RcUnit.valueOfDataTypeInfo(data, getSchema()));
          } else {
            outPutSet.add(RcUnit.valueOf(data));
          }
        }
      }
    } catch (Exception ex) {
      logger.error("JdbcDynamicConnWrapper getData Exception : ", ex);
      throw new SourceReadDataException(
          "getData from " + table + "failed：" + ex
              + "， URI：" + url + "，Table：" + table);
    }
    return outPutSet;
  }
}
