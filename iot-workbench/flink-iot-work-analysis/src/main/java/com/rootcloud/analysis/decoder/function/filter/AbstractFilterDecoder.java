/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.decoder.function.filter;

import com.alibaba.fastjson.JSONObject;
import com.rootcloud.analysis.decoder.IDecoder;
import com.rootcloud.analysis.wrapper.function.filter.AbstractFilterWrapper;

public abstract class AbstractFilterDecoder implements IDecoder {

  /**
   * decode filter config.
   *
   * @param config config
   * @return filter wrapper
   */
  public abstract AbstractFilterWrapper decode(JSONObject config);

}
