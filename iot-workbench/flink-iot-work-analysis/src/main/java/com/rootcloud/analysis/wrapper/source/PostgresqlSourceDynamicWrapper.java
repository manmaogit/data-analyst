/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.wrapper.source;

import com.rootcloud.analysis.common.context.IotWorkRuntimeContext;
import com.rootcloud.analysis.common.entity.RcUnit;
import com.rootcloud.analysis.core.constants.GrayLogConstant;
import com.rootcloud.analysis.core.constants.ShortMessageConstant;
import com.rootcloud.analysis.core.enums.GrayLogResultEnum;

import com.rootcloud.analysis.core.graylog.LogVo;
import com.rootcloud.analysis.log.loggingservice.LoggingServiceUtil;
import com.rootcloud.analysis.wrapper.function.jdbc.JdbcDynamicRichSourceFunction;
import com.rootcloud.analysis.wrapper.jdbc.JdbcDynamicConnWrapper;
import com.rootcloud.analysis.wrapper.jdbc.PostgresqlDynamicConnWrapper;
import java.util.Set;
import lombok.Getter;
import lombok.ToString;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.source.SourceFunction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Please use PostgresqlBatchSourceWrapper instead.
 */
@Deprecated
@Getter
@ToString(callSuper = true)
public class PostgresqlSourceDynamicWrapper extends JdbcSourceDynamicWrapper {

  private final Logger logger =
      LoggerFactory.getLogger(PostgresqlSourceDynamicWrapper.class);

  // SQL的表名、变量名使用的引号.
  private final String keyQuote = "\"";
  // SQL的值使用的引号.
  private final String valueQuote = "'";

  public PostgresqlSourceDynamicWrapper(Builder builder) {
    super(builder);
  }

  public static class Builder extends JdbcSourceDynamicWrapper.Builder {

    @Override
    public PostgresqlSourceDynamicWrapper build() {
      return new PostgresqlSourceDynamicWrapper(this);
    }
  }

  @Override
  public void registerInput(IotWorkRuntimeContext context) {
    String countSql = buildCountSql(keyQuote, valueQuote);
    String selectSql = buildSelectSql(keyQuote, valueQuote);
    logger.info("getData countSql : {}, selectSql : {}", countSql, selectSql);

    PostgresqlDynamicConnWrapper postgresqlDynamicConnWrapper =
        new PostgresqlDynamicConnWrapper("org.postgresql.Driver",
            getUri(), getUsername(), getPassword(), "select 1",
            getTable(), getSchema(), countSql, selectSql);
    context.putSourceConnToCache(getNodeId(), postgresqlDynamicConnWrapper);

    StreamExecutionEnvironment env = context.getExecutionEnv();

    SourceFunction sourceFunction = new PostgresqlDynamicSourceFunction(
        postgresqlDynamicConnWrapper, getKafkaLoggingServiceServers(),
        getKafkaLoggingServiceTopics());

    SingleOutputStreamOperator<RcUnit> sourceStream = env
        .addSource(sourceFunction)
        .uid(getNodeId())
        .setParallelism(1)
        .name(getNodeName());
    context.putObjectToContainer(getNodeId(), sourceStream);
  }

  private class PostgresqlDynamicSourceFunction extends JdbcDynamicRichSourceFunction<RcUnit> {

    private final String kafkaLoggingServiceServers;
    private final String kafkaLoggingServiceTopics;

    /**
     * 构造方法.
     */
    public PostgresqlDynamicSourceFunction(
        JdbcDynamicConnWrapper jdbcDynamicConnWrapper, String kafkaLoggingServiceServers,
        String kafkaLoggingServiceTopics) {
      super(jdbcDynamicConnWrapper);
      this.kafkaLoggingServiceServers = kafkaLoggingServiceServers;
      this.kafkaLoggingServiceTopics = kafkaLoggingServiceTopics;
    }

    @Override
    public void open(Configuration parameters) throws Exception {
      super.open(parameters);
      openProducer(kafkaLoggingServiceServers);
    }

    @Override
    public void run(SourceContext<RcUnit> ctx) {
      try {
        Set<RcUnit> result = null;
        if (!Thread.currentThread().isInterrupted()) {
          result = jdbcDynamicConnWrapper.getData();
        }
        if (!Thread.currentThread().isInterrupted() && result != null) {
          for (RcUnit rcUnit : result) {
            if (!Thread.currentThread().isInterrupted()) {
              ctx.collect(rcUnit);
            }
          }
        }
      } catch (Exception ex) {
        logger.error("Exception occurred when reading data from mysql: {}",
            ex.getMessage(), ex);
        LoggingServiceUtil.alarm(LogVo.builder()
            .k8sServiceName(GrayLogConstant.K8S_SERVICE_NAME)
            .module(GrayLogConstant.MODULE_NAME)
            .userName(getTenantId())
            .tenantId(getTenantId())
            .operateObjectName(getJobName())
            .operateObject(getJobId())
            .shortMessage(String.format("Exception occurred in node %s when reading data from"
                + " postgresql table:%s. Detail: %s",getNodeName(),getTable(), ex.toString()))
            .operation(ShortMessageConstant.READ_POSTGRESQL_DATA)
            .requestId(String.valueOf(System.currentTimeMillis()))
            .result(GrayLogResultEnum.FAIL.getValue())
            .logType(getLogType())
            .kafkaLoggingServiceTopic(kafkaLoggingServiceTopics)
            .kafkaLoggingServiceServers(kafkaLoggingServiceServers)
            .build(), getFlinkKafkaInternalProducer());
      }
      logger.info("PostgreSql source is over,URI：{}，Table：{}", getUri(), getTable());
    }


    @Override
    public void cancel() {
      logger.info("PostgreSql source is canceled.");
      LoggingServiceUtil.info(LogVo.builder()
          .k8sServiceName(GrayLogConstant.K8S_SERVICE_NAME)
          .module(GrayLogConstant.MODULE_NAME)
          .userName(getTenantId())
          .userId(getUserId())
          .tenantId(getTenantId())
          .operateObjectName(getJobName())
          .operateObject(getJobId())
          .shortMessage(String.format("Node %s disconnected from PostgreSQL. "
                  + "Database connection string: %s. Table: %s.",getNodeName(),getUri(),
              getTable()))
          .operation(ShortMessageConstant.CLOSE_POSTGRESQL)
          .requestId(String.valueOf(System.currentTimeMillis()))
          .result(GrayLogResultEnum.SUCCESS.getValue())
          .logType(getLogType())
          .kafkaLoggingServiceTopic(kafkaLoggingServiceTopics)
          .kafkaLoggingServiceServers(kafkaLoggingServiceServers)
          .build(), getFlinkKafkaInternalProducer());

    }

    @Override
    public void close() throws Exception {
      logger.info("PostgreSql source is closed.");
      LoggingServiceUtil.info(LogVo.builder()
          .k8sServiceName(GrayLogConstant.K8S_SERVICE_NAME)
          .module(GrayLogConstant.MODULE_NAME)
          .userName(getTenantId())
          .userId(getUserId())
          .tenantId(getTenantId())
          .operateObjectName(getJobName())
          .operateObject(getJobId())
          .shortMessage(String.format("Node %s disconnected from PostgreSQL."
                  + " Database connection string: %s. Table: %s.",getNodeName(),getUri(),
              getTable()))
          .operation(ShortMessageConstant.CLOSE_POSTGRESQL)
          .requestId(String.valueOf(System.currentTimeMillis()))
          .result(GrayLogResultEnum.SUCCESS.getValue())
          .logType(getLogType())
          .kafkaLoggingServiceTopic(kafkaLoggingServiceTopics)
          .kafkaLoggingServiceServers(kafkaLoggingServiceServers)
          .build(), getFlinkKafkaInternalProducer());
      super.close();
    }
  }

  ;
}
