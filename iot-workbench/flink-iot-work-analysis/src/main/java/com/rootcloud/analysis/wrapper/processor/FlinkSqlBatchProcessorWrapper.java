/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.wrapper.processor;

import com.alibaba.fastjson.JSONArray;
import com.google.common.collect.Maps;
import com.rootcloud.analysis.common.context.IotWorkRuntimeContext;
import com.rootcloud.analysis.common.entity.RcUnit;
import com.rootcloud.analysis.common.factory.KafkaLogMiddlewareResourceFactory;
import com.rootcloud.analysis.context.IotWorkRuntimeBuildContext;
import com.rootcloud.analysis.core.constants.GrayLogConstant;
import com.rootcloud.analysis.core.constants.ShortMessageConstant;
import com.rootcloud.analysis.core.enums.GrayLogResultEnum;
import com.rootcloud.analysis.core.enums.GraylogLogTypeEnum;
import com.rootcloud.analysis.core.graylog.LogVo;
import com.rootcloud.analysis.log.loggingservice.LoggingServiceUtil;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.functions.ProcessFunction;
import org.apache.flink.streaming.connectors.kafka.internals.FlinkKafkaInternalProducer;
import org.apache.flink.table.api.Table;
import org.apache.flink.table.api.bridge.java.StreamTableEnvironment;
import org.apache.flink.types.Row;
import org.apache.flink.util.Collector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Getter
@ToString(callSuper = true)
public class FlinkSqlBatchProcessorWrapper extends AbstractProcessorWrapper {

    private final Logger logger = LoggerFactory.getLogger(FlinkSqlBatchProcessorWrapper.class);

    private final String sql;

    private final JSONArray schema;

    private final JSONArray priorNodeIds;

    private FlinkSqlBatchProcessorWrapper(Builder builder) {
        this.schema = builder.schema;
        this.sql = builder.sql;
        this.priorNodeIds = builder.priorNodeIds;
    }

    /**
     * Resets the sequence.
     */
    @Override
    public void resetSequence(Map<String, AbstractProcessorWrapper> processorWrapperMap) {
        priorNodeIds.forEach(priorNodeId -> {
            AbstractProcessorWrapper priorProcessorWrapper = processorWrapperMap.get(priorNodeId);
            if (priorProcessorWrapper != null) {
                priorProcessorWrapper.setSequence(Math.min(getSequence() - 1, priorProcessorWrapper.getSequence()));
                priorProcessorWrapper.resetSequence(processorWrapperMap);
            }
        });
    }

    @ToString
    public static class Builder {

        private JSONArray schema;

        private String sql;

        private JSONArray priorNodeIds;

        public Builder setSchema(JSONArray schema) {
            this.schema = schema;
            return this;
        }

        public Builder setSql(String sql) {
            this.sql = sql;
            return this;
        }

        public Builder setPriorNodeIds(JSONArray priorNodeIds) {
            this.priorNodeIds = priorNodeIds;
            return this;
        }

        public FlinkSqlBatchProcessorWrapper build() {
            return new FlinkSqlBatchProcessorWrapper(this);
        }

    }

    private String getTemporaryViewName() {
        return getNodeId();
    }

    @Override
    public void registerProcess(IotWorkRuntimeContext context) throws Exception {
      StreamTableEnvironment tableEnv = context.getTableEnvFromCache();
      Configuration configuration = tableEnv.getConfig().getConfiguration();
      configuration.setInteger("table.exec.resource.default-parallelism", IotWorkRuntimeBuildContext.getSqlParallelism());
      // execute sql query and get sql result stream
      Table table = tableEnv.sqlQuery(sql);

      logger.debug("registerProcess successfully execute sql query: \"{}\"", sql);
      DataStream<RcUnit> dataStream = tableEnv.toChangelogStream(table)
              .process(new FlinkSqlRow2RcUnitFlatMap(getJobId(), getTenantId(),
                      getJobName(), getNodeId(), true,
                      Arrays.asList(table.getSchema().getFieldNames()),
                      getKafkaLoggingServiceServers(),
                      getKafkaLoggingServiceTopics(), null
      )).uid(getNodeId()).name(getNodeName());
      context.putObjectToContainer(getNodeId(), dataStream);

      tableEnv.createTemporaryView(getNodeId(), table);
    }

    @AllArgsConstructor
    private class FlinkSqlRow2RcUnitFlatMap extends ProcessFunction<Row, RcUnit> {

        private String jobId;
        private String tenantId;
        private String jobName;
        private String nodeId;

        /**
         * Flag of whether to keep the retracted rows. This should always be set to false except for a
         * few cases.
         */
        private final boolean keepRetractedRow;

        /**
         * The field names of the incoming Row data.
         */
        private final List<String> fieldList;

        private final String kafkaLoggingServiceServers;
        private final String kafkaLoggingServiceTopics;
        private FlinkKafkaInternalProducer flinkKafkaInternalProducer;

        @Override
        public void open(Configuration parameters) throws Exception {
            super.open(parameters);
            if (flinkKafkaInternalProducer == null && kafkaLoggingServiceServers != null) {
                flinkKafkaInternalProducer = KafkaLogMiddlewareResourceFactory.getProducer(kafkaLoggingServiceServers, this.toString());
            }

        }

        @Override
        public void close() throws Exception {
            super.close();
            KafkaLogMiddlewareResourceFactory.close(this.toString());
            flinkKafkaInternalProducer = null;
        }

        @Override
        public void processElement(Row value, ProcessFunction<Row, RcUnit>.Context ctx, Collector<RcUnit> out) throws Exception {
            if (value == null) {
                return;
            }
            if (!keepRetractedRow) {
                return;
            }
            Map<String, Object> map = Maps.newHashMap();
            try {
                for (int i = 0; i < fieldList.size(); i++) {
                    String key = fieldList.get(i);
                    Object val = value.getField(i);
                    if (val instanceof LocalDateTime) {
                        val = Timestamp.valueOf((LocalDateTime) val);
                    }
                    map.put(key, val);
                }
                // 不做Schema转换.
                RcUnit result = RcUnit.valueOf(map);
                logger.debug("FlinkSql RcUnit output: {}", result);
                if (!Thread.currentThread().isInterrupted()) {
                    out.collect(result);
                }
            } catch (Exception ex) {
                logger.error("Exception occurred when processing sql result: {}", ex);
                LoggingServiceUtil.error(LogVo.builder().k8sServiceName(GrayLogConstant.K8S_SERVICE_NAME)
                        .module(GrayLogConstant.MODULE_NAME).userName(tenantId).userId(getUserId()).tenantId(tenantId)
                        .operateObjectName(jobName).operateObject(jobId).shortMessage("flink sql node（" + nodeId + "） Exception failed" + ex.toString())
                        .operation(ShortMessageConstant.PROCESS_SQL_RESULT)
                        .requestId(String.valueOf(System.currentTimeMillis())).result(GrayLogResultEnum.FAIL.getValue())
                        .logType(GraylogLogTypeEnum.OFFLINE).kafkaLoggingServiceTopic(kafkaLoggingServiceTopics)
                        .kafkaLoggingServiceServers(kafkaLoggingServiceServers).build(), flinkKafkaInternalProducer);
            }
        }

    }
}
