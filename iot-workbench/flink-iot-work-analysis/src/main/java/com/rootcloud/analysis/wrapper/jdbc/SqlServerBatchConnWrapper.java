/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.wrapper.jdbc;

import com.alibaba.fastjson.JSONArray;
import com.google.common.collect.Maps;
import com.rootcloud.analysis.common.entity.RcUnit;
import com.rootcloud.analysis.common.util.DateUtil;
import com.rootcloud.analysis.context.IotWorkRuntimeDecodeContext;
import com.rootcloud.analysis.core.constants.JobConstant;
import com.rootcloud.analysis.core.enums.SqlModeEnum;
import com.rootcloud.analysis.core.enums.TimeZoneTypeEnum;
import com.rootcloud.analysis.exception.SourceReadDataException;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.time.OffsetDateTime;
import java.util.Date;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SqlServerBatchConnWrapper extends JdbcConnWrapper {

  private static final Logger logger = LoggerFactory.getLogger(SqlServerBatchConnWrapper.class);

  private SqlModeEnum sqlMode;
  private String tableSql;
  private String selectSql;

  /**
   * 构造方法.
   */
  public SqlServerBatchConnWrapper(String driverClassName, String url, String username,
                                   String password, String validationQuery, String table,
                                   JSONArray schema, Integer limit, String filterSql,
                                   String tableSql,
                                   SqlModeEnum sqlMode) {
    super(driverClassName, url, username, password, validationQuery, table, schema, limit,
        filterSql);
    this.tableSql = tableSql;
    this.sqlMode = sqlMode;
    this.selectSql = SqlModeEnum.TABLE.equals(sqlMode) ? buildSelectSql() : buildTableSql();
  }

  @Override
  public String buildCountSql() {
    return "select count(1) from  " + "\"" + getTable() + "\"";
  }


  /**
   * 需要支持TABLE模式后实现.
   */
  @Override
  public String buildSelectSql() {
    //需要支持TABLE模式后实现
    return null;
  }

  /**
   * 适用于通用的SQL格式化操作.
   */
  protected String getFormattedWhereSql(String whereSql, String valueQuote) {
    if (StringUtils.isBlank(whereSql)) {
      return "";
    }
    if (StringUtils.isNotBlank(whereSql)) {
      // 封装系统变量值.
      if (whereSql.contains(JobConstant.SYSTEM_ZONE_ID)
          && StringUtils.isNotBlank(IotWorkRuntimeDecodeContext.getTimezone())) {
        // 时区ID.
        whereSql = whereSql.replaceAll(JobConstant.SYSTEM_ZONE_ID, valueQuote
            + TimeZoneTypeEnum.valueOf(IotWorkRuntimeDecodeContext.getTimezone()).getTimeZoneValue()
            + valueQuote);
      }
      if (whereSql.contains(JobConstant.SYSTEM_TIME_ZONE)
          && StringUtils.isNotBlank(IotWorkRuntimeDecodeContext.getTimezone())) {
        // 时区.
        whereSql = whereSql.replaceAll(JobConstant.SYSTEM_TIME_ZONE, valueQuote
            + IotWorkRuntimeDecodeContext.getTimezone()
            + valueQuote);
      }

      if (whereSql.contains(JobConstant.SYS_DATE_FUNC_KEY)) {
        // 时区.
        whereSql = whereSql.replaceAll(JobConstant.SYS_DATE_FUNC_KEY_REG,
            valueQuote + new SimpleDateFormat("yyyy-MM-dd").format(new Date()) + valueQuote);
      }
      if (whereSql.contains(JobConstant.SYSTEM_BIZ_START_DATETIME_FUNC_KEY)
          && IotWorkRuntimeDecodeContext.getTimeScope() != null
          && StringUtils.isNotBlank(IotWorkRuntimeDecodeContext.getTimeScope().getStartTime())) {
        // 业务开始时间.
        whereSql = whereSql.replaceAll(JobConstant.SYSTEM_BIZ_START_DATETIME_FUNC_KEY_REG,
            valueQuote
                + IotWorkRuntimeDecodeContext.getTimeScope().getStartTime() + valueQuote);
      }
      if (whereSql.contains(JobConstant.SYSTEM_BIZ_END_DATETIME_FUNC_KEY)
          && IotWorkRuntimeDecodeContext.getTimeScope() != null
          && StringUtils.isNotBlank(IotWorkRuntimeDecodeContext.getTimeScope().getEndTime())) {
        // 业务结束时间.
        whereSql = whereSql.replaceAll(JobConstant.SYSTEM_BIZ_END_DATETIME_FUNC_KEY_REG,
            valueQuote
                + IotWorkRuntimeDecodeContext.getTimeScope().getEndTime() + valueQuote);
      }
      if (whereSql.contains(JobConstant.SYSTEM_DATETIME_FUNC_KEY)
          && StringUtils.isNotBlank(IotWorkRuntimeDecodeContext.getSystemDatetime())
          && StringUtils.isNotBlank(IotWorkRuntimeDecodeContext.getTimezone())) {
        // 计划调度时间.
        whereSql = whereSql.replaceAll(JobConstant.SYSTEM_DATETIME_FUNC_KEY_REG,
            valueQuote + DateUtil
                .formatTime(DateUtil.getEpochSecond(IotWorkRuntimeDecodeContext.getSystemDatetime(),
                        "yyyyMMddHHmmss", OffsetDateTime.now().getOffset().getId()) * 1000,
                    TimeZoneTypeEnum.valueOf(IotWorkRuntimeDecodeContext.getTimezone()).getZoneId(),
                    "yyyy-MM-dd HH:mm:ss") + valueQuote);
      }
      if (whereSql.contains(JobConstant.SYS_DATE)) {
        // 时区.
        whereSql = whereSql.replaceAll(JobConstant.SYS_DATE,
            valueQuote + new SimpleDateFormat("yyyy-MM-dd").format(new Date())
                + valueQuote);
      }

      if (whereSql.contains(JobConstant.SYSTEM_BIZ_START_DATETIME)
          && IotWorkRuntimeDecodeContext.getTimeScope() != null
          && StringUtils.isNotBlank(IotWorkRuntimeDecodeContext.getTimeScope().getStartTime())) {
        // 业务开始时间.
        whereSql = whereSql.replaceAll(JobConstant.SYSTEM_BIZ_START_DATETIME, valueQuote
            + IotWorkRuntimeDecodeContext.getTimeScope().getStartTime() + valueQuote);
      }
      if (whereSql.contains(JobConstant.SYSTEM_BIZ_END_DATETIME)
          && IotWorkRuntimeDecodeContext.getTimeScope() != null
          && StringUtils.isNotBlank(IotWorkRuntimeDecodeContext.getTimeScope().getEndTime())) {
        // 业务结束时间.
        whereSql = whereSql.replaceAll(JobConstant.SYSTEM_BIZ_END_DATETIME, valueQuote
            + IotWorkRuntimeDecodeContext.getTimeScope().getEndTime() + valueQuote);
      }
      if (whereSql.contains(JobConstant.SYSTEM_DATETIME)
          && StringUtils.isNotBlank(IotWorkRuntimeDecodeContext.getSystemDatetime())
          && StringUtils.isNotBlank(IotWorkRuntimeDecodeContext.getTimezone())) {
        // 计划调度时间.
        whereSql = whereSql.replaceAll(JobConstant.SYSTEM_DATETIME, valueQuote + DateUtil
            .formatTime(DateUtil.getEpochSecond(IotWorkRuntimeDecodeContext.getSystemDatetime(),
                    "yyyyMMddHHmmss", OffsetDateTime.now().getOffset().getId()) * 1000,
                TimeZoneTypeEnum.valueOf(IotWorkRuntimeDecodeContext.getTimezone()).getZoneId(),
                "yyyy-MM-dd HH:mm:ss") + valueQuote);
      }

      // 去掉结尾的分号
      whereSql = whereSql.replaceAll(";", "");
    }
    return whereSql;
  }

  /**
   * 处理 tableSql.
   */
  public String buildTableSql() {

    String formattedSql = getFormattedWhereSql(tableSql, "'");
    formattedSql = formattedSql.replaceAll("\\n", " ");
    formattedSql = formattedSql.replaceAll("\\t", " ");
    formattedSql = formattedSql.replaceAll("\\\\n", " ");
    StringBuilder selectSql = new StringBuilder();
    selectSql.append(formattedSql);
    if (!hasComplexFunction(formattedSql)) {
      selectSql = surroundSqlWithLimit(selectSql);
    } else {
      selectSql = new StringBuilder(surroundComplexSqlWithLimit(selectSql.toString()));
    }
    return selectSql.toString();
  }

  /**
   * 为复杂sql添加limit限制，默认是WITH ... AS 语句.
   */
  private String surroundComplexSqlWithLimit(String selectSql) {
    if (!Pattern.compile("\\s*WITH\\s+\\d?\\w+\\d?\\s+AS(\\s+|[(])")
        .matcher(selectSql.toString().toUpperCase())
        .find()) {
      return selectSql;
    }
    Integer index = 0;
    String subString = "";
    while (!Pattern.compile("^\\s+SELECT\\s+")
        .matcher(subString.toUpperCase()).find()) {
      index += getWithIndex(selectSql.toString().substring(index));
      subString = selectSql.toString().substring(index);
    }
    String start = selectSql.toString().substring(0, index);
    String after = selectSql.toString().substring(index);
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(start);
    stringBuilder.append(" select   ");
    stringBuilder.append(" top   ");
    stringBuilder.append(getLimit());
    stringBuilder.append(" * ");
    stringBuilder.append("    from ( ");
    stringBuilder.append(after);
    stringBuilder.append(") iotworks_a");
    return stringBuilder.toString();
  }


  private Integer getWithIndex(String selectSql) {
    Integer leftBracket = 0;
    Integer rightBracket = 0;
    Integer difference = 0;
    Integer index = 0;
    byte[] sqlByte = selectSql.toString().getBytes();
    do {
      if (sqlByte[index] == '(') {
        leftBracket++;
        difference = leftBracket - rightBracket;
      } else if (sqlByte[index] == ')') {
        rightBracket++;
        difference = leftBracket - rightBracket;
      }
      index++;
    } while (!(leftBracket > 0 && difference == 0));
    return index;
  }

  /**
   * 为sql添加limit限制.
   */
  private StringBuilder surroundSqlWithLimit(StringBuilder selectSql) {
    StringBuilder limitSql = new StringBuilder();
    limitSql.append(" select   ");
    limitSql.append(" top   ");
    limitSql.append(getLimit());
    limitSql.append(" * ");
    limitSql.append("    from ( ");
    limitSql.append(selectSql.toString());
    limitSql.append(") iotworks_a");
    return limitSql;
  }

  @Override
  public Set<RcUnit> getData() throws Exception {
    Set<RcUnit> outPutSet = new HashSet<>();
    try (Statement statement = getConnectionWrapper().createStatement()) {

      logger.debug("Try to execute sql: {}", selectSql);
      try (ResultSet resultSet = statement.executeQuery(selectSql)) {
        ResultSetMetaData resultSetMetaData = resultSet.getMetaData();
        while (resultSet.next()) {
          Map<String, Object> data = Maps.newHashMap();
          for (int i = 1; i <= resultSetMetaData.getColumnCount(); i++) {
            String column = resultSetMetaData.getColumnName(i);
            resultSetMetaData.getColumnTypeName(i);
            data.put(column, resultSet.getObject(column));
          }
          if (getSchema() != null && !getSchema().isEmpty()) {
            outPutSet.add(RcUnit.valueOfDataTypeInfo(data, getSchema()));
          } else {
            outPutSet.add(RcUnit.valueOf(data));
          }
        }

      }
    } catch (Exception ex) {
      logger.error("Exception occurred when reading data from SqlServer: {}",
          ex.getMessage(), ex);
      throw new SourceReadDataException(
          "Read SqlServer data table" + getTable() + "fail：" + ex.toString()
              + "，URI：" + getUrl() + "，Table："
              + getTable() + " , selectSql: " + selectSql);
    }
    return outPutSet;
  }

  /**
   * 检查sql中是否包含复杂函数，例如WITH.
   */
  public Boolean hasComplexFunction(String sql) {
    if (StringUtils.isBlank(sql)) {
      return false;
    }
    if (sql.toUpperCase().startsWith("WITH")) {
      return true;
    }
    Matcher matcher = Pattern.compile("\\s*WITH\\s+\\d?\\w+\\d?\\s+AS(\\s+|[(])")
        .matcher(sql.toUpperCase());
    return matcher.find();
  }
}
