/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.common.util;

import static com.rootcloud.analysis.core.constants.JobConstant.KEY_ABSTRACT_DEVICE_TYPE_IDS;
import static com.rootcloud.analysis.core.constants.JobConstant.KEY_DEVICE_MAPPINGS;
import static com.rootcloud.analysis.core.constants.JobConstant.KEY_HUB_SERVICE_URL;

import com.alibaba.fastjson.JSONObject;
import com.rootcloud.analysis.common.entity.DeviceMapping;
import com.rootcloud.analysis.core.constants.JobConstant;

import java.util.Collections;
import java.util.List;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

/**
 * 任务运行时动态获取抽象模型最新映射关系.
 *
 * @author: zexin.huang
 * @createdDate: 2022/7/1 16:19
 */
public class AbstractDeviceUtil {

  /**
   * 处理抽象模型与设备的映射关系.
   */
  public static List<DeviceMapping> getDeviceMappings(JSONObject conf) {
    JSONObject params = conf.getJSONObject(JobConstant.KEY_PARAMETERS);
    // 设备模型ID、抽象模型ID、设备ID集合 的映射关系.
    List<DeviceMapping> deviceMappings = JSONObject.parseArray(
        params.getString(KEY_DEVICE_MAPPINGS), DeviceMapping.class);

    if (params.containsKey(KEY_ABSTRACT_DEVICE_TYPE_IDS)
        && conf.containsKey(KEY_HUB_SERVICE_URL)) {
      List<String> abstractDeviceTypeIds =  JSONObject.parseArray(
          params.getString(KEY_ABSTRACT_DEVICE_TYPE_IDS), String.class);
      if (CollectionUtils.isNotEmpty(abstractDeviceTypeIds)) {
        String hubServiceUrl = conf.getString(KEY_HUB_SERVICE_URL);
        String tenantId = conf.getString(JobConstant.TENANT_ID);
        for (String abstractId : abstractDeviceTypeIds) {
          // 调用HUB接口，查询抽象模型下的模型列表.
          List<String> deviceTypeIds = HubUtil.getDeviceTypesAbstractDerive(
              hubServiceUrl, tenantId, abstractId);
          for (String deviceTypeId : deviceTypeIds) {
            if (!isExistsDeviceMapping(deviceMappings, deviceTypeId)) {
              deviceMappings.add(new DeviceMapping(
                  abstractId, deviceTypeId, Collections.emptyList()));
            }
          }
        }
      }
    }
    return deviceMappings;
  }

  /**
   * 判断是否已经绑定设备映射关系.
   *
   * @param deviceMappings 设备模型ID、抽象模型ID、设备ID集合 的映射关系.
   * @param deviceTypeId 设备模型ID.
   */
  private static boolean isExistsDeviceMapping(List<DeviceMapping> deviceMappings,
                                               String deviceTypeId) {
    for (DeviceMapping temp : deviceMappings) {
      if (StringUtils.equals(deviceTypeId, temp.getDeviceTypeId())) {
        return true;
      }
    }
    return false;
  }
}
