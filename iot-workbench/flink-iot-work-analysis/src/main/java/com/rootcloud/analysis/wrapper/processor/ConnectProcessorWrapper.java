/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.wrapper.processor;

import static com.rootcloud.analysis.core.constants.JobConstant.KEY_SOURCE_ATTRIBUTE;
import static com.rootcloud.analysis.core.constants.JobConstant.KEY_TARGET_ATTRIBUTE;
import static com.rootcloud.analysis.core.constants.SchemaConstant.ATTR_NAME;
import static com.rootcloud.analysis.core.enums.AttrSourceEnum.DIMENSION;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.rootcloud.analysis.common.context.IotWorkRuntimeContext;
import com.rootcloud.analysis.common.entity.RcUnit;
import com.rootcloud.analysis.common.factory.KafkaLogMiddlewareResourceFactory;
import com.rootcloud.analysis.context.IotWorkRuntimeDecodeContext;
import com.rootcloud.analysis.core.constants.GrayLogConstant;
import com.rootcloud.analysis.core.constants.SchemaConstant;
import com.rootcloud.analysis.core.constants.ShortMessageConstant;
import com.rootcloud.analysis.core.enums.GrayLogResultEnum;
import com.rootcloud.analysis.core.enums.GraylogLogTypeEnum;
import com.rootcloud.analysis.core.graylog.LogVo;
import com.rootcloud.analysis.exception.BuildProcessorException;
import com.rootcloud.analysis.log.loggingservice.LoggingServiceUtil;
import com.rootcloud.analysis.wrapper.IDataSourceWrapper;
import com.rootcloud.analysis.wrapper.WrapperBuilder;
import com.rootcloud.analysis.wrapper.function.join.AbstractJoinWrapper;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;
import lombok.Getter;
import lombok.ToString;
import org.apache.flink.api.common.state.BroadcastState;
import org.apache.flink.api.common.state.MapStateDescriptor;
import org.apache.flink.api.common.typeinfo.TypeHint;
import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.runtime.state.HeapBroadcastState;
import org.apache.flink.streaming.api.datastream.BroadcastStream;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.functions.co.BroadcastProcessFunction;
import org.apache.flink.streaming.connectors.kafka.internals.FlinkKafkaInternalProducer;
import org.apache.flink.util.Collector;
import org.apache.flink.util.Preconditions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Getter
@ToString(callSuper = true)
public class ConnectProcessorWrapper extends AbstractProcessorWrapper {

  private static Logger logger = LoggerFactory.getLogger(ConnectProcessorWrapper.class);

  private static final String DESCRIPTOR_NAME = "connect-target";

  private final String dimensionNodeId;

  private final JSONObject attributeMapper;

  private final List<JSONObject> attributeMapperList;

  private final JSONArray schema;

  private final AbstractJoinWrapper abstractJoinWrapper;

  private ConnectProcessorWrapper(Builder builder) {
    super(builder.jobId, builder.jobName, builder.tenantId, builder.nodeId,
        builder.parallelism, builder.nodeName, builder.priorNodeId, builder.processType,
        builder.logType);
    this.abstractJoinWrapper = builder.abstractJoinWrapper;
    this.dimensionNodeId = builder.dimensionNodeId;
    this.schema = builder.schema;
    this.attributeMapper = builder.attributeMapper;
    this.attributeMapperList = builder.attributeMapperList;
  }

  @ToString
  public static class Builder extends WrapperBuilder {

    private String nodeId;
    private String nodeName;
    private Integer parallelism;
    private String priorNodeId;
    private String processType;
    private JSONArray schema;
    private String dimensionNodeId;
    private JSONObject attributeMapper;
    private AbstractJoinWrapper abstractJoinWrapper;
    private List<JSONObject> attributeMapperList;

    public Builder setParallelism(Integer parallelism) {
      this.parallelism = parallelism;
      return this;
    }

    public Builder setPriorNodeId(String priorNodeId) {
      this.priorNodeId = priorNodeId;
      return this;
    }

    public Builder setProcessType(String processType) {
      this.processType = processType;
      return this;
    }

    public Builder setSchema(JSONArray schema) {
      this.schema = schema;
      return this;
    }

    public Builder setDimensionNodeId(String dimensionNodeId) {
      this.dimensionNodeId = dimensionNodeId;
      return this;
    }

    public Builder setAttributeMapper(JSONObject attributeMapper) {
      this.attributeMapper = attributeMapper;
      return this;
    }

    public Builder setAttributeMapperList(List<JSONObject> attributeMapperList) {
      this.attributeMapperList = attributeMapperList;
      return this;
    }

    public Builder setNodeName(String nodeName) {
      this.nodeName = nodeName;
      return this;
    }

    public Builder setAbstractJoinWrapper(AbstractJoinWrapper abstractJoinWrapper) {
      this.abstractJoinWrapper = abstractJoinWrapper;
      return this;
    }

    public ConnectProcessorWrapper build() {
      return new ConnectProcessorWrapper(this);
    }
  }


  @Override
  public void registerProcess(IotWorkRuntimeContext context) throws Exception {
    DataStreamSource<Set<RcUnit>> dimensionDataStream = context
        .getObjectFromContainer(dimensionNodeId);
    if (dimensionDataStream == null) {
      throw new BuildProcessorException();
    }
    IDataSourceWrapper sourceWrapper = context.getSourceConnCacheKey(dimensionNodeId);
    if (sourceWrapper == null) {
      throw new BuildProcessorException();
    }
    DataStream<RcUnit> priorDataStream = context.getObjectFromContainer(getPriorNodeId());
    if (priorDataStream == null) {
      throw new BuildProcessorException();
    }

    BroadcastStream<Set<RcUnit>> broadcastStream = context.getBroadcastStreamFromCache(dimensionNodeId);
    MapStateDescriptor<Object, List<RcUnit>> mapStateDescriptor = new MapStateDescriptor<>(
        DESCRIPTOR_NAME + dimensionNodeId,
        TypeInformation.of(Object.class), TypeInformation.of(new TypeHint<List<RcUnit>>() {
        }));
    if (broadcastStream == null) {
      broadcastStream = dimensionDataStream.broadcast(mapStateDescriptor);
      context.putBroadcastStreamToCache(dimensionNodeId, broadcastStream);
      logger.debug("Broadcasts data stream: {}", dimensionNodeId);
    }
    DataStream<RcUnit> connectedStream = priorDataStream.connect(broadcastStream).process(
            new BroadcastConnect(getJobId(),
                getJobName(),
                getTenantId(),
                attributeMapper,
                attributeMapperList,
                abstractJoinWrapper,
                getSchema(), sourceWrapper,
                IotWorkRuntimeDecodeContext.getJobType(), mapStateDescriptor, getKafkaLoggingServiceServers(),
                getKafkaLoggingServiceTopics())).uid(getNodeId())
        .returns(TypeInformation.of(new TypeHint<RcUnit>() {
        }
            )
        )
        .setParallelism(getParallelism()).name(getNodeName());

    context.putObjectToContainer(getNodeId(), connectedStream);
  }

  private static class BroadcastConnect
      extends BroadcastProcessFunction<RcUnit, Set<RcUnit>, RcUnit> {

    private final String jobId;
    private final String jobName;
    private final String tenantId;
    private final JSONObject attributeMapper;
    private final List<JSONObject> attributeMapperList;
    private final JSONArray schema;
    private final Set<String> dimensionAttrSet;
    private final IDataSourceWrapper sourceWrapper;
    private Map<Object, List<RcUnit>> cache = null;
    private final GraylogLogTypeEnum logType;
    private final AbstractJoinWrapper abstractJoinWrapper;
    private final MapStateDescriptor<Object, List<RcUnit>> connectTargetListDescriptor;
    private final String kafkaLoggingServiceServers;
    private final String kafkaLoggingServiceTopics;
    private FlinkKafkaInternalProducer flinkKafkaInternalProducer;


    public BroadcastConnect(String jobId, String jobName, String tenantId,
                            JSONObject attributeMapper,
                            List<JSONObject> attributeMapperList,
                            AbstractJoinWrapper abstractJoinWrapper,
                            JSONArray schema, IDataSourceWrapper sourceWrapper,
                            GraylogLogTypeEnum logType,
                            MapStateDescriptor<Object, List<RcUnit>> connectTargetListDescriptor,
                            String kafkaServer, String kafkaTopic) {

      this.jobName = jobName;
      this.connectTargetListDescriptor = connectTargetListDescriptor;
      Preconditions.checkArgument(attributeMapper != null && !attributeMapper.isEmpty(),
          "Attribute mapper is null or empty.");
      this.jobId = jobId;
      this.tenantId = tenantId;
      this.attributeMapper = attributeMapper;
      this.attributeMapperList = attributeMapperList;
      this.abstractJoinWrapper = abstractJoinWrapper;
      this.schema = schema;
      this.sourceWrapper = sourceWrapper;
      Preconditions.checkArgument(schema != null && !schema.isEmpty(), "Schema is null or empty.");
      dimensionAttrSet = schema.stream().filter(
          o -> ((JSONObject) o).getString(SchemaConstant.ATTR_SOURCE).equals(DIMENSION.toString()))
          .map(o -> ((JSONObject) o).getString(ATTR_NAME))
          .collect(Collectors.toSet());
      this.logType = logType;
      this.kafkaLoggingServiceServers = kafkaServer;
      this.kafkaLoggingServiceTopics = kafkaTopic;
    }

    @Override
    public void close() throws Exception {
      super.close();
      KafkaLogMiddlewareResourceFactory.close(this.toString());
      flinkKafkaInternalProducer = null;
    }

    @Override
    public void open(Configuration parameters) throws Exception {
      super.open(parameters);
      try {
        cache = sourceWrapper.getData().stream().filter(rc ->
            rc.getAttr(attributeMapper.getJSONObject(KEY_TARGET_ATTRIBUTE)
                .getString(ATTR_NAME)) != null).collect(Collectors
            .groupingBy(
                rc -> concatTargetGroupKey(rc)));
      } catch (Exception e) {
        logger.error("conn node open fail!!" + e.toString());
      }
      if (flinkKafkaInternalProducer == null && kafkaLoggingServiceServers != null) {
        flinkKafkaInternalProducer = KafkaLogMiddlewareResourceFactory.getProducer(
            kafkaLoggingServiceServers, this.toString());
      }


    }


    @Override
    public void processElement(RcUnit input, ReadOnlyContext ctx,
                               Collector<RcUnit> collector) throws Exception {
      try {
        HeapBroadcastState<Object, List<RcUnit>> dimensionState =
            (HeapBroadcastState) ctx.getBroadcastState(connectTargetListDescriptor);
        if (cache != null) {
          dimensionState.clear();
          dimensionState.putAll(cache);
          cache = null;
        }
        logger.debug("Connect processor element: {}", input);
        Object value = concatSourceGroupKey(input);
        if (abstractJoinWrapper.contain(dimensionState, value, attributeMapper)) {
          List<RcUnit> dimensionList = abstractJoinWrapper
              .join(dimensionState, input, attributeMapper, dimensionAttrSet, schema, value);
          for (RcUnit dimension : dimensionList) {

            logger.debug("Connect processor result: {}", dimension);
            if (!Thread.currentThread().isInterrupted()) {
              collector.collect(dimension);
            }
          }
        } else {
          logger.debug("Connect dimension data not found: {}, connect key: {}", input, value);

        }
      } catch (Exception ex) {
        logger.error("Connect error value: {}, ex: {}", input, ex.getMessage(), ex);
        LoggingServiceUtil.alarm(LogVo.builder()
            .k8sServiceName(GrayLogConstant.K8S_SERVICE_NAME)
            .module(GrayLogConstant.MODULE_NAME)
            .userName(tenantId)
            .tenantId(tenantId)
            .operateObjectName(jobName)
            .operateObject(jobId)
            .shortMessage(String.format("Join failure with the dimension data. Cause: %s. "
                + "Incoming record: %s", ex.toString(), input.toString()))
            .operation(ShortMessageConstant.FILTER_SCHEMA_DATA)
            .requestId(String.valueOf(System.currentTimeMillis()))
            .result(GrayLogResultEnum.FAIL.getValue())
            .logType(logType)
            .kafkaLoggingServiceTopic(kafkaLoggingServiceTopics)
            .kafkaLoggingServiceServers(kafkaLoggingServiceServers)
            .build(), flinkKafkaInternalProducer);
      }
    }


    @Override
    public void processBroadcastElement(Set<RcUnit> dimension, Context ctx,
                                        Collector<RcUnit> collector)
        throws Exception {
      BroadcastState<Object, List<RcUnit>> state = ctx.getBroadcastState(
          connectTargetListDescriptor);

      try {
        Map<String, List<RcUnit>> collect = dimension.stream()
            .filter(rc ->
                rc.getAttr(attributeMapper.getJSONObject(KEY_TARGET_ATTRIBUTE)
                    .getString(ATTR_NAME)) != null).collect(Collectors
                .groupingBy(
                    rc -> concatTargetGroupKey(rc)));
        Set<String> newKeySet = collect.keySet();
        Iterable<Entry<Object, List<RcUnit>>> oldEntry = state.entries();
        Set<String> oldKeySet = new HashSet<>();
        oldEntry.forEach(en -> {
          if (!newKeySet.contains((String) en.getKey())) {
            oldKeySet.add((String) en.getKey());
          }
        });
        for (String key : collect.keySet()) {
          state.put(key, collect.get(key));

        }
        for (String key : oldKeySet) {
          state.remove(key);
        }
      } catch (Exception e) {
        state.clear();
        logger.error("broadcast data err：{}", e.getMessage());
        LoggingServiceUtil.alarm(LogVo.builder()
            .k8sServiceName(GrayLogConstant.K8S_SERVICE_NAME)
            .module(GrayLogConstant.MODULE_NAME)
            .userName(tenantId)
            .tenantId(tenantId)
            .operateObjectName(jobName)
            .operateObject(jobId)
            .shortMessage(String.format("Broadcast data process failure. Cause: %s", e.toString()))
            .operation(ShortMessageConstant.PROCESS_BROADCAST_ELEMENT)
            .requestId(String.valueOf(System.currentTimeMillis()))
            .result(GrayLogResultEnum.FAIL.getValue())
            .logType(logType)
            .kafkaLoggingServiceTopic(kafkaLoggingServiceTopics)
            .kafkaLoggingServiceServers(kafkaLoggingServiceServers)
            .build(), flinkKafkaInternalProducer);
      }

    }

    /**
     * 拼接工况数据用于过滤.
     */
    public String concatSourceGroupKey(RcUnit input) {
      StringBuilder stringBuilder = null;
      if (attributeMapperList == null || attributeMapperList.isEmpty()) {
        return String
            .valueOf(input.getAttr(attributeMapper.getJSONObject(KEY_SOURCE_ATTRIBUTE)
                .getString(ATTR_NAME)));
      } else {
        stringBuilder = new StringBuilder();
        stringBuilder.append(input.getAttr(attributeMapper.getJSONObject(KEY_SOURCE_ATTRIBUTE)
            .getString(ATTR_NAME)));
        for (JSONObject jsonObject : attributeMapperList) {
          stringBuilder.append("|");
          stringBuilder.append(input.getAttr(jsonObject.getJSONObject(KEY_SOURCE_ATTRIBUTE)
              .getString(ATTR_NAME)));
        }
      }
      return stringBuilder.toString();
    }

    /**
     * 拼接维表数据用于过滤.
     */
    public String concatTargetGroupKey(RcUnit input) {
      StringBuilder stringBuilder = null;
      if (attributeMapperList == null || attributeMapperList.isEmpty()) {
        return String
            .valueOf(input.getAttr(attributeMapper.getJSONObject(KEY_TARGET_ATTRIBUTE)
                .getString(ATTR_NAME)));
      } else {
        stringBuilder = new StringBuilder();
        stringBuilder.append(input.getAttr(attributeMapper.getJSONObject(KEY_TARGET_ATTRIBUTE)
            .getString(ATTR_NAME)));
        for (JSONObject jsonObject : attributeMapperList) {
          stringBuilder.append("|");
          stringBuilder.append(input.getAttr(jsonObject.getJSONObject(KEY_TARGET_ATTRIBUTE)
              .getString(ATTR_NAME)));
        }
      }
      return stringBuilder.toString();
    }
  }
}

