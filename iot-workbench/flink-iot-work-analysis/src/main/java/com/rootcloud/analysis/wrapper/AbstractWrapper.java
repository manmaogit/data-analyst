/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.wrapper;

import com.rootcloud.analysis.core.enums.GraylogLogTypeEnum;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Getter
@Setter
@NoArgsConstructor
@ToString(callSuper = true)
public abstract class AbstractWrapper implements IWrapper {

  private static Logger logger = LoggerFactory.getLogger(AbstractWrapper.class);
  private GraylogLogTypeEnum jobType;

  private String jobId;

  private String jobName;

  private String tenantId;

  private String userId;

  private String nodeId;

  private String nodeName;

  private Integer parallelism;

  private String timeZone;

  private GraylogLogTypeEnum logType;

  public String kafkaLoggingServiceServers;
  public String kafkaLoggingServiceTopics;

  public void setKafkaLoggingServiceServers(String kafkaLoggingServiceServers) {
    logger.info("setKafkaLoggingServiceServers:" + kafkaLoggingServiceServers);
    this.kafkaLoggingServiceServers = kafkaLoggingServiceServers;
  }

  public void setKafkaLoggingServiceTopics(String kafkaLoggingServiceTopics) {
    logger.info("setKafkaLoggingServiceTopics:" + kafkaLoggingServiceTopics);
    this.kafkaLoggingServiceTopics = kafkaLoggingServiceTopics;
  }

  public String getKafkaLoggingServiceServers() {
    return kafkaLoggingServiceServers;
  }

  public String getKafkaLoggingServiceTopics() {
    return kafkaLoggingServiceTopics;
  }

  protected AbstractWrapper(String jobId, String jobName,
                            String tenantId, String nodeId,
                            Integer parallelism,
                            String nodeName, GraylogLogTypeEnum logType) {
    super();
    this.jobId = jobId;
    this.jobName = jobName;
    this.tenantId = tenantId;
    this.nodeId = nodeId;
    this.parallelism = parallelism;
    this.nodeName = nodeName;
    this.logType = logType;
  }

}
