/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.decoder.job;

import com.alibaba.fastjson.JSONObject;
import com.rootcloud.analysis.core.graph.IGraph;
import com.rootcloud.analysis.wrapper.IWrapper;

public abstract class AbstractJobDecoder {

  /**
   * decode.
   */
  public abstract IGraph<IWrapper> decode(JSONObject jobConf);
}
