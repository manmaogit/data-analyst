/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.decoder.source.impl;

import static com.rootcloud.analysis.core.constants.JobConstant.HTTP_REQUEST_PAGE_LIMIT;
import static com.rootcloud.analysis.core.constants.JobConstant.HTTP_REQUEST_TIMEOUT_SECONDS;
import static com.rootcloud.analysis.core.constants.JobConstant.KEY_PAGE_LIMIT;
import static com.rootcloud.analysis.core.constants.JobConstant.KEY_TIMEOUT_SECONDS;

import com.alibaba.fastjson.JSONObject;
import com.rootcloud.analysis.core.constants.JobConstant;
import com.rootcloud.analysis.core.constants.RestNodeConstants;
import com.rootcloud.analysis.core.enums.DataSourceTypeEnum;
import com.rootcloud.analysis.core.enums.MethodEnum;
import com.rootcloud.analysis.decoder.source.AbstractSourceDecoder;
import com.rootcloud.analysis.decoder.source.SourceDecoder;
import com.rootcloud.analysis.exception.DecodeSourceException;
import com.rootcloud.analysis.wrapper.source.AbstractSourceWrapper;
import com.rootcloud.analysis.wrapper.source.RestBatchSourceWrapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * IDF-设备台账-输入节点-离线任务.
 */
@SourceDecoder(dataSourceType = DataSourceTypeEnum.REST_BATCH)
public class RestBatchSourceDecoder extends AbstractSourceDecoder {

  private static Logger logger = LoggerFactory.getLogger(RestBatchSourceDecoder.class);


  @Override
  public AbstractSourceWrapper decodeSourceParams(JSONObject conf) {
    try {
      RestBatchSourceWrapper.Builder builder = new RestBatchSourceWrapper.Builder();

      builder.setUrl(conf.getString(RestNodeConstants.URL_KEY))
          .setMethod(MethodEnum.valueOf(conf.getString(RestNodeConstants.METHOD_KEY)))
          .setHeaders(conf.getJSONArray(RestNodeConstants.HEADERS_KEY))
          .setBody(conf.getJSONObject(RestNodeConstants.BODY_KEY))
          .setParameters(conf.getJSONArray(RestNodeConstants.PARAMETERS_KEY))
          .setRestApiInputSchema(conf.getJSONArray(RestNodeConstants.REST_API_INPUT_SCHEMA))
          .setTimeOutSeconds(conf.containsKey(KEY_TIMEOUT_SECONDS)
              ? conf.getInteger(KEY_TIMEOUT_SECONDS) : HTTP_REQUEST_TIMEOUT_SECONDS)
          .setPageLimit(conf.containsKey(KEY_PAGE_LIMIT)
              ? conf.getInteger(KEY_PAGE_LIMIT) : HTTP_REQUEST_PAGE_LIMIT);

      RestBatchSourceWrapper result = builder.build();
      logger.debug("Decoding rest api source: {}", result);
      return result;
    } catch (Exception ex) {
      logger.error("Exception occurred when decoding rest api source: {}", conf);
      throw new DecodeSourceException(conf.getString(JobConstant.KEY_NODE_ID),
          conf.toJSONString());
    }

  }
}
