/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.wrapper.sink;

import static com.rootcloud.analysis.core.constants.WrapperConstant.NULL_IDENTIFICATION;
import static com.rootcloud.analysis.core.constants.WrapperConstant.SPLIT_SEPARATOR;

import com.google.common.collect.Maps;
import com.rootcloud.analysis.common.context.IotWorkRuntimeContext;
import com.rootcloud.analysis.common.entity.RcUnit;
import com.rootcloud.analysis.context.IotWorkRuntimeDecodeContext;
import com.rootcloud.analysis.core.constants.CommonConstant;
import com.rootcloud.analysis.core.constants.GrayLogConstant;
import com.rootcloud.analysis.core.constants.JobConstant;
import com.rootcloud.analysis.core.constants.ShortMessageConstant;
import com.rootcloud.analysis.core.enums.GrayLogResultEnum;
import com.rootcloud.analysis.core.enums.GraylogLogTypeEnum;
import com.rootcloud.analysis.core.graylog.LogVo;
import com.rootcloud.analysis.log.loggingservice.LoggingServiceUtil;
import com.rootcloud.analysis.wrapper.function.jdbc.JdbcRichSinkFunction;
import com.rootcloud.analysis.wrapper.jdbc.MysqlConnWrapper;

import java.sql.PreparedStatement;
import java.util.List;
import java.util.Map;
import lombok.Getter;
import lombok.ToString;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.metrics.Counter;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Getter
@ToString(callSuper = true)
public class MysqlBatchSinkWrapper extends JdbcBatchSinkWrapper {

  private static Logger logger = LoggerFactory.getLogger(MysqlBatchSinkWrapper.class);

  private String buildInsertSql() {
    StringBuilder sqlBuilder = new StringBuilder();
    sqlBuilder.append("insert into ");
    sqlBuilder.append("`");
    sqlBuilder.append(getTable());
    sqlBuilder.append("`");
    sqlBuilder.append("(");
    StringBuilder paramBuilder = new StringBuilder();
    StringBuilder updateBuilder = new StringBuilder();
    for (int i = 0; i < getColumns().size(); i++) {
      if (i > 0) {
        sqlBuilder.append(CommonConstant.COMMA);
        paramBuilder.append(CommonConstant.COMMA);
        updateBuilder.append(CommonConstant.COMMA);
      }
      sqlBuilder.append("`");
      sqlBuilder.append(getColumns().get(i));
      sqlBuilder.append("`");
      paramBuilder.append("?");
      updateBuilder.append("`");
      updateBuilder.append(getColumns().get(i));
      updateBuilder.append("`");
      updateBuilder.append("=?");
    }
    sqlBuilder.append(") values(");
    sqlBuilder.append(paramBuilder);
    sqlBuilder.append(") on duplicate key update ");
    sqlBuilder.append(updateBuilder);
    return sqlBuilder.toString();
  }

  @Override
  public void registerOutput(IotWorkRuntimeContext context) {
    DataStream<RcUnit> priorDataStream = getPriorDataStream(context);
    priorDataStream.addSink(
            new MysqlBatchRichSinkFunction(getJobId(), getTenantId(), getUserId(), getUri(),
                getUsername(),
                getPassword(), getTable(), buildInsertSql(), getColumns(), getAttributeMapper(),
                IotWorkRuntimeDecodeContext.getJobType(), getPriorTableColIndex(),
                getInsertKey(), getBatchSize(),
                getUpsertKeyBuilder(), getJobName(), getNodeName(), getKafkaLoggingServiceServers(),
                getKafkaLoggingServiceTopics()))
        .uid(getNodeId())
        .setParallelism(getParallelism())
        .name(getNodeName());
  }

  static class MysqlBatchRichSinkFunction extends JdbcRichSinkFunction<RcUnit> {

    private final String jobId;

    private final String jobName;

    private final String nodeName;

    private final String tenantId;

    private final String userId;

    private final String uri;

    private final String table;

    private final String insertSql;

    private final List<String> columns;

    private final Map<String, String> attributeMapper;

    private final GraylogLogTypeEnum logType;

    private final Map<String, Integer> priorTableColIndex;

    private final List<String> insertKey;

    private final int batchSize;

    private final Map<String, RcUnit> rowMap = Maps.newHashMap();

    private UpsertKeyBuilder upsertKeyBuilder;

    private Counter counterRecordsOut;
    private String kafkaLoggingServiceServers;
    private String kafkaLoggingServiceTopics;

    MysqlBatchRichSinkFunction(String jobId, String tenantId, String userId, String uri,
                               String username,
                               String password, String table,
                               String insertSql,
                               List<String> columns, Map<String, String> attributeMapper,
                               GraylogLogTypeEnum logType,
                               Map<String, Integer> priorTableColIndex,
                               List<String> insertKey,
                               int batchSize,
                               UpsertKeyBuilder upsertKeyBuilder,
                               String jobName,
                               String nodeName, String kafkaLoggingServiceServers,
                               String kafkaLoggingServiceTopics) {
      super(new MysqlConnWrapper("com.mysql.jdbc.Driver", uri, username, password,
          "select 1", table, null, 0, null));
      this.jobId = jobId;
      this.tenantId = tenantId;
      this.uri = uri;
      this.userId = userId;
      this.table = table;
      this.insertSql = insertSql;
      this.columns = columns;
      this.attributeMapper = attributeMapper;
      this.logType = logType;
      this.priorTableColIndex = priorTableColIndex;
      this.insertKey = insertKey;
      this.batchSize = batchSize;
      this.upsertKeyBuilder = upsertKeyBuilder;
      this.jobName = jobName;
      this.nodeName = nodeName;
      this.kafkaLoggingServiceServers = kafkaLoggingServiceServers;
      this.kafkaLoggingServiceTopics = kafkaLoggingServiceTopics;
    }

    @Override
    public void open(Configuration parameters) throws Exception {
      super.open(parameters);
      counterRecordsOut = getRuntimeContext().getMetricGroup()
          .counter(JobConstant.FLINK_METRIC_NUM_RECORDS_OUT_OVERWRITE);
      openProducer(kafkaLoggingServiceServers);

    }

    @Override
    public synchronized void invoke(RcUnit record, Context context) throws Exception {
      logger.debug("MysqlBatchSink process element: {}", record);
      rowMap.put(upsertKeyBuilder
          .build(insertKey, record, attributeMapper), record);

      // 自定义的输出指标数据.
      counterRecordsOut.inc();

      if (rowMap.size() >= batchSize) {
        flush();
      }
    }

    private void flush() {
      if (rowMap.size() == 0) {
        return;
      }
      try {
        try (PreparedStatement preparedStatement = this.jdbcConnWrapper.getConnectionWrapper()
            .prepareStatement(insertSql)) {
          for (String upsertKey : rowMap.keySet()) {
            if (upsertKey.contains(NULL_IDENTIFICATION)) {
              String[] keyArr = upsertKey.split(SPLIT_SEPARATOR);
              for (int i = 0; i < keyArr.length - 1; i = i + 2) {
                if (NULL_IDENTIFICATION.equals(keyArr[i + 1])) {
                  LoggingServiceUtil.alarm(LogVo.builder()
                      .k8sServiceName(GrayLogConstant.K8S_SERVICE_NAME)
                      .module(GrayLogConstant.MODULE_NAME)
                      .userId(userId)
                      .userName(tenantId)
                      .tenantId(tenantId)
                      .operateObjectName(jobName)
                      .operateObject(jobId)
                      .shortMessage(
                          nodeName + "MySQL data table"
                              + table + " Update failed：key " + keyArr[i] + " is null"
                              + "，URI：" + uri + "，Table："
                              + table)
                      .operation(ShortMessageConstant.INSERT_MYSQL_DATA)
                      .requestId(String.valueOf(System.currentTimeMillis()))
                      .result(GrayLogResultEnum.FAIL.getValue())
                      .logType(logType)
                      .kafkaLoggingServiceTopic(kafkaLoggingServiceTopics)
                      .kafkaLoggingServiceServers(kafkaLoggingServiceServers)
                      .build(), flinkKafkaInternalProducer);
                }
              }
              continue;
            }
            RcUnit record = rowMap.get(upsertKey);
            for (int i = 0; i < columns.size(); i++) {
              Object val = attributeMapper.containsKey(columns.get(i))
                  ? record.getAttr(attributeMapper.get(columns.get(i))) : null;
              preparedStatement.setObject(i + 1, val);
              preparedStatement.setObject(i + 1 + columns.size(), val);
            }
            preparedStatement.addBatch();
          }
          preparedStatement.executeBatch();
          logger.debug("successfully execute mysql batch insert sql: {}", preparedStatement);
        }
      } catch (Exception e) {
        logger.error("Exception occurred when try to batch insert/update to {}, exception: {}",
            table, e.getMessage(), e);
        LoggingServiceUtil.alarm(LogVo.builder()
            .k8sServiceName(GrayLogConstant.K8S_SERVICE_NAME)
            .module(GrayLogConstant.MODULE_NAME)
            .userName(tenantId)
            .userId(userId)
            .tenantId(tenantId)
            .operateObjectName(jobName)
            .operateObject(jobId)
            .shortMessage(
                nodeName + "mysql data table "
                    + table + " Update failed：" + e + "，URI：" + uri + "，Table：" + table)
            .operation(ShortMessageConstant.INSERT_MYSQL_DATA)
            .requestId(String.valueOf(System.currentTimeMillis()))
            .result(GrayLogResultEnum.FAIL.getValue())
            .logType(logType)
            .kafkaLoggingServiceTopic(kafkaLoggingServiceTopics)
            .kafkaLoggingServiceServers(kafkaLoggingServiceServers)
            .build(), flinkKafkaInternalProducer);
      } finally {
        rowMap.clear();
      }
    }

    @Override
    public void close() throws Exception {

      LoggingServiceUtil.info(LogVo.builder()
          .k8sServiceName(GrayLogConstant.K8S_SERVICE_NAME)
          .module(GrayLogConstant.MODULE_NAME)
          .userName(tenantId)
          .userId(userId)
          .tenantId(tenantId)
          .operateObjectName(jobName)
          .operateObject(jobId)
          .shortMessage(nodeName + "MySQL data source disconnected successfully，"
              + "URI：" + uri + "，Table：" + table)
          .operation(ShortMessageConstant.CLOSE_MYSQL)
          .requestId(String.valueOf(System.currentTimeMillis()))
          .result(GrayLogResultEnum.SUCCESS.getValue())
          .logType(logType)
          .kafkaLoggingServiceTopic(kafkaLoggingServiceTopics)
          .kafkaLoggingServiceServers(kafkaLoggingServiceServers)
          .build(), flinkKafkaInternalProducer);
      flush();
      super.close();
      logger.info("MysqlBatchSink is closed.");
    }

  }
}

