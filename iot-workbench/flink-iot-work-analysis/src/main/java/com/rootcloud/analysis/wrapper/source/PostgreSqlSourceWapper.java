/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.wrapper.source;

import com.rootcloud.analysis.common.context.IotWorkRuntimeContext;
import com.rootcloud.analysis.common.entity.RcUnit;
import com.rootcloud.analysis.core.constants.GrayLogConstant;
import com.rootcloud.analysis.core.constants.ShortMessageConstant;
import com.rootcloud.analysis.core.enums.GrayLogResultEnum;
import com.rootcloud.analysis.core.graylog.LogVo;
import com.rootcloud.analysis.core.log.LogUtil;
import com.rootcloud.analysis.log.loggingservice.LoggingServiceUtil;
import com.rootcloud.analysis.wrapper.function.jdbc.JdbcRichSourceFunction;
import com.rootcloud.analysis.wrapper.jdbc.JdbcConnWrapper;
import com.rootcloud.analysis.wrapper.jdbc.PostgreSqlConnWrapper;
import java.util.Set;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.source.SourceFunction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Please use PostgresqlBatchSourceWrapper instead.
 */
@Deprecated
public class PostgreSqlSourceWapper extends JdbcSourceWrapper {

  private static final Logger logger = LoggerFactory.getLogger(PostgreSqlSourceWapper.class);

  public PostgreSqlSourceWapper(Builder builder) {
    super(builder);
  }

  public static class Builder extends JdbcSourceWrapper.Builder {

    @Override
    public PostgreSqlSourceWapper build() {
      return new PostgreSqlSourceWapper(this);
    }
  }

  /**
   * registerInput.
   */
  @Override
  public void registerInput(IotWorkRuntimeContext context) {
    PostgreSqlConnWrapper postgreSqlConnWrapper = new PostgreSqlConnWrapper("org.postgresql.Driver",
        uri,
        username, password, "select 1", table, getSchema(), limit, filterSql);
    context.putSourceConnToCache(getNodeId(), postgreSqlConnWrapper);
    SourceFunction sourceFunction = new PostgreSqlSourceFunction(postgreSqlConnWrapper,
        getKafkaLoggingServiceServers(), getKafkaLoggingServiceTopics());

    StreamExecutionEnvironment env = context.getExecutionEnv();
    DataStream<String> streamSource = env.addSource(sourceFunction).uid(getNodeId())
        .setParallelism(1).name(getNodeName());
    context.putObjectToContainer(getNodeId(), streamSource);
  }

  class PostgreSqlSourceFunction extends JdbcRichSourceFunction<Set<RcUnit>> {

    private final String kafkaLoggingServiceServers;
    private final String kafkaLoggingServiceTopics;

    /**
     * 构造方法.
     */
    public PostgreSqlSourceFunction(JdbcConnWrapper jdbcConnWrapper,
                                    String kafkaLoggingServiceServers,
                                    String kafkaLoggingServiceTopics) {
      super(jdbcConnWrapper);
      this.kafkaLoggingServiceServers = kafkaLoggingServiceServers;
      this.kafkaLoggingServiceTopics = kafkaLoggingServiceTopics;
    }

    @Override
    public void open(Configuration parameters) throws Exception {
      super.open(parameters);
      openProducer(kafkaLoggingServiceServers);
    }

    @Override
    public void run(SourceContext<Set<RcUnit>> ctx) throws Exception {
      while (isRunning() && !Thread.currentThread().isInterrupted()) {
        try {
          Set<RcUnit> result = null;
          if (!Thread.currentThread().isInterrupted()) {
            result = jdbcConnWrapper.getData();
          }
          if (!Thread.currentThread().isInterrupted() && result != null) {
            ctx.collect(result);
          }
        } catch (Exception ex) {
          LoggingServiceUtil.alarm(LogVo.builder()
              .k8sServiceName(GrayLogConstant.K8S_SERVICE_NAME)
              .module(GrayLogConstant.MODULE_NAME)
              .userName(getTenantId())
              .tenantId(getTenantId())
              .operateObjectName(getJobName())
              .operateObject(getJobId())
              .shortMessage(String.format("Exception occurred in node %s when reading data from "
                  + "postgresql Table:%s . Detail: %s",getNodeName(),table, ex.toString()))
              .operation(ShortMessageConstant.READ_POSTGRESQL_DATA)
              .requestId(String.valueOf(System.currentTimeMillis()))
              .result(GrayLogResultEnum.FAIL.getValue())
              .logType(getLogType())
              .kafkaLoggingServiceTopic(kafkaLoggingServiceTopics)
              .kafkaLoggingServiceServers(kafkaLoggingServiceServers)
              .build(), flinkKafkaInternalProducer);
        }

        try {
          Thread.sleep(detectPeriod * 1000);
        } catch (InterruptedException ex) {
          logger.warn("Thread is interrupted.");
          break;
        } catch (NullPointerException np) {
          logger.error("detectPeriod is null.");
        } catch (Exception e) {
          logger.error(e.toString());
        }
      }
      logger.info("PostgreSql source is over,URI：{}，Table：{}", uri, table);
    }


    @Override
    public void cancel() {
      logger.info("PostgreSql source is canceled.");
      LoggingServiceUtil.info(LogVo.builder()
          .k8sServiceName(GrayLogConstant.K8S_SERVICE_NAME)
          .module(GrayLogConstant.MODULE_NAME)
          .userName(getTenantId())
          .userId(getUserId())
          .tenantId(getTenantId())
          .operateObjectName(getJobName())
          .operateObject(getJobId())
          .shortMessage(String.format("Node %s disconnected from PostgreSQL. "
              + "Database connection string: %s. Table: %s.",getNodeName(),uri,table))
          .operation(ShortMessageConstant.CLOSE_POSTGRESQL)
          .requestId(String.valueOf(System.currentTimeMillis()))
          .result(GrayLogResultEnum.SUCCESS.getValue())
          .logType(getLogType())
          .kafkaLoggingServiceTopic(kafkaLoggingServiceTopics)
          .kafkaLoggingServiceServers(kafkaLoggingServiceServers)
          .build(), flinkKafkaInternalProducer);
    }

    @Override
    public void close() throws Exception {

      logger.info("PostgreSql source is closed.");
      LoggingServiceUtil.info(LogVo.builder()
          .k8sServiceName(GrayLogConstant.K8S_SERVICE_NAME)
          .module(GrayLogConstant.MODULE_NAME)
          .userName(getTenantId())
          .userId(getUserId())
          .tenantId(getTenantId())
          .operateObjectName(getJobName())
          .operateObject(getJobId())
          .shortMessage(String.format("Node %s disconnected from Postgresql."
              + " Database connection string: %s. Table: %s.",getNodeName(),uri,table))
          .operation(ShortMessageConstant.CLOSE_POSTGRESQL)
          .requestId(String.valueOf(System.currentTimeMillis()))
          .result(GrayLogResultEnum.SUCCESS.getValue())
          .logType(getLogType())
          .kafkaLoggingServiceTopic(kafkaLoggingServiceTopics)
          .kafkaLoggingServiceServers(kafkaLoggingServiceServers)
          .build(), flinkKafkaInternalProducer);
      super.close();
    }

  }

  ;
}
