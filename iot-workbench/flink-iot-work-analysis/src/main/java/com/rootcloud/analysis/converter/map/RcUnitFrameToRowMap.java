/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.converter.map;

import com.rootcloud.analysis.common.entity.RcUnitFrame;

import java.util.List;

import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.types.Row;

public class RcUnitFrameToRowMap implements MapFunction<RcUnitFrame, Row> {
  private List<String> keyList = null;

  public RcUnitFrameToRowMap(List<String> keyList) {
    this.keyList = keyList;
  }

  @Override
  public Row map(RcUnitFrame value) throws Exception {
    Row row = new Row(keyList.size());
    for (int index = 0; index < keyList.size(); index++) {
      row.setField(index, index);
    }

    return row;
  }
}
