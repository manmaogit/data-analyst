/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.decoder.source.impl;

import static com.rootcloud.analysis.core.constants.JobConstant.KEY_DATABASE;
import static com.rootcloud.analysis.core.constants.JobConstant.KEY_PASSWORD;
import static com.rootcloud.analysis.core.constants.JobConstant.KEY_USERNAME;
import static com.rootcloud.analysis.core.constants.SchemaConstant.OR;

import com.alibaba.fastjson.JSONObject;
import com.rootcloud.analysis.common.util.AbstractDeviceUtil;
import com.rootcloud.analysis.context.IotWorkRuntimeDecodeContext;
import com.rootcloud.analysis.core.constants.JobConstant;
import com.rootcloud.analysis.core.enums.DataSourceTypeEnum;
import com.rootcloud.analysis.core.enums.HistorianIgnoreSuffixEnum;
import com.rootcloud.analysis.decoder.source.AbstractSourceDecoder;
import com.rootcloud.analysis.decoder.source.SourceDecoder;
import com.rootcloud.analysis.exception.DecodeSourceException;
import com.rootcloud.analysis.wrapper.source.AbstractSourceWrapper;
import com.rootcloud.analysis.wrapper.source.TdEngineWithAggSourceWrapper;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SourceDecoder(dataSourceType = DataSourceTypeEnum.TD_ENGINE_WITH_AGG)
public class TdEngineWithAggSourceDecoder extends AbstractSourceDecoder {

  private static Logger logger = LoggerFactory.getLogger(TdEngineWithAggSourceDecoder.class);

  @Override
  public AbstractSourceWrapper decodeSourceParams(JSONObject conf) {
    try {
      TdEngineWithAggSourceWrapper.TdEngineWithAggSourceWrapperBuilder builder =
          TdEngineWithAggSourceWrapper.builder();

      JSONObject params = conf.getJSONObject(JobConstant.KEY_PARAMETERS);
      builder.tenantId(conf.getString(JobConstant.TENANT_ID));
      String urls = params.getString("urls");
      if (StringUtils.isNotBlank(urls)) {
        builder.urls(Arrays.asList(StringUtils.split(urls, OR)));
      }
      builder.userName(params.getString(KEY_USERNAME))
          .password(params.getString(KEY_PASSWORD))
          .database(params.getString(KEY_DATABASE))
          .groupByTimeScope(params.getIntValue("group_by_time_scope"))
          .groupByTimeType(params.getString("group_by_time_type"));
      builder.timeLowerBound(IotWorkRuntimeDecodeContext.getTimeScope().getStartEpochSecond() * 1000);
      builder.timeUpperBound(IotWorkRuntimeDecodeContext.getTimeScope().getEndEpochSecond() * 1000);

      Set<String> ignoredFieldNames = new HashSet<String>();
      ignoredFieldNames.addAll(HistorianIgnoreSuffixEnum.getAllFieldNames());
      builder.ignoredFieldNames(ignoredFieldNames);
      // 动态获取抽象模型最新映射关系.
      builder.deviceMappings(AbstractDeviceUtil.getDeviceMappings(conf));

      return builder.build();
    } catch (Exception ex) {
      logger.error("Exception occurred when decoding TdEngine source: {}, error message: {}", conf,
          ex);
      throw new DecodeSourceException(conf.getString(JobConstant.KEY_NODE_ID),
          conf.toJSONString());
    }
  }

}
