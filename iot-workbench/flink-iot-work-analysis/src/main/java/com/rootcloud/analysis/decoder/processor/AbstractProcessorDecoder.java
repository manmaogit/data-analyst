/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.decoder.processor;

import com.alibaba.fastjson.JSONObject;
import com.rootcloud.analysis.core.constants.JobConstant;
import com.rootcloud.analysis.decoder.AbstractDecoder;
import com.rootcloud.analysis.wrapper.AbstractWrapper;
import com.rootcloud.analysis.wrapper.processor.AbstractProcessorWrapper;

public abstract class AbstractProcessorDecoder extends AbstractDecoder {

  @Override
  protected final AbstractWrapper decodeParams(JSONObject conf) {
    AbstractProcessorWrapper abstractProcessorWrapper = decodeProcessParams(conf);
    abstractProcessorWrapper.setPriorNodeId(conf.getString(JobConstant.KEY_PRIOR_NODE_ID));
    abstractProcessorWrapper.setProcessorType(conf.getString(JobConstant.KEY_PROCESSOR_TYPE));
    return abstractProcessorWrapper;
  }

  protected abstract AbstractProcessorWrapper decodeProcessParams(JSONObject conf);
}
