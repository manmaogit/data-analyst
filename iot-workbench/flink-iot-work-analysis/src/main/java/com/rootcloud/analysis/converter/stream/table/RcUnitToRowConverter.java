/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.converter.stream.table;

import com.rootcloud.analysis.common.entity.RcUnit;
import com.rootcloud.analysis.common.util.ClassUtil;
import com.rootcloud.analysis.converter.stream.IConverter;
import com.rootcloud.analysis.core.enums.DataTypeInfoEnum;
import java.lang.reflect.Method;
import java.util.List;
import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtMethod;
import lombok.AllArgsConstructor;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.typeinfo.BasicTypeInfo;
import org.apache.flink.api.common.typeinfo.SqlTimeTypeInfo;
import org.apache.flink.api.java.typeutils.RowTypeInfo;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.table.runtime.typeutils.BigDecimalTypeInfo;
import org.apache.flink.types.Row;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@AllArgsConstructor
public class RcUnitToRowConverter implements IConverter<RcUnit, Row> {

  private static final Logger logger = LoggerFactory.getLogger(RcUnitToRowConverter.class);

  private static final String ROW_TYPE_CONTAINER = "com.rootcloud.RowTypeContainer";

  private final String tableName;
  private final List<String> key;
  private final List<String> valueType;
  private final List<Integer> valueTypeScales;

  private String getRowTypeContainerClassName() {
    return ROW_TYPE_CONTAINER + tableName;
  }

  @Override
  public DataStream<Row> convert(DataStream<RcUnit> input) throws Exception {

    ClassPool pool = ClassPool.getDefault();
    pool.importPackage(RowTypeInfo.class.getPackage().getName());
    pool.importPackage(BasicTypeInfo.class.getPackage().getName());
    pool.importPackage(SqlTimeTypeInfo.class.getPackage().getName());
    pool.importPackage(BigDecimalTypeInfo.class.getPackage().getName());

    StringBuilder builder = new StringBuilder();
    builder.append("public RowTypeInfo getRowType(){");
    builder.append("TypeInformation[] fieldTypes = new TypeInformation[] {");

    for (int index = 0; index < valueType.size(); index++) {
      builder.append(DataTypeInfoEnum.getFlinkTypeInfoClass(valueType.get(index),
          valueTypeScales.get(index)));
      if (index != valueType.size() - 1) {
        builder.append(",");
      }
    }

    builder.append("};");

    builder.append("String[] fieldNames = new String[] { ");
    for (int index = 0; index < key.size(); index++) {
      builder.append("\"");
      builder.append(key.get(index));
      builder.append("\"");
      if (index != key.size() - 1) {
        builder.append(",");
      }
    }
    builder.append("};");

    builder.append("return new RowTypeInfo(fieldTypes,fieldNames);");

    builder.append("}");

    logger
        .info("RowTypeContainer class: {}, {}", getRowTypeContainerClassName(), builder.toString());

    CtClass container = ClassUtil.makeClass(pool, getRowTypeContainerClassName());
    CtMethod gmethod = CtMethod.make(builder.toString(), container);
    container.addMethod(gmethod);
    Class runner = container.toClass();

    Method gm = runner.getMethod("getRowType");
    Object object = runner.newInstance();
    RowTypeInfo rowTypeInfo = (RowTypeInfo) gm.invoke(object);

    return input.map(new MapFunction<RcUnit, Row>() {
      @Override
      public Row map(RcUnit value) throws Exception {
        Row row = new Row(key.size());
        for (int index = 0; index < key.size(); index++) {
          row.setField(index, value.getAttr(key.get(index)));
        }
        return row;
      }
    }).setParallelism(input.getParallelism()).returns(rowTypeInfo);

  }
}
