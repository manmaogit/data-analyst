/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.wrapper.sink;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Maps;
import com.rootcloud.analysis.common.context.IotWorkRuntimeContext;
import com.rootcloud.analysis.common.entity.RcUnit;
import com.rootcloud.analysis.context.IotWorkRuntimeDecodeContext;
import com.rootcloud.analysis.core.constants.JobConstant;
import com.rootcloud.analysis.core.constants.SchemaConstant;
import com.rootcloud.analysis.core.enums.KafkaKeyTypeEnum;
import com.rootcloud.analysis.core.enums.SinkTypeEnum;
import com.rootcloud.analysis.extent.FlinkKafkaExtendProducer;
import com.rootcloud.analysis.wrapper.WrapperBuilder;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import javax.annotation.Nullable;
import lombok.ToString;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaProducer;
import org.apache.flink.streaming.connectors.kafka.KafkaSerializationSchema;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@ToString(callSuper = true)
public class KafkaSinkWrapper extends AbstractSinkWrapper {

  private static Logger logger = LoggerFactory.getLogger(KafkaSinkWrapper.class);

  private final String brokers;

  private final List<String> topics;

  private final JSONObject keyParams;

  private final JSONArray attributeMapper;

  private KafkaSinkWrapper(Builder builder) {
    super(builder.jobId, builder.jobName, builder.tenantId, builder.nodeId, builder.parallelism,
        builder.nodeName, builder.priorNodeId, builder.sinkType,
        builder.logType);
    this.brokers = builder.brokers;
    this.topics = builder.topics;
    this.keyParams = builder.keyParams;
    this.attributeMapper = builder.attributeMapper;
  }

  /**
   * Javadoc.
   */
  public static class Builder extends WrapperBuilder {

    private String nodeId;
    private String nodeName;
    private Integer parallelism;

    private String priorNodeId;
    private SinkTypeEnum sinkType;

    private String brokers;
    private List<String> topics;
    private JSONObject keyParams;
    private JSONArray attributeMapper;

    public Builder setNodeId(String nodeId) {
      this.nodeId = nodeId;
      return this;
    }

    public Builder setNodeName(String nodeName) {
      this.nodeName = nodeName;
      return this;
    }

    public Builder setParallelism(Integer parallelism) {
      this.parallelism = parallelism;
      return this;
    }

    public Builder setPriorNodeId(String priorNodeId) {
      this.priorNodeId = priorNodeId;
      return this;
    }

    public Builder setSinkType(SinkTypeEnum sinkType) {
      this.sinkType = sinkType;
      return this;
    }

    public Builder setBrokers(String brokers) {
      this.brokers = brokers;
      return this;
    }

    public Builder setTopics(List<String> topics) {
      this.topics = topics;
      return this;
    }

    public Builder setKeyParams(JSONObject keyParams) {
      this.keyParams = keyParams;
      return this;
    }

    public Builder setAttributeMapper(JSONArray attributeMapper) {
      this.attributeMapper = attributeMapper;
      return this;
    }

    public KafkaSinkWrapper build() {
      return new KafkaSinkWrapper(this);
    }

  }

  @Override
  public void registerOutput(IotWorkRuntimeContext context) {

    Properties properties = new Properties();
    properties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, brokers);
    properties.put(ProducerConfig.BATCH_SIZE_CONFIG, 16384);
    properties.put(ProducerConfig.LINGER_MS_CONFIG, 1);
    properties.put(ProducerConfig.BUFFER_MEMORY_CONFIG, 33554432);
    properties.put(ProducerConfig.RETRIES_CONFIG, 5);
    // idempotent producer to avoid job restart
    properties.put(ProducerConfig.MAX_IN_FLIGHT_REQUESTS_PER_CONNECTION, 1);
    properties.put(ProducerConfig.ENABLE_IDEMPOTENCE_CONFIG, true);

    DataStream<RcUnit> priorDataStream = context.getObjectFromContainer(getPriorNodeId());
    if (topics != null && !topics.isEmpty()) {
      for (String topic : topics) {
        priorDataStream.addSink(new FlinkKafkaExtendProducer<>(
                topic,
                new RcKafkaSerializer<>(topic, attributeMapper, keyParams),
                properties,
                FlinkKafkaProducer.Semantic.AT_LEAST_ONCE,
                getTenantId(),
                getUserId(), getJobId(),
                getJobName(),
                getNodeName(),
                brokers, IotWorkRuntimeDecodeContext.getLoggingServiceKafkaTopic(),
                IotWorkRuntimeDecodeContext.getLoggingServiceKafkaServers()
            )).uid(getNodeId())
            .name(getNodeName())
            .setParallelism(getParallelism());
      }
    } else {
      logger.warn("Kafka topics is empty.");
    }

  }

  private class RcKafkaSerializer<T> implements KafkaSerializationSchema<T> {

    private final String topic;

    private final JSONObject keyParams;

    private final Map<String, String> attributeMapper = Maps.newHashMap();

    private final JSONArray schema = new JSONArray();

    private RcKafkaSerializer(String topic, JSONArray attributeMapperArr, JSONObject keyParams) {
      this.topic = topic;
      this.keyParams = keyParams;
      for (Object o : attributeMapperArr) {
        JSONObject targetAttribute = ((JSONObject) o)
            .getJSONObject(JobConstant.KEY_TARGET_ATTRIBUTE);
        JSONObject sourceAttribute = ((JSONObject) o)
            .getJSONObject(JobConstant.KEY_SOURCE_ATTRIBUTE);
        schema.add(targetAttribute);
        attributeMapper.put(targetAttribute.getString(SchemaConstant.ATTR_NAME),
            sourceAttribute.getString(SchemaConstant.ATTR_NAME));
      }
    }

    @Override
    public ProducerRecord<byte[], byte[]> serialize(T element, @Nullable Long timestamp) {

      Map<String, Object> intermediateResult = Maps.newHashMap();
      for (String key : attributeMapper.keySet()) {
        intermediateResult.put(key, ((RcUnit) element).getAttr(attributeMapper.get(key)));
      }
      RcUnit result = RcUnit.valueOf(intermediateResult, schema);

      String key = "";
      String keyType = keyParams.getString(JobConstant.KEY_KAFKA_KEY_TYPE);
      if (KafkaKeyTypeEnum.ATTR_VALUE.name().equals(keyType)) {
        Object intermediateKey = ((RcUnit) element)
            .getAttr(keyParams.getString(JobConstant.KEY_ATTR__NAME));
        if (intermediateKey != null) {
          key = intermediateKey.toString();
        }
      } else if (KafkaKeyTypeEnum.HARD_CODE.name().equals(keyType)) {
        key = keyParams.getString(JobConstant.KEY_KAFKA_KEY_VALUE);
      }

      logger.debug("Kafka producer result: {}", result);

      return new ProducerRecord<>(topic, key.getBytes(StandardCharsets.UTF_8),
          JSONObject.toJSONString(result.getData()).getBytes(StandardCharsets.UTF_8));
    }
  }

}

