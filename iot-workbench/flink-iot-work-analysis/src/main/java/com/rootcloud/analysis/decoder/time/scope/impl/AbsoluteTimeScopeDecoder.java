/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.decoder.time.scope.impl;

import com.alibaba.fastjson.JSONObject;
import com.rootcloud.analysis.common.util.DateUtil;
import com.rootcloud.analysis.context.IotWorkRuntimeDecodeContext;
import com.rootcloud.analysis.core.constants.JobConstant;
import com.rootcloud.analysis.core.enums.TimeScopeTypeEnum;
import com.rootcloud.analysis.core.enums.TimeZoneTypeEnum;
import com.rootcloud.analysis.decoder.time.scope.AbstractTimeScopeDecoder;
import com.rootcloud.analysis.decoder.time.scope.TimeScopeDecoder;
import com.rootcloud.analysis.wrapper.time.scope.AbsoluteTimeScopeWrapper;
import com.rootcloud.analysis.wrapper.time.scope.AbstractTimeScopeWrapper;

@TimeScopeDecoder(timeScopeTypeEnum = TimeScopeTypeEnum.ABSOLUTE_TIME)
public class AbsoluteTimeScopeDecoder extends AbstractTimeScopeDecoder {

  @Override
  public AbstractTimeScopeWrapper decode(JSONObject conf) {
    AbsoluteTimeScopeWrapper timeScopeWrapper = new AbsoluteTimeScopeWrapper();
    timeScopeWrapper.setStartTime(conf.getString(JobConstant.KEY_START_TIME));
    timeScopeWrapper.setEndTime(conf.getString(JobConstant.KEY_END_TIME));
    timeScopeWrapper.setStartEpochSecond(DateUtil
        .getEpochSecond(timeScopeWrapper.getStartTime(), "yyyy-MM-dd HH:mm:ss",
            TimeZoneTypeEnum.valueOf(IotWorkRuntimeDecodeContext.getTimezone()).getTimeZoneValue()
                .substring(3)));
    timeScopeWrapper.setEndEpochSecond(DateUtil
        .getEpochSecond(timeScopeWrapper.getEndTime(), "yyyy-MM-dd HH:mm:ss",
            TimeZoneTypeEnum.valueOf(IotWorkRuntimeDecodeContext.getTimezone()).getTimeZoneValue()
                .substring(3)));
    return timeScopeWrapper;
  }
}
