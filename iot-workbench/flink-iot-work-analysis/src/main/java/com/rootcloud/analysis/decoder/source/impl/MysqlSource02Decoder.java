/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.decoder.source.impl;

import com.alibaba.fastjson.JSONObject;
import com.rootcloud.analysis.core.enums.DataSourceTypeEnum;
import com.rootcloud.analysis.decoder.source.SourceDecoder;
import com.rootcloud.analysis.wrapper.source.JdbcSource02Wrapper;
import com.rootcloud.analysis.wrapper.source.MysqlSource02Wrapper;

/**
 * Please use MysqlBatchSourceDecoder instead.
 */
@Deprecated
@SourceDecoder(dataSourceType = DataSourceTypeEnum.MYSQL_02)
public class MysqlSource02Decoder extends JdbcSource02Decoder {

  @Override
  protected JdbcSource02Wrapper decodeJdbcSourceParams(JSONObject conf) {
    return new MysqlSource02Wrapper();
  }
}
