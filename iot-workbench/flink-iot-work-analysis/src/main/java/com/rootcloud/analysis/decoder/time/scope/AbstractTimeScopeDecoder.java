/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.decoder.time.scope;

import com.alibaba.fastjson.JSONObject;
import com.rootcloud.analysis.decoder.IDecoder;
import com.rootcloud.analysis.wrapper.time.scope.AbstractTimeScopeWrapper;

public abstract class AbstractTimeScopeDecoder implements IDecoder {

  @Override
  public abstract AbstractTimeScopeWrapper decode(JSONObject conf);

}
