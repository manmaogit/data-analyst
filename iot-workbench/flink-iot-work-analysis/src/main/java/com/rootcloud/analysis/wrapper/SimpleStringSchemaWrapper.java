/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.wrapper;

import lombok.extern.slf4j.Slf4j;
import org.apache.flink.annotation.PublicEvolving;
import org.apache.flink.api.common.serialization.SimpleStringSchema;


/**
 * Very simple serialization schema for strings.
 *
 * <p>By default, the serializer uses "UTF-8" for string/byte conversion.
 */
@PublicEvolving
@Slf4j
public class SimpleStringSchemaWrapper extends SimpleStringSchema {

  private static final long serialVersionUID = 1L;

  @Override
  public String deserialize(byte[] message) {
    //上游工况可能出现空
    try {
      return new String(message, super.getCharset());
    } catch (Exception e) {
      return new String();
    }
  }

}
