/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.converter.time.impl;

import static com.rootcloud.analysis.core.enums.TimeDimensionEnum.MONTH;

import com.rootcloud.analysis.common.util.DateUtil;
import com.rootcloud.analysis.converter.time.ITimeConverter;
import com.rootcloud.analysis.converter.time.TimeConvert;

import java.time.ZoneId;
import java.util.Calendar;
import java.util.GregorianCalendar;

@TimeConvert(converterType = MONTH)
public class TimeToMonthConverter implements ITimeConverter {

  @Override
  public String convert(String time, ZoneId zoneId) {
    Long timeMs = DateUtil.convertTimeToLongMs(time);
    return timeToMonth(timeMs, zoneId);
  }

  @Override
  public String convert(long time, ZoneId zoneId) {
    return timeToMonth(time, zoneId);
  }

  @Override
  public String convert(long time, ZoneId zoneId, int startMonth, int startDay,
      int startHour, int startMinute) {
    return timeToMonth(time, zoneId, startMonth, startDay, startHour, startMinute);
  }

  private String timeToMonth(long time, ZoneId zoneId) {
    String dateTimeFormatter = "yyyy-MM-01 00:00:00";
    return DateUtil.formatTime(time, zoneId, dateTimeFormatter);
  }

  private String timeToMonth(long time, ZoneId zoneId, int startMonth, int startDay,
      int startHour, int startMinute) {
    Calendar cal = new GregorianCalendar();
    cal.setTimeInMillis(time);
    DateUtil.minusTime(cal, startMonth - 1, startDay - 1, startHour, startMinute);
    String dateTimeFormatter = "yyyy-MM-01 00:00:00";
    return DateUtil.formatTime(cal.getTimeInMillis(), zoneId, dateTimeFormatter);
  }
}
