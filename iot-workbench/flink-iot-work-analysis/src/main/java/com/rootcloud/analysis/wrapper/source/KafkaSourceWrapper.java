/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.wrapper.source;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.rootcloud.analysis.common.context.IotWorkRuntimeContext;
import com.rootcloud.analysis.common.entity.RcUnit;
import com.rootcloud.analysis.common.factory.KafkaLogMiddlewareResourceFactory;
import com.rootcloud.analysis.core.constants.GrayLogConstant;
import com.rootcloud.analysis.core.constants.JobConstant;
import com.rootcloud.analysis.core.constants.ShortMessageConstant;
import com.rootcloud.analysis.core.enums.GrayLogResultEnum;
import com.rootcloud.analysis.core.graylog.LogVo;
import com.rootcloud.analysis.entity.RcTimeAttr;
import com.rootcloud.analysis.log.loggingservice.LoggingServiceUtil;
import com.rootcloud.analysis.wrapper.SimpleStringSchemaWrapper;
import com.rootcloud.analysis.wrapper.WrapperBuilder;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import lombok.Getter;
import lombok.ToString;
import org.apache.commons.lang3.StringUtils;
import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.connector.kafka.source.KafkaSource;
import org.apache.flink.connector.kafka.source.KafkaSourceBuilder;
import org.apache.flink.connector.kafka.source.KafkaSourceOptions;
import org.apache.flink.connector.kafka.source.enumerator.initializer.OffsetsInitializer;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.ProcessFunction;
import org.apache.flink.streaming.connectors.kafka.internals.FlinkKafkaInternalProducer;
import org.apache.flink.util.Collector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Getter
@ToString(callSuper = true)
public class KafkaSourceWrapper extends AbstractSourceWrapper {

  private static Logger logger = LoggerFactory.getLogger(KafkaSourceWrapper.class);

  private final RcTimeAttr timeAttr;

  private final String brokers;

  private final List<String> topics;

  private final String groupId;

  private final String customConsumerGroupId;

  private final String offsetStrategy;

  private KafkaSourceWrapper(Builder builder) {
    super(builder.jobId, builder.jobName, builder.tenantId, builder.nodeId, builder.parallelism,
        builder.nodeName, builder.sourceType, builder.schema, builder.logType);
    this.customConsumerGroupId = builder.customConsumerGroupId;
    this.timeAttr = builder.timeAttr;
    this.brokers = builder.brokers;
    this.topics = builder.topics;
    this.groupId = builder.groupId;
    if (builder.offsetStrategy == null) {
      this.offsetStrategy = JobConstant.KAFKA_CONSUMER_OFFSET_STRATEGY_DEFAULT;
    } else {
      this.offsetStrategy = builder.offsetStrategy;
    }
  }

  public static class Builder extends WrapperBuilder {

    private String nodeId;
    private String nodeName;
    private Integer parallelism;
    private String sourceType;
    private JSONArray schema;
    private String brokers;
    private List<String> topics;
    private String groupId;
    private String offsetStrategy;
    private RcTimeAttr timeAttr;
    private String customConsumerGroupId;

    public Builder setCustomConsumerGroupId(String customConsumerGroupId) {
      this.customConsumerGroupId = customConsumerGroupId;
      return this;
    }

    public Builder setNodeId(String nodeId) {
      this.nodeId = nodeId;
      return this;
    }

    public Builder setNodeName(String nodeName) {
      this.nodeName = nodeName;
      return this;
    }

    public Builder setParallelism(Integer parallelism) {
      this.parallelism = parallelism;
      return this;
    }

    public Builder setSourceType(String sourceType) {
      this.sourceType = sourceType;
      return this;
    }

    public Builder setBrokers(String brokers) {
      this.brokers = brokers;
      return this;
    }

    public Builder setSchema(JSONArray schema) {
      this.schema = schema;
      return this;
    }

    public Builder setTimeAttr(RcTimeAttr timeAttr) {
      this.timeAttr = timeAttr;
      return this;
    }

    public Builder setTopics(List<String> topics) {
      this.topics = topics;
      return this;
    }

    public Builder setGroupId(String groupId) {
      this.groupId = groupId;
      return this;
    }

    public Builder setOffsetStrategy(String offsetStrategy) {
      this.offsetStrategy = offsetStrategy;
      return this;
    }

    public KafkaSourceWrapper build() {
      return new KafkaSourceWrapper(this);
    }

  }

  private String getGroupId() {
    return JobConstant.IOTWORKS + groupId;
  }

  @Override
  public void registerInput(IotWorkRuntimeContext context) {
    Properties properties = new Properties();
    properties.setProperty("bootstrap.servers", brokers);
    properties.setProperty("auto.offset.reset", "latest");
    properties.setProperty("group.id", StringUtils.isBlank(customConsumerGroupId) ? getGroupId() : getCustomConsumerGroupId(customConsumerGroupId));
    // 禁用自动提交.
    properties.setProperty("enable.auto.commit", "false");
    // checkpoint 时提交 offset
    properties.setProperty(KafkaSourceOptions.COMMIT_OFFSETS_ON_CHECKPOINT.key(), "true");

    KafkaSourceBuilder<String> consumerBuilder = KafkaSource.<String>builder().setProperties(properties).setTopics(topics)
            .setValueOnlyDeserializer(new SimpleStringSchemaWrapper());

    // 根据前端选择，配置消费者的offset消费策略.
    logger.debug("KafkaSourceWrapper-OffsetStrategy = {}.", getOffsetStrategy());
    switch (getOffsetStrategy()) {
      case "GROUP":
        consumerBuilder.setStartingOffsets(OffsetsInitializer.committedOffsets());
        break;
      case "EARLIEST":
        consumerBuilder.setStartingOffsets(OffsetsInitializer.earliest());
        break;
      case "LATEST":
        consumerBuilder.setStartingOffsets(OffsetsInitializer.latest());
        break;
      default:
        break;
    }

    StreamExecutionEnvironment env = context.getExecutionEnv();

    DataStream<String> sourceStream = env.fromSource(consumerBuilder.build(), WatermarkStrategy.noWatermarks(), getNodeName())
            .uid(getNodeId()).setParallelism(getParallelism());

    DataStream<RcUnit> rcUnitStream = sourceStream.process(new KafkaFlatMapFunction(getKafkaLoggingServiceServers(), getKafkaLoggingServiceTopics()))
            .setParallelism(getParallelism());
    context.putObjectToContainer(getNodeId(), rcUnitStream);
  }


  class KafkaFlatMapFunction extends ProcessFunction<String, RcUnit> {

    private final String kafkaLoggingServiceServers;
    private final String kafkaLoggingServiceTopics;
    private FlinkKafkaInternalProducer flinkKafkaInternalProducer;

    KafkaFlatMapFunction(String kafkaLoggingServiceServers, String kafkaLoggingServiceTopics) {
      this.kafkaLoggingServiceServers = kafkaLoggingServiceServers;
      this.kafkaLoggingServiceTopics = kafkaLoggingServiceTopics;
    }

    @Override
    public void open(Configuration parameters) throws Exception {
      super.open(parameters);
      if (flinkKafkaInternalProducer == null && kafkaLoggingServiceServers != null) {
        flinkKafkaInternalProducer = KafkaLogMiddlewareResourceFactory.getProducer(
            kafkaLoggingServiceServers,
            this.toString());
      }
    }

    @Override
    public void close() throws Exception {
      super.close();
      KafkaLogMiddlewareResourceFactory.close(this.toString());
      flinkKafkaInternalProducer = null;
    }

    @Override
    public void processElement(String value, ProcessFunction<String, RcUnit>.Context ctx,
                               Collector<RcUnit> out) throws Exception {
      if (!value.isEmpty()) {
        try {
          logger.debug("Consuming message {} from kafka.", value);
          RcUnit unit = RcUnit.valueOf(JSON.parseObject(value, HashMap.class));
          logger.debug("Kafka message handling result: {}", unit.getData());
          if (!Thread.currentThread().isInterrupted()) {
            out.collect(unit);
          }
        } catch (Exception e) {
          logger.error("Exception occurred when consuming message {} from kafka.", value, e);
          LoggingServiceUtil.alarm(LogVo.builder()
              .k8sServiceName(GrayLogConstant.K8S_SERVICE_NAME)
              .module(GrayLogConstant.MODULE_NAME)
              .userName(getTenantId())
              .tenantId(getTenantId())
              .operateObjectName(getJobName())
              .operateObject(getJobId())
              .shortMessage(String.format("Exception occurred when consuming message %s from kafka.  Detail: %s",value))
              .operation(ShortMessageConstant.READ_KAFKA_DATA)
              .requestId(String.valueOf(System.currentTimeMillis()))
              .result(GrayLogResultEnum.FAIL.getValue())
              .logType(getLogType())
              .kafkaLoggingServiceTopic(kafkaLoggingServiceTopics)
              .kafkaLoggingServiceServers(kafkaLoggingServiceServers)
              .build(), flinkKafkaInternalProducer);
        }

      } else {
        logger.warn("Consuming empty message from kafka.");
        LoggingServiceUtil.warn(LogVo.builder()
            .k8sServiceName(GrayLogConstant.K8S_SERVICE_NAME)
            .module(GrayLogConstant.MODULE_NAME)
            .userName(getTenantId())
            .userId(getUserId())
            .tenantId(getTenantId())
            .operateObjectName(getJobName())
            .operateObject(getJobId())
            .shortMessage(String.format("Consuming empty message from kafka."))
            .operation(ShortMessageConstant.READ_KAFKA_DATA)
            .requestId(String.valueOf(System.currentTimeMillis()))
            .result(GrayLogResultEnum.FAIL.getValue())
            .logType(getLogType())
            .kafkaLoggingServiceTopic(kafkaLoggingServiceTopics)
            .kafkaLoggingServiceServers(kafkaLoggingServiceServers)
            .build(), flinkKafkaInternalProducer);
      }
    }
  }

  private String getCustomConsumerGroupId(String customConsumerGroupId) {
    return JobConstant.IOTWORKS + customConsumerGroupId;
  }

}
