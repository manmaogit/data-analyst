/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.decoder;

import com.alibaba.fastjson.JSONObject;
import com.rootcloud.analysis.context.IotWorkRuntimeDecodeContext;
import com.rootcloud.analysis.core.constants.JobConstant;
import com.rootcloud.analysis.decoder.source.impl.InnerHiveSourceDecoder;
import com.rootcloud.analysis.wrapper.AbstractWrapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class AbstractDecoder implements IDecoder {

  private static Logger logger = LoggerFactory.getLogger(InnerHiveSourceDecoder.class);

  /**
   * decode.
   */
  public final AbstractWrapper decode(JSONObject conf) {
    // 核心解析逻辑，转换成对应算子的配置封装
    AbstractWrapper abstractWrapper = decodeParams(conf);

    abstractWrapper.setKafkaLoggingServiceServers(IotWorkRuntimeDecodeContext.getLoggingServiceKafkaServers());
    abstractWrapper.setKafkaLoggingServiceTopics(IotWorkRuntimeDecodeContext.getLoggingServiceKafkaTopic());
    abstractWrapper.setJobType(IotWorkRuntimeDecodeContext.getJobType());
    abstractWrapper.setLogType(IotWorkRuntimeDecodeContext.getJobType());
    abstractWrapper.setJobId(IotWorkRuntimeDecodeContext.getJobId());
    abstractWrapper.setUserId(IotWorkRuntimeDecodeContext.getUserId());
    abstractWrapper.setJobName(IotWorkRuntimeDecodeContext.getJobName());
    abstractWrapper.setTenantId(IotWorkRuntimeDecodeContext.getTenantId());
    abstractWrapper.setNodeId(conf.getString(JobConstant.KEY_NODE_ID));
    abstractWrapper.setNodeName(conf.getString(JobConstant.KEY_NODE_NAME));
    abstractWrapper.setParallelism(conf.getInteger(JobConstant.KEY_PARALLELISM));
    abstractWrapper.setTimeZone(IotWorkRuntimeDecodeContext.getTimezone());
    logger.debug("Decoding wrapper: {}", abstractWrapper);
    return abstractWrapper;
  }

  /**
   * decode params.
   */
  protected abstract AbstractWrapper decodeParams(JSONObject conf);

}