/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.decoder.sink.impl;

import com.alibaba.fastjson.JSONObject;
import com.rootcloud.analysis.core.enums.SinkTypeEnum;
import com.rootcloud.analysis.decoder.sink.SinkDecoder;
import com.rootcloud.analysis.wrapper.sink.JdbcSink02Wrapper;
import com.rootcloud.analysis.wrapper.sink.MysqlSink02Wrapper;

/**
 * Please use MysqlBatchSinkDecoder instead.
 */
@Deprecated
@SinkDecoder(sinkType = SinkTypeEnum.MYSQL_02)
public class MysqlSink02Decoder extends JdbcSink02Decoder {

  @Override
  protected JdbcSink02Wrapper decodeJdbcSinkParams(JSONObject conf) {
    return new MysqlSink02Wrapper();
  }
}
