/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.decoder.sink.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Maps;
import com.rootcloud.analysis.context.IotWorkRuntimeDecodeContext;
import com.rootcloud.analysis.core.constants.JobConstant;
import com.rootcloud.analysis.core.constants.SchemaConstant;
import com.rootcloud.analysis.core.util.RsaUtil;
import com.rootcloud.analysis.decoder.sink.AbstractSinkDecoder;
import com.rootcloud.analysis.exception.DecodeSinkException;
import com.rootcloud.analysis.wrapper.sink.AbstractSinkWrapper;
import com.rootcloud.analysis.wrapper.sink.JdbcSink03Wrapper;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class JdbcSink03Decoder extends AbstractSinkDecoder {

  private static Logger logger = LoggerFactory.getLogger(JdbcSink03Decoder.class);

  @Override
  public AbstractSinkWrapper decodeSinkParams(JSONObject conf) {
    try {
      JSONObject params = conf.getJSONObject(JobConstant.KEY_PARAMETERS);
      JSONObject outputMapper = conf.getJSONObject(JobConstant.KEY_OUTPUT_MAPPER);
      JdbcSink03Wrapper result = decodeJdbcSinkParams(conf);
      result.setUri(params.getString(JobConstant.KEY_URI));
      result.setUsername(params.getString(JobConstant.KEY_USERNAME));
      result.setPassword(RsaUtil.decryptDataOnJava(params.getString(JobConstant.KEY_PASSWORD),
          IotWorkRuntimeDecodeContext.getPrivateKey()));
      result.setTable(conf.getString(JobConstant.KEY_TABLE));
      List<String> columns = new ArrayList<>();
      Map<String, String> attributeMapper = Maps.newHashMap();
      JSONArray attributeMapperJs = outputMapper.getJSONArray(JobConstant.KEY_ATTRIBUTE_MAPPER);
      JSONArray schema = new JSONArray();
      result.setSchema(schema);
      for (int i = 0; i < attributeMapperJs.size(); i++) {
        JSONObject targetAttribute = ((JSONObject) attributeMapperJs.get(i))
            .getJSONObject(JobConstant.KEY_TARGET_ATTRIBUTE);
        JSONObject sourceAttribute = ((JSONObject) attributeMapperJs.get(i))
            .getJSONObject(JobConstant.KEY_SOURCE_ATTRIBUTE);
        columns.add(targetAttribute.getString(SchemaConstant.ATTR_NAME));
        attributeMapper.put(targetAttribute.getString(SchemaConstant.ATTR_NAME),
            sourceAttribute.getString(SchemaConstant.ATTR_NAME));
        schema.add(
            ((JSONObject) attributeMapperJs.get(i))
                .getJSONObject(JobConstant.KEY_TARGET_ATTRIBUTE));
      }
      result.setColumns(columns);
      result.setAttributeMapper(attributeMapper);
      result.setPrimaryKey(conf.getString(JobConstant.KEY_INSERT_KEY));

      return result;
    } catch (Exception ex) {
      logger.error("Exception occurred when decoding JdbcSink03Wrapper: {}, error: {}", conf,
          ex.toString());
      throw new DecodeSinkException(conf.getString(JobConstant.KEY_NODE_ID),
          conf.toJSONString());
    }
  }

  protected abstract JdbcSink03Wrapper decodeJdbcSinkParams(JSONObject conf);

}
