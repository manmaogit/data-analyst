/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.decoder.processor.impl;

import com.alibaba.fastjson.JSONObject;
import com.rootcloud.analysis.core.constants.JobConstant;
import com.rootcloud.analysis.core.enums.ProcessTypeEnum;
import com.rootcloud.analysis.decoder.processor.AbstractProcessorDecoder;
import com.rootcloud.analysis.decoder.processor.ProcessorDecoder;
import com.rootcloud.analysis.exception.DecodeProcessorException;
import com.rootcloud.analysis.wrapper.processor.AbstractProcessorWrapper;
import com.rootcloud.analysis.wrapper.processor.TempViewProcessorWrapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@ProcessorDecoder(processType = ProcessTypeEnum.TEMPORARY_VIEW)
public class TempViewProcessorDecoder extends AbstractProcessorDecoder {

  private static Logger logger = LoggerFactory.getLogger(
      TempViewProcessorDecoder.class);

  @Override
  public AbstractProcessorWrapper decodeProcessParams(JSONObject conf) {
    try {
      TempViewProcessorWrapper.Builder builder = new TempViewProcessorWrapper.Builder();
      builder.setSchema(conf.getJSONArray(JobConstant.KEY_SCHEMA));
      builder.setViewName(conf.getString(JobConstant.KEY_VIEW_NAME));
      TempViewProcessorWrapper result = builder.build();
      return result;
    } catch (Exception ex) {
      logger.error("Exception occurred when decoding temporary view processor: {}, error: {}", conf,
          ex.toString());
      throw new DecodeProcessorException();
    }
  }

}
