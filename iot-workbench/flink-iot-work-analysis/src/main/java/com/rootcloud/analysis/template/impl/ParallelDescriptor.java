/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.template.impl;

import static com.rootcloud.analysis.core.enums.DataTemplateEnum.PARALLEL;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import com.rootcloud.analysis.template.IDescriptor;

import com.rootcloud.analysis.template.ServiceTemplate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@ServiceTemplate(template = PARALLEL)
public class ParallelDescriptor implements IDescriptor {

  private static Logger logger = LoggerFactory.getLogger(ParallelDescriptor.class);
  private static final String LAYER_NAME = "layer_name";
  private static final String EQUIP_CODES = "equip_codes";
  private static final String ID = "_id";
  private static final String PARENT_ID = "parent_id";

  private Map<Object, Map<String, Object>> result = new HashMap<>();


  @Override
  public Map<Object, Map<String, Object>> parse(String content, String keyAttr) {
    JSONArray confArray = JSONArray.parseArray(content);

    Map<String, JSONObject> confMap = new HashMap<>();
    List<JSONObject> equipContainerList = new ArrayList<>();
    for (int index = 0; index < confArray.size(); index++) {
      JSONObject confObject = (JSONObject) confArray.get(index);
      if (confObject.containsKey(EQUIP_CODES)) {
        equipContainerList.add(confObject);
      } else {
        confMap.put(confObject.getString(ID), confObject);
      }
    }

    for (JSONObject equipContainer : equipContainerList) {
      Map<String, Object> orgMap = new HashMap<>();
      orgMap.put(equipContainer.getString(LAYER_NAME), equipContainer.getString(ID));
      String parentId = equipContainer.getString(PARENT_ID);
      while (!parentId.isEmpty()) {
        JSONObject parentObject = confMap.get(parentId);
        orgMap.put(parentObject.getString(LAYER_NAME), parentObject.getString(ID));
        parentId = parentObject.getString(PARENT_ID);
      }

      JSONArray equipArray = equipContainer.getJSONArray(EQUIP_CODES);
      for (int index = 0; index < equipArray.size(); index++) {
        String equipcode = (String) equipArray.get(index);
        result.put(equipcode, orgMap);
      }
    }

    return result;
  }

}
