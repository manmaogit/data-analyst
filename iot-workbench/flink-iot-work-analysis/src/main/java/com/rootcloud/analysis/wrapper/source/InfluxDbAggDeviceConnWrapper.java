/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.wrapper.source;

import static com.rootcloud.analysis.core.constants.JobConstant.INFLUXDB_TIME_COLUMN_NAME;
import static com.rootcloud.analysis.core.constants.SchemaConstant.DEVICE_ID;
import static com.rootcloud.analysis.core.constants.SchemaConstant.DEVICE_TYPE_ID;
import static com.rootcloud.analysis.core.constants.SchemaConstant.SINGLE_QUOTE;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.google.common.base.Strings;
import com.google.common.collect.Maps;
import com.rootcloud.analysis.common.entity.DeviceMapping;
import com.rootcloud.analysis.common.entity.RcUnit;
import com.rootcloud.analysis.core.constants.CommonConstant;
import com.rootcloud.analysis.core.constants.SchemaConstant;
import com.rootcloud.analysis.core.enums.HistorianAggFunctionEnum;
import com.rootcloud.analysis.core.enums.HistorianDataTypeEnum;
import com.rootcloud.analysis.core.enums.HistorianGroupByTimeTypeEnum;
import com.rootcloud.analysis.entity.InfluxDbQueryResult;
import com.rootcloud.analysis.exception.SourceReadDataException;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import lombok.Getter;
import org.apache.commons.collections.CollectionUtils;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Getter
public class InfluxDbAggDeviceConnWrapper extends InfluxDbDeviceConnBaseWrapper {

  private static final Logger logger = LoggerFactory.getLogger(InfluxDbAggDeviceConnWrapper.class);

  private final int startOfHour;
  private final String startTime;
  private final String endTime;
  private final int groupByTimeScope;
  private final String groupByTimeType;
  private final List<DeviceMapping> deviceMappings;
  private Set<String> ignoredFieldNames;

  /**
   * InfluxDbWithAggConnWrapper.
   */
  public InfluxDbAggDeviceConnWrapper(String url, String username, String password,
                                      String database, String tenantId,
                                      int timeOutSeconds, String timezone,
                                      JSONArray schema, int startOfHour,
                                      String startTime, String endTime,
                                      int groupByTimeScope, String groupByTimeType,
                                      Set<String> ignoredFieldNames,
                                      List<DeviceMapping> deviceMappings) {
    super(url, username, password, database, tenantId,
        timeOutSeconds, timezone, schema, deviceMappings);
    this.startOfHour = startOfHour;
    this.startTime = startTime;
    this.endTime = endTime;
    this.groupByTimeScope = groupByTimeScope;
    this.groupByTimeType = groupByTimeType;
    this.ignoredFieldNames = ignoredFieldNames;
    this.deviceMappings = deviceMappings;
  }

  @Override
  public Set<RcUnit> getData() throws Exception {
    Set<RcUnit> outPutSet = new HashSet<>();
    if (CollectionUtils.isEmpty(getDeviceMappings())) {
      return outPutSet;
    }
    CloseableHttpClient httpClient = null;
    try {
      httpClient = HttpClients.createDefault();
      for (DeviceMapping deviceMapping : getDeviceMappings()) {
        String querySql = this.buildQuerySql(deviceMapping);
        List<InfluxDbQueryResult.Series> seriesList = getInfluxDbData(httpClient, querySql);
        Set<RcUnit> rcUnitResult = parseData2RcUnit(deviceMapping, seriesList);
        outPutSet.addAll(rcUnitResult);
      }
    } catch (Exception ex) {
      logger.error("InfluxDbWithAggConnWrapper getData Exception : ", ex);
      throw new SourceReadDataException(
          String.format("InfluxDb getData from %s failed：%s, URI： %s , deviceMappings： %s ",
              getDatabase(), ex, getUrl(), getDeviceMappings()));
    } finally {
      if (httpClient != null) {
        httpClient.close();
      }
    }
    return outPutSet;
  }

  /**
   * 拼接查询SQL.
   */
  protected String buildQuerySql(DeviceMapping deviceMapping) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("select ");
    // Key：原始字段名 Value：加了后缀和聚合函数的字段名.
    Map<String, String> columns = getQuerySqlColumns();
    if (columns.isEmpty()) {
      stringBuilder.append(" * ");
    } else {
      columns.forEach((key, value) -> {
        stringBuilder.append(value).append(" as ")
            .append(SchemaConstant.DOUBLE_QUOTE).append(key).append(SchemaConstant.DOUBLE_QUOTE)
            .append(CommonConstant.COMMA);
      });
      stringBuilder.deleteCharAt(stringBuilder.length() - 1);
    }
    String table = "type_" + getTenantId() + "_" + deviceMapping.getDeviceTypeId();
    stringBuilder.append(" from ").append(table).append(" where ");
    // 设备ID过滤.
    List<String> deviceIds = deviceMapping.getDeviceIds();
    if (CollectionUtils.isNotEmpty(deviceIds)) {
      stringBuilder.append("(");
      for (int i = 0; i < deviceIds.size(); i++) {
        if (i > 0) {
          stringBuilder.append(" or ");
        }
        stringBuilder.append(SchemaConstant.DEVICE_ID).append(" = ")
            .append(SINGLE_QUOTE).append(deviceIds.get(i)).append(SINGLE_QUOTE);
      }
      stringBuilder.append(") and ");
    }
    // 时间戳过滤.
    stringBuilder.append(INFLUXDB_TIME_COLUMN_NAME).append(" >= '").append(startTime)
        .append("' and ").append(INFLUXDB_TIME_COLUMN_NAME).append(" < '")
        .append(endTime).append("'")
        .append(" group by __deviceId__, time(").append(groupByTimeScope);
    if (HistorianGroupByTimeTypeEnum.HOUR.name().equals(groupByTimeType)) {
      stringBuilder.append("h");
    } else {
      stringBuilder.append("m");
    }
    stringBuilder.append(",").append(startOfHour).append("h)");
    return stringBuilder.toString();
  }

  /**
   * 获取SQL使用的查询字段集合.
   * @return
   */
  private Map<String, String> getQuerySqlColumns() {
    // Key：原始字段名 Value：加了后缀和聚合函数的字段名.
    Map<String, String> columns = new HashMap<>();
    for (Object obj : getSchema()) {
      JSONObject jsonObj = (JSONObject) obj;
      String originalColumn =
          Strings.isNullOrEmpty(jsonObj.getString(SchemaConstant.ATTR_ORIGINAL_NAME))
              ? jsonObj.getString(SchemaConstant.ATTR_NAME)
              : jsonObj.getString(SchemaConstant.ATTR_ORIGINAL_NAME);
      String dataType = Strings.isNullOrEmpty(
          jsonObj.getString(SchemaConstant.ATTR_OLD_DATA_TYPE))
          ? jsonObj.getString(SchemaConstant.ATTR_DATA_TYPE)
          : jsonObj.getString(SchemaConstant.ATTR_OLD_DATA_TYPE);
      // 聚合函数.
      String aggFunction = jsonObj.getString(SchemaConstant.AGG_FUNCTION);
      String columnWithSuffix;
      if (ignoredFieldNames.contains(originalColumn)) {
        if (HistorianAggFunctionEnum.IGNORED.name().equals(aggFunction)) {
          continue;
        } else {
          columnWithSuffix = originalColumn;
        }
      } else {
        // ECA InfluxDB会对字段KEY额外进行类型的拼接.
        String influxDbDataType = HistorianDataTypeEnum.getHistorianType(dataType);
        columnWithSuffix = originalColumn + "$" + influxDbDataType;
      }
      columns.put(originalColumn, aggFunction + "("
          + SchemaConstant.DOUBLE_QUOTE + columnWithSuffix + SchemaConstant.DOUBLE_QUOTE + ")");
    }
    return columns;
  }

  /**
   * 解析InfluxDB数据，转换为RcUnit.
   */
  private Set<RcUnit> parseData2RcUnit(DeviceMapping deviceMapping,
                                       List<InfluxDbQueryResult.Series> seriesList) {
    Set<RcUnit> outPutSet = new HashSet<>();
    for (InfluxDbQueryResult.Series series : seriesList) {
      List<String> columns = series.getColumns();
      Iterator<List<Object>> it = series.getValues().iterator();
      while (it.hasNext()) {
        List<Object> valueList = it.next();
        if (columns.size() != valueList.size()) {
          throw new SourceReadDataException(
              "parseData2RcUnit fail：model（" + deviceMapping.getDeviceTypeId()
                  + "）Data exception! There may be a wrong column. Please replace "
                  + "the model or contact the administrator to verify the influxdb partition data。"
                  + "，columns.size()=" + columns.size()
                  + "，valueList.size()=" + valueList.size());
        }
        Map<String, Object> data = Maps.newHashMap();
        // 模型ID.
        data.put(DEVICE_TYPE_ID, deviceMapping.getDeviceTypeId());
        // 设备ID.
        data.put(DEVICE_ID, series.getTags().get("__deviceId__"));

        for (int i = 0; i < valueList.size(); i++) {
          checkColumnData(columns.get(i), valueList.get(i), data);
        }
        RcUnit result = RcUnit.valueOfDataTypeInfo(data, getSchema());
        // 补全内置字段的值.
        String deviceId = series.getTags().get("__deviceId__");
        fillDefaultFieldValue(result, deviceMapping, deviceId);
        outPutSet.add(result);
      }
    }
    return outPutSet;
  }

}
