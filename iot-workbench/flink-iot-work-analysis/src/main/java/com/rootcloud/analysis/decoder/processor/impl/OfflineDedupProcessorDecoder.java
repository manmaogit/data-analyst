/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.decoder.processor.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.rootcloud.analysis.context.IotWorkRuntimeDecodeContext;
import com.rootcloud.analysis.core.constants.JobConstant;
import com.rootcloud.analysis.core.enums.KeepPolicyEnum;
import com.rootcloud.analysis.core.enums.ProcessTypeEnum;
import com.rootcloud.analysis.decoder.processor.AbstractProcessorDecoder;
import com.rootcloud.analysis.decoder.processor.ProcessorDecoder;
import com.rootcloud.analysis.exception.DecodeProcessorException;
import com.rootcloud.analysis.wrapper.processor.AbstractProcessorWrapper;
import com.rootcloud.analysis.wrapper.processor.OfflineDedupProcessorWrapper;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by 王纯超 on 2021/9/1.
 */
@ProcessorDecoder(processType = ProcessTypeEnum.OFFLINE_DEDUP)
public class OfflineDedupProcessorDecoder extends AbstractProcessorDecoder {
  private static Logger logger = LoggerFactory.getLogger(
      OfflineDedupProcessorDecoder.class);

  @Override
  protected AbstractProcessorWrapper decodeProcessParams(JSONObject conf) {
    try {
      AbstractProcessorWrapper wrapper = OfflineDedupProcessorWrapper.builder()
          .schema(inferSchema(conf))
          .viewName(conf.getString(JobConstant.KEY_NODE_ID))
          .priorTsViewName(conf.getString(JobConstant.KEY_PRIOR_NODE_ID) + conf
              .getString(JobConstant.KEY_DEDUP_SORT_BY) + "_TS")
          .keyFieldList(conf.getObject(JobConstant.KEY_DEDUP_KEY_FIELD_LIST, List.class))
          .sortBy(conf.getString(JobConstant.KEY_DEDUP_SORT_BY))
          .keep(KeepPolicyEnum.valueOf(conf.getString(JobConstant.KEY_DEDUP_KEEP)))
          .build();
      logger.debug("Decoding dedup process: {}", wrapper);
      return wrapper;
    } catch (Exception ex) {
      logger.error("Exception occurred when decoding dedup processor: {}", conf);
      throw new DecodeProcessorException();
    }
  }

  /**
   * Obtain output schema information from prior node
   * until reaching the input node. Throw DecodeProcessorException if failing.
   *
   * @param conf not null
   * @return
   */
  private JSONArray inferSchema(JSONObject conf) {
    if (!conf.containsKey(JobConstant.KEY_PRIOR_NODE_ID)) { //we have reached the source node
      throw new DecodeProcessorException();//and it does not define an output schema.
    }

    JSONObject priorNodeConf = IotWorkRuntimeDecodeContext
        .getNodeConf(conf.getString(JobConstant.KEY_PRIOR_NODE_ID));
    JSONArray priorNodeSchema = priorNodeConf.getJSONArray(JobConstant.KEY_SCHEMA);
    return priorNodeSchema == null ? inferSchema(priorNodeConf) : priorNodeSchema;
  }
}
