/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.log.loggingservice;

import com.alibaba.fastjson.JSON;
import com.rootcloud.analysis.core.enums.LogLogLevelEnum;
import com.rootcloud.analysis.core.graylog.LogVo;
import java.nio.charset.StandardCharsets;
import org.apache.flink.streaming.connectors.kafka.internals.FlinkKafkaInternalProducer;
import org.apache.kafka.clients.producer.Callback;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LoggingServiceUtil {

  private static Logger log = LoggerFactory.getLogger(LoggingServiceUtil.class);

  /**
   * grayLog 记录info级别的日志.
   */
  public static void info(LogVo logVo, FlinkKafkaInternalProducer kafkaLogMiddlewareResource) {
    logVo.setLogLevel(LogLogLevelEnum.INFO.getLogLevel());
    send(logVo, kafkaLogMiddlewareResource);
  }

  /**
   * grayLog 记录info级别的日志.
   */
  @Deprecated
  public static void info(LogVo logVo) {
    logVo.setLogLevel(LogLogLevelEnum.INFO.getLogLevel());
    send(logVo);
  }

  /**
   * grayLog 记录warn级别的日志.
   */
  public static void warn(LogVo logVo, FlinkKafkaInternalProducer kafkaLogMiddlewareResource) {
    logVo.setLogLevel(LogLogLevelEnum.WARN.getLogLevel());
    send(logVo, kafkaLogMiddlewareResource);
  }

  /**
   * grayLog 记录warn级别的日志.
   */
  @Deprecated
  public static void warn(LogVo logVo) {
    logVo.setLogLevel(LogLogLevelEnum.WARN.getLogLevel());
    send(logVo);
    log.warn(JSON.toJSONString(logVo));
  }

  /**
   * grayLog 记录error级别的日志.
   */
  public static void error(LogVo logVo, FlinkKafkaInternalProducer kafkaLogMiddlewareResource) {
    logVo.setLogLevel(LogLogLevelEnum.ERROR.getLogLevel());
    send(logVo, kafkaLogMiddlewareResource);
  }

  /**
   * grayLog 记录error级别的日志.
   */
  @Deprecated
  public static void error(LogVo logVo) {
    logVo.setLogLevel(LogLogLevelEnum.ERROR.getLogLevel());
    send(logVo);
    log.error(JSON.toJSONString(logVo));
  }

  /**
   * grayLog 记录alarm级别的日志,将会触发告警推送.
   */
  public static void alarm(LogVo logVo, FlinkKafkaInternalProducer kafkaLogMiddlewareResource) {
    error(logVo, kafkaLogMiddlewareResource);

  }

  /**
   * grayLog 记录alarm级别的日志,将会触发告警推送.
   */
  public static void alarm(LogVo logVo) {
    error(logVo);


  }

  private static void send(LogVo logVo) {
    log.info("LoggingService log but not send :{} ", JSON.toJSONString(logVo));

  }

  private static void send(LogVo logVo, FlinkKafkaInternalProducer kafkaLogMiddlewareResource) {

    if (kafkaLogMiddlewareResource == null) {
      log.warn("kafkaLogMiddlewareResource is null!");
      return;
    }
    log.info("LoggingService send with kafkaLogMiddlewareResource:{} kafkaLogMiddlewareResource {}",
        kafkaLogMiddlewareResource.toString(), JSON.toJSONString(logVo));
    String valueString = "";
    try {
      valueString = JSON.toJSONString(LoggingServiceEncoder.encode(logVo));
      log.info("KafkaLogMiddlewareResource{}  msg:{}", kafkaLogMiddlewareResource.toString(), valueString);
    } catch (Exception e) {
      log.error("switch views：" + e.getMessage());
      return;
    }
    log.info("Start pushing logs：{} --{}", valueString, kafkaLogMiddlewareResource.toString());
    try {
      kafkaLogMiddlewareResource.send(new ProducerRecord<>(logVo.getKafkaLoggingServiceTopic(), null,
                      System.currentTimeMillis(), logVo.getTenantId().getBytes(StandardCharsets.UTF_8),
                      valueString.getBytes(StandardCharsets.UTF_8)),
          new Callback() {
            @Override
            public void onCompletion(RecordMetadata metadata, Exception exception) {
              if (metadata != null) {
                log.info("LoggingServiceUtil send with offset {}", metadata.offset());
              } else {
                log.info("LoggingServiceUtil send with exception {}",
                    exception != null ? exception.toString() : "");
              }

            }
          }
      );
    } catch (Exception e) {
      log.error("LoggingServiceUtil send error:msg{}", e.toString());
    }

  }
}
