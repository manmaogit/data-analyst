/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.decoder.function.filter.impl;

import com.alibaba.fastjson.JSONObject;
import com.rootcloud.analysis.core.constants.FilterConstant;
import com.rootcloud.analysis.core.enums.FilterRuleEnum;
import com.rootcloud.analysis.decoder.function.filter.AbstractFilterDecoder;
import com.rootcloud.analysis.decoder.function.filter.FilterDecoder;
import com.rootcloud.analysis.wrapper.function.filter.AbstractFilterWrapper;
import com.rootcloud.analysis.wrapper.function.filter.InListFilterWrapper;
import com.rootcloud.analysis.wrapper.function.filter.IntersectFilterWrapper;

@FilterDecoder(filterRule = FilterRuleEnum.INTERSECT)
public class IntersectFilterDecoder extends AbstractFilterDecoder {

  @Override
  public AbstractFilterWrapper decode(JSONObject config) {
    return IntersectFilterWrapper.builder().attribute(config.getString(FilterConstant.ATTRIBUTE))
        .values(config.getJSONArray(FilterConstant.VALUES).toJavaList(Object.class)).build();
  }
}
