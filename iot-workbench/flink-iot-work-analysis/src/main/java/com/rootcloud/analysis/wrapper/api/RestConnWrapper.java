/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.wrapper.api;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.Maps;
import com.rootcloud.analysis.common.entity.RcUnit;
import com.rootcloud.analysis.common.entity.RestNodeVo;
import com.rootcloud.analysis.core.constants.SchemaConstant;
import com.rootcloud.analysis.core.enums.MethodEnum;
import com.rootcloud.analysis.core.util.DataUtil;
import com.rootcloud.analysis.exception.SourceReadDataException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RestConnWrapper extends ApiConnWrapper {

  private static final Logger logger = LoggerFactory.getLogger(RestConnWrapper.class);
  private final MethodEnum method;
  private final String url;
  private final JSONArray headers;
  private final JSONArray parameters;
  private final JSONObject body;
  /**
   * originalColumn最深层级.
   */
  private static final String REST_NODE_DATA_QUEUE = "__@data_path@__";

  /**
   * HTTP超时时长（单位：毫秒）.
   */
  private int timeOutMilliSeconds;

  /**
   * HTTP查询分页大小.
   */
  private int pageLimit;

  /**
   * 构造方法.
   */
  public RestConnWrapper(String url, JSONArray schema, MethodEnum method, JSONArray headers,
                         JSONArray parameters, JSONObject body, int timeOutSeconds,
                         int pageLimit) {
    super(schema);
    this.method = method;
    this.url = url;
    this.headers = headers;
    this.parameters = parameters;
    this.body = body;
    this.timeOutMilliSeconds = timeOutSeconds * 1000;
    this.pageLimit = pageLimit;
  }

  /**
   * 查询数据.
   */
  @Override
  public Set<RcUnit> getData() throws Exception {
    CloseableHttpResponse response = null;
    CloseableHttpClient httpclient = HttpClients.createDefault();
    final long begin = System.currentTimeMillis();
    Set<RcUnit> result = new HashSet<>();
    boolean searchContinue = true;
    int skip = 0;
    try {
      while (searchContinue) {
        // 分页查询.
        String url = String.format("%s&skip=%s&limit=%s", this.url, skip, pageLimit);
        response = invokeGet(httpclient, url);
        HttpEntity resEntity = response.getEntity();
        String bodyString = EntityUtils.toString(resEntity, "utf-8");
        if (response.getStatusLine().getStatusCode() == 200) {
          Set<RcUnit> rcUnits = declareJson(bodyString);
          if (CollectionUtils.isEmpty(rcUnits)) {
            searchContinue = false;
          } else {
            result.addAll(rcUnits);
            skip += pageLimit;
          }

        } else {
          long cost = System.currentTimeMillis() - begin;
          String errMessage = String.format("Consumption restapi data exception："
                  + "url = %s, timeOutSeconds = %s ms, cost = %s ms, response message: %s",
              url, timeOutMilliSeconds, cost, bodyString);
          logger.error(errMessage);
          throw new SourceReadDataException(errMessage);
        }
      }
    } catch (Exception e) {
      throw e;
    } finally {
      if (response != null) {
        response.close();
      }
      httpclient.close();
    }
    return result;
  }

  @Override
  public void close() {
    logger.info("RestConnWrapper closed!");

  }

  /**
   * 解析json.
   */
  public Set<RcUnit> declareJson(String bodyString) {
    //以‘-’分割，当json解析到JSONarray就加一层  比如 0 -> 0-0,数字为JSONarray下标，用于保存字段与JSONarray的关系
    String path = "";
    //将所有返回转换为List进行处理
    if (bodyString.startsWith("{") && bodyString.endsWith("}")) {
      bodyString = StringUtils.join(new String[]{"[", bodyString, "]"}, "");
    }
    List<Object> body = JSONArray.parseArray(bodyString);
    //最终返回结果
    List<Map<String, RestNodeVo>> finalResult = new ArrayList<>();
    //字段与path的映射
    Map<String, String> schemaQueue = new HashMap<>();
    for (int index = 0; index < body.size(); index++) {
      //单条数据组合结果
      List<Map<String, RestNodeVo>> fillResult = new ArrayList<>();
      String pathTemp = path + "-" + index;
      Object rowData = body.get(index);

      List<JSONObject> rowDataJsonArray = new ArrayList<>();
      //循环获取jsonObject
      getObjects(rowData, rowDataJsonArray);
      for (int x = 0; x < rowDataJsonArray.size(); x++) {
        JSONObject row = rowDataJsonArray.get(x);
        Map<String, Object> data = Maps.newHashMap();
        pathTemp = pathTemp + "-" + x;
        for (String key : row.keySet()) {
          data.put(key, row.get(key));
        }
        Map rc = valueOfDeeply(data, getSchema(), pathTemp, schemaQueue);
        //开始处理输出
        //根据originalColumn层数和解析出的数量对schema排序
        List<Object> sortedSchema = sortSchema(getSchema(), rc);
        for (int y = 0; y < sortedSchema.size() - 1; y++) {

          JSONObject currOriginal = ((JSONObject) sortedSchema.get(y));
          JSONObject nextOriginal = ((JSONObject) sortedSchema.get(y + 1));
          packageData(fillResult, rc, currOriginal, nextOriginal, schemaQueue);
        }
      }

      finalResult.addAll(fillResult);
    }
    //处理最终输出结果
    List<Map<String, Object>> output = new ArrayList<>();
    convertOupPut(output, finalResult);
    Set<RcUnit> outPutSet = new HashSet<>();
    for (Map<String, Object> data : output) {
      outPutSet.add(RcUnit.valueOf(data));
    }
    return outPutSet;
  }

  /**
   * 循环获取JOSNObject.
   */
  private void getObjects(Object rowData, List<JSONObject> rowDataJsonArray) {

    if (rowData instanceof JSONObject) {
      rowDataJsonArray.add((JSONObject) rowData);
    } else {
      ((JSONArray) rowData).forEach(row -> {
        getObjects(row, rowDataJsonArray);
      });
    }
  }

  /**
   * rest get.
   */
  private CloseableHttpResponse invokeGet(CloseableHttpClient httpclient, String url)
      throws Exception {
    // 创建httpget.
    HttpGet httpget = new HttpGet(url);
    // 设置超时时长.
    RequestConfig requestConfig = RequestConfig.custom()
        .setConnectTimeout(timeOutMilliSeconds)
        .setConnectionRequestTimeout(timeOutMilliSeconds)
        .setSocketTimeout(timeOutMilliSeconds)
        .build();
    httpget.setConfig(requestConfig);

    for (int i = 0; i < headers.size(); i++) {
      httpget.setHeader(headers.getJSONObject(i).getString("key"),
          headers.getJSONObject(i).getString("value"));
    }
    // 执行get请求.
    CloseableHttpResponse response = httpclient.execute(httpget);
    return response;

  }

  /**
   * 将restNodeVo中的value取出.
   */
  private void convertOupPut(List<Map<String, Object>> output,
                             List<Map<String, RestNodeVo>> finalResult) {
    for (Map<String, RestNodeVo> map : finalResult) {
      Map<String, Object> mapTemp = new HashMap<>();
      for (String key : map.keySet()) {
        mapTemp.put(key, map.get(key).getValue());
      }
      output.add(mapTemp);
    }

  }

  /**
   * 两个originalColumn组装数据.
   */
  private void packageData(List<Map<String, RestNodeVo>> finalResult, Map result,
                           JSONObject currOriginal, JSONObject nextOriginal,
                           Map<String, String> schemaQueue) {
    Boolean relation = compareSchemaRelation(
        currOriginal.getString(SchemaConstant.ATTR_ORIGINAL_NAME),
        nextOriginal.getString(SchemaConstant.ATTR_ORIGINAL_NAME), schemaQueue);

    List<RestNodeVo> currAttrOutputList = (List<RestNodeVo>) result
        .get(currOriginal.getString(SchemaConstant.ATTR_NAME));
    List<RestNodeVo> nextAttrOutputList = (List<RestNodeVo>) result
        .get(nextOriginal.getString(SchemaConstant.ATTR_NAME));
    //判断是否为第一次组装
    if (finalResult.size() == 0) {
      for (RestNodeVo output : currAttrOutputList) {
        Map<String, RestNodeVo> data = new HashMap<>();
        data.put(currOriginal.getString(SchemaConstant.ATTR_NAME),
            output);
        data.put(REST_NODE_DATA_QUEUE, new RestNodeVo(output.getPath(), output.getPath()));
        finalResult.add(data);
      }
    }
    if (relation) {
      //有关联组合
      List<Map<String, RestNodeVo>> addResults = new ArrayList<>();
      for (RestNodeVo nextAttrObj : nextAttrOutputList) {
        Boolean exist = false;
        //
        // 遍历现有结果来判断该对新的数据做什么处理.
        // 如果在json中关系在不同数据链路则应该构造他们的并集
        //如果在json中关系在同一条数据链路中，则应该直接将值填充到对应位置中
        //
        for (Map<String, RestNodeVo> resultEle : finalResult) {
          if (isPathStartWith(resultEle.get(REST_NODE_DATA_QUEUE).getPath(),
              nextAttrObj.getPath())
              || isPathStartWith(nextAttrObj.getPath(),
              resultEle.get(REST_NODE_DATA_QUEUE).getPath())) {
            resultEle.put(nextOriginal.getString(SchemaConstant.ATTR_NAME),
                nextAttrObj);
            if (resultEle.get(REST_NODE_DATA_QUEUE) != null) {
              if (nextAttrObj.getPath().length() > resultEle.get(REST_NODE_DATA_QUEUE)
                  .getPath()
                  .length()) {
                resultEle.get(REST_NODE_DATA_QUEUE).setPath(nextAttrObj.getPath());
              }
            }
            exist = true;
          }
        }

        //可能会出现层数更深original后面处理，finalResult数据数量比original数量少

        if (!exist) {
          Map<String, RestNodeVo> dataTemp;
          if (finalResult.isEmpty()) {
            dataTemp = new HashMap<>();
          } else {
            dataTemp = mapCopy(finalResult.get(0));
          }
          Map<String, RestNodeVo> newDataEleMap = mapCopy(dataTemp);
          newDataEleMap.put(nextOriginal.getString(SchemaConstant.ATTR_NAME),
              nextAttrObj);
          if (newDataEleMap.get(REST_NODE_DATA_QUEUE) != null) {
            if (nextAttrObj.getPath().length() > newDataEleMap.get(REST_NODE_DATA_QUEUE)
                .getPath()
                .length()) {
              newDataEleMap.get(REST_NODE_DATA_QUEUE).setPath(nextAttrObj.getPath());
            }
          }
          addResults.add(newDataEleMap);

        }

      }
      finalResult.addAll(addResults);
    } else {
      //无关联则取并集
      List<Map<String, RestNodeVo>> finalResultTemp = new ArrayList<>();
      for (Map<String, RestNodeVo> resultEle : finalResult) {
        for (RestNodeVo nextAttrObj : nextAttrOutputList) {
          Map<String, RestNodeVo> newResultEle = mapCopy(resultEle);
          newResultEle.put(nextOriginal.getString(SchemaConstant.ATTR_NAME), nextAttrObj);
          newResultEle.get(REST_NODE_DATA_QUEUE).setPath(nextAttrObj.getPath());
          finalResultTemp.add(newResultEle);
        }
      }
      if (nextAttrOutputList.size() > 0) {
        finalResult.clear();
      }
      finalResult.addAll(finalResultTemp);
    }
  }

  /**
   * 判断path是否具有包含关系. 1-1-1-1   1-1   true; 1-1-1    1-1-1-1  false.
   */
  public Boolean isPathStartWith(String first, String second) {
    if (first == null || second == null) {
      return false;
    }
    if (first.equals(second)) {
      return true;
    }
    if (first.length() > second.length()) {
      return first.startsWith(second) && first.substring(second.length()).startsWith("-");
    }
    return false;
  }

  Map<String, RestNodeVo> mapCopy(Map<String, RestNodeVo> resultEle) {
    Map<String, RestNodeVo> ele = new HashMap<>();
    for (String key : resultEle.keySet()) {
      ele.put(key, resultEle.get(key));
    }
    return ele;
  }

  /**
   * 判断orginalName之间是否有联系.  2.当next的路径只有一级 3.当next为curr的父级路径 4.curr==next.
   */
  private Boolean compareSchemaRelation(String curr, String next,
                                        Map<String, String> nextOriginal) {
    String currPath = nextOriginal.get(curr);
    String nextPath = nextOriginal.get(next);

    return (compare(curr, next) || compare(next, curr))
        || currPath.split("\\-").length == nextPath.split("\\-").length || (
        currPath.startsWith(nextPath) || nextPath.startsWith(currPath));
  }

  private Boolean compare(String curr, String next) {
    //进过排序的schema
    if (next.split("\\|").length <= 2) {
      //如果为第一级或第二级字段
      return true;
    }
    //根据层级判断
    if (curr.split("\\|").length == next.split("\\|").length) {

      return curr.compareTo(next) == 0 || ((curr.substring(0, curr.lastIndexOf("|")))
          .equals(next.substring(0, next.lastIndexOf("|"))));
    } else {
      if (curr.startsWith(next)) {
        //这种情况违背originalName类型必须为String
        return true;
      }

      return curr.startsWith(next.substring(0, next.lastIndexOf("|")));

    }


  }

  /**
   * 输出字段按照数据长度和深度排序.
   */
  private List<Object> sortSchema(JSONArray schema, Map rc) {
    return schema.stream().sorted((curr, next) -> {
          String currName = ((JSONObject) curr).getString(SchemaConstant.ATTR_NAME);
          String nextName = ((JSONObject) next).getString(SchemaConstant.ATTR_NAME);
          String currOriginalName = ((JSONObject) curr)
              .getString(SchemaConstant.ATTR_ORIGINAL_NAME);
          String nextOriginalName = ((JSONObject) next)
              .getString(SchemaConstant.ATTR_ORIGINAL_NAME);
          if (((List<RestNodeVo>) rc.get(currName)).size() > ((List<RestNodeVo>) rc.get(nextName))
              .size()) {
            return -1;
          } else if (((List<RestNodeVo>) rc.get(currName)).size() < ((List<RestNodeVo>) rc
              .get(nextName))
              .size()) {
            return 1;
          } else {
            if (currOriginalName.split("\\|").length > nextOriginalName.split("\\|").length) {
              return -1;
            } else if (
                currOriginalName.split("\\|").length == nextOriginalName.split("\\|").length) {
              return currOriginalName.compareTo(nextOriginalName) * (-1);
            } else {
              return 1;
            }
          }
        }

    ).collect(Collectors.toList());

  }

  /**
   * 解析复杂结构的json.
   *
   * @param map         解析的json
   * @param schema      输出映射
   * @param pathTemp    路径
   * @param schemaQueue 输出与路径的映射
   */
  private Map valueOfDeeply(Map map, List<Object> schema, String pathTemp,
                            Map<String, String> schemaQueue) {
    Preconditions.checkArgument(schema != null && !schema.isEmpty(), "Schema is null or empty.");
    Map<String, List<RestNodeVo>> data = Maps.newHashMap();
    JSONObject jsonObject = JSONObject.parseObject(JSONObject.toJSONString(map));
    for (Object obj : schema) {
      JSONObject schemaObject = (JSONObject) obj;
      String originalColumn =
          Strings.isNullOrEmpty(schemaObject.getString(SchemaConstant.ATTR_ORIGINAL_NAME))
              ? schemaObject.getString(SchemaConstant.ATTR_NAME)
              : schemaObject.getString(SchemaConstant.ATTR_ORIGINAL_NAME);
      schemaQueue.put(originalColumn, pathTemp);
      String column = schemaObject.getString(SchemaConstant.ATTR_NAME);
      if (originalColumn.contains(SchemaConstant.OR)) {
        List<RestNodeVo> listObj = dealResponse(originalColumn, JSON.toJSONString(jsonObject),
            pathTemp, schemaQueue);
        listObj = listObj.stream().filter(o -> {
          return o.getValue() != null;
        }).map(o -> {
          return new RestNodeVo(o.getValue(), o.getPath(),
              originalColumn);
        }).collect(Collectors.toList());
        data.put(column, listObj);
      } else {
        data.put(column,
            Arrays.asList(new RestNodeVo(DataUtil.convert(jsonObject.get(originalColumn),
                schemaObject.getString(SchemaConstant.ATTR_DATA_TYPE)), pathTemp,
                originalColumn)));
      }
    }

    return data;
  }

  /**
   * 解析单个字段的数据.
   *
   * @param originalColumn original_column
   * @param responseString 待解析的json
   */
  private List<RestNodeVo> dealResponse(String originalColumn,
                                        String responseString, String path,
                                        Map<String, String> schemaQueue) {
    if (responseString.startsWith("{") && responseString.endsWith("}")) {
      responseString = StringUtils.join(new String[]{"[", responseString, "]"}, "");
    }
    JSONArray obj = JSONArray.parseArray(responseString);
    List<RestNodeVo> jnvList = new ArrayList<>();
    Object[] objArray = obj.toArray();
    for (int i = 0; i < objArray.length; i++) {
      RestNodeVo jn = new RestNodeVo();
      jn.setValue(objArray[i]);
      jn.setPath(path + "-" + i);
      schemaQueue.put(originalColumn, path + "-" + i);
      jnvList.add(jn);
    }

    String[] params = originalColumn.split("\\|");
    List<String> paramsList = new LinkedList<>(Arrays.asList(params));
    List<RestNodeVo> result = new ArrayList<>();
    getValue(jnvList, paramsList, result, originalColumn, schemaQueue);

    return result;
  }

  /**
   * 循环获取值.
   */
  private void getValue(List<RestNodeVo> param, List<String> paramsList,
                        List<RestNodeVo> result, String s,
                        Map<String, String> schemaQueue) {

    for (int i = 0; i < param.size(); i++) {
      RestNodeVo restNode = param.get(i);
      if (restNode == null || restNode.getValue() == null) {
        continue;
      }
      //备份original_column
      List<String> newParamList = new LinkedList<String>() {
        {
          addAll(paramsList);
        }
      };

      Iterator<String> paramIte = newParamList.iterator();
      String paramStr = paramIte.next();
      Object a = restNode.getValueValue(paramStr);
      newParamList.remove(paramStr);
      List<RestNodeVo> data = new ArrayList<>();
      if (newParamList.size() > 0 && a instanceof JSONArray) {
        List<RestNodeVo> jsonRestNodeList = new ArrayList<>();
        Object[] objArray = ((JSONArray) a).toArray();
        for (int y = 0; y < objArray.length; y++) {
          RestNodeVo node = new RestNodeVo();
          node.setValue(objArray[y]);
          node.setPath(restNode.getPath() + "-" + y);
          jsonRestNodeList.add(node);
          schemaQueue.put(s, restNode.getPath() + "-" + y);
        }
        data.addAll(jsonRestNodeList);
      } else {

        data.add(new RestNodeVo(a, restNode.getPath()));
      }
      if (newParamList.size() > 0) {
        getValue(data, newParamList, result, s, schemaQueue);
      } else {
        result.addAll(data);
      }

    }

  }
}
