/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.template.impl;

import static com.rootcloud.analysis.core.enums.DataTemplateEnum.TAG;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import com.rootcloud.analysis.template.IDescriptor;

import com.rootcloud.analysis.template.ServiceTemplate;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@ServiceTemplate(template = TAG)
public class TagDescriptor implements IDescriptor {

  private static Logger logger = LoggerFactory.getLogger(TagDescriptor.class);
  private static final String DEVICES = "devices";
  private static final String TAGS = "tags";
  private static final String TAG_TYPE = "tag_type";
  private static final String TAG_NAME = "tag_name";

  private Map<Object, Map<String, Object>> result = new HashMap<>();


  @Override
  public Map<Object, Map<String, Object>> parse(String content, String keyAttr) {
    JSONObject confObj = JSON.parseObject(content);
    if (!confObj.containsKey(DEVICES)) {
      logger.error("error content:{}", content);
      return null;
    }

    JSONArray deviceList = confObj.getJSONArray(DEVICES);
    for (int index = 0; index < deviceList.size(); index++) {
      JSONObject deviceObj = deviceList.getJSONObject(index);

      Map<String, Object> tagMap = new HashMap<>();
      JSONArray tagList = deviceObj.getJSONArray(TAGS);
      for (int index2 = 0; index2 < tagList.size(); index2++) {
        JSONObject tagObject = tagList.getJSONObject(index2);
        tagMap.put(tagObject.getString(TAG_TYPE), tagObject.getString(TAG_NAME));
      }

      result.put(deviceObj.getString(keyAttr), tagMap);

    }

    return result;
  }

}
