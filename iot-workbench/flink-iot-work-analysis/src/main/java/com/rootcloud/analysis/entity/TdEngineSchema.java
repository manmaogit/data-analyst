/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.entity;

import java.io.Serializable;
import java.util.List;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * TdEngine相关的元数据信息，供查询拼接SQL使用.
 * @CreatedBy: zexin.huang
 * @Date: 2022/5/11 15:37
 */
@ToString
@Getter
@Setter
public class TdEngineSchema implements Serializable {

  /**
   * 字段属性集合.
   */
  private List<TdEngineSchemaField> fields;

  /**
   * 超级表名.
   */
  private String stabeName;

  /**
   * 设备ID集合.
   * 如果没有指定设备ID则包含该模型下的所有设备.
   */
  private List<String> deviceIds;

}
