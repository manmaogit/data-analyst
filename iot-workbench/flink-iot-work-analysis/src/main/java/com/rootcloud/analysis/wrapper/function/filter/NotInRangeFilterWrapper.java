/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.wrapper.function.filter;

import com.rootcloud.analysis.common.entity.RcUnit;
import java.util.List;
import lombok.Builder;
import lombok.ToString;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Builder
@ToString(callSuper = true)
public class NotInRangeFilterWrapper extends AbstractFilterWrapper {

  private static Logger logger = LoggerFactory.getLogger(NotInRangeFilterWrapper.class);

  private String attribute;

  public List<Object> values;


  @Override
  public boolean filter(RcUnit record) {
    boolean result = false;
    Object upperLimit = new Object();
    Object lowerLimit = new Object();
    try {

      lowerLimit = values.get(0);
      upperLimit = values.get(1);
      if (record.getData().containsKey(attribute)) {
        Object attrValue = record.getAttr(attribute);
        if (attrValue != null
            && (Double.compare(Double.parseDouble(String.valueOf(attrValue)),
            Double.parseDouble((String) upperLimit))
            > 0
            ||
            Double.compare(Double.parseDouble(String.valueOf(attrValue)),
                Double.parseDouble((String) lowerLimit))
                < 0)) {
          result = true;
        }
      }

    } catch (Exception ex) {
      logger.error("Exception occurred when filtering data: {}， exception message: {}",
          record.toString(), ex.getMessage(), ex);
      result = false;
    }
    logger.debug(
        "'NOTINRANGE' filter result: {}, attribute: {}, record: {}, upperLimit: {},lowerLimit:{}",
        result, attribute, record.toString(), upperLimit.toString(), lowerLimit.toString());
    return result;
  }

}
