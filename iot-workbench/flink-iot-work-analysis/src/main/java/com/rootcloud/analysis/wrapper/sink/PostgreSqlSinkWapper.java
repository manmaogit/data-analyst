/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.wrapper.sink;

import static com.rootcloud.analysis.core.enums.InsertTypeEnum.INSERT;
import static com.rootcloud.analysis.core.enums.InsertTypeEnum.UPSERT;

import com.google.common.base.Strings;
import com.google.common.collect.Maps;
import com.rootcloud.analysis.common.context.IotWorkRuntimeContext;
import com.rootcloud.analysis.common.entity.RcUnit;
import com.rootcloud.analysis.core.constants.GrayLogConstant;
import com.rootcloud.analysis.core.constants.JobConstant;
import com.rootcloud.analysis.core.constants.SchemaConstant;
import com.rootcloud.analysis.core.constants.ShortMessageConstant;
import com.rootcloud.analysis.core.enums.GrayLogResultEnum;
import com.rootcloud.analysis.core.enums.GraylogLogTypeEnum;
import com.rootcloud.analysis.core.graylog.LogVo;
import com.rootcloud.analysis.log.loggingservice.LoggingServiceUtil;
import com.rootcloud.analysis.wrapper.function.jdbc.JdbcRichSinkFunction;
import com.rootcloud.analysis.wrapper.jdbc.PostgreSqlConnWrapper;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.metrics.Counter;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PostgreSqlSinkWapper extends JdbcSinkWrapper {

  private static Logger logger = LoggerFactory.getLogger(PostgreSqlSinkWapper.class);

  public PostgreSqlSinkWapper(Builder builder) {
    super(builder);
  }

  private static final String SHOW_COLUMN = "SELECT "
      + "    a.attname as Field,"
      + "    concat_ws('',t.typname,SUBSTRING(format_type(a.atttypid,a.atttypmod)"
      + " from '\\(.*\\)')) as Type "
      + "FROM "
      + "    pg_class as c,"
      + "    pg_attribute as a, "
      + "    pg_type as t "
      + "WHERE "
      + "    c.relname = '%s' "
      + "    and a.atttypid = t.oid "
      + "    and a.attrelid = c.oid "
      + "    and a.attnum>0";

  @Override
  public void registerOutput(IotWorkRuntimeContext context) {
    DataStream<RcUnit> priorDataStream = context.getObjectFromContainer(getPriorNodeId());
    logger.info(getNodeName() + ":" + getNodeId());
    priorDataStream
        .addSink(new PostgreSqlRichSinkFunction(getJobId(), getTenantId(), getLogType(),
            getKafkaLoggingServiceServers(), getKafkaLoggingServiceTopics()))
        .uid(getNodeId())
        .setParallelism(getParallelism())
        .name(getNodeName());
  }

  public static class Builder extends JdbcSinkWrapper.Builder {

    /**
     * build.
     */
    public PostgreSqlSinkWapper build() {
      super.build();
      return new PostgreSqlSinkWapper(this);
    }
  }

  private class PostgreSqlRichSinkFunction extends JdbcRichSinkFunction<RcUnit> {

    private final String jobId;
    private final String tenantId;
    private Counter counterRecordsOut;
    private final GraylogLogTypeEnum logType;
    private final String kafkaLoggingServiceServers;
    private final String kafkaLoggingServiceTopics;

    private PostgreSqlRichSinkFunction(String jobId, String tenantId, GraylogLogTypeEnum logType,
                                       String kafkaLoggingServiceServers,
                                       String kafkaLoggingServiceTopics) {
      super(new PostgreSqlConnWrapper("org.postgresql.Driver",
          uri, username, password,
          "select 1", table, null, 0, null));
      this.jobId = jobId;
      this.tenantId = tenantId;
      this.logType = logType;
      this.kafkaLoggingServiceServers = kafkaLoggingServiceServers;
      this.kafkaLoggingServiceTopics = kafkaLoggingServiceTopics;
    }

    // Postgre Sql table schema
    private Map<String, String> schema;

    @Override
    public void open(Configuration parameters) throws Exception {
      super.open(parameters);
      initSchema();
      counterRecordsOut = getRuntimeContext().getMetricGroup()
          .counter(JobConstant.FLINK_METRIC_NUM_RECORDS_OUT_OVERWRITE);
      openProducer(kafkaLoggingServiceServers);
    }

    private void initSchema() throws SQLException {
      schema = Maps.newHashMap();
      String format = String.format(SHOW_COLUMN, table);
      try (PreparedStatement preparedStatement = getConnectionWrapper()
          .prepareStatement(format)) {
        try (ResultSet rs = preparedStatement.executeQuery()) {
          while (rs.next()) {
            schema.put(SchemaConstant.DOUBLE_QUOTE + rs.getString("Field")
                + SchemaConstant.DOUBLE_QUOTE, rs.getString("Type"));
          }
        }
      } catch (Exception ex) {
        logger.error("Exception occurred when try to init schema, exception: {}", ex.getMessage());
        LoggingServiceUtil.alarm(LogVo.builder()
            .k8sServiceName(GrayLogConstant.K8S_SERVICE_NAME)
            .module(GrayLogConstant.MODULE_NAME)
            .userName(tenantId)
            .tenantId(tenantId)
            .operateObjectName(getJobName())
            .operateObject(jobId)
            .shortMessage(String.format("Failed to init the schema within AssetRichSinkFunction. "
                + "Cause: %s. Database connection string: %s. Table: %s.",ex.toString(),uri,table))
            .operation(ShortMessageConstant.INIT_SCHEMA)
            .requestId(String.valueOf(System.currentTimeMillis()))
            .result(GrayLogResultEnum.FAIL.getValue())
            .logType(logType)
            .kafkaLoggingServiceTopic(kafkaLoggingServiceTopics)
            .kafkaLoggingServiceServers(kafkaLoggingServiceServers)
            .build(), flinkKafkaInternalProducer);
      }
      logger.debug("Postgre sql table schema: {}", schema);
    }

    @Override
    public void invoke(RcUnit record, Context context) throws Exception {
      logger.debug("postgre sql sink process element: {}", record);
      try {
        if (INSERT.name().equals(insertType)) {
          insert(record);
        } else if (UPSERT.name().equals(insertType)) {
          try {
            upsert(record);
          } catch (SQLException sqlException) {
            // Duplicate entry, try again.
            if (RETRY_SQL_STATES.contains(sqlException.getSQLState())) {
              logger.debug("Duplicate entry, try again, record: {}", record);
              upsert(record);
            } else {
              throw sqlException;
            }
          }

        } else {
          logger.error("Invalid insertType: {}", insertType);
        }
      } catch (Exception e) {
        logger.error("Exception occurred when try to insert/update {} to {}, exception: {}",
            record, table, e.getMessage(), e);
        LoggingServiceUtil.alarm(LogVo.builder()
            .k8sServiceName(GrayLogConstant.K8S_SERVICE_NAME)
            .module(GrayLogConstant.MODULE_NAME)
            .userName(tenantId)
            .tenantId(tenantId)
            .operateObjectName(getJobName())
            .operateObject(jobId)
            .shortMessage(String.format("Node %s failed to insert into PostgreSQL table %s due"
                + " to %s. Database connection string: %s. Table: %s.",getNodeName(),
                table,e.toString(),uri,table))
            .operation(ShortMessageConstant.INSERT_POSTGRESQL_DATA)
            .requestId(String.valueOf(System.currentTimeMillis()))
            .result(GrayLogResultEnum.FAIL.getValue())
            .logType(logType)
            .kafkaLoggingServiceTopic(kafkaLoggingServiceTopics)
            .kafkaLoggingServiceServers(kafkaLoggingServiceServers)
            .build(), flinkKafkaInternalProducer);
      }
    }

    private void insert(RcUnit record) throws Exception {
      try (PreparedStatement insert = getConnectionWrapper().prepareStatement(insertSql);) {
        buildSql(insert, record, insertFields, schema);
        insert.execute();
      }
      counterRecordsOut.inc();
    }

    private void upsert(RcUnit record) throws Exception {
      try (PreparedStatement select = getConnectionWrapper().prepareStatement(selectSql)) {
        buildSql(select, record, selectFields, schema);
        try (ResultSet resultSet = select.executeQuery()) {
          if (!resultSet.next()) {
            try (PreparedStatement insert = getConnectionWrapper().prepareStatement(insertSql);) {
              buildSql(insert, record, insertFields, schema);
              insert.execute();
            }
          } else {
            if (Strings.isNullOrEmpty(updateSql)) {
              logger.debug("Update sql is empty.");
            } else {
              try (PreparedStatement update = getConnectionWrapper().prepareStatement(updateSql)) {
                buildSql(update, record, updateFields, schema);
                update.executeUpdate();
              }
            }
          }
        }
      }
      counterRecordsOut.inc();
    }

    @Override
    public void close() throws Exception {

      LoggingServiceUtil.info(LogVo.builder()
          .k8sServiceName(GrayLogConstant.K8S_SERVICE_NAME)
          .module(GrayLogConstant.MODULE_NAME)
          .userName(tenantId)
          .userId(getUserId())
          .tenantId(tenantId)
          .operateObjectName(getJobName())
          .operateObject(jobId)
          .shortMessage(String.format("Node %s disconnected from Postgresql."
              + " Database connection string: %s. Table: %s.",getNodeName(),uri,table))
          .operation(ShortMessageConstant.CLOSE_POSTGRESQL)
          .requestId(String.valueOf(System.currentTimeMillis()))
          .result(GrayLogResultEnum.SUCCESS.getValue())
          .logType(logType)
          .kafkaLoggingServiceTopic(kafkaLoggingServiceTopics)
          .kafkaLoggingServiceServers(kafkaLoggingServiceServers)
          .build(), flinkKafkaInternalProducer);
      super.close();
      schema = null;
      logger.info("PostgreSql sink is closed.");
    }


  }
}
