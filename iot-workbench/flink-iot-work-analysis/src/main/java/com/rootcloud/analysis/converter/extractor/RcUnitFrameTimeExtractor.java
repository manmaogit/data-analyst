/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.converter.extractor;

import com.rootcloud.analysis.FlinkMain;
import com.rootcloud.analysis.common.entity.RcUnitFrame;
import com.rootcloud.analysis.common.util.DateUtil;
import com.rootcloud.analysis.entity.RcTimeAttr;

import javax.annotation.Nullable;

import org.apache.flink.streaming.api.functions.AssignerWithPeriodicWatermarks;
import org.apache.flink.streaming.api.watermark.Watermark;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RcUnitFrameTimeExtractor implements
    AssignerWithPeriodicWatermarks<RcUnitFrame> {

  private static Logger logger = LoggerFactory.getLogger(FlinkMain.class);

  private static final long MAX_OUT_OF_ORDERNESS = 10000L;
  private static final long S_TO_MS = 1000L;
  private long currentMaxTimestamp = 0L;
  private RcTimeAttr timeAttr;
  private long lastEmittedWatermark = Long.MIN_VALUE;

  public RcUnitFrameTimeExtractor(RcTimeAttr timeAttr) {
    this.timeAttr = timeAttr;
  }

  @Nullable
  @Override
  public Watermark getCurrentWatermark() {
    long potentialWM = currentMaxTimestamp - MAX_OUT_OF_ORDERNESS;
    if (potentialWM >= lastEmittedWatermark) {
      lastEmittedWatermark = potentialWM;
    }
    logger.debug("Current watermark: {}", lastEmittedWatermark);
    return new Watermark(lastEmittedWatermark);
  }

  @Override
  public long extractTimestamp(RcUnitFrame element, long previousElementTimestamp) {

    logger.debug("time extractor process element: {}", element);

    //todo:临时修改
    Object time = element.getRcu().getAttr(timeAttr.getKey());

    //todo:抽象成多个类进行处理
    if (timeAttr.getType().toLowerCase().equals(RcTimeAttr.TIME_TYPE_STRING)) {
      currentMaxTimestamp = DateUtil.convertTimeToLongMs((String) time);
    } else {
      if (time instanceof Integer) {
        currentMaxTimestamp = Long.valueOf((Integer) time);
      } else {
        currentMaxTimestamp = (long) time;
      }

      if (RcTimeAttr.TIME_UNIT_S.equals(timeAttr.getFormat())) {
        currentMaxTimestamp *= S_TO_MS;
      }
    }

    return currentMaxTimestamp;
  }
}
