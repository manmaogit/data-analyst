/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.decoder.function.filter;

import com.rootcloud.analysis.scanner.ClasspathPackageScanner;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FilterDecoderFactory {

  private static Logger logger = LoggerFactory.getLogger(FilterDecoderFactory.class);
  private static Map<String, AbstractFilterDecoder> decoderMap = new HashMap<>();

  static {
    ClasspathPackageScanner scanner = new ClasspathPackageScanner(
        "com.rootcloud.analysis.decoder.function.filter.impl");
    try {
      List<String> classList = scanner.getFullyQualifiedClassNameList();

      for (String clazz : classList) {
        AbstractFilterDecoder decoder = (AbstractFilterDecoder) FilterDecoderFactory.class.getClassLoader()
            .loadClass(clazz.replace("com/rootcloud/analysis/decoder/function/filter/impl/", "")).newInstance();
        FilterDecoder filterDecoder = decoder.getClass().getAnnotation(FilterDecoder.class);
        decoderMap.put(filterDecoder.filterRule().name(), decoder);
      }
    } catch (IOException | ClassNotFoundException | InstantiationException
        | IllegalAccessException e) {
      logger.error("Failed to init FilterDecoderFactory", e);
    }
  }

  /**
   * getDecoder.
   */
  public static AbstractFilterDecoder getDecoder(String filterRule) {
    if (decoderMap.containsKey(filterRule)) {
      return decoderMap.get(filterRule);
    }

    logger.error("FilterDecoder not found: {}", filterRule);
    return null;
  }

}
