/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.decoder.sink.impl;

import com.alibaba.fastjson.JSONObject;
import com.rootcloud.analysis.core.constants.JobConstant;
import com.rootcloud.analysis.core.enums.SinkTypeEnum;
import com.rootcloud.analysis.decoder.sink.AbstractSinkDecoder;
import com.rootcloud.analysis.decoder.sink.SinkDecoder;
import com.rootcloud.analysis.exception.DecodeSinkException;
import com.rootcloud.analysis.wrapper.sink.AbstractSinkWrapper;
import com.rootcloud.analysis.wrapper.sink.KafkaSinkWrapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SinkDecoder(sinkType = SinkTypeEnum.KAFKA)
public class KafkaSinkDecoder extends AbstractSinkDecoder {

  private static Logger logger = LoggerFactory.getLogger(KafkaSinkDecoder.class);

  @Override
  public AbstractSinkWrapper decodeSinkParams(JSONObject conf) {
    try {
      KafkaSinkWrapper.Builder builder = new KafkaSinkWrapper.Builder();
      JSONObject params = (JSONObject) conf.get(JobConstant.KEY_PARAMETERS);
      builder
          .setBrokers(params.getString(JobConstant.KEY_BROKER_LIST))
          .setKeyParams(conf.getJSONObject(JobConstant.KEY_KAFKA_KEY))
          .setTopics(params.getJSONArray(JobConstant.KEY_TOPICS).toJavaList(String.class))
          .setSinkType(SinkTypeEnum.KAFKA)
          .setAttributeMapper(conf.getJSONArray(JobConstant.KEY_ATTRIBUTE_MAPPER));

      KafkaSinkWrapper result = builder.build();
      logger.debug("Decoding kafka sink: {}", result);
      return result;
    } catch (Exception ex) {
      logger.error("Exception occurred when decoding kafka sink: {}", conf);
      throw new DecodeSinkException(conf.getString(JobConstant.KEY_NODE_ID),
          conf.toJSONString());
    }
  }

}
