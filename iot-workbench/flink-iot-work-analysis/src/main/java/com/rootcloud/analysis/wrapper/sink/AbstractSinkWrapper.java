/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.wrapper.sink;

import com.rootcloud.analysis.common.context.IotWorkRuntimeContext;
import com.rootcloud.analysis.core.enums.GraylogLogTypeEnum;
import com.rootcloud.analysis.core.enums.SinkTypeEnum;
import com.rootcloud.analysis.wrapper.AbstractWrapper;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@ToString(callSuper = true)
public abstract class AbstractSinkWrapper extends AbstractWrapper {

  private String priorNodeId;

  private SinkTypeEnum sinkType;

  /**
   * Constructor.
   *
   * @param jobId       node id
   * @param tenantId    node id
   * @param nodeId      node id
   * @param parallelism parallelism
   * @param nodeName    node name
   * @param priorNodeId prior node id
   * @param sinkType    output type
   */
  public AbstractSinkWrapper(String jobId, String jobName,
                             String tenantId, String nodeId,
                             Integer parallelism, String nodeName, String priorNodeId,
                             SinkTypeEnum sinkType, GraylogLogTypeEnum logType) {
    super(jobId, jobName, tenantId, nodeId, parallelism, nodeName, logType);
    this.priorNodeId = priorNodeId;
    this.sinkType = sinkType;
  }

  public abstract void registerOutput(IotWorkRuntimeContext context);

}
