/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.decoder.source.impl;

import com.alibaba.fastjson.JSONObject;
import com.rootcloud.analysis.context.IotWorkRuntimeDecodeContext;
import com.rootcloud.analysis.core.constants.JobConstant;
import com.rootcloud.analysis.core.enums.DataSourceTypeEnum;
import com.rootcloud.analysis.decoder.source.SourceDecoder;
import com.rootcloud.analysis.exception.DecodeSourceException;
import com.rootcloud.analysis.wrapper.source.HiveSourceWrapper;
import com.rootcloud.analysis.wrapper.source.NeoInnerHiveSourceWrapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Please use NeoInnerHiveBatchSourceDecoder instead.
 * Created by 王纯超 on 2021/8/11.
 * <br/>
 * Decoder for
 * {@link com.rootcloud.analysis.wrapper.source.NeoInnerHiveSourceWrapper}
 */
@Deprecated
@SourceDecoder(dataSourceType = DataSourceTypeEnum.NEO_INNER_HIVE)
public class NeoInnerHiveSourceDecoder extends HiveSourceDecoder {
  private static Logger logger = LoggerFactory.getLogger(NeoInnerHiveSourceDecoder.class);

  @Override
  protected HiveSourceWrapper decodeHiveSourceParams(JSONObject conf) {
    try {
      NeoInnerHiveSourceWrapper.NeoInnerHiveSourceWrapperBuilder builder =
          NeoInnerHiveSourceWrapper.builder();
      builder.partitionIdLowerBound(IotWorkRuntimeDecodeContext.getTimeScope().getStartEpochSecond() / 3600);
      builder.partitionIdUpperBound(IotWorkRuntimeDecodeContext.getTimeScope().getEndEpochSecond() / 3600);
      return builder.build();
    } catch (Exception ex) {
      logger
          .error("Exception occurred when decoding inner hive source: {}, error message: {}", conf,
              ex.toString());
      throw new DecodeSourceException(conf.getString(JobConstant.KEY_NODE_ID),
          conf.toJSONString());
    }
  }
}
