/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.converter.time;

import java.io.Serializable;
import java.time.ZoneId;

public interface ITimeConverter extends Serializable {

  String convert(String time, ZoneId zoneId);

  String convert(long time, ZoneId zoneId);

  String convert(long time, ZoneId zoneId, int startMonth, int startDay,
      int startHour, int startMinute);

}
