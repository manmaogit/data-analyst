/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.wrapper.sink;

import static com.rootcloud.analysis.core.constants.WrapperConstant.NULL_IDENTIFICATION;
import static com.rootcloud.analysis.core.constants.WrapperConstant.SEPARATOR;

import com.rootcloud.analysis.common.context.IotWorkRuntimeContext;
import com.rootcloud.analysis.common.entity.RcUnit;
import com.rootcloud.analysis.context.IotWorkRuntimeBuildContext;
import com.rootcloud.analysis.core.constants.CommonConstant;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import org.apache.flink.api.java.functions.KeySelector;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.KeyedStream;

@Getter
@Setter
@ToString(callSuper = true)
public abstract class JdbcBatchSinkWrapper extends AbstractSinkWrapper {

  private String uri;

  private String username;

  private String password;

  private String table;

  private List<String> columns;

  private List<String> updateColumns;

  private Map<String, String> attributeMapper;

  private Map<String, String> schema;

  private List<String> insertKey;

  private String insertType;


  private Map<String, Integer> priorTableColIndex;

  private static final int BATCH_SIZE = 1000;

  @Getter
  private UpsertKeyBuilder upsertKeyBuilder = new UpsertKeyBuilder();

  /**
   * Gets batch size.
   */
  protected int getBatchSize() {
    return BATCH_SIZE;
  }

  /**
   * Gets prior data stream.
   */
  protected KeyedStream<RcUnit, String> getPriorDataStream(IotWorkRuntimeContext context) {
    DataStream<RcUnit> priorDataStream = context.getObjectFromContainer(getPriorNodeId());
    return priorDataStream.keyBy(
        (KeySelector<RcUnit, String>) record ->
            upsertKeyBuilder.build(insertKey, record, attributeMapper)
    );
  }

  /**
   * Builds table query sql.
   */
  protected String buildTableQuerySql() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("select ");
    int colCount = 0;
    for (String colName : attributeMapper.values()) {
      if (colCount > 0) {
        stringBuilder.append(CommonConstant.COMMA);
      }
      stringBuilder.append("`");
      stringBuilder.append(colName);
      stringBuilder.append("`");
      colCount++;
    }
    stringBuilder.append(" from ");
    stringBuilder.append(IotWorkRuntimeBuildContext.getTemporaryViewName(getPriorNodeId()));
    return stringBuilder.toString();
  }

  @AllArgsConstructor
  public static class UpsertKeyBuilder implements Serializable {

    private static final long serialVersionUID = 8720931237798424138L;

    /**
     * Builds upsert key.
     */
    String build(List<String> insertKey, RcUnit record,  Map<String, String> attributeMapper) {
      return insertKey.stream()
          .map(key -> {
            Object val = record.getAttr(attributeMapper.get(key));
            if (val != null) {
              return key + SEPARATOR + val;
            }
            return key + SEPARATOR + NULL_IDENTIFICATION;
          }).collect(Collectors.joining(SEPARATOR));
    }
  }

}

