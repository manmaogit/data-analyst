/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.entity;

import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@ToString
public class RcTimeAttr implements Serializable {

  private static final long serialVersionUID = -7820751219443017468L;
  private static Logger logger = LoggerFactory.getLogger(RcTimeAttr.class);
  public static final String TIME_TYPE_STRING = "string";
  public static final String TIME_TYPE_LONG   = "long";

  public static final String TIME_UNIT_MS = "ms";
  public static final String TIME_UNIT_S  = "s";

  @Setter
  @Getter
  private String key;

  @Setter
  @Getter
  private String type;

  @Setter
  @Getter
  private String format;

  /**
   * valueof.
   */
  public static RcTimeAttr valueOf(String timeAttr) {

    String[] tempArray = timeAttr.split("\\|");

    RcTimeAttr rcTimeAttr = new RcTimeAttr();
    if (tempArray.length != 3) {
      logger.error("time attribute error,time_attr:{}",timeAttr);
      return null;
    }

    //todo:增加数据内容校验
    rcTimeAttr.setKey(tempArray[0]);
    rcTimeAttr.setType(tempArray[1]);
    rcTimeAttr.setFormat(tempArray[2]);

    return rcTimeAttr;

  }

}
