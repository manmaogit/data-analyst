/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.wrapper.source;

import com.alibaba.fastjson.JSONObject;
import com.rootcloud.analysis.common.context.IotWorkRuntimeContext;
import com.rootcloud.analysis.common.entity.RcUnit;
import com.rootcloud.analysis.converter.stream.table.RcUnitToRowConverter;
import com.rootcloud.analysis.core.constants.GrayLogConstant;
import com.rootcloud.analysis.core.constants.SchemaConstant;
import com.rootcloud.analysis.core.constants.ShortMessageConstant;
import com.rootcloud.analysis.core.enums.GrayLogResultEnum;
import com.rootcloud.analysis.core.graylog.LogVo;
import com.rootcloud.analysis.log.loggingservice.LoggingServiceUtil;
import com.rootcloud.analysis.wrapper.function.jdbc.JdbcRichSourceFunction;
import com.rootcloud.analysis.wrapper.jdbc.JdbcConnWrapper;
import com.rootcloud.analysis.wrapper.jdbc.SqlServerBatchConnWrapper;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.source.SourceFunction;
import org.apache.flink.table.api.bridge.java.StreamTableEnvironment;
import org.apache.flink.types.Row;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 离线任务sqlServer输入节点.
 */
public class SqlServerBatchSourceWapper extends JdbcSourceWrapper {


  private static final Logger logger = LoggerFactory.getLogger(SqlServerBatchSourceWapper.class);


  public SqlServerBatchSourceWapper(Builder builder) {
    super(builder);
  }

  public static class Builder extends JdbcSourceWrapper.Builder {

    public SqlServerBatchSourceWapper build() {
      return new SqlServerBatchSourceWapper(this);
    }
  }

  /**
   * registerInput.
   */
  public void registerInput(IotWorkRuntimeContext context) {
    SqlServerBatchConnWrapper sqlServerBatchConnWrapper = new SqlServerBatchConnWrapper(
        "com.microsoft.sqlserver.jdbc.SQLServerDriver",
        uri, username,
        password, "select 1 ", table, getSchema(), limit, filterSql,
        tableSql, sqlMode);
    context.putSourceConnToCache(getNodeId(), sqlServerBatchConnWrapper);
    SourceFunction sourceFunction = new SqlServerSourceFunction(sqlServerBatchConnWrapper,
        getKafkaLoggingServiceServers(), getKafkaLoggingServiceTopics());

    StreamExecutionEnvironment env = context.getExecutionEnv();
    SingleOutputStreamOperator<RcUnit> streamSource = env.addSource(sourceFunction).uid(getNodeId())
        .setParallelism(1).name(getNodeName());
    context.putObjectToContainer(getNodeId(), streamSource);
    try {
      createTemporaryViewForRow(context, streamSource);
    } catch (Exception e) {
      logger.error("createTemporaryView-error:", e);
    }
  }

  protected void createTemporaryViewForRow(IotWorkRuntimeContext context,
                                           DataStream<RcUnit> sourceDataStream) throws Exception {
    List<String> fieldNames = new ArrayList<>(getSchema().size());
    List<String> fieldTypes = new ArrayList<>(getSchema().size());
    List<Integer> valueTypeScales = new ArrayList<>(getSchema().size());
    for (int i = 0; i < getSchema().size(); i++) {
      JSONObject jsonObject = (JSONObject) getSchema().get(i);
      fieldNames.add(jsonObject.getString(SchemaConstant.ATTR_NAME));
      fieldTypes.add(jsonObject.getString(SchemaConstant.ATTR_DATA_TYPE));
      valueTypeScales.add(jsonObject.getInteger(SchemaConstant.ATTR_SCALE));
    }
    RcUnitToRowConverter converter = new RcUnitToRowConverter(getNodeId(), fieldNames,
        fieldTypes, valueTypeScales);
    DataStream<Row> rowDataStream = converter.convert(sourceDataStream);
    StreamTableEnvironment tableEnv = context.getTableEnvFromCache();
    tableEnv.createTemporaryView(getNodeId(), rowDataStream);
  }


  private class SqlServerSourceFunction extends JdbcRichSourceFunction<RcUnit> {

    private final String kafkaLoggingServiceServers;
    private final String kafkaLoggingServiceTopics;

    /**
     * 构造方法.
     */
    public SqlServerSourceFunction(JdbcConnWrapper jdbcConnWrapper,
                                   String kafkaLoggingServiceServers,
                                   String kafkaLoggingServiceTopics) {
      super(jdbcConnWrapper);
      this.kafkaLoggingServiceServers = kafkaLoggingServiceServers;
      this.kafkaLoggingServiceTopics = kafkaLoggingServiceTopics;
    }

    @Override
    public void open(Configuration parameters) throws Exception {
      super.open(parameters);
      openProducer(kafkaLoggingServiceServers);
    }

    @Override
    public void run(SourceContext<RcUnit> ctx) throws Exception {

      try {
        Set<RcUnit> result = null;
        if (!Thread.currentThread().isInterrupted()) {
          result = jdbcConnWrapper.getData();
        }
        if (!Thread.currentThread().isInterrupted() && result != null) {
          for (RcUnit rcUnit : result) {
            if (!Thread.currentThread().isInterrupted()) {
              ctx.collect(rcUnit);
            }
          }
        }
      } catch (Exception ex) {
        LoggingServiceUtil.alarm(LogVo.builder()
            .k8sServiceName(GrayLogConstant.K8S_SERVICE_NAME)
            .module(GrayLogConstant.MODULE_NAME)
            .userName(getTenantId())
            .tenantId(getTenantId())
            .operateObjectName(getJobName())
            .operateObject(getJobId())
            .shortMessage(String.format("Exception occurred in node %s when reading "
                    + "data from table %s in SqlServer. Detail: %s", getNodeName(),
                table == null ? "" : table,
                ex.toString()))
            .operation(ShortMessageConstant.READ_SQL_SERVER_DATA)
            .requestId(String.valueOf(System.currentTimeMillis()))
            .result(GrayLogResultEnum.FAIL.getValue())
            .logType(getLogType())
            .kafkaLoggingServiceTopic(kafkaLoggingServiceTopics)
            .kafkaLoggingServiceServers(kafkaLoggingServiceServers)
            .build(), flinkKafkaInternalProducer);
      }

      logger.info("SqlServer source is over,URI：{}，Table：{}", uri, table);
    }

    @Override
    public void cancel() {
      logger.info("SqlServer source is canceled.");
      LoggingServiceUtil.info(LogVo.builder()
          .k8sServiceName(GrayLogConstant.K8S_SERVICE_NAME)
          .module(GrayLogConstant.MODULE_NAME)
          .userName(getTenantId())
          .userId(getUserId())
          .tenantId(getTenantId())
          .operateObjectName(getJobName())
          .operateObject(getJobId())
          .shortMessage(String.format("Node %s disconnected from SqlServer. "
                  + "Database connection string: %s." + (table == null ? "" : "Table:" + table),
              getNodeName(), uri))
          .operation(ShortMessageConstant.CLOSE_SQLSERVER)
          .requestId(String.valueOf(System.currentTimeMillis()))
          .result(GrayLogResultEnum.SUCCESS.getValue())
          .logType(getLogType())
          .kafkaLoggingServiceTopic(kafkaLoggingServiceTopics)
          .kafkaLoggingServiceServers(kafkaLoggingServiceServers)
          .build(), flinkKafkaInternalProducer);
    }

    @Override
    public void close() throws Exception {

      logger.info("SqlServer source is closed.");
      LoggingServiceUtil.info(LogVo.builder()
          .k8sServiceName(GrayLogConstant.K8S_SERVICE_NAME)
          .module(GrayLogConstant.MODULE_NAME)
          .userName(getTenantId())
          .userId(getUserId())
          .tenantId(getTenantId())
          .operateObjectName(getJobName())
          .operateObject(getJobId())
          .shortMessage(String.format("Node %s disconnected from SqlServer. "
                  + "Database connection string: %s." + (table == null ? "" : "Table:" + table),
              getNodeName(), uri))
          .operation(ShortMessageConstant.CLOSE_SQLSERVER)
          .requestId(String.valueOf(System.currentTimeMillis()))
          .result(GrayLogResultEnum.SUCCESS.getValue())
          .logType(getLogType())
          .kafkaLoggingServiceTopic(kafkaLoggingServiceTopics)
          .kafkaLoggingServiceServers(kafkaLoggingServiceServers)
          .build(), flinkKafkaInternalProducer);
      super.close();
    }

  }

  ;
}

