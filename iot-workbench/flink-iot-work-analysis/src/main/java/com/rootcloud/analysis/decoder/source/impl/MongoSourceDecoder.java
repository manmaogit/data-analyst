/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.decoder.source.impl;

import com.alibaba.fastjson.JSONObject;
import com.rootcloud.analysis.core.constants.JobConstant;
import com.rootcloud.analysis.core.enums.DataSourceTypeEnum;
import com.rootcloud.analysis.decoder.source.AbstractSourceDecoder;
import com.rootcloud.analysis.decoder.source.SourceDecoder;
import com.rootcloud.analysis.exception.DecodeSourceException;
import com.rootcloud.analysis.wrapper.source.AbstractSourceWrapper;
import com.rootcloud.analysis.wrapper.source.MongoSourceWrapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Deprecated
@SourceDecoder(dataSourceType = DataSourceTypeEnum.MONGO)
public class MongoSourceDecoder extends AbstractSourceDecoder {

  private static Logger logger = LoggerFactory.getLogger(MongoSourceDecoder.class);

  @Override
  public AbstractSourceWrapper decodeSourceParams(JSONObject conf) {
    if (!conf.containsKey(JobConstant.KEY_SOURCE_ID)) {
      throw new DecodeSourceException("source_id empty", conf.toJSONString());
    }

    try {
      JSONObject params = conf.getJSONObject(JobConstant.KEY_PARAMETERS);
      JSONObject detectParams = params.getJSONObject(JobConstant.KEY_CHANGE_DETECT);

      // TODO: 2020/8/17 调通
      MongoSourceWrapper wrapper = new MongoSourceWrapper.Builder()
          .setUri(params.getString(JobConstant.KEY_URI))
          .setDatabase(params.getString(JobConstant.KEY_DATABASE))
          .setCollection(params.getString(JobConstant.KEY_COLLECTION))
          .setDetectPeriod(detectParams.getInteger(JobConstant.KEY_PERIOD))
          .setDetectType(detectParams.getString(JobConstant.KEY_MODE))
          .setDetectKey(detectParams.getString(JobConstant.KEY_ATTR)).build();

      // wrapper.setNodeId(conf.getString(JobConstant.KEY_NODE_ID));
      // wrapper.setInputType(conf.getString(JobConstant.KEY_INPUT_TYPE));

      return wrapper;
    } catch (Exception ex) {
      logger.error("Exception occurred when decoding source: {}", conf);
      throw new DecodeSourceException(conf.getString(JobConstant.KEY_SOURCE_ID),
          conf.toJSONString());
    }


  }
}
