/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.wrapper.function.join;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.rootcloud.analysis.common.entity.RcUnit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import org.apache.flink.runtime.state.HeapBroadcastState;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RightOuterJoinWrapper extends AbstractJoinWrapper {

  private static final Logger logger = LoggerFactory.getLogger(RightOuterJoinWrapper.class);

  @Override
  public List<RcUnit> join(HeapBroadcastState<Object, List<RcUnit>> dimensionState, RcUnit input,
                           JSONObject attributeMapper,
                           Set<String> dimensionAttrSet,
                           JSONArray schema, Object value) {
    List<RcUnit> resultList = new ArrayList<>();
    try {
      if (dimensionState.contains(value)) {

        for (RcUnit dimension : dimensionState
            .get(value)) {
          for (String attrName : dimensionAttrSet) {
            input.setAttr(attrName, dimension.getAttr(attrName));
          }
          RcUnit result = RcUnit.valueOf(input.getData(), schema);
          resultList.add(result);
        }
      } else {
        Iterator<Entry<Object, List<RcUnit>>> dimensionStateIte = dimensionState.iterator();
        while (dimensionStateIte.hasNext()) {
          Entry<Object, List<RcUnit>> rcUnitEntry = dimensionStateIte.next();
          for (RcUnit rcUnit : rcUnitEntry.getValue()) {
            resultList.add(rcUnit);
          }
        }
      }
    } catch (Exception e) {
      logger.error(e.toString());
    }
    return resultList;
  }

  @Override
  public Boolean contain(HeapBroadcastState<Object, List<RcUnit>> dimensionState, Object value,
                         JSONObject attributeMapper) {
    return true;
  }
}
