/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.converter.time.impl;

import static com.rootcloud.analysis.core.enums.TimeDimensionEnum.WEEK;

import com.rootcloud.analysis.common.util.DateUtil;
import com.rootcloud.analysis.converter.time.ITimeConverter;
import com.rootcloud.analysis.converter.time.TimeConvert;

import java.text.ParseException;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@TimeConvert(converterType = WEEK)
public class TimeToWeekConverter implements ITimeConverter {
  private final Logger logger = LoggerFactory.getLogger(TimeToWeekConverter.class);

  @Override
  public String convert(String time, ZoneId zoneId) {
    Date date = null;
    try {
      date = DateUtil.convertTimeToDate(time);
    } catch (ParseException e) {
      logger.error("error msg:", e);
      return null;
    }

    return dateToTime(date, zoneId);
  }

  @Override
  public String convert(long time, ZoneId zoneId) {
    return dateToTime(new Date(time), zoneId);
  }

  @Override
  public String convert(long time, ZoneId zoneId, int startMonth, int startDay,
      int startHour, int startMinute) {
    return dateToTime(time, zoneId, startMonth, startDay, startHour, startMinute);
  }

  private String dateToTime(Date date, ZoneId zoneId) {
    Calendar cal = new GregorianCalendar();
    cal.setFirstDayOfWeek(Calendar.MONDAY);
    cal.setTime(date);
    cal.set(Calendar.DAY_OF_WEEK, cal.getFirstDayOfWeek());
    Date first = cal.getTime();
    long timeMs = first.getTime();
    String dateTimeFormatter = "yyyy-MM-dd 00:00:00";
    return DateUtil.formatTime(timeMs, zoneId, dateTimeFormatter);
  }

  private String dateToTime(long time, ZoneId zoneId, int startMonth, int startDay,
      int startHour, int startMinute) {
    Calendar cal = new GregorianCalendar();
    cal.setFirstDayOfWeek(Calendar.MONDAY);
    cal.setTimeInMillis(time);
    DateUtil.minusTime(cal, startMonth - 1, startDay - 1, startHour, startMinute);
    cal.set(Calendar.DAY_OF_WEEK, cal.getFirstDayOfWeek());
    String dateTimeFormatter = "yyyy-MM-dd 00:00:00";
    return DateUtil.formatTime(cal.getTimeInMillis(), zoneId, dateTimeFormatter);
  }

}
