/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.wrapper.processor;

import static org.apache.flink.table.api.Expressions.$;
import static org.apache.flink.table.api.Expressions.call;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.rootcloud.analysis.common.context.IotWorkRuntimeContext;
import com.rootcloud.analysis.common.entity.RcUnit;
import com.rootcloud.analysis.context.IotWorkRuntimeBuildContext;
import com.rootcloud.analysis.core.constants.SchemaConstant;
import com.rootcloud.analysis.core.enums.AttrSourceEnum;
import com.rootcloud.analysis.wrapper.function.flinksql.FlinkSqlRow2RcUnitFlatMap;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.table.api.ApiExpression;
import org.apache.flink.table.api.Table;
import org.apache.flink.table.api.bridge.java.StreamTableEnvironment;
import org.apache.flink.types.Row;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * 表值聚合算子.
 *
 * @author: zexin.huang
 * @createdDate: 2022/8/15 10:49
 */
@Getter
@Setter
@ToString(callSuper = true)
@Builder
public class TableAggProcessorWrapper extends AbstractProcessorWrapper {

  private final Logger logger = LoggerFactory.getLogger(TableAggProcessorWrapper.class);

  // 表值聚合函数名.
  private final String udtAggFuncName;
  // 表值聚合分组字段Key.
  private final List<String> groupFields;
  // 输入字段.
  private final JSONArray sourceAttribute;
  // 输出字段.
  private final JSONArray targetAttribute;

  @Override
  public void registerProcess(IotWorkRuntimeContext context) throws Exception {
    StreamTableEnvironment tableEnv = context.getTableEnvFromCache();
    Configuration configuration = tableEnv.getConfig().getConfiguration();
    configuration.setInteger("table.exec.resource.default-parallelism",
        IotWorkRuntimeBuildContext.getSqlParallelism());

    StringBuilder sb = new StringBuilder();
    sb.append("select ");
    sb.append(" * ");
    sb.append(" from ");
    sb.append(getPriorNodeId());

    // execute sql query and get sql result stream
    Table table = tableEnv.sqlQuery(sb.toString());

    // 分组字段集合.

    // 函数入参字段集合.
    List<ApiExpression> inputFields = new LinkedList<>();
    sourceAttribute.stream().forEach(i -> {
      String filedName = ((JSONObject) i).getString(SchemaConstant.ATTR_NAME);
      inputFields.add($(filedName));
    });
    List targetWithoutGroupAttribute = targetAttribute.stream()
        .filter(f -> ((JSONObject) f).getString(SchemaConstant.ATTR_SOURCE).equals(
            AttrSourceEnum.UDF_OUTPUT.name()))
        .collect(
            Collectors.toList());
    // 函数出参字段集合.
    List<ApiExpression> outputFields = new LinkedList<>();
    targetWithoutGroupAttribute
        .forEach(i -> {
          String filedName = ((JSONObject) i).getString(SchemaConstant.ATTR_NAME);
          outputFields.add($(filedName));
        });
    // 函数出参字段重命名集合.
    List<String> renameFields = new LinkedList<>();
    String firstAsField = ((JSONObject) (targetWithoutGroupAttribute.get(0))).getString(
        SchemaConstant.ATTR_NAME);
    targetWithoutGroupAttribute.stream().filter(
        i -> !((JSONObject) i).getString(SchemaConstant.ATTR_NAME).equals(firstAsField))
        .forEach(i -> {
          String filedName = ((JSONObject) i).getString(SchemaConstant.ATTR_NAME);
          renameFields.add(filedName);
        });
    ApiExpression[] groupFieldsExp = this.groupFields.stream().map(f -> $(f))
        .toArray(ApiExpression[]::new);
    // 调用表值聚合函数.
    List<ApiExpression> selectFields = new LinkedList<ApiExpression>() {{
        addAll(outputFields);
        addAll(Arrays.stream(groupFieldsExp).collect(Collectors.toList()));
      }};
    try {
      table = table.groupBy(groupFieldsExp).flatAggregate(
              call(this.udtAggFuncName, inputFields.toArray(new ApiExpression[]{})).as(firstAsField,
                  renameFields.toArray(new String[]{})))
          .select(selectFields.toArray(new ApiExpression[]{}));

    } catch (Exception e) {
      logger.error(this.udtAggFuncName + " execution failed:" + e.toString(), e);
    }

    DataStream<RcUnit> dataStream = tableEnv.toRetractStream(table, Row.class).process(
        new FlinkSqlRow2RcUnitFlatMap(getJobId(), getTenantId(), getUserId(), getJobName(),
            getNodeId(), true, Arrays.asList(table.getSchema().getFieldNames()),
            getKafkaLoggingServiceServers(), getKafkaLoggingServiceTopics(), null

        )).uid(getNodeId()).name(getNodeName());
    context.putObjectToContainer(getNodeId(), dataStream);

    tableEnv.createTemporaryView(getNodeId(), table);
  }
}
