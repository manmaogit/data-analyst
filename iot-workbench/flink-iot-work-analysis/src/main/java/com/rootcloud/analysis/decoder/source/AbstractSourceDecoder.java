/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.decoder.source;

import com.alibaba.fastjson.JSONObject;
import com.rootcloud.analysis.core.constants.JobConstant;
import com.rootcloud.analysis.decoder.AbstractDecoder;
import com.rootcloud.analysis.decoder.IDecoder;
import com.rootcloud.analysis.wrapper.AbstractWrapper;
import com.rootcloud.analysis.wrapper.processor.AbstractProcessorWrapper;
import com.rootcloud.analysis.wrapper.source.AbstractSourceWrapper;

public abstract class AbstractSourceDecoder extends AbstractDecoder {

  @Override
  protected final AbstractWrapper decodeParams(JSONObject conf) {
    AbstractSourceWrapper abstractSourceWrapper = decodeSourceParams(conf);
    abstractSourceWrapper.setSourceType(conf.getString(JobConstant.KEY_SOURCE_TYPE));
    abstractSourceWrapper.setSchema(conf.getJSONArray(JobConstant.KEY_SCHEMA));
    return abstractSourceWrapper;
  }

  protected abstract AbstractSourceWrapper decodeSourceParams(JSONObject conf);

}
