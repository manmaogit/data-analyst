/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.wrapper.source;

import com.alibaba.fastjson.JSONArray;
import com.rootcloud.analysis.common.context.IotWorkRuntimeContext;
import com.rootcloud.analysis.common.entity.RcUnit;
import com.rootcloud.analysis.core.constants.GrayLogConstant;
import com.rootcloud.analysis.core.constants.ShortMessageConstant;
import com.rootcloud.analysis.core.enums.GrayLogResultEnum;
import com.rootcloud.analysis.core.graylog.LogVo;
import com.rootcloud.analysis.log.loggingservice.LoggingServiceUtil;
import com.rootcloud.analysis.wrapper.WrapperBuilder;
import com.rootcloud.analysis.wrapper.function.jdbc.JdbcRichSourceFunction;
import com.rootcloud.analysis.wrapper.jdbc.JdbcConnWrapper;
import com.rootcloud.analysis.wrapper.jdbc.MysqlConnWrapper;
import java.util.Set;
import lombok.Getter;
import lombok.ToString;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.source.SourceFunction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Please use MysqlBatchSourceWrapper instead.
 */
@Deprecated
@Getter
@ToString(callSuper = true)
public class MysqlSourceWrapper extends AbstractSourceWrapper {

  private static final Logger logger = LoggerFactory.getLogger(MysqlSourceWrapper.class);

  private final String uri;
  private final String username;
  private final String password;
  private final String table;
  private final Integer detectPeriod;
  private final Integer limit;
  private final String filterSql;


  private MysqlSourceWrapper(Builder builder) {
    super(builder.jobId, builder.jobName, builder.tenantId, builder.nodeId, builder.parallelism,
        builder.nodeName, builder.inputType, builder.schema, builder.logType);
    this.uri = builder.uri;
    this.username = builder.username;
    this.password = builder.password;
    this.table = builder.table;
    this.detectPeriod = builder.detectPeriod;
    this.limit = builder.limit;
    this.filterSql = builder.filterSql;
  }

  public static class Builder extends WrapperBuilder {

    private String nodeId;
    private String nodeName;
    private Integer parallelism;
    private String inputType;
    private JSONArray schema;
    private String uri;
    private String username;
    private String password;
    private String table;
    private Integer detectPeriod;
    private Integer limit;
    private String filterSql;

    public Builder setLimit(Integer limit) {
      this.limit = limit;
      return this;
    }

    public Builder setNodeId(String nodeId) {
      this.nodeId = nodeId;
      return this;
    }

    public Builder setNodeName(String nodeName) {
      this.nodeName = nodeName;
      return this;
    }

    public Builder setParallelism(Integer parallelism) {
      this.parallelism = parallelism;
      return this;
    }

    public Builder setInputType(String inputType) {
      this.inputType = inputType;
      return this;
    }

    public Builder setSchema(JSONArray schema) {
      this.schema = schema;
      return this;
    }

    public Builder setUri(String uri) {
      this.uri = uri;
      return this;
    }

    public Builder setUsername(String username) {
      this.username = username;
      return this;
    }

    public Builder setPassword(String password) {
      this.password = password;
      return this;
    }

    public Builder setTable(String table) {
      this.table = table;
      return this;
    }

    public Builder setDetectPeriod(Integer detectPeriod) {
      this.detectPeriod = detectPeriod;
      return this;
    }

    public Builder setFilterSql(String filterSql) {
      this.filterSql = filterSql;
      return this;
    }

    public MysqlSourceWrapper build() {
      return new MysqlSourceWrapper(this);
    }

  }

  @Override
  public void registerInput(IotWorkRuntimeContext context) {
    MysqlConnWrapper mysqlConnWrapper = new MysqlConnWrapper("com.mysql.jdbc.Driver", uri, username,
        password, "select 1",
        table, getSchema(), limit, filterSql);
    context.putSourceConnToCache(getNodeId(), mysqlConnWrapper);
    SourceFunction sourceFunction = new MysqlRichSourceFunction(mysqlConnWrapper,
        getKafkaLoggingServiceServers(), getKafkaLoggingServiceTopics());
    StreamExecutionEnvironment env = context.getExecutionEnv();
    DataStream<String> streamSource = env.addSource(sourceFunction).uid(getNodeId())
        .setParallelism(1).name(getNodeName());
    context.putObjectToContainer(

        getNodeId(), streamSource);
  }

  class MysqlRichSourceFunction extends JdbcRichSourceFunction<Set<RcUnit>> {

    private final String kafkaLoggingServiceServers;
    private final String kafkaLoggingServiceTopics;


    /**
     * 构造方法.
     */
    public MysqlRichSourceFunction(
        JdbcConnWrapper jdbcConnWrapper, String kafkaLoggingServiceServers,
        String kafkaLoggingServiceTopics
    ) {
      super(jdbcConnWrapper);
      this.kafkaLoggingServiceServers = kafkaLoggingServiceServers;
      this.kafkaLoggingServiceTopics = kafkaLoggingServiceTopics;
    }

    @Override
    public void open(Configuration parameters) throws Exception {
      super.open(parameters);
      openProducer(kafkaLoggingServiceServers);
    }

    @Override
    public void run(SourceContext<Set<RcUnit>> ctx) {
      while (isRunning() && !Thread.currentThread().isInterrupted()) {
        try {
          Set<RcUnit> result = null;
          if (!Thread.currentThread().isInterrupted()) {
            result = jdbcConnWrapper.getData();
          }
          if (!Thread.currentThread().isInterrupted() && result != null) {
            ctx.collect(result);
          }
        } catch (Exception ex) {
          logger.error("Exception occurred when reading data from mysql: {}",
              ex.getMessage(), ex);
          LoggingServiceUtil.alarm(LogVo.builder()
              .k8sServiceName(GrayLogConstant.K8S_SERVICE_NAME)
              .module(GrayLogConstant.MODULE_NAME)
              .userName(getTenantId())
              .userId(getUserId())
              .tenantId(getTenantId())
              .operateObjectName(getJobName())
              .operateObject(getJobId())
              .shortMessage(String.format("Exception occurred in node %s when reading "
                  + "data from mysql. Detail: %s",getNodeName(),ex.toString()))
              .operation(ShortMessageConstant.READ_MYSQL_DATA)
              .requestId(String.valueOf(System.currentTimeMillis()))
              .result(GrayLogResultEnum.FAIL.getValue())
              .logType(getLogType())
              .kafkaLoggingServiceTopic(kafkaLoggingServiceTopics)
              .kafkaLoggingServiceServers(kafkaLoggingServiceServers)
              .build(), flinkKafkaInternalProducer);
        }
        try {
          Thread.sleep(detectPeriod * 1000);
        } catch (InterruptedException ex) {
          logger.warn("Thread is interrupted.");
          break;
        } catch (NullPointerException np) {
          logger.error("detectPeriod is null.");
        } catch (Exception e) {
          logger.error(e.toString());
        }
      }
      logger.info("Mysql source is over,URI：{}，Table：{}", uri, table);
    }

    @Override
    public void cancel() {
      logger.info("Mysql source is canceled.");

      LoggingServiceUtil.info(LogVo.builder()
          .k8sServiceName(GrayLogConstant.K8S_SERVICE_NAME)
          .module(GrayLogConstant.MODULE_NAME)
          .userName(getTenantId())
          .userId(getUserId())
          .tenantId(getTenantId())
          .operateObjectName(getJobName())
          .operateObject(getJobId())
          .shortMessage(String.format("Node %s disconnected from Mysql. "
              + "Database connection string: %s. Table: %s.",getNodeName(),getUri(),getTable()))
          .operation(ShortMessageConstant.CLOSE_MYSQL)
          .requestId(String.valueOf(System.currentTimeMillis()))
          .result(GrayLogResultEnum.SUCCESS.getValue())
          .logType(getLogType())
          .kafkaLoggingServiceTopic(kafkaLoggingServiceTopics)
          .kafkaLoggingServiceServers(kafkaLoggingServiceServers)
          .build(), flinkKafkaInternalProducer);

    }

    @Override
    public void close() throws Exception {

      logger.info("Mysql source is closed.");

      LoggingServiceUtil.info(LogVo.builder()
          .k8sServiceName(GrayLogConstant.K8S_SERVICE_NAME)
          .module(GrayLogConstant.MODULE_NAME)
          .userName(getTenantId())
          .userId(getUserId())
          .tenantId(getTenantId())
          .operateObjectName(getJobName())
          .operateObject(getJobId())
          .shortMessage(String.format("Node %s disconnected from Mysql. Database connection "
              + "string: %s. Table: %s.",getNodeName(),getUri(),getTable()))
          .operation(ShortMessageConstant.CLOSE_MYSQL)
          .requestId(String.valueOf(System.currentTimeMillis()))
          .result(GrayLogResultEnum.SUCCESS.getValue())
          .logType(getLogType())
          .kafkaLoggingServiceTopic(kafkaLoggingServiceTopics)
          .kafkaLoggingServiceServers(kafkaLoggingServiceServers)
          .build(), flinkKafkaInternalProducer);
      super.close();
    }
  }

  ;
}
