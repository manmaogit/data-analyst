/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.wrapper.source;

import static com.rootcloud.analysis.core.constants.CommonConstant.BACK_SLASH_QUOTE;
import static com.rootcloud.analysis.core.constants.CommonConstant.DOLLAR;
import static com.rootcloud.analysis.core.constants.JobConstant.TD_ENGINE_TIME_COLUMN_NAME;
import static com.rootcloud.analysis.core.constants.SchemaConstant.ANCESTORS;
import static com.rootcloud.analysis.core.constants.SchemaConstant.DEVICE_ID;
import static com.rootcloud.analysis.core.constants.SchemaConstant.DEVICE_TYPE_ID;
import static com.rootcloud.analysis.core.constants.SchemaConstant.DOT;
import static com.rootcloud.analysis.core.constants.SchemaConstant.DOUBLE_QUOTE;
import static com.rootcloud.analysis.core.constants.SchemaConstant.TENANT_ID;
import static com.rootcloud.analysis.core.constants.SchemaConstant.TIMESTAMP;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.google.common.base.Strings;
import com.google.common.collect.Maps;
import com.rootcloud.analysis.common.entity.DeviceMapping;
import com.rootcloud.analysis.common.entity.RcUnit;
import com.rootcloud.analysis.common.util.DateUtil;
import com.rootcloud.analysis.core.constants.SchemaConstant;
import com.rootcloud.analysis.core.enums.HistorianAggFunctionEnum;
import com.rootcloud.analysis.core.enums.HistorianDataTypeEnum;
import com.rootcloud.analysis.core.enums.HistorianIgnoreTagType;
import com.rootcloud.analysis.core.enums.TimeZoneTypeEnum;
import com.rootcloud.analysis.entity.TdEngineSchema;
import com.rootcloud.analysis.entity.TdEngineSchemaField;
import com.rootcloud.analysis.exception.SourceReadDataException;
import com.rootcloud.analysis.wrapper.IDataSourceWrapper;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import lombok.Getter;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * TdEngine连接器父类.
 *
 * @CreatedBy: zexin.huang
 * @Date: 2022/5/6 16:29
 */
@Getter
public abstract class TdEngineConnBaseWrapper implements IDataSourceWrapper {

  private final Logger logger = LoggerFactory.getLogger(TdEngineConnBaseWrapper.class);

  // true：聚合查询模式，false：原始查询模式.
  private final boolean isWithAgg;
  private final String userName;
  private final String password;
  private final String database;
  private final String driverClassName = "com.taosdata.jdbc.rs.RestfulDriver";
  private transient Connection conn;

  private final List<String> urls;
  private final JSONArray schema;

  private final String timezone;

  private final String tenantId;
  private final long timeLowerBound;
  private final long timeUpperBound;

  // Historian不额外加$后缀的内置字段集合.
  private final Set<String> ignoredFieldNames;
  // 设备模型ID、抽象模型ID、设备ID集合 的映射关系.
  private final List<DeviceMapping> deviceMappings;

  // 为了绕过TdEngine别名不能为关键字的限制，需要在查询时加固定前缀.
  private final String outputColumnNamePrefix = "_&_&";

  /**
   * TdEngineConnBaseWrapper.
   */
  public TdEngineConnBaseWrapper(boolean isWithAgg, String userName, String password,
                                 String database, List<String> urls, JSONArray schema,
                                 String timezone, String tenantId,
                                 long timeLowerBound, long timeUpperBound,
                                 Set<String> ignoredFieldNames,
                                 List<DeviceMapping> deviceMappings) {
    this.isWithAgg = isWithAgg;
    this.userName = userName;
    this.password = password;
    this.database = database;
    this.urls = urls;
    this.schema = schema;
    this.timezone = timezone;
    this.tenantId = tenantId;
    this.timeLowerBound = timeLowerBound;
    this.timeUpperBound = timeUpperBound;
    this.ignoredFieldNames = ignoredFieldNames;
    this.deviceMappings = deviceMappings;
  }

  /**
   * 建立连接.
   */
  protected void openConnection() throws Exception {

    synchronized (getClass().getClassLoader()) {
      Class.forName(driverClassName);
    }
    for (String url : urls) {
      String jdbcUrl = String.format("jdbc:TAOS-RS://%s/%s?user=%s&password=%s",
            url, database, userName, password);
      try {
        this.conn = DriverManager.getConnection(jdbcUrl);
        logger.info("Get TdEngine connection success! url = {}, database={}", url, database);
        break;
      } catch (Exception e) {
        logger.error("Get TdEngine connection error, database = {}, jdbcUrl = {}, exception:",
            database, jdbcUrl, e);
      }
    }
    if (this.conn == null) {
      throw new SourceReadDataException(
          "Failed to get available TdEngine connection!urls = " + urls);
    }
  }

  /**
   * 获取需要查询的目标字段名集合（处理类型后缀和聚合函数）.
   * @return 需要查询的字段名集合.
   */
  public List<TdEngineSchemaField> getTargetColumns() {
    List<TdEngineSchemaField> targetColumns = new LinkedList<>();
    for (Object obj : getSchema()) {
      JSONObject jsonObj = (JSONObject) obj;
      String outputColumnName =
          Strings.isNullOrEmpty(jsonObj.getString(SchemaConstant.ATTR_ORIGINAL_NAME))
              ? jsonObj.getString(SchemaConstant.ATTR_NAME)
              : jsonObj.getString(SchemaConstant.ATTR_ORIGINAL_NAME);
      String dataType = Strings.isNullOrEmpty(
          jsonObj.getString(SchemaConstant.ATTR_OLD_DATA_TYPE))
          ? jsonObj.getString(SchemaConstant.ATTR_DATA_TYPE)
          : jsonObj.getString(SchemaConstant.ATTR_OLD_DATA_TYPE);
      if (StringUtils.equals(outputColumnName, TENANT_ID)
          || StringUtils.equals(outputColumnName, DEVICE_TYPE_ID)) {
        continue;
      }
      // 聚合函数.
      HistorianAggFunctionEnum aggFunction = HistorianAggFunctionEnum
          .getAggFunctionEnum(jsonObj.getString(SchemaConstant.AGG_FUNCTION));
      if (this.isWithAgg && HistorianAggFunctionEnum.IGNORED == aggFunction) {
        continue;
      }
      // 所有字段查询TdEngine时需要统一转小写，以后可能会改回来.
      String queryColumnName = outputColumnName;
      TdEngineSchemaField schemaField = new TdEngineSchemaField();
      schemaField.setAggregateFunction(aggFunction);
      if (ignoredFieldNames.contains(outputColumnName)) {
        if (StringUtils.equals(outputColumnName, TIMESTAMP)) {
          queryColumnName = TD_ENGINE_TIME_COLUMN_NAME;
        }
      } else {
        // Historian 会对字段KEY额外进行类型的后缀拼接.
        // 特殊：DECIMAL类型且精度为0，在Historian里会按后缀$i保存
        if (StringUtils.equals("DECIMAL", dataType)) {
          Integer scale = jsonObj.getInteger(SchemaConstant.ATTR_SCALE);
          if (scale != null && scale == 0) {
            dataType = HistorianDataTypeEnum.Integer.getFlinkType();
          }
        }
        // Historian会对JSON类型字段的部分Key做特殊处理.
        if (StringUtils.equals(HistorianDataTypeEnum.Json.getFlinkType(), dataType)
            && outputColumnName.contains(DOT)) {
          String outputDataType = jsonObj.getString(SchemaConstant.ATTR_DATA_TYPE);
          String[] split = outputColumnName.split("\\.");
          if (HistorianIgnoreTagType.isHistorianIgnoreTagType(outputColumnName)
                && HistorianDataTypeEnum.isPrimitiveType(outputDataType)) {
            outputColumnName = split[1];
            dataType = outputDataType;

          } else {
            outputColumnName = split[0];
            queryColumnName = outputColumnName;
          }
        }
        schemaField.setHistorianType(HistorianDataTypeEnum.getHistorianType(dataType));
      }

      schemaField.setOutputColumnName(outputColumnNamePrefix + outputColumnName);
      schemaField.setQueryColumnName(queryColumnName);
      targetColumns.add(schemaField);
    }
    return targetColumns;
  }

  /**
   * 获取拼接查询SQL所需的Schema信息.
   * 兼容单个超级表因字段太多被拆分成多个超级表的情况.
   */
  public List<TdEngineSchema> getSchemaList(final String tableNameDefault,
                                               final List<String> limitDeviceIds)
      throws Exception {
    Statement stmt = this.conn.createStatement();
    // 1. 按设备模型ID模糊匹配拆分后的超级表集合；（show database.stables like 'xxx%')
    List<String> stables =  new LinkedList<>();
    String sql = String.format("show %s.stables like '%s%%'", database, tableNameDefault);
    ResultSet rs = stmt.executeQuery(sql);
    while (rs.next()) {
      String stableName = rs.getString(1);
      stables.add(stableName);
    }
    List<TdEngineSchema> result = new LinkedList<>();
    if (CollectionUtils.isEmpty(stables)) {
      return result;
    }

    List<String> deviceIds = new LinkedList<>();
    if (CollectionUtils.isNotEmpty(limitDeviceIds)) {
      // 说明指定了设备ID，不需要查询该模型下的所有设备.
      deviceIds.addAll(limitDeviceIds);
    } else {
      // 2. 找到超级表下所包含的设备ID集合；（select __deviceId__ from database.`xxx`）
      sql = String.format("select `__deviceId__` from  %s.`%s`", database, tableNameDefault);
      rs = stmt.executeQuery(sql);
      while (rs.next()) {
        String deviceId = rs.getString(1);
        deviceIds.add(deviceId);
      }
    }

    // 3. 轮询超级表集合，确定超级表与字段集合的映射关系；(describe database.`xxx`)
    List<TdEngineSchemaField> targetColumns = this.getTargetColumns();
    for (String stable : stables) {
      sql = String.format("describe %s.`%s`", database, stable);
      rs = stmt.executeQuery(sql);
      List<TdEngineSchemaField> fields = new LinkedList<>();
      while (rs.next()) {
        String fieldName = rs.getString(1);
        if (StringUtils.equalsIgnoreCase(fieldName, TENANT_ID)
            || StringUtils.equalsIgnoreCase(fieldName, DEVICE_TYPE_ID)) {
          continue;
        }
        targetColumns.forEach(schemaField -> {
          String columnNameWithDataType = schemaField.getQueryColumnName() + DOLLAR
              + schemaField.getHistorianType();
          if (StringUtils.equals(fieldName, schemaField.getQueryColumnName())
              || StringUtils.equals(fieldName, columnNameWithDataType)) {
            fields.add(schemaField);
          }
        });
      }
      if (!fields.isEmpty()) {
        TdEngineSchema tdEngineSchema = new TdEngineSchema();
        tdEngineSchema.setStabeName(stable);
        tdEngineSchema.setFields(fields);
        tdEngineSchema.setDeviceIds(deviceIds);
        result.add(tdEngineSchema);
      }
    }
    return result;
  }

  /**
   * 查询数据并汇总结果.
   */
  protected void queryAndCollectData(String sql,
                                     List<Map<String, Object>> deviceTotalResult,
                                     String abstractDeviceTypeId, String deviceTypeId,
                                     String deviceId)
      throws Exception {
    List<Map<String, Object>> recordList = this.query(sql);
    for (Map<String, Object> record : recordList) {
      Map newRecord = new HashMap(record.size() * 2);
      // 还原查询时拼接的固定前缀.
      for (String key : record.keySet()) {
        String newKey = key.replace(outputColumnNamePrefix, "");
        newRecord.put(newKey, record.get(key));
      }
      // 设备模型ID.
      newRecord.putIfAbsent(DEVICE_TYPE_ID, deviceTypeId);
      // 设备ID.
      newRecord.putIfAbsent(DEVICE_ID, deviceId);
      // 抽象模型ID.
      newRecord.putIfAbsent(ANCESTORS, abstractDeviceTypeId);
      // 租户ID.
      newRecord.putIfAbsent(TENANT_ID, tenantId);
      // 时间戳Key更正（适用于聚合模式下）.
      if (newRecord.containsKey(TD_ENGINE_TIME_COLUMN_NAME)) {
        newRecord.put(TIMESTAMP, newRecord.get(TD_ENGINE_TIME_COLUMN_NAME));
        newRecord.remove(TD_ENGINE_TIME_COLUMN_NAME);
      }
      // 时间戳的时区转换：从零时区转为指定时区.
      Timestamp utcTimestamp = (Timestamp) newRecord.get(TIMESTAMP);
      Timestamp cttTimestamp = new Timestamp(DateUtil.convertTimeToZoneDate(
          utcTimestamp.getTime(), TimeZoneTypeEnum.valueOf(timezone).getZoneId()));
      newRecord.put(TIMESTAMP, cttTimestamp);
      // 汇总结果.
      this.collectQueryResult(deviceTotalResult, newRecord);
    }
  }

  /**
   * 通过SQL方式查询数据.
   */
  private List<Map<String, Object>> query(String sql)
      throws Exception {
    Statement stmt = this.conn.createStatement();
    logger.info("TdEngine querySql = {}", sql);
    ResultSet rs = stmt.executeQuery(sql);

    List<Map<String, Object>> result = new LinkedList<>();
    while (rs.next()) {
      ResultSetMetaData rsMetaData = rs.getMetaData();
      Map<String, Object> newRecord = Maps.newHashMap();
      for (int i = 1; i <= rsMetaData.getColumnCount(); i++) {
        String key = rsMetaData.getColumnName(i);
        Object value = rs.getObject(i);
        // 针对非法转义字符的处理.
        if (value != null && value instanceof String
            && value.toString().contains(BACK_SLASH_QUOTE)) {
          value = value.toString().replace(BACK_SLASH_QUOTE, DOUBLE_QUOTE);
        }
        newRecord.put(key, value);
      }
      result.add(newRecord);
    }
    return result;
  }

  /**
   * 将查询结果汇总到现有记录里.
   * @param deviceTotalResult 汇总后的结果集合.
   * @param newRecord 查询到的新记录.
   */
  private void collectQueryResult(List<Map<String, Object>> deviceTotalResult,
                                  Map<String, Object> newRecord) {
    if (deviceTotalResult == null || newRecord == null) {
      return;
    }
    String deviceId = (String) newRecord.get(DEVICE_ID);
    Timestamp ts = (Timestamp) newRecord.get(TIMESTAMP);
    boolean isMatch = false;
    for (Map<String, Object> oldRecord : deviceTotalResult) {
      String deviceId2 = (String) oldRecord.get(DEVICE_ID);
      Timestamp ts2 = (Timestamp) oldRecord.get(TIMESTAMP);
      if (StringUtils.equals(deviceId, deviceId2) && ts.equals(ts2)) {
        // 说明是同一条记录的不同字段数据，可以汇总查询结果.
        oldRecord.putAll(newRecord);
        isMatch = true;
        break;
      }
    }
    if (!isMatch) {
      // 没有匹配的说明是一条新的记录.
      deviceTotalResult.add(newRecord);
    }
  }

  /**
   * 解析原始数据，根据Schema转换为RcUnit.
   */
  protected Set<RcUnit> parseData2RcUnit(final List<Map<String, Object>> dataList) {
    Set<RcUnit> outPutSet = new HashSet<>();
    for (Map<String, Object> data : dataList) {
      if (!Thread.currentThread().isInterrupted()) {
        try {
          outPutSet.add(RcUnit.valueOfDataTypeInfo(data, getSchema()));
        } catch (Exception e) {
          logger.error(String.format("TdEngine parseData2RcUnit data = %s, exception:", data), e);
          throw new SourceReadDataException("TdEngine has illegal data! data" + data);
        }
      }
    }
    return outPutSet;
  }

  public void open() throws Exception {
    this.openConnection();
  }

  /**
   * 关闭连接.
   */
  @Override
  public void close() {
    try {
      this.conn.close();
    } catch (SQLException e) {
      logger.error("Failed close TdEngine connection, urls = {}", urls);
    }
  }
}
