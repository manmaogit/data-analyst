/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.wrapper.source;

import com.rootcloud.analysis.common.context.IotWorkRuntimeContext;
import com.rootcloud.analysis.common.entity.DeviceMapping;
import com.rootcloud.analysis.common.entity.RcUnit;
import com.rootcloud.analysis.core.constants.GrayLogConstant;
import com.rootcloud.analysis.core.constants.ShortMessageConstant;
import com.rootcloud.analysis.core.enums.GrayLogResultEnum;
import com.rootcloud.analysis.core.graylog.LogVo;
import com.rootcloud.analysis.log.loggingservice.LoggingServiceUtil;

import java.util.List;
import java.util.Set;
import lombok.Builder;
import lombok.Getter;
import org.apache.flink.api.common.functions.AbstractRichFunction;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.source.SourceFunction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * TDEngine输入节点-查询原始数据.
 *
 * @CreatedBy: zexin.huang
 * @Date: 2022/5/5 14:03
 */
@Getter
@Builder
public class TdEngineSourceWrapper extends AbstractBatchSourceWrapper {

  private final Logger logger = LoggerFactory.getLogger(TdEngineSourceWrapper.class);

  private final String tenantId;
  private final List<String> urls;
  private final String userName;
  private final String password;
  private final String database;
  private final int queryScopeSeconds;
  private final long timeLowerBound;
  private final long timeUpperBound;
  private final Set<String> ignoredFieldNames;
  // 设备模型ID、抽象模型ID、设备ID集合 的映射关系.
  private final List<DeviceMapping> deviceMappings;

  @Override
  public void registerInput(IotWorkRuntimeContext context) {
    TdEngineConnWrapper connWrapper = new TdEngineConnWrapper(
        tenantId, urls, userName, password, database, getSchema(), getTimeZone(),
        timeLowerBound, timeUpperBound, ignoredFieldNames, deviceMappings, queryScopeSeconds);

    context.putSourceConnToCache(getNodeId(), connWrapper);

    SourceFunction sourceFunction = new TdEngineSourceWrapper.TdEngineSourceFunction(connWrapper);
    StreamExecutionEnvironment env = context.getExecutionEnv();
    SingleOutputStreamOperator<RcUnit> sourceStream = env
        .addSource(sourceFunction)
        .uid(getNodeId())
        .setParallelism(1)
        .name(getNodeName());
    context.putObjectToContainer(getNodeId(), sourceStream);

    try {
      createTemporaryViewForRow(context, sourceStream);
    } catch (Exception e) {
      logger.error("createTemporaryView-error:", e);
    }
  }

  private class TdEngineSourceFunction
      extends AbstractRichFunction implements SourceFunction<RcUnit> {

    private TdEngineConnWrapper connWrapper;

    public TdEngineSourceFunction(TdEngineConnWrapper connWrapper) {
      this.connWrapper = connWrapper;
    }

    @Override
    public void open(Configuration parameters) throws Exception {
      super.open(parameters);
      this.connWrapper.open();
    }

    @Override
    public void run(SourceContext<RcUnit> ctx) {
      try {
        Set<RcUnit> result = null;
        if (!Thread.currentThread().isInterrupted()) {
          result = connWrapper.getData();
        }
        if (!Thread.currentThread().isInterrupted() && result != null) {
          for (RcUnit rcUnit : result) {
            if (!Thread.currentThread().isInterrupted()) {
              ctx.collect(rcUnit);
            }
          }
        }
      } catch (Exception ex) {
        LoggingServiceUtil.error(LogVo.builder()
            .k8sServiceName(GrayLogConstant.K8S_SERVICE_NAME)
            .module(GrayLogConstant.MODULE_NAME)
            .userName(tenantId)
            .tenantId(tenantId)
            .operateObjectName(getJobName())
            .operateObject(getJobId())
            .shortMessage(getNodeName() + "Read TdEngine data error：Database="
                + connWrapper.getDatabase()
                + "，deviceMappings=" + connWrapper.getDeviceMappings()
                + ex)
            .operation(ShortMessageConstant.READ_INFLUXDB_DATA)
            .requestId(String.valueOf(System.currentTimeMillis()))
            .result(GrayLogResultEnum.FAIL.getValue())
            .logType(getLogType())
            .build());
        throw new RuntimeException("Exception occurred when reading data from TdEngine: {}", ex);
      }
      logger.info("TdEngine source is over,Url：{}，Database：{}，DeviceMappings：{}",
          connWrapper.getUrls(), connWrapper.getDatabase(), connWrapper.getDeviceMappings());
    }

    @Override
    public void cancel() {
      logger.info("TdEngine source is canceled.");

      LoggingServiceUtil.info(LogVo.builder()
          .k8sServiceName(GrayLogConstant.K8S_SERVICE_NAME)
          .module(GrayLogConstant.MODULE_NAME)
          .userName(tenantId)
          .tenantId(tenantId)
          .operateObjectName(getJobName())
          .operateObject(getJobId())
          .shortMessage(
              getNodeName() + " close TdEngine connection success，"
                  + "，DeviceMappings=" + connWrapper.getDeviceMappings())
          .operation(ShortMessageConstant.CLOSE_INFLUXDB)
          .requestId(String.valueOf(System.currentTimeMillis()))
          .result(GrayLogResultEnum.SUCCESS.getValue())
          .logType(getLogType())
          .build());
    }
  }
}
