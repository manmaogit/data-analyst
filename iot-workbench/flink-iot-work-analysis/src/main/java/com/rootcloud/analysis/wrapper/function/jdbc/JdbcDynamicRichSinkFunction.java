/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.wrapper.function.jdbc;

import com.rootcloud.analysis.common.jdbc.ConnectionWrapper;
import com.rootcloud.analysis.wrapper.jdbc.JdbcConnWrapper;
import com.rootcloud.analysis.wrapper.jdbc.JdbcDynamicConnWrapper;
import org.apache.flink.streaming.api.functions.sink.SinkFunction;

public abstract class JdbcDynamicRichSinkFunction<T> extends JdbcDynamicRichFunction implements SinkFunction<T> {

  private static final long serialVersionUID = 1L;

  /**
   * 构造方法.
   */
  public JdbcDynamicRichSinkFunction(JdbcDynamicConnWrapper jdbcDynamicConnWrapper) {
    super(jdbcDynamicConnWrapper);
  }

  /**
   * 获取connection对象.
   */
  protected ConnectionWrapper getConnectionWrapper() {
    return this.jdbcDynamicConnWrapper.getConnectionWrapper();
  }
}
