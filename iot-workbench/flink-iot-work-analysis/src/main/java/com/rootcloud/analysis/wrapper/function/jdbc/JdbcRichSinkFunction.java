/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.wrapper.function.jdbc;

import com.rootcloud.analysis.common.factory.KafkaLogMiddlewareResourceFactory;
import com.rootcloud.analysis.common.jdbc.ConnectionWrapper;
import com.rootcloud.analysis.wrapper.jdbc.JdbcConnWrapper;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.functions.sink.SinkFunction;

public abstract class JdbcRichSinkFunction<T> extends JdbcRichFunction implements SinkFunction<T> {

  private static final long serialVersionUID = 1L;

  /**
   * 构造方法.
   */
  public JdbcRichSinkFunction(JdbcConnWrapper jdbcConnWrapper) {
    super(jdbcConnWrapper);
  }

  /**
   * 获取connection对象.
   */
  protected ConnectionWrapper getConnectionWrapper() {
    return this.jdbcConnWrapper.getConnectionWrapper();
  }

  @Override
  public void open(Configuration parameters) throws Exception {
    jdbcConnWrapper.getConnectionWrapper().connect();
  }

  @Override
  public void openProducer(String server) {
    if (flinkKafkaInternalProducer == null && server != null) {
      flinkKafkaInternalProducer = KafkaLogMiddlewareResourceFactory.getProducer(server, this.toString());
    }
  }
}
