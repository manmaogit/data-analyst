/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.wrapper.processor;

import com.alibaba.fastjson.JSONArray;
import com.rootcloud.analysis.common.context.IotWorkRuntimeContext;
import com.rootcloud.analysis.common.entity.RcUnit;
import com.rootcloud.analysis.common.factory.KafkaLogMiddlewareResourceFactory;
import com.rootcloud.analysis.core.constants.GrayLogConstant;
import com.rootcloud.analysis.core.constants.ShortMessageConstant;
import com.rootcloud.analysis.core.enums.GrayLogResultEnum;
import com.rootcloud.analysis.core.graylog.LogVo;
import com.rootcloud.analysis.log.loggingservice.LoggingServiceUtil;
import com.rootcloud.analysis.wrapper.WrapperBuilder;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.functions.ProcessFunction;
import org.apache.flink.streaming.connectors.kafka.internals.FlinkKafkaInternalProducer;
import org.apache.flink.util.Collector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Getter
@ToString(callSuper = true)
public class SchemaProcessorWrapper extends AbstractProcessorWrapper {

  private static Logger logger = LoggerFactory.getLogger(SchemaProcessorWrapper.class);
  private final JSONArray schema;

  private SchemaProcessorWrapper(Builder builder) {
    super(builder.jobId, builder.jobName, builder.tenantId, builder.nodeId, builder.parallelism,
        builder.nodeName, builder.priorNodeId, builder.processType, builder.logType);
    this.schema = builder.schema;

  }

  @ToString
  public static class Builder extends WrapperBuilder {

    private JSONArray schema;
    private String nodeId;
    private String nodeName;
    private Integer parallelism;
    private String priorNodeId;
    private String processType;

    public void setSchema(JSONArray schema) {
      this.schema = schema;
    }

    public Builder setNodeId(String nodeId) {
      this.nodeId = nodeId;
      return this;
    }

    public Builder setParallelism(Integer parallelism) {
      this.parallelism = parallelism;
      return this;
    }

    public Builder setPriorNodeId(String priorNodeId) {
      this.priorNodeId = priorNodeId;
      return this;
    }

    public Builder setProcessType(String processType) {
      this.processType = processType;
      return this;
    }


    public Builder setNodeName(String nodeName) {
      this.nodeName = nodeName;
      return this;
    }

    public SchemaProcessorWrapper build() {
      return new SchemaProcessorWrapper(this);
    }
  }

  @Override
  public void registerProcess(IotWorkRuntimeContext context) throws Exception {
    DataStream<RcUnit> priorDataStream = context.getObjectFromContainer(getPriorNodeId());
    DataStream<RcUnit> filteredStream = priorDataStream
        .process(new SchemaFlatMapFunction(getKafkaLoggingServiceServers(),
            getKafkaLoggingServiceTopics(), null)).uid(getNodeId()).setParallelism(getParallelism())
        .name(getNodeName());
    context.putObjectToContainer(getNodeId(), filteredStream);
  }

  @AllArgsConstructor
  private class SchemaFlatMapFunction extends ProcessFunction<RcUnit, RcUnit> {

    private final String kafkaLoggingServiceServers;
    private final String kafkaLoggingServiceTopics;
    private FlinkKafkaInternalProducer flinkKafkaInternalProducer;

    @Override
    public void open(Configuration parameters) throws Exception {
      super.open(parameters);
      if (flinkKafkaInternalProducer == null && kafkaLoggingServiceServers != null) {
        flinkKafkaInternalProducer = KafkaLogMiddlewareResourceFactory.getProducer(kafkaLoggingServiceServers, this.toString());
      }

    }

    @Override
    public void close() throws Exception {
      super.close();
      KafkaLogMiddlewareResourceFactory.close(this.toString());
      flinkKafkaInternalProducer = null;
    }

    @Override
    public void processElement(RcUnit value, ProcessFunction<RcUnit, RcUnit>.Context ctx,
                               Collector<RcUnit> out) throws Exception {
      if (value.getData() != null) {
        try {
          logger.debug("Consuming message {} from  schema.", value);
          RcUnit unit = RcUnit.valueOf(value.getData(), getSchema());
          logger.debug("Schema message handling result: {}", unit.getData());
          if (!Thread.currentThread().isInterrupted()) {
            out.collect(unit);
          }
        } catch (Exception e) {
          logger.error("Exception occurred  from schema.{}", value, e);
          LoggingServiceUtil.alarm(LogVo.builder()
              .k8sServiceName(GrayLogConstant.K8S_SERVICE_NAME)
              .module(GrayLogConstant.MODULE_NAME)
              .userName(getTenantId())
              .tenantId(getTenantId())
              .operateObjectName(getJobName())
              .operateObject(getJobId())
              .shortMessage(String.format("Process failure. Cause: %s. Data: %s", e.toString(), value))
              .operation(ShortMessageConstant.FILTER_SCHEMA_DATA)
              .requestId(String.valueOf(System.currentTimeMillis()))
              .result(GrayLogResultEnum.FAIL.getValue())
              .logType(getLogType())
              .kafkaLoggingServiceTopic(kafkaLoggingServiceTopics)
              .kafkaLoggingServiceServers(kafkaLoggingServiceServers)
              .build(), flinkKafkaInternalProducer);
        }
      } else {
        logger.warn("Consuming empty message from schema.");
        LoggingServiceUtil.warn(LogVo.builder()
            .k8sServiceName(GrayLogConstant.K8S_SERVICE_NAME)
            .module(GrayLogConstant.MODULE_NAME)
            .userName(getTenantId())
            .userId(getUserId())
            .tenantId(getTenantId())
            .operateObjectName(getJobName())
            .operateObject(getJobId())
            .shortMessage(String.format("Process failure. Data is null."))
            .operation(ShortMessageConstant.FILTER_SCHEMA_DATA)
            .requestId(String.valueOf(System.currentTimeMillis()))
            .result(GrayLogResultEnum.SUCCESS.getValue())
            .logType(getLogType())
            .kafkaLoggingServiceTopic(kafkaLoggingServiceTopics)
            .kafkaLoggingServiceServers(kafkaLoggingServiceServers)
            .build(), flinkKafkaInternalProducer);
      }
    }

  }

}
