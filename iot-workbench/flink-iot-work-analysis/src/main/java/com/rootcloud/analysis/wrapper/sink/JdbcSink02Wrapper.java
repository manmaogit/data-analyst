/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.wrapper.sink;

import static com.rootcloud.analysis.core.constants.WrapperConstant.NULL_IDENTIFICATION;
import static com.rootcloud.analysis.core.constants.WrapperConstant.SEPARATOR;

import com.google.common.collect.Maps;
import com.rootcloud.analysis.common.context.IotWorkRuntimeContext;
import com.rootcloud.analysis.context.IotWorkRuntimeBuildContext;
import com.rootcloud.analysis.core.constants.CommonConstant;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.apache.flink.api.java.functions.KeySelector;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.streaming.api.datastream.KeyedStream;
import org.apache.flink.table.api.Table;
import org.apache.flink.table.api.bridge.java.StreamTableEnvironment;
import org.apache.flink.types.Row;

@Getter
@Setter
@ToString(callSuper = true)
public abstract class JdbcSink02Wrapper extends AbstractSinkWrapper {

  private String uri;

  private String username;

  private String password;

  private String table;

  private List<String> columns;

  private List<String> updateColumns;

  private Map<String, String> attributeMapper;

  private Map<String, String> schema;

  private List<String> insertKey;

  private Map<String, Integer> priorTableColIndex;

  private static final int BATCH_SIZE = 1000;

  @Getter
  private UpsertKeyBuilder upsertKeyBuilder = new UpsertKeyBuilder();

  /**
   * Gets batch size.
   */
  protected int getBatchSize() {
    return BATCH_SIZE;
  }

  /**
   * Gets prior data stream.
   */
  protected KeyedStream<Tuple2<Boolean, Row>, String> getPriorDataStream(IotWorkRuntimeContext context) {
    Table priorTable = IotWorkRuntimeBuildContext.getTableObject(getPriorNodeId());
    StreamTableEnvironment tableEnv = context.getTableEnvFromCache();
    if (priorTable == null) {
      priorTable = tableEnv.sqlQuery(buildTableQuerySql());
    }
    priorTableColIndex = Maps.newHashMap();
    for (int i = 0; i < priorTable.getSchema().getFieldNames().length; i++) {
      priorTableColIndex.put(priorTable.getSchema().getFieldNames()[i], i);
    }
    return tableEnv.toRetractStream(priorTable, Row.class).keyBy(
        (KeySelector<Tuple2<Boolean, Row>, String>) record ->
            upsertKeyBuilder.build(insertKey, record, priorTableColIndex, attributeMapper)
    );
  }

  /**
   * Builds table query sql.
   */
  protected String buildTableQuerySql() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("select ");
    int colCount = 0;
    for (String colName : attributeMapper.values()) {
      if (colCount > 0) {
        stringBuilder.append(CommonConstant.COMMA);
      }
      stringBuilder.append("`");
      stringBuilder.append(colName);
      stringBuilder.append("`");
      colCount++;
    }
    stringBuilder.append(" from ");
    stringBuilder.append(IotWorkRuntimeBuildContext.getTemporaryViewName(getPriorNodeId()));
    return stringBuilder.toString();
  }

  @AllArgsConstructor
  public static class UpsertKeyBuilder implements Serializable {

    private static final long serialVersionUID = 8720931237798424138L;

    /**
     * Builds upsert key.
     */
    String build(List<String> insertKey, Tuple2<Boolean, Row> record,
                 Map<String, Integer> priorTableColIndex, Map<String, String> attributeMapper) {
      return insertKey.stream()
          .map(key -> {
            if (priorTableColIndex.containsKey(attributeMapper.get(key))) {
              Object val = record.f1.getField(priorTableColIndex.get(attributeMapper.get(key)));
              if (val != null) {
                return key + SEPARATOR + val;
              }
            }
            return key + SEPARATOR + NULL_IDENTIFICATION;
          }).collect(Collectors.joining(SEPARATOR));
    }
  }

}

