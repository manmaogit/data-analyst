/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.wrapper.time.scope;

import com.rootcloud.analysis.wrapper.IWrapper;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@ToString(callSuper = true)
public abstract class AbstractTimeScopeWrapper implements IWrapper {

  /**
   * 业务开始时间（非原始时间）.
   */
  private String startTime;

  /**
   * 业务结束时间（非原始时间）.
   */
  private String endTime;

  /**
   * 业务开始时间（非原始时间）毫秒数.
   */
  private long startEpochSecond;

  /**
   * 业务结束时间（非原始时间）毫秒数.
   */
  private long endEpochSecond;

}
