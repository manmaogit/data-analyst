/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.wrapper.source;

import com.rootcloud.analysis.common.context.IotWorkRuntimeContext;
import com.rootcloud.analysis.common.entity.DeviceMapping;
import com.rootcloud.analysis.common.entity.RcUnit;
import com.rootcloud.analysis.common.factory.KafkaLogMiddlewareResourceFactory;
import com.rootcloud.analysis.core.constants.GrayLogConstant;
import com.rootcloud.analysis.core.constants.ShortMessageConstant;
import com.rootcloud.analysis.core.enums.GrayLogResultEnum;
import com.rootcloud.analysis.core.graylog.LogVo;
import com.rootcloud.analysis.log.loggingservice.LoggingServiceUtil;

import java.util.List;
import java.util.Set;
import lombok.Builder;
import lombok.Getter;
import org.apache.flink.api.common.functions.AbstractRichFunction;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.source.SourceFunction;
import org.apache.flink.streaming.connectors.kafka.internals.FlinkKafkaInternalProducer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * InfluxDB-查询聚合数据模式.
 */
@Builder
@Getter
public class InfluxDbAggDeviceSourceWrapper extends AbstractBatchSourceWrapper {

  private final String url;
  private final String userName;
  private final String password;
  private final String database;
  private final int timeOutSeconds;
  private final int startOfHour;
  private final String startTime;
  private final String endTime;
  private final int groupByTimeScope;
  private final String groupByTimeType;
  private final Set<String> ignoredFieldNames;
  private final String tenantId;

  // 设备模型ID、抽象模型ID、设备ID集合 的映射关系.
  private final List<DeviceMapping> deviceMappings;

  private final Logger logger =
      LoggerFactory.getLogger(InfluxDbAggDeviceSourceWrapper.class);

  @Override
  public void registerInput(IotWorkRuntimeContext context) {
    InfluxDbAggDeviceConnWrapper connWrapper = new InfluxDbAggDeviceConnWrapper(
        url, userName, password, database, tenantId, timeOutSeconds, getTimeZone(),
        getSchema(), startOfHour, startTime, endTime, groupByTimeScope, groupByTimeType,
        ignoredFieldNames, deviceMappings);
    context.putSourceConnToCache(getNodeId(), connWrapper);

    SourceFunction sourceFunction = new InfluxDbSourceFunction(connWrapper,
        getKafkaLoggingServiceServers(), getKafkaLoggingServiceTopics()
    );
    StreamExecutionEnvironment env = context.getExecutionEnv();
    SingleOutputStreamOperator<RcUnit> sourceStream = env
        .addSource(sourceFunction)
        .uid(getNodeId())
        .setParallelism(1)
        .name(getNodeName());
    context.putObjectToContainer(getNodeId(), sourceStream);

    try {
      createTemporaryViewForRow(context, sourceStream);
    } catch (Exception e) {
      logger.error("createTemporaryView-error:", e);
    }
  }

  private class InfluxDbSourceFunction
      extends AbstractRichFunction implements SourceFunction<RcUnit> {

    private InfluxDbAggDeviceConnWrapper connWrapper;
    private final String kafkaLoggingServiceServers;
    private final String kafkaLoggingServiceTopics;
    private FlinkKafkaInternalProducer flinkKafkaInternalProducer;

    public InfluxDbSourceFunction(InfluxDbAggDeviceConnWrapper connWrapper,
                                  String kafkaLoggingServiceServers,
                                  String kafkaLoggingServiceTopics) {
      this.connWrapper = connWrapper;
      this.kafkaLoggingServiceServers = kafkaLoggingServiceServers;
      this.kafkaLoggingServiceTopics = kafkaLoggingServiceTopics;
    }

    @Override
    public void open(Configuration parameters) throws Exception {
      super.open(parameters);
      if (flinkKafkaInternalProducer == null && kafkaLoggingServiceServers != null) {
        flinkKafkaInternalProducer = KafkaLogMiddlewareResourceFactory.getProducer(
            kafkaLoggingServiceServers,
            this.toString());
      }
    }

    @Override
    public void run(SourceContext<RcUnit> ctx) {
      try {
        Set<RcUnit> result = null;
        if (!Thread.currentThread().isInterrupted()) {
          result = connWrapper.getData();
        }
        if (!Thread.currentThread().isInterrupted() && result != null) {
          for (RcUnit rcUnit : result) {
            if (!Thread.currentThread().isInterrupted()) {
              ctx.collect(rcUnit);
            }
          }
        }
      } catch (Exception ex) {
        LoggingServiceUtil.error(LogVo.builder()
            .k8sServiceName(GrayLogConstant.K8S_SERVICE_NAME)
            .module(GrayLogConstant.MODULE_NAME)
            .userName(tenantId)
            .userId(getUserId())
            .tenantId(tenantId)
            .operateObjectName(getJobName())
            .operateObject(getJobId())
            .shortMessage(getNodeName() + "Read InfluxDB data error：Database="
                + database
                + "，Url=" + url
                + "，deviceMappings=" + deviceMappings
                + ex)
            .operation(ShortMessageConstant.READ_INFLUXDB_DATA)
            .requestId(String.valueOf(System.currentTimeMillis()))
            .result(GrayLogResultEnum.FAIL.getValue())
            .logType(getLogType())
            .kafkaLoggingServiceTopic(kafkaLoggingServiceTopics)
            .kafkaLoggingServiceServers(kafkaLoggingServiceServers)
            .build(), flinkKafkaInternalProducer);
        throw new RuntimeException("Exception occurred when reading data from InfluxDB: {}", ex);
      }
      logger.info("InfluxDB source is over,Url：{}，Database：{}，deviceMappings：{}",
          url, database, deviceMappings);
    }

    @Override
    public void cancel() {
      logger.info("InfluxDB source is canceled.");
    }

    @Override
    public void close() {
      try {
        super.close();
      } catch (Exception e) {
        LoggingServiceUtil.error(LogVo.builder()
            .k8sServiceName(GrayLogConstant.K8S_SERVICE_NAME)
            .module(GrayLogConstant.MODULE_NAME)
            .userName(tenantId)
            .userId(getUserId())
            .tenantId(tenantId)
            .operateObjectName(getJobName())
            .operateObject(getJobId())
            .shortMessage(getNodeName() + "Close InfluxDB data error：Database="
                + connWrapper.getDatabase()
                + "，Url=" + connWrapper.getUrl() + ", "
                + e)
            .operation(ShortMessageConstant.READ_INFLUXDB_DATA)
            .requestId(String.valueOf(System.currentTimeMillis()))
            .result(GrayLogResultEnum.FAIL.getValue())
            .logType(getLogType())
            .kafkaLoggingServiceTopic(kafkaLoggingServiceTopics)
            .kafkaLoggingServiceServers(kafkaLoggingServiceServers)
            .build(), flinkKafkaInternalProducer);
        throw new RuntimeException("Exception occurred when closing InfluxDB: {}", e);
      } finally {
        connWrapper.close();
        connWrapper = null;
        KafkaLogMiddlewareResourceFactory.close(this.toString());
        flinkKafkaInternalProducer = null;
      }
    }
  }
}
