/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.wrapper.source;

import com.alibaba.fastjson.JSONArray;
import com.rootcloud.analysis.common.context.IotWorkRuntimeContext;
import com.rootcloud.analysis.core.enums.GraylogLogTypeEnum;
import com.rootcloud.analysis.wrapper.AbstractWrapper;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@ToString(callSuper = true)
public abstract class AbstractSourceWrapper extends AbstractWrapper {

  private String sourceType;

  private JSONArray schema;

  protected AbstractSourceWrapper(String jobId, String jobName,
                                  String tenantId, String nodeId,
                                  Integer parallelism, String nodeName, String sourceType,
                                  JSONArray schema, GraylogLogTypeEnum logType) {
    super(jobId, jobName, tenantId, nodeId, parallelism, nodeName, logType);
    this.sourceType = sourceType;
    this.schema = schema;
  }

  public abstract void registerInput(IotWorkRuntimeContext context);

}
