/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.decoder.time.scope;

import com.rootcloud.analysis.scanner.ClasspathPackageScanner;
import java.io.IOException;
import java.lang.reflect.Modifier;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TimeScopeDecoderFactory {

  private static Logger logger = LoggerFactory.getLogger(TimeScopeDecoderFactory.class);
  private static Map<String, AbstractTimeScopeDecoder> decoderMap = new HashMap<>();

  static {
    ClasspathPackageScanner scanner = new ClasspathPackageScanner(
        "com.rootcloud.analysis.decoder.time.scope.impl");
    try {
      List<String> classList = scanner.getFullyQualifiedClassNameList();

      for (String className : classList) {
        try {
          Class clazz = TimeScopeDecoderFactory.class.getClassLoader()
              .loadClass(className.replace("com/rootcloud/analysis/decoder/time/scope/impl/",
                  ""));
          if (Modifier.isAbstract(clazz.getModifiers()) || clazz.isInterface()) {
            continue;
          }
          Object obj = clazz.newInstance();
          if (!(obj instanceof AbstractTimeScopeDecoder)) {
            continue;
          }
          AbstractTimeScopeDecoder decoder = (AbstractTimeScopeDecoder) obj;
          TimeScopeDecoder anno = decoder.getClass().getAnnotation(TimeScopeDecoder.class);
          if (anno == null) {
            continue;
          }
          decoderMap.put(anno.timeScopeTypeEnum().name(), decoder);
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException e) {
          logger.warn("Exception occurred when initializing AbstractTimeScopeDecoder instance", e);
        }
      }
    } catch (IOException e) {
      logger.error("Failed to init TimeScopeDecoderFactory", e);
    }
  }

  /**
   * Gets decoder.
   */
  public static AbstractTimeScopeDecoder getDecoder(String timeScopeType) {
    if (decoderMap.containsKey(timeScopeType)) {
      return decoderMap.get(timeScopeType);
    }

    logger.error("Time scope decoder not found: {}", timeScopeType);
    return null;
  }
}
