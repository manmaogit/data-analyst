/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.decoder.source.impl;

import com.alibaba.fastjson.JSONObject;
import com.rootcloud.analysis.context.IotWorkRuntimeDecodeContext;
import com.rootcloud.analysis.core.constants.JobConstant;
import com.rootcloud.analysis.core.util.RsaUtil;
import com.rootcloud.analysis.decoder.source.AbstractSourceDecoder;
import com.rootcloud.analysis.exception.DecodeSourceException;
import com.rootcloud.analysis.wrapper.source.AbstractSourceWrapper;
import com.rootcloud.analysis.wrapper.source.JdbcSource02Wrapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class JdbcSource02Decoder extends AbstractSourceDecoder {

  private static Logger logger = LoggerFactory.getLogger(JdbcSource02Decoder.class);

  @Override
  public AbstractSourceWrapper decodeSourceParams(JSONObject conf) {
    try {
      JdbcSource02Wrapper jdbcSource02Wrapper = decodeJdbcSourceParams(conf);
      JSONObject params = conf.getJSONObject(JobConstant.KEY_PARAMETERS);
      jdbcSource02Wrapper.setUri(params.getString(JobConstant.KEY_URI));
      jdbcSource02Wrapper.setUsername(params.getString(JobConstant.KEY_USERNAME));
      jdbcSource02Wrapper.setPassword(RsaUtil
          .decryptDataOnJava(params.getString(JobConstant.KEY_PASSWORD),
              IotWorkRuntimeDecodeContext.getPrivateKey()));
      jdbcSource02Wrapper.setTable(conf.getString(JobConstant.KEY_TABLE));
      jdbcSource02Wrapper.setPrimaryKey(conf.getString(JobConstant.KEY_PRIMARY_KEY));
      return jdbcSource02Wrapper;
    } catch (Exception ex) {
      logger.error("Exception occurred when decoding JdbcSource02Wrapper: {}, error message: {}",
          conf, ex.toString());
      throw new DecodeSourceException(conf.getString(JobConstant.KEY_NODE_ID),
          conf.toJSONString());
    }
  }

  protected abstract JdbcSource02Wrapper decodeJdbcSourceParams(JSONObject conf);

}
