/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.wrapper.source;

import static com.rootcloud.analysis.core.constants.CommonConstant.DOLLAR;
import static com.rootcloud.analysis.core.constants.JobConstant.TD_ENGINE_TIME_COLUMN_NAME;
import static com.rootcloud.analysis.core.constants.SchemaConstant.BACK_QUOTE;
import static com.rootcloud.analysis.core.constants.SchemaConstant.DEVICE_ID;
import static com.rootcloud.analysis.core.constants.SchemaConstant.DOT;
import static com.rootcloud.analysis.core.constants.SchemaConstant.SINGLE_QUOTE;

import com.alibaba.fastjson.JSONArray;
import com.google.common.collect.Lists;
import com.rootcloud.analysis.common.entity.DeviceMapping;
import com.rootcloud.analysis.common.entity.RcUnit;
import com.rootcloud.analysis.common.util.DateUtil;
import com.rootcloud.analysis.core.constants.CommonConstant;
import com.rootcloud.analysis.core.constants.SchemaConstant;
import com.rootcloud.analysis.entity.TdEngineSchema;
import com.rootcloud.analysis.entity.TdEngineSchemaField;
import com.rootcloud.analysis.exception.SourceReadDataException;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import lombok.Getter;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * TDEngine输入节点-查询原始数据.
 *
 * @CreatedBy: zexin.huang
 * @Date: 2022/5/5 14:03
 */
@Getter
public class TdEngineConnWrapper extends TdEngineConnBaseWrapper {

  private final Logger logger = LoggerFactory.getLogger(TdEngineConnWrapper.class);

  private final int queryScopeSeconds;


  /**
   * TdEngineConnWrapper.
   */
  public TdEngineConnWrapper(String tenantId, List<String> urls,
                             String userName, String password,
                             String database, JSONArray schema, String timezone,
                             long timeLowerBound, long timeUpperBound,
                             Set<String> ignoredFieldNames,
                             List<DeviceMapping> deviceMappings,
                             int queryScopeSeconds) {
    super(false, userName, password, database, urls, schema, timezone, tenantId,
        timeLowerBound, timeUpperBound, ignoredFieldNames, deviceMappings);
    this.queryScopeSeconds = queryScopeSeconds;
  }

  /**
   * TdEngine查询原始数据的主要流程：
   * 获取查询SQL：
   * 1. 按设备模型ID模糊匹配拆分后的超级表集合；（show database.stables like 'xxx%')
   * 2. 找到超级表下所包含的设备ID集合；（select __deviceId__ from database.`xxx`）
   * 3. 轮询超级表集合，确定超级表与字段集合的映射关系；(DESCRIBE database.`xxx`)
   * 4. 使用字段集合+单个超级表名+单个设备ID，拼接查询SQL，到对应的超级表查询所包含的字段。
   * 通过SQL查询TdEngine时的多次拆分查询：
   * 1. 按设备模型ID拆分为多次查询，每次读单个表.
   * 2. 按同一个表的不同子表拆分为多次查询，每次取单个子表和该表包含的目标字段.
   * 3. 按设备ID拆分为多次查询，每次取单个设备的数据.
   * 4. 按时间拆分为多次查询，每次取某段时间区间内的数据.
   * 最后，自底向上，逐层汇总整合结果，按Schema转换流向下个节点.
   */
  @Override
  public Set<RcUnit> getData() throws Exception {
    Set<RcUnit> outPutSet = new HashSet<>();
    if (CollectionUtils.isEmpty(getDeviceMappings())) {
      return outPutSet;
    }
    try {
      // 1. 按设备模型ID拆分为多次查询，每次读单个表.
      for (DeviceMapping mapping : getDeviceMappings()) {
        String abstractDeviceTypeId = mapping.getAbstractDeviceTypeId();
        String deviceTypeId = mapping.getDeviceTypeId();
        List<String> deviceIds = mapping.getDeviceIds();
        // 2. 按同一个表的不同子表拆分为多次查询，每次取单个子表和该表包含的目标字段.
        String tableNameDefault = String.format("type_%s_%s", getTenantId(), deviceTypeId);

        // 获取拼接查询SQL所需的Schema信息.
        List<TdEngineSchema> schemaList = super.getSchemaList(tableNameDefault, deviceIds);
        logger.info("TdEngine tableNameDefault = {}, deviceIds = {}, schemaList = {}",
            tableNameDefault, deviceIds, schemaList);

        // 不同子表查询后的汇总结果.
        final List<Map<String, Object>> deviceTotalResult = Lists.newLinkedList();
        for (TdEngineSchema schemaInfo : schemaList) {
          // 3. 按设备ID拆分为多次查询，每次取单个设备的数据.
          for (String deviceId : schemaInfo.getDeviceIds()) {
            // 4. 按时间拆分为多次查询，每次取某段时间区间内的数据.
            long startOffset = getTimeLowerBound();
            while (startOffset < getTimeUpperBound()) {
              long endOffset = startOffset + queryScopeSeconds * 1000;
              // 拼接查询SQL.
              String querySql = this.buildQuerySql(
                  schemaInfo.getStabeName(), schemaInfo.getFields(),
                  startOffset, endOffset, deviceId);
              // 查询数据并汇总结果.
              super.queryAndCollectData(querySql, deviceTotalResult,
                  abstractDeviceTypeId, deviceTypeId, deviceId);
              // 更新下一轮查询时间区间的开始位置.
              startOffset = Math.min(endOffset, getTimeUpperBound());
            }
          }
        }
        // 5. 将汇总后的结果，按Schema转换
        Set<RcUnit> rcUnitSet = super.parseData2RcUnit(deviceTotalResult);
        if (CollectionUtils.isNotEmpty(rcUnitSet)) {
          outPutSet.addAll(rcUnitSet);
        }
      }
    } catch (Exception ex) {
      logger.error("getData Exception : ", ex);
      throw new SourceReadDataException(
          "TdEngine getData from " + getDatabase() + " failed：" + ex
              + "，deviceMappings：" + getDeviceMappings()
              + "，timeLowerBound：" + getTimeLowerBound()
              + "，timeUpperBound：" + getTimeUpperBound());
    }
    return outPutSet;
  }

  /**
   * 拼接查询SQL.
   * @param schemaFields 需要查询的字段名集合.
   */
  protected String buildQuerySql(String tableName, List<TdEngineSchemaField> schemaFields,
                                 long startOffset, long endOffset, String deviceId) {
    if (schemaFields.isEmpty()) {
      return null;
    }
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("select ");
    schemaFields.forEach(schemaField -> {
      stringBuilder.append(SchemaConstant.BACK_QUOTE)
          .append(schemaField.getQueryColumnName());
      if (StringUtils.isNotBlank(schemaField.getHistorianType())) {
        stringBuilder.append(DOLLAR).append(schemaField.getHistorianType());
      }
      stringBuilder.append(SchemaConstant.BACK_QUOTE).append(" as ").append(SINGLE_QUOTE)
          .append(schemaField.getOutputColumnName()).append(SINGLE_QUOTE)
          .append(CommonConstant.COMMA);
    });
    stringBuilder.deleteCharAt(stringBuilder.length() - 1);
    stringBuilder.append(" from ").append(getDatabase()).append(DOT)
        .append(BACK_QUOTE).append(tableName).append(BACK_QUOTE).append(" where ")
        // 设备ID筛选.
        .append(BACK_QUOTE).append(DEVICE_ID).append(BACK_QUOTE)
        .append(" = '").append(deviceId).append("' and ");

    // 时间区间筛选.
    String startTime = DateUtil.formatIso8601LongToString(startOffset);
    String endTime = DateUtil.formatIso8601LongToString(endOffset);
    stringBuilder.append(BACK_QUOTE).append(TD_ENGINE_TIME_COLUMN_NAME).append(BACK_QUOTE)
        .append(" >= '").append(startTime).append("' and ")
        .append(BACK_QUOTE).append(TD_ENGINE_TIME_COLUMN_NAME).append(BACK_QUOTE)
        .append(" < '").append(endTime).append("'");
    return stringBuilder.toString();
  }
}
