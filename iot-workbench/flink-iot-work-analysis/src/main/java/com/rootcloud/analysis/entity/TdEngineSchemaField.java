/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.entity;

import com.rootcloud.analysis.core.enums.HistorianAggFunctionEnum;
import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * TdEngine相关的元数据信息，供查询拼接SQL使用.
 * @CreatedBy: zexin.huang
 * @Date: 2022/5/11 15:37
 */
@ToString
@Getter
@Setter
public class TdEngineSchemaField implements Serializable {


  /**
   * 查询字段名（全小写）.
   * 即：查询SQL中"AS"之前的字段名.
   */
  private String queryColumnName;

  /**
   * 输出字段名（区分大小写）.
   * 即：查询SQL中"AS"之后的字段名.
   */
  private String outputColumnName;

  /**
   * 聚合函数名.
   */
  private HistorianAggFunctionEnum aggregateFunction = HistorianAggFunctionEnum.IGNORED;

  /**
   * 字段对应的类型.
   */
  private String historianType;
}
