/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.actuator.builder.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.rootcloud.analysis.actuator.builder.IActuatorBuilder;

import com.rootcloud.analysis.actuator.impl.DefaultActuator;
import com.rootcloud.analysis.common.context.IotWorkRuntimeContext;

import com.rootcloud.analysis.context.IotWorkRuntimeDecodeContext;
import com.rootcloud.analysis.core.exec.Actuator;
import com.rootcloud.analysis.core.graph.IGraph;

import com.rootcloud.analysis.wrapper.IWrapper;
import com.rootcloud.analysis.wrapper.processor.AbstractProcessorWrapper;
import com.rootcloud.analysis.wrapper.sink.AbstractSinkWrapper;
import com.rootcloud.analysis.wrapper.source.AbstractSourceWrapper;
import java.util.List;

import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.table.api.EnvironmentSettings;
import org.apache.flink.table.api.StatementSet;
import org.apache.flink.table.api.bridge.java.StreamTableEnvironment;
import org.apache.flink.table.functions.UserDefinedFunction;

public class DefaultActuatorBuilder implements IActuatorBuilder {

  private DefaultActuator actuator = new DefaultActuator();

  @Override
  public Actuator build(String jobName, String jobType, IGraph graph,
                        StreamExecutionEnvironment env) throws Exception {

    EnvironmentSettings settings = EnvironmentSettings.newInstance().build();
    StreamTableEnvironment tableEnv = StreamTableEnvironment.create(env, settings);

    Configuration configuration = tableEnv.getConfig().getConfiguration();
    configuration.setString("table.local-time-zone", "Asia/Shanghai");
    configuration.setString("pipeline.name", jobName);
    JSONArray udfInfos = IotWorkRuntimeDecodeContext.getUdfInfos();
    if (udfInfos != null) {
      for (Object obj : udfInfos) {
        JSONObject udfInfo = (JSONObject) obj;
        // 将UDF函数注册为 SQL 临时方法
        tableEnv.createTemporaryFunction(udfInfo.getString("name"),
            (UserDefinedFunction) this.getClass().getClassLoader().loadClass(udfInfo.getString("def_class")).getConstructor().newInstance());
      }
    }
    IotWorkRuntimeContext context = new IotWorkRuntimeContext();
    context.putExecutionEnv(env);
    context.putTableEnvToCache(tableEnv);

    StatementSet statementSet = tableEnv.createStatementSet();
    context.putStatementSetToCache(statementSet);

    List<IWrapper> sources = graph.getSources();
    for (IWrapper wrapper : sources) {
      AbstractSourceWrapper inputWrapper = (AbstractSourceWrapper) wrapper;
      inputWrapper.registerInput(context);
    }

    List<IWrapper> processors = graph.getProcessors();
    if (processors != null && !processors.isEmpty()) {
      for (IWrapper wrapper : processors) {
        AbstractProcessorWrapper processWrapper = (AbstractProcessorWrapper) wrapper;
        processWrapper.registerProcess(context);
      }
    }

    List<IWrapper> sinks = graph.getSinks();
    for (IWrapper wrapper : sinks) {
      AbstractSinkWrapper outputWrapper = (AbstractSinkWrapper) wrapper;
      outputWrapper.registerOutput(context);
    }

    actuator.setStatementSet(statementSet);
    actuator.setJobName(jobName);
    actuator.setJobType(jobType);
    actuator.setEnv(env);

    return actuator;

  }
}
