/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.decoder.processor.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.rootcloud.analysis.context.IotWorkRuntimeDecodeContext;
import com.rootcloud.analysis.core.constants.JobConstant;
import com.rootcloud.analysis.core.constants.SchemaConstant;
import com.rootcloud.analysis.core.enums.ProcessTypeEnum;
import com.rootcloud.analysis.decoder.processor.AbstractProcessorDecoder;
import com.rootcloud.analysis.decoder.processor.ProcessorDecoder;
import com.rootcloud.analysis.exception.DecodeProcessorException;
import com.rootcloud.analysis.wrapper.processor.AbstractProcessorWrapper;
import com.rootcloud.analysis.wrapper.processor.SqlProcessorWrapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@ProcessorDecoder(processType = ProcessTypeEnum.SQL)
public class SqlProcessorDecoder extends AbstractProcessorDecoder {

    private static Logger logger = LoggerFactory.getLogger(
            SqlProcessorDecoder.class);

    @Override
    public AbstractProcessorWrapper decodeProcessParams(JSONObject conf) {
        try {
            SqlProcessorWrapper.Builder builder = new SqlProcessorWrapper.Builder();
            builder.setTableSchema(getTableSchema(conf)).setSchema(conf.getJSONArray(JobConstant.KEY_SCHEMA))
                    .setSql(buildColumnValueTruncatedSql(conf.getString(JobConstant.KEY_SQL), conf.getJSONArray(JobConstant.KEY_SCHEMA)))
                    .setMaxOutOfOrderness(conf.getInteger(JobConstant.KEY_MAX_OUT_OF_ORDERNESS) == null
                            ? JobConstant.DEFAULT_MAX_OUT_OF_ORDERNESS : conf.getInteger(JobConstant.KEY_MAX_OUT_OF_ORDERNESS))
                    .setNodeName(conf.getString(JobConstant.KEY_NODE_NAME));
            SqlProcessorWrapper result = builder.build();
            logger.debug("Decoding sql process: {}", result);
            return result;
        } catch (Exception ex) {
            logger.error("Exception occurred when decoding sql processor: {}", conf);
            throw new DecodeProcessorException();
        }
    }

    private JSONArray getTableSchema(JSONObject conf) {
        JSONObject priorNodeConf = IotWorkRuntimeDecodeContext.getNodeConf(conf.getString(JobConstant.KEY_PRIOR_NODE_ID));
        JSONArray priorNodeSchema = priorNodeConf.getJSONArray(JobConstant.KEY_SCHEMA);
        return priorNodeSchema == null ? getTableSchema(priorNodeConf) : priorNodeSchema;
    }

    private String buildColumnValueTruncatedSql(String inputSql, JSONArray outputSchema) {
        StringBuilder sb = new StringBuilder("select ");
        for (Object o : outputSchema) {
            JSONObject jsonObject = (JSONObject) o;
            String column = jsonObject.getString(SchemaConstant.ATTR_NAME);
            String type = jsonObject.getString(SchemaConstant.ATTR_DATA_TYPE);

            String escapedColumn = "`" + column + "`";
            if ("NUMBER".equals(type)) {
                Integer scale = jsonObject.getInteger(SchemaConstant.ATTR_SCALE);
                if (scale == null) {
                    scale = JobConstant.DEFAULT_SCALE;
                }
                sb.append("TRUNCATE(").append(escapedColumn).append(", ").append(scale).append(")").append(" as ").append(escapedColumn);
            } else {
                sb.append(escapedColumn).append(" as ").append(escapedColumn);
            }

            sb.append(",");
        }
        sb.deleteCharAt(sb.length() - 1);
        sb.append(" from (").append(inputSql).append(")");
        return sb.toString();
    }
}
