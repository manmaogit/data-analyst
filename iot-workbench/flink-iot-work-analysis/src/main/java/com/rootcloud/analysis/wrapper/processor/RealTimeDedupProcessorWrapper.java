/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.wrapper.processor;

import com.rootcloud.analysis.common.context.IotWorkRuntimeContext;
import com.rootcloud.analysis.common.entity.RcUnit;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.apache.commons.collections.MapUtils;
import org.apache.flink.api.common.functions.FilterFunction;
import org.apache.flink.api.common.functions.RichMapFunction;
import org.apache.flink.api.common.state.ValueState;
import org.apache.flink.api.common.state.ValueStateDescriptor;
import org.apache.flink.api.java.functions.KeySelector;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;

/**
 * 流计算-去重节点.
 *
 * @CreatedBy: zexin.huang
 * @Date: 2021/12/20 2:46 下午
 */
@Getter
@Setter
@ToString(callSuper = true)
@Builder
public class RealTimeDedupProcessorWrapper extends AbstractProcessorWrapper {

    /**
     * 聚合的主键字段.
     */
    private final String aggPrimaryKey;

    /**
     * 去重的唯一性属性字段集合.
     */
    private final List<String> keyFieldList;

    /**
     * 去重依据的内存历史数据数量上限.
     */
    private final int dedupMapMaxSize;

    @Override
    public void registerProcess(IotWorkRuntimeContext context) throws Exception {
        DataStream<RcUnit> priorDataStream = context.getObjectFromContainer(getPriorNodeId());
        SingleOutputStreamOperator<RcUnit> dedupStream =
                priorDataStream.keyBy(new KeySelector<RcUnit, String>() {
                            @Override
                            public String getKey(RcUnit value) throws Exception {
                                return (String) value.getAttr(aggPrimaryKey);
                            }
                        })
                        .map(new KeyDedupMapper())
                        .filter(
                                new FilterFunction<RcUnit>() {
                                    @Override
                                    public boolean filter(RcUnit value) throws Exception {
                                        return MapUtils.isNotEmpty(value.getData());
                                    }
                                })
                        .uid(getNodeId())
                        .setParallelism(getParallelism())
                        .name(getNodeName());
        context.putObjectToContainer(getNodeId(), dedupStream);
    }

    private class KeyDedupMapper extends RichMapFunction<RcUnit, RcUnit> {

        private ValueState<LinkedHashMap> keyDedupState;

        @Override
        public void open(Configuration parameters) throws Exception {
            keyDedupState = getRuntimeContext().getState(
                    new ValueStateDescriptor("REALTIME_DEDUP", LinkedHashMap.class));
        }

        @Override
        public RcUnit map(RcUnit rcUnit) throws Exception {
            LinkedHashMap<String, Boolean> value = keyDedupState.value();
            StringBuilder sb = new StringBuilder();
            keyFieldList.stream().forEach(x -> sb.append(rcUnit.getAttr(x)).append("##"));
            String keyContent = sb.toString();
            if (value != null && value.containsKey(keyContent)) {
                value.remove(keyContent);
                value.put(keyContent, true);
                return RcUnit.valueOf(Collections.emptyMap());
            } else {
                if (value == null) {
                    value = new LinkedHashMap<String, Boolean>();
                }
                value.put(keyContent, true);
                // 控制内存数据规模.
                if (value.size() > dedupMapMaxSize) {
                    String keyOfFirst = value.keySet().iterator().next();
                    value.remove(keyOfFirst);
                }
                keyDedupState.update(value);
                return rcUnit;
            }
        }
    }

}
