/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.actuator.builder;

import com.rootcloud.analysis.scanner.ClasspathPackageScanner;

import java.io.IOException;
import java.lang.reflect.Modifier;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ActuatorBuilderFactory {

  private static Logger logger = LoggerFactory.getLogger(ActuatorBuilderFactory.class);
  private static Map<String, IActuatorBuilder> builderMap = new HashMap<>();

  static {
    ClasspathPackageScanner scanner = new ClasspathPackageScanner(
        "com.rootcloud.analysis.actuator.builder.impl");
    try {
      List<String> classList = scanner.getFullyQualifiedClassNameList();

      for (String className : classList) {
        try {
          Class clazz = ActuatorBuilderFactory.class.getClassLoader()
              .loadClass(className.replace("com/rootcloud/analysis/actuator/builder/impl/", ""));
          if (Modifier.isAbstract(clazz.getModifiers()) || clazz.isInterface()) {
            continue;
          }
          Object obj = clazz.newInstance();
          if (!(obj instanceof IActuatorBuilder)) {
            continue;
          }
          IActuatorBuilder builder = (IActuatorBuilder) obj;
          ActuatorBuilder anno = builder.getClass().getAnnotation(ActuatorBuilder.class);
          if (anno == null) {
            continue;
          }
          builderMap.put(anno.jobType().name(),
              builder);
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException e) {
          logger.warn("Exception occurred when IActuatorBuilder class instance", e);
        }
      }
    } catch (IOException e) {
      logger.error("ActuatorBuilderFactory init failed", e);
    }
  }

  /**
   * getActuatorBuilder.
   */
  public static IActuatorBuilder getActuatorBuilder(String jobType) {
    String key = jobType;

    if (builderMap.containsKey(key)) {
      return builderMap.get(key);
    }

    logger.error("ActuatorBuilder not found: {}", key);
    return null;
  }

  public static void clean() {
    builderMap.clear();
  }

}
