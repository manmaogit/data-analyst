/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.wrapper.source;

import com.rootcloud.analysis.common.entity.RcUnit;
import com.rootcloud.analysis.core.constants.SchemaConstant;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.apache.commons.collections.MapUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Getter
@Setter
@ToString(callSuper = true)
public abstract class HiveSourceWrapper extends AbstractBatchSourceWrapper {

  private static Logger logger = LoggerFactory.getLogger(HiveSourceWrapper.class);

  public String catalogName;

  public String defaultDatabase;

  public String hiveConfDir;

  public String table;

  public long bizStartTime;

  public long bizEndTime;

  /**
   * 判断是否在业务开始-结束时间（原始时间）范围内.
   * 左闭右开.
   */
  public static boolean isInBizTimeScope(RcUnit value, long bizStartTime, long bizEndTime) {
    if (value != null && MapUtils.isNotEmpty(value.getData())) {
      Object timestampObj = value.getAttr(SchemaConstant.TIMESTAMP);
      if (timestampObj != null && bizStartTime > 0L && bizEndTime > 0L) {
        try {
          long timestamp = Long.valueOf(timestampObj.toString());
          return timestamp >= bizStartTime && timestamp < bizEndTime;
        } catch (Exception e) {
          logger.error("isInBizTimeScope:timestampObj={}, bizStartTime={}, bizEndTime={}",
              timestampObj, bizStartTime, bizEndTime, e);
        }
      }
    }
    return false;
  }
}