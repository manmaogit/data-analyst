/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.decoder.source.impl;

import static com.rootcloud.analysis.core.constants.JobConstant.KEY_ABSTRACT_DEVICE_TYPE_IDS;
import static com.rootcloud.analysis.core.constants.JobConstant.KEY_HUB_SERVICE_URL;
import static com.rootcloud.analysis.core.constants.JobConstant.KEY_TABLE;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.rootcloud.analysis.common.util.HubUtil;
import com.rootcloud.analysis.context.IotWorkRuntimeDecodeContext;
import com.rootcloud.analysis.core.constants.JobConstant;
import com.rootcloud.analysis.decoder.source.AbstractSourceDecoder;
import com.rootcloud.analysis.exception.DecodeSourceException;
import com.rootcloud.analysis.wrapper.source.AbstractSourceWrapper;
import com.rootcloud.analysis.wrapper.source.HiveSourceWrapper;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class HiveSourceDecoder extends AbstractSourceDecoder {

  private static Logger logger = LoggerFactory.getLogger(HiveSourceDecoder.class);

  @Override
  public AbstractSourceWrapper decodeSourceParams(JSONObject conf) {
    try {
      HiveSourceWrapper hiveSourceWrapper = decodeHiveSourceParams(conf);
      JSONObject params = conf.getJSONObject(JobConstant.KEY_PARAMETERS);
      hiveSourceWrapper.setCatalogName(params.getString(JobConstant.KEY_CATALOG_NAME));
      hiveSourceWrapper.setDefaultDatabase(params.getString(JobConstant.KEY_DEFAULT_DATABASE));
      hiveSourceWrapper.setHiveConfDir(params.getString(JobConstant.KEY_HIVE_CONF_DIR));
      hiveSourceWrapper.setTable(getTables(conf));
      hiveSourceWrapper.setBizStartTime(
                            IotWorkRuntimeDecodeContext.getTimeScope().getStartEpochSecond() * 1000);
      hiveSourceWrapper.setBizEndTime(
                            IotWorkRuntimeDecodeContext.getTimeScope().getEndEpochSecond() * 1000);
      return hiveSourceWrapper;
    } catch (Exception ex) {
      logger.error("Exception occurred when decoding hive source: {}, error message: {}", conf,
          ex.toString());
      throw new DecodeSourceException(conf.getString(JobConstant.KEY_NODE_ID),
          conf.toJSONString());
    }
  }

  protected abstract HiveSourceWrapper decodeHiveSourceParams(JSONObject conf);

  /**
   * Hive新工况是按模型分表的.
   */
  private String getTables(JSONObject conf) {
    JSONObject params = conf.getJSONObject(JobConstant.KEY_PARAMETERS);
    Set<String> dynamicTables = new HashSet<>();
    if (params.containsKey(KEY_TABLE)) {
      dynamicTables.addAll(
          Arrays.asList(StringUtils.split(params.getString(KEY_TABLE), ",")));
    }
    if (params.containsKey(KEY_ABSTRACT_DEVICE_TYPE_IDS)
        && conf.containsKey(KEY_HUB_SERVICE_URL)) {
      String hubServiceUrl = conf.getString(KEY_HUB_SERVICE_URL);
      String tenantId = conf.getString(JobConstant.TENANT_ID);
      JSONArray abstractDeviceTypeIds = JSONObject.parseArray(
          params.getString(KEY_ABSTRACT_DEVICE_TYPE_IDS));
      for (Object abstractId : abstractDeviceTypeIds) {
        List<String> result = HubUtil.getDeviceTypesAbstractDerive(
            hubServiceUrl, tenantId, (String) abstractId);
        if (result != null && !result.isEmpty()) {
          dynamicTables.addAll(result.stream()
              .map(i -> "tbl_ods_" + i).collect(Collectors.toSet()));
        }
      }
    }
    return StringUtils.join(dynamicTables, ',');
  }
}
