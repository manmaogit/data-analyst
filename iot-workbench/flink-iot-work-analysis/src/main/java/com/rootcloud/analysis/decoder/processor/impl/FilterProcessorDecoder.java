/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.decoder.processor.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.rootcloud.analysis.core.constants.FilterConstant;
import com.rootcloud.analysis.core.enums.LogicalOperatorEnum;
import com.rootcloud.analysis.core.enums.ProcessTypeEnum;
import com.rootcloud.analysis.decoder.function.filter.FilterDecoderFactory;
import com.rootcloud.analysis.decoder.processor.AbstractProcessorDecoder;
import com.rootcloud.analysis.decoder.processor.ProcessorDecoder;
import com.rootcloud.analysis.exception.DecodeProcessorException;
import com.rootcloud.analysis.wrapper.function.filter.AndFilterWrapper;
import com.rootcloud.analysis.wrapper.function.filter.CompositeFilterWrapper;
import com.rootcloud.analysis.wrapper.function.filter.OrFilterWrapper;
import com.rootcloud.analysis.wrapper.processor.AbstractProcessorWrapper;
import com.rootcloud.analysis.wrapper.processor.FilterProcessorWrapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author manmao
 */
@ProcessorDecoder(processType = ProcessTypeEnum.FILTER)
public class FilterProcessorDecoder extends AbstractProcessorDecoder {

    private static Logger logger = LoggerFactory.getLogger(FilterProcessorDecoder.class);

    @Override
    public AbstractProcessorWrapper decodeProcessParams(JSONObject conf) {

        if (!conf.containsKey(FilterConstant.CONTENTS) || !conf.containsKey(FilterConstant.LOGICAL_OPERATOR)) {
            throw new DecodeProcessorException();
        }

        try {
            CompositeFilterWrapper filter;
            if (conf.getString(FilterConstant.LOGICAL_OPERATOR).equals(LogicalOperatorEnum.AND.name())) {
                filter = new AndFilterWrapper();
            } else if (conf.getString(FilterConstant.LOGICAL_OPERATOR).equals(LogicalOperatorEnum.OR.name())) {
                filter = new OrFilterWrapper();
            } else {
                throw new DecodeProcessorException();
            }

            FilterProcessorWrapper.Builder builder = new FilterProcessorWrapper.Builder();
            builder.setFilter(filter);

            JSONArray content = conf.getJSONArray(FilterConstant.CONTENTS);
            for (int index = 0; index < content.size(); index++) {
                JSONObject object = content.getJSONObject(index);
                filter.addFilter(FilterDecoderFactory.getDecoder(object.getString(FilterConstant.FILTER_RULE)).decode(object));
            }
            return builder.build();
        } catch (Exception ex) {
            logger.error("Exception occurred when decoding filter processor: {}", conf);
            throw new DecodeProcessorException();
        }
    }

}
