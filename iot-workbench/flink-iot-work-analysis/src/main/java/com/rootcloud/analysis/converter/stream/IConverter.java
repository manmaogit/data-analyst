/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.converter.stream;

import java.io.Serializable;
import org.apache.flink.streaming.api.datastream.DataStream;

public interface IConverter<I, O> extends Serializable {
  DataStream<O> convert(DataStream<I> input) throws NoSuchMethodException, Exception;
}
