/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.wrapper.processor;

import com.alibaba.fastjson.JSONArray;
import com.rootcloud.analysis.common.context.IotWorkRuntimeContext;
import com.rootcloud.analysis.context.IotWorkRuntimeBuildContext;
import java.util.Map;
import lombok.Getter;
import lombok.ToString;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.table.api.Table;
import org.apache.flink.table.api.bridge.java.StreamTableEnvironment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Please use FlinkSqlBatchProcessorWrapper instead.
 */
@Deprecated
@Getter
@ToString(callSuper = true)
public class FlinkSqlProcessorWrapper extends AbstractProcessorWrapper {

  private static Logger logger = LoggerFactory.getLogger(FlinkSqlProcessorWrapper.class);

  private final String sql;

  private final JSONArray schema;

  private final JSONArray priorNodeIds;

  private FlinkSqlProcessorWrapper(Builder builder) {
    this.schema = builder.schema;
    this.sql = builder.sql;
    this.priorNodeIds = builder.priorNodeIds;
  }

  /**
   * Resets the sequence.
   */
  @Override
  public void resetSequence(Map<String, AbstractProcessorWrapper> processorWrapperMap) {
    priorNodeIds.forEach(priorNodeId -> {
      AbstractProcessorWrapper priorProcessorWrapper = processorWrapperMap.get(priorNodeId);
      if (priorProcessorWrapper != null) {
        priorProcessorWrapper
            .setSequence(Math.min(getSequence() - 1, priorProcessorWrapper.getSequence()));
        priorProcessorWrapper.resetSequence(processorWrapperMap);
      }
    });
  }

  @ToString
  public static class Builder {

    private JSONArray schema;

    private String sql;

    private JSONArray priorNodeIds;

    public Builder setSchema(JSONArray schema) {
      this.schema = schema;
      return this;
    }

    public Builder setSql(String sql) {
      this.sql = sql;
      return this;
    }

    public Builder setPriorNodeIds(JSONArray priorNodeIds) {
      this.priorNodeIds = priorNodeIds;
      return this;
    }

    public FlinkSqlProcessorWrapper build() {
      return new FlinkSqlProcessorWrapper(this);
    }

  }

  private String getTemporaryViewName() {
    return getNodeId();
  }

  @Override
  public void registerProcess(IotWorkRuntimeContext  context) throws Exception {
    StreamTableEnvironment tableEnv = context.getTableEnvFromCache();
    Configuration configuration = tableEnv.getConfig().getConfiguration();
    configuration.setInteger("table.exec.resource.default-parallelism",
            IotWorkRuntimeBuildContext.getSqlParallelism());
    // execute sql query and get sql result stream
    Table table = tableEnv.sqlQuery(sql);
    tableEnv.createTemporaryView(getTemporaryViewName(), table);
    IotWorkRuntimeBuildContext.putTemporaryViewName(getNodeId(), getTemporaryViewName());
    IotWorkRuntimeBuildContext.putTableObject(getNodeId(),table);
    logger.debug("successfully execute sql query: \"{}\"", sql);
  }

}
