/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.decoder.processor;

import com.rootcloud.analysis.scanner.ClasspathPackageScanner;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ProcessorDecoderFactory {

  private static Logger logger = LoggerFactory.getLogger(
      ProcessorDecoderFactory.class);
  private static Map<String, AbstractProcessorDecoder> decoderMap = new HashMap<>();

  static {
    ClasspathPackageScanner scanner = new ClasspathPackageScanner(
        "com.rootcloud.analysis.decoder.processor.impl");
    try {
      List<String> classList = scanner.getFullyQualifiedClassNameList();

      for (String clazz : classList) {
        AbstractProcessorDecoder decoder = (AbstractProcessorDecoder) ProcessorDecoderFactory.class
            .getClassLoader()
            .loadClass(clazz.replace("com/rootcloud/analysis/decoder/processor/impl/",
                "")).newInstance();
        ProcessorDecoder anno = decoder.getClass().getAnnotation(ProcessorDecoder.class);
        decoderMap.put(anno.processType().name(), decoder);
      }
    } catch (IOException | ClassNotFoundException | InstantiationException
        | IllegalAccessException e) {
      logger.error("Failed to init ProcessDecoderFactory", e);
    }
  }

  /**
   * getDecoder.
   */
  public static AbstractProcessorDecoder getDecoder(String processType) {
    if (decoderMap.containsKey(processType)) {
      return decoderMap.get(processType);
    }

    logger.error("Process decoder not found: {}", processType);
    return null;
  }

}
