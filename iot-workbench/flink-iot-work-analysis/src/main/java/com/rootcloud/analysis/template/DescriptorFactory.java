/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.template;

import com.rootcloud.analysis.scanner.ClasspathPackageScanner;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DescriptorFactory {
  private static Logger logger = LoggerFactory.getLogger(DescriptorFactory.class);
  private static Map<String, IDescriptor> contentMap = new HashMap<>();

  static {
    ClasspathPackageScanner scanner = new ClasspathPackageScanner(
        "com.rootcloud.analysis.template.impl");
    try {
      List<String> classList = scanner.getFullyQualifiedClassNameList();

      for (String clazz : classList) {
        IDescriptor template = (IDescriptor) DescriptorFactory.class
            .getClassLoader()
            .loadClass(clazz.replace("com/rootcloud/analysis/template/impl/",
                "")).newInstance();
        ServiceTemplate anno = template.getClass().getAnnotation(ServiceTemplate.class);
        contentMap.put(anno.template().name(), template);
      }
    } catch (IOException | ClassNotFoundException | InstantiationException
        | IllegalAccessException e) {
      logger.error("TemplateFactory init failed");
    }
  }

  /**
   * getDecoder.
   */
  public static IDescriptor getDiscriptor(String template) {
    if (contentMap.containsKey(template)) {
      return contentMap.get(template);
    }

    logger.error("TemplateFactory getDiscriptor template:{} no template", template);
    return null;
  }
}
