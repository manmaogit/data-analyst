/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.converter.extractor;

import com.rootcloud.analysis.common.util.DateUtil;
import com.rootcloud.analysis.context.IotWorkRuntimeBuildContext;

import java.lang.reflect.Field;
import java.util.Arrays;
import javax.annotation.Nullable;

import org.apache.flink.streaming.api.functions.AssignerWithPeriodicWatermarks;
import org.apache.flink.streaming.api.watermark.Watermark;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EventTimeExtractor<T> implements AssignerWithPeriodicWatermarks<T> {

  private final Logger logger = LoggerFactory.getLogger(EventTimeExtractor.class);
  private static final Long MAX_ORDERNESS = Long.valueOf(10000);
  private long currentMaxTimestamp;

  @Override
  public long extractTimestamp(T data, long previousElementTimestamp) {
    Class clazz = data.getClass();
    Field[] fields = clazz.getDeclaredFields();
    Field resultField = Arrays.stream(fields)
        .filter(field -> field.getName().equals(IotWorkRuntimeBuildContext.getTimeAttr().getKey()))
        .findFirst()
        .get();

    Object object = null;
    try {
      object = resultField.get(data);
    } catch (IllegalAccessException e) {
      logger.error("error msg:", e);
    }

    currentMaxTimestamp = Math.max(DateUtil.convertTimeToLongMs((String) object),
        previousElementTimestamp);
    return currentMaxTimestamp;
  }

  @Nullable
  @Override
  public Watermark getCurrentWatermark() {
    return new Watermark(currentMaxTimestamp - MAX_ORDERNESS);
  }
}
