/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.wrapper.processor;

import static com.rootcloud.analysis.core.constants.SchemaConstant.ATTR_DATA_TYPE;
import static com.rootcloud.analysis.core.constants.SchemaConstant.ATTR_NAME;
import static com.rootcloud.analysis.core.constants.SchemaConstant.ATTR_SCALE;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.rootcloud.analysis.common.context.IotWorkRuntimeContext;
import com.rootcloud.analysis.common.entity.RcUnit;
import com.rootcloud.analysis.common.factory.KafkaLogMiddlewareResourceFactory;
import com.rootcloud.analysis.context.IotWorkRuntimeBuildContext;
import com.rootcloud.analysis.context.IotWorkRuntimeDecodeContext;
import com.rootcloud.analysis.converter.extractor.RcUnitTimeExtractor;
import com.rootcloud.analysis.converter.stream.RcUnitToRowConverter;
import com.rootcloud.analysis.core.constants.GrayLogConstant;
import com.rootcloud.analysis.core.constants.JobConstant;
import com.rootcloud.analysis.core.constants.ShortMessageConstant;
import com.rootcloud.analysis.core.constants.WrapperConstant;
import com.rootcloud.analysis.core.enums.GrayLogResultEnum;
import com.rootcloud.analysis.core.enums.GraylogLogTypeEnum;
import com.rootcloud.analysis.core.graylog.LogVo;
import com.rootcloud.analysis.extent.SqlContext;
import com.rootcloud.analysis.log.loggingservice.LoggingServiceUtil;
import com.rootcloud.analysis.wrapper.WrapperBuilder;
import java.sql.Timestamp;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;
import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.ProcessFunction;
import org.apache.flink.streaming.connectors.kafka.internals.FlinkKafkaInternalProducer;
import org.apache.flink.table.api.EnvironmentSettings;
import org.apache.flink.table.api.Schema;
import org.apache.flink.table.api.Table;
import org.apache.flink.table.api.bridge.java.StreamTableEnvironment;
import org.apache.flink.types.Row;
import org.apache.flink.util.Collector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Getter
@ToString(callSuper = true)
public class SqlProcessorWrapper extends AbstractProcessorWrapper {

  private static Logger logger = LoggerFactory.getLogger(SqlProcessorWrapper.class);

  private final String sql;

  private final JSONArray tableSchema;

  private final JSONArray schema;

  private final Integer maxOutOfOrderness;

  private static final String ROW_TIME = "rowtime.rowtime";

  private static final String PROC_TIME = "proctime.proctime";

  private SqlProcessorWrapper(Builder builder) {
    super(builder.jobId, builder.jobName, builder.tenantId, builder.nodeId, builder.parallelism,
        builder.nodeName, builder.priorNodeId, builder.processType, builder.logType);
    this.tableSchema = builder.tableSchema;
    this.schema = builder.schema;
    this.sql = builder.sql;
    this.maxOutOfOrderness = builder.maxOutOfOrderness == null ? JobConstant.DEFAULT_MAX_OUT_OF_ORDERNESS
            : builder.maxOutOfOrderness;;
  }

  @ToString
  public static class Builder extends WrapperBuilder {

    private String nodeId;
    private String nodeName;
    private Integer parallelism;
    private String priorNodeId;
    private String processType;
    private JSONArray tableSchema;
    private JSONArray schema;
    private String sql;
    private Integer maxOutOfOrderness;

    public Builder setNodeId(String nodeId) {
      this.nodeId = nodeId;
      return this;
    }

    public Builder setParallelism(Integer parallelism) {
      this.parallelism = parallelism;
      return this;
    }

    public Builder setPriorNodeId(String priorNodeId) {
      this.priorNodeId = priorNodeId;
      return this;
    }

    public Builder setProcessType(String processType) {
      this.processType = processType;
      return this;
    }

    public Builder setTableSchema(JSONArray tableSchema) {
      this.tableSchema = tableSchema;
      return this;
    }

    public Builder setSchema(JSONArray schema) {
      this.schema = schema;
      return this;
    }

    public Builder setSql(String sql) {
      this.sql = sql;
      return this;
    }

    public Builder setNodeName(String nodeName) {
      this.nodeName = nodeName;
      return this;
    }

    public Builder setMaxOutOfOrderness(Integer maxOutOfOrderness) {
      this.maxOutOfOrderness = maxOutOfOrderness;
      return this;
    }

    public SqlProcessorWrapper build() {
      return new SqlProcessorWrapper(this);
    }
  }

  @Override
  public void registerProcess(IotWorkRuntimeContext context) throws Exception {
    // get execution environment
    StreamTableEnvironment tableEnv = context.getTableEnvFromCache();
    if (tableEnv == null) {
      StreamExecutionEnvironment env = context.getExecutionEnv();
      env.setParallelism(IotWorkRuntimeBuildContext.getSqlParallelism());
      EnvironmentSettings bsSettings = EnvironmentSettings.newInstance().inStreamingMode().build();
      tableEnv = StreamTableEnvironment.create(env, bsSettings);
      Configuration configuration = tableEnv.getConfig().getConfiguration();
      configuration.setString("table.local-time-zone", "Asia/Shanghai");
      context.putTableEnvToCache(tableEnv);
    }
    // convert prior data stream with assigned watermark to rowDataStream
    List<String> keys = new ArrayList<>();
    List<String> valueTypes = new ArrayList<>();
    List<Integer> valueTypeScales = new ArrayList<>();

    /* DataStream<RcUnit>  字段值构造一个表结构Schema */
    Schema.Builder schemaBuilder = Schema.newBuilder();
    for (Object o : tableSchema) {
      String attrName = ((JSONObject) o).getString(ATTR_NAME);
      String attrDataType = ((JSONObject) o).getString(ATTR_DATA_TYPE);
      Integer attrDataTypeScale = ((JSONObject) o).getInteger(ATTR_SCALE);
      keys.add(attrName);
      valueTypes.add(attrDataType);
      valueTypeScales.add(attrDataTypeScale);
      // 构造表结构
      schemaBuilder.column(attrName, attrDataType);
    }
    // 增加row time 和 proc time
    schemaBuilder.columnByMetadata(ROW_TIME, "TIMESTAMP_LTZ");
    schemaBuilder.columnByMetadata(PROC_TIME, "TIMESTAMP_LTZ");

    String tableName = getNodeId();
    RcUnitToRowConverter converter = new RcUnitToRowConverter(tableName, getJobId(), keys, valueTypes, valueTypeScales);

    // get prior data stream
    DataStream<RcUnit> priorDataStream = context.getObjectFromContainer(getPriorNodeId());

    // assign watermark 设置watermark
    DataStream<RcUnit> watermarkDataStream =
            priorDataStream.assignTimestampsAndWatermarks((WatermarkStrategy.<RcUnit>forBoundedOutOfOrderness(Duration.ofSeconds(maxOutOfOrderness))
                            .withTimestampAssigner(new RcUnitTimeExtractor(IotWorkRuntimeBuildContext.getTimeAttr(), getJobId(), getJobName(),
                                    getTenantId(), IotWorkRuntimeDecodeContext.getJobType(),
                                    IotWorkRuntimeDecodeContext.getLoggingServiceKafkaTopic(),
                                    IotWorkRuntimeDecodeContext.getLoggingServiceKafkaServers()))))
                    .setParallelism(IotWorkRuntimeBuildContext.getSqlParallelism())
                    .name(getNodeName() + WrapperConstant.SQL_NODE_START_SUFFIX);

    DataStream<Row> rowDataStream = converter.convert(watermarkDataStream);

    SqlContext sqlContext = new SqlContext();
    sqlContext.setNodeName(getNodeName());

    // register table 把前一个节点输出的数据注册为一个临时 table，table name 使用当前节点的nodeId
    // select * from {} where xxx sql中配置的表名为 {}
    tableEnv.createTemporaryView(tableName, rowDataStream, schemaBuilder.build());

    String flinkSql = sql.replaceAll("\\{\\}", tableName);
    // execute sql query and get sql result stream
    Table table = tableEnv.sqlQuery(flinkSql);

    List<String> fieldList = Lists.newArrayList(table.getResolvedSchema().getColumnNames());

    // SQL执行结果转换为DataStream
    DataStream<RcUnit> sqlStream = tableEnv.toChangelogStream(table)
            .process(new SqlFlatMap(getJobId(), getTenantId(), getJobName(), fieldList,
                    getSchema(), getLogType(), getKafkaLoggingServiceServers(),
                    getKafkaLoggingServiceTopics(), null))
            .uid(getNodeId()).name(getNodeName());

    // 放入节点下一个流
    context.putObjectToContainer(tableName, sqlStream);
  }

  @AllArgsConstructor
  private static class SqlFlatMap extends ProcessFunction<Row, RcUnit> {

    private String jobId;
    private String tenantId;
    private String jobName;

    private final List<String> fieldList;

    private final JSONArray schema;

    private final GraylogLogTypeEnum logType;
    private final String kafkaLoggingServiceServers;
    private final String kafkaLoggingServiceTopics;

    private FlinkKafkaInternalProducer flinkKafkaInternalProducer;

    @Override
    public void open(Configuration parameters) throws Exception {
      super.open(parameters);
      if (flinkKafkaInternalProducer == null && kafkaLoggingServiceServers != null) {
        flinkKafkaInternalProducer = KafkaLogMiddlewareResourceFactory.getProducer(
            kafkaLoggingServiceServers,
            this.toString());
      }
    }

    @Override
    public void close() throws Exception {
      super.close();
      KafkaLogMiddlewareResourceFactory.close(this.toString());
      flinkKafkaInternalProducer = null;
    }

    @Override
    public void processElement(Row value, ProcessFunction<Row, RcUnit>.Context ctx, Collector<RcUnit> out) {
      Map<String, Object> map = Maps.newHashMap();
      try {
        for (int i = 0; i < fieldList.size(); i++) {
          String key = fieldList.get(i);
          Object val = value.getField(i);
          if (val instanceof Timestamp) {
            ((Timestamp) val).setTime(((Timestamp) val).getTime() + TimeZone.getTimeZone("GMT+8").getOffset(System.currentTimeMillis()));
          } else if (val instanceof LocalDateTime) {
            val = Timestamp.valueOf((LocalDateTime) val);
            ((Timestamp) val).setTime(((Timestamp) val).getTime() + TimeZone.getTimeZone("GMT+8").getOffset(System.currentTimeMillis()));
          }
          map.put(key, val);
        }
        RcUnit result = RcUnit.valueOf(map, schema);
        logger.debug("Sql processor output: {}", result);
        if (!Thread.currentThread().isInterrupted()) {
          out.collect(result);
        }
      } catch (Exception ex) {
        logger.error("Exception occurred when processing sql result: {}", ex.toString());
        LoggingServiceUtil.alarm(LogVo.builder()
            .k8sServiceName(GrayLogConstant.K8S_SERVICE_NAME)
            .module(GrayLogConstant.MODULE_NAME)
            .userName(tenantId)
            .tenantId(tenantId)
            .operateObjectName(jobName)
            .operateObject(jobId)
            .shortMessage(String.format("Failure within Flink SQL node due to %s",ex.toString()))
            .operation(ShortMessageConstant.PROCESS_SQL_RESULT)
            .requestId(String.valueOf(System.currentTimeMillis()))
            .result(GrayLogResultEnum.FAIL.getValue())
            .logType(logType)
            .kafkaLoggingServiceTopic(kafkaLoggingServiceTopics)
            .kafkaLoggingServiceServers(kafkaLoggingServiceServers)
            .build(), flinkKafkaInternalProducer);
      }
    }

  }

}
