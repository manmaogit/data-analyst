/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.wrapper.function.jdbc;

import com.rootcloud.analysis.common.factory.KafkaLogMiddlewareResourceFactory;
import com.rootcloud.analysis.common.jdbc.ConnectionWrapper;
import com.rootcloud.analysis.wrapper.jdbc.JdbcConnWrapper;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.functions.source.SourceFunction;

public abstract class JdbcRichSourceFunction<T> extends JdbcRichFunction implements SourceFunction<T> {

  private static final long serialVersionUID = 1L;

  private volatile boolean isRunning = true;

  /**
   * 构造方法.
   */
  public JdbcRichSourceFunction(JdbcConnWrapper jdbcConnWrapper) {
    super(jdbcConnWrapper);
  }

  @Override
  public void open(Configuration parameters) throws Exception {
    super.open(parameters);


  }

  /**
   * 获取运行状态.
   */
  public boolean isRunning() {
    return isRunning;
  }

  @Override
  public void close() throws Exception {
    isRunning = false;
    super.close();
  }

  @Override
  public void openProducer(String server) {
    if (flinkKafkaInternalProducer == null && server != null) {
      flinkKafkaInternalProducer = KafkaLogMiddlewareResourceFactory.getProducer(server,
          this.toString());
    }
  }
}
