/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.wrapper.source;

import static com.rootcloud.analysis.core.constants.SchemaConstant.ATTR_ORIGINAL_NAME;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.rootcloud.analysis.common.context.IotWorkRuntimeContext;
import com.rootcloud.analysis.common.util.DateUtil;
import com.rootcloud.analysis.context.IotWorkRuntimeDecodeContext;
import com.rootcloud.analysis.core.constants.CommonConstant;
import com.rootcloud.analysis.core.constants.JobConstant;
import com.rootcloud.analysis.core.enums.TimeZoneTypeEnum;
import com.rootcloud.analysis.wrapper.WrapperBuilder;
import java.text.SimpleDateFormat;
import java.time.OffsetDateTime;

import java.util.Date;
import lombok.Getter;
import lombok.ToString;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Getter
@ToString(callSuper = true)
public class JdbcSourceDynamicWrapper extends AbstractBatchSourceWrapper {

  private final Logger logger =
      LoggerFactory.getLogger(JdbcSourceDynamicWrapper.class);

  private final String uri;
  private final String username;
  private final String password;
  private final String table;
  private final String sql;


  /**
   * 构造方法.
   */
  public JdbcSourceDynamicWrapper(JdbcSourceDynamicWrapper.Builder builder) {
    super(builder.jobId, builder.jobName, builder.tenantId, builder.nodeId, builder.parallelism,
        builder.nodeName, builder.sourceType, builder.schema, builder.logType);
    this.uri = builder.uri;
    this.username = builder.username;
    this.password = builder.password;
    this.table = builder.table;
    this.sql = builder.sql;
  }

  @Override
  public void registerInput(IotWorkRuntimeContext context) {
    // do nothing.
  }

  public static class Builder extends WrapperBuilder {

    private String nodeId;
    private String nodeName;
    private Integer parallelism;
    private String sourceType;
    private JSONArray schema;
    private String uri;
    private String username;
    private String password;
    private String table;
    private String sql;

    public JdbcSourceDynamicWrapper.Builder setSql(String sql) {
      this.sql = sql;
      return this;
    }

    public JdbcSourceDynamicWrapper.Builder setNodeId(String nodeId) {
      this.nodeId = nodeId;
      return this;
    }

    public JdbcSourceDynamicWrapper.Builder setNodeName(String nodeName) {
      this.nodeName = nodeName;
      return this;
    }

    public JdbcSourceDynamicWrapper.Builder setParallelism(Integer parallelism) {
      this.parallelism = parallelism;
      return this;
    }

    public JdbcSourceDynamicWrapper.Builder setSourceType(String sourceType) {
      this.sourceType = sourceType;
      return this;
    }

    public JdbcSourceDynamicWrapper.Builder setSchema(JSONArray schema) {
      this.schema = schema;
      return this;
    }

    public JdbcSourceDynamicWrapper.Builder setUri(String uri) {
      this.uri = uri;
      return this;
    }

    public JdbcSourceDynamicWrapper.Builder setUsername(String username) {
      this.username = username;
      return this;
    }

    public JdbcSourceDynamicWrapper.Builder setPassword(String password) {
      this.password = password;
      return this;
    }

    public JdbcSourceDynamicWrapper.Builder setTable(String table) {
      this.table = table;
      return this;
    }

    public JdbcSourceDynamicWrapper build() {
      return new JdbcSourceDynamicWrapper(this);
    }
  }

  /**
   * 拼接查询总数sql.
   */
  public String buildCountSql(String keyQuote, String valueQuote) {
    StringBuilder countSql = new StringBuilder();
    countSql.append("select count(1) from ").append(keyQuote)
        .append(getTable()).append(keyQuote).append(" ");
    String whereSql = getFormattedWhereSql(valueQuote);
    if (StringUtils.isNotBlank(whereSql)) {
      countSql.append(whereSql);
    }
    return countSql.toString();
  }

  /**
   * 拼接查询sql.
   */
  public String buildSelectSql(String keyQuote, String valueQuote) {
    StringBuilder selectSql = new StringBuilder();
    selectSql.append("select ");
    if (getSchema() != null && !getSchema().isEmpty()) {
      for (int i = 0; i < getSchema().size(); i++) {
        if (i > 0) {
          selectSql.append(CommonConstant.COMMA);
        }
        selectSql.append(keyQuote);
        selectSql.append(((JSONObject) getSchema().get(i)).getString(ATTR_ORIGINAL_NAME));
        selectSql.append(keyQuote);
      }
    } else {
      selectSql.append("*");
    }
    selectSql.append(" from ")
        .append(keyQuote).append(getTable()).append(keyQuote).append(" ");

    String whereSql = getFormattedWhereSql(valueQuote);
    selectSql.append(whereSql);

    return selectSql.toString();
  }

  /**
   * 适用于通用的SQL格式化操作.
   */
  protected String getFormattedWhereSql(String valueQuote) {
    String whereSql = getSql();
    if (StringUtils.isBlank(whereSql)) {
      return "";
    }
    if (StringUtils.isNotBlank(whereSql)) {
      // 封装系统变量值.
      if (whereSql.contains(JobConstant.SYSTEM_ZONE_ID)
          && StringUtils.isNotBlank(IotWorkRuntimeDecodeContext.getTimezone())) {
        // 时区ID.
        whereSql = whereSql.replaceAll(JobConstant.SYSTEM_ZONE_ID, valueQuote
            + TimeZoneTypeEnum.valueOf(IotWorkRuntimeDecodeContext.getTimezone()).getTimeZoneValue()
            + valueQuote);
      }
      if (whereSql.contains(JobConstant.SYSTEM_TIME_ZONE)
          && StringUtils.isNotBlank(IotWorkRuntimeDecodeContext.getTimezone())) {
        // 时区.
        whereSql = whereSql.replaceAll(JobConstant.SYSTEM_TIME_ZONE, valueQuote
            + IotWorkRuntimeDecodeContext.getTimezone()
            + valueQuote);
      }

      if (whereSql.contains(JobConstant.SYS_DATE_FUNC_KEY)) {
        // 时区.
        whereSql = whereSql.replaceAll(JobConstant.SYS_DATE_FUNC_KEY_REG,
            valueQuote + new SimpleDateFormat("yyyy-MM-dd").format(new Date()) + valueQuote);
      }
      if (whereSql.contains(JobConstant.SYSTEM_BIZ_START_DATETIME_FUNC_KEY)
          && IotWorkRuntimeDecodeContext.getTimeScope() != null
          && StringUtils.isNotBlank(IotWorkRuntimeDecodeContext.getTimeScope().getStartTime())) {
        // 业务开始时间.
        whereSql = whereSql.replaceAll(JobConstant.SYSTEM_BIZ_START_DATETIME_FUNC_KEY_REG,
            valueQuote
            + IotWorkRuntimeDecodeContext.getTimeScope().getStartTime() + valueQuote);
      }
      if (whereSql.contains(JobConstant.SYSTEM_BIZ_END_DATETIME_FUNC_KEY)
          && IotWorkRuntimeDecodeContext.getTimeScope() != null
          && StringUtils.isNotBlank(IotWorkRuntimeDecodeContext.getTimeScope().getEndTime())) {
        // 业务结束时间.
        whereSql = whereSql.replaceAll(JobConstant.SYSTEM_BIZ_END_DATETIME_FUNC_KEY_REG,
            valueQuote
            + IotWorkRuntimeDecodeContext.getTimeScope().getEndTime() + valueQuote);
      }
      if (whereSql.contains(JobConstant.SYSTEM_DATETIME_FUNC_KEY)
          && StringUtils.isNotBlank(IotWorkRuntimeDecodeContext.getSystemDatetime())
          && StringUtils.isNotBlank(IotWorkRuntimeDecodeContext.getTimezone())) {
        // 计划调度时间.
        whereSql = whereSql.replaceAll(JobConstant.SYSTEM_DATETIME_FUNC_KEY_REG,
            valueQuote + DateUtil
            .formatTime(DateUtil.getEpochSecond(IotWorkRuntimeDecodeContext.getSystemDatetime(),
                    "yyyyMMddHHmmss", OffsetDateTime.now().getOffset().getId()) * 1000,
                TimeZoneTypeEnum.valueOf(IotWorkRuntimeDecodeContext.getTimezone()).getZoneId(),
                "yyyy-MM-dd HH:mm:ss") + valueQuote);
      }
      if (whereSql.contains(JobConstant.SYS_DATE)) {
        // 时区.
        whereSql = whereSql.replaceAll(JobConstant.SYS_DATE,
            valueQuote + new SimpleDateFormat("yyyy-MM-dd").format(new Date())
                + valueQuote);
      }

      if (whereSql.contains(JobConstant.SYSTEM_BIZ_START_DATETIME)
          && IotWorkRuntimeDecodeContext.getTimeScope() != null
          && StringUtils.isNotBlank(IotWorkRuntimeDecodeContext.getTimeScope().getStartTime())) {
        // 业务开始时间.
        whereSql = whereSql.replaceAll(JobConstant.SYSTEM_BIZ_START_DATETIME, valueQuote
            + IotWorkRuntimeDecodeContext.getTimeScope().getStartTime() + valueQuote);
      }
      if (whereSql.contains(JobConstant.SYSTEM_BIZ_END_DATETIME)
          && IotWorkRuntimeDecodeContext.getTimeScope() != null
          && StringUtils.isNotBlank(IotWorkRuntimeDecodeContext.getTimeScope().getEndTime())) {
        // 业务结束时间.
        whereSql = whereSql.replaceAll(JobConstant.SYSTEM_BIZ_END_DATETIME, valueQuote
            + IotWorkRuntimeDecodeContext.getTimeScope().getEndTime() + valueQuote);
      }
      if (whereSql.contains(JobConstant.SYSTEM_DATETIME)
          && StringUtils.isNotBlank(IotWorkRuntimeDecodeContext.getSystemDatetime())
          && StringUtils.isNotBlank(IotWorkRuntimeDecodeContext.getTimezone())) {
        // 计划调度时间.
        whereSql = whereSql.replaceAll(JobConstant.SYSTEM_DATETIME, valueQuote + DateUtil
            .formatTime(DateUtil.getEpochSecond(IotWorkRuntimeDecodeContext.getSystemDatetime(),
                    "yyyyMMddHHmmss", OffsetDateTime.now().getOffset().getId()) * 1000,
                TimeZoneTypeEnum.valueOf(IotWorkRuntimeDecodeContext.getTimezone()).getZoneId(),
                "yyyy-MM-dd HH:mm:ss") + valueQuote);
      }




      // 去掉结尾的分号
      whereSql = whereSql.replaceAll(";", "");
    }
    return whereSql;
  }


}
