/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.template;

import java.io.Serializable;
import java.util.Map;

public interface IDescriptor extends Serializable {

  Map<Object, Map<String, Object>> parse(String content, String keyAttr);

}
