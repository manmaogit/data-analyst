/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.decoder.job.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.rootcloud.analysis.common.context.IotWorkRuntimeContext;
import com.rootcloud.analysis.context.IotWorkRuntimeBuildContext;
import com.rootcloud.analysis.context.IotWorkRuntimeDecodeContext;
import com.rootcloud.analysis.core.constants.JobConstant;
import com.rootcloud.analysis.core.enums.ProcessTypeEnum;
import com.rootcloud.analysis.core.graph.DefaultJobGraph;
import com.rootcloud.analysis.core.graph.IGraph;
import com.rootcloud.analysis.decoder.job.AbstractJobDecoder;
import com.rootcloud.analysis.decoder.processor.AbstractProcessorDecoder;
import com.rootcloud.analysis.decoder.processor.ProcessorDecoderFactory;
import com.rootcloud.analysis.decoder.sink.AbstractSinkDecoder;
import com.rootcloud.analysis.decoder.sink.SinkDecoderFactory;
import com.rootcloud.analysis.decoder.source.AbstractSourceDecoder;
import com.rootcloud.analysis.decoder.source.SourceDecoderFactory;
import com.rootcloud.analysis.wrapper.IWrapper;
import com.rootcloud.analysis.wrapper.processor.AbstractProcessorWrapper;
import com.rootcloud.analysis.wrapper.processor.FlinkSqlProcessorWrapper;
import com.rootcloud.analysis.wrapper.sink.AbstractSinkWrapper;
import com.rootcloud.analysis.wrapper.source.AbstractSourceWrapper;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import org.apache.commons.compress.utils.Lists;

public class DefaultJobDecoder extends AbstractJobDecoder {

  /**
   * Decodes sources.
   */
  private void decodeSources(JSONArray sources, DefaultJobGraph.Builder builder) {
    List<AbstractSourceWrapper> sourceWrapperList = Lists.newArrayList();
    Set<String> nodeIds = new HashSet<>();
    for (int index = 0; index < sources.size(); index++) {
      JSONObject conf = (JSONObject) sources.get(index);

      String nodeId = conf.getString(JobConstant.KEY_NODE_ID);
      if (!nodeIds.contains(nodeId)) {

        AbstractSourceDecoder decoder = SourceDecoderFactory
            .getDecoder(conf.getString(JobConstant.KEY_SOURCE_TYPE));

        AbstractSourceWrapper inputWrapper = (AbstractSourceWrapper) decoder.decode(conf);
        sourceWrapperList.add(inputWrapper);

        nodeIds.add(nodeId);
      }
    }
    builder.setSources(sourceWrapperList);
  }

  /**
   * Decodes process nodes.
   */
  private void decodeProcessors(JSONArray processors, DefaultJobGraph.Builder builder) {
    if (processors == null || processors.isEmpty()) {
      return;
    }
    List<AbstractProcessorWrapper> processorWrapperList = Lists.newArrayList();
    for (int index = 0; index < processors.size(); index++) {
      JSONObject conf = (JSONObject) processors.get(index);

      AbstractProcessorDecoder decoder = ProcessorDecoderFactory
          .getDecoder(conf.getString(JobConstant.KEY_PROCESSOR_TYPE));

      AbstractProcessorWrapper wrapper = (AbstractProcessorWrapper) decoder.decode(conf);
      processorWrapperList.add(wrapper);
    }

    // Sorts processor nodes
    Map<String, AbstractProcessorWrapper> processorWrapperMap = processorWrapperList.stream()
        .collect(Collectors.toMap(AbstractProcessorWrapper::getNodeId, Function.identity()));

    Set<String> priorNodeIdSet = new HashSet<>();
    processorWrapperList.forEach(processorWrapper -> {
      if (processorWrapper instanceof FlinkSqlProcessorWrapper) {
        priorNodeIdSet.addAll(((FlinkSqlProcessorWrapper) processorWrapper).getPriorNodeIds()
            .toJavaList(String.class));
      } else {
        priorNodeIdSet.add(processorWrapper.getPriorNodeId());
      }
    });

    List<AbstractProcessorWrapper> maxSequenceProcessorNodeLst = processorWrapperList.stream()
        .filter(processWrapper -> !priorNodeIdSet.contains(processWrapper.getNodeId()))
        .collect(Collectors.toList());

    for (AbstractProcessorWrapper maxSequenceProcessWrapper : maxSequenceProcessorNodeLst) {
      maxSequenceProcessWrapper.resetSequence(processorWrapperMap);
    }

    // 处理节点的下节点为空则删除该冗余节点
    processorWrapperList = processorWrapperList.stream().filter(abstractProcessorWrapper ->
        IotWorkRuntimeBuildContext.getNextNodesFromCache(abstractProcessorWrapper.getNodeId()) != null)
        .collect(Collectors.toList());

    // Caches parallelism for sql processors
    List<AbstractProcessorWrapper> flinkSqlProcessorWrapperLst = processorWrapperList.stream()
        .filter(processWrapper -> processWrapper.getProcessorType()
            .equals(ProcessTypeEnum.FLINK_SQL.name())).collect(Collectors.toList());
    if (!flinkSqlProcessorWrapperLst.isEmpty()) {
      IotWorkRuntimeBuildContext.setSqlParallelism(flinkSqlProcessorWrapperLst.stream()
          .max(Comparator.comparing(sqlProcessWrapper -> sqlProcessWrapper
              .getParallelism())).get().getParallelism());
    }

    List<AbstractProcessorWrapper> flinkSqlBatchProcessorWrapperLst = processorWrapperList.stream()
        .filter(processWrapper -> processWrapper.getProcessorType()
            .equals(ProcessTypeEnum.FLINK_SQL_BATCH.name()) || processWrapper.getProcessorType()
            .equals(ProcessTypeEnum.UDT_AGG.name())).collect(Collectors.toList());
    if (!flinkSqlBatchProcessorWrapperLst.isEmpty()) {
      IotWorkRuntimeBuildContext.setSqlParallelism(flinkSqlBatchProcessorWrapperLst.stream()
          .max(Comparator.comparing(sqlProcessWrapper -> sqlProcessWrapper
              .getParallelism())).get().getParallelism());
    }

    builder.setProcessors(processorWrapperList.stream()
        .sorted(Comparator.comparing(processWrapper -> processWrapper.getSequence()))
        .collect(Collectors.toList()));
  }

  /**
   * Decodes sinks.
   */
  private void decodeSinks(JSONArray sinks, DefaultJobGraph.Builder builder) {
    List<AbstractSinkWrapper> sinkWrapperList = Lists.newArrayList();
    for (int index = 0; index < sinks.size(); index++) {
      JSONObject conf = (JSONObject) sinks.get(index);

      AbstractSinkDecoder decoder = SinkDecoderFactory.getDecoder(
          conf.getString(JobConstant.KEY_SINK_TYPE));
      AbstractSinkWrapper wrapper = (AbstractSinkWrapper) decoder.decode(conf);

      sinkWrapperList.add(wrapper);
    }
    builder.setSinks(sinkWrapperList);
  }


  @Override
  public IGraph decode(JSONObject conf) {
    JSONArray sources = conf.getJSONArray(JobConstant.KEY_SOURCES);
    for (Object nodeConf : sources) {
      IotWorkRuntimeDecodeContext.addNodeConf(((JSONObject) nodeConf).getString(JobConstant.KEY_NODE_ID),
          (JSONObject) nodeConf);
    }

    JSONArray processors = conf.getJSONArray(JobConstant.KEY_PROCESSORS);
    if (processors != null && !processors.isEmpty()) {
      for (Object nodeConf : processors) {
        JSONObject jsonNodeConf = (JSONObject) nodeConf;
        IotWorkRuntimeDecodeContext.addNodeConf(jsonNodeConf.getString(JobConstant.KEY_NODE_ID), jsonNodeConf);

        String processType = jsonNodeConf.getString(JobConstant.KEY_PROCESSOR_TYPE);
        if (ProcessTypeEnum.FLINK_SQL.name().equals(processType)
            || ProcessTypeEnum.FLINK_SQL_BATCH.name().equals(processType)) {
          jsonNodeConf.getJSONArray(JobConstant.KEY_PRIOR_NODE_IDS)
              .forEach(priorNodeId -> IotWorkRuntimeContext
                  .putNextNodesToCache((String) priorNodeId, jsonNodeConf));
        } else {
          IotWorkRuntimeContext.putNextNodesToCache(jsonNodeConf
              .getString(JobConstant.KEY_PRIOR_NODE_ID), jsonNodeConf);
        }
      }
    }
    JSONArray sinks = conf.getJSONArray(JobConstant.KEY_SINKS);
    for (Object nodeConf : sinks) {
      JSONObject jsonNodeConf = (JSONObject) nodeConf;
      IotWorkRuntimeDecodeContext.addNodeConf(jsonNodeConf.getString(JobConstant.KEY_NODE_ID), jsonNodeConf);
      IotWorkRuntimeContext.putNextNodesToCache(jsonNodeConf
          .getString(JobConstant.KEY_PRIOR_NODE_ID), jsonNodeConf);
    }
    DefaultJobGraph.Builder builder = new DefaultJobGraph.Builder<IWrapper>();
    decodeSources(sources, builder);
    decodeProcessors(processors, builder);
    decodeSinks(sinks, builder);
    return builder.build();
  }
}
