/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.decoder.source.impl;

import com.alibaba.fastjson.JSONObject;
import com.rootcloud.analysis.context.IotWorkRuntimeBuildContext;
import com.rootcloud.analysis.context.IotWorkRuntimeDecodeContext;
import com.rootcloud.analysis.core.constants.JobConstant;
import com.rootcloud.analysis.core.enums.DataSourceTypeEnum;
import com.rootcloud.analysis.decoder.source.AbstractSourceDecoder;
import com.rootcloud.analysis.decoder.source.SourceDecoder;
import com.rootcloud.analysis.entity.RcTimeAttr;
import com.rootcloud.analysis.exception.DecodeSourceException;
import com.rootcloud.analysis.wrapper.source.AbstractSourceWrapper;
import com.rootcloud.analysis.wrapper.source.KafkaSourceWrapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SourceDecoder(dataSourceType = DataSourceTypeEnum.KAFKA)
public class KafkaSourceDecoder extends AbstractSourceDecoder {

  private static Logger logger = LoggerFactory.getLogger(KafkaSourceDecoder.class);

  @Override
  public AbstractSourceWrapper decodeSourceParams(JSONObject conf) {

    try {
      JSONObject params = (JSONObject) conf.get(JobConstant.KEY_PARAMETERS);
      KafkaSourceWrapper.Builder builder = new KafkaSourceWrapper.Builder();
      RcTimeAttr timeAttr = RcTimeAttr.valueOf(conf.getString(JobConstant.KEY_TIME_ATTR));
      builder.setBrokers(params.getString(JobConstant.KEY_BROKER_LIST))
          .setCustomConsumerGroupId(params.getString(JobConstant.KEY_CUSTOM_CONSUMER_GROUP_ID))
          .setTimeAttr(timeAttr)
          .setTopics(params.getJSONArray(JobConstant.KEY_TOPICS).toJavaList(String.class))
          .setGroupId(IotWorkRuntimeDecodeContext.getJobId())
          .setOffsetStrategy(params.getString(JobConstant.KEY_OFFSET_STRATEGY));
      IotWorkRuntimeBuildContext.setTimeAttr(timeAttr);
      KafkaSourceWrapper result = builder.build();
      logger.debug("Decoding kafka source: {}", result);
      return result;
    } catch (Exception ex) {
      logger.error("Exception occurred when decoding kafka source: {}", conf);
      throw new DecodeSourceException(conf.getString(JobConstant.KEY_NODE_ID),
          conf.toJSONString());
    }
  }
}
