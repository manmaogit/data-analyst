/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.scanner;

import com.rootcloud.analysis.common.util.StringUtil;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarInputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ClasspathPackageScanner implements PackageScanner {
  private Logger logger = LoggerFactory.getLogger(ClasspathPackageScanner.class);
  private String basePackage;
  private ClassLoader cl;

  /**
   * 初始化.
   */
  public ClasspathPackageScanner(String basePackage) {
    this.basePackage = basePackage;
    this.cl = getClass().getClassLoader();
  }

  public ClasspathPackageScanner(String basePackage, ClassLoader cl) {
    this.basePackage = basePackage;
    this.cl = cl;
  }

  /**
   * 获取指定包下的所有字节码文件的全类名.
   */
  @Override
  public List<String> getFullyQualifiedClassNameList() throws IOException {
    logger.info("Start scanning all classes under package {}", basePackage);
    return doScan(basePackage, new ArrayList<String>());
  }

  /**
   * doScan函数.
   */
  private List<String> doScan(String basePackage, List<String> nameList) throws IOException {
    String splashPath = StringUtil.dotToSplash(basePackage);

    //file:/D:/WorkSpace/java/ScanTest/target/classes/com/scan
    URL url = cl.getResource(splashPath);
    String filePath = StringUtil.getRootPath(url);

    // contains the name of the class file. e.g., Apple.class will be stored as "Apple"
    List<String> names = null;

    // 先判断是否是jar包，如果是jar包，通过JarInputStream产生的JarEntity去递归查询所有类
    if (isJarFile(filePath)) {
      if (logger.isDebugEnabled()) {
        logger.debug("{} Is a jar package", filePath);
      }
      names = readFromJarFile(filePath, splashPath);
    } else {
      if (logger.isDebugEnabled()) {
        logger.debug("{} Is a directory", filePath);
      }
      names = readFromDirectory(filePath);
    }
    for (String name : names) {
      if (isClassFile(name)) {
        nameList.add(toFullyQualifiedName(name, basePackage));
      } else {
        doScan(basePackage + "." + name, nameList);
      }
    }
    if (logger.isDebugEnabled()) {
      for (String n : nameList) {
        logger.debug("find {}", n);
      }
    }
    return nameList;
  }

  private String toFullyQualifiedName(String shortName, String basePackage) {
    StringBuilder sb = new StringBuilder(basePackage);
    sb.append('.');
    sb.append(StringUtil.trimExtension(shortName));

    return sb.toString();
  }

  private List<String> readFromJarFile(String jarPath,
                                       String splashedPackageName) throws IOException {
    if (logger.isDebugEnabled()) {
      logger.debug("从JAR包中读取类: {}", jarPath);
    }
    JarInputStream jarIn = new JarInputStream(new FileInputStream(jarPath));
    JarEntry entry = jarIn.getNextJarEntry();
    List<String> nameList = new ArrayList<String>();
    while (null != entry) {
      String name = entry.getName();
      if (name.startsWith(splashedPackageName) && isClassFile(name)) {
        nameList.add(name);
      }

      entry = jarIn.getNextJarEntry();
    }

    return nameList;
  }

  private List<String> readFromDirectory(String path) {
    File file = new File(path);
    String[] names = file.list();

    if (null == names) {
      return null;
    }

    return Arrays.asList(names);
  }

  private boolean isClassFile(String name) {
    return name.endsWith(".class");
  }

  private boolean isJarFile(String name) {
    return name.endsWith(".jar");
  }

}
