/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.common.util;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.apache.http.HttpEntity;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Hub接口调用工具类.
 *
 * @CreatedBy: zexin.huang
 * @Date: 2021/11/22 5:41 下午
 */
public class HubUtil {

  private static final String HUB_GET_DEVICE_TYPES_ABSTRACT_DERIVE_URL =
      "%s/device/types/abstract/derive?tenantId=%s&status=active&refId=%s&_includeMetadata=true";

  private static Logger logger = LoggerFactory.getLogger(HubUtil.class);

  /**
   * 调用Hub接口根据条件查询抽象物模型衍生模型.
   */
  public static List<String> getDeviceTypesAbstractDerive(String hubServiceUrl, String tenantId,
                                                          String abstractTypeId) {
    List<String> result = new LinkedList<>();
    String url = null;
    try {
      url = String.format(HUB_GET_DEVICE_TYPES_ABSTRACT_DERIVE_URL,
          hubServiceUrl, tenantId, abstractTypeId);
      result.addAll(getDeviceTypeIdPaging(url));
    } catch (Exception e) {
      logger.error("调用HUB API异常, url=" + url, e);
    }
    return result;
  }

  /**
   * 调用Hub接口根据条件查询抽象物模型衍生模型-分页查询.
   */
  private static List<String> getDeviceTypeIdPaging(final String url) throws Exception {
    List<String> result = new LinkedList<>();
    int pageSize = 1000;
    int skipOffset = 0;
    CloseableHttpClient httpclient = null;
    CloseableHttpResponse response = null;
    try {
      httpclient = HttpClients.createDefault();
      while (skipOffset >= 0) {
        String newUrl = url + "&_limit=" + pageSize + "&_skip=" + skipOffset;
        response = invokeHttpGet(httpclient, newUrl);
        if (response != null
            && response.getStatusLine().getStatusCode() == 200) {
          HttpEntity resEntity = response.getEntity();
          JSONObject responseBody = JSON.parseObject(
              EntityUtils.toString(resEntity, "UTF-8"));
          Map<String, Object> metadata = responseBody.getObject("metadata", Map.class);
          int totalCount = (int) metadata.get("totalCount");
          skipOffset += pageSize;
          if (skipOffset > totalCount) {
            skipOffset = -1;
          }
          List<String> payload = responseBody.getObject("payload", List.class);
          if (payload != null) {
            result.addAll(payload);
          }
        }
      }
    } catch (Exception e) {
      throw e;
    } finally {
      if (response != null) {
        response.close();
      }
      if (httpclient != null) {
        httpclient.close();
      }
    }
    return result;
  }

  /**
   * 建立连接.
   */
  private static CloseableHttpResponse invokeHttpGet(CloseableHttpClient httpClient, String url)
      throws IOException {
    logger.info("HubUtil.invokeHttpGet query url = {}", url);
    HttpGet httpGet = new HttpGet(url);
    // 设置超时时长.
    int timeOutMilliSeconds = 300 * 1000;
    RequestConfig requestConfig = RequestConfig.custom()
        .setConnectTimeout(timeOutMilliSeconds)
        .setConnectionRequestTimeout(timeOutMilliSeconds)
        .setSocketTimeout(timeOutMilliSeconds)
        .build();
    httpGet.setConfig(requestConfig);
    return httpClient.execute(httpGet);
  }
}
