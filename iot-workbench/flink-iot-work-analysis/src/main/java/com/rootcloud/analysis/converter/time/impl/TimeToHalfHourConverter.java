/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2020 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.converter.time.impl;

import static com.rootcloud.analysis.core.enums.TimeDimensionEnum.HALF_HOUR;

import com.rootcloud.analysis.common.util.DateUtil;
import com.rootcloud.analysis.converter.time.ITimeConverter;
import com.rootcloud.analysis.converter.time.TimeConvert;

import java.time.ZoneId;
import java.util.Calendar;
import java.util.GregorianCalendar;

@TimeConvert(converterType = HALF_HOUR)
public class TimeToHalfHourConverter implements ITimeConverter {

  private static final Long ONE_HOUR_MS = 60 * 60 * 1000L;

  private static final Long HALF_HOUR_MS = 30 * 60 * 1000L;

  @Override
  public String convert(String time, ZoneId zoneId) {
    Long timeMs = DateUtil.convertTimeToLongMs(time);
    return timeToHalfHour(timeMs, zoneId);
  }

  @Override
  public String convert(long time, ZoneId zoneId) {
    return timeToHalfHour(time, zoneId);
  }

  @Override
  public String convert(long time, ZoneId zoneId, int startMonth, int startDay,
                        int startHour, int startMinute) {
    return timeToHalfHour(time, zoneId, startMonth, startDay, startHour, startMinute);
  }

  private String timeToHalfHour(long time, ZoneId zoneId) {
    String dateTimeFormatter;
    if (time % ONE_HOUR_MS >= HALF_HOUR_MS) {
      dateTimeFormatter = "yyyy-MM-dd HH:30:00";
    } else {
      dateTimeFormatter = "yyyy-MM-dd HH:00:00";
    }
    return DateUtil.formatTime(time, zoneId, dateTimeFormatter);
  }

  private String timeToHalfHour(long time, ZoneId zoneId, int startMonth, int startDay,
                                int startHour, int startMinute) {
    Calendar cal = new GregorianCalendar();
    cal.setTimeInMillis(time);
    DateUtil.minusTime(cal, startMonth - 1, startDay - 1, startHour, startMinute);
    String dateTimeFormatter;
    if (cal.getTimeInMillis() % ONE_HOUR_MS >= HALF_HOUR_MS) {
      dateTimeFormatter = "yyyy-MM-dd HH:30:00";
    } else {
      dateTimeFormatter = "yyyy-MM-dd HH:00:00";
    }
    return DateUtil.formatTime(cal.getTimeInMillis(), zoneId, dateTimeFormatter);
  }

}