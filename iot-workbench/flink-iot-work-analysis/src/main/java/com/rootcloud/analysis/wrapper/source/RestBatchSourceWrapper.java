/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.wrapper.source;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.rootcloud.analysis.common.context.IotWorkRuntimeContext;
import com.rootcloud.analysis.common.entity.RcUnit;
import com.rootcloud.analysis.common.factory.KafkaLogMiddlewareResourceFactory;
import com.rootcloud.analysis.core.constants.GrayLogConstant;
import com.rootcloud.analysis.core.constants.ShortMessageConstant;
import com.rootcloud.analysis.core.enums.GrayLogResultEnum;
import com.rootcloud.analysis.core.enums.MethodEnum;
import com.rootcloud.analysis.core.graylog.LogVo;
import com.rootcloud.analysis.log.loggingservice.LoggingServiceUtil;
import com.rootcloud.analysis.wrapper.WrapperBuilder;
import com.rootcloud.analysis.wrapper.api.RestConnWrapper;

import java.util.Set;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.source.RichSourceFunction;
import org.apache.flink.streaming.connectors.kafka.internals.FlinkKafkaInternalProducer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * IDF-设备台账-输入节点-离线任务.
 */
public class RestBatchSourceWrapper extends AbstractBatchSourceWrapper {

  private static Logger logger = LoggerFactory.getLogger(RestBatchSourceWrapper.class);
  private final JSONArray headers;
  private final String url;
  private final MethodEnum method;
  private final JSONArray parameters;
  private final JSONObject body;
  private final JSONArray restApiInputSchema;

  /**
   * HTTP超时时长（单位：秒）.
   */
  private int timeOutSeconds;

  /**
   * HTTP查询分页大小.
   */
  private int pageLimit;

  protected RestBatchSourceWrapper(Builder builder) {
    super(builder.jobId, builder.jobName, builder.tenantId, builder.nodeId, builder.parallelism,
        builder.nodeName, builder.inputType, builder.schema, builder.logType);
    this.headers = builder.headers;
    this.body = builder.body;
    this.url = builder.url;
    this.method = builder.method;
    this.parameters = builder.parameters;
    this.restApiInputSchema = builder.restApiInputSchema;
    this.timeOutSeconds = builder.timeOutSeconds;
    this.pageLimit = builder.pageLimit;
  }

  @Override
  public void registerInput(IotWorkRuntimeContext context) {
    StreamExecutionEnvironment env = context.getExecutionEnv();
    RestConnWrapper restConnWrapper = new RestConnWrapper(url, restApiInputSchema, method, headers,
        parameters, body, timeOutSeconds, pageLimit);
    context.putSourceConnToCache(getNodeId(), restConnWrapper);
    SingleOutputStreamOperator<RcUnit> sourceStream = env
        .addSource(new RestBatchSourceFunction(restConnWrapper, getKafkaLoggingServiceServers(),
            getKafkaLoggingServiceTopics())).uid(getNodeId())
        .setParallelism(1).name(getNodeName());

    context.putObjectToContainer(getNodeId(), sourceStream);

    try {
      createTemporaryViewForRow(context, sourceStream);
    } catch (Exception e) {
      logger.error("createTemporaryView-error:", e);
    }
  }

  public static class Builder extends WrapperBuilder {

    private String nodeId;
    private String nodeName;
    private Integer parallelism;
    private String inputType;
    private JSONArray schema;
    private JSONArray headers;
    private JSONObject body;
    private String url;
    private MethodEnum method;
    private JSONArray parameters;
    private JSONArray restApiInputSchema;
    private int timeOutSeconds;
    private int pageLimit;

    public Builder setHeaders(JSONArray headers) {
      this.headers = headers;
      return this;
    }

    public Builder setBody(JSONObject body) {
      this.body = body;
      return this;
    }

    public Builder setUrl(String url) {
      this.url = url;
      return this;
    }

    public Builder setMethod(MethodEnum method) {
      this.method = method;
      return this;
    }

    public Builder setParameters(JSONArray parameters) {
      this.parameters = parameters;
      return this;
    }

    public Builder setNodeName(String nodeName) {
      this.nodeName = nodeName;
      return this;
    }

    public Builder setNodeId(String nodeId) {
      this.nodeId = nodeId;
      return this;
    }

    public Builder setSchema(JSONArray schema) {
      this.schema = schema;
      return this;
    }

    public Builder setParallelism(Integer parallelism) {
      this.parallelism = parallelism;
      return this;
    }

    public Builder setInputType(String inputType) {
      this.inputType = inputType;
      return this;
    }

    public Builder setRestApiInputSchema(JSONArray restApiInputSchema) {
      this.restApiInputSchema = restApiInputSchema;
      return this;
    }

    public Builder setTimeOutSeconds(int timeOutSeconds) {
      this.timeOutSeconds = timeOutSeconds;
      return this;
    }

    public Builder setPageLimit(int pageLimit) {
      this.pageLimit = pageLimit;
      return this;
    }

    public RestBatchSourceWrapper build() {
      return new RestBatchSourceWrapper(this);
    }
  }

  public class RestBatchSourceFunction extends RichSourceFunction<RcUnit> {

    private RestConnWrapper restConnWrapper;
    private volatile boolean isRunning = true;
    private final String kafkaLoggingServiceServers;
    private final String kafkaLoggingServiceTopics;
    private FlinkKafkaInternalProducer flinkKafkaInternalProducer;

    /**
     * 构造方法.
     */
    public RestBatchSourceFunction(RestConnWrapper restConnWrapper,
                                   String kafkaLoggingServiceServers,
                                   String kafkaLoggingServiceTopics) {
      this.restConnWrapper = restConnWrapper;
      this.kafkaLoggingServiceServers = kafkaLoggingServiceServers;
      this.kafkaLoggingServiceTopics = kafkaLoggingServiceTopics;
    }

    public boolean isRunning() {
      return isRunning;
    }

    @Override
    public void run(SourceContext<RcUnit> ctx) throws Exception {
      if (flinkKafkaInternalProducer == null && kafkaLoggingServiceServers != null) {
        flinkKafkaInternalProducer = KafkaLogMiddlewareResourceFactory.getProducer(
            kafkaLoggingServiceServers,
            this.toString());
      }
      try {
        Set<RcUnit> result = null;
        if (!Thread.currentThread().isInterrupted()) {
          result = restConnWrapper.getData();
        }
        if (!Thread.currentThread().isInterrupted() && result != null) {
          for (RcUnit value : result) {
            if (!Thread.currentThread().isInterrupted()) {
              RcUnit rcUnit = RcUnit.valueOfDataTypeInfo(value.getData(), getSchema());
              ctx.collect(rcUnit);
            }
          }
        }
      } catch (Exception e) {
        LoggingServiceUtil.alarm(LogVo.builder()
            .k8sServiceName(GrayLogConstant.K8S_SERVICE_NAME)
            .module(GrayLogConstant.MODULE_NAME)
            .userName(getTenantId())
            .tenantId(getTenantId())
            .operateObjectName(getJobName())
            .operateObject(getJobId())
            .shortMessage("node" + getNodeName() + "Processing asset ledger API："
                + url + "Failed to return result：" + e
            )
            .operation(ShortMessageConstant.INVOKE_REST_API)
            .requestId(String.valueOf(System.currentTimeMillis()))
            .result(GrayLogResultEnum.FAIL.getValue())
            .logType(getLogType())
            .kafkaLoggingServiceTopic(kafkaLoggingServiceTopics)
            .kafkaLoggingServiceServers(kafkaLoggingServiceServers)
            .build(), flinkKafkaInternalProducer);
        throw e;
      }
    }


    @Override
    public void cancel() {
      isRunning = false;
    }

    @Override
    public void close() throws Exception {
      logger.info("Rest-api source is closed." + kafkaLoggingServiceTopics + "  "
          + kafkaLoggingServiceServers);
      LoggingServiceUtil.info(LogVo.builder()
          .k8sServiceName(GrayLogConstant.K8S_SERVICE_NAME)
          .module(GrayLogConstant.MODULE_NAME)
          .userName(getTenantId())
          .userId(getUserId())
          .tenantId(getTenantId())
          .operateObjectName(getJobName())
          .operateObject(getJobId())
          .shortMessage("Node closing asset ledger API connection succeeded")
          .operation(ShortMessageConstant.CLOSE_REST_API)
          .requestId(String.valueOf(System.currentTimeMillis()))
          .result(GrayLogResultEnum.SUCCESS.getValue())
          .logType(getLogType())
          .kafkaLoggingServiceTopic(kafkaLoggingServiceTopics)
          .kafkaLoggingServiceServers(kafkaLoggingServiceServers)
          .build(), flinkKafkaInternalProducer);
      KafkaLogMiddlewareResourceFactory.close(this.toString());
      flinkKafkaInternalProducer = null;
      restConnWrapper.close();
      restConnWrapper = null;
      super.close();
    }

  }
}
