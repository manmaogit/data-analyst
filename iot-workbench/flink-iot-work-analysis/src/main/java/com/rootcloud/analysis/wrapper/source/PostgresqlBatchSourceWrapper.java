/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.wrapper.source;

import com.alibaba.fastjson.JSONObject;
import com.rootcloud.analysis.common.context.IotWorkRuntimeContext;
import com.rootcloud.analysis.common.entity.RcUnit;
import com.rootcloud.analysis.converter.stream.table.RcUnitToRowConverter;
import com.rootcloud.analysis.core.constants.GrayLogConstant;
import com.rootcloud.analysis.core.constants.SchemaConstant;
import com.rootcloud.analysis.core.constants.ShortMessageConstant;
import com.rootcloud.analysis.core.enums.GrayLogResultEnum;
import com.rootcloud.analysis.core.graylog.LogVo;
import com.rootcloud.analysis.log.loggingservice.LoggingServiceUtil;
import com.rootcloud.analysis.wrapper.function.jdbc.JdbcRichSourceFunction;
import com.rootcloud.analysis.wrapper.jdbc.JdbcConnWrapper;
import com.rootcloud.analysis.wrapper.jdbc.PostgreSqlBatchConnWrapper;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import lombok.Getter;
import lombok.ToString;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.source.SourceFunction;
import org.apache.flink.table.api.bridge.java.StreamTableEnvironment;
import org.apache.flink.types.Row;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Getter
@ToString(callSuper = true)
public class PostgresqlBatchSourceWrapper extends JdbcSourceWrapper {

  private final Logger logger =
      LoggerFactory.getLogger(PostgresqlBatchSourceWrapper.class);



  public PostgresqlBatchSourceWrapper(Builder builder) {
    super(builder);
  }

  public static class Builder extends JdbcSourceWrapper.Builder {

    public PostgresqlBatchSourceWrapper build() {
      return new PostgresqlBatchSourceWrapper(this);
    }
  }


  @Override
  public void registerInput(IotWorkRuntimeContext context) {
    PostgreSqlBatchConnWrapper postgresqlDynamicConnWrapper =
        new PostgreSqlBatchConnWrapper("org.postgresql.Driver",
            uri, username, password, "select 1",
            table, getSchema(), limit, filterSql,
            tableSql, sqlMode);
    context.putSourceConnToCache(getNodeId(), postgresqlDynamicConnWrapper);

    StreamExecutionEnvironment env = context.getExecutionEnv();

    SourceFunction sourceFunction = new PostgreSqlBatchSourceFunction(
        postgresqlDynamicConnWrapper, getKafkaLoggingServiceServers(),
        getKafkaLoggingServiceTopics());

    SingleOutputStreamOperator<RcUnit> sourceStream = env
        .addSource(sourceFunction)
        .uid(getNodeId())
        .setParallelism(1)
        .name(getNodeName());
    context.putObjectToContainer(getNodeId(), sourceStream);
    try {
      createTemporaryViewForRow(context, sourceStream);
    } catch (Exception e) {
      logger.error("createTemporaryView-error:", e);
    }
  }

  protected void createTemporaryViewForRow(IotWorkRuntimeContext context,
                                           DataStream<RcUnit> sourceDataStream) throws Exception {
    List<String> fieldNames = new ArrayList<>(getSchema().size());
    List<String> fieldTypes = new ArrayList<>(getSchema().size());
    List<Integer> valueTypeScales = new ArrayList<>(getSchema().size());
    for (int i = 0; i < getSchema().size(); i++) {
      JSONObject jsonObject = (JSONObject) getSchema().get(i);
      fieldNames.add(jsonObject.getString(SchemaConstant.ATTR_NAME));
      fieldTypes.add(jsonObject.getString(SchemaConstant.ATTR_DATA_TYPE));
      valueTypeScales.add(jsonObject.getInteger(SchemaConstant.ATTR_SCALE));
    }
    RcUnitToRowConverter converter = new RcUnitToRowConverter(getNodeId(), fieldNames,
        fieldTypes, valueTypeScales);
    DataStream<Row> rowDataStream = converter.convert(sourceDataStream);
    StreamTableEnvironment tableEnv = context.getTableEnvFromCache();
    tableEnv.createTemporaryView(getNodeId(), rowDataStream);
  }


  private class PostgreSqlBatchSourceFunction extends JdbcRichSourceFunction<RcUnit> {

    private final String kafkaLoggingServiceServers;
    private final String kafkaLoggingServiceTopics;

    /**
     * 构造方法.
     */
    public PostgreSqlBatchSourceFunction(
        JdbcConnWrapper jdbcConnWrapper, String kafkaLoggingServiceServers,
        String kafkaLoggingServiceTopics) {
      super(jdbcConnWrapper);
      this.kafkaLoggingServiceServers = kafkaLoggingServiceServers;
      this.kafkaLoggingServiceTopics = kafkaLoggingServiceTopics;
    }

    @Override
    public void open(Configuration parameters) throws Exception {
      super.open(parameters);
      openProducer(kafkaLoggingServiceServers);
    }

    @Override
    public void run(SourceContext<RcUnit> ctx) {
      try {
        Set<RcUnit> result = null;
        if (!Thread.currentThread().isInterrupted()) {
          result = jdbcConnWrapper.getData();
        }
        if (!Thread.currentThread().isInterrupted() && result != null) {
          for (RcUnit rcUnit : result) {
            if (!Thread.currentThread().isInterrupted()) {
              ctx.collect(rcUnit);
            }
          }
        }
      } catch (Exception ex) {
        logger.error("Exception occurred when reading data from PostgreSQL: {}",
            ex.getMessage(), ex);
        LoggingServiceUtil.alarm(LogVo.builder()
            .k8sServiceName(GrayLogConstant.K8S_SERVICE_NAME)
            .module(GrayLogConstant.MODULE_NAME)
            .userName(getTenantId())
            .tenantId(getTenantId())
            .operateObjectName(getJobName())
            .operateObject(getJobId())
            .shortMessage(String.format("Exception occurred in node %s when reading "
                    + "data from table %s in PostgreSQL. Detail: %s", getNodeName(),
                table == null ? "" : table,
                ex.toString()))
            .operation(ShortMessageConstant.READ_POSTGRESQL_DATA)
            .requestId(String.valueOf(System.currentTimeMillis()))
            .result(GrayLogResultEnum.FAIL.getValue())
            .logType(getLogType())
            .kafkaLoggingServiceTopic(kafkaLoggingServiceTopics)
            .kafkaLoggingServiceServers(kafkaLoggingServiceServers)
            .build(), flinkKafkaInternalProducer);
      }
      logger.info("PostgreSql source is over,URI：{}，Table：{}", uri, table);
    }


    @Override
    public void cancel() {
      logger.info("PostgreSql source is canceled.");
      LoggingServiceUtil.info(LogVo.builder()
          .k8sServiceName(GrayLogConstant.K8S_SERVICE_NAME)
          .module(GrayLogConstant.MODULE_NAME)
          .userName(getTenantId())
          .userId(getUserId())
          .tenantId(getTenantId())
          .operateObjectName(getJobName())
          .operateObject(getJobId())
          .shortMessage(String.format("Node %s disconnected from PostgreSQL."
                  + "Database connection string: %s." + (table == null ? "" : "Table:" + table),
              getNodeName(), uri))
          .operation(ShortMessageConstant.CLOSE_POSTGRESQL)
          .requestId(String.valueOf(System.currentTimeMillis()))
          .result(GrayLogResultEnum.SUCCESS.getValue())
          .logType(getLogType())
          .kafkaLoggingServiceTopic(kafkaLoggingServiceTopics)
          .kafkaLoggingServiceServers(kafkaLoggingServiceServers)
          .build(), flinkKafkaInternalProducer);

    }

    @Override
    public void close() throws Exception {

      logger.info("PostgreSql source is closed.");
      LoggingServiceUtil.info(LogVo.builder()
          .k8sServiceName(GrayLogConstant.K8S_SERVICE_NAME)
          .module(GrayLogConstant.MODULE_NAME)
          .userName(getTenantId())
          .userId(getUserId())
          .tenantId(getTenantId())
          .operateObjectName(getJobName())
          .operateObject(getJobId())
          .shortMessage(String.format("Node %s disconnected from PostgreSQL. "
                  + "Database connection string: %s." + (table == null ? "" : "Table:" + table),
              getNodeName(), uri))
          .operation(ShortMessageConstant.CLOSE_POSTGRESQL)
          .requestId(String.valueOf(System.currentTimeMillis()))
          .result(GrayLogResultEnum.SUCCESS.getValue())
          .logType(getLogType())
          .kafkaLoggingServiceTopic(kafkaLoggingServiceTopics)
          .kafkaLoggingServiceServers(kafkaLoggingServiceServers)
          .build(), flinkKafkaInternalProducer);
      super.close();
    }
  }

}
