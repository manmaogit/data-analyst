/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.converter.time.relative;

import com.rootcloud.analysis.core.enums.TimeZoneTypeEnum;
import java.io.Serializable;
import java.time.ZoneOffset;

public interface IRelativeTimeConverter extends Serializable {

  /**
   * Converts datetime to epoch second.
   */
  long convert2EpochSecond(String datetimeStr, String pattern, TimeZoneTypeEnum timezone,
                           int timeOffset);

  /**
   * Converts datetime to epoch second.
   */
  long convert2EpochSecond(String datetimeStr, String pattern, ZoneOffset offset,
                           int timeOffset);

  /**
   * Converts datetime to epoch second.
   */
  long convert2EpochSecond(String yyyyMMddHHmmss, ZoneOffset offset, int timeOffset);

}
