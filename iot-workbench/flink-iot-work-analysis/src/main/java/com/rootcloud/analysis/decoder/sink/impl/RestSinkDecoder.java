/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.decoder.sink.impl;

import static com.rootcloud.analysis.core.constants.JobConstant.KEY_TIMEOUT_SECONDS;
import static com.rootcloud.analysis.core.constants.RestNodeConstants.REST_BATCH_SIZE;
import static com.rootcloud.analysis.core.constants.RestNodeConstants.REST_INSERT_KEYS;
import static com.rootcloud.analysis.core.constants.RestNodeConstants.REST_IS_STREAM;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Maps;
import com.rootcloud.analysis.core.constants.JobConstant;
import com.rootcloud.analysis.core.constants.RestNodeConstants;
import com.rootcloud.analysis.core.constants.SchemaConstant;
import com.rootcloud.analysis.core.enums.SinkTypeEnum;
import com.rootcloud.analysis.decoder.sink.AbstractSinkDecoder;
import com.rootcloud.analysis.decoder.sink.SinkDecoder;
import com.rootcloud.analysis.exception.DecodeSinkException;
import com.rootcloud.analysis.wrapper.sink.AbstractSinkWrapper;
import com.rootcloud.analysis.wrapper.sink.RestSinkWrapper;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * IDF-设备台账-输出节点.
 *
 * @author: zexin.huang
 * @createdDate: 2022/6/6 17:16
 */
@SinkDecoder(sinkType = SinkTypeEnum.REST)
public class RestSinkDecoder extends AbstractSinkDecoder {

  private Logger logger = LoggerFactory.getLogger(RestSinkDecoder.class);

  @Override
  protected AbstractSinkWrapper decodeSinkParams(JSONObject conf) {
    try {
      RestSinkWrapper.RestSinkWrapperBuilder builder = RestSinkWrapper.builder();
      JSONObject params = (JSONObject) conf.get(JobConstant.KEY_PARAMETERS);
      builder.url(params.getString(RestNodeConstants.URL_KEY))
          .headers(params.getJSONArray(RestNodeConstants.HEADERS_KEY))
          .timeOutSeconds(params.getIntValue(KEY_TIMEOUT_SECONDS))
          .batchSize(params.getIntValue(REST_BATCH_SIZE))
          .isStream(conf.getBoolean(REST_IS_STREAM))
          .insertKeys(JSONObject.parseArray(conf.getString(REST_INSERT_KEYS), String.class))
          .notNullKeys(JSONObject.parseArray(conf.getString(REST_INSERT_KEYS), String.class));

      Map<String, String> attributeMapper = Maps.newHashMap();
      JSONArray attributeMapperJs = conf.getJSONArray(JobConstant.KEY_ATTRIBUTE_MAPPER);
      for (int i = 0; i < attributeMapperJs.size(); i++) {
        JSONObject targetAttribute = ((JSONObject) attributeMapperJs.get(i))
            .getJSONObject(JobConstant.KEY_TARGET_ATTRIBUTE);
        JSONObject sourceAttribute = ((JSONObject) attributeMapperJs.get(i))
            .getJSONObject(JobConstant.KEY_SOURCE_ATTRIBUTE);
        attributeMapper.put(targetAttribute.getString(SchemaConstant.ATTR_NAME),
            sourceAttribute.getString(SchemaConstant.ATTR_NAME));
      }
      builder.attributeMapper(attributeMapper);

      RestSinkWrapper wrapper = builder.build();
      logger.debug("Decoding rest sink: {}", wrapper);
      return wrapper;
    } catch (Exception ex) {
      logger.error("Exception occurred when decoding rest sink: {}", conf);
      throw new DecodeSinkException(conf.getString(JobConstant.KEY_NODE_ID),
          conf.toJSONString());
    }
  }
}
