/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.wrapper.function.filter;

import com.rootcloud.analysis.common.entity.RcUnit;
import lombok.ToString;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@ToString(callSuper = true)
public class AndFilterWrapper extends CompositeFilterWrapper {

  private static Logger logger = LoggerFactory.getLogger(OrFilterWrapper.class);

  @Override
  public boolean filter(RcUnit value) throws Exception {
    boolean result = true;
    try {
      for (AbstractFilterWrapper filter : filterList) {
        if (!filter.filter(value)) {
          result = false;
        }
      }
    } catch (Exception ex) {
      logger.error("Exception occurred when filtering data: {}， exception message: {}",
          value.toString(), ex.getMessage(), ex);
      result = false;
    }
    logger.debug("'AND' filter result: {}, record: {}", result, value.toString());
    return result;
  }
}
