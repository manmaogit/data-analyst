/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.decoder.function.filter.impl;

import com.alibaba.fastjson.JSONObject;
import com.rootcloud.analysis.core.constants.FilterConstant;
import com.rootcloud.analysis.core.enums.FilterRuleEnum;
import com.rootcloud.analysis.decoder.function.filter.AbstractFilterDecoder;
import com.rootcloud.analysis.decoder.function.filter.FilterDecoder;
import com.rootcloud.analysis.wrapper.function.filter.AbstractFilterWrapper;
import com.rootcloud.analysis.wrapper.function.filter.PrefixFilterWrapper;

@FilterDecoder(filterRule = FilterRuleEnum.PREFIX)
public class PrefixFilterDecoder extends AbstractFilterDecoder {

  @Override
  public AbstractFilterWrapper decode(JSONObject config) {
    return PrefixFilterWrapper.builder().attribute(config.getString(FilterConstant.ATTRIBUTE))
        .value(config.get(FilterConstant.VALUE)).build();
  }
}