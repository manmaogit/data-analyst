/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.converter.frame;

import com.rootcloud.analysis.common.entity.RcUnitFrame;
import com.rootcloud.analysis.common.util.ClassUtil;
import com.rootcloud.analysis.core.enums.DataTypeEnum;
import java.lang.reflect.Method;
import java.util.List;
import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtMethod;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.typeinfo.BasicTypeInfo;
import org.apache.flink.api.java.typeutils.RowTypeInfo;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.table.runtime.typeutils.BigDecimalTypeInfo;
import org.apache.flink.types.Row;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The class is Deprecated.
 */
@Deprecated
public class RcUnitFrameToRowConverter implements IFrameConverter<RcUnitFrame, Row> {

  private static final Logger logger = LoggerFactory.getLogger(RcUnitFrameToRowConverter.class);

  private static final String ROW_TYPE_CONTAINER = "com.rootcloud.RowtypeContainer";

  private List<String> key;
  private List<String> valueType;

  /**
   * RcUnitToRowConverter.
   */
  public RcUnitFrameToRowConverter(List<String> keys, List<String> valueTypes) throws Exception {
    this.key = keys;
    this.valueType = valueTypes;
  }

  @Override
  public DataStream<Row> convert(DataStream<RcUnitFrame> input) throws Exception {

    ClassPool pool = ClassPool.getDefault();
    pool.importPackage(RowTypeInfo.class.getPackage().getName());
    pool.importPackage(BasicTypeInfo.class.getPackage().getName());
    pool.importPackage(BigDecimalTypeInfo.class.getPackage().getName());

    StringBuilder builder = new StringBuilder();
    builder.append("public RowTypeInfo getRowType(){");
    builder.append("TypeInformation[] fieldTypes = new TypeInformation[] {");

    for (int index = 0; index < valueType.size(); index++) {
      builder.append(DataTypeEnum.createFlinkDataType(valueType.get(index), 4));
      if (index != valueType.size() - 1) {
        builder.append(",");
      }
    }

    builder.append("};");

    builder.append("String[] fieldNames = new String[] { ");
    for (int index = 0; index < key.size(); index++) {
      builder.append("\"");
      builder.append(key.get(index));
      builder.append("\"");
      if (index != key.size() - 1) {
        builder.append(",");
      }
    }
    builder.append("};");

    builder.append("return new RowTypeInfo(fieldTypes,fieldNames);");

    builder.append("}");

    logger.info("RowtypeContainer class:{}", builder.toString());

    CtClass container = ClassUtil.makeClass(pool, ROW_TYPE_CONTAINER);
    CtMethod gmethod = CtMethod.make(builder.toString(), container);
    container.addMethod(gmethod);
    Class runner = container.toClass();

    Method gm = runner.getMethod("getRowType");
    Object object = runner.newInstance();
    RowTypeInfo rowTypeInfo = (RowTypeInfo) gm.invoke(object);

    return input.map(new MapFunction<RcUnitFrame, Row>() {
      @Override
      public Row map(RcUnitFrame value) throws Exception {
        Row row = new Row(key.size());
        for (int index = 0; index < key.size(); index++) {
          row.setField(index, value.getRcu().getAttr(key.get(index)));
        }
        return row;
      }
    }).returns(rowTypeInfo);

  }
}
