/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.wrapper.function.flinksql;

import com.google.common.collect.Maps;
import com.rootcloud.analysis.common.entity.RcUnit;
import com.rootcloud.analysis.common.factory.KafkaLogMiddlewareResourceFactory;
import com.rootcloud.analysis.core.constants.GrayLogConstant;
import com.rootcloud.analysis.core.constants.ShortMessageConstant;
import com.rootcloud.analysis.core.enums.GrayLogResultEnum;
import com.rootcloud.analysis.core.enums.GraylogLogTypeEnum;
import com.rootcloud.analysis.core.graylog.LogVo;
import com.rootcloud.analysis.log.loggingservice.LoggingServiceUtil;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import lombok.AllArgsConstructor;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.functions.ProcessFunction;
import org.apache.flink.streaming.connectors.kafka.internals.FlinkKafkaInternalProducer;
import org.apache.flink.types.Row;
import org.apache.flink.util.Collector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



/**
 * flink sql.
 *
 * @author: zexin.huang
 * @createdDate: 2022/8/15 15:26
 */
@AllArgsConstructor
public class FlinkSqlRow2RcUnitFlatMap extends ProcessFunction<Tuple2<Boolean, Row>, RcUnit> {

  private final Logger logger = LoggerFactory.getLogger(FlinkSqlRow2RcUnitFlatMap.class);

  private String jobId;
  private String tenantId;
  private String userId;
  private String jobName;
  private String nodeId;

  /**
   * Flag of whether to keep the retracted rows. This should always be set to false except for a
   * few cases.
   */
  private final boolean keepRetractedRow;

  /**
   * The field names of the incoming Row data.
   */
  private final List<String> fieldList;

  private final String kafkaLoggingServiceServers;

  private final String kafkaLoggingServiceTopics;

  private FlinkKafkaInternalProducer flinkKafkaInternalProducer;

  @Override
  public void open(Configuration parameters) throws Exception {
    super.open(parameters);
    if (flinkKafkaInternalProducer == null && kafkaLoggingServiceServers != null) {
      flinkKafkaInternalProducer = KafkaLogMiddlewareResourceFactory.getProducer(kafkaLoggingServiceServers, this.toString());
    }

  }

  @Override
  public void close() throws Exception {
    super.close();
    KafkaLogMiddlewareResourceFactory.close(this.toString());
    flinkKafkaInternalProducer = null;
  }

  @Override
  public void processElement(Tuple2<Boolean, Row> value,
                             ProcessFunction<Tuple2<Boolean, Row>, RcUnit>.Context ctx,
                             Collector<RcUnit> out) throws Exception {
    if (value == null) {
      return;
    }
    if (!keepRetractedRow && !value.f0) {
      return;
    }
    Map<String, Object> map = Maps.newHashMap();
    try {
      for (int i = 0; i < fieldList.size(); i++) {
        String key = fieldList.get(i);
        Object val = value.f1.getField(i);
        if (val instanceof LocalDateTime) {
          val = Timestamp.valueOf((LocalDateTime) val);
        }
        map.put(key, val);
      }
      // 不做Schema转换.
      RcUnit result = RcUnit.valueOf(map);
      logger.debug("FlinkSql RcUnit output: {}", result);
      if (!Thread.currentThread().isInterrupted()) {
        out.collect(result);
      }
    } catch (Exception ex) {
      logger.error("Exception occurred when processing sql result: {}", ex);
      LoggingServiceUtil.error(LogVo.builder()
          .k8sServiceName(GrayLogConstant.K8S_SERVICE_NAME)
          .module(GrayLogConstant.MODULE_NAME)
          .userName(tenantId)
          .userId(userId)
          .tenantId(tenantId)
          .operateObjectName(jobName)
          .operateObject(jobId)
          .shortMessage("node" + nodeId + "Execution failed" + ex.toString())
          .operation(ShortMessageConstant.PROCESS_SQL_RESULT)
          .requestId(String.valueOf(System.currentTimeMillis()))
          .result(GrayLogResultEnum.FAIL.getValue())
          .logType(GraylogLogTypeEnum.OFFLINE)
          .kafkaLoggingServiceTopic(kafkaLoggingServiceTopics)
          .kafkaLoggingServiceServers(kafkaLoggingServiceServers)
          .build(), flinkKafkaInternalProducer);
    }
  }

}
