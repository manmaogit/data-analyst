/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.wrapper.sink;

import static com.rootcloud.analysis.core.constants.CommonConstant.COMMA;
import static com.rootcloud.analysis.core.constants.CommonConstant.VERTICAL_LINE;
import static com.rootcloud.analysis.core.constants.SchemaConstant.COLUMN_SEPARATOR;
import static com.rootcloud.analysis.core.enums.InsertTypeEnum.INSERT;
import static com.rootcloud.analysis.core.enums.InsertTypeEnum.UPSERT;

import com.google.common.base.Strings;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.rootcloud.analysis.common.context.IotWorkRuntimeContext;
import com.rootcloud.analysis.common.entity.RcUnit;
import com.rootcloud.analysis.common.util.DateUtil;
import com.rootcloud.analysis.converter.time.ITimeConverter;
import com.rootcloud.analysis.converter.time.TimeConverterFactory;
import com.rootcloud.analysis.core.constants.GrayLogConstant;
import com.rootcloud.analysis.core.constants.JobConstant;
import com.rootcloud.analysis.core.constants.ShortMessageConstant;
import com.rootcloud.analysis.core.enums.GrayLogResultEnum;
import com.rootcloud.analysis.core.enums.GraylogLogTypeEnum;
import com.rootcloud.analysis.core.enums.SinkTypeEnum;
import com.rootcloud.analysis.core.graylog.LogVo;
import com.rootcloud.analysis.entity.RcTimeAttr;
import com.rootcloud.analysis.log.loggingservice.LoggingServiceUtil;
import com.rootcloud.analysis.wrapper.WrapperBuilder;
import com.rootcloud.analysis.wrapper.function.jdbc.JdbcRichSinkFunction;
import com.rootcloud.analysis.wrapper.jdbc.SqlServerConnWrapper;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import lombok.Getter;
import lombok.ToString;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.metrics.Counter;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@ToString(callSuper = true)
public class SqlServerSinkWrapper extends AbstractSinkWrapper {

  private static final String SHOW_COLUMN =
      "SELECT name AS Field ,TYPE_NAME(system_type_id) "
          + "AS Type  FROM sys.columns WHERE object_id = OBJECT_ID('%s')";
  private static final Set<String> RETRY_SQL_STATES = Sets.newHashSet("23000");
  private static final Logger logger = LoggerFactory.getLogger(SqlServerSinkWrapper.class);
  private final String uri;
  private final String username;
  private final String password;

  private final String table;

  private final String insertSql;
  private final String updateSql;
  private final String selectSql;

  private final List<String> insertFields;
  private final List<String> updateFields;
  private final List<String> selectFields;

  private final ITimeConverter timeConverter;

  private final RcTimeAttr timeAttr;

  @Getter private final ZoneId zoneId;

  private final Map<String, String> attributeMapper;

  private final String insertType;

  @Getter private final int startMonth;

  @Getter private final int startDay;

  @Getter private final int startHour;

  @Getter private final int startMinute;

  private final List<String> accAttrs;

  private final boolean timeAggregationFlag;

  private SqlServerSinkWrapper(Builder builder) {
    super(
        builder.jobId,
        builder.jobName,
        builder.tenantId,
        builder.nodeId,
        builder.parallelism,
        builder.nodeName,
        builder.priorNodeId,
        builder.sinkType,
        builder.logType);

    this.uri = builder.uri;
    this.username = builder.username;
    this.password = builder.password;

    this.table = builder.table;

    this.insertSql = builder.insertSql;
    this.updateSql = builder.updateSql;
    this.selectSql = builder.selectSql;

    this.insertFields = builder.insertFields;
    this.updateFields = builder.updateFields;
    this.selectFields = builder.selectFields;

    this.timeAttr = builder.timeAttr;

    this.zoneId = builder.timeZone;

    this.timeConverter = TimeConverterFactory.getConverter(builder.timeDimension);

    this.attributeMapper = builder.attributeMapper;

    this.insertType = builder.insertType;

    this.startMonth = builder.startMonth;
    this.startDay = builder.startDay;
    this.startHour = builder.startHour;
    this.startMinute = builder.startMinute;

    this.accAttrs = builder.accAttrs;

    this.timeAggregationFlag = builder.timeAggregationFlag;
  }

  private void buildSql(
      PreparedStatement statement,
      RcUnit record,
      List<String> fieldList,
      Map<String, String> schema)
      throws SQLException {
    for (int index = 0; index < fieldList.size(); index++) {
      String field = fieldList.get(index);
      Object itemValue = record.getAttr(attributeMapper.get(field));

      itemValue = itemValue == null && accAttrs.contains(field) ? 0 : itemValue;

      if (timeAggregationFlag && timeAttr != null && field.equals(timeAttr.getKey())) {
        if (itemValue instanceof String) {
          statement.setObject(index + 1, timeConverter.convert((String) itemValue, zoneId));
        } else if (itemValue instanceof Long) {
          statement.setObject(
              index + 1,
              timeConverter.convert(
                  (Long) itemValue, zoneId, startMonth, startDay, startHour, startMinute));
        } else {
          Timestamp timestamp = (Timestamp) itemValue;
          statement.setObject(
              index + 1,
              timeConverter.convert(
                  timestamp.getTime(), zoneId, startMonth, startDay, startHour, startMinute));
        }
      } else {
        if (itemValue == null) {
          statement.setObject(index + 1, itemValue);
          continue;
        }
        if (schema.get(field).toUpperCase().matches("^(DATETIME|DATETIME2|SMALLDATETIME).*$")) {
          if (itemValue instanceof String
              && ((String) itemValue).matches("^\\d{4}-\\d{1,2}-\\d{1,2}$")) {
            itemValue = Date.valueOf(((String) itemValue)).getTime();
          }
          statement.setString(
              index + 1,
              DateUtil.formatNormalDateString(
                  itemValue instanceof Timestamp
                      ? ((Timestamp) itemValue).getTime()
                      : (Long) itemValue,
                  "yyyy-MM-dd HH:mm:ss"));
        } else if (schema.get(field).toUpperCase().matches("^(DATE).*$")) {
          statement.setString(
              index + 1,
              DateUtil.formatNormalDateString(
                  itemValue instanceof Timestamp
                      ? ((Timestamp) itemValue).getTime()
                      : (Long) itemValue,
                  "yyyy-MM-dd"));
        } else if (schema.get(field).toUpperCase().matches("^(TIME).*$")) {
          statement.setString(
              index + 1,
              DateUtil.formatNormalDateString(
                  itemValue instanceof Timestamp
                      ? ((Timestamp) itemValue).getTime()
                      : (Long) itemValue,
                  "HH:mm:ss"));
        } else {
          statement.setObject(index + 1, itemValue);
        }
      }
    }
  }

  @Override
  public void registerOutput(IotWorkRuntimeContext context) {
    DataStream<RcUnit> priorDataStream = context.getObjectFromContainer(getPriorNodeId());
    priorDataStream
        .addSink(
            new SqlServerRichSinkFunction(
                getJobId(),
                getTenantId(),
                getLogType(),
                getKafkaLoggingServiceServers(),
                getKafkaLoggingServiceTopics()))
        .uid(getNodeId())
        .setParallelism(getParallelism())
        .name(getNodeName());
  }

  public static class Builder extends WrapperBuilder {

    private final List<String> insertFields = new ArrayList<>();
    private final List<String> updateFields = new ArrayList<>();
    private final List<String> selectFields = new ArrayList<>();
    private String nodeId;
    private String nodeName;
    private Integer parallelism;
    private String priorNodeId;
    private SinkTypeEnum sinkType;
    private String uri;
    private String username;
    private String password;
    private String table;
    private String insertKey;
    private String insertType;
    private Map<String, String> attributeMapper;
    private RcTimeAttr timeAttr;
    private String timeDimension;
    private int startMonth;
    private int startDay;
    private int startHour;
    private int startMinute;
    private ZoneId timeZone;
    private List<String> accAttrs;
    private List<String> replaceAttrs;
    private List<String> replaceWithNonNullAttrs;
    private List<String> greatestAttrs;
    private List<String> leastAttrs;
    private boolean timeAggregationFlag;
    private List<String> schemas;
    private String insertSql;
    private String updateSql;
    private String selectSql;

    public Builder setNodeId(String nodeId) {
      this.nodeId = nodeId;
      return this;
    }

    public Builder setNodeName(String nodeName) {
      this.nodeName = nodeName;
      return this;
    }

    public Builder setParallelism(Integer parallelism) {
      this.parallelism = parallelism;
      return this;
    }

    /**
     * Sets start time.
     *
     * @param startTime start time
     * @return builder
     */
    public Builder setStartTime(String startTime) {
      String[] tmp = startTime.split(VERTICAL_LINE);
      startMonth = tmp.length - 4 >= 0 ? Integer.parseInt(tmp[tmp.length - 4]) : 1;
      startDay = tmp.length - 3 >= 0 ? Integer.parseInt(tmp[tmp.length - 3]) : 1;
      startHour = tmp.length - 2 >= 0 ? Integer.parseInt(tmp[tmp.length - 2]) : 0;
      startMinute = tmp.length - 1 >= 0 ? Integer.parseInt(tmp[tmp.length - 1]) : 0;
      return this;
    }

    public Builder setUri(String uri) {
      this.uri = uri;
      return this;
    }

    public Builder setInsertType(String insertType) {
      this.insertType = insertType;
      return this;
    }

    public Builder setUsername(String username) {
      this.username = username;
      return this;
    }

    public Builder setAttributeMapper(Map<String, String> attributeMapper) {
      this.attributeMapper = attributeMapper;
      return this;
    }

    public Builder setSchemas(List<String> schemas) {
      this.schemas = schemas;
      return this;
    }

    public Builder setPassword(String password) {
      this.password = password;
      return this;
    }

    public Builder setInsertKey(String insertKey) {
      this.insertKey = insertKey;
      return this;
    }

    public Builder setAccAttrs(List<String> accAttrs) {
      this.accAttrs = accAttrs;
      return this;
    }

    public Builder setReplaceAttrs(List<String> replaceAttrs) {
      this.replaceAttrs = replaceAttrs;
      return this;
    }

    public Builder setReplaceWithNonNullAttrs(List<String> replaceWithNonNullAttrs) {
      this.replaceWithNonNullAttrs = replaceWithNonNullAttrs;
      return this;
    }

    public Builder setGreatestAttrs(List<String> greatestAttrs) {
      this.greatestAttrs = greatestAttrs;
      return this;
    }

    public Builder setLeastAttrs(List<String> leastAttrs) {
      this.leastAttrs = leastAttrs;
      return this;
    }

    public Builder setTimeDimension(String timeDimension) {
      this.timeDimension = timeDimension;
      return this;
    }

    public Builder setTimeAttr(RcTimeAttr timeAttr) {
      this.timeAttr = timeAttr;
      return this;
    }

    public Builder setTimeZone(ZoneId timeZone) {
      this.timeZone = timeZone;
      return this;
    }

    public Builder setPriorNodeId(String priorNodeId) {
      this.priorNodeId = priorNodeId;
      return this;
    }

    public Builder setSinkType(SinkTypeEnum sinkType) {
      this.sinkType = sinkType;
      return this;
    }

    public Builder setTable(String table) {
      this.table = table;
      return this;
    }

    public Builder setTimeAggregationFlag(boolean timeAggregationFlag) {
      this.timeAggregationFlag = timeAggregationFlag;
      return this;
    }

    private String formatField(String field) {
      String rt = field;
      if (field.startsWith("[") && field.endsWith("]")) {
        rt = field.replace("[","\\[");
        rt = rt.substring(0, rt.length() - 1) + "\\]";
        return rt;
      }
      return String.format("[%s]", rt);
    }

    /** build. */
    public SqlServerSinkWrapper build() {
      this.table = buildTableName();
      this.insertSql = buildInsertSql();
      this.updateSql = buildUpdateSql();
      this.selectSql = buildSelectSql();
      return new SqlServerSinkWrapper(this);
    }

    private String buildTableName() {
      String[] split = table.split("\\.");
      String schemaName = "dbo";
      String tableName;
      if (split.length != 2) {
        tableName = table;
      } else {
        schemaName = split[0];
        tableName = split[1];
      }
      if (tableName.startsWith("[") && tableName.endsWith("]")) {
        tableName = tableName.substring(1, tableName.length() - 1);
      }
      if (schemaName.startsWith("[") && schemaName.endsWith("]")) {
        schemaName = schemaName.substring(1, schemaName.length() - 1);
      }
      return String.format("[%s].[%s]", schemaName, tableName);
    }

    private String buildInsertSql() {
      StringBuilder builder = new StringBuilder();
      builder.append("insert into ");
      builder.append(table);
      builder.append(" (");

      for (int index = 0; index < schemas.size(); index++) {
        String[] unit = schemas.get(index).split(COLUMN_SEPARATOR);
        String field = unit[0];

        if (index > 0) {
          builder.append(COMMA);
        }
        builder.append(formatField(field));

        insertFields.add(field);
      }

      builder.append(") values(");
      for (int index = 0; index < schemas.size(); index++) {
        builder.append("?");
        if (index < schemas.size() - 1) {
          builder.append(COMMA);
        }
      }
      builder.append(")");
      return builder.toString();
    }

    private String buildUpdateSql() {
      if (accAttrs.isEmpty()
          && replaceAttrs.isEmpty()
          && greatestAttrs.isEmpty()
          && leastAttrs.isEmpty()
          && replaceWithNonNullAttrs.isEmpty()) {
        return null;
      }
      StringBuilder builder = new StringBuilder();
      builder.append("update ").append(table).append(" set ");

      for (int index = 0; index < accAttrs.size(); index++) {
        String field = accAttrs.get(index);
        builder
            .append(formatField(field))
            .append(" = ")
            .append("ISNULL(")
            .append(formatField(field))
            .append(",0)")
            .append("+?");
        if (index < accAttrs.size() - 1) {
          builder.append(COMMA);
        }
        updateFields.add(field);
      }
      if (replaceAttrs != null && !replaceAttrs.isEmpty()) {
        replaceAttrs.removeAll(accAttrs);
        for (int i = 0; i < replaceAttrs.size(); i++) {
          String field = replaceAttrs.get(i);
          if (i > 0 || updateFields.size() > 0) {
            builder.append(COMMA);
          }
          builder.append(formatField(field)).append(" = ").append("?");
          updateFields.add(field);
        }
      }

      if (replaceWithNonNullAttrs != null && !replaceWithNonNullAttrs.isEmpty()) {
        for (int i = 0; i < replaceWithNonNullAttrs.size(); i++) {
          String field = replaceWithNonNullAttrs.get(i);
          if (i > 0 || updateFields.size() > 0) {
            builder.append(COMMA);
          }
          builder
              .append(formatField(field))
              .append(" = ")
              .append("ISNULL(?, ")
              .append(formatField(field))
              .append(")");
          updateFields.add(field);
        }
      }

      if (greatestAttrs != null && !greatestAttrs.isEmpty()) {
        greatestAttrs.removeAll(accAttrs);
        greatestAttrs.removeAll(replaceAttrs);
        for (int i = 0; i < greatestAttrs.size(); i++) {
          if (i > 0 || updateFields.size() > 0) {
            builder.append(COMMA);
          }
          String field = greatestAttrs.get(i);
          builder
              .append(formatField(field))
              .append(" = IIF((? is NULL), ").append(formatField(field)).append(",")
              .append("(select max(i) from (values (")
              .append(formatField(field))
              .append(") ,")
              .append("(?)) AS T(i)))");
          updateFields.add(field);
          updateFields.add(field);
        }
      }

      if (leastAttrs != null && !leastAttrs.isEmpty()) {
        leastAttrs.removeAll(accAttrs);
        leastAttrs.removeAll(replaceAttrs);
        leastAttrs.removeAll(greatestAttrs);
        for (int i = 0; i < leastAttrs.size(); i++) {
          if (i > 0 || !updateFields.isEmpty()) {
            builder.append(COMMA);
          }
          String field = leastAttrs.get(i);
          builder
              .append(formatField(field))
              .append(" = IIF((? is NULL), ").append(formatField(field)).append(",")
              .append("(select min(i) from (values (")
              .append(formatField(field))
              .append(") ,")
              .append("(?)) AS T(i)))");
          updateFields.add(field);
          updateFields.add(field);
        }
      }
      if (updateFields.size() == 0) {
        return null;
      }
      builder.append(" where ");

      String[] insetKeyTmp = insertKey.split(COMMA);
      for (int index = 0; index < insetKeyTmp.length; index++) {
        String field = insetKeyTmp[index];
        builder.append(formatField(field));
        builder.append("=?");
        if (index < insetKeyTmp.length - 1) {
          builder.append(" and ");
        }
        updateFields.add(field);
      }

      return builder.toString();
    }

    private String buildSelectSql() {
      StringBuilder builder = new StringBuilder();
      builder.append("select top 1 * from ");
      builder.append(table);
      builder.append(" where ");

      String[] temp = insertKey.split(COMMA);
      for (int index = 0; index < temp.length; index++) {
        String field = temp[index];
        if (index > 0) {
          builder.append(" and ");
        }

        builder.append(formatField(field));
        builder.append("=?");

        selectFields.add(field);
      }
      return builder.toString();
    }
  }

  private class SqlServerRichSinkFunction extends JdbcRichSinkFunction<RcUnit> {

    private final String jobId;
    private final String tenantId;
    private final GraylogLogTypeEnum logType;
    private Counter counterRecordsOut;
    // SqlServer table schema
    private Map<String, String> schema;

    private SqlServerRichSinkFunction(
        String jobId,
        String tenantId,
        GraylogLogTypeEnum logType,
        String kafkaLoggingServiceServers,
        String kafkaLoggingServiceTopics) {
      super(
          new SqlServerConnWrapper(
              "com.microsoft.sqlserver.jdbc.SQLServerDriver",
              uri,
              username,
              password,
              "select 1",
              table,
              null,
              0,
              null));
      this.jobId = jobId;
      this.tenantId = tenantId;
      this.logType = logType;
    }

    @Override
    public void open(Configuration parameters) throws Exception {
      super.open(parameters);
      initSchema();
      counterRecordsOut =
          getRuntimeContext()
              .getMetricGroup()
              .counter(JobConstant.FLINK_METRIC_NUM_RECORDS_OUT_OVERWRITE);
      openProducer(kafkaLoggingServiceServers);
    }

    private void initSchema() throws SQLException {
      schema = Maps.newHashMap();
      try (PreparedStatement preparedStatement =
              this.jdbcConnWrapper
                  .getConnectionWrapper()
                  .prepareStatement(String.format(SHOW_COLUMN, table));
          ResultSet rs = preparedStatement.executeQuery()) {
        while (rs.next()) {
          schema.put(rs.getString("Field"), rs.getString("Type"));
        }
      } catch (Exception ex) {
        logger.error("Exception occurred when try to init schema, exception: {}", ex.getMessage());
        LoggingServiceUtil.alarm(
            LogVo.builder()
                .k8sServiceName(GrayLogConstant.K8S_SERVICE_NAME)
                .module(GrayLogConstant.MODULE_NAME)
                .userName(tenantId)
                .tenantId(tenantId)
                .operateObjectName(getJobName())
                .operateObject(jobId)
                .shortMessage(
                    String.format(
                        "Failed to init the schema in Node %s: %s."
                            + " Database connection string: %s. Table: %s.",
                        getNodeName(), ex.toString(), uri, table))
                .operation(ShortMessageConstant.INIT_SCHEMA)
                .requestId(String.valueOf(System.currentTimeMillis()))
                .result(GrayLogResultEnum.FAIL.getValue())
                .kafkaLoggingServiceTopic(kafkaLoggingServiceTopics)
                .kafkaLoggingServiceServers(kafkaLoggingServiceServers)
                .logType(logType)
                .build(),
            flinkKafkaInternalProducer);
      }
      logger.debug("SqlServer table schema: {}", schema);
    }

    @Override
    public void invoke(RcUnit record, Context context) throws Exception {
      logger.debug("SqlServer sink process element: {}", record);
      try {
        if (INSERT.name().equals(insertType)) {
          insert(record);
        } else if (UPSERT.name().equals(insertType)) {
          try {
            upsert(record);
          } catch (SQLException sqlException) {
            // Duplicate entry, try again.
            if (RETRY_SQL_STATES.contains(sqlException.getSQLState())) {
              logger.debug("Duplicate entry, try again, record: {}", record);
              upsert(record);
            } else {
              throw sqlException;
            }
          }
        } else {
          logger.error("Invalid insertType: {}", insertType);
        }
      } catch (Exception e) {
        logger.error(
            "Exception occurred when try to insert/update {} to {}, exception: {}",
            record,
            table,
            e.getMessage(),
            e);
        LoggingServiceUtil.alarm(
            LogVo.builder()
                .k8sServiceName(GrayLogConstant.K8S_SERVICE_NAME)
                .module(GrayLogConstant.MODULE_NAME)
                .userName(tenantId)
                .tenantId(tenantId)
                .operateObjectName(getJobName())
                .operateObject(jobId)
                .shortMessage(
                    String.format(
                        "Node %s failed to insert into SqlServer table %s due to %s."
                            + " Database connection string: %s. Table: %s.",
                        getNodeName(), table, e.toString(), uri, table))
                .operation(ShortMessageConstant.INSERT_SQLSERVER_DATA)
                .requestId(String.valueOf(System.currentTimeMillis()))
                .result(GrayLogResultEnum.FAIL.getValue())
                .kafkaLoggingServiceTopic(kafkaLoggingServiceTopics)
                .kafkaLoggingServiceServers(kafkaLoggingServiceServers)
                .logType(logType)
                .build(),
            flinkKafkaInternalProducer);
      }
    }

    private void insert(RcUnit record) throws Exception {
      try (PreparedStatement insert =
          this.jdbcConnWrapper.getConnectionWrapper().prepareStatement(insertSql)) {
        buildSql(insert, record, insertFields, schema);
        insert.execute();
      }
      counterRecordsOut.inc();
    }

    private void upsert(RcUnit record) throws Exception {
      try (PreparedStatement select = getConnectionWrapper().prepareStatement(selectSql)) {
        buildSql(select, record, selectFields, schema);
        try (ResultSet resultSet = select.executeQuery()) {
          if (!resultSet.next()) {
            try (PreparedStatement insert = getConnectionWrapper().prepareStatement(insertSql)) {
              buildSql(insert, record, insertFields, schema);
              insert.execute();
            }
          } else {
            if (Strings.isNullOrEmpty(updateSql)) {
              logger.debug("Update sql is empty.");
            } else {
              try (PreparedStatement update = getConnectionWrapper().prepareStatement(updateSql)) {
                buildSql(update, record, updateFields, schema);
                update.executeUpdate();
              }
            }
          }
        }
      }
      counterRecordsOut.inc();
    }

    @Override
    public void close() throws Exception {

      LoggingServiceUtil.info(
          LogVo.builder()
              .k8sServiceName(GrayLogConstant.K8S_SERVICE_NAME)
              .module(GrayLogConstant.MODULE_NAME)
              .userName(tenantId)
              .userId(getUserId())
              .tenantId(tenantId)
              .operateObjectName(getJobName())
              .operateObject(jobId)
              .shortMessage(
                  String.format(
                      "Node %s disconnected from SqlServer. Database "
                          + "connection string: %s. Table: %s.",
                      getNodeName(), uri, table))
              .operation(ShortMessageConstant.CLOSE_SQLSERVER)
              .requestId(String.valueOf(System.currentTimeMillis()))
              .result(GrayLogResultEnum.SUCCESS.getValue())
              .kafkaLoggingServiceTopic(kafkaLoggingServiceTopics)
              .kafkaLoggingServiceServers(kafkaLoggingServiceServers)
              .logType(logType)
              .build(),
          flinkKafkaInternalProducer);
      super.close();
      schema = null;
      logger.info("SqlServer sink is closed.");
    }
  }
}
