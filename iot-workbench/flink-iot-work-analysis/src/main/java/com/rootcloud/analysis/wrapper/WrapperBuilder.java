/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.wrapper;

import com.rootcloud.analysis.core.enums.GraylogLogTypeEnum;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString(callSuper = true)
public class WrapperBuilder {

  public String jobId;
  public String jobName;
  public String tenantId;
  public GraylogLogTypeEnum logType;

  public WrapperBuilder setJobId(String jobId) {
    this.jobId = jobId;
    return this;
  }

  public WrapperBuilder setJobName(String jobName) {
    this.jobName = jobName;
    return this;
  }

  public WrapperBuilder setTenantId(String tenantId) {
    this.tenantId = tenantId;
    return this;
  }

  public WrapperBuilder setLogType(GraylogLogTypeEnum logType) {
    this.logType = logType;
    return this;
  }

}