/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.decoder.sink.impl;

import com.alibaba.fastjson.JSONObject;
import com.rootcloud.analysis.core.constants.JobConstant;
import com.rootcloud.analysis.core.enums.SinkTypeEnum;
import com.rootcloud.analysis.decoder.sink.SinkDecoder;
import com.rootcloud.analysis.wrapper.sink.JdbcSink02Wrapper;
import com.rootcloud.analysis.wrapper.sink.PostgreSqlSink02Wrapper;

/**
 * Please use InfluxDbBatchSourceDecoder instead.
 */
@Deprecated
@SinkDecoder(sinkType = SinkTypeEnum.PGSQL_02)
public class PostgreSqlSink02Decoder extends JdbcSink02Decoder {

  @Override
  protected JdbcSink02Wrapper decodeJdbcSinkParams(JSONObject conf) {
    PostgreSqlSink02Wrapper postgreSqlSink02Wrapper = new PostgreSqlSink02Wrapper();
    if (conf.containsKey(JobConstant.CONTAIN_AUTO_INCREMENT_KEY)) {
      postgreSqlSink02Wrapper.setContainsAutoIncrementKey(
          conf.getBoolean(JobConstant.CONTAIN_AUTO_INCREMENT_KEY));
    }
    return postgreSqlSink02Wrapper;
  }

}