/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.actuator.impl;

import com.rootcloud.analysis.actuator.AbstractActuator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

public class StreamActuator extends AbstractActuator {

  private StreamExecutionEnvironment actuatorEnv;

  public void setActuatorEnv(
      StreamExecutionEnvironment actuatorEnv) {
    this.actuatorEnv = actuatorEnv;
  }

  @Override
  public void execute() throws Exception {
    actuatorEnv.execute(this.getJobName());
  }

}
