/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.converter.extractor;

import com.rootcloud.analysis.common.entity.RcUnit;
import com.rootcloud.analysis.common.util.DateUtil;
import com.rootcloud.analysis.entity.RcTimeAttr;
import java.sql.Timestamp;
import java.time.Instant;
import lombok.AllArgsConstructor;
import org.apache.flink.api.common.eventtime.TimestampAssigner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by 王纯超 on 2021/9/8.
 */
@AllArgsConstructor
public class FieldBasedTimestampAssigner implements TimestampAssigner<RcUnit> {
  private static final Logger LOGGER = LoggerFactory.getLogger(FieldBasedTimestampAssigner.class);
  private RcTimeAttr timeAttr;

  @Override
  public long extractTimestamp(RcUnit element, long recordTimestamp) {
    long currentMaxTimestamp = Instant.now().toEpochMilli();

    Object time = element.getAttr(timeAttr.getKey());

    if (time == null) {
      LOGGER.debug("Field {} does not exist or its value is null. Use system time instead!",
          timeAttr.getKey());
      return currentMaxTimestamp;
    }

    if (timeAttr.getType().toLowerCase().equals(RcTimeAttr.TIME_TYPE_STRING)) {
      currentMaxTimestamp = DateUtil.convertTimeToLongMs((String) time);
    } else {
      if (time instanceof Number) {
        currentMaxTimestamp = Long.valueOf(((Number) time).longValue());
      } else if (time instanceof Timestamp) {
        currentMaxTimestamp = ((Timestamp) time).getTime();
      } else {
        currentMaxTimestamp = (long) time;
      }

      if (RcTimeAttr.TIME_UNIT_S.equals(timeAttr.getFormat())) {
        currentMaxTimestamp *= 1000L;
      }
    }
    return currentMaxTimestamp;
  }
}
