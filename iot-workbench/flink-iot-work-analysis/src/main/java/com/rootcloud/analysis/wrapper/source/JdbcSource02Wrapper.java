/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.wrapper.source;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Maps;
import com.rootcloud.analysis.common.context.IotWorkRuntimeContext;
import com.rootcloud.analysis.common.entity.RcUnit;
import com.rootcloud.analysis.common.factory.KafkaLogMiddlewareResourceFactory;
import com.rootcloud.analysis.core.constants.CommonConstant;
import com.rootcloud.analysis.core.constants.GrayLogConstant;
import com.rootcloud.analysis.core.constants.SchemaConstant;
import com.rootcloud.analysis.core.constants.ShortMessageConstant;
import com.rootcloud.analysis.core.enums.GrayLogResultEnum;
import com.rootcloud.analysis.core.enums.GraylogLogTypeEnum;

import com.rootcloud.analysis.core.graylog.LogVo;
import com.rootcloud.analysis.log.loggingservice.LoggingServiceUtil;
import java.util.Map;
import java.util.stream.Collectors;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.apache.commons.lang3.StringUtils;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.functions.ProcessFunction;
import org.apache.flink.streaming.connectors.kafka.internals.FlinkKafkaInternalProducer;
import org.apache.flink.table.api.Table;
import org.apache.flink.table.api.bridge.java.StreamTableEnvironment;
import org.apache.flink.types.Row;
import org.apache.flink.util.Collector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Getter
@Setter
@ToString(callSuper = true)
public abstract class JdbcSource02Wrapper extends AbstractSourceWrapper {

  private static Logger logger = LoggerFactory.getLogger(MysqlSource02Wrapper.class);

  private static final int FETCH_SIZE = 10000;

  private String primaryKey;

  public String uri;

  public String username;

  public String password;

  public String table;

  /**
   * Gets flink sql type.
   */
  protected abstract String getFlinkSqlType(String originDbType);

  /**
   * Builds ddl.
   */
  protected String buildDdl() {
    StringBuilder ddlBuilder = new StringBuilder();
    ddlBuilder.append("CREATE TABLE ");
    ddlBuilder.append(getTempTableName());
    ddlBuilder.append(" (");
    for (int i = 0; i < getSchema().size(); i++) {
      if (i > 0) {
        ddlBuilder.append(CommonConstant.COMMA);
      }
      JSONObject jsonObject = getSchema().getJSONObject(i);
      ddlBuilder.append("`");
      ddlBuilder.append(jsonObject.getString(SchemaConstant.ATTR_ORIGINAL_NAME));
      ddlBuilder.append("`");
      ddlBuilder.append(" ");
      ddlBuilder.append(getFlinkSqlType(jsonObject
          .getString(SchemaConstant.ATTR_ORIGINAL_TYPE)));
    }
    if (StringUtils.isNotEmpty(getPrimaryKey())) {
      ddlBuilder.append(CommonConstant.COMMA);
      ddlBuilder.append("PRIMARY KEY (");
      String[] primaryKeyCols = getPrimaryKey().split(CommonConstant.COMMA);
      for (int i = 0; i < primaryKeyCols.length; i++) {
        if (i > 0) {
          ddlBuilder.append(CommonConstant.COMMA);
        }
        ddlBuilder.append("`");
        ddlBuilder.append(primaryKeyCols[i]);
        ddlBuilder.append("`");
      }
      ddlBuilder.append(") NOT ENFORCED");
    }
    ddlBuilder.append(") WITH ('connector' = 'jdbc',");
    ddlBuilder.append("'url' = '");
    ddlBuilder.append(getUri());
    ddlBuilder.append("',");
    ddlBuilder.append("'username'  = '");
    ddlBuilder.append(getUsername());
    ddlBuilder.append("',");
    ddlBuilder.append("'password'  = '");
    ddlBuilder.append(getPassword());
    ddlBuilder.append("',");
    ddlBuilder.append("'scan.fetch-size'  = '");
    ddlBuilder.append(getFetchSize());
    ddlBuilder.append("',");
    ddlBuilder.append("'table-name' = '");
    ddlBuilder.append(getTable());
    ddlBuilder.append("'");
    ddlBuilder.append(" )");
    return ddlBuilder.toString();
  }

  /**
   * Builds query sql.
   */
  protected String buildQuerySql() {
    StringBuilder querySql = new StringBuilder();
    querySql.append("select ");
    querySql.append(getSchema().stream()
        .map(
            jsonObject -> "`" + ((JSONObject) jsonObject)
                .getString(SchemaConstant.ATTR_ORIGINAL_NAME) + "`")
        .collect(
            Collectors.joining(CommonConstant.COMMA)));
    querySql.append(" from ");
    querySql.append(getTempTableName());
    logger.debug("QuerySql: {}", buildDdl());
    return querySql.toString();
  }

  protected String getTempTableName() {
    return "tmp_" + getNodeId();
  }

  protected int getFetchSize() {
    return FETCH_SIZE;
  }

  @Override
  public void registerInput(IotWorkRuntimeContext context) {
    StreamTableEnvironment tableEnv = context.getTableEnvFromCache();
    logger.debug("DDL: {}", buildDdl());
    tableEnv.executeSql(buildDdl());
    Table table = tableEnv.sqlQuery(buildQuerySql());
    DataStream<RcUnit> dataStream = tableEnv.toRetractStream(table, Row.class)
        .process(new Row2RcUnitFlatMap(getJobId(), getTenantId(),
            getJobName(), getLogType(), getSchema(), getKafkaLoggingServiceServers(),
            getKafkaLoggingServiceTopics(), null))
        .setParallelism(getParallelism());
    context.putObjectToContainer(getNodeId(), dataStream);
  }

  @AllArgsConstructor
  private static class Row2RcUnitFlatMap extends ProcessFunction<Tuple2<Boolean, Row>, RcUnit> {

    private final String jobId;
    private final String tenantId;
    private final String jobName;
    private final GraylogLogTypeEnum logType;
    private final JSONArray schema;
    private final String kafkaLoggingServiceServers;
    private final String kafkaLoggingServiceTopics;
    private FlinkKafkaInternalProducer flinkKafkaInternalProducer;

    @Override
    public void open(Configuration parameters) throws Exception {
      super.open(parameters);
      if (flinkKafkaInternalProducer == null && kafkaLoggingServiceServers != null) {
        flinkKafkaInternalProducer = KafkaLogMiddlewareResourceFactory.getProducer(
            kafkaLoggingServiceServers,
            this.toString());
      }
    }

    @Override
    public void close() throws Exception {
      super.close();
      KafkaLogMiddlewareResourceFactory.close(this.toString());
      flinkKafkaInternalProducer = null;
    }

    @Override
    public void processElement(Tuple2<Boolean, Row> value,
                               ProcessFunction<Tuple2<Boolean, Row>, RcUnit>.Context ctx,
                               Collector<RcUnit> out) throws Exception {
      try {
        Map<String, Object> map = Maps.newHashMap();
        for (int i = 0; i < schema.size(); i++) {
          map.put(((JSONObject) schema.get(i)).getString(SchemaConstant.ATTR_ORIGINAL_NAME),
              value.f1.getField(i));
        }
        RcUnit result = RcUnit.valueOfDataTypeInfo(map, schema);
        logger.debug("Jdbc source output: {}", result);
        if (!Thread.currentThread().isInterrupted()) {
          out.collect(result);
        }
      } catch (Exception ex) {
        ex.printStackTrace();
        logger.error("Exception occurred when querying data by jdbc: {}", ex.toString());
        LoggingServiceUtil.alarm(LogVo.builder()
            .k8sServiceName(GrayLogConstant.K8S_SERVICE_NAME)
            .module(GrayLogConstant.MODULE_NAME)
            .userName(tenantId)
            .tenantId(tenantId)
            .operateObjectName(jobName)
            .operateObject(jobId)
            .shortMessage(String.format("Exception occurred when querying data by jdbc: %s",
                ex.toString()))
            .operation(ShortMessageConstant.PROCESS_SQL_RESULT)
            .requestId(String.valueOf(System.currentTimeMillis()))
            .result(GrayLogResultEnum.FAIL.getValue())
            .logType(logType)
            .kafkaLoggingServiceTopic(kafkaLoggingServiceTopics)
            .kafkaLoggingServiceServers(kafkaLoggingServiceServers)
            .build(), flinkKafkaInternalProducer);
      }
    }
  }

}