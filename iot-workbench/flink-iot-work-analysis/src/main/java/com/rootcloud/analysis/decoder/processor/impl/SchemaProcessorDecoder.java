/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.decoder.processor.impl;

import com.alibaba.fastjson.JSONObject;
import com.rootcloud.analysis.core.constants.JobConstant;
import com.rootcloud.analysis.core.enums.ProcessTypeEnum;
import com.rootcloud.analysis.decoder.processor.AbstractProcessorDecoder;
import com.rootcloud.analysis.decoder.processor.ProcessorDecoder;
import com.rootcloud.analysis.exception.DecodeProcessorException;
import com.rootcloud.analysis.wrapper.processor.AbstractProcessorWrapper;
import com.rootcloud.analysis.wrapper.processor.SchemaProcessorWrapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@ProcessorDecoder(processType = ProcessTypeEnum.SCHEMA)
public class SchemaProcessorDecoder extends AbstractProcessorDecoder {

  private static Logger logger = LoggerFactory.getLogger(
      SchemaProcessorDecoder.class);

  @Override
  public AbstractProcessorWrapper decodeProcessParams(JSONObject conf) {
    try {
      SchemaProcessorWrapper.Builder builder = new SchemaProcessorWrapper.Builder();
      builder.setSchema(conf.getJSONArray(JobConstant.KEY_SCHEMA));
      SchemaProcessorWrapper result = builder.build();
      return result;
    } catch (Exception ex) {
      logger.error("Exception occurred when decoding schema processor: {}", conf);
      throw new DecodeProcessorException();
    }
  }

}
