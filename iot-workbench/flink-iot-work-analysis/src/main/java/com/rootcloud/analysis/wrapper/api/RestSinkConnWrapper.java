/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.wrapper.api;

import com.alibaba.fastjson.JSONArray;
import com.rootcloud.analysis.exception.SinkWriteDataException;
import com.rootcloud.analysis.wrapper.IDataSinkWrapper;
import lombok.Getter;
import org.apache.http.HttpEntity;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * IDF-设备台账-输出节点.
 *
 * @author: zexin.huang
 * @createdDate: 2022/6/6 10:53
 */
@Getter
public class RestSinkConnWrapper implements IDataSinkWrapper {

  private final Logger logger = LoggerFactory.getLogger(RestSinkConnWrapper.class);

  private final String url;
  private final JSONArray headers;
  private final int timeOutMilliSeconds;

  /**
   * IDF-设备台账-输出节点.
   */
  public RestSinkConnWrapper(String url, JSONArray headers, int timeOutSeconds) {
    this.url = url;
    this.headers = headers;
    this.timeOutMilliSeconds = timeOutSeconds * 1000;
  }

  public void open() {
    logger.info("RestSinkConnWrapper opened!");
  }

  /**
   * 输出数据.
   */
  public void outputData(JSONArray dataList) throws Exception {
    CloseableHttpClient httpclient = null;
    CloseableHttpResponse response = null;
    long begin = System.currentTimeMillis();
    try {
      httpclient = HttpClients.createDefault();
      response = invokeHttpPut(httpclient, dataList);

      HttpEntity resEntity = response.getEntity();
      String resBody = EntityUtils.toString(resEntity, "UTF-8");

      if (response.getStatusLine().getStatusCode() == 200) {
        logger.info("IDF sink success! url = {}, dataList.size = {}", url, dataList.size());

      } else {
        throw new SinkWriteDataException(
            String.format("IDF Sink error : url = %s , received resBody = %s, send dataList = %s ",
                url, resBody, dataList));
      }
    } catch (Exception e) {
      long cost = System.currentTimeMillis() - begin;
      String errorMsg = String.format("Error in request under rest sink node："
          + "url = %s, timeOutSeconds = %s ms, cost = %s ms, error = %s",
          url, timeOutMilliSeconds, cost, e);
      throw new SinkWriteDataException(errorMsg);
    } finally {
      if (response != null) {
        response.close();
      }
      if (httpclient != null) {
        httpclient.close();
      }
    }
  }

  /**
   * rest HttpPut.
   */
  private CloseableHttpResponse invokeHttpPut(CloseableHttpClient httpclient,
                                              JSONArray data) throws Exception {
    HttpPut httpPut = new HttpPut(url);
    // 设置超时时长.
    RequestConfig requestConfig = RequestConfig.custom()
        .setConnectTimeout(timeOutMilliSeconds)
        .setConnectionRequestTimeout(timeOutMilliSeconds)
        .setSocketTimeout(timeOutMilliSeconds)
        .build();
    httpPut.setConfig(requestConfig);

    for (int i = 0; i < headers.size(); i++) {
      httpPut.setHeader(headers.getJSONObject(i).getString("key"),
                      headers.getJSONObject(i).getString("value"));
    }
    StringEntity httpEntity = new StringEntity(data.toJSONString(), "UTF-8");
    httpEntity.setContentEncoding("UTF-8");
    httpEntity.setContentType("application/json");
    httpPut.setEntity(httpEntity);
    return httpclient.execute(httpPut);
  }

  @Override
  public void close() {
    logger.info("RestSinkConnWrapper closed!");
  }
}
