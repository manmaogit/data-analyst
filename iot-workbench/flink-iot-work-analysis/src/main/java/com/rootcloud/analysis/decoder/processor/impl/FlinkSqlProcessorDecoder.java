/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.decoder.processor.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.rootcloud.analysis.common.util.DateUtil;
import com.rootcloud.analysis.context.IotWorkRuntimeDecodeContext;
import com.rootcloud.analysis.core.constants.JobConstant;
import com.rootcloud.analysis.core.constants.SchemaConstant;
import com.rootcloud.analysis.core.enums.ProcessTypeEnum;
import com.rootcloud.analysis.core.enums.TimeZoneTypeEnum;
import com.rootcloud.analysis.decoder.processor.AbstractProcessorDecoder;
import com.rootcloud.analysis.decoder.processor.ProcessorDecoder;
import com.rootcloud.analysis.exception.DecodeProcessorException;
import com.rootcloud.analysis.wrapper.processor.AbstractProcessorWrapper;
import com.rootcloud.analysis.wrapper.processor.FlinkSqlProcessorWrapper;
import java.text.SimpleDateFormat;
import java.time.OffsetDateTime;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Please use FlinkSqlBatchProcessorDecoder instead.
 */
@Deprecated
@ProcessorDecoder(processType = ProcessTypeEnum.FLINK_SQL)
public class FlinkSqlProcessorDecoder extends AbstractProcessorDecoder {

  private static Logger logger = LoggerFactory.getLogger(
      FlinkSqlProcessorDecoder.class);

  @Override
  public AbstractProcessorWrapper decodeProcessParams(JSONObject conf) {

    try {
      FlinkSqlProcessorWrapper.Builder builder = new FlinkSqlProcessorWrapper.Builder();

      builder.setSchema(conf.getJSONArray(JobConstant.KEY_SCHEMA))
          .setSql(buildColumnValueTruncatedSql(conf.getString(JobConstant.KEY_SQL)
                  .replace(JobConstant.SYSTEM_TIME_ZONE, "'" + IotWorkRuntimeDecodeContext.getTimezone() + "'")
                  .replace(JobConstant.SYSTEM_ZONE_ID,
                      "'" + TimeZoneTypeEnum
                          .valueOf(IotWorkRuntimeDecodeContext.getTimezone())
                          .getTimeZoneValue()
                          + "'")
                  .replaceAll(JobConstant.SYSTEM_DATETIME_FUNC_KEY_REG, "'" + DateUtil
                      .formatTime(DateUtil.getEpochSecond(IotWorkRuntimeDecodeContext.getSystemDatetime(),
                              "yyyyMMddHHmmss", OffsetDateTime.now().getOffset().getId()) * 1000,
                          TimeZoneTypeEnum.valueOf(IotWorkRuntimeDecodeContext.getTimezone()).getZoneId(),
                          "yyyy-MM-dd HH:mm:ss") + "'")
                  .replaceAll(JobConstant.SYS_DATE_FUNC_KEY_REG,
                      "'" + new SimpleDateFormat("yyyy-MM-dd").format(new Date()) + "'")
                  .replaceAll(JobConstant.SYSTEM_BIZ_START_DATETIME_FUNC_KEY_REG,
                      "'" + IotWorkRuntimeDecodeContext.getTimeScope().getStartTime() + "'")
                  .replaceAll(JobConstant.SYSTEM_BIZ_END_DATETIME_FUNC_KEY_REG,
                      "'" + IotWorkRuntimeDecodeContext.getTimeScope().getEndTime() + "'")
                  .replaceAll(JobConstant.SYSTEM_DATETIME, "'" + DateUtil
                      .formatTime(DateUtil.getEpochSecond(IotWorkRuntimeDecodeContext.getSystemDatetime(),
                              "yyyyMMddHHmmss", OffsetDateTime.now().getOffset().getId()) * 1000,
                          TimeZoneTypeEnum.valueOf(IotWorkRuntimeDecodeContext.getTimezone()).getZoneId(),
                          "yyyy-MM-dd HH:mm:ss") + "'")
                  .replaceAll(JobConstant.SYS_DATE,
                      "'" + new SimpleDateFormat("yyyy-MM-dd").format(new Date()) + "'")
                  .replaceAll(JobConstant.SYSTEM_BIZ_START_DATETIME,
                      "'" + IotWorkRuntimeDecodeContext.getTimeScope().getStartTime() + "'")
                  .replaceAll(JobConstant.SYSTEM_BIZ_END_DATETIME,
                      "'" + IotWorkRuntimeDecodeContext.getTimeScope().getEndTime() + "'"),
              conf.getJSONArray(JobConstant.KEY_SCHEMA))
          ).setPriorNodeIds(conf.getJSONArray(JobConstant.KEY_PRIOR_NODE_IDS));
      FlinkSqlProcessorWrapper result = builder.build();
      return result;
    } catch (Exception ex) {
      logger.error("Exception occurred when decoding Flink Sql processor: {}", conf, ex);
      throw new DecodeProcessorException();
    }
  }

  private String buildColumnValueTruncatedSql(String inputSql, JSONArray outputSchema) {
    StringBuilder sb = new StringBuilder("select ");
    for (Object o : outputSchema) {
      JSONObject jsonObject = (JSONObject) o;
      String column = jsonObject.getString(SchemaConstant.ATTR_NAME);
      String type = jsonObject.getString(SchemaConstant.ATTR_DATA_TYPE);

      String escapedColumn = "`" + column + "`";
      if ("DECIMAL".equals(type)) {
        Integer scale = jsonObject.getInteger(SchemaConstant.ATTR_SCALE);
        if (scale == null) {
          scale = JobConstant.DEFAULT_SCALE;
        }
        sb.append("TRUNCATE(").append(escapedColumn).append(", ").append(scale).append(")")
            .append(" as ")
            .append(escapedColumn);
      } else {
        sb.append(escapedColumn).append(" as ").append(escapedColumn);
      }

      sb.append(",");
    }
    sb.deleteCharAt(sb.length() - 1);
    sb.append(" from (").append(inputSql).append(")");
    return sb.toString();
  }

}
