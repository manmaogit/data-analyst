/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.entity;

import java.util.List;
import java.util.Map;

/**
 * 封装InfluxDB查询结果.
 *
 * @author: zexin.huang
 * @createdDate: 2022/6/9 20:21
 */
public class InfluxDbQueryResult {

  /**
   * 结果列表.
   */
  private List<InfluxDbQueryResult.Result> results;

  /**
   * 错误信息.
   */
  private String error;

  /**
   * 获取结果.
   */
  public List<InfluxDbQueryResult.Result> getResults() {
    return this.results;
  }

  /**
   * 设置结果.
   * @param results 结果.
   */
  public void setResults(final List<InfluxDbQueryResult.Result> results) {
    this.results = results;
  }

  /**
   * Checks if this QueryResult has an error message.
   *
   * @return <code>true</code> if there is an error message, <code>false</code> if not.
   */
  public boolean hasError() {
    return this.error != null;
  }

  /**
   * getError.
   */
  public String getError() {
    return this.error;
  }

  /**
   * setError.
   */
  public void setError(final String error) {
    this.error = error;
  }

  public static class Result {
    /**
     * series.
     */
    private List<InfluxDbQueryResult.Series> series;
    /**
     * error.
     */
    private String error;

    /**
     * getSeries.
     */
    public List<InfluxDbQueryResult.Series> getSeries() {
      return this.series;
    }

    /**
     * setSeries.
     */
    public void setSeries(final List<InfluxDbQueryResult.Series> series) {
      this.series = series;
    }

    /**
     * Checks if this Result has an error message.
     */
    public boolean hasError() {
      return this.error != null;
    }

    /**
     * getError.
     */
    public String getError() {
      return this.error;
    }

    /**
     * setError.
     */
    public void setError(final String error) {
      this.error = error;
    }

    /**
     * toString.
     */
    @Override
    public String toString() {
      StringBuilder builder = new StringBuilder();
      builder.append("Result [series=");
      builder.append(this.series);
      builder.append(", error=");
      builder.append(this.error);
      builder.append("]");
      return builder.toString();
    }

  }

  public static class Series {
    private String name;
    private Map<String, String> tags;
    private List<String> columns;
    private List<List<Object>> values;

    /**
     * getName.
     */
    public String getName() {
      return this.name;
    }

    /**
     * setName.
     * @param name
     *            the name to set
     */
    public void setName(final String name) {
      this.name = name;
    }

    /**
     * getTags.
     */
    public Map<String, String> getTags() {
      return this.tags;
    }

    /**
     * setTags.
     */
    public void setTags(final Map<String, String> tags) {
      this.tags = tags;
    }

    /**
     * getColumns.
     */
    public List<String> getColumns() {
      return this.columns;
    }

    /**
     * setColumns.
     * @param columns
     *            the columns to set
     */
    public void setColumns(final List<String> columns) {
      this.columns = columns;
    }

    /**
     * getValues.
     */
    public List<List<Object>> getValues() {
      return this.values;
    }

    /**
     * setValues.
     * @param values
     *            the values to set
     */
    public void setValues(final List<List<Object>> values) {
      this.values = values;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
      StringBuilder builder = new StringBuilder();
      builder.append("Series [name=");
      builder.append(this.name);
      builder.append(", tags=");
      builder.append(this.tags);
      builder.append(", columns=");
      builder.append(this.columns);
      builder.append(", values=");
      builder.append(this.values);
      builder.append("]");
      return builder.toString();
    }

  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("QueryResult [results=");
    builder.append(this.results);
    builder.append(", error=");
    builder.append(this.error);
    builder.append("]");
    return builder.toString();
  }

}
