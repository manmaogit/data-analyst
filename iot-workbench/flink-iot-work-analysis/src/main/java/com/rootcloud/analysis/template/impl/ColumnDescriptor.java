/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.template.impl;

import static com.rootcloud.analysis.core.enums.DataTemplateEnum.COLUMN;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Maps;
import com.rootcloud.analysis.template.IDescriptor;
import com.rootcloud.analysis.template.ServiceTemplate;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@ServiceTemplate(template = COLUMN)
public class ColumnDescriptor implements IDescriptor {

  private static Logger logger = LoggerFactory.getLogger(ColumnDescriptor.class);

  private Map<Object, Map<String, Object>> result = Maps.newHashMap();

  @Override
  public Map<Object, Map<String, Object>> parse(String content, String keyAttr) {
    JSONArray configJsonArray = JSONArray.parseArray(content);
    if (configJsonArray.isEmpty()) {
      logger.warn("content is empty.");
      return result;
    }

    for (int i = 0; i < configJsonArray.size(); i++) {
      JSONObject jsonObject = configJsonArray.getJSONObject(i);
      if (jsonObject.containsKey(keyAttr)) {
        Map<String, Object> data = Maps.newHashMap();
        result.put(jsonObject.get(keyAttr), data);
        for (String key : jsonObject.keySet()) {
          data.put(key, jsonObject.get(key));
        }
      } else {
        logger.error("{} is not found in {}.", keyAttr, jsonObject);
      }

    }
    return result;
  }

}
