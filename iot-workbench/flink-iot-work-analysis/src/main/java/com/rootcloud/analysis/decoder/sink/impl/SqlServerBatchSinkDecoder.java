/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.decoder.sink.impl;

import com.alibaba.fastjson.JSONObject;
import com.rootcloud.analysis.core.enums.SinkTypeEnum;
import com.rootcloud.analysis.decoder.sink.SinkDecoder;
import com.rootcloud.analysis.wrapper.sink.JdbcBatchSinkWrapper;
import com.rootcloud.analysis.wrapper.sink.SqlServerBatchSinkWrapper;

@SinkDecoder(sinkType = SinkTypeEnum.SQLSERVER_BATCH)
public class SqlServerBatchSinkDecoder extends JdbcBatchSinkDecoder {

  @Override
  protected JdbcBatchSinkWrapper decodeJdbcSinkParams(JSONObject conf) {
    return new SqlServerBatchSinkWrapper();
  }
}
