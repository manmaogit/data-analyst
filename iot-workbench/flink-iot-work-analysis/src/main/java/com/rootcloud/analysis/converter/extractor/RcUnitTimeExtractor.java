/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.converter.extractor;

import com.rootcloud.analysis.FlinkMain;
import com.rootcloud.analysis.common.entity.RcUnit;
import com.rootcloud.analysis.common.util.DateUtil;
import com.rootcloud.analysis.core.constants.GrayLogConstant;
import com.rootcloud.analysis.core.constants.JobConstant;
import com.rootcloud.analysis.core.constants.ShortMessageConstant;
import com.rootcloud.analysis.core.enums.GrayLogResultEnum;
import com.rootcloud.analysis.core.enums.GraylogLogTypeEnum;
import com.rootcloud.analysis.core.graylog.LogVo;
import com.rootcloud.analysis.entity.RcTimeAttr;
import com.rootcloud.analysis.log.loggingservice.LoggingServiceUtil;
import javax.annotation.Nullable;

import org.apache.flink.api.common.eventtime.SerializableTimestampAssigner;
import org.apache.flink.streaming.api.functions.AssignerWithPeriodicWatermarks;
import org.apache.flink.streaming.api.watermark.Watermark;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RcUnitTimeExtractor implements SerializableTimestampAssigner<RcUnit> {

  private static final Logger logger = LoggerFactory.getLogger(FlinkMain.class);

  private static final long S_TO_MS = 1000L;
  private long currentMaxTimestamp = 0L;

  private final RcTimeAttr timeAttr;
  private final String jobId;
  private final String jobName;
  private final String tenantId;
  private final GraylogLogTypeEnum logType;
  private final String kafkaLoggingServiceTopics;
  private final String kafkaLoggingServiceServers;

  /**
   * Constructor.
   */
  public RcUnitTimeExtractor(RcTimeAttr timeAttr, String jobId, String jobName, String tenantId, GraylogLogTypeEnum logType,
                             String kafkaLoggingServiceTopics, String kafkaLoggingServiceServers) {
    this.timeAttr = timeAttr;
    this.jobId = jobId;
    this.jobName = jobName;
    this.tenantId = tenantId;
    this.logType = logType;
    this.kafkaLoggingServiceTopics = kafkaLoggingServiceTopics;
    this.kafkaLoggingServiceServers = kafkaLoggingServiceServers;
  }

  @Override
  public long extractTimestamp(RcUnit element, long previousElementTimestamp) {

    logger.debug("time extractor process element: {}", element);
    try {

      //todo:临时修改
      Object time = element.getAttr(timeAttr.getKey());

      //todo:抽象成多个类进行处理
      if (timeAttr.getType().equalsIgnoreCase(RcTimeAttr.TIME_TYPE_STRING)) {
        currentMaxTimestamp = DateUtil.convertTimeToLongMs((String) time);
      } else {
        if (time instanceof Integer) {
          currentMaxTimestamp = Long.valueOf((Integer) time);
        } else {
          currentMaxTimestamp = (long) time;
        }

        if (RcTimeAttr.TIME_UNIT_S.equals(timeAttr.getFormat())) {
          currentMaxTimestamp *= S_TO_MS;
        }
      }
    } catch (Exception ex) {
      logger.error("Exception occurred when extracting timestamp: {}", ex.toString());
      LoggingServiceUtil.alarm(LogVo.builder()
          .k8sServiceName(GrayLogConstant.K8S_SERVICE_NAME)
          .module(GrayLogConstant.MODULE_NAME)
          .userName(tenantId)
          .tenantId(tenantId)
          .operateObjectName(jobName)
          .operateObject(jobId)
          .shortMessage(String.format("Failed to extract event time in the application %s."
              + " Cause: %s",jobName,ex.toString()))
          .operation(ShortMessageConstant.EXTRACT_TIMESTAMP)
          .requestId(String.valueOf(System.currentTimeMillis()))
          .result(GrayLogResultEnum.FAIL.getValue())
          .kafkaLoggingServiceTopic(kafkaLoggingServiceTopics)
          .kafkaLoggingServiceServers(kafkaLoggingServiceServers)
          .logType(logType)
          .build());
    }
    return currentMaxTimestamp;
  }
}
