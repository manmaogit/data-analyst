/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.exception;

public class DecodeSinkException extends RuntimeException {
  private String nodeId;
  private String conf;

  public DecodeSinkException(String nodeId,String conf) {
    this.nodeId = nodeId;
    this.conf = conf;
  }

  public String getNodeId() {
    return nodeId;
  }

  public String getConf() {
    return conf;
  }

  @Override
  public String toString() {
    return "DecodeSinkException{"
        + "sinkId='" + nodeId + '\''
        + ", sinkConf='" + conf + '\''
        + '}';
  }
}
