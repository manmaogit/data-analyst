/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.decoder.processor.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.rootcloud.analysis.core.constants.JobConstant;
import com.rootcloud.analysis.core.enums.ProcessTypeEnum;
import com.rootcloud.analysis.decoder.processor.AbstractProcessorDecoder;
import com.rootcloud.analysis.decoder.processor.ProcessorDecoder;
import com.rootcloud.analysis.exception.DecodeProcessorException;
import com.rootcloud.analysis.wrapper.function.join.AbstractJoinWrapper;
import com.rootcloud.analysis.wrapper.function.join.FullOuterJoinWrapper;
import com.rootcloud.analysis.wrapper.function.join.InnerJoinWrapper;
import com.rootcloud.analysis.wrapper.function.join.LeftOuterJoinWrapper;
import com.rootcloud.analysis.wrapper.function.join.RightOuterJoinWrapper;
import com.rootcloud.analysis.wrapper.processor.AbstractProcessorWrapper;
import com.rootcloud.analysis.wrapper.processor.ConnectProcessorWrapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@ProcessorDecoder(processType = ProcessTypeEnum.CONNECT)
public class ConnectProcessorDecoder extends AbstractProcessorDecoder {

  private static Logger logger = LoggerFactory.getLogger(
      ConnectProcessorDecoder.class);

  @Override
  public AbstractProcessorWrapper decodeProcessParams(JSONObject conf) {
    try {
      AbstractJoinWrapper abstractJoinWrapper = new LeftOuterJoinWrapper();
      if (conf.getString(JobConstant.JOIN_TYPE) == null) {
        abstractJoinWrapper = new InnerJoinWrapper();
      } else {
        switch (conf.getString(JobConstant.JOIN_TYPE)) {
          case "LEFT_OUTER_JOIN":
            abstractJoinWrapper = new LeftOuterJoinWrapper();
            break;
          case "FULL_OUTER_JOIN":
            abstractJoinWrapper = new FullOuterJoinWrapper();
            break;
          case "RIGHT_OUTER_JOIN":
            abstractJoinWrapper = new RightOuterJoinWrapper();
            break;
          case "INNER_JOIN":
          default:
            abstractJoinWrapper = new InnerJoinWrapper();
            break;
        }
      }
      ConnectProcessorWrapper.Builder builder = new ConnectProcessorWrapper.Builder();
      builder.setDimensionNodeId(conf.getString(JobConstant.KEY_DIMENSION_NODE_D))
          .setAttributeMapper(conf.getJSONObject(JobConstant.KEY_ATTRIBUTE_MAPPER))
          .setAttributeMapperList(
              JSONArray.parseArray(
                  JSON.toJSONString(conf.getJSONArray(JobConstant.KEY_ATTRIBUTE_MAPPER_LIST)),
                  JSONObject.class))
          .setSchema(conf.getJSONArray(JobConstant.KEY_SCHEMA))
          .setAbstractJoinWrapper(abstractJoinWrapper);
      ConnectProcessorWrapper result = builder.build();
      logger.debug("Decoding connect processor: {}", result);
      return result;
    } catch (Exception ex) {
      logger.error("Exception occurred when decoding connect processor: {}", conf);
      throw new DecodeProcessorException();
    }
  }
}
