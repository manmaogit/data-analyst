/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.wrapper.source;

import static com.rootcloud.analysis.core.constants.JobConstant.INFLUXDB_TIME_COLUMN_NAME;
import static com.rootcloud.analysis.core.constants.SchemaConstant.SINGLE_QUOTE;
import static com.rootcloud.analysis.core.constants.SchemaConstant.TIMESTAMP;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Maps;
import com.rootcloud.analysis.common.entity.DeviceMapping;
import com.rootcloud.analysis.common.entity.RcUnit;
import com.rootcloud.analysis.common.util.DateUtil;
import com.rootcloud.analysis.core.constants.CommonConstant;
import com.rootcloud.analysis.core.constants.SchemaConstant;
import com.rootcloud.analysis.core.enums.HistorianDataTypeEnum;
import com.rootcloud.analysis.core.enums.TimeZoneTypeEnum;
import com.rootcloud.analysis.entity.InfluxDbQueryResult;
import com.rootcloud.analysis.exception.SourceReadDataException;

import java.time.ZoneId;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import lombok.Getter;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Getter
public class InfluxDbDeviceConnWrapper extends InfluxDbDeviceConnBaseWrapper {

  private static final Logger logger = LoggerFactory.getLogger(InfluxDbDeviceConnWrapper.class);

  private final long timeLowerBound;
  private final long timeUpperBound;
  private long timeQueryOffset;
  private long timeScopeMilliSeconds;

  private Set<String> ignoredFieldNames;

  /**
   * InfluxDbConnWrapper.
   */
  public InfluxDbDeviceConnWrapper(String url, String username, String password, String database,
                                   String tenantId, JSONArray schema, String timezone,
                                   int timeOutSeconds, long timeLowerBound, long timeUpperBound,
                                   int queryScopeSeconds, Set<String> ignoredFieldNames,
                                   List<DeviceMapping> deviceMappings) {
    super(url, username, password, database, tenantId,
        timeOutSeconds, timezone, schema, deviceMappings);
    this.timeQueryOffset = timeLowerBound;
    this.timeLowerBound = timeLowerBound;
    this.timeUpperBound = timeUpperBound;
    this.timeScopeMilliSeconds = queryScopeSeconds * 1000;
    this.ignoredFieldNames = ignoredFieldNames;
  }

  @Override
  public Set<RcUnit> getData() throws Exception {
    Set<RcUnit> outPutSet = new HashSet<>();
    if (CollectionUtils.isEmpty(getDeviceMappings())) {
      return outPutSet;
    }
    CloseableHttpClient httpClient = null;
    try {
      httpClient = HttpClients.createDefault();
      for (DeviceMapping deviceMapping : getDeviceMappings()) {
        this.timeQueryOffset = timeLowerBound;
        while (timeQueryOffset < timeUpperBound) {
          String querySql = this.buildQuerySql(deviceMapping);
          List<InfluxDbQueryResult.Series> seriesList = getInfluxDbData(httpClient, querySql);
          Set<RcUnit> rcUnitResult = parseData2RcUnit(deviceMapping, seriesList);
          outPutSet.addAll(rcUnitResult);
        }
      }
    } catch (Exception ex) {
      logger.error("InfluxDbConnWrapper getData Exception : ", ex);
      throw new SourceReadDataException(
          "getData from " + getDatabase() + "failed：" + ex
              + "，URI：" + getUrl() + "，deviceMappings：" + getDeviceMappings()
              + "，timeLowerBound：" + timeLowerBound
              + "，timeUpperBound：" + timeUpperBound);
    } finally {
      if (httpClient != null) {
        httpClient.close();
      }
    }
    return outPutSet;
  }

  /**
   * 拼接查询SQL.
   */
  public String buildQuerySql(DeviceMapping deviceMapping) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("select ");
    // Key：原始字段名 Value：加了后缀的字段名.
    Map<String, String> columns = getQuerySqlColumns();
    if (columns.isEmpty()) {
      stringBuilder.append(" * ");
    } else {
      columns.forEach((key, value) -> {
        stringBuilder
            .append(SchemaConstant.DOUBLE_QUOTE).append(value).append(SchemaConstant.DOUBLE_QUOTE)
            .append(" as ")
            .append(SchemaConstant.DOUBLE_QUOTE).append(key).append(SchemaConstant.DOUBLE_QUOTE)
            .append(CommonConstant.COMMA);
      });
      stringBuilder.deleteCharAt(stringBuilder.length() - 1);
    }
    String table = "type_" + getTenantId() + "_" + deviceMapping.getDeviceTypeId();
    stringBuilder.append(" from ").append(table).append(" where ");
    // 设备ID过滤.
    List<String> deviceIds = deviceMapping.getDeviceIds();
    if (CollectionUtils.isNotEmpty(deviceIds)) {
      stringBuilder.append("(");
      for (int i = 0; i < deviceIds.size(); i++) {
        if (i > 0) {
          stringBuilder.append(" or ");
        }
        stringBuilder.append(SchemaConstant.DEVICE_ID).append(" = ")
            .append(SINGLE_QUOTE).append(deviceIds.get(i)).append(SINGLE_QUOTE);
      }
      stringBuilder.append(") and ");
    }
    // 时间戳过滤.
    stringBuilder.append(INFLUXDB_TIME_COLUMN_NAME).append(" >= '");
    ZoneId timezoneId = TimeZoneTypeEnum.valueOf(getTimezone()).getZoneId();
    String startTime = DateUtil.convertTimeToZoneDate(timeQueryOffset,
        getInfluxdbTimePattern(), timezoneId);
    stringBuilder.append(startTime).append("' and ");
    stringBuilder.append(INFLUXDB_TIME_COLUMN_NAME).append(" < '");
    long endTimeMills = timeQueryOffset + timeScopeMilliSeconds;
    if (endTimeMills > timeUpperBound) {
      timeQueryOffset = timeUpperBound;
    } else {
      timeQueryOffset = endTimeMills;
    }
    String endTime = DateUtil.convertTimeToZoneDate(timeQueryOffset,
        getInfluxdbTimePattern(), timezoneId);
    stringBuilder.append(endTime).append("'");
    return stringBuilder.toString();
  }

  /**
   * 解析InfluxDB数据，转换为RcUnit.
   */
  private Set<RcUnit> parseData2RcUnit(DeviceMapping deviceMapping,
                                       final List<InfluxDbQueryResult.Series> seriesList) {
    Set<RcUnit> outPutSet = new HashSet<>();
    for (InfluxDbQueryResult.Series series : seriesList) {
      List<String> columns = series.getColumns();
      Iterator<List<Object>> it = series.getValues().iterator();
      while (it.hasNext()) {
        List<Object> valueList = it.next();
        if (columns.size() != valueList.size()) {
          throw new SourceReadDataException(
              "parseData2RcUnit fail：model（" + deviceMapping.getDeviceTypeId()
                  + "）Data exception! There may be a wrong column. Please replace the"
                  + " model or contact the administrator to verify the influxdb partition data。"
                  + "，columns.size()=" + columns.size()
                  + "，valueList.size()=" + valueList.size());
        }
        Map<String, Object> data = Maps.newHashMap();
        for (int i = 0; i < valueList.size(); i++) {
          checkColumnData(columns.get(i), valueList.get(i), data);
        }
        RcUnit result = RcUnit.valueOfDataTypeInfo(data, getSchema());
        // 补全内置字段的值.
        fillDefaultFieldValue(result, deviceMapping, null);
        outPutSet.add(result);
      }
    }
    return outPutSet;
  }

  /**
   * 获取SQL使用的查询字段集合.
   * @return
   */
  private Map<String, String> getQuerySqlColumns() {
    // Key：原始字段名 Value：加了后缀的字段名.
    Map<String, String> columns = new HashMap<>();
    for (Object obj : getSchema()) {
      JSONObject jsonObj = (JSONObject) obj;
      String outputColumnName = jsonObj.getString(SchemaConstant.ATTR_ORIGINAL_NAME);
      String queryColumnName = outputColumnName;
      String dataType = jsonObj.getString(SchemaConstant.ATTR_OLD_DATA_TYPE);
      if (ignoredFieldNames.contains(outputColumnName)) {
        if (StringUtils.equals(outputColumnName, TIMESTAMP)) {
          continue;
        } else {
          columns.put(outputColumnName, queryColumnName);
        }
      } else {
        // ECA InfluxDB会对字段KEY额外进行类型的后缀拼接.
        // 特殊：DECIMAL类型且精度为0，在InfluxDB里会按后缀$i保存
        if (StringUtils.equals("DECIMAL", dataType)) {
          Integer scale = jsonObj.getInteger(SchemaConstant.ATTR_SCALE);
          if (scale != null && scale == 0) {
            dataType = "INT";
          }
        }
        if (StringUtils.equals(HistorianDataTypeEnum.Json.getFlinkType(), dataType)
            && outputColumnName.contains(SchemaConstant.DOT)) {
          String[] split = outputColumnName.split("\\.");
          if (outputColumnName.startsWith("__location__.")
              || outputColumnName.startsWith("__online__.")) {
            outputColumnName = split[1];
            dataType = jsonObj.getString(SchemaConstant.ATTR_DATA_TYPE);

          } else {
            outputColumnName = queryColumnName = split[0];
          }
        }
        String influxDbDataType = HistorianDataTypeEnum.getHistorianType(dataType);
        columns.put(outputColumnName, queryColumnName + "$" + influxDbDataType);
      }
    }
    return columns;
  }

}
