/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.wrapper.source;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.rootcloud.analysis.common.context.IotWorkRuntimeContext;
import com.rootcloud.analysis.common.entity.RcUnit;
import com.rootcloud.analysis.converter.stream.table.RcUnitToRowConverter;
import com.rootcloud.analysis.core.constants.SchemaConstant;
import com.rootcloud.analysis.core.enums.GraylogLogTypeEnum;
import java.util.ArrayList;
import java.util.List;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.table.api.bridge.java.StreamTableEnvironment;
import org.apache.flink.types.Row;

@Getter
@Setter
@NoArgsConstructor
@ToString(callSuper = true)
public abstract class AbstractBatchSourceWrapper extends AbstractSourceWrapper {

  protected AbstractBatchSourceWrapper(String jobId, String jobName,
                                       String tenantId, String nodeId,
                                       Integer parallelism, String nodeName, String sourceType,
                                       JSONArray schema, GraylogLogTypeEnum logType) {
    super(jobId, jobName, tenantId, nodeId, parallelism, nodeName, sourceType, schema, logType);
  }

  protected void createTemporaryViewForRow(IotWorkRuntimeContext context,
                                           DataStream<RcUnit> sourceDataStream) throws Exception {
    List<String> fieldNames = new ArrayList<>(getSchema().size());
    List<String> fieldTypes = new ArrayList<>(getSchema().size());
    List<Integer> valueTypeScales = new ArrayList<>(getSchema().size());
    for (int i = 0; i < getSchema().size(); i++) {
      JSONObject jsonObject = (JSONObject) getSchema().get(i);
      fieldNames.add(jsonObject.getString(SchemaConstant.ATTR_NAME));
      fieldTypes.add(jsonObject.getString(SchemaConstant.ATTR_DATA_TYPE));
      valueTypeScales.add(jsonObject.getInteger(SchemaConstant.ATTR_SCALE));
    }
    RcUnitToRowConverter converter = new RcUnitToRowConverter(getNodeId(), fieldNames,
        fieldTypes, valueTypeScales);
    DataStream<Row> rowDataStream = converter.convert(sourceDataStream);
    StreamTableEnvironment tableEnv = context.getTableEnvFromCache();
    tableEnv.createTemporaryView(getNodeId(), rowDataStream);
  }
}
