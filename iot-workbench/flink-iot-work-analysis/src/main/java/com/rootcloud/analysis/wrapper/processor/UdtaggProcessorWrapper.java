/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.wrapper.processor;

import com.rootcloud.analysis.common.context.IotWorkRuntimeContext;
import lombok.Getter;
import lombok.ToString;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Getter
@ToString(callSuper = true)
public class UdtaggProcessorWrapper  extends AbstractProcessorWrapper {
  private static final Logger LOGGER = LoggerFactory.getLogger(UdtaggProcessorWrapper.class);


  @Override
  public void registerProcess(IotWorkRuntimeContext context) throws Exception {

  }
}
