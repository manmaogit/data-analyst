/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.wrapper.api;

import com.rootcloud.analysis.common.factory.KafkaLogMiddlewareResourceFactory;
import java.util.concurrent.TimeUnit;
import org.apache.flink.api.common.functions.AbstractRichFunction;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.functions.sink.SinkFunction;
import org.apache.flink.streaming.connectors.kafka.internals.FlinkKafkaInternalProducer;

/**
 * IDF输出节点.
 *
 * @author: zexin.huang
 * @createdDate: 2022/6/6 16:13
 */
public abstract class RestSinkRichFunction<T>
    extends AbstractRichFunction implements SinkFunction<T> {

  private static final long serialVersionUID = 1L;
  public RestSinkConnWrapper restSinkConnWrapper;

  /**
   * 构造方法.
   */
  public RestSinkRichFunction(RestSinkConnWrapper restSinkConnWrapper) {
    this.restSinkConnWrapper = restSinkConnWrapper;
  }

  /**
   * 获取connection对象.
   */
  public RestSinkConnWrapper getRestSinkConnWrapper() {
    return restSinkConnWrapper;
  }


  @Override
  public void open(Configuration parameters) throws Exception {
    restSinkConnWrapper.open();
  }

  @Override
  public void close() throws Exception {
    restSinkConnWrapper.close();
    restSinkConnWrapper = null;
  }
}
