/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.wrapper.source;

import com.alibaba.fastjson.JSONArray;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.model.Filters;
import com.rootcloud.analysis.common.context.IotWorkRuntimeContext;
import com.rootcloud.analysis.common.factory.KafkaLogMiddlewareResourceFactory;
import com.rootcloud.analysis.common.util.DateUtil;
import com.rootcloud.analysis.context.IotWorkRuntimeDecodeContext;
import com.rootcloud.analysis.core.constants.GrayLogConstant;
import com.rootcloud.analysis.core.constants.ShortMessageConstant;
import com.rootcloud.analysis.core.enums.GrayLogResultEnum;
import com.rootcloud.analysis.core.graylog.LogVo;
import com.rootcloud.analysis.core.mongodb.CryptoRcMongoTemplate;
import com.rootcloud.analysis.core.mongodb.RcMongoTemplate;
import com.rootcloud.analysis.log.loggingservice.LoggingServiceUtil;
import com.rootcloud.analysis.wrapper.WrapperBuilder;
import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.source.RichParallelSourceFunction;
import org.apache.flink.streaming.api.functions.source.SourceFunction;
import org.apache.flink.streaming.connectors.kafka.internals.FlinkKafkaInternalProducer;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Getter
@Deprecated
public class MongoSourceWrapper extends AbstractSourceWrapper {

  private static final Logger logger = LoggerFactory.getLogger(MongoSourceWrapper.class);

  private static final String DETECT_TYPE_VERSION = "version";
  private static final String DETECT_TYPE_UPDATETIME = "updatetime";

  private final String uri;
  private final String database;
  private final String collection;

  private final String detectType;
  private final String detectKey;
  private final Integer detectPeriod;

  private MongoSourceWrapper(Builder builder) {
    super(builder.jobId, builder.jobName, builder.tenantId, builder.nodeId, builder.parallelism,
        builder.nodeName, builder.inputType, builder.schema, builder.logType);
    this.uri = builder.uri;
    this.database = builder.database;
    this.collection = builder.collection;
    this.detectType = builder.detectType;
    this.detectKey = builder.detectKey;
    this.detectPeriod = builder.detectPeriod;
  }

  public static class Builder extends WrapperBuilder {

    private String nodeId;
    private String nodeName;
    private Integer parallelism;
    private String inputType;
    private JSONArray schema;

    private String uri;
    private String database;
    private String collection;

    private String detectType;
    private String detectKey;
    private Integer detectPeriod;

    public Builder setUri(String uri) {
      this.uri = uri;
      return this;
    }

    public Builder setDatabase(String database) {
      this.database = database;
      return this;
    }

    public Builder setCollection(String collection) {
      this.collection = collection;
      return this;
    }

    public Builder setDetectType(String detectType) {
      this.detectType = detectType;
      return this;
    }

    public Builder setDetectKey(String detectKey) {
      this.detectKey = detectKey;
      return this;
    }

    public Builder setDetectPeriod(Integer detectPeriod) {
      this.detectPeriod = detectPeriod;
      return this;
    }

    public Builder setNodeId(String nodeId) {
      this.nodeId = nodeId;
      return this;
    }

    public Builder setNodeName(String nodeName) {
      this.nodeName = nodeName;
      return this;
    }

    public Builder setParallelism(Integer parallelism) {
      this.parallelism = parallelism;
      return this;
    }

    public Builder setInputType(String inputType) {
      this.inputType = inputType;
      return this;
    }

    public Builder setSchema(JSONArray schema) {
      this.schema = schema;
      return this;
    }

    public MongoSourceWrapper build() {
      return new MongoSourceWrapper(this);
    }

  }

  @Override
  public void registerInput(IotWorkRuntimeContext context) {

    SourceFunction sourceFunction = new RichParallelSourceFunction<String>() {

      private volatile boolean isRun = Boolean.TRUE;
      private String version = "";
      private long updateTime = DateUtil.convertTimeToLongMs("1970-01-01 00:00:00.000");
      private RcMongoTemplate rcMongoTemplate;

      private String kafkaLoggingServiceServers;
      private String kafkaLoggingServiceTopics;
      private FlinkKafkaInternalProducer flinkKafkaInternalProducer;
      @Override
      public void open(Configuration parameters) throws Exception {
        super.open(parameters);
        rcMongoTemplate = new CryptoRcMongoTemplate(uri, IotWorkRuntimeDecodeContext.getPrivateKey());
        this.kafkaLoggingServiceServers = IotWorkRuntimeDecodeContext.getLoggingServiceKafkaServers();
        this.kafkaLoggingServiceTopics = IotWorkRuntimeDecodeContext.getLoggingServiceKafkaTopic();
        if (flinkKafkaInternalProducer == null && kafkaLoggingServiceServers != null) {
          flinkKafkaInternalProducer = KafkaLogMiddlewareResourceFactory.getProducer(
              kafkaLoggingServiceServers,
              this.toString());
        }

      }

      @Override
      public void run(SourceContext<String> ctx) throws Exception {
        while (isRun && !Thread.currentThread().isInterrupted()) {
          try {
            //todo:抽象成多个类实现
            if (detectType.equals(DETECT_TYPE_UPDATETIME)) {
              Bson filter = Filters.gt(detectKey, updateTime);
              FindIterable<Document> findIterable = rcMongoTemplate
                  .find(database, collection, filter, 0, 1);
              MongoCursor<Document> mongoCursor = findIterable.iterator();

              if (mongoCursor.hasNext()) {
                mongoCursor = rcMongoTemplate.find(database, collection, filter).iterator();

                List<Document> conf = new ArrayList<>();
                while (mongoCursor.hasNext()) {
                  Document document = mongoCursor.next();
                  conf.add(document);

                  long uptime = (long) document.get(detectKey);
                  if (uptime > updateTime) {
                    updateTime = uptime;
                  }
                }

                ctx.collect(JSONArray.toJSON(conf).toString());
              }
            } else {
              FindIterable<Document> findIterable = rcMongoTemplate.find(database, collection)
                  .limit(1);
              MongoCursor<Document> mongoCursor = findIterable.iterator();

              while (mongoCursor.hasNext()) {
                Document document = mongoCursor.next();

                if (isNeedUpdate(document)) {
                  logger.debug("consume from mongo: {}", document.toJson());
                  ctx.collect(document.toJson());
                }
              }
            }
          } catch (Exception ex) {
            LoggingServiceUtil.alarm(LogVo.builder()
                .k8sServiceName(GrayLogConstant.K8S_SERVICE_NAME)
                .module(GrayLogConstant.MODULE_NAME)
                .userName(getTenantId())
                .tenantId(getTenantId())
                .operateObjectName(getJobName())
                .operateObject(getJobId())
                .shortMessage(String.format("Exception occurred in node %s when consuming message"
                    + " from MongoDB. Detail: %s",getNodeName(), ex.toString()))
                .operation(ShortMessageConstant.READ_MONGO_DATA)
                .requestId(String.valueOf(System.currentTimeMillis()))
                .result(GrayLogResultEnum.FAIL.getValue())
                .logType(getLogType())
                .kafkaLoggingServiceTopic(kafkaLoggingServiceTopics)
                .kafkaLoggingServiceServers(kafkaLoggingServiceServers)
                .build());
            logger.error(ex.toString());
          }
          try {
            Thread.sleep(detectPeriod * 1000);
          } catch (InterruptedException ex) {
            logger.warn("Thread is interrupted.");
            break;
          } catch (NullPointerException np) {
            logger.error("detectPeriod is null.");
          } catch (Exception e) {
            logger.error(e.toString());
          }
        }
      }

      @Override
      public void cancel() {
        isRun = false;
      }

      @Override
      public void close() throws Exception {
        super.close();
        rcMongoTemplate.close();
        KafkaLogMiddlewareResourceFactory.close(this.toString());
        flinkKafkaInternalProducer = null;
      }

      private Boolean isNeedUpdate(Document document) {
        if (!document.containsKey(detectKey)) {
          return Boolean.TRUE;
        }

        if (!version.equals(document.getString(detectKey))) {
          version = document.getString(detectKey);
          return Boolean.TRUE;
        }

        return Boolean.FALSE;
      }
    };

    StreamExecutionEnvironment env = context.getExecutionEnv();
    DataStreamSource<String> streamSource = env.addSource(sourceFunction).setParallelism(getParallelism());
    context.putObjectToContainer(getNodeId(), streamSource);
  }
}
