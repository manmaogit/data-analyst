/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.extent;

import com.rootcloud.analysis.common.factory.KafkaLogMiddlewareResourceFactory;
import com.rootcloud.analysis.core.constants.GrayLogConstant;
import com.rootcloud.analysis.core.constants.JobConstant;
import com.rootcloud.analysis.core.constants.ShortMessageConstant;
import com.rootcloud.analysis.core.enums.GrayLogResultEnum;
import com.rootcloud.analysis.core.enums.GraylogLogTypeEnum;
import com.rootcloud.analysis.core.graylog.LogVo;
import com.rootcloud.analysis.log.loggingservice.LoggingServiceUtil;
import java.util.Properties;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.metrics.Counter;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaException;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaProducer;
import org.apache.flink.streaming.connectors.kafka.KafkaSerializationSchema;
import org.apache.flink.streaming.connectors.kafka.internals.FlinkKafkaInternalProducer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 继承Kafka-Producer.
 */
public class FlinkKafkaExtendProducer<T> extends FlinkKafkaProducer<T> {

  private final String tenantId;
  private final String userId;
  private final String jobId;
  private final String jobName;
  private final String nodeName;
  private final String brokers;
  private String kafkaLoggingServiceTopics;
  private String kafkaLoggingServiceServers;
  private Counter counterRecordsOut;
  private FlinkKafkaInternalProducer flinkKafkaInternalProducer;

  private static Logger logger = LoggerFactory.getLogger(FlinkKafkaExtendProducer.class);

  /**
   * 构造方法直接调用父类方法.
   */
  public FlinkKafkaExtendProducer(String defaultTopic,
                                  KafkaSerializationSchema<T> serializationSchema,
                                  Properties producerConfig,
                                  Semantic semantic, String tenantId, String userId,
                                  String jobId,
                                  String jobName, String nodeName, String brokers,
                                  String kafkaLoggingServiceTopics,
                                  String kafkaLoggingServiceServers) {
    super(
        defaultTopic,
        serializationSchema,
        producerConfig,
        semantic,
        DEFAULT_KAFKA_PRODUCERS_POOL_SIZE);
    this.tenantId = tenantId;
    this.userId = userId;
    this.jobId = jobId;
    this.jobName = jobName;
    this.nodeName = nodeName;
    this.brokers = brokers;
    this.kafkaLoggingServiceServers = kafkaLoggingServiceServers;
    this.kafkaLoggingServiceTopics = kafkaLoggingServiceTopics;
  }

  @Override
  public void open(Configuration configuration) throws Exception {
    try {
      super.open(configuration);
      if (flinkKafkaInternalProducer == null && kafkaLoggingServiceServers != null) {
        flinkKafkaInternalProducer = KafkaLogMiddlewareResourceFactory.getProducer(
            kafkaLoggingServiceServers,
            this.toString());
      }
    } catch (Exception e) {
      LoggingServiceUtil.error(LogVo.builder()
          .k8sServiceName(GrayLogConstant.K8S_SERVICE_NAME)
          .module(GrayLogConstant.MODULE_NAME)
          .userName(tenantId)
          .userId(userId)
          .tenantId(tenantId)
          .operateObjectName(jobName)
          .operateObject(jobId)
          .shortMessage(String.format("Failed to connect to the Kafka cluster. Node: %s, "
              + "Kafka brokers: %s. Cause: %s",nodeName,brokers,e.toString()))
          .operation(ShortMessageConstant.READ_KAFKA_DATA)
          .requestId(String.valueOf(System.currentTimeMillis()))
          .result(GrayLogResultEnum.FAIL.getValue())
          .logType(GraylogLogTypeEnum.REALTIME)
          .kafkaLoggingServiceTopic(kafkaLoggingServiceTopics)
          .kafkaLoggingServiceServers(kafkaLoggingServiceServers)
          .build(), flinkKafkaInternalProducer);
    }
    counterRecordsOut = getRuntimeContext().getMetricGroup()
        .counter(JobConstant.FLINK_METRIC_NUM_RECORDS_OUT_OVERWRITE);
  }

  @Override
  public void invoke(
      KafkaTransactionState transaction, T next, Context context)
      throws FlinkKafkaException {
    try {
      super.invoke(transaction, next, context);

    } catch (Exception e) {
      LoggingServiceUtil.error(LogVo.builder()
          .k8sServiceName(GrayLogConstant.K8S_SERVICE_NAME)
          .module(GrayLogConstant.MODULE_NAME)
          .userName(tenantId)
          .userId(userId)
          .tenantId(tenantId)
          .operateObjectName(jobName)
          .operateObject(jobId)
          .shortMessage(String.format("Network problems with the Kafka cluster(%s)."
              + " Cause: %s",brokers,e.toString()))
          .operation(ShortMessageConstant.READ_KAFKA_DATA)
          .requestId(String.valueOf(System.currentTimeMillis()))
          .result(GrayLogResultEnum.FAIL.getValue())
          .logType(GraylogLogTypeEnum.REALTIME)
          .kafkaLoggingServiceTopic(kafkaLoggingServiceTopics)
          .kafkaLoggingServiceServers(kafkaLoggingServiceServers)
          .build(), flinkKafkaInternalProducer);
    }
    counterRecordsOut.inc();
  }

  @Override
  public void close() throws FlinkKafkaException {
    try {
      super.close();
      KafkaLogMiddlewareResourceFactory.close(this.toString());
      flinkKafkaInternalProducer = null;
    } catch (Exception e) {
      logger.error("FlinkKafkaExtendProducer-close exception:", e);
    }
  }
}
