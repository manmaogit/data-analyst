/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.wrapper.function.join;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.rootcloud.analysis.common.entity.RcUnit;
import java.io.Serializable;
import java.util.List;
import java.util.Set;
import org.apache.flink.runtime.state.HeapBroadcastState;

public abstract class AbstractJoinWrapper implements Serializable {

  /**
   * 聚合维表数据输出.
   */
  public abstract List<RcUnit> join(HeapBroadcastState<Object, List<RcUnit>> dimensionState,
                                    RcUnit input, JSONObject attributeMapper,
                                    Set<String> dimensionAttrSet,
                                    JSONArray schema, Object value);

  /**
   * 维表是否匹配工况数据.
   */
  public abstract Boolean contain(HeapBroadcastState<Object, List<RcUnit>> dimensionState,
                                  Object value, JSONObject attributeMapper);

}
