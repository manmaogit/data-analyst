/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.wrapper.function.join;

import static com.rootcloud.analysis.core.constants.JobConstant.KEY_SOURCE_ATTRIBUTE;
import static com.rootcloud.analysis.core.constants.SchemaConstant.ATTR_NAME;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.rootcloud.analysis.common.entity.RcUnit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import org.apache.flink.runtime.state.HeapBroadcastState;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LeftOuterJoinWrapper extends AbstractJoinWrapper {

  private static final Logger logger = LoggerFactory.getLogger(LeftOuterJoinWrapper.class);

  @Override
  public List<RcUnit> join(HeapBroadcastState<Object, List<RcUnit>> dimensionState, RcUnit input,
                           JSONObject attributeMapper,
                           Set<String> dimensionAttrSet,
                           JSONArray schema, Object value) {
    List<RcUnit> resultList = new ArrayList<>();
    if (dimensionState.contains(value)) {
      try {
        for (RcUnit dimension : dimensionState
            .get(value)) {
          for (String attrName : dimensionAttrSet) {
            input.setAttr(attrName, dimension.getAttr(attrName));
          }
          RcUnit result = RcUnit.valueOf(input.getData(),schema);
          resultList.add(result);
        }
      } catch (Exception e) {
        logger.error(e.toString());
      }
    } else {
      resultList.add(RcUnit.valueOf(input.getData(),schema));
    }
    return resultList;
  }

  @Override
  public Boolean contain(HeapBroadcastState<Object, List<RcUnit>> dimensionState, Object value,
                         JSONObject attributeMapper) {
    return true;
  }
}
