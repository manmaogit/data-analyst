/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.decoder.sink.impl;

import static com.rootcloud.analysis.core.constants.WrapperConstant.DEFAULT_START_TIME;
import static com.rootcloud.analysis.core.constants.WrapperConstant.DEFAULT_ZONE_ID;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Maps;
import com.rootcloud.analysis.context.IotWorkRuntimeDecodeContext;
import com.rootcloud.analysis.core.constants.JobConstant;
import com.rootcloud.analysis.core.constants.SchemaConstant;
import com.rootcloud.analysis.core.enums.SinkTypeEnum;
import com.rootcloud.analysis.core.enums.TimeZoneTypeEnum;
import com.rootcloud.analysis.core.enums.UpdateStrategyEnum;
import com.rootcloud.analysis.core.util.RsaUtil;
import com.rootcloud.analysis.decoder.sink.AbstractSinkDecoder;
import com.rootcloud.analysis.decoder.sink.SinkDecoder;
import com.rootcloud.analysis.entity.RcTimeAttr;
import com.rootcloud.analysis.exception.DecodeSinkException;
import com.rootcloud.analysis.wrapper.sink.AbstractSinkWrapper;
import com.rootcloud.analysis.wrapper.sink.PostgreSqlSinkWapper;
import java.util.List;
import java.util.Map;
import org.apache.commons.compress.utils.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SinkDecoder(sinkType = SinkTypeEnum.POSTGRESQL)
public class PostgreSqlSinkDecoder extends AbstractSinkDecoder {

  private static Logger logger = LoggerFactory.getLogger(PostgreSqlSinkDecoder.class);

  @Override
  public AbstractSinkWrapper decodeSinkParams(JSONObject conf) {
    try {
      PostgreSqlSinkWapper.Builder builder = new PostgreSqlSinkWapper.Builder();
      JSONObject params = conf.getJSONObject(JobConstant.KEY_PARAMETERS);
      JSONObject outputMapper = conf.getJSONObject(JobConstant.KEY_OUTPUT_MAPPER);

      JSONArray attributeMapperArr = outputMapper.getJSONArray(JobConstant.KEY_ATTRIBUTE_MAPPER);
      List<String> schemas = Lists.newArrayList();
      List<String> accAttrs = Lists.newArrayList();
      List<String> replaceAttrs = Lists.newArrayList();
      List<String> replaceWithNonNullAttrs = Lists.newArrayList();
      List<String> greatestAttrs = Lists.newArrayList();
      List<String> leastAttrs = Lists.newArrayList();
      Map<String, String> attributeMapper = Maps.newHashMap();

      for (Object o : attributeMapperArr) {
        JSONObject targetAttribute = ((JSONObject) o)
            .getJSONObject(JobConstant.KEY_TARGET_ATTRIBUTE);
        JSONObject sourceAttribute = ((JSONObject) o)
            .getJSONObject(JobConstant.KEY_SOURCE_ATTRIBUTE);

        attributeMapper.put(SchemaConstant.DOUBLE_QUOTE
                + targetAttribute.getString(SchemaConstant.ATTR_NAME)
                + SchemaConstant.DOUBLE_QUOTE,
            sourceAttribute.getString(SchemaConstant.ATTR_NAME));

        schemas.add(SchemaConstant.DOUBLE_QUOTE
            + targetAttribute.getString(SchemaConstant.ATTR_NAME)
            + SchemaConstant.DOUBLE_QUOTE + SchemaConstant.COLUMN_SEPARATOR
            + targetAttribute
            .getString(SchemaConstant.ATTR_DATA_TYPE));

        String updateStrategy = ((JSONObject) o).getString(JobConstant.KEY_UPDATE_STRATEGY);
        String attrName = SchemaConstant.DOUBLE_QUOTE
            + targetAttribute.getString(SchemaConstant.ATTR_NAME)
            + SchemaConstant.DOUBLE_QUOTE;
        if (UpdateStrategyEnum.ACCUMULATE.name().equals(updateStrategy)) {
          accAttrs.add(attrName);
        } else if (UpdateStrategyEnum.REPLACE.name().equals(updateStrategy)) {
          replaceAttrs.add(attrName);
        } else if (UpdateStrategyEnum.REPLACE_WITH_NON_NULL.name().equals(updateStrategy)) {
          replaceWithNonNullAttrs.add(attrName);
        } else if (UpdateStrategyEnum.GREATEST.name().equals(updateStrategy)) {
          greatestAttrs.add(attrName);
        } else if (UpdateStrategyEnum.LEAST.name().equals(updateStrategy)) {
          leastAttrs.add(attrName);

        }
      }

      boolean timeAggregationFlag = conf.getBoolean(JobConstant.KEY_TIME_AGGREGATION_FLAG);

      builder
          .setUri(params.getString(JobConstant.KEY_URI))
          .setUsername(params.getString(JobConstant.KEY_USERNAME))
          .setPassword(RsaUtil.decryptDataOnJava(params.getString(JobConstant.KEY_PASSWORD),
              IotWorkRuntimeDecodeContext.getPrivateKey()))
          .setAttributeMapper(attributeMapper)
          .setAccAttrs(accAttrs)
          .setReplaceAttrs(replaceAttrs)
          .setReplaceWithNonNullAttrs(replaceWithNonNullAttrs)
          .setGreatestAttrs(greatestAttrs)
          .setLeastAttrs(leastAttrs)
          .setInsertKey(SchemaConstant.DOUBLE_QUOTE + conf.getString(JobConstant.KEY_INSERT_KEY)
              .replace(SchemaConstant.INDEX_COLUMN_SEPARATOR, SchemaConstant.DOUBLE_QUOTE
                  + SchemaConstant.INDEX_COLUMN_SEPARATOR + SchemaConstant.DOUBLE_QUOTE)
              + SchemaConstant.DOUBLE_QUOTE)
          .setSchemas(schemas)
          .setTable(conf.getString(JobConstant.KEY_TABLE))
          .setInsertType(outputMapper.getString(JobConstant.KEY_INSERT_TYPE))
          .setSinkType(SinkTypeEnum.PG)
          .setTimeAggregationFlag(timeAggregationFlag);

      if (timeAggregationFlag) {
        JSONObject timeAggregation = conf.getJSONObject(JobConstant.KEY_TIME_AGGREGATION);

        builder.setTimeDimension(timeAggregation.getString(JobConstant.KEY_TIME_DIMENSION))
            .setTimeAttr(RcTimeAttr.valueOf(SchemaConstant.DOUBLE_QUOTE
                + timeAggregation.getString(JobConstant.KEY_TIME_ATTR)
                .replaceFirst("\\|", SchemaConstant.DOUBLE_QUOTE + "\\|")))
            .setTimeZone(timeAggregation.containsKey(JobConstant.KEY_TIMEZONE) ? TimeZoneTypeEnum
                .valueOf(timeAggregation
                    .getString(JobConstant.KEY_TIMEZONE)).getZoneId() : DEFAULT_ZONE_ID)
            .setStartTime(timeAggregation.containsKey(JobConstant.KEY_START_TIME) ? timeAggregation
                .getString(JobConstant.KEY_START_TIME) : DEFAULT_START_TIME);

      }
      PostgreSqlSinkWapper result = builder.build();
      logger.debug("Decoding postgre sql sink: {}", result);
      return result;
    } catch (Exception ex) {
      logger.error("Exception occurred when decoding postgre sql sink: {}", conf);
      throw new DecodeSinkException(conf.getString(JobConstant.KEY_NODE_ID),
          conf.toJSONString());
    }
  }
}
