/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.wrapper.function.filter;

import com.rootcloud.analysis.common.entity.RcUnit;
import java.util.Collection;
import java.util.List;
import lombok.Builder;
import lombok.ToString;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Builder
@ToString(callSuper = true)
public class IntersectFilterWrapper extends AbstractFilterWrapper {

  private static Logger logger = LoggerFactory.getLogger(IntersectFilterWrapper.class);
  public List<Object> values;
  private String attribute;

  @Override
  public boolean filter(RcUnit record) {
    boolean result = false;
    try {
      if (record.getData().containsKey(attribute)) {
        Object attrValue = record.getAttr(attribute);
        if (attrValue != null) {
          for (Object targetValue : values) {
            if (attrValue instanceof Collection) {
              Collection a = (Collection) attrValue;
              if (a.contains(targetValue)) {
                result = true;
                break;
              }
            } else {
              if (String.valueOf(attrValue).equals(String.valueOf(targetValue))) {
                result = true;
                break;
              }
            }
          }
        }
      }
    } catch (Exception ex) {
      logger.error("Exception occurred when filtering data: {}， exception message: {}",
          record.toString(), ex.getMessage(), ex);
      result = false;
    }
    logger.debug("'Intersect' filter result: {}, attribute: {}, record: {}, value: {}",
        result, attribute, record.toString(), values.toString());
    return result;
  }

}
