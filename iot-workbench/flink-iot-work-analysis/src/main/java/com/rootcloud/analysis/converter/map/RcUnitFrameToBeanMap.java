/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.converter.map;

import com.rootcloud.analysis.common.entity.RcUnitFrame;

import java.lang.reflect.Field;
import java.util.List;

import org.apache.flink.api.common.functions.MapFunction;

public class RcUnitFrameToBeanMap<T> implements MapFunction<RcUnitFrame, T> {
  private List<String> keyList = null;
  private Class<?> clazz;

  public RcUnitFrameToBeanMap(List<String> keyList, Class<?> clazz) {
    this.keyList = keyList;
    this.clazz = clazz;
  }

  @Override
  public T map(RcUnitFrame value) throws Exception {

    //此处使用反射，对性能有一定影响，转化成ROW没有成功
    T object = (T) clazz.newInstance();
    try {

      for (int index = 0; index < keyList.size(); index++) {
        Field field = object.getClass().getDeclaredField(keyList.get(index));
        field.set(object, value.getRcu().getAttr(keyList.get(index)));
      }
    } catch (Exception e) {
      return null;
    }

    return object;
  }
}
