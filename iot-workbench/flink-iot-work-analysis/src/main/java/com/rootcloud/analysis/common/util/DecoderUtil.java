/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.common.util;

public class DecoderUtil {

  /**
   * get base url from uri.
   *
   * @param uri e.g."jdbc:postgresql://10.70.40.99:5432/kong?stringtype=unspecified"
   *
   * @return url e.g."jdbc:postgresql://10.70.40.99:5432/"
   */
  public static String getBaseUrl(String uri) {
    return uri.substring(0, uri.lastIndexOf("/") + 1);
  }

}