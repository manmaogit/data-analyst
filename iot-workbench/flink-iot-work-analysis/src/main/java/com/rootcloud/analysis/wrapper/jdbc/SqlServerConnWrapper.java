/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.wrapper.jdbc;

import static com.rootcloud.analysis.core.constants.SchemaConstant.ATTR_ORIGINAL_NAME;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Maps;
import com.rootcloud.analysis.common.entity.RcUnit;
import com.rootcloud.analysis.core.constants.CommonConstant;
import com.rootcloud.analysis.exception.SourceReadDataException;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SqlServerConnWrapper extends JdbcConnWrapper {

  private static final Logger logger = LoggerFactory.getLogger(SqlServerConnWrapper.class);

  /**
   * 构造方法.
   */
  public SqlServerConnWrapper(String driverClassName, String url, String username,
                              String password, String validationQuery, String table,
                              JSONArray schema, Integer limit, String filterSql) {
    super(driverClassName, url, username, password, validationQuery, table, schema, limit,
        filterSql);
  }

  @Override
  public String getTable() {
    return Arrays.stream(super.getTable().split("\\."))
        .map((ele) -> String.format("[%s]", ele))
        .collect(Collectors.joining(CommonConstant.DOT));
  }

  @Override
  public String buildCountSql() {
    return "select count(1) from " + getTable();
  }


  @Override
  public String buildSelectSql() {
    StringBuilder selectSql = new StringBuilder();
    selectSql.append("select ");
    if (!validateLimit(getFilterSql())) {
      selectSql.append("  TOP " + getLimit() + " ");
    }
    if (getSchema() != null && !getSchema().isEmpty()) {
      for (int i = 0; i < getSchema().size(); i++) {
        if (i > 0) {
          selectSql.append(CommonConstant.COMMA);
        }
        String columnName = ((JSONObject) getSchema().get(i)).getString(ATTR_ORIGINAL_NAME);
        selectSql.append(String.format("[%s]", columnName));
      }
    } else {
      selectSql.append("*");
    }
    selectSql.append(" from ");
    selectSql.append(getTable());
    if (!StringUtils.isBlank(getFilterSql())) {
      selectSql.append("  ");
      selectSql.append(getFilterSql());
    }
    return selectSql.toString().replaceAll(";", " ");
  }

  @Override
  public Set<RcUnit> getData() throws Exception {
    Set<RcUnit> outPutSet = new HashSet<>();
    try (Statement statement = getConnectionWrapper()
        .createStatement()) {
      try (ResultSet countResultSet = statement
          .executeQuery(buildCountSql())) {
        if (countResultSet.next()) {
          int totalCount = countResultSet.getInt(1);
          if (totalCount > 0) {
            String sql = buildSelectSql();
            logger.debug("Try to execute sql: {}", sql);
            try (ResultSet resultSet = statement.executeQuery(sql)) {
              ResultSetMetaData resultSetMetaData = resultSet.getMetaData();
              while (resultSet.next()) {
                Map<String, Object> data = Maps.newHashMap();
                for (int i = 1; i <= resultSetMetaData.getColumnCount(); i++) {
                  String column = resultSetMetaData.getColumnName(i);
                  resultSetMetaData.getColumnTypeName(i);
                  data.put(column, resultSet.getObject(column));
                }
                if (getSchema() != null && !getSchema().isEmpty()) {
                  outPutSet.add(RcUnit.valueOf(data, getSchema()));
                } else {
                  outPutSet.add(RcUnit.valueOf(data));
                }
              }
            }
          } else {
            logger.warn("SqlServer table {} is empty.", getTable());
          }
        }
      }
    } catch (Exception ex) {
      throw new SourceReadDataException(
          String.format("Exception occurred when reading data from SqlServer."
              + " Table:%s Detail: %s",getTable(), ex.toString())
       );
    }
    return outPutSet;
  }

  /**
   * 检查sql中是否包含limit.
   */
  public Boolean validateLimit(String sql) {
    if (StringUtils.isBlank(sql)) {
      return false;
    }
    Matcher matcher = Pattern.compile("[\\s\\S]+[\\s\\r\\f\\n]+TOP[\\s\\r\\f\\n]+[\\s\\S]+")
        .matcher(sql.toUpperCase());
    return matcher.find();
  }
}
