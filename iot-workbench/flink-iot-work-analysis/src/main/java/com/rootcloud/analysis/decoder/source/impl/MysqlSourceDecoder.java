/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.decoder.source.impl;

import com.alibaba.fastjson.JSONObject;
import com.rootcloud.analysis.context.IotWorkRuntimeDecodeContext;
import com.rootcloud.analysis.core.constants.JobConstant;
import com.rootcloud.analysis.core.enums.DataSourceTypeEnum;
import com.rootcloud.analysis.core.util.RsaUtil;
import com.rootcloud.analysis.decoder.source.AbstractSourceDecoder;
import com.rootcloud.analysis.decoder.source.SourceDecoder;
import com.rootcloud.analysis.exception.DecodeSourceException;
import com.rootcloud.analysis.wrapper.source.AbstractSourceWrapper;
import com.rootcloud.analysis.wrapper.source.MysqlSourceWrapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SourceDecoder(dataSourceType = DataSourceTypeEnum.MYSQL)
public class MysqlSourceDecoder extends AbstractSourceDecoder {

  private static Logger logger = LoggerFactory.getLogger(MysqlSourceDecoder.class);

  @Override
  public AbstractSourceWrapper decodeSourceParams(JSONObject conf) {
    try {
      JSONObject params = conf.getJSONObject(JobConstant.KEY_PARAMETERS);
      String filterSql = conf.getString(JobConstant.KEY_FILTER_SQL);
      JSONObject detectParams = params.getJSONObject(JobConstant.KEY_CHANGE_DETECT);

      MysqlSourceWrapper.Builder builder = new MysqlSourceWrapper.Builder();

      builder.setUri(params.getString(JobConstant.KEY_URI))
          .setUsername(params.getString(JobConstant.KEY_USERNAME))
          .setPassword(RsaUtil.decryptDataOnJava(params.getString(JobConstant.KEY_PASSWORD),
              IotWorkRuntimeDecodeContext.getPrivateKey()))
          .setTable(params.getString(JobConstant.KEY_TABLE))
          .setLimit(params.getInteger(JobConstant.KEY_LIMIT) == null ? JobConstant.LIMIT_DEFAULT
              : params.getInteger(JobConstant.KEY_LIMIT))
          .setDetectPeriod(detectParams.getInteger(JobConstant.KEY_PERIOD))
          .setFilterSql(filterSql);
      MysqlSourceWrapper result = builder.build();
      logger.debug("Decoding mysql source: {}", result);
      return result;
    } catch (Exception ex) {
      logger.error("Exception occurred when decoding mysql source: {}", conf);
      throw new DecodeSourceException(conf.getString(JobConstant.KEY_NODE_ID),
          conf.toJSONString());
    }

  }
}
