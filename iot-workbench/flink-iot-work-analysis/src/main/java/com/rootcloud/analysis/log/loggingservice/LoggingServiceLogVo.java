/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.log.loggingservice;

import com.alibaba.fastjson.JSONObject;
import java.io.Serializable;
import java.util.Map;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Iam对应的kafka消息格式：http://confluence.irootech.com/pages/viewpage.action?pageId=298023239.
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class LoggingServiceLogVo implements Serializable {

  //string, uuid, optional, 如果没有，就在服务端随机生成一个
  private String id;
  //如login、operation、business、diagnosis
  private String logType;
  //每个服务自定义的日志标签元数据,json格式
  private Map<String,String> labels;
  //服务类型，枚举值，表示哪个服务使用的日志
  private String logService;
  private Long timestamp;
  private LoggingServiceLogPayloadVo payload;
}



