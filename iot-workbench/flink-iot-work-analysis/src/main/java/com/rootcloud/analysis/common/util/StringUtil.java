/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.common.util;

import java.net.URL;
import java.util.List;

public class StringUtil {

  private StringUtil() {

  }

  /**
   * "file:/home/whf/cn/fh" -> "/home/whf/cn/fh". "jar:file:/home/whf/foo.jar!cn/fh" ->
   * "/home/whf/foo.jar".
   */
  public static String getRootPath(URL url) {
    String fileUrl = url.getFile();
    int pos = fileUrl.indexOf('!');

    if (-1 == pos) {
      return fileUrl;
    }

    return fileUrl.substring(5, pos);
  }

  /**
   * "cn.fh.lightning" -> "cn/fh/lightning"
   *
   * @param name .
   *
   * @return .
   */
  public static String dotToSplash(String name) {
    return name.replaceAll("\\.", "/");
  }

  /**
   * "Apple.class" -> "Apple"
   */
  public static String trimExtension(String name) {
    int pos = name.indexOf('.');
    if (-1 != pos) {
      return name.substring(0, pos);
    }

    return name;
  }

  /**
   * /application/home -> /home.
   *
   * @param uri .
   *
   * @return .RcBuildContext
   */
  public static String trimUri(String uri) {
    String trimmed = uri.substring(1);
    int splashIndex = trimmed.indexOf('/');

    return trimmed.substring(splashIndex);
  }

  /**
   * 使用指定字符将字符串集合连接.
   */
  public static String concatWithChar(List<String> concatList, String spilt) {
    if (concatList == null || concatList.isEmpty()) {
      return "";
    }
    StringBuilder stringBuilder = new StringBuilder();
    Integer size = concatList.size();
    for (String str : concatList) {
      stringBuilder.append(str);
      size--;
      if (size != 0) {
        stringBuilder.append(spilt);
      }

    }
    return stringBuilder.toString();
  }
}