/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.wrapper.source;

import com.alibaba.fastjson.JSON;
import com.rootcloud.analysis.common.context.IotWorkRuntimeContext;
import com.rootcloud.analysis.common.entity.RcUnit;
import com.rootcloud.analysis.common.factory.KafkaLogMiddlewareResourceFactory;
import com.rootcloud.analysis.core.constants.CommonConstant;
import com.rootcloud.analysis.core.constants.GrayLogConstant;
import com.rootcloud.analysis.core.constants.ShortMessageConstant;
import com.rootcloud.analysis.core.enums.GrayLogResultEnum;
import com.rootcloud.analysis.core.enums.GraylogLogTypeEnum;

import com.rootcloud.analysis.core.graylog.LogVo;
import com.rootcloud.analysis.log.loggingservice.LoggingServiceUtil;
import java.util.HashMap;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.functions.ProcessFunction;
import org.apache.flink.streaming.connectors.kafka.internals.FlinkKafkaInternalProducer;
import org.apache.flink.table.api.Table;
import org.apache.flink.table.api.bridge.java.StreamTableEnvironment;
import org.apache.flink.table.catalog.hive.HiveCatalog;
import org.apache.flink.types.Row;
import org.apache.flink.util.Collector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Getter
@ToString(callSuper = true)
public class InnerHiveSourceWrapper extends HiveSourceWrapper {

  private static Logger logger = LoggerFactory.getLogger(InnerHiveSourceWrapper.class);

  private static final String COLUMN_PARTITION_ID = "partitionid";

  private static final String COLUMN_JSON_STRING = "jsonvalue_s";

  private final long partitionIdLowerBound;

  private final long partitionIdUpperBound;

  private InnerHiveSourceWrapper(Builder builder) {
    this.partitionIdLowerBound = builder.partitionIdLowerBound;
    this.partitionIdUpperBound = builder.partitionIdUpperBound;
  }

  public static class Builder {

    private long partitionIdLowerBound;

    private long partitionIdUpperBound;

    public Builder setPartitionIdLowerBound(long partitionIdLowerBound) {
      this.partitionIdLowerBound = partitionIdLowerBound;
      return this;
    }

    public Builder setPartitionIdUpperBound(long partitionIdUpperBound) {
      this.partitionIdUpperBound = partitionIdUpperBound;
      return this;
    }

    public InnerHiveSourceWrapper build() {
      return new InnerHiveSourceWrapper(this);
    }
  }

  /**
   * Builds query sql.
   */
  public String buildQuerySql() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("select ");
    stringBuilder.append(COLUMN_JSON_STRING);
    stringBuilder.append(" from ");
    stringBuilder.append(getCatalogName());
    stringBuilder.append(CommonConstant.DOT);
    stringBuilder.append(getDefaultDatabase());
    stringBuilder.append(CommonConstant.DOT);
    stringBuilder.append(getTable());
    stringBuilder.append(" where ");
    stringBuilder.append(COLUMN_PARTITION_ID);
    stringBuilder.append(" >= ");
    stringBuilder.append(partitionIdLowerBound);
    stringBuilder.append(" and ");
    stringBuilder.append(COLUMN_PARTITION_ID);
    stringBuilder.append(" <= ");
    stringBuilder.append(partitionIdUpperBound);
    return stringBuilder.toString();
  }

  @Override
  public void registerInput(IotWorkRuntimeContext context) {
    StreamTableEnvironment tableEnv = context.getTableEnvFromCache();
    Configuration configuration = tableEnv.getConfig().getConfiguration();
    configuration.setInteger("table.exec.hive.infer-source-parallelism.max", getParallelism());
    HiveCatalog hiveCatalog = new HiveCatalog(getCatalogName(), getDefaultDatabase(),
        getHiveConfDir());
    tableEnv.registerCatalog(getCatalogName(), hiveCatalog);
    String querySql = buildQuerySql();
    logger.info("InnerHive:querySql={}", querySql);
    Table table = tableEnv.sqlQuery(querySql);
    DataStream<RcUnit> dataStream = tableEnv.toRetractStream(table, Row.class)
        .process(new Row2RcUnitFlatMap(getJobId(), getTenantId(), getJobName(), getLogType(),
            getBizStartTime(), getBizEndTime(), getKafkaLoggingServiceServers(),
            getKafkaLoggingServiceTopics(), null))
        .setParallelism(getParallelism())
        .uid(getNodeId())
        .name(getNodeName());
    context.putObjectToContainer(getNodeId(), dataStream);
  }

  @AllArgsConstructor
  private static class Row2RcUnitFlatMap extends ProcessFunction<Tuple2<Boolean, Row>, RcUnit> {

    private final String jobId;
    private final String tenantId;
    private final String jobName;
    private final GraylogLogTypeEnum logType;
    private final long bizStartTime;
    private final long bizEndTime;
    private final String kafkaLoggingServiceServers;
    private final String kafkaLoggingServiceTopics;
    private FlinkKafkaInternalProducer flinkKafkaInternalProducer;

    @Override
    public void open(Configuration parameters) throws Exception {
      super.open(parameters);
      if (flinkKafkaInternalProducer == null && kafkaLoggingServiceServers != null) {
        flinkKafkaInternalProducer = KafkaLogMiddlewareResourceFactory.getProducer(
            kafkaLoggingServiceServers,
            this.toString());
      }

    }

    @Override
    public void close() throws Exception {
      super.close();
      KafkaLogMiddlewareResourceFactory.close(this.toString());
      flinkKafkaInternalProducer = null;
    }

    @Override
    public void processElement(Tuple2<Boolean, Row> value,
                               ProcessFunction<Tuple2<Boolean, Row>, RcUnit>.Context ctx,
                               Collector<RcUnit> out) throws Exception {
      try {
        RcUnit result = RcUnit
            .valueOf(JSON.parseObject(value.f1.getField(0).toString(), HashMap.class));
        logger.debug("Inner hive output: {}", result);
        if (isInBizTimeScope(result, bizStartTime, bizEndTime)
            && !Thread.currentThread().isInterrupted()) {
          out.collect(result);
        }
      } catch (Exception ex) {
        logger.error("Exception occurred when processing inner hive data: {}", ex.toString());
        LoggingServiceUtil.alarm(LogVo.builder()
            .k8sServiceName(GrayLogConstant.K8S_SERVICE_NAME)
            .module(GrayLogConstant.MODULE_NAME)
            .userName(tenantId)
            .tenantId(tenantId)
            .operateObjectName(jobName)
            .operateObject(jobId)
            .shortMessage(String.format("Failed to parse JSON string from Hive. Cause: %s",
                ex.toString()))
            .operation(ShortMessageConstant.PROCESS_SQL_RESULT)
            .requestId(String.valueOf(System.currentTimeMillis()))
            .result(GrayLogResultEnum.FAIL.getValue())
            .logType(logType)
            .kafkaLoggingServiceTopic(kafkaLoggingServiceTopics)
            .kafkaLoggingServiceServers(kafkaLoggingServiceServers)
            .build());
      }
    }
  }

}
