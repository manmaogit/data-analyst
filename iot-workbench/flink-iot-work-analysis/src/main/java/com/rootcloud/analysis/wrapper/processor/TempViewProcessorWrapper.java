/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.wrapper.processor;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.rootcloud.analysis.common.context.IotWorkRuntimeContext;
import com.rootcloud.analysis.common.entity.RcUnit;
import com.rootcloud.analysis.context.IotWorkRuntimeBuildContext;
import com.rootcloud.analysis.converter.stream.table.RcUnitToRowConverter;
import com.rootcloud.analysis.core.constants.SchemaConstant;
import java.util.ArrayList;
import java.util.List;
import lombok.Getter;
import lombok.ToString;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.table.api.bridge.java.StreamTableEnvironment;
import org.apache.flink.types.Row;

@Getter
@ToString(callSuper = true)
public class TempViewProcessorWrapper extends AbstractProcessorWrapper {

  private final String viewName;
  private final JSONArray schema;

  private TempViewProcessorWrapper(Builder builder) {
    this.viewName = builder.viewName;
    this.schema = builder.schema;

  }

  @ToString
  public static class Builder {

    private JSONArray schema;

    private String viewName;

    public Builder setSchema(JSONArray schema) {
      this.schema = schema;
      return this;
    }

    public Builder setViewName(String viewName) {
      this.viewName = viewName;
      return this;
    }

    public TempViewProcessorWrapper build() {
      return new TempViewProcessorWrapper(this);
    }
  }

  @Override
  public void registerProcess(IotWorkRuntimeContext context) throws Exception {
    DataStream<RcUnit> priorDataStream = context.getObjectFromContainer(getPriorNodeId());
    List<String> fieldNames = new ArrayList<>(getSchema().size());
    List<String> fieldTypes = new ArrayList<>(getSchema().size());
    List<Integer> valueTypeScales = new ArrayList<>(getSchema().size());
    for (int i = 0; i < getSchema().size(); i++) {
      JSONObject jsonObject = (JSONObject) getSchema().get(i);
      fieldNames.add(jsonObject.getString(SchemaConstant.ATTR_NAME));
      fieldTypes.add(jsonObject.getString(SchemaConstant.ATTR_DATA_TYPE));
      valueTypeScales.add(jsonObject.getInteger(SchemaConstant.ATTR_SCALE));
    }
    RcUnitToRowConverter converter = new RcUnitToRowConverter(getViewName(), fieldNames,
        fieldTypes, valueTypeScales);
    DataStream<Row> rowDataStream = converter.convert(priorDataStream);
    StreamTableEnvironment tableEnv = context.getTableEnvFromCache();
    tableEnv.createTemporaryView(getViewName(), rowDataStream);
    IotWorkRuntimeBuildContext.putTemporaryViewName(getNodeId(), getViewName());
    context.putObjectToContainer(getNodeId(), priorDataStream);
  }
}
