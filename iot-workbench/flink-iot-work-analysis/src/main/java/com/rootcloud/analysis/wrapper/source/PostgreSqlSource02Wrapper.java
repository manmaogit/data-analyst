/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.wrapper.source;

import com.rootcloud.analysis.common.util.PostgreSqlDataTypeConverter;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Please use PostgresqlBatchSourceWrapper instead.
 */
@Deprecated
@Getter
@Setter
@ToString(callSuper = true)
public class PostgreSqlSource02Wrapper extends JdbcSource02Wrapper {

  @Override
  protected String getFlinkSqlType(String originDbType) {
    return PostgreSqlDataTypeConverter
        .convert2FlinkSqlType(originDbType);
  }

}