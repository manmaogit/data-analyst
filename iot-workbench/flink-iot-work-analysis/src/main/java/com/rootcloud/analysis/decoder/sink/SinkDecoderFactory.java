/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.decoder.sink;

import com.rootcloud.analysis.decoder.source.AbstractSourceDecoder;
import com.rootcloud.analysis.scanner.ClasspathPackageScanner;

import java.io.IOException;
import java.lang.reflect.Modifier;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SinkDecoderFactory {

  private static Logger logger = LoggerFactory.getLogger(SinkDecoderFactory.class);
  private static Map<String, AbstractSinkDecoder> decoderMap = new HashMap<>();

  static {
    ClasspathPackageScanner scanner = new ClasspathPackageScanner(
        "com.rootcloud.analysis.decoder.sink.impl");
    try {
      List<String> classList = scanner.getFullyQualifiedClassNameList();

      for (String className : classList) {
        try {
          Class clazz = SinkDecoderFactory.class.getClassLoader()
              .loadClass(className.replace("com/rootcloud/analysis/decoder/sink/impl/", ""));
          if (Modifier.isAbstract(clazz.getModifiers()) || clazz.isInterface()) {
            continue;
          }
          Object obj = clazz.newInstance();
          if (!(obj instanceof AbstractSinkDecoder)) {
            continue;
          }
          AbstractSinkDecoder decoder = (AbstractSinkDecoder) obj;
          SinkDecoder anno = decoder.getClass().getAnnotation(SinkDecoder.class);
          if (anno == null) {
            continue;
          }
          decoderMap.put(anno.sinkType().name(), decoder);
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException e) {
          logger.warn("Exception occurred when initializing AbstractSinkDecoder instance", e);
        }
      }
    } catch (IOException e) {
      logger.error("SinkDecoderFactory init failed", e);
    }
  }

  /**
   * getDecoder.
   */
  public static AbstractSinkDecoder getDecoder(String outputType) {
    if (decoderMap.containsKey(outputType)) {
      return decoderMap.get(outputType);
    }

    logger.error("Output Decoder not found: {}", outputType);
    return null;
  }

}
