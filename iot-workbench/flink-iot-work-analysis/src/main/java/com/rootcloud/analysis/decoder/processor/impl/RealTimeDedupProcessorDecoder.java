/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.decoder.processor.impl;

import com.alibaba.fastjson.JSONObject;
import com.rootcloud.analysis.core.constants.JobConstant;
import com.rootcloud.analysis.core.enums.ProcessTypeEnum;
import com.rootcloud.analysis.decoder.processor.AbstractProcessorDecoder;
import com.rootcloud.analysis.decoder.processor.ProcessorDecoder;
import com.rootcloud.analysis.exception.DecodeProcessorException;
import com.rootcloud.analysis.wrapper.processor.AbstractProcessorWrapper;
import com.rootcloud.analysis.wrapper.processor.RealTimeDedupProcessorWrapper;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 流计算-去重节点.
 *
 * @CreatedBy: zexin.huang
 * @Date: 2021/12/20 2:40 下午
 */
@ProcessorDecoder(processType = ProcessTypeEnum.REALTIME_DEDUP)
public class RealTimeDedupProcessorDecoder extends AbstractProcessorDecoder {

  private static Logger logger = LoggerFactory.getLogger(RealTimeDedupProcessorDecoder.class);

  @Override
  protected AbstractProcessorWrapper decodeProcessParams(JSONObject conf) {
    try {
      AbstractProcessorWrapper wrapper = RealTimeDedupProcessorWrapper.builder()
          .aggPrimaryKey(conf.getString(JobConstant.KEY_DEDUP_AGG_PRIMARY_KEY))
          .keyFieldList(conf.getObject(JobConstant.KEY_DEDUP_KEY_FIELD_LIST, List.class))
          .dedupMapMaxSize(conf.getInteger(JobConstant.KEY_DEDUP_MAP_MAX_SIZE))
          .build();
      logger.debug("Decoding dedup process: {}", wrapper);
      return wrapper;
    } catch (Exception ex) {
      logger.error("Exception occurred when decoding dedup processor: {}", conf);
      throw new DecodeProcessorException();
    }
  }
}
