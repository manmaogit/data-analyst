/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.wrapper.jdbc;

import com.alibaba.fastjson.JSONArray;
import com.rootcloud.analysis.common.entity.RcUnit;
import com.rootcloud.analysis.common.jdbc.ConnectionWrapper;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.Set;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class JdbcConnWrapper implements IJdbcWrapper {

  private static final Logger logger = LoggerFactory.getLogger(JdbcConnWrapper.class);

  private static final long serialVersionUID = 1L;
  private ConnectionWrapper connectionWrapper;

  private final String table;
  private final JSONArray schema;
  private final Integer limit;
  private final String url;
  private final String filterSql;

  /**
   * 构造方法.
   */
  public JdbcConnWrapper(String driverClassName, String url, String username,
                         String password, String validationQuery, String table,
                         JSONArray schema, Integer limit, String filterSql) {
    connectionWrapper = new ConnectionWrapper(driverClassName, url, username, password,
        validationQuery);
    this.table = table;
    this.schema = schema;
    this.limit = limit;
    this.url = url;
    this.filterSql = filterSql;
  }

  /**
   * 获取limit.
   */
  public Integer getLimit() {
    return this.limit;
  }

  /**
   * 获取schema.
   */
  public JSONArray getSchema() {
    return this.schema;
  }

  /**
   * 获取table.
   */
  public String getTable() {
    return this.table;
  }

  /**
   * 获取url.
   */
  public String getUrl() {
    return this.url;
  }

  public String getFilterSql() {
    return this.filterSql;
  }

  /**
   * 获取JDBC连接.
   */
  public ConnectionWrapper getConnectionWrapper() {
    return connectionWrapper;
  }

  /**
   * 销毁连接.
   */
  public void close() {
    connectionWrapper.close();
    connectionWrapper = null;
  }

  /**
   * 拼接查询总数sql.
   */
  public abstract String buildCountSql();

  /**
   * 拼接查询sql.
   */
  public abstract String buildSelectSql();

  /**
   * 查询数据.
   */
  public abstract Set<RcUnit> getData() throws Exception;

  /**
   * 销毁已注册驱动.
   */
  @Deprecated
  private void deregisterDriver() {
    ClassLoader cl = getClass().getClassLoader();
    Enumeration<Driver> drivers = DriverManager.getDrivers();
    while (drivers.hasMoreElements()) {
      Driver driver = drivers.nextElement();
      if (driver != null && driver.getClass().getClassLoader() == cl) {
        try {
          logger.info("Try to deregister driver: {}, class loader: {}",
              driver.toString(), driver.getClass().getClassLoader().toString());
          DriverManager.deregisterDriver(driver);
        } catch (SQLException e) {
          logger.warn("Failed to deregister driver: {}, exception: {}", driver.toString(),
              e.toString());
        }
      }
    }
  }
}
