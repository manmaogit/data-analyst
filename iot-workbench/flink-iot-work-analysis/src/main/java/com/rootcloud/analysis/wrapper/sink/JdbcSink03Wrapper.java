/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.wrapper.sink;

import com.alibaba.fastjson.JSONArray;
import com.rootcloud.analysis.common.context.IotWorkRuntimeContext;
import com.rootcloud.analysis.context.IotWorkRuntimeBuildContext;
import com.rootcloud.analysis.core.constants.CommonConstant;

import java.util.List;
import java.util.Map;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.apache.flink.table.api.StatementSet;
import org.apache.flink.table.api.bridge.java.StreamTableEnvironment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Getter
@Setter
@ToString(callSuper = true)
public abstract class JdbcSink03Wrapper extends AbstractSinkWrapper {

  private static Logger logger = LoggerFactory.getLogger(JdbcSink03Wrapper.class);

  private String uri;

  private String username;

  private String password;

  private String table;

  private List<String> columns;

  private Map<String, String> attributeMapper;

  private String primaryKey;

  private JSONArray schema;

  /**
   * Gets flink sql type.
   */
  protected abstract String getFlinkSqlType(String originDbType);

  /**
   * Builds DML.
   */
  public String buildDml() {
    // e.g., INSERT INTO MyUserTable(id, name, age) SELECT id, name, age, status FROM T;
    StringBuilder dmlBuilder = new StringBuilder();
    dmlBuilder.append("INSERT INTO ");
    dmlBuilder.append(getTempTableName());
    dmlBuilder.append(" (");
    StringBuilder sourceColBuilder = new StringBuilder();
    for (int i = 0; i < getColumns().size(); i++) {
      if (i > 0) {
        dmlBuilder.append(CommonConstant.COMMA);
        sourceColBuilder.append(CommonConstant.COMMA);
      }
      dmlBuilder.append("`");
      dmlBuilder.append(getColumns().get(i));
      dmlBuilder.append("`");
      sourceColBuilder.append("`");
      sourceColBuilder.append(getAttributeMapper().get(getColumns().get(i)));
      sourceColBuilder.append("`");
    }
    dmlBuilder.append(") SELECT ");
    dmlBuilder.append(sourceColBuilder);
    dmlBuilder.append(" FROM ");
    dmlBuilder.append(IotWorkRuntimeBuildContext.getTemporaryViewName(getPriorNodeId()));
    return dmlBuilder.toString();
  }

  /**
   * Builds DDL.
   */
  public abstract String buildDdl();

  protected String getTempTableName() {
    return getNodeId();
  }

  @Override
  public void registerOutput(IotWorkRuntimeContext context) {
    StreamTableEnvironment tableEnv = context.getTableEnvFromCache();
    logger.debug("DDL: {}", buildDdl());
    tableEnv.executeSql(buildDdl());
    logger.debug("DML: {}", buildDml());
    StatementSet statementSet = context.getStatementSetFromCache();
    statementSet.addInsertSql(buildDml());
  }

}

