/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.wrapper.function.filter;

import com.rootcloud.analysis.common.entity.RcUnit;
import com.rootcloud.analysis.wrapper.IWrapper;
import org.apache.flink.api.common.functions.FilterFunction;

public abstract class AbstractFilterWrapper implements FilterFunction<RcUnit>, IWrapper {

}
