/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.context;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Maps;
import com.rootcloud.analysis.common.context.IotWorkRuntimeContext;
import com.rootcloud.analysis.core.enums.GraylogLogTypeEnum;
import com.rootcloud.analysis.wrapper.time.scope.AbstractTimeScopeWrapper;
import java.util.Map;

public class IotWorkRuntimeDecodeContext extends IotWorkRuntimeContext {

  private static String jobId;

  private static String jobName;

  private static String tenantId;

  private static String userId;

  private static Map<String, JSONObject> nodeConfMap = Maps.newHashMap();

  private static String privateKey;

  private static String systemDatetime;

  private static AbstractTimeScopeWrapper timeScope;

  private static GraylogLogTypeEnum jobType;

  private static String loggingServiceKafkaTopic;

  private static String loggingServiceKafkaServers;

  private static Integer kafkaAdminExecTimeout;

  private static JSONArray udfInfos;

  public static JSONArray getUdfInfos() {
    return udfInfos;
  }

  public static void setUdfInfos(JSONArray udfInfos) {
    IotWorkRuntimeDecodeContext.udfInfos = udfInfos;
  }

  public static String getUserId() {
    return userId;
  }

  public static void setUserId(String userId) {
    IotWorkRuntimeDecodeContext.userId = userId;
  }

  public static void setLoggingServiceKafkaTopic(String loggingServiceKafkaTopic) {
    IotWorkRuntimeDecodeContext.loggingServiceKafkaTopic = loggingServiceKafkaTopic;
  }

  public static void setLoggingServiceKafkaServers(String loggingServiceKafkaServers) {
    IotWorkRuntimeDecodeContext.loggingServiceKafkaServers = loggingServiceKafkaServers;
  }

  public static String getLoggingServiceKafkaTopic() {
    return loggingServiceKafkaTopic;
  }

  public static String getLoggingServiceKafkaServers() {
    return loggingServiceKafkaServers;
  }

  public static Integer getKafkaAdminExecTimeout() {
    return kafkaAdminExecTimeout;
  }

  public static void setKafkaAdminExecTimeout(Integer kafkaAdminExecTimeout) {
    IotWorkRuntimeDecodeContext.kafkaAdminExecTimeout = kafkaAdminExecTimeout;
  }

  public static void setTenantId(String tenantId) {
    IotWorkRuntimeDecodeContext.tenantId = tenantId;
  }

  public static String getTenantId() {
    return tenantId;
  }

  public static String getJobId() {
    return jobId;
  }

  public static void setJobId(String jobId) {
    IotWorkRuntimeDecodeContext.jobId = jobId;
  }

  public static String getJobName() {
    return jobName;
  }

  public static void setJobName(String jobName) {
    IotWorkRuntimeDecodeContext.jobName = jobName;
  }

  public static void addNodeConf(String nodeId, JSONObject nodeConf) {
    nodeConfMap.put(nodeId, nodeConf);
  }

  public static JSONObject getNodeConf(String nodeId) {
    return nodeConfMap.get(nodeId);
  }

  public static String getPrivateKey() {
    return privateKey;
  }

  public static void setPrivateKey(String privateKey) {
    IotWorkRuntimeDecodeContext.privateKey = privateKey;
  }

  public static void setSystemDatetime(String systemDatetime) {
    IotWorkRuntimeDecodeContext.systemDatetime = systemDatetime;
  }

  public static String getSystemDatetime() {
    return systemDatetime;
  }

  public static void setTimeScope(AbstractTimeScopeWrapper timeScope) {
    IotWorkRuntimeDecodeContext.timeScope = timeScope;
  }

  public static AbstractTimeScopeWrapper getTimeScope() {
    return timeScope;
  }

  public static void setJobType(GraylogLogTypeEnum jobType) {
    IotWorkRuntimeDecodeContext.jobType = jobType;
  }

  public static GraylogLogTypeEnum getJobType() {
    return jobType;
  }

  /**
   * clear data.
   */
  public static void clean() {
    jobId = null;
    jobName = null;
    tenantId = null;
    nodeConfMap = null;
    privateKey = null;
    systemDatetime = null;
    timeScope = null;
    jobType = null;
    loggingServiceKafkaTopic = null;
    loggingServiceKafkaServers = null;
    kafkaAdminExecTimeout = null;
  }
}
