/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.decoder.job;

import com.rootcloud.analysis.scanner.ClasspathPackageScanner;

import java.io.IOException;
import java.lang.reflect.Modifier;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class JobDecoderFactory {

  private static Logger logger = LoggerFactory.getLogger(JobDecoderFactory.class);
  private static Map<String, AbstractJobDecoder> decoderMap = new HashMap<>();

  static {
    ClasspathPackageScanner scanner = new ClasspathPackageScanner(
        "com.rootcloud.analysis.decoder.job.impl");

    try {
      List<String> classList = scanner.getFullyQualifiedClassNameList();

      for (String className : classList) {
        try {
          Class clazz = JobDecoderFactory.class.getClassLoader()
              .loadClass(className.replace("com/rootcloud/analysis/decoder/job/impl/", ""));
          if (Modifier.isAbstract(clazz.getModifiers()) || clazz.isInterface()) {
            continue;
          }
          Object obj = clazz.newInstance();
          if (!(obj instanceof AbstractJobDecoder)) {
            continue;
          }
          AbstractJobDecoder decoder = (AbstractJobDecoder) obj;
          JobDecoder anno = decoder.getClass().getAnnotation(JobDecoder.class);
          if (anno == null) {
            continue;
          }
          decoderMap.put(anno.jobType().name(), decoder);
        } catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
          logger.warn("Exception occurred when initializing AbstractJobDecoder instance", e);
        }
      }
    } catch (IOException e) {
      logger.error("Failed to init JobDecoderFactory", e);
    }
  }

  /**
   * getJobDecoder.
   */
  public static AbstractJobDecoder getJobDecoder(String jobType) {
    if (decoderMap.containsKey(jobType)) {
      return decoderMap.get(jobType);
    }

    logger.error("Job decoder not found, job type: {}", jobType);
    return null;
  }

  /**
   * clean map.
   */
  public static void clean() {
    decoderMap.clear();
  }

}
