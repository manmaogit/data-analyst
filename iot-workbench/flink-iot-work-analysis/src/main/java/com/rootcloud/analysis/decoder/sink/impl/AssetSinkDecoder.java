/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.decoder.sink.impl;

import static com.rootcloud.analysis.core.constants.CommonConstant.DOT;
import static com.rootcloud.analysis.core.constants.WrapperConstant.DEFAULT_START_TIME;
import static com.rootcloud.analysis.core.constants.WrapperConstant.DEFAULT_ZONE_ID;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Maps;
import com.rootcloud.analysis.context.IotWorkRuntimeDecodeContext;
import com.rootcloud.analysis.core.constants.JobConstant;
import com.rootcloud.analysis.core.constants.SchemaConstant;
import com.rootcloud.analysis.core.enums.SinkTypeEnum;
import com.rootcloud.analysis.core.enums.TimeZoneTypeEnum;
import com.rootcloud.analysis.core.enums.UpdateStrategyEnum;
import com.rootcloud.analysis.core.util.RsaUtil;
import com.rootcloud.analysis.decoder.sink.AbstractSinkDecoder;
import com.rootcloud.analysis.decoder.sink.SinkDecoder;
import com.rootcloud.analysis.entity.RcTimeAttr;
import com.rootcloud.analysis.exception.DecodeSinkException;
import com.rootcloud.analysis.wrapper.sink.AbstractSinkWrapper;
import com.rootcloud.analysis.wrapper.sink.AssetSinkWapper;
import java.util.List;
import java.util.Map;
import org.apache.commons.compress.utils.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SinkDecoder(sinkType = SinkTypeEnum.ASSET)
public class AssetSinkDecoder extends AbstractSinkDecoder {

  private static Logger logger = LoggerFactory.getLogger(AssetSinkDecoder.class);

  @Override
  public AbstractSinkWrapper decodeSinkParams(JSONObject conf) {
    try {
      AssetSinkWapper.Builder builder = new AssetSinkWapper.Builder();
      JSONObject params = conf.getJSONObject(JobConstant.KEY_PARAMETERS);
      JSONObject outputMapper = conf.getJSONObject(JobConstant.KEY_OUTPUT_MAPPER);
      JSONArray attributeMapperArr = outputMapper.getJSONArray(JobConstant.KEY_ATTRIBUTE_MAPPER);
      Map<String, List<String>> schemas = Maps.newHashMap();
      List<String> accAttrs = Lists.newArrayList();
      List<String> replaceAttrs = Lists.newArrayList();
      List<String> greatestAttrs = Lists.newArrayList();
      List<String> leastAttrs = Lists.newArrayList();
      Map<String, List<String>> jsonReplaceAttrs = Maps.newHashMap();
      Map<String, List<String>> jsonAccAttrs = Maps.newHashMap();
      Map<String, List<String>> jsonUndoAccAttrs = Maps.newHashMap();
      Map<String, String> attributeMapper = Maps.newHashMap();
      Map<String, String> dataTypes = Maps.newHashMap();

      for (Object o : attributeMapperArr) {
        JSONObject targetAttribute = ((JSONObject) o)
            .getJSONObject(JobConstant.KEY_TARGET_ATTRIBUTE);
        JSONObject sourceAttribute = ((JSONObject) o)
            .getJSONObject(JobConstant.KEY_SOURCE_ATTRIBUTE);

        attributeMapper.put(SchemaConstant.DOUBLE_QUOTE
                + targetAttribute.getString(SchemaConstant.ATTR_NAME)
                + SchemaConstant.DOUBLE_QUOTE,
            sourceAttribute.getString(SchemaConstant.ATTR_NAME));
        String targetAttributeName = targetAttribute.getString(SchemaConstant.ATTR_NAME);
        if (targetAttributeName.contains(DOT)) {
          String parentField = SchemaConstant.DOUBLE_QUOTE
              + targetAttributeName.substring(0, targetAttributeName.indexOf(DOT))
              + SchemaConstant.DOUBLE_QUOTE;
          String childField = SchemaConstant.DOUBLE_QUOTE
              + targetAttributeName.substring(targetAttributeName.indexOf(DOT) + 1)
              + SchemaConstant.DOUBLE_QUOTE;
          if (schemas.containsKey(parentField)) {
            List<String> accAttrList = schemas.get(parentField);
            accAttrList.add(childField);
            schemas.put(parentField, accAttrList);
          } else {
            List<String> accAttrList = Lists.newArrayList();
            accAttrList.add(childField);
            schemas.put(parentField, accAttrList);
          }
        } else {
          schemas.put(SchemaConstant.DOUBLE_QUOTE
              + targetAttributeName + SchemaConstant.DOUBLE_QUOTE, null);
        }

        String updateStrategy = ((JSONObject) o).getString(JobConstant.KEY_UPDATE_STRATEGY);
        String attrName = SchemaConstant.DOUBLE_QUOTE
            + targetAttribute.getString(SchemaConstant.ATTR_NAME)
            + SchemaConstant.DOUBLE_QUOTE;
        String dataType = targetAttribute.getString(SchemaConstant.ATTR_DATA_TYPE);

        if (UpdateStrategyEnum.ACCUMULATE.name().equals(updateStrategy)) {
          accAttrs.add(attrName);
          dataTypes.put(attrName, dataType);
        } else if (UpdateStrategyEnum.REPLACE.name().equals(updateStrategy)) {
          replaceAttrs.add(attrName);
          dataTypes.put(attrName, dataType);
        } else if (UpdateStrategyEnum.GREATEST.name().equals(updateStrategy)) {
          greatestAttrs.add(attrName);
          dataTypes.put(attrName, dataType);
        } else if (UpdateStrategyEnum.LEAST.name().equals(updateStrategy)) {
          leastAttrs.add(attrName);
          dataTypes.put(attrName, dataType);
        } else if (UpdateStrategyEnum.JSON_ACCUMULATE.name().equals(updateStrategy)) {
          if (attrName.contains(DOT)) {
            String parentField = attrName.substring(0, attrName.indexOf(DOT))
                + SchemaConstant.DOUBLE_QUOTE;
            String childField = SchemaConstant.DOUBLE_QUOTE
                + attrName.substring(attrName.indexOf(DOT) + 1);
            if (jsonAccAttrs.containsKey(parentField)) {
              List<String> accAttrList = jsonAccAttrs.get(parentField);
              accAttrList.add(childField);
              jsonAccAttrs.put(parentField, accAttrList);
            } else {
              List<String> accAttrList = Lists.newArrayList();
              accAttrList.add(childField);
              jsonAccAttrs.put(parentField, accAttrList);
            }
            dataTypes.put(childField, dataType);
          } else {
            jsonAccAttrs.put(attrName, null);
            dataTypes.put(attrName, dataType);
          }

        } else if (UpdateStrategyEnum.JSON_REPLACE.name().equals(updateStrategy)) {
          if (attrName.contains(DOT)) {
            String parentField = attrName.substring(0, attrName.indexOf(DOT))
                + SchemaConstant.DOUBLE_QUOTE;
            String childField = SchemaConstant.DOUBLE_QUOTE
                + attrName.substring(attrName.indexOf(DOT) + 1);
            if (jsonReplaceAttrs.containsKey(parentField)) {
              List<String> accAttrList = jsonReplaceAttrs.get(parentField);
              accAttrList.add(childField);
              jsonReplaceAttrs.put(parentField, accAttrList);
            } else {
              List<String> accAttrList = Lists.newArrayList();
              accAttrList.add(childField);
              jsonReplaceAttrs.put(parentField, accAttrList);
            }
            dataTypes.put(childField, dataType);
          } else {
            jsonReplaceAttrs.put(attrName, null);
            dataTypes.put(attrName, dataType);
          }
        } else if (UpdateStrategyEnum.JSON_UNDO.name().equals(updateStrategy)) {
          if (attrName.contains(DOT)) {
            String parentField = attrName.substring(0, attrName.indexOf(DOT))
                + SchemaConstant.DOUBLE_QUOTE;
            String childField = SchemaConstant.DOUBLE_QUOTE
                + attrName.substring(attrName.indexOf(DOT) + 1);
            if (jsonUndoAccAttrs.containsKey(parentField)) {
              List<String> accAttrList = jsonUndoAccAttrs.get(parentField);
              accAttrList.add(childField);
              jsonUndoAccAttrs.put(parentField, accAttrList);
            } else {
              List<String> accAttrList = Lists.newArrayList();
              accAttrList.add(childField);
              jsonUndoAccAttrs.put(parentField, accAttrList);
            }
            dataTypes.put(childField, dataType);
          } else {
            jsonUndoAccAttrs.put(attrName, null);
            dataTypes.put(attrName, dataType);
          }
        }
      }

      boolean timeAggregationFlag = conf.getBoolean(JobConstant.KEY_TIME_AGGREGATION_FLAG);

      String groupId = conf.getString(JobConstant.ASSET_KEY_GROUP_ID) == null ? conf
          .getString("group_id") : conf.getString(JobConstant.ASSET_KEY_GROUP_ID);

      builder
          .setUri(params.getString(JobConstant.KEY_URI))
          .setUsername(params.getString(JobConstant.KEY_USERNAME))
          .setPassword(RsaUtil.decryptDataOnJava(params.getString(JobConstant.KEY_PASSWORD),
              IotWorkRuntimeDecodeContext.getPrivateKey()))
          .setAttributeMapper(attributeMapper)
          .setAccAttrs(accAttrs)
          .setReplaceAttrs(replaceAttrs)
          .setGreatestAttrs(greatestAttrs)
          .setJsonAccAttrs(jsonAccAttrs)
          .setJsonUndoAccAttrs(jsonUndoAccAttrs)
          .setJsonReplaceAttrs(jsonReplaceAttrs)
          .setLeastAttrs(leastAttrs)
          .setInsertKey(SchemaConstant.DOUBLE_QUOTE + conf.getString(JobConstant.KEY_INSERT_KEY)
              .replace(SchemaConstant.INDEX_COLUMN_SEPARATOR, SchemaConstant.DOUBLE_QUOTE
                  + SchemaConstant.INDEX_COLUMN_SEPARATOR + SchemaConstant.DOUBLE_QUOTE)
              + SchemaConstant.DOUBLE_QUOTE)
          .setSchemas(schemas)
          .setTable(conf.getString(JobConstant.KEY_TABLE))
          .setInsertType(outputMapper.getString(JobConstant.KEY_INSERT_TYPE))
          .setTimeAggregationFlag(timeAggregationFlag)
          .setGroupid(groupId)
          .setGroupbase(conf.getString(JobConstant.ASSET_KEY_GROUP_BASE))
          .setTimebase(conf.getString(JobConstant.ASSET_KEY_TIMEBASE))
          .setObjectid(attributeMapper.get("\"" + JobConstant.ASSET_KEY_OBJECT_ID + "\""))
          .setTs(attributeMapper.get("\"" + JobConstant.ASSET_KEY_TS + "\""));

      if (timeAggregationFlag) {
        JSONObject timeAggregation = conf.getJSONObject(JobConstant.KEY_TIME_AGGREGATION);

        builder.setTimeDimension(timeAggregation.getString(JobConstant.KEY_TIME_DIMENSION))
            .setTimeAttr(RcTimeAttr.valueOf(SchemaConstant.DOUBLE_QUOTE
                + timeAggregation.getString(JobConstant.KEY_TIME_ATTR)
                .replaceFirst("\\|", SchemaConstant.DOUBLE_QUOTE + "\\|")))
            .setTimeZone(timeAggregation.containsKey(JobConstant.KEY_TIMEZONE) ? TimeZoneTypeEnum
                .valueOf(timeAggregation
                    .getString(JobConstant.KEY_TIMEZONE)).getZoneId() : DEFAULT_ZONE_ID)
            .setStartTime(timeAggregation.containsKey(JobConstant.KEY_START_TIME) ? timeAggregation
                .getString(JobConstant.KEY_START_TIME) : DEFAULT_START_TIME);
      }
      AssetSinkWapper result = builder.build();
      logger.debug("Decoding postgre sql sink: {}", result);
      return result;
    } catch (Exception ex) {
      logger.error("Exception occurred when decoding postgre sql sink: {}", conf);
      throw new DecodeSinkException(conf.getString(JobConstant.KEY_NODE_ID),
          conf.toJSONString());
    }
  }
}
