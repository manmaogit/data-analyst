/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.wrapper.source;

import com.alibaba.fastjson.JSONObject;
import com.rootcloud.analysis.common.util.MysqlDataTypeConverter;
import com.rootcloud.analysis.core.constants.CommonConstant;
import com.rootcloud.analysis.core.constants.SchemaConstant;
import com.rootcloud.analysis.wrapper.source.JdbcSource02Wrapper;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.apache.commons.lang.StringUtils;

/**
 * Please use MysqlBatchSourceWrapper instead.
 */
@Deprecated
@Getter
@Setter
@ToString(callSuper = true)
public class MysqlSource02Wrapper extends JdbcSource02Wrapper {

  @Override
  protected String getFlinkSqlType(String originDbType) {
    return MysqlDataTypeConverter
        .convert2FlinkSqlType(originDbType);
  }

}
