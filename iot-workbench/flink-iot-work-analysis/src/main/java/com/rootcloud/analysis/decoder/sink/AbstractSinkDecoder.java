/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.decoder.sink;

import com.alibaba.fastjson.JSONObject;
import com.rootcloud.analysis.core.constants.JobConstant;
import com.rootcloud.analysis.core.enums.SinkTypeEnum;
import com.rootcloud.analysis.decoder.AbstractDecoder;
import com.rootcloud.analysis.decoder.IDecoder;
import com.rootcloud.analysis.wrapper.AbstractWrapper;
import com.rootcloud.analysis.wrapper.processor.AbstractProcessorWrapper;
import com.rootcloud.analysis.wrapper.sink.AbstractSinkWrapper;

public abstract class AbstractSinkDecoder extends AbstractDecoder {

  @Override
  protected final AbstractWrapper decodeParams(JSONObject conf) {
    AbstractSinkWrapper abstractSinkWrapper = decodeSinkParams(conf);
    abstractSinkWrapper.setPriorNodeId(conf.getString(JobConstant.KEY_PRIOR_NODE_ID));
    abstractSinkWrapper.setSinkType(conf.getObject(JobConstant.KEY_SINK_TYPE, SinkTypeEnum.class));
    return abstractSinkWrapper;
  }

  protected abstract AbstractSinkWrapper decodeSinkParams(JSONObject conf);

}
