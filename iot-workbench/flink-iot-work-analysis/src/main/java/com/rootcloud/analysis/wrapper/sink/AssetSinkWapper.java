/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2020 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.wrapper.sink;

import static com.rootcloud.analysis.core.constants.CommonConstant.COMMA;
import static com.rootcloud.analysis.core.constants.CommonConstant.DOT;
import static com.rootcloud.analysis.core.constants.CommonConstant.VERTICAL_LINE;
import static com.rootcloud.analysis.core.enums.InsertTypeEnum.INSERT;
import static com.rootcloud.analysis.core.enums.InsertTypeEnum.UPSERT;

import com.alibaba.fastjson.JSONObject;
import com.google.common.base.Strings;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.rootcloud.analysis.common.context.IotWorkRuntimeContext;
import com.rootcloud.analysis.common.entity.RcUnit;
import com.rootcloud.analysis.common.util.DateUtil;
import com.rootcloud.analysis.converter.time.ITimeConverter;
import com.rootcloud.analysis.converter.time.TimeConverterFactory;
import com.rootcloud.analysis.core.constants.GrayLogConstant;
import com.rootcloud.analysis.core.constants.JobConstant;
import com.rootcloud.analysis.core.constants.SchemaConstant;
import com.rootcloud.analysis.core.constants.ShortMessageConstant;
import com.rootcloud.analysis.core.enums.GrayLogResultEnum;
import com.rootcloud.analysis.core.enums.GraylogLogTypeEnum;
import com.rootcloud.analysis.core.enums.SinkTypeEnum;
import com.rootcloud.analysis.core.enums.UpdateStrategyEnum;
import com.rootcloud.analysis.core.graylog.LogVo;
import com.rootcloud.analysis.entity.RcTimeAttr;
import com.rootcloud.analysis.log.loggingservice.LoggingServiceUtil;
import com.rootcloud.analysis.wrapper.WrapperBuilder;
import com.rootcloud.analysis.wrapper.function.jdbc.JdbcRichSinkFunction;
import com.rootcloud.analysis.wrapper.jdbc.PostgreSqlConnWrapper;
import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import lombok.Getter;
import lombok.ToString;
import org.apache.commons.compress.utils.Lists;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.metrics.Counter;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.postgresql.util.PGobject;
import org.postgresql.util.PSQLException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@ToString(callSuper = true)
public class AssetSinkWapper extends AbstractSinkWrapper {

  private static Logger logger = LoggerFactory.getLogger(AssetSinkWapper.class);
  private static final String KPIS = "\"kpis\"";
  private static final Set<String> RETRY_SQL_STATES = Sets.newHashSet("23505");
  private static final String QUOTE = "\"";
  private static final String TIMEBASE = QUOTE + JobConstant.ASSET_KEY_TIMEBASE
      + QUOTE;//"\"time_base\"";
  private static final String TENANTID = QUOTE + JobConstant.ASSET_KEY_TENANT_ID
      + QUOTE;//"\"tenant_id\"";
  private static final String GROUPID = QUOTE + JobConstant.ASSET_KEY_GROUP_ID
      + QUOTE;//"\"group_id\"";
  private static final String GROUPBASE = QUOTE + JobConstant.ASSET_KEY_GROUP_BASE
      + QUOTE;//"\"group_base\"";
  private static final String OBJECTID = QUOTE + JobConstant.ASSET_KEY_OBJECT_ID
      + QUOTE;//"\"object_id\"";
  private static final String TS = QUOTE + JobConstant.ASSET_KEY_TS
      + QUOTE;//"\"ts\"";

  private static final String SHOW_COLUMN = "SELECT "
      + "    a.attname as Field,"
      + "    concat_ws('',t.typname,SUBSTRING(format_type(a.atttypid,a.atttypmod)"
      + " from '\\(.*\\)')) as Type "
      + "FROM "
      + "    pg_class as c,"
      + "    pg_attribute as a, "
      + "    pg_type as t "
      + "WHERE "
      + "    c.relname = '%s' "
      + "    and a.atttypid = t.oid "
      + "    and a.attrelid = c.oid "
      + "    and a.attnum>0";

  private String uri;
  private String username;
  private String password;

  private String table;

  private String insertSql;
  private String updateSql;
  private String selectSql;

  private Map<String, List<String>> insertFields;
  private Map<String, String> fieldMapper;
  private List<String> kpisFiels;
  private Map<String, List<String>> updateFields;
  private Map<String, List<String>> selectFields;

  private List<String> insertFieldList;
  private List<String> updateFieldList;
  private List<String> selectFieldList;

  private ITimeConverter timeConverter;

  private RcTimeAttr timeAttr;

  @Getter
  private ZoneId zoneId;

  private Map<String, String> attributeMapper;

  private String insertType;

  @Getter
  private int startMonth;

  @Getter
  private int startDay;

  @Getter
  private int startHour;

  @Getter
  private int startMinute;

  private List<String> accAttrs;
  private Map<String, List<String>> jsonAccAttrs;
  private Map<String, List<String>> jsonReplaceAttrs;
  private Map<String, List<String>> jsonUndoAttrs;
  private List<String> replaceAttrs;
  private List<String> greatestAttrs;
  private List<String> leastAttrs;

  private boolean timeAggregationFlag;
  private String timeDimension;
  private Map<String, String> dataTypes;
  private String timebase;
  private String groupid;
  private String objectid;
  private String ts;
  private String groupbase;

  private AssetSinkWapper(AssetSinkWapper.Builder builder) {
    super(builder.jobId, builder.jobName, builder.tenantId, builder.nodeId,
        builder.parallelism, builder.nodeName, builder.priorNodeId, builder.sinkType,
        builder.logType);

    this.uri = builder.uri;
    this.username = builder.username;
    this.password = builder.password;

    this.table = builder.table;

    this.insertSql = builder.insertSql;
    this.updateSql = builder.updateSql;
    this.selectSql = builder.selectSql;

    this.insertFields = builder.insertFields;
    this.fieldMapper = builder.fieldMapper;
    this.kpisFiels = builder.kpisFields;
    this.updateFields = builder.updateFields;
    this.selectFields = builder.selectFields;

    this.insertFieldList = builder.insertFieldList;
    this.updateFieldList = builder.updateFieldList;
    this.selectFieldList = builder.selectFieldList;

    this.timeAttr = builder.timeAttr;

    this.zoneId = builder.timeZone;

    this.timeConverter = TimeConverterFactory.getConverter(builder.timeDimension);

    this.attributeMapper = builder.attributeMapper;

    this.insertType = builder.insertType;

    this.startMonth = builder.startMonth;
    this.startDay = builder.startDay;
    this.startHour = builder.startHour;
    this.startMinute = builder.startMinute;

    this.accAttrs = builder.accAttrs;
    this.jsonAccAttrs = builder.jsonAccAttrs;
    this.jsonUndoAttrs = builder.jsonUndoAccAttrs;
    this.jsonReplaceAttrs = builder.jsonReplaceAttrs;
    this.replaceAttrs = builder.replaceAttrs;
    this.greatestAttrs = builder.greatestAttrs;
    this.leastAttrs = builder.leastAttrs;

    this.timeAggregationFlag = builder.timeAggregationFlag;
    this.timeDimension = builder.timeDimension;
    this.dataTypes = builder.dataTypes;
    this.timebase = builder.timebase;
    this.groupid = builder.groupid;
    this.objectid = builder.objectid;
    this.ts = builder.ts;
    this.groupbase = builder.groupbase;
  }

  public static class Builder extends WrapperBuilder {

    private String nodeId;
    private String nodeName;
    private Integer parallelism;

    private String priorNodeId;
    private SinkTypeEnum sinkType;

    private String uri;
    private String username;
    private String password;
    private String table;
    private String insertKey;
    private String insertType;
    private Map<String, String> attributeMapper;
    private RcTimeAttr timeAttr;
    private String timeDimension;
    private int startMonth;
    private int startDay;
    private int startHour;
    private int startMinute;
    private ZoneId timeZone;
    private List<String> accAttrs;
    private Map<String, List<String>> jsonAccAttrs;
    private Map<String, List<String>> jsonUndoAccAttrs;
    private Map<String, List<String>> jsonReplaceAttrs;
    private List<String> replaceAttrs;
    private List<String> greatestAttrs;
    private List<String> leastAttrs;
    private boolean timeAggregationFlag;

    private Map<String, List<String>> schemas;

    private String insertSql;
    private String updateSql;
    private String selectSql;

    private Map<String, List<String>> insertFields = Maps.newHashMap();
    private Map<String, String> fieldMapper = Maps.newHashMap();
    private List<String> kpisFields = Lists.newArrayList();
    private Map<String, List<String>> updateFields = Maps.newHashMap();
    private Map<String, List<String>> selectFields = Maps.newHashMap();
    private List<String> insertFieldList = Lists.newArrayList();
    private List<String> updateFieldList = Lists.newArrayList();
    private List<String> selectFieldList = Lists.newArrayList();
    private Map<String, String> dataTypes;
    private String timebase;
    private String groupid;
    private String objectid;
    private String ts;
    private String groupbase;

    public AssetSinkWapper.Builder setDataTypes(Map<String, String> dataTypes) {
      this.dataTypes = dataTypes;
      return this;
    }

    public AssetSinkWapper.Builder setTimebase(String timebase) {
      this.timebase = timebase;
      return this;
    }

    public AssetSinkWapper.Builder setGroupbase(String groupbase) {
      this.groupbase = groupbase;
      return this;
    }

    public AssetSinkWapper.Builder setGroupid(String groupid) {
      this.groupid = groupid;
      return this;
    }

    public AssetSinkWapper.Builder setObjectid(String objectid) {
      this.objectid = objectid;
      return this;
    }

    public AssetSinkWapper.Builder setTs(String ts) {
      this.ts = ts;
      return this;
    }

    public AssetSinkWapper.Builder setNodeId(String nodeId) {
      this.nodeId = nodeId;
      return this;
    }

    public AssetSinkWapper.Builder setNodeName(String nodeName) {
      this.nodeName = nodeName;
      return this;
    }

    public AssetSinkWapper.Builder setParallelism(Integer parallelism) {
      this.parallelism = parallelism;
      return this;
    }

    /**
     * Sets start time.
     *
     * @param startTime start time
     *
     * @return builder
     */
    public AssetSinkWapper.Builder setStartTime(String startTime) {
      String[] tmp = startTime.split(VERTICAL_LINE);
      startMonth = tmp.length - 4 >= 0 ? Integer.parseInt(tmp[tmp.length - 4]) : 1;
      startDay = tmp.length - 3 >= 0 ? Integer.parseInt(tmp[tmp.length - 3]) : 1;
      startHour = tmp.length - 2 >= 0 ? Integer.parseInt(tmp[tmp.length - 2]) : 0;
      startMinute = tmp.length - 1 >= 0 ? Integer.parseInt(tmp[tmp.length - 1]) : 0;
      return this;
    }

    public AssetSinkWapper.Builder setUri(String uri) {
      this.uri = uri;
      return this;
    }

    public AssetSinkWapper.Builder setInsertType(String insertType) {
      this.insertType = insertType;
      return this;
    }

    public AssetSinkWapper.Builder setUsername(String username) {
      this.username = username;
      return this;
    }

    public AssetSinkWapper.Builder setAttributeMapper(Map<String, String> attributeMapper) {
      this.attributeMapper = attributeMapper;
      return this;
    }


    public AssetSinkWapper.Builder setSchemas(Map<String, List<String>> schemas) {
      this.schemas = schemas;
      return this;
    }

    public AssetSinkWapper.Builder setPassword(String password) {
      this.password = password;
      return this;
    }

    public AssetSinkWapper.Builder setInsertKey(String insertKey) {
      this.insertKey = insertKey;
      return this;
    }

    public AssetSinkWapper.Builder setAccAttrs(List<String> accAttrs) {
      this.accAttrs = accAttrs;
      return this;
    }

    public AssetSinkWapper.Builder setJsonAccAttrs(Map<String, List<String>> jsonAccAttrs) {
      this.jsonAccAttrs = jsonAccAttrs;
      return this;
    }

    public AssetSinkWapper.Builder setJsonUndoAccAttrs(Map<String, List<String>> jsonUndoAccAttrs) {
      this.jsonUndoAccAttrs = jsonUndoAccAttrs;
      return this;
    }

    public AssetSinkWapper.Builder setJsonReplaceAttrs(Map<String, List<String>> jsonReplaceAttrs) {
      this.jsonReplaceAttrs = jsonReplaceAttrs;
      return this;
    }

    public AssetSinkWapper.Builder setReplaceAttrs(List<String> replaceAttrs) {
      this.replaceAttrs = replaceAttrs;
      return this;
    }

    public AssetSinkWapper.Builder setGreatestAttrs(List<String> greatestAttrs) {
      this.greatestAttrs = greatestAttrs;
      return this;
    }

    public AssetSinkWapper.Builder setLeastAttrs(List<String> leastAttrs) {
      this.leastAttrs = leastAttrs;
      return this;
    }

    public AssetSinkWapper.Builder setTimeDimension(String timeDimension) {
      this.timeDimension = timeDimension;
      return this;
    }

    public AssetSinkWapper.Builder setTimeAttr(RcTimeAttr timeAttr) {
      this.timeAttr = timeAttr;
      return this;
    }

    public AssetSinkWapper.Builder setTimeZone(ZoneId timeZone) {
      this.timeZone = timeZone;
      return this;
    }

    public AssetSinkWapper.Builder setPriorNodeId(String priorNodeId) {
      this.priorNodeId = priorNodeId;
      return this;
    }

    public AssetSinkWapper.Builder setSinkType(SinkTypeEnum sinkType) {
      this.sinkType = sinkType;
      return this;
    }

    public AssetSinkWapper.Builder setTable(String table) {
      this.table = table;
      return this;
    }

    public AssetSinkWapper.Builder setTimeAggregationFlag(boolean timeAggregationFlag) {
      this.timeAggregationFlag = timeAggregationFlag;
      return this;
    }

    /**
     * build.
     */
    public AssetSinkWapper build() {
      this.table = buildTableName();
      this.insertSql = buildInsertSqlWithoutJson();
      this.updateSql = buildUpdateSql();
      this.selectSql = buildSelectSql();
      return new AssetSinkWapper(this);
    }

    private String buildTableName() {
      return table;
    }

    private String buildJsonSetSql(String jsonSetSql, String field,
                                   UpdateStrategyEnum updateStrategyEnum) {
      String fieldName = field.substring(1, field.length() - 1);

      StringBuilder jsonSetBuilder = new StringBuilder();

      if (updateStrategyEnum == UpdateStrategyEnum.JSON_REPLACE) {

        if (jsonSetSql == null) {
          jsonSetBuilder.append("jsonb_set(kpis, '{");

        } else {
          jsonSetBuilder.append("jsonb_set(");
          jsonSetBuilder.append(jsonSetSql);
          jsonSetBuilder.append(", '{");
        }
        jsonSetBuilder.append(fieldName);
        jsonSetBuilder.append("}', ?)");

      } else if (updateStrategyEnum == UpdateStrategyEnum.JSON_ACCUMULATE) {

        if (jsonSetSql == null) {
          jsonSetBuilder.append("jsonb_set(kpis, '{");
        } else {
          jsonSetBuilder.append("jsonb_set(");
          jsonSetBuilder.append(jsonSetSql);
          jsonSetBuilder.append(", '{");
        }
        jsonSetBuilder.append(fieldName);
        jsonSetBuilder.append("}', ((kpis->>'");
        jsonSetBuilder.append(fieldName);
        jsonSetBuilder.append("')::numeric + ?)::text::jsonb)");

      } else {
        jsonSetBuilder.append(jsonSetSql);
      }

      return jsonSetBuilder.toString();
    }

    private String buildUpdateSql() {
      if (accAttrs.isEmpty() && replaceAttrs.isEmpty() && greatestAttrs.isEmpty() && leastAttrs
          .isEmpty() && jsonReplaceAttrs.isEmpty() && jsonAccAttrs.isEmpty() && jsonUndoAccAttrs
          .isEmpty()) {
        return null;
      }

      if (jsonReplaceAttrs.isEmpty() && jsonAccAttrs.isEmpty() && !jsonUndoAccAttrs
          .isEmpty()) {
        return null;
      }

      StringBuilder builder = new StringBuilder();
      builder.append("update ");
      builder.append("\"" + table + "\"");
      builder.append(" set kpis = ");

      String jsonSetSql = null;

      if (jsonReplaceAttrs != null && !jsonReplaceAttrs.isEmpty()) {
        final List<String> accFields = jsonReplaceAttrs.get(KPIS);
        for (int index = 0; index < accFields.size(); index++) {
          String field = accFields.get(index);
          jsonSetSql = buildJsonSetSql(jsonSetSql, field,
              UpdateStrategyEnum.JSON_REPLACE);
          updateFieldList.add(field);
        }
      }

      String jsonSetSql1 = jsonSetSql;

      if (jsonAccAttrs != null && !jsonAccAttrs.isEmpty()) {
        final List<String> accFields = jsonAccAttrs.get(KPIS);
        for (int index = 0; index < accFields.size(); index++) {
          String field = accFields.get(index);
          jsonSetSql1 = buildJsonSetSql(jsonSetSql1, field,
              UpdateStrategyEnum.JSON_ACCUMULATE);
          updateFieldList.add(field);
        }
      }

      builder.append(jsonSetSql1);

      builder.append(" where ");
      String[] insetKeyTmp = insertKey.split(COMMA);
      for (int index = 0; index < insetKeyTmp.length; index++) {
        String field = insetKeyTmp[index];
        builder.append(field);
        builder.append("=?");
        if (index < insetKeyTmp.length - 1) {
          builder.append(" and ");
        }
        updateFieldList.add(field);
      }

      return builder.toString();
    }

    @Deprecated
    private String buildInsertSql() {
      StringBuilder builder = new StringBuilder();
      builder.append("insert into ");
      builder.append("\"" + table + "\"");
      builder.append(" (");
      int index = 0;
      for (Map.Entry<String, List<String>> entry : schemas.entrySet()) {
        String parentField = entry.getKey();

        if (!parentField.equals(KPIS)) {

          if (index > 0) {
            builder.append(COMMA);
          }
          builder.append(parentField);
          index++;
          insertFieldList.add(parentField);
          if (parentField.equals(TS)) {
            fieldMapper.put(parentField, attributeMapper.get(TS));
          } else if (parentField.equals(OBJECTID)) {
            fieldMapper.put(parentField, attributeMapper.get(OBJECTID));
          }
        }
      }

      if (index > 0) {
        builder.append(COMMA);
      }
      builder.append(KPIS);

      // insert into "kpi_asset"
      // ("ts","timebase","objectid","groupbase","tenantid","groupid","kpis")
      // values(?,?,?,?,?,?,'{"sales": ?,"price": ?,"catagory": ?,"event_time": ?}')
      builder.append(") values(");
      for (int count = 0; count < insertFieldList.size(); count++) {
        builder.append("?");
        builder.append(COMMA);
      }

      builder.append("'{");
      int j = 0;
      for (String jsonKey : schemas.get(KPIS)) {
        builder.append(jsonKey);
        builder.append(": +?");
        if (j < schemas.get(KPIS).size() - 1) {
          builder.append(COMMA);
        }
        insertFieldList.add(jsonKey);
        j++;
      }
      builder.append("}'");

      builder.append(")");
      return builder.toString();
    }

    private String buildInsertSqlWithoutJson() {
      StringBuilder builder = new StringBuilder();
      builder.append("insert into ");
      builder.append("\"" + table + "\"");
      builder.append(" (");
      int index = 0;
      for (Map.Entry<String, List<String>> entry : schemas.entrySet()) {
        String parentField = entry.getKey();

        if (!parentField.equals(KPIS)) {
          if (index > 0) {
            builder.append(COMMA);
          }
          builder.append(parentField);
          index++;
          insertFieldList.add(parentField);
          if (parentField.equals(TS)) {
            fieldMapper.put(parentField, attributeMapper.get(TS));
          } else if (parentField.equals(OBJECTID)) {
            fieldMapper.put(parentField, attributeMapper.get(OBJECTID));
          }
        } else {
          for (String jsonKey : schemas.get(KPIS)) {
            kpisFields.add(jsonKey);
          }
        }
      }

      if (index > 0) {
        builder.append(COMMA);
      }

      builder.append(KPIS);
      insertFieldList.add(KPIS);

      //// insert into "kpi_asset"
      // ("ts","timebase","objectid","groupbase","tenantid","groupid","kpis")
      // values(?,?,?,?,?,?,?)
      builder.append(") values (");
      for (int count = 0; count < insertFieldList.size(); count++) {
        builder.append("?");
        if (count < insertFieldList.size() - 1) {
          builder.append(COMMA);
        }
      }
      builder.append(")");
      return builder.toString();
    }

    private String buildSelectSql() {
      StringBuilder builder = new StringBuilder();
      builder.append("select * from ");
      builder.append("\"" + table + "\"");
      builder.append(" where ");

      int index = 0;
      for (String key : Arrays.asList(insertKey.split(COMMA))) {
        if (!key.equals(KPIS)) {
          if (index > 0) {
            builder.append(" and ");
          }
          builder.append(key);
          builder.append("=?");

          selectFields.put(key, null);
          selectFieldList.add(key);
        }
        index++;
      }
      return builder.toString();
    }

    @Deprecated
    private String buildInsertSql_old() {
      StringBuilder builder = new StringBuilder();
      builder.append("insert into ");
      builder.append("\"" + table + "\"");
      builder.append(" (");
      int index = 0;
      for (Map.Entry<String, List<String>> entry : schemas.entrySet()) {
        String parentField = entry.getKey();
        List<String> childFields = entry.getValue();
        if (index > 0) {
          builder.append(COMMA);
        }
        builder.append(parentField);

        insertFields.put(parentField, childFields);
        insertFieldList.add(parentField);
        index++;
      }

      builder.append(") values(");
      for (int i = 0; i < schemas.keySet().size(); i++) {
        builder.append("?");
        if (i < schemas.size() - 1) {
          builder.append(COMMA);
        }
      }
      builder.append(")");
      return builder.toString();
    }

    @Deprecated
    private String buildUpdateSql_old() {
      if (accAttrs.isEmpty() && replaceAttrs.isEmpty() && greatestAttrs.isEmpty() && leastAttrs
          .isEmpty() && jsonReplaceAttrs.isEmpty() && jsonAccAttrs.isEmpty() && jsonUndoAccAttrs
          .isEmpty()) {
        return null;
      }
      StringBuilder builder = new StringBuilder();
      builder.append("update ");
      builder.append("\"" + table + "\"");
      builder.append(" set ");

      for (int index = 0; index < accAttrs.size(); index++) {
        String field = accAttrs.get(index);
        builder.append(field);
        builder.append("=");
        builder.append("COALESCE(");
        builder.append(field);
        builder.append(",0)");
        builder.append("+?");

        if (index < accAttrs.size() - 1) {
          builder.append(COMMA);
        }

        updateFields.put(field, null);
        updateFieldList.add(field);
      }

      if (replaceAttrs != null && !replaceAttrs.isEmpty()) {
        replaceAttrs.removeAll(accAttrs);
        for (int i = 0; i < replaceAttrs.size(); i++) {
          String field = replaceAttrs.get(i);
          if (i > 0 || !accAttrs.isEmpty()) {
            builder.append(COMMA);
          }
          builder.append(field);
          builder.append("=");
          builder.append("?");
          updateFields.put(field, null);
          updateFieldList.add(field);
        }
      }

      if (greatestAttrs != null && !greatestAttrs.isEmpty()) {
        greatestAttrs.removeAll(accAttrs);
        greatestAttrs.removeAll(replaceAttrs);
        for (int i = 0; i < greatestAttrs.size(); i++) {
          if (i > 0 || !replaceAttrs.isEmpty()) {
            builder.append(COMMA);
          }
          String field = greatestAttrs.get(i);
          builder.append(field);
          builder.append("=GREATEST(");
          builder.append(field);
          builder.append(COMMA);
          builder.append("?)");
          updateFields.put(field, null);
          updateFieldList.add(field);
        }
      }

      if (leastAttrs != null && !leastAttrs.isEmpty()) {
        leastAttrs.removeAll(accAttrs);
        leastAttrs.removeAll(replaceAttrs);
        leastAttrs.removeAll(greatestAttrs);
        for (int i = 0; i < leastAttrs.size(); i++) {
          if (i > 0 || !greatestAttrs.isEmpty()) {
            builder.append(COMMA);
          }
          String field = leastAttrs.get(i);
          builder.append(field);
          builder.append("=LEAST(");
          builder.append(field);
          builder.append(COMMA);
          builder.append("?)");
          updateFields.put(field, null);
          updateFieldList.add(field);
        }
      }
      int i = 0;
      if (jsonAccAttrs != null && !jsonAccAttrs.isEmpty()) {
        for (Map.Entry<String, List<String>> entry : jsonAccAttrs.entrySet()) {
          if (i > 0 || !leastAttrs.isEmpty()) {
            builder.append(COMMA);
          }
          String parentField = entry.getKey();
          final List<String> childFields = entry.getValue();
          if (builder.toString().contains(parentField)) {
            continue;
          }
          builder.append(parentField);
          builder.append("=");
          builder.append("?");
          updateFields.put(parentField, childFields);
          updateFieldList.add(parentField);
          i++;
        }
      }

      int j = 0;
      Map<String, List<String>> jsonReplaceAttrMap = new HashMap<>(jsonReplaceAttrs);
      for (String key : updateFields.keySet()) {
        if (jsonReplaceAttrs.containsKey(key)) {
          List<String> list = new ArrayList<>(updateFields.get(key));
          List<String> childFields = jsonReplaceAttrs.get(key);
          list.addAll(childFields);
          updateFields.put(key, list);
          jsonReplaceAttrMap.remove(key);
        }
      }
      if (jsonReplaceAttrMap != null && !jsonReplaceAttrMap.isEmpty()) {
        for (Map.Entry<String, List<String>> entry : jsonReplaceAttrMap.entrySet()) {
          if (j > 0 || !jsonAccAttrs.isEmpty()) {
            builder.append(COMMA);
          }
          String parentField = entry.getKey();
          final List<String> childFields = entry.getValue();
          builder.append(parentField);
          builder.append("=");
          builder.append("?");
          updateFields.put(parentField, childFields);
          updateFieldList.add(parentField);
          j++;
        }
      }

      int k = 0;
      Map<String, List<String>> jsonNudoAttrMap = new HashMap<>(jsonUndoAccAttrs);
      for (String key : updateFields.keySet()) {
        if (jsonUndoAccAttrs.containsKey(key)) {
          List<String> list = new ArrayList<>(updateFields.get(key));
          List<String> childFields = jsonUndoAccAttrs.get(key);
          list.addAll(childFields);
          updateFields.put(key, list);
          jsonNudoAttrMap.remove(key);
        }
      }
      if (jsonReplaceAttrMap != null && !jsonNudoAttrMap.isEmpty()) {
        for (Map.Entry<String, List<String>> entry : jsonReplaceAttrMap.entrySet()) {
          if (k > 0 || !jsonReplaceAttrMap.isEmpty()) {
            builder.append(COMMA);
          }

          String parentField = entry.getKey();
          final List<String> childFields = entry.getValue();
          builder.append(parentField);
          builder.append("=");
          builder.append("?");
          updateFieldList.add(parentField);
          updateFields.put(parentField, childFields);
          k++;
        }
      }
      if (updateFields.size() == 0) {
        return null;
      }
      builder.append(" where ");

      String[] insetKeyTmp = insertKey.split(COMMA);
      for (int index = 0; index < insetKeyTmp.length; index++) {
        String field = insetKeyTmp[index];
        builder.append(field);
        builder.append("=?");
        if (index < insetKeyTmp.length - 1) {
          builder.append(" and ");
        }
        updateFields.put(field, null);
        updateFieldList.add(field);
      }

      return builder.toString();
    }

    @Deprecated
    private String buildSelectSql_old() {
      StringBuilder builder = new StringBuilder();
      builder.append("select * from ");
      builder.append("\"" + table + "\"");
      builder.append(" where ");

      String[] temp = insertKey.split(COMMA);
      for (int index = 0; index < temp.length; index++) {
        String field = temp[index];
        if (index > 0) {
          builder.append(" and ");
        }

        builder.append(field);
        builder.append("=?");

        selectFields.put(field, null);
        selectFieldList.add(field);
      }

      builder.append(" limit 1");
      return builder.toString();
    }

  }

  private PreparedStatement fulfillSelectSql(PreparedStatement statement, List<String> fields,
                                             RcUnit record) throws Exception {
    for (int index = 0; index < fields.size(); index++) {
      switch (fields.get(index)) {
        case OBJECTID:
          statement.setObject(index + 1, record.getAttr(objectid));
          break;
        case TS:
          Object value = record.getAttr(ts);
          if (value instanceof String) {
            statement.setObject(index + 1,
                Timestamp.valueOf(timeConverter.convert((String) value, zoneId)));
          } else if (value instanceof Long) {
            statement.setObject(index + 1,
                Timestamp.valueOf(timeConverter.convert((Long) value, zoneId,
                    startMonth, startDay, startHour, startMinute)));
          } else {
            Timestamp timestamp = (Timestamp) value;
            statement.setObject(index + 1,
                Timestamp.valueOf(timeConverter.convert(timestamp.getTime(), zoneId,
                    startMonth, startDay, startHour, startMinute)));
          }
          break;
        case TIMEBASE:
          statement.setObject(index + 1, timebase);
          break;
        case GROUPID:
          statement.setObject(index + 1, groupid);
          break;
        case GROUPBASE:
          statement.setObject(index + 1, groupbase);
          break;
        case TENANTID:
          statement.setObject(index + 1, getTenantId());
          break;
        default:
          break;
      }
    }
    return statement;
  }

  private String kpis(RcUnit record, List<String> keys) {
    StringBuilder builder = new StringBuilder();

    builder.append("{");
    int j = 0;
    for (String jsonKey : keys) {
      builder.append(jsonKey);
      builder.append(":");

      Object value = record.getAttr(jsonKey.substring(1, jsonKey.length() - 1));
      if (value instanceof String) {
        value = "\"" + value + "\"";
      }
      builder.append(value);

      if (j < keys.size() - 1) {
        builder.append(COMMA);
      }
      j++;
    }
    builder.append("}");
    return builder.toString();
  }


  private void fulfillInsertSql(PreparedStatement statement, List<String> fields,
                                Map<String, String> fieldMapper, List<String> keys, RcUnit record)
      throws Exception {
    try {
      for (int index = 0; index < fields.size(); index++) {
        String key = fields.get(index);
        Object value = null;

        if (fieldMapper.keySet().contains(key)) {
          value = record.getAttr(fieldMapper.get(key));
        } else if (key.equals(KPIS)) {
          value = kpis(record, keys);
          logger.debug("[kpis json string] {}", value.toString());
        } else {
          value = record.getAttr(key);
          if (value == null) {
            value = record.getAttr(key.substring(1, key.length() - 1));
            if (value instanceof String) {
              value = "\"" + value + "\"";
            }
          }
        }

        //preprocess time attribute : discrete time for time aggregation
        if (timeAttr != null && key.equals(timeAttr.getKey())) {
          if (value instanceof String) {
            statement.setObject(index + 1,
                Timestamp.valueOf(timeConverter.convert((String) value, zoneId)));
          } else if (value instanceof Long) {
            statement.setObject(index + 1,
                Timestamp.valueOf(timeConverter.convert((Long) value, zoneId,
                    startMonth, startDay, startHour, startMinute)));
          } else {
            Timestamp timestamp = (Timestamp) value;
            statement.setObject(index + 1,
                Timestamp.valueOf(timeConverter.convert(timestamp.getTime(), zoneId,
                    startMonth, startDay, startHour, startMinute)));
          }
        } else {
          if (TIMEBASE.equals(key)) {
            statement.setObject(index + 1, timebase);
          } else if (GROUPID.equals(key)) {
            statement.setObject(index + 1, groupid);
          } else if (TENANTID.equals(key)) {
            statement.setObject(index + 1, getTenantId());
          } else if (GROUPBASE.equals(key)) {
            statement.setObject(index + 1, groupbase);
          } else {
            statement.setObject(index + 1, value);
          }
        }
      }
    } catch (PSQLException e) {
      logger.debug("[SQL EXCEPTION]: {}", e.toString());
      throw e;
    }
  }

  private PreparedStatement fulfillUpdateSql(PreparedStatement statement, List<String> fields,
                                             Map<String, String> fieldMapper, RcUnit record)
      throws Exception {
    logger.debug("[updatesql ? fields] {}", fields.toString());
    for (int index = 0; index < fields.size(); index++) {
      String key = fields.get(index);

      Object value = null;
      switch (key) {
        case OBJECTID:
          value = record.getAttr(fieldMapper.get(key));
          break;
        case TS:
          value = record.getAttr(fieldMapper.get(key));
          if (value instanceof String) {
            value = Timestamp.valueOf(timeConverter.convert((String) value, zoneId));
          } else if (value instanceof Long) {
            value = Timestamp.valueOf(timeConverter.convert((Long) value, zoneId,
                startMonth, startDay, startHour, startMinute));
          } else {
            Timestamp timestamp = (Timestamp) value;
            value = Timestamp.valueOf(timeConverter.convert(timestamp.getTime(), zoneId,
                startMonth, startDay, startHour, startMinute));
          }
          break;
        case TIMEBASE:
          value = timebase;
          break;
        case GROUPID:
          value = groupid;
          break;
        case GROUPBASE:
          value = groupbase;
          break;
        case TENANTID:
          value = getTenantId();
          break;
        default:
          value = record.getAttr(key.substring(1, key.length() - 1));

          if (value instanceof String) {
            value = "\"" + value + "\"";
          }

          if (value != null) {
            value = value.toString();
          }

          break;
      }
      statement.setObject(index + 1, value);
    }
    return statement;
  }


  @Deprecated
  private void buildSql(PreparedStatement statement,
                        RcUnit record,
                        List<String> fiels,
                        Map<String, List<String>> fieldList,
                        Map<String, String> schema,
                        Map<String, Object> results)
      throws SQLException {
    for (Map.Entry<String, List<String>> entry : fieldList.entrySet()) {
      String parentField = entry.getKey();
      List<String> childFields = entry.getValue();
      int index = fiels.indexOf(parentField) + 1;
      if (childFields == null || childFields.isEmpty()) {
        Object itemValue = record.getAttr(attributeMapper.get(parentField));
        itemValue = itemValue == null && accAttrs.contains(parentField) ? 0 : itemValue;
        if (timeAggregationFlag) {

          if (timeAttr != null && parentField.equals(timeAttr.getKey())) {
            if (itemValue instanceof String) {
              statement.setObject(index,
                  Timestamp.valueOf(timeConverter.convert((String) itemValue, zoneId)));
            } else if (itemValue instanceof Long) {
              statement.setObject(index,
                  Timestamp.valueOf(timeConverter.convert((Long) itemValue, zoneId,
                      startMonth, startDay, startHour, startMinute)));
            } else {
              Timestamp timestamp = (Timestamp) itemValue;
              statement.setObject(index,
                  Timestamp.valueOf(timeConverter.convert(timestamp.getTime(), zoneId,
                      startMonth, startDay, startHour, startMinute)));
            }

          } else {
            if (TIMEBASE.equals(parentField)) {
              statement.setObject(index, timebase);
            } else if (GROUPID.equals(parentField)) {
              statement.setObject(index, groupid);
            } else if (TENANTID.equals(parentField)) {
              statement.setObject(index, getTenantId());
            } else if (GROUPBASE.equals(parentField)) {
              statement.setObject(index, groupbase);
            } else {
              statement.setObject(index, itemValue);
            }
          }

        } else {
          String fidldName = schema.get(parentField).toLowerCase();

          if (fidldName.contains("(")) {
            fidldName = schema.get(parentField).toLowerCase().substring(0,
                schema.get(parentField).indexOf("("));
          }
          if (fidldName.contains(" ")) {
            fidldName = schema.get(parentField).toLowerCase().substring(0,
                schema.get(parentField).indexOf(" "));
          }
          switch (fidldName) {
            case "datetime":
            case "timestamp":
            case "date":
            case "time":
              statement.setObject(index, Timestamp.valueOf(DateUtil.formatNormalDateString(
                  itemValue instanceof Timestamp ? ((Timestamp) itemValue).getTime()
                      : (Long) itemValue, "yyyy-MM-dd HH:mm:ss.SSS")));
              continue;
            default:
              statement.setObject(index, itemValue);
              continue;
          }
        }
      } else {
        //拼接kpis json string
        Map<String, Object> values = Maps.newHashMap();
        for (String childField : childFields) {
          try {
            Object childValue = null;
            Object itemValue = record.getAttr(attributeMapper.get((parentField
                + DOT + childField).replace("\".\"", DOT)));
            if (jsonAccAttrs.containsKey(parentField)) {
              List<String> jsonAccAttrsChildFileds = jsonAccAttrs.get(parentField);
              if (jsonAccAttrsChildFileds.contains(childField)) {
                if (results == null || !results.containsKey(parentField.replace("\"", ""))) {
                  childValue = 0;
                } else {
                  PGobject pgobject = (PGobject) results.get(parentField.replace("\"", ""));
                  JSONObject jsonObject = JSONObject.parseObject(pgobject.getValue());
                  childValue = jsonObject.getOrDefault(childField.replace("\"", ""), 0);
                }
                childValue = childValue == null ? "0.00" : childValue;
                itemValue = itemValue == null ? 0 : itemValue;
                itemValue = new BigDecimal(childValue.toString()).add(
                    new BigDecimal(itemValue == null ? "0" : itemValue.toString()));
              }
            }

            if (jsonReplaceAttrs.containsKey(parentField)) {
              List<String> jsonReplaceAttrsChildFileds = jsonReplaceAttrs.get(parentField);
              if (jsonReplaceAttrsChildFileds.contains(childField)) {
                PGobject pgobject = (PGobject) results
                    .get(parentField.replace("\"", ""));//Null pointer exception
                JSONObject jsonObject = JSONObject.parseObject(pgobject.getValue());
                childValue = jsonObject.get(childField.replace("\"", ""));
                itemValue = itemValue == null ? childValue : itemValue;
              }
            }

            if (jsonUndoAttrs.containsKey(parentField)) {
              List<String> jsonUndoAttrsChildFields = jsonUndoAttrs.get(parentField);
              if (jsonUndoAttrsChildFields.contains(childField)) {
                PGobject pgobject = (PGobject) results
                    .get(parentField.replace("\"", ""));//Null pointer exception
                JSONObject jsonObject = JSONObject.parseObject(pgobject.getValue());
                childValue = jsonObject.get(childField.replace("\"", ""));
                itemValue = childValue == null ? itemValue : childValue;
              }
            }
            values.put(childField.replace("\"", ""), itemValue);
          } catch (Exception e) {
            logger.error("error", e);
          }

        }
        String kpis = JSONObject.toJSONString(values);
        statement.setObject(index, kpis);
      }
    }

  }

  @Override
  public void registerOutput(IotWorkRuntimeContext context) {
    DataStream<RcUnit> priorDataStream = context.getObjectFromContainer(getPriorNodeId());
    priorDataStream
        .addSink(new AssetSinkWapper.AssetRichSinkFunction(getJobId(),
            getTenantId(), getLogType(), getKafkaLoggingServiceServers(),
            getKafkaLoggingServiceTopics())).uid(getNodeId())
        .setParallelism(getParallelism())
        .name(getNodeName());
  }

  private class AssetRichSinkFunction extends JdbcRichSinkFunction<RcUnit> {

    private final String jobId;
    private final String tenantId;
    private Counter counterRecordsOut;
    private final GraylogLogTypeEnum logType;
    private final String kafkaLoggingServiceServers;
    private final String kafkaLoggingServiceTopics;

    private AssetRichSinkFunction(String jobId, String tenantId, GraylogLogTypeEnum logType,
                                  String kafkaLoggingServiceServers,
                                  String kafkaLoggingServiceTopics) {
      super(new PostgreSqlConnWrapper("org.postgresql.Driver",
          uri, username, password,
          "select 1", table, null, 0, null));
      this.jobId = jobId;
      this.tenantId = tenantId;
      this.logType = logType;
      this.kafkaLoggingServiceServers = kafkaLoggingServiceServers;
      this.kafkaLoggingServiceTopics = kafkaLoggingServiceTopics;
    }

    // Postgre Sql table schema
    private Map<String, String> schema;

    @Override
    public void open(Configuration parameters) throws Exception {
      super.open(parameters);
      initSchema();
      counterRecordsOut = getRuntimeContext().getMetricGroup()
          .counter(JobConstant.FLINK_METRIC_NUM_RECORDS_OUT_OVERWRITE);
      openProducer(kafkaLoggingServiceServers);
    }

    private void initSchema() throws SQLException {
      schema = Maps.newHashMap();
      String format = String.format(SHOW_COLUMN, table);
      try (PreparedStatement preparedStatement = getConnectionWrapper()
          .prepareStatement(format)) {
        try (ResultSet rs = preparedStatement.executeQuery()) {
          while (rs.next()) {
            schema.put(SchemaConstant.DOUBLE_QUOTE + rs.getString("Field")
                + SchemaConstant.DOUBLE_QUOTE, rs.getString("Type"));
          }
        }
      } catch (Exception ex) {
        logger.error("Exception occurred when try to init schema, exception: {}", ex.toString());
        LoggingServiceUtil.alarm(LogVo.builder()
            .k8sServiceName(GrayLogConstant.K8S_SERVICE_NAME)
            .module(GrayLogConstant.MODULE_NAME)
            .userName(tenantId)
            .tenantId(tenantId)
            .operateObjectName(getJobName())
            .operateObject(jobId)
            .shortMessage(String.format("Failed to init the schema within AssetRichSinkFunction."
                + " Cause: %s. Database connection string: %s. Table: %s.",ex.toString(),uri,table))
            .operation(ShortMessageConstant.INIT_SCHEMA)
            .requestId(String.valueOf(System.currentTimeMillis()))
            .result(GrayLogResultEnum.FAIL.getValue())
            .logType(logType)
            .kafkaLoggingServiceTopic(kafkaLoggingServiceTopics)
            .kafkaLoggingServiceServers(kafkaLoggingServiceServers)
            .build(), flinkKafkaInternalProducer);
      }
      logger.debug("Postgre sql table schema: {}", schema);
    }

    @Override
    public void invoke(RcUnit record, Context context) {
      logger.debug("postgre sql sink process element: {}", record);
      try {
        if (INSERT.name().equals(insertType)) {
          insert(record);
        } else if (UPSERT.name().equals(insertType)) {
          try {
            upsert(record);
          } catch (SQLException sqlException) {
            // Duplicate entry, try again.
            if (RETRY_SQL_STATES.contains(sqlException.getSQLState())) {
              logger.debug("Duplicate entry, try again, record: {}", record);
              upsert(record);
            } else {
              throw sqlException;
            }
          }

        } else {
          logger.error("Invalid insertType: {}", insertType);
        }
      } catch (Exception e) {
        logger.error("Exception occurred when try to insert/update {} to {}, exception: {}",
            record, table, e.getMessage(), e);
        LoggingServiceUtil.alarm(LogVo.builder()
            .k8sServiceName(GrayLogConstant.K8S_SERVICE_NAME)
            .module(GrayLogConstant.MODULE_NAME)
            .userName(tenantId)
            .tenantId(tenantId)
            .operateObjectName(getJobName())
            .operateObject(jobId)
            .shortMessage(String.format("Failed to insert into the Postgresql table %s in node %s."
                + " Cause: %s. Database connection string: %s. Table: %s.",
                table,getNodeName(), e.toString(),uri,table))
            .operation(ShortMessageConstant.INSERT_POSTGRESQL_DATA)
            .requestId(String.valueOf(System.currentTimeMillis()))
            .result(GrayLogResultEnum.FAIL.getValue())
            .logType(logType)
            .kafkaLoggingServiceTopic(kafkaLoggingServiceTopics)
            .kafkaLoggingServiceServers(kafkaLoggingServiceServers)
            .build(), flinkKafkaInternalProducer);
      }
    }

    //备注：资产节点不会调用insert方法：资产节点默认upsert
    private void insert(RcUnit record) throws Exception {
      try (PreparedStatement insert = getConnectionWrapper().prepareStatement(insertSql);) {
        buildSql(insert, record, insertFieldList, insertFields, schema, null);
        insert.execute();
      }
      counterRecordsOut.inc();
    }

    private void upsert(RcUnit record) throws Exception {
      try (PreparedStatement select = getConnectionWrapper().prepareStatement(selectSql)) {
        //buildSql(select, record, selectFieldList, selectFields, schema, null);
        fulfillSelectSql(select, selectFieldList, record);
        logger.debug("[SELECT SQL] " + select.toString());
        try (ResultSet resultSet = select.executeQuery()) {
          logger.debug("Successfully execute asset sink postgresql select: {}",
              select.toString());

          if (!resultSet.next()) {
            //If record does not exist in the databse, then INSERT
            try (PreparedStatement insert = getConnectionWrapper().prepareStatement(insertSql);) {
              //buildSql(insert, record, insertFieldList, insertFields, schema, null);
              logger.debug("[INSERT SQL with ?] " + insert.toString());
              fulfillInsertSql(insert, insertFieldList, fieldMapper, kpisFiels, record);
              logger.debug("[INSERT SQL] " + insert.toString());
              insert.execute();
              counterRecordsOut.inc();
              logger.debug("Successfully execute asset sink postgresql insert: {}",
                  insert.toString());
            }
          } else {
            //If record exists in the database, then UPDATE
            /*
            try (ResultSet result = select.executeQuery()) {
              List<Map<String, Object>> maps = convertList(result);
              if (Strings.isNullOrEmpty(updateSql)) {
                logger.warn("Update sql is empty.");
              } else {
                try (PreparedStatement update = getConnectionWrapper()
                    .prepareStatement(updateSql)) {
                  buildSql(update, record, updateFieldList, updateFields, schema, maps.get(0));
                  update.executeUpdate();
                }
              }
            }
             */
            if (Strings.isNullOrEmpty(updateSql)) {
              logger.debug("Update sql is empty.");
            } else {
              try (PreparedStatement update = getConnectionWrapper()
                  .prepareStatement(updateSql)) {
                logger.debug("[UPDATE SQL with ?] " + update.toString());
                fulfillUpdateSql(update, updateFieldList, fieldMapper, record);
                logger.debug("[UPDATE SQL] " + update.toString());
                try {
                  update.executeUpdate();
                  counterRecordsOut.inc();
                  logger.debug("Successfully execute asset sink postgresql update: {}",
                      update.toString());
                } catch (Exception e) {
                  logger.error(
                      "Failed to execute asset sink postgresql update: {}, Exception message: {}",
                      update.toString(), e.toString());
                }
              }
            }
          }
        }
      }
    }

    private List<Map<String, Object>> convertList(ResultSet rs) throws SQLException {
      List<Map<String, Object>> list = new ArrayList();
      ResultSetMetaData md = rs.getMetaData();//获取键名
      int columnCount = md.getColumnCount();//获取行的数量
      while (rs.next()) {
        Map<String, Object> rowData = Maps.newHashMap();//声明Map
        for (int i = 1; i <= columnCount; i++) {
          rowData.put(md.getColumnName(i), rs.getObject(i));//获取键名及值
        }
        list.add(rowData);
      }
      return list;
    }

    @Override
    public void close() throws Exception {

      LoggingServiceUtil.info(LogVo.builder()
          .k8sServiceName(GrayLogConstant.K8S_SERVICE_NAME)
          .module(GrayLogConstant.MODULE_NAME)
          .userName(tenantId)
          .tenantId(tenantId)
          .userId(getUserId())
          .operateObjectName(getJobName())
          .operateObject(jobId)
          .shortMessage(String.format("Node %s disconnected from Postgresql."
              + " Database connection string: %s. Table: %s.",getNodeName(),uri,table))
          .operation(ShortMessageConstant.CLOSE_POSTGRESQL)
          .requestId(String.valueOf(System.currentTimeMillis()))
          .result(GrayLogResultEnum.SUCCESS.getValue())
          .logType(logType)
          .kafkaLoggingServiceTopic(kafkaLoggingServiceTopics)
          .kafkaLoggingServiceServers(kafkaLoggingServiceServers)
          .build(), flinkKafkaInternalProducer);
      super.close();
      schema = null;
      logger.info("PostgreSql sink is closed.");
    }

  }

}
