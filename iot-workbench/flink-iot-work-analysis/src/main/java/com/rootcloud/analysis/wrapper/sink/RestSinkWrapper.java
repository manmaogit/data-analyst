/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.wrapper.sink;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.rootcloud.analysis.common.context.IotWorkRuntimeContext;
import com.rootcloud.analysis.common.entity.RcUnit;
import com.rootcloud.analysis.common.factory.KafkaLogMiddlewareResourceFactory;
import com.rootcloud.analysis.core.constants.GrayLogConstant;
import com.rootcloud.analysis.core.constants.JobConstant;
import com.rootcloud.analysis.core.constants.ShortMessageConstant;
import com.rootcloud.analysis.core.enums.GrayLogResultEnum;
import com.rootcloud.analysis.core.enums.GraylogLogTypeEnum;
import com.rootcloud.analysis.core.graylog.LogVo;
import com.rootcloud.analysis.log.loggingservice.LoggingServiceUtil;
import com.rootcloud.analysis.wrapper.api.RestSinkConnWrapper;
import com.rootcloud.analysis.wrapper.api.RestSinkRichFunction;

import java.time.Duration;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import lombok.Builder;
import lombok.Getter;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.metrics.Counter;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.functions.sink.SinkFunction;
import org.apache.flink.streaming.connectors.kafka.internals.FlinkKafkaInternalProducer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * IDF-设备台账-输出节点.
 *
 * @CreatedBy: zexin.huang
 * @Date: 2022/6/2 11:21
 */
@Getter
@Builder
public class RestSinkWrapper extends AbstractSinkWrapper {

  private static Logger logger = LoggerFactory.getLogger(RestSinkWrapper.class);

  private final String url;
  private final JSONArray headers;
  private final int timeOutSeconds;
  /**
   * Key：输出字段名，Value：输入字段名.
   */
  private final Map<String, String> attributeMapper;
  /**
   * 插入键.
   */
  private final List<String> insertKeys;
  /**
   * 非空键.
   */
  private final List<String> notNullKeys;

  private final boolean isStream;
  /**
   * IDF批量写入的每批数量大小.
   */
  private final int batchSize;
  /**
   * IDF批量写入的每批时间窗口大小（单位：秒）.
   */
  private final int timeWindowSeconds;

  @Override
  public void registerOutput(IotWorkRuntimeContext context) {
    DataStream<RcUnit> priorDataStream = context.getObjectFromContainer(getPriorNodeId());

    priorDataStream.addSink(
            new RestRichSinkFunction(getJobId(), getTenantId(), getUserId(), getUrl(), getHeaders(),
                getTimeOutSeconds(), getAttributeMapper(),
                getInsertKeys(), getNotNullKeys(),
                isStream(), getBatchSize(), getTimeWindowSeconds(),
                getLogType(), getJobName(), getNodeName(),
                getKafkaLoggingServiceServers(), getKafkaLoggingServiceTopics()))
        .setParallelism(1)
        .uid(getNodeId())
        .name(getNodeName());
  }

  static class RestRichSinkFunction extends RestSinkRichFunction<RcUnit> {

    private final String keyFieldId = "fieldId";

    private final String keyFieldValue = "fieldValue";

    private final String jobId;

    private final String jobName;

    private final String nodeName;

    private final String tenantId;

    private final String userId;

    private final GraylogLogTypeEnum logType;

    private final Map<String, String> attributeMapper;

    private final List<String> insertKeys;

    private final List<String> notNullKeys;

    private final boolean isStream;

    private final int batchSize;

    private final int timeWindowSeconds;
    private long timeWindowStartMillis = -1L;

    private final List<Map> dataList = new LinkedList<>();

    private Counter counterRecordsOut;

    private final String kafkaLoggingServiceServers;
    private final String kafkaLoggingServiceTopics;
    private FlinkKafkaInternalProducer flinkKafkaInternalProducer;

    RestRichSinkFunction(String jobId, String tenantId, String userId,String url, JSONArray headers,
                         int timeOutSeconds,
                          Map<String, String> attributeMapper,
                         List<String> insertKeys, List<String> notNullKeys,
                         boolean isStream,
                         int batchSize, int timeWindowSeconds,
                         GraylogLogTypeEnum logType,
                         String jobName, String nodeName,
                         String kafkaLoggingServiceServers,
                         String kafkaLoggingServiceTopics) {
      super(new RestSinkConnWrapper(url, headers, timeOutSeconds));
      this.jobId = jobId;
      this.tenantId = tenantId;
      this.userId = userId;
      this.attributeMapper = attributeMapper;
      this.insertKeys = insertKeys;
      this.notNullKeys = notNullKeys;
      this.isStream = isStream;
      this.batchSize = batchSize;
      this.timeWindowSeconds = timeWindowSeconds;
      this.logType = logType;
      this.jobName = jobName;
      this.nodeName = nodeName;
      this.kafkaLoggingServiceServers = kafkaLoggingServiceServers;
      this.kafkaLoggingServiceTopics = kafkaLoggingServiceTopics;
    }

    @Override
    public void open(Configuration parameters) throws Exception {
      super.open(parameters);
      counterRecordsOut = getRuntimeContext().getMetricGroup()
          .counter(JobConstant.FLINK_METRIC_NUM_RECORDS_OUT_OVERWRITE);
      openProducer();
    }

    @Override
    public synchronized void invoke(RcUnit record, SinkFunction.Context context) throws Exception {
      logger.debug("RestSink process element: {}", record);

      Map<String, Object> data = new HashMap<>(record.getData().size() * 2);
      for (String outputColumnName : attributeMapper.keySet()) {
        String inputColumnName = attributeMapper.get(outputColumnName);
        Object value = record.getAttr(inputColumnName);
        data.put(outputColumnName, value);
      }
      if (validateDataKeys(data)) {
        dataList.add(data);
        // 自定义的输出指标数据.
        counterRecordsOut.inc();
      }

      if (isStream) {
        // 实时任务 - 按时间窗口大小批量写入.
        long currentTimeMillis = System.currentTimeMillis();
        if (timeWindowStartMillis > 0L) {
          if (currentTimeMillis - timeWindowStartMillis >= timeWindowSeconds * 1000) {
            flush();
          }
        }
        timeWindowStartMillis = currentTimeMillis;

      } else {
        // 离线任务 - 按数据量大小批量写入.
        if (dataList.size() >= batchSize) {
          flush();
        }
      }
    }

    /**
     * 校验待输出的数据是否包含全部插入键字段和非空字段.
     */
    private boolean validateDataKeys(Map<String, Object> data) {
      // 插入键字段.
      for (String insertKey : insertKeys) {
        if (!data.containsKey(insertKey)) {
          logger.error("data don't contains insertKey --> {}, data = {}", insertKey, data);
          return false;
        }
      }
      // 非空字段.
      for (String notNullKey : notNullKeys) {
        if (!data.containsKey(notNullKey)) {
          logger.error("data don't contains notNullKey --> {}, data = {}", notNullKey, data);
          return false;
        }
      }
      return true;
    }

    private void flush() throws Exception {
      if (dataList.size() == 0) {
        return;
      }
      try {
        // 拼装成IDF接受的入参格式.
        JSONArray jsonArray = new JSONArray();
        for (Map<String, Object> data : dataList) {
          JSONArray subJsonArray = new JSONArray();
          for (String key : data.keySet()) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(keyFieldId, key);
            jsonObject.put(keyFieldValue, data.get(key));
            subJsonArray.add(jsonObject);
          }
          jsonArray.add(subJsonArray);
        }
        // 开始批量写入IDF.
        getRestSinkConnWrapper().outputData(jsonArray);
        logger.info("successfully execute rest batch upsert dataList size: {}", dataList.size());
      } catch (Exception e) {
        String url = getRestSinkConnWrapper().getUrl();
        logger.error("Exception occurred when try to rest batch upsert to {}, exception: {}",
             url, e.getMessage(), e);
        LoggingServiceUtil.alarm(LogVo.builder()
            .k8sServiceName(GrayLogConstant.K8S_SERVICE_NAME)
            .module(GrayLogConstant.MODULE_NAME)
            .userName(tenantId)
            .userId(userId)
            .tenantId(tenantId)
            .operateObjectName(jobName)
            .operateObject(jobId)
            .shortMessage(
                String.format("%s rest batch upsert failed：%s, URL = %s", nodeName, e, url))
            .operation(ShortMessageConstant.UPSERT_REST_DATA)
            .requestId(String.valueOf(System.currentTimeMillis()))
            .result(GrayLogResultEnum.FAIL.getValue())
            .logType(logType)
            .kafkaLoggingServiceTopic(kafkaLoggingServiceTopics)
            .kafkaLoggingServiceServers(kafkaLoggingServiceServers)
            .build(), flinkKafkaInternalProducer);
        throw e;
      } finally {
        dataList.clear();
      }
    }

    @Override
    public void close() throws Exception {
      try {
        flush();
        LoggingServiceUtil.info(LogVo.builder()
            .k8sServiceName(GrayLogConstant.K8S_SERVICE_NAME)
            .module(GrayLogConstant.MODULE_NAME)
            .userName(tenantId)
            .userId(userId)
            .tenantId(tenantId)
            .operateObjectName(jobName)
            .operateObject(jobId)
            .shortMessage(
                String.format("%s rest batch upsert http closed successfully.", nodeName))
            .operation(ShortMessageConstant.CLOSE_REST_HTTP)
            .requestId(String.valueOf(System.currentTimeMillis()))
            .result(GrayLogResultEnum.SUCCESS.getValue())
            .logType(logType)
            .kafkaLoggingServiceTopic(kafkaLoggingServiceTopics)
            .kafkaLoggingServiceServers(kafkaLoggingServiceServers)
            .build(), flinkKafkaInternalProducer);
        logger.info("RestSink is closed.");
      } catch (Exception e) {
        throw e;
      } finally {
        KafkaLogMiddlewareResourceFactory.close(this.toString());
        flinkKafkaInternalProducer = null;
        closeProducer();
        super.close();
      }
    }

    /**
     * openProducer.
     */
    private void openProducer() {
      if (flinkKafkaInternalProducer == null && kafkaLoggingServiceServers != null) {
        flinkKafkaInternalProducer = KafkaLogMiddlewareResourceFactory.getProducer(
            kafkaLoggingServiceServers,
            this.toString());
      }
    }

    /**
     * 关闭 kafka Producer.
     */
    private void closeProducer() {
      if (flinkKafkaInternalProducer != null) {
        flinkKafkaInternalProducer.close(Duration.ofSeconds(3));
        flinkKafkaInternalProducer = null;
      }
    }
  }

}
