/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.wrapper.processor;

import com.rootcloud.analysis.common.context.IotWorkRuntimeContext;
import com.rootcloud.analysis.core.enums.GraylogLogTypeEnum;
import com.rootcloud.analysis.wrapper.AbstractWrapper;
import java.util.Map;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@ToString(callSuper = true)
public abstract class AbstractProcessorWrapper extends AbstractWrapper {

  private String priorNodeId;

  private String processorType;

  private int sequence = Integer.MAX_VALUE;

  protected AbstractProcessorWrapper(String jobId, String jobName,
                                     String tenantId, String nodeId,
                                     Integer parallelism, String nodeName, String priorNodeId,
                                     String processorType, GraylogLogTypeEnum logType) {
    super(jobId, jobName, tenantId, nodeId, parallelism, nodeName, logType);
    this.priorNodeId = priorNodeId;
    this.processorType = processorType;
  }

  public abstract void registerProcess(IotWorkRuntimeContext context) throws Exception;

  /**
   * Resets the sequence.
   */
  public void resetSequence(Map<String, AbstractProcessorWrapper> processorWrapperMap) {
    AbstractProcessorWrapper priorProcessorWrapper = processorWrapperMap.get(getPriorNodeId());
    if (priorProcessorWrapper != null) {
      priorProcessorWrapper.setSequence(Math.min(getSequence() - 1, priorProcessorWrapper.getSequence()));
      priorProcessorWrapper.resetSequence(processorWrapperMap);
    }
  }
}
