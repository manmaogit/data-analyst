/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.wrapper.function.join;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.rootcloud.analysis.common.entity.RcUnit;
import java.util.List;
import java.util.Set;
import org.apache.flink.runtime.state.HeapBroadcastState;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FullOuterJoinWrapper extends AbstractJoinWrapper {
  private static final Logger logger = LoggerFactory.getLogger(FullOuterJoinWrapper.class);

  @Override
  public List<RcUnit> join(HeapBroadcastState<Object, List<RcUnit>> dimensionState, RcUnit input,
                           JSONObject attributeMapper,
                           Set<String> dimensionAttrSet,
                           JSONArray schema, Object value) {
    return null;
  }

  @Override
  public Boolean contain(HeapBroadcastState<Object, List<RcUnit>> dimensionState, Object value,
                         JSONObject attributeMapper) {
    return null;
  }
}
