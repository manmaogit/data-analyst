/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.actuator.builder.impl;

import static com.rootcloud.analysis.core.enums.JobTypeEnum.REALTIME;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.rootcloud.analysis.actuator.builder.ActuatorBuilder;
import com.rootcloud.analysis.actuator.builder.IActuatorBuilder;
import com.rootcloud.analysis.actuator.impl.StreamActuator;
import com.rootcloud.analysis.common.context.IotWorkRuntimeContext;
import com.rootcloud.analysis.context.IotWorkRuntimeDecodeContext;
import com.rootcloud.analysis.core.exec.Actuator;
import com.rootcloud.analysis.core.graph.IGraph;
import com.rootcloud.analysis.wrapper.IWrapper;
import com.rootcloud.analysis.wrapper.processor.AbstractProcessorWrapper;
import com.rootcloud.analysis.wrapper.sink.AbstractSinkWrapper;
import com.rootcloud.analysis.wrapper.source.AbstractSourceWrapper;
import java.util.List;
import org.apache.flink.streaming.api.CheckpointingMode;
import org.apache.flink.streaming.api.environment.CheckpointConfig;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.table.api.EnvironmentSettings;
import org.apache.flink.table.api.bridge.java.StreamTableEnvironment;
import org.apache.flink.table.functions.UserDefinedFunction;

@ActuatorBuilder(jobType = REALTIME)
public class StreamActuatorBuilder implements IActuatorBuilder {

  private static final long CHECKPOINT_INTERVAL = 300000L;

  private static final long CHECKPOINT_TIMEOUT = 300000L;

  private static final int MAX_CONCURRENT_CHECKPOINTS = 1;

  private StreamActuator actuator = new StreamActuator();


  private static StreamTableEnvironment buildTableEnv(StreamExecutionEnvironment env) {
    EnvironmentSettings settings = EnvironmentSettings.newInstance().inStreamingMode().build();
    return StreamTableEnvironment.create(env, settings);
  }

  @Override
  public Actuator build(String jobName, String jobType, IGraph<IWrapper> graph, StreamExecutionEnvironment env) throws Exception {
    // 配置CheckPoint相关参数
    env.getCheckpointConfig().setCheckpointInterval(CHECKPOINT_INTERVAL);
    env.getCheckpointConfig().setCheckpointingMode(CheckpointingMode.EXACTLY_ONCE);
    env.getCheckpointConfig().setCheckpointTimeout(CHECKPOINT_TIMEOUT);
    env.getCheckpointConfig().setMaxConcurrentCheckpoints(MAX_CONCURRENT_CHECKPOINTS);
    env.getCheckpointConfig().enableExternalizedCheckpoints(CheckpointConfig.ExternalizedCheckpointCleanup.DELETE_ON_CANCELLATION);

    IotWorkRuntimeContext context = new IotWorkRuntimeContext();

    JSONArray udfInfos = IotWorkRuntimeDecodeContext.getUdfInfos();
    if (udfInfos != null) {
      StreamTableEnvironment tableEnv = buildTableEnv(env);
      // StreamTableEnvironment 注册 UDF
      for (Object obj : udfInfos) {
        JSONObject udfInfo = (JSONObject) obj;
        tableEnv.createTemporaryFunction(udfInfo.getString("name"),
            (UserDefinedFunction) this.getClass().getClassLoader().loadClass(udfInfo.getString("def_class")).getConstructor().newInstance());
      }

      context.putTableEnvToCache(tableEnv);
    }

    context.putExecutionEnv(env);

    // 1.生成Flink source 算子，生成DataStream，并且根据配置连接前后算子
    List<IWrapper> sources = graph.getSources();
    for (IWrapper wrapper : sources) {
      AbstractSourceWrapper inputWrapper = (AbstractSourceWrapper) wrapper;
      inputWrapper.registerInput(context);
    }

    // 2.生成Flink processor 算子，生成DataStream
    List<IWrapper> processors = graph.getProcessors();
    if (processors != null && !processors.isEmpty()) {
      for (IWrapper wrapper : processors) {
        AbstractProcessorWrapper processWrapper = (AbstractProcessorWrapper) wrapper;
        processWrapper.registerProcess(context);
      }
    }

    // 3.生成Flink sink 算子，生成DataStream
    List<IWrapper> sinks = graph.getSinks();
    for (IWrapper wrapper : sinks) {
      AbstractSinkWrapper outputWrapper = (AbstractSinkWrapper) wrapper;
      outputWrapper.registerOutput(context);
    }

    actuator.setActuatorEnv(env);
    actuator.setJobName(jobName);
    actuator.setJobType(jobType);

    return actuator;

  }
}
