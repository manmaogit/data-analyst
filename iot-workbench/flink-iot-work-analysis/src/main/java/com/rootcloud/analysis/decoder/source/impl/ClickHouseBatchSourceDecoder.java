/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.decoder.source.impl;

import com.alibaba.fastjson.JSONObject;
import com.rootcloud.analysis.context.IotWorkRuntimeDecodeContext;
import com.rootcloud.analysis.core.constants.JobConstant;
import com.rootcloud.analysis.core.enums.DataSourceTypeEnum;
import com.rootcloud.analysis.core.enums.SqlModeEnum;
import com.rootcloud.analysis.core.util.RsaUtil;
import com.rootcloud.analysis.decoder.source.AbstractSourceDecoder;
import com.rootcloud.analysis.decoder.source.SourceDecoder;
import com.rootcloud.analysis.exception.DecodeSourceException;
import com.rootcloud.analysis.wrapper.source.AbstractSourceWrapper;
import com.rootcloud.analysis.wrapper.source.ClickHouseBatchSourceWapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SourceDecoder(dataSourceType = DataSourceTypeEnum.CLICKHOUSE_INPUT)
public class ClickHouseBatchSourceDecoder extends AbstractSourceDecoder {

  private static Logger logger = LoggerFactory.getLogger(ClickHouseBatchSourceDecoder.class);

  @Override
  public AbstractSourceWrapper decodeSourceParams(JSONObject conf) {
    try {
      JSONObject params = conf.getJSONObject(JobConstant.KEY_PARAMETERS);
      JSONObject detectParams = params.getJSONObject(JobConstant.KEY_CHANGE_DETECT);
      String sql = conf.getString(JobConstant.KEY_SQL);
      ClickHouseBatchSourceWapper.Builder builder = new ClickHouseBatchSourceWapper.Builder();

      builder.setUri(params.getString(JobConstant.KEY_URI))
          .setSqlMode(SqlModeEnum.valueOf(conf.getString(JobConstant.KEY_SQL_MODE)))
          .setTableSql(conf.getString(JobConstant.KEY_TABLE_SQL))
          .setUsername(params.getString(JobConstant.KEY_USERNAME))
          .setFilterSql(sql)
          .setPassword(RsaUtil.decryptDataOnJava(params.getString(JobConstant.KEY_PASSWORD),
              IotWorkRuntimeDecodeContext.getPrivateKey()))
          .setTable(params.getString(JobConstant.KEY_TABLE))
          .setLimit(conf.getInteger(JobConstant.KEY_LIMIT) == null ? JobConstant.LIMIT_DEFAULT
              : conf.getInteger(JobConstant.KEY_LIMIT));
      ClickHouseBatchSourceWapper result = builder.build();
      logger.debug("Decoding sqlServer sql source: {}", result);
      return result;
    } catch (Exception ex) {
      logger.error("Exception occurred when decoding sqlServer source: {}", conf);
      throw new DecodeSourceException(conf.getString(JobConstant.KEY_NODE_ID),
          conf.toJSONString());
    }

  }
}