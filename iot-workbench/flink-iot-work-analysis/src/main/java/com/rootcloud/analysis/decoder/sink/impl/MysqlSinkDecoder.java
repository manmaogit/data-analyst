/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.decoder.sink.impl;

import static com.rootcloud.analysis.core.constants.WrapperConstant.DEFAULT_START_TIME;
import static com.rootcloud.analysis.core.constants.WrapperConstant.DEFAULT_ZONE_ID;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Maps;
import com.rootcloud.analysis.context.IotWorkRuntimeDecodeContext;
import com.rootcloud.analysis.core.constants.JobConstant;
import com.rootcloud.analysis.core.constants.SchemaConstant;
import com.rootcloud.analysis.core.enums.SinkTypeEnum;
import com.rootcloud.analysis.core.enums.TimeZoneTypeEnum;
import com.rootcloud.analysis.core.enums.UpdateStrategyEnum;
import com.rootcloud.analysis.core.util.RsaUtil;
import com.rootcloud.analysis.decoder.sink.AbstractSinkDecoder;
import com.rootcloud.analysis.decoder.sink.SinkDecoder;
import com.rootcloud.analysis.entity.RcTimeAttr;
import com.rootcloud.analysis.exception.DecodeSinkException;
import com.rootcloud.analysis.wrapper.sink.AbstractSinkWrapper;
import com.rootcloud.analysis.wrapper.sink.MysqlSinkWrapper;
import java.util.List;
import java.util.Map;
import org.apache.commons.compress.utils.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SinkDecoder(sinkType = SinkTypeEnum.MYSQL)
public class MysqlSinkDecoder extends AbstractSinkDecoder {

  private static Logger logger = LoggerFactory.getLogger(MysqlSinkDecoder.class);

  @Override
  public AbstractSinkWrapper decodeSinkParams(JSONObject conf) {
    try {
      MysqlSinkWrapper.Builder builder = new MysqlSinkWrapper.Builder();
      JSONObject params = conf.getJSONObject(JobConstant.KEY_PARAMETERS);
      JSONObject outputMapper = conf.getJSONObject(JobConstant.KEY_OUTPUT_MAPPER);

      JSONArray attributeMapperArr = outputMapper.getJSONArray(JobConstant.KEY_ATTRIBUTE_MAPPER);
      List<String> schemas = Lists.newArrayList();
      List<String> accAttrs = Lists.newArrayList();
      List<String> replaceAttrs = Lists.newArrayList();
      List<String> replaceWithNonNullAttrs = Lists.newArrayList();
      List<String> greatestAttrs = Lists.newArrayList();
      List<String> leastAttrs = Lists.newArrayList();

      Map<String, String> attributeMapper = Maps.newHashMap();

      for (Object o : attributeMapperArr) {
        JSONObject targetAttribute = ((JSONObject) o)
            .getJSONObject(JobConstant.KEY_TARGET_ATTRIBUTE);
        JSONObject sourceAttribute = ((JSONObject) o)
            .getJSONObject(JobConstant.KEY_SOURCE_ATTRIBUTE);

        attributeMapper.put(targetAttribute.getString(SchemaConstant.ATTR_NAME),
            sourceAttribute.getString(SchemaConstant.ATTR_NAME));

        schemas.add(
            targetAttribute.getString(SchemaConstant.ATTR_NAME) + SchemaConstant.COLUMN_SEPARATOR
                + targetAttribute
                .getString(SchemaConstant.ATTR_DATA_TYPE));

        String updateStrategy = ((JSONObject) o).getString(JobConstant.KEY_UPDATE_STRATEGY);
        if (UpdateStrategyEnum.ACCUMULATE.name().equals(updateStrategy)) {
          accAttrs.add(targetAttribute.getString(SchemaConstant.ATTR_NAME));
        } else if (UpdateStrategyEnum.REPLACE.name().equals(updateStrategy)) {
          replaceAttrs.add(targetAttribute.getString(SchemaConstant.ATTR_NAME));
        } else if (UpdateStrategyEnum.REPLACE_WITH_NON_NULL.name().equals(updateStrategy)) {
          replaceWithNonNullAttrs.add(targetAttribute.getString(SchemaConstant.ATTR_NAME));
        } else if (UpdateStrategyEnum.GREATEST.name().equals(updateStrategy)) {
          greatestAttrs.add(targetAttribute.getString(SchemaConstant.ATTR_NAME));
        } else if (UpdateStrategyEnum.LEAST.name().equals(updateStrategy)) {
          leastAttrs.add(targetAttribute.getString(SchemaConstant.ATTR_NAME));
        }
      }

      boolean timeAggregationFlag = conf.getBoolean(JobConstant.KEY_TIME_AGGREGATION_FLAG);

      builder
          .setUri(params.getString(JobConstant.KEY_URI))
          .setUsername(params.getString(JobConstant.KEY_USERNAME))
          .setPassword(RsaUtil.decryptDataOnJava(params.getString(JobConstant.KEY_PASSWORD),
              IotWorkRuntimeDecodeContext.getPrivateKey()))
          .setAttributeMapper(attributeMapper)
          .setAccAttrs(accAttrs)
          .setReplaceAttrs(replaceAttrs)
          .setReplaceWithNonNullAttrs(replaceWithNonNullAttrs)
          .setGreatestAttrs(greatestAttrs)
          .setLeastAttrs(leastAttrs)
          .setInsertKey(conf.getString(JobConstant.KEY_INSERT_KEY))
          .setSchemas(schemas)
          .setTable(conf.getString(JobConstant.KEY_TABLE))
          .setInsertType(outputMapper.getString(JobConstant.KEY_INSERT_TYPE))
          .setSinkType(SinkTypeEnum.MYSQL)
          .setTimeAggregationFlag(timeAggregationFlag);

      if (timeAggregationFlag) {
        JSONObject timeAggregation = conf.getJSONObject(JobConstant.KEY_TIME_AGGREGATION);

        builder.setTimeDimension(timeAggregation.getString(JobConstant.KEY_TIME_DIMENSION))
            .setTimeAttr(RcTimeAttr.valueOf(timeAggregation.getString(JobConstant.KEY_TIME_ATTR)))
            .setTimeZone(timeAggregation.containsKey(JobConstant.KEY_TIMEZONE) ? TimeZoneTypeEnum
                .valueOf(timeAggregation
                    .getString(JobConstant.KEY_TIMEZONE)).getZoneId() : DEFAULT_ZONE_ID)
            .setStartTime(timeAggregation.containsKey(JobConstant.KEY_START_TIME) ? timeAggregation
                .getString(JobConstant.KEY_START_TIME) : DEFAULT_START_TIME);

      }
      MysqlSinkWrapper result = builder.build();
      logger.debug("Decoding mysql sink: {}", result);
      return result;
    } catch (Exception ex) {
      logger.error("Exception occurred when decoding mysql sink: {}", conf);
      throw new DecodeSinkException(conf.getString(JobConstant.KEY_NODE_ID),
          conf.toJSONString());
    }
  }

}
