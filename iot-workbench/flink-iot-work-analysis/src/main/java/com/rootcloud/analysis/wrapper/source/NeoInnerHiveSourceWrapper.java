/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.wrapper.source;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.google.common.base.Strings;
import com.rootcloud.analysis.common.context.IotWorkRuntimeContext;
import com.rootcloud.analysis.common.entity.RcUnit;
import com.rootcloud.analysis.common.factory.KafkaLogMiddlewareResourceFactory;
import com.rootcloud.analysis.core.constants.CommonConstant;
import com.rootcloud.analysis.core.constants.GrayLogConstant;
import com.rootcloud.analysis.core.constants.SchemaConstant;
import com.rootcloud.analysis.core.constants.ShortMessageConstant;
import com.rootcloud.analysis.core.enums.GrayLogResultEnum;
import com.rootcloud.analysis.core.enums.GraylogLogTypeEnum;;
import com.rootcloud.analysis.core.graylog.LogVo;
import com.rootcloud.analysis.exception.BuildSourceException;
import com.rootcloud.analysis.log.loggingservice.LoggingServiceUtil;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import lombok.AllArgsConstructor;
import lombok.Builder;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.functions.ProcessFunction;
import org.apache.flink.streaming.connectors.kafka.internals.FlinkKafkaInternalProducer;
import org.apache.flink.table.api.Table;
import org.apache.flink.table.api.bridge.java.StreamTableEnvironment;
import org.apache.flink.table.catalog.ObjectPath;
import org.apache.flink.table.catalog.exceptions.TableNotExistException;
import org.apache.flink.table.catalog.hive.HiveCatalog;
import org.apache.flink.types.Row;
import org.apache.flink.util.Collector;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by 王纯超 on 2021/8/11. 由于Hive里的工况数据由json变为拆分列，创建此类。
 * InnerHiveSourceWrapper指定消费type_【tenantid】_allmodel，优化后此表不存在，需要指定消费的
 * 具体物模型表tbl_ods_[modelId]。此表所有列均为String，需要进行类型处理。
 */
@Builder
public class NeoInnerHiveSourceWrapper extends HiveSourceWrapper {

  private static Logger logger = LoggerFactory.getLogger(NeoInnerHiveSourceWrapper.class);

  public static final String TB_NAME_FIELD = "tablename";

  private static final String COLUMN_PARTITION_ID = "partitionid";

  private final long partitionIdLowerBound;

  private final long partitionIdUpperBound;

  /**
   * key is hive table name. value is a list of fields that are declared as output but do not exist
   * in the table.
   */
  private Map<String, List<String>> tableMissingFieldMap;

  @Override
  public void registerInput(IotWorkRuntimeContext context) {
    tableMissingFieldMap = new HashMap<>();
    StreamTableEnvironment tableEnv = context.getTableEnvFromCache();
    Configuration configuration = tableEnv.getConfig().getConfiguration();
    configuration.setInteger("table.exec.hive.infer-source-parallelism.max", getParallelism());
    HiveCatalog hiveCatalog = new HiveCatalog(getCatalogName(), getDefaultDatabase(),
        getHiveConfDir());
    tableEnv.registerCatalog(getCatalogName(), hiveCatalog);

    List<String> existentTables = checkTables(hiveCatalog);

    String querySql = buildQuerySql(hiveCatalog, existentTables);
    logger.info("NeoInnerHive:querySql={}", querySql);

    Table table = tableEnv.sqlQuery(querySql);

    DataStream<RcUnit> dataStream = tableEnv.toRetractStream(table, Row.class)
        .process(
            new NeoInnerHiveSourceWrapper.Row2RcUnitFlatMap(getJobId(), getTenantId(), getJobName(),
                getLogType(), getSchema(), tableMissingFieldMap,
                getBizStartTime(), getBizEndTime(), getKafkaLoggingServiceServers(),
                getKafkaLoggingServiceTopics(), null))
        .setParallelism(getParallelism())
        .uid(getNodeId())
        .name(getNodeName());
    context.putObjectToContainer(getNodeId(), dataStream);
  }

  /**
   * Check whether the tables exist and return those existing ones.
   *
   * @param catalog not null
   */
  private List<String> checkTables(HiveCatalog catalog) {
    String[] odsTables = table.split(",", -1);
    List<String> tables = new LinkedList<>();
    List<String> nonexistent = new LinkedList<>();
    for (String odsTable : odsTables) {
      boolean existent = catalog.tableExists(new ObjectPath(getDefaultDatabase(), odsTable));
      if (existent) {
        tables.add(odsTable);
      } else {
        nonexistent.add(odsTable);
      }
    }

    if (nonexistent.isEmpty() && tables.isEmpty()) {
      LoggingServiceUtil.error(LogVo.builder()
          .k8sServiceName(GrayLogConstant.K8S_SERVICE_NAME)
          .module(GrayLogConstant.MODULE_NAME)
          .userName(getTenantId())
          .userId(getUserId())
          .tenantId(getTenantId())
          .operateObjectName(getJobName())
          .operateObject(getJobId())
          .shortMessage(String.format("No table in Hive is specified to read. "
              + "Please choose at least one model."))
          .operation(ShortMessageConstant.READ_HIVE_DATA)
          .requestId(String.valueOf(System.currentTimeMillis()))
          .result(GrayLogResultEnum.FAIL.getValue())
          .logType(getLogType())
          .kafkaLoggingServiceTopic(kafkaLoggingServiceTopics)
          .kafkaLoggingServiceServers(kafkaLoggingServiceServers)
          .build());
      throw new RuntimeException(String.format("No table in Hive is specified to read."
          + " Please choose at least one model."));
    }

    if (nonexistent.size() > 0) {
      LoggingServiceUtil.warn(LogVo.builder()
          .k8sServiceName(GrayLogConstant.K8S_SERVICE_NAME)
          .module(GrayLogConstant.MODULE_NAME)
          .userName(getTenantId())
          .userId(getUserId())
          .tenantId(getTenantId())
          .operateObjectName(getJobName())
          .operateObject(getJobId())
          .shortMessage(String.format("The table %s does not exist in Hive."
              + " Please ensure the chosen model is uploading data normally.",getTable()))
          .operation(ShortMessageConstant.READ_HIVE_DATA)
          .requestId(String.valueOf(System.currentTimeMillis()))
          .result(GrayLogResultEnum.FAIL.getValue())
          .logType(getLogType())
          .kafkaLoggingServiceTopic(kafkaLoggingServiceTopics)
          .kafkaLoggingServiceServers(kafkaLoggingServiceServers)
          .build());
    }

    if (tables.isEmpty()) {
      LoggingServiceUtil.error(LogVo.builder()
          .k8sServiceName(GrayLogConstant.K8S_SERVICE_NAME)
          .module(GrayLogConstant.MODULE_NAME)
          .userName(getTenantId())
          .tenantId(getTenantId())
          .operateObjectName(getJobName())
          .operateObject(getJobId())
          .shortMessage(String.format("No table in Hive is available to the Hive input node."
              + " Please ensure the chosen model is uploading data normally."))
          .operation(ShortMessageConstant.READ_HIVE_DATA)
          .requestId(String.valueOf(System.currentTimeMillis()))
          .result(GrayLogResultEnum.FAIL.getValue())
          .logType(getLogType())
          .kafkaLoggingServiceTopic(kafkaLoggingServiceTopics)
          .kafkaLoggingServiceServers(kafkaLoggingServiceServers)
          .build());
      throw new RuntimeException(String.format("No table in Hive is available to the Hive"
          + " input node."
          + " Please ensure the chosen model is uploading data normally."));
    }

    return tables;
  }

  /**
   * Builds query sql.
   *
   * @param hiveCatalog    not null
   * @param existentTables not null
   */
  public String buildQuerySql(HiveCatalog hiveCatalog,
                              List<String> existentTables) {
    StringBuilder sb = new StringBuilder();

    for (int i = 0; i < existentTables.size(); i++) {
      if (i != 0) {
        sb.append(" union all ");
      }
      String tableName = existentTables.get(i);
      List<String> columns = getTableColumns(hiveCatalog, tableName);
      List<String> missingFields = new LinkedList<>();
      tableMissingFieldMap.put(tableName, missingFields);
      sb.append(buildTableQuerySql(columns, tableName, missingFields));
    }
    return sb.toString();
  }

  /**
   * Get the hive table columns.
   *
   * @param hiveCatalog not null
   * @param tableName   not null
   */
  @NotNull
  public List<String> getTableColumns(HiveCatalog hiveCatalog, String tableName) {
    List<String> columns;
    try {
      columns =
          Arrays.asList(hiveCatalog.getTable(new ObjectPath(getDefaultDatabase(),
              tableName)).getSchema().getFieldNames());
    } catch (TableNotExistException e) {
      logger.error(e.getMessage());
      throw new BuildSourceException(getNodeId());
    }
    return columns;
  }

  /**
   * Get the field expression to use in select query. If the field exists in the table, return it
   * quoted with backtick, and return a default value according to its data type.
   *
   * @param columns the existent columns
   * @param type    the conceptual data type of the field
   * @param field   the field to select
   */
  private String getSelectFieldName(List<String> columns, String type, String field) {
    if (columns.contains(field)) {
      return "`" + field + "`";
    } else {
      String literal = "''"; //Use an empty string irrespective of the declared type
      //as the field will be replaced with null after the sql execution

      return literal + " as " + field;
    }
  }

  /**
   * Build a query for a single table.
   *
   * @param columns       not null
   * @param table         not null
   * @param missingFields not null
   */
  public String buildTableQuerySql(List<String> columns,
                                   String table, List<String> missingFields) {
    StringBuilder sb =
        new StringBuilder("select '").append(table).append("' as ").append(TB_NAME_FIELD)
            .append(",");
    JSONArray schemaJson = this.getSchema();
    for (Object obj : schemaJson) {
      JSONObject fieldJson = (JSONObject) obj;
      String field = getActualFieldName(fieldJson);
      String type = fieldJson.getString(SchemaConstant.ATTR_DATA_TYPE);

      if (!columns.contains(field.toLowerCase(Locale.ENGLISH))) {
        missingFields.add(field);
        logger.warn("Field {} does not exist in hive table {}！", table, field);
      }
      sb.append(getSelectFieldName(columns, type, field.toLowerCase(Locale.ENGLISH))).append(",");
    }

    sb.deleteCharAt(sb.length() - 1);

    sb.append(" from ");
    sb.append(getCatalogName());
    sb.append(CommonConstant.DOT);
    sb.append(getDefaultDatabase());
    sb.append(CommonConstant.DOT);
    sb.append(table);
    sb.append(" where ");
    sb.append(COLUMN_PARTITION_ID);
    sb.append(" >= ");
    sb.append(partitionIdLowerBound);
    sb.append(" and ");
    sb.append(COLUMN_PARTITION_ID);
    sb.append(" <= ");
    sb.append(partitionIdUpperBound);
    return sb.toString();
  }

  @AllArgsConstructor
  public static class Row2RcUnitFlatMap extends ProcessFunction<Tuple2<Boolean, Row>, RcUnit> {

    private final String jobId;
    private final String tenantId;
    private final String jobName;
    private final GraylogLogTypeEnum logType;
    private final JSONArray schema;
    private final Map<String, List<String>> tableMissingFieldMap;
    private final long bizStartTime;
    private final long bizEndTime;
    private final String kafkaLoggingServiceServers;
    private final String kafkaLoggingServiceTopics;
    private FlinkKafkaInternalProducer flinkKafkaInternalProducer;

    @Override
    public void open(Configuration parameters) throws Exception {
      super.open(parameters);
      if (flinkKafkaInternalProducer == null && kafkaLoggingServiceServers != null) {
        flinkKafkaInternalProducer = KafkaLogMiddlewareResourceFactory.getProducer(
            kafkaLoggingServiceServers,
            this.toString());
      }

    }

    @Override
    public void close() throws Exception {
      super.close();
      KafkaLogMiddlewareResourceFactory.close(this.toString());
      flinkKafkaInternalProducer = null;
    }

    @Override
    public void processElement(Tuple2<Boolean, Row> value,
                               ProcessFunction<Tuple2<Boolean, Row>, RcUnit>.Context ctx,
                               Collector<RcUnit> out) throws Exception {
      try {
        Map<String, Object> map = new HashMap<>();

        Row row = value.f1;
        int arity = row.getArity();

        String tableName = (String) row.getField(0);
        List<String> missingFields = tableMissingFieldMap.get(tableName);
        map.put(TB_NAME_FIELD, tableName);//to distinguish rows by hive table, which is used by
        // the immediate following filter logic

        for (int i = 1; i < arity; i++) {
          JSONObject fieldJson = schema.getJSONObject(i - 1);
          String field = getActualFieldName(fieldJson);

          //map.put(field, DataUtil.convert2DataTypeInfo(row.getField(i),
          //fieldJson.getString(SchemaConstant.ATTR_DATA_TYPE)));
          if (missingFields != null && missingFields.contains(field)) {
            map.put(field, null);
          } else {
            map.put(field, row.getField(i));
          }
        }

        RcUnit result = RcUnit.valueOf(map);
        logger.debug("Inner hive output: {}", result);
        if (isInBizTimeScope(result, bizStartTime, bizEndTime)
            && !Thread.currentThread().isInterrupted()) {
          out.collect(result);
        }
      } catch (Exception ex) {
        logger.error("Exception occurred when processing inner hive data: {}", ex.toString());
        LoggingServiceUtil.alarm(LogVo.builder()
            .k8sServiceName(GrayLogConstant.K8S_SERVICE_NAME)
            .module(GrayLogConstant.MODULE_NAME)
            .userName(tenantId)
            .tenantId(tenantId)
            .operateObjectName(jobName)
            .operateObject(jobId)
            .shortMessage(String.format("Exception occurred when processing hive data: %s",
                ex.toString()))
            .operation(ShortMessageConstant.READ_HIVE_DATA)
            .requestId(String.valueOf(System.currentTimeMillis()))
            .result(GrayLogResultEnum.FAIL.getValue())
            .logType(logType)
            .kafkaLoggingServiceTopic(kafkaLoggingServiceTopics)
            .kafkaLoggingServiceServers(kafkaLoggingServiceServers)
            .build(), flinkKafkaInternalProducer);
      }
    }
  }

  @Nullable
  private static String getActualFieldName(JSONObject fieldJson) {
    String field = Strings.isNullOrEmpty(fieldJson.getString(SchemaConstant.ATTR_ORIGINAL_NAME))
        ? fieldJson.getString(SchemaConstant.ATTR_NAME)
        : fieldJson.getString(SchemaConstant.ATTR_ORIGINAL_NAME);
    //handle reading property of json field
    if (field != null && field.contains(SchemaConstant.DOT)) {
      String[] split = field.split("\\.");
      field = split[0];
    }
    return field;
  }
}
