/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.wrapper.function.filter;

import com.rootcloud.analysis.common.entity.RcUnit;
import java.util.List;
import lombok.Builder;
import lombok.ToString;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Builder
@ToString(callSuper = true)
public class InListFilterWrapper extends AbstractFilterWrapper {

  private static Logger logger = LoggerFactory.getLogger(InListFilterWrapper.class);

  private String attribute;

  public List<Object> values;

  @Override
  public boolean filter(RcUnit record) {
    boolean result = false;
    try {
      if (record.getData().containsKey(attribute)) {
        Object attrValue = record.getAttr(attribute);
        if (attrValue != null) {
          if (attrValue instanceof String || attrValue instanceof Boolean) {
            for (Object value : values) {
              if (value.equals(attrValue.toString())) {
                result = true;
              }
            }
          } else {
            for (Object value : values) {
              if (Double
                  .compare(Double.parseDouble(String.valueOf(attrValue)),
                      Double.parseDouble((String) value))
                  == 0) {
                result = true;
              }
            }
          }
        }
      }
    } catch (Exception ex) {
      logger.error("Exception occurred when filtering data: {}， exception message: {}",
          record.toString(), ex.getMessage(), ex);
      result = false;
    }
    logger.debug("'INLIST' filter result: {}, attribute: {}, record: {}, value: {}",
        result, attribute, record.toString(), values.toString());
    return result;
  }

}
