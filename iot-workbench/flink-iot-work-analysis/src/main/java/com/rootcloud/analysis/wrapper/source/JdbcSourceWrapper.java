/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.wrapper.source;

import com.alibaba.fastjson.JSONArray;
import com.rootcloud.analysis.common.context.IotWorkRuntimeContext;
import com.rootcloud.analysis.core.enums.SqlModeEnum;
import com.rootcloud.analysis.wrapper.WrapperBuilder;

public class JdbcSourceWrapper extends AbstractSourceWrapper {

  public final String uri;
  public final String username;
  public final String password;
  public final String table;
  public final Integer detectPeriod;
  public final Integer limit;
  public final String filterSql;
  public final SqlModeEnum sqlMode;
  public final String tableSql;

  /**
   * build.
   */
  public JdbcSourceWrapper(Builder builder) {
    super(builder.jobId, builder.jobName, builder.tenantId, builder.nodeId, builder.parallelism,
        builder.nodeName, builder.inputType, builder.schema,
        builder.logType);
    this.limit = builder.limit;
    this.uri = builder.uri;
    this.username = builder.username;
    this.password = builder.password;
    this.table = builder.table;
    this.detectPeriod = builder.detectPeriod;
    this.filterSql = builder.filterSql;
    this.sqlMode = builder.sqlMode;
    this.tableSql = builder.tableSql;
  }

  public static class Builder extends WrapperBuilder {

    public String nodeId;
    public String nodeName;
    public Integer parallelism;
    public String inputType;
    public JSONArray schema;
    public String uri;
    public String username;
    public String password;
    public String table;
    public Integer detectPeriod;
    public Integer limit;
    public String filterSql;
    private SqlModeEnum sqlMode;
    private String tableSql;

    public JdbcSourceWrapper.Builder setSqlMode(SqlModeEnum sqlMode) {
      this.sqlMode = sqlMode;
      return this;
    }

    public JdbcSourceWrapper.Builder setTableSql(String tableSql) {
      this.tableSql = tableSql;
      return this;
    }

    public JdbcSourceWrapper.Builder setLimit(Integer limit) {
      this.limit = limit;
      return this;
    }

    public JdbcSourceWrapper.Builder setNodeId(String nodeId) {
      this.nodeId = nodeId;
      return this;
    }

    public JdbcSourceWrapper.Builder setNodeName(String nodeName) {
      this.nodeName = nodeName;
      return this;
    }

    public JdbcSourceWrapper.Builder setParallelism(Integer parallelism) {
      this.parallelism = parallelism;
      return this;
    }

    public JdbcSourceWrapper.Builder setInputType(String inputType) {
      this.inputType = inputType;
      return this;
    }

    public JdbcSourceWrapper.Builder setSchema(JSONArray schema) {
      this.schema = schema;
      return this;
    }

    public JdbcSourceWrapper.Builder setUri(String uri) {
      this.uri = uri;
      return this;
    }

    public JdbcSourceWrapper.Builder setUsername(String username) {
      this.username = username;
      return this;
    }

    public JdbcSourceWrapper.Builder setPassword(String password) {
      this.password = password;
      return this;
    }

    public JdbcSourceWrapper.Builder setTable(String table) {
      this.table = table;
      return this;
    }

    public JdbcSourceWrapper.Builder setFilterSql(String filterSql) {
      this.filterSql = filterSql;
      return this;
    }

    public JdbcSourceWrapper.Builder setDetectPeriod(Integer detectPeriod) {
      this.detectPeriod = detectPeriod;
      return this;
    }

    public JdbcSourceWrapper build() {
      return new JdbcSourceWrapper(this);
    }

  }

  @Override
  public void registerInput(IotWorkRuntimeContext context) {
  }
}