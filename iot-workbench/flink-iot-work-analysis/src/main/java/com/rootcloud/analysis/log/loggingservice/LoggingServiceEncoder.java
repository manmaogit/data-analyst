/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2020 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.log.loggingservice;

import com.alibaba.fastjson.JSONObject;
import com.rootcloud.analysis.core.constants.JobConstant;
import com.rootcloud.analysis.core.enums.GrayLogResultEnum;
import com.rootcloud.analysis.core.graylog.LogVo;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LoggingServiceEncoder {

  private static Logger logger = LoggerFactory.getLogger(LoggingServiceEncoder.class);

  /**
   * 转换为IamLogVo.
   */
  public static LoggingServiceLogVo encode(LogVo logVo) {
    LoggingServiceLogVo loggingServiceLogVo = new LoggingServiceLogVo();
    loggingServiceLogVo.setId(UUID.randomUUID().toString());
    loggingServiceLogVo.setLogType(logVo.getLogType().name());
    loggingServiceLogVo.setTimestamp(System.currentTimeMillis() * 1000 * 1000);
    loggingServiceLogVo.setLogService(JobConstant.KEY_IOTOWRKS);
    Map<String,String> labelObject = new HashMap<String,String>();
    labelObject.put("logType", loggingServiceLogVo.getLogType());
    labelObject.put("logService", loggingServiceLogVo.getLogService());
    loggingServiceLogVo.setLabels(labelObject);
    LoggingServiceLogPayloadVo loggingServiceLogPayloadVo = new LoggingServiceLogPayloadVo();
    loggingServiceLogPayloadVo.setOperateObject(logVo.getOperateObject());
    loggingServiceLogPayloadVo.setOperateObjectName(logVo.getOperateObjectName());
    loggingServiceLogPayloadVo.setLogLevel(logVo.getLogLevel());
    loggingServiceLogPayloadVo.setUserId(logVo.getUserId());
    try {
      loggingServiceLogPayloadVo.setOrganizationId(logVo.getOrganizationId());
    } catch (Exception e) {
      logger.error("error msg:", e);
    }
    loggingServiceLogPayloadVo.setTenantId(logVo.getTenantId());
    loggingServiceLogPayloadVo.setClientId(logVo.getClient());
    loggingServiceLogPayloadVo.setClientType(null);
    loggingServiceLogPayloadVo.setRequestId(logVo.getRequestId());
    loggingServiceLogPayloadVo.setStatus(
        logVo.getResult().equals(GrayLogResultEnum.SUCCESS.getValue()) ? 1 : 0);
    loggingServiceLogPayloadVo.setServiceProvider(logVo.getModule());
    loggingServiceLogPayloadVo.setDescription(logVo.getShortMessage());
    loggingServiceLogPayloadVo.setAction(logVo.getAction());
    loggingServiceLogPayloadVo.setResourceId(logVo.getOperateObject());
    loggingServiceLogPayloadVo.setResourceType(logVo.getOperateObjectType());
    loggingServiceLogPayloadVo.setIp(logVo.getIp());
    loggingServiceLogVo.setPayload(loggingServiceLogPayloadVo);
    return loggingServiceLogVo;
  }


}
