/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.actuator.impl;

import com.rootcloud.analysis.actuator.AbstractActuator;
import lombok.Setter;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.table.api.StatementSet;

@Setter
public class DefaultActuator extends AbstractActuator {

  private StatementSet statementSet;

  private StreamExecutionEnvironment env;

  @Override
  public void execute() throws Exception {
    env.execute(getJobName());
  }

}
