/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.wrapper.processor;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.rootcloud.analysis.common.context.IotWorkRuntimeContext;
import com.rootcloud.analysis.common.entity.RcUnit;
import com.rootcloud.analysis.context.IotWorkRuntimeBuildContext;
import com.rootcloud.analysis.context.IotWorkRuntimeDecodeContext;
import com.rootcloud.analysis.converter.extractor.FieldBasedTimestampAssigner;
import com.rootcloud.analysis.converter.stream.table.RcUnitToRowConverter;
import com.rootcloud.analysis.core.constants.SchemaConstant;
import com.rootcloud.analysis.core.enums.GraylogLogTypeEnum;
import com.rootcloud.analysis.core.enums.KeepPolicyEnum;
import com.rootcloud.analysis.entity.RcTimeAttr;
import com.rootcloud.analysis.exception.DecodeProcessorException;
import com.rootcloud.analysis.function.map.Row2RcUnitFlatMap;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.apache.commons.lang3.StringUtils;
import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.api.common.functions.ReduceFunction;
import org.apache.flink.api.dag.Transformation;
import org.apache.flink.api.java.functions.KeySelector;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.WindowedStream;
import org.apache.flink.streaming.api.windowing.assigners.EventTimeSessionWindows;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.flink.table.api.Expressions;
import org.apache.flink.table.api.Table;
import org.apache.flink.table.api.bridge.java.StreamTableEnvironment;
import org.apache.flink.types.Row;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by 王纯超 on 2021/9/1.
 */
@Getter
@Setter
@ToString(callSuper = true)
@Builder
public class OfflineDedupProcessorWrapper extends AbstractProcessorWrapper {
  private static final long OUT_OF_ORDER_SEC = 3L;
  private static final long LATENCY_SEC = 90L;
  private static final long DUPLICATION_GAP = Integer.MAX_VALUE;
  private static Logger logger = LoggerFactory.getLogger(OfflineDedupProcessorWrapper.class);
  private final JSONArray schema;
  private final String viewName;
  private final List<String> keyFieldList;
  private String sortBy;
  private KeepPolicyEnum keep;
  private String priorTsViewName;

  @Override
  public void registerProcess(IotWorkRuntimeContext context) throws Exception {
    final StreamTableEnvironment tableEnv = context.getTableEnvFromCache();

    final String priorKeyedWindowDsKey = getPriorNodeId() + StringUtils.join(keyFieldList, "");
    final String dedupNodeKey = priorKeyedWindowDsKey + sortBy + keep.name();

    Table dedupTable = IotWorkRuntimeBuildContext.getTableObject(dedupNodeKey);
    DataStream dedupDataStream = context.getObjectFromContainer(dedupNodeKey);
    if (dedupDataStream
        == null) { //process only once for the same deduplication nodes defined in the
      // job configuration.
      dedupDataStream = appendDedupTransformation(context, tableEnv);
      context.putObjectToContainer(dedupNodeKey, dedupDataStream);

      dedupTable = convert2Table(tableEnv, dedupDataStream);
      IotWorkRuntimeBuildContext.putTableObject(dedupNodeKey, dedupTable);
    }

    housekeeping(context, tableEnv, dedupDataStream, dedupTable);
  }

  private void housekeeping(IotWorkRuntimeContext context,
                            StreamTableEnvironment tableEnv,
                            DataStream dedupStream, Table dedupTable) {
    try {
      //For use in afterward sql
      tableEnv.createTemporaryView(viewName, dedupTable);
      context.putObjectToContainer(getNodeId(), dedupStream);//For building the flink job graph
      IotWorkRuntimeBuildContext.putTemporaryViewName(getNodeId(), viewName);//For the record in case of query
      // temporary view name by node id
      IotWorkRuntimeBuildContext.putTableObject(getNodeId(), dedupTable);//For building the flink job graph by
      // invoking Table API
    } catch (Exception e) {
      throw new DecodeProcessorException();
    }
  }

  /**
   * Get the object associated with key from context and put the object created by func to
   * context if there is no object associated with key.
   *
   * @param context RcContext
   * @param key     key
   * @param func    function to create an object
   * @return
   */
  private <T> T getAndPutIfAbsent(IotWorkRuntimeContext context, String key, Function func) {
    T ds = context.getObjectFromContainer(key);
    if (ds == null) {
      ds = (T) func.apply(key);
      context.putObjectToContainer(key, ds);
    }
    return ds;
  }

  private DataStream appendDedupTransformation(IotWorkRuntimeContext context, StreamTableEnvironment tableEnv) {
    final DataStream<RcUnit> priorDataStream =
        getAndPutIfAbsent(context, getPriorNodeId(), priorNodeId -> {
          Table priorTable = IotWorkRuntimeBuildContext.getTableObject(getPriorNodeId());
          return tableEnv.toRetractStream(priorTable, Row.class)
              .flatMap(new Row2RcUnitFlatMap(
                  getJobId(),
                  getTenantId(),
                  getUserId(),
                  getJobName(),
                  IotWorkRuntimeDecodeContext.getLoggingServiceKafkaTopic(),
                  IotWorkRuntimeDecodeContext.getLoggingServiceKafkaServers(),
                  getNodeId(),
                  Arrays.asList(priorTable.getSchema().getFieldNames()),
                  getSchema(),
                  GraylogLogTypeEnum.OFFLINE,
                  false,
                  true
              ))
              .uid(getNodeId())
              .name(getNodeName());
        });


    final WatermarkStrategy originalWatermarkStrategy =
        getWatermarkStrategy(priorDataStream.getTransformation());

    final String dataStreamWithTsKey = getPriorNodeId() + sortBy;
    DataStream<RcUnit> dsWithTimestamp =
        getAndPutIfAbsent(context, dataStreamWithTsKey,
            t -> assignTimestampAndWatermark(priorDataStream));

    final String dedupDsKey = dataStreamWithTsKey + StringUtils.join(keyFieldList);
    WindowedStream<RcUnit, String, TimeWindow> windowedStream = getAndPutIfAbsent(context,
        dedupDsKey, k ->
            dsWithTimestamp
                .keyBy(new KeySelector<RcUnit, String>() {
                  @Override
                  public String getKey(RcUnit value) throws Exception {
                    StringBuilder sb = new StringBuilder();
                    boolean existNullKey = false;
                    for (String f : keyFieldList) {
                      Object keyFieldValue = value.getAttr(f);
                      if (keyFieldValue != null) {
                        sb.append(keyFieldValue.toString());
                      } else {
                        existNullKey = true;
                      }
                    }

                    if (existNullKey) {
                      sb.delete(0, sb.length());
                      //if some key field value is null,
                      //use all the values to differentiate two records,
                      //as PO demands that two nulls are not equal in the duplication decision.
                      value.getData().values().stream().forEach(x -> sb.append(x));
                    }

                    return sb.toString();
                  }
                })
                .window(EventTimeSessionWindows.withGap(Time.seconds(DUPLICATION_GAP)))
                .allowedLateness(Time.seconds(LATENCY_SEC))
    );

    if (keep == KeepPolicyEnum.LAST) {
      return windowedStream.reduce(new ReduceFunction<RcUnit>() {
        @Override
        public RcUnit reduce(RcUnit value1, RcUnit value2) throws Exception {
          Object ts1 = value1.getAttr(sortBy);
          Object ts2 = value2.getAttr(sortBy);

          if (ts1 == null) {
            return value2;
          }

          if (ts2 == null) {
            return value1;
          }

          if (ts1 instanceof Comparable) {
            return ((Comparable) ts1).compareTo(ts2) > 0 ? value1 : value2;
          } else {
            return value2;
          }
        }
      }).setParallelism(this.getParallelism())
          .name(getNodeName())
          .uid(getNodeId());
    } else {
      return windowedStream.reduce(new ReduceFunction<RcUnit>() {
        @Override
        public RcUnit reduce(RcUnit value1, RcUnit value2) throws Exception {
          Object ts1 = value1.getAttr(sortBy);
          Object ts2 = value2.getAttr(sortBy);

          if (ts2 == null) {
            return value1;
          }
          if (ts1 == null) {
            return value2;
          }

          if (ts1 instanceof Comparable) {
            return ((Comparable) ts1).compareTo(ts2) <= 0 ? value1 : value2;
          } else {
            return value1;
          }
        }
      }).setParallelism(this.getParallelism())
          .name(getNodeName())
          .uid(getNodeId());
    }

  }

  private WatermarkStrategy getWatermarkStrategy(Transformation transformation) {
    return null;
  }

  /**
   * Flink only supports time attribute in deduplication sql. Assign timestamps and watermarks
   * based on the {@link OfflineDedupProcessorWrapper#sortBy}, which is used to sort duplicate
   * records afterwards.
   *
   * @param priorDataStream not null
   */
  private DataStream<RcUnit> assignTimestampAndWatermark(DataStream<RcUnit> priorDataStream) {
    RcTimeAttr timeAttr = new RcTimeAttr();
    timeAttr.setKey(sortBy);
    timeAttr.setType("Timestamp");
    timeAttr.setFormat("ms");

    return priorDataStream.assignTimestampsAndWatermarks(
        WatermarkStrategy.<RcUnit>forBoundedOutOfOrderness(Duration.ofSeconds(OUT_OF_ORDER_SEC))
            .withTimestampAssigner(
                ctx -> new FieldBasedTimestampAssigner(timeAttr)));
  }

  /**
   * Check whether the temporary view specified by {@code path} already exists.
   *
   * @param tableEnv not null
   * @param path     not null
   * @return
   */
  public boolean tempViewExists(StreamTableEnvironment tableEnv, String path) {
    String[] views = tableEnv.listTemporaryViews();
    for (String view : views) {
      if (view.equals(path)) {
        return true;
      }
    }
    return false;
  }

  private Table convert2Table(StreamTableEnvironment tableEnv, DataStream<RcUnit> priorDataStream)
      throws Exception {
    List<String> fieldNames = new ArrayList<>(getSchema().size());
    List<String> fieldTypes = new ArrayList<>(getSchema().size());
    List<Integer> valueTypeScales = new ArrayList<>(getSchema().size());

    int viewSchemaLen = getSchema().size() + 1;
    org.apache.flink.table.expressions.Expression[] viewSchema =
        new org.apache.flink.table.expressions.Expression[viewSchemaLen];

    for (int i = 0; i < getSchema().size(); i++) {
      JSONObject jsonObject = (JSONObject) getSchema().get(i);
      String fieldName = jsonObject.getString(SchemaConstant.ATTR_NAME);
      fieldNames.add(fieldName);
      fieldTypes.add(jsonObject.getString(SchemaConstant.ATTR_DATA_TYPE));
      valueTypeScales.add(jsonObject.getInteger(SchemaConstant.ATTR_SCALE));

      viewSchema[i] = Expressions.$(fieldName);
    }

    viewSchema[viewSchemaLen - 1] = Expressions.$("rowtime").rowtime();

    RcUnitToRowConverter converter = new RcUnitToRowConverter(priorTsViewName, fieldNames,
        fieldTypes, valueTypeScales);
    DataStream<Row> rowDataStream = converter.convert(priorDataStream);
    Table t = tableEnv.fromDataStream(rowDataStream, viewSchema);
    return t;
  }

}
