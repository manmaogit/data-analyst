/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.common.util;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import javassist.CannotCompileException;
import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtField;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ClassUtil {

  private static final Logger logger = LoggerFactory.getLogger(ClassUtil.class);

  /**
   * makeClass.
   */
  public static Class<?> makeClass(String className, String schema)
      throws CannotCompileException {
    ClassPool pool = ClassPool.getDefault();
    CtClass ctClass = pool.makeClass(className);

    addFields(ctClass, schema);
    return ctClass.toClass(ClassUtil.class.getClassLoader(), null);
  }

  /**
   * makeClass.
   */
  public static CtClass makeClass(String className) {
    ClassPool pool = ClassPool.getDefault();
    CtClass ctClass = pool.makeClass(className);

    return ctClass;
  }

  /**
   * makeClass.
   */
  public static CtClass makeClass(String className, List<String> keys, List<String> valueTyes)
      throws CannotCompileException {
    ClassPool pool = ClassPool.getDefault();
    return makeClass(pool, className, keys, valueTyes);
  }

  /**
   * makeClass.
   */
  public static CtClass makeClass(ClassPool pool, String className) {
    CtClass ctClass = pool.makeClass(className);
    return ctClass;
  }

  /**
   * makeClass.
   */
  public static CtClass makeClass(ClassPool pool, String className,
                                  List<String> keys, List<String> valueTyes)
      throws CannotCompileException {
    CtClass ctClass = pool.makeClass(className);

    if (keys.size() != valueTyes.size()) {
      throw new CannotCompileException("size not equal");
    }

    try {
      for (int index = 0; index < keys.size(); index++) {
        CtField field = CtField.make("public " + valueTyes.get(index)
            + " " + keys.get(index) + ";", ctClass);
        ctClass.addField(field);
      }
    } catch (Exception e) {
      logger.error("error msg：",e);
    }

    return ctClass;
  }


  /**
   * addFields.
   */
  public static CtClass addFields(CtClass ctClass, String schema) {
    try {
      String[] temp = schema.split("\\|");
      for (String item : temp) {
        String[] unit = item.split("\\:");

        CtField field = CtField.make("public " + unit[1] + " " + unit[0] + ";", ctClass);
        ctClass.addField(field);
      }
    } catch (Exception e) {
      logger.error("error msg：",e);
    }

    return ctClass;
  }

  /**
   * addFields.
   */
  public static CtClass addFields(String className, String schema) {
    ClassPool pool = ClassPool.getDefault();

    CtClass ctClass = null;
    try {
      ctClass = pool.get(className);
      addFields(ctClass, schema);
    } catch (Exception e) {
      logger.error("error msg：",e);
    }

    return ctClass;
  }

  /**
   * getObjectValues.
   */
  public static List<Object> getObjectValues(Object value) {
    List<Object> values = new ArrayList<>();

    Class<?> clz = value.getClass();
    Field[] fields = clz.getDeclaredFields();
    try {
      for (Field field : fields) {
        values.add(field.get(value));
      }
    } catch (IllegalAccessException e) {
      logger.error("error msg：",e);
    }

    return values;
  }

  /**
   * getObjectFields.
   */
  public static List<String> getObjectFields(Object value) {
    List<String> fieldList = new ArrayList<>();

    Class<?> clz = value.getClass();
    Field[] fields = clz.getDeclaredFields();
    for (Field field : fields) {
      fieldList.add(field.getName());
    }

    return fieldList;
  }

  /**
   * getFieldValue.
   */
  public static Object getFieldValue(Object value, String field) {

    Class<?> clz = value.getClass();
    Field[] fields = clz.getDeclaredFields();

    try {
      for (Field item : fields) {
        if (item.getName().equals(field)) {
          return item.get(value);
        }
      }
    } catch (IllegalAccessException e) {
      logger.error("error msg：",e);
    }

    return null;
  }

  /**
   * get the value of the specified field of the object recursively. Throw a NoSuchFieldException
   * when the field does not exist
   *
   * @param obj       the object to get field
   * @param fieldName the field to get value
   * @return the value of the specified field of the object
   * @throws Exception NoSuchFieldException
   */
  public static Object getObjectFieldValue(Object obj, String fieldName) throws Exception {
    Exception ex = null;

    Class superClass = obj.getClass();

    while (superClass != null && superClass != Object.class) {
      try {
        Field field = superClass.getDeclaredField(fieldName);
        field.setAccessible(true);

        return field.get(obj);
      } catch (NoSuchFieldException e) {
        ex = e;
      }

      superClass = superClass.getSuperclass();
    }

    if (ex == null) {
      ex = new NoSuchFieldException();
    }

    throw ex;

  }

  /**
   * Set the value of fieldName to fieldValue. Throw a NoSuchFieldException
   * when the field does not exist.
   *
   * @param obj        the object to get field
   * @param fieldName  the field to set value
   * @param fieldValue the value to set
   * @throws Exception thrown exception
   */
  public static void setObjectFieldValue(Object obj, String fieldName, Object fieldValue)
      throws Exception {
    Exception ex = null;

    Class superClass = obj.getClass();

    while (superClass != null && superClass != Object.class) {
      try {
        Field field = superClass.getDeclaredField(fieldName);
        field.setAccessible(true);

        field.set(obj, fieldValue);
        return;
      } catch (NoSuchFieldException e) {
        ex = e;
      }

      superClass = superClass.getSuperclass();
    }

    if (ex == null) {
      ex = new NoSuchFieldException();
    }

    throw ex;
  }
}
