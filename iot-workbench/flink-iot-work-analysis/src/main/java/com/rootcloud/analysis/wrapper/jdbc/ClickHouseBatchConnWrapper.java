/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.wrapper.jdbc;

import com.alibaba.fastjson.JSONArray;
import com.rootcloud.analysis.core.enums.SqlModeEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ClickHouseBatchConnWrapper extends JdbcSqlModeConnWrapper {

  private static final Logger logger = LoggerFactory.getLogger(ClickHouseBatchConnWrapper.class);

  /** 构造方法. */
  public ClickHouseBatchConnWrapper(
      String driverClassName,
      String url,
      String username,
      String password,
      String validationQuery,
      String table,
      JSONArray schema,
      Integer limit,
      String filterSql,
      String tableSql,
      SqlModeEnum sqlMode) {
    super(
        driverClassName,
        url,
        username,
        password,
        validationQuery,
        table,
        schema,
        limit,
        filterSql,
        tableSql,
        sqlMode);
  }

  @Override
  protected String getKeyQuote() {
    return "`";
  }

  @Override
  protected String getValueQuote() {
    return "'";
  }

  @Override
  protected String getLimitSqlTemplate() {
    return String.format("select * from %s limit %s", TABLE_PLACEHOLDER, LIMIT_PLACEHOLDER);
  }
}
