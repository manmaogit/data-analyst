/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.wrapper.function.filter;

import java.util.List;
import lombok.ToString;
import org.apache.commons.compress.utils.Lists;

@ToString(callSuper = true)
public abstract class CompositeFilterWrapper extends AbstractFilterWrapper {

  protected List<AbstractFilterWrapper> filterList = Lists.newArrayList();

  public void addFilter(AbstractFilterWrapper filter) {
    filterList.add(filter);
  }

}
