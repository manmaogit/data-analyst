/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.extent;

import java.io.Serializable;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.flink.table.types.logical.RowType;

/**
 * Created by 王纯超 on 2021/8/10.
 */
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class SqlContext implements Serializable {
  private String nodeName;
  private List<RowType.RowField> fields;
}
