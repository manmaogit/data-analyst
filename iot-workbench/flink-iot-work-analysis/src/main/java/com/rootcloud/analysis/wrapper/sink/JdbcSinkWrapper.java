/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.wrapper.sink;

import static com.rootcloud.analysis.core.constants.CommonConstant.COMMA;
import static com.rootcloud.analysis.core.constants.CommonConstant.VERTICAL_LINE;
import static com.rootcloud.analysis.core.constants.SchemaConstant.COLUMN_SEPARATOR;

import com.google.common.collect.Sets;
import com.rootcloud.analysis.common.context.IotWorkRuntimeContext;
import com.rootcloud.analysis.common.entity.RcUnit;
import com.rootcloud.analysis.common.util.DateUtil;
import com.rootcloud.analysis.converter.time.ITimeConverter;
import com.rootcloud.analysis.converter.time.TimeConverterFactory;
import com.rootcloud.analysis.core.enums.SinkTypeEnum;
import com.rootcloud.analysis.entity.RcTimeAttr;
import com.rootcloud.analysis.wrapper.WrapperBuilder;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import lombok.Getter;

public class JdbcSinkWrapper extends AbstractSinkWrapper {

  public static final Set<String> RETRY_SQL_STATES = Sets.newHashSet("23000", "23505");

  public String uri;
  public String username;
  public String password;

  public String table;

  public String insertSql;
  public String updateSql;
  public String selectSql;

  public List<String> insertFields;
  public List<String> updateFields;
  public List<String> selectFields;

  public ITimeConverter timeConverter;

  public RcTimeAttr timeAttr;

  @Getter
  public ZoneId zoneId;

  public Map<String, String> attributeMapper;

  public String insertType;

  @Getter
  public int startMonth;

  @Getter
  public int startDay;

  @Getter
  public int startHour;

  @Getter
  public int startMinute;

  public List<String> accAttrs;

  public boolean timeAggregationFlag;

  /**
   * builder.
   */
  public JdbcSinkWrapper(JdbcSinkWrapper.Builder builder) {
    super(builder.jobId, builder.jobName, builder.tenantId, builder.nodeId, builder.parallelism,
        builder.nodeName, builder.priorNodeId, builder.sinkType,
        builder.logType);

    this.uri = builder.uri;
    this.username = builder.username;
    this.password = builder.password;

    this.table = builder.table;

    this.insertSql = builder.insertSql;
    this.updateSql = builder.updateSql;
    this.selectSql = builder.selectSql;

    this.insertFields = builder.insertFields;
    this.updateFields = builder.updateFields;
    this.selectFields = builder.selectFields;

    this.timeAttr = builder.timeAttr;

    this.zoneId = builder.timeZone;

    this.timeConverter = TimeConverterFactory.getConverter(builder.timeDimension);

    this.attributeMapper = builder.attributeMapper;

    this.insertType = builder.insertType;

    this.startMonth = builder.startMonth;
    this.startDay = builder.startDay;
    this.startHour = builder.startHour;
    this.startMinute = builder.startMinute;

    this.accAttrs = builder.accAttrs;

    this.timeAggregationFlag = builder.timeAggregationFlag;
  }

  public static class Builder extends WrapperBuilder {

    public String nodeId;
    public String nodeName;
    public Integer parallelism;

    public String priorNodeId;
    public SinkTypeEnum sinkType;

    public String uri;
    public String username;
    public String password;
    public String table;
    public String insertKey;
    public String insertType;
    public Map<String, String> attributeMapper;
    public RcTimeAttr timeAttr;
    public String timeDimension;
    public int startMonth;
    public int startDay;
    public int startHour;
    public int startMinute;
    public ZoneId timeZone;
    public List<String> accAttrs;
    public List<String> replaceAttrs;
    public List<String> replaceWithNonNullAttrs;
    public List<String> greatestAttrs;
    public List<String> leastAttrs;
    public boolean timeAggregationFlag;

    public List<String> schemas;

    public String insertSql;
    public String updateSql;
    public String selectSql;

    public List<String> insertFields = new ArrayList<>();
    public List<String> updateFields = new ArrayList<>();
    public List<String> selectFields = new ArrayList<>();

    public JdbcSinkWrapper.Builder setNodeId(String nodeId) {
      this.nodeId = nodeId;
      return this;
    }

    public JdbcSinkWrapper.Builder setNodeName(String nodeName) {
      this.nodeName = nodeName;
      return this;
    }

    public JdbcSinkWrapper.Builder setParallelism(Integer parallelism) {
      this.parallelism = parallelism;
      return this;
    }

    /**
     * Sets start time.
     *
     * @param startTime start time
     *
     * @return builder
     */
    public JdbcSinkWrapper.Builder setStartTime(String startTime) {
      String[] tmp = startTime.split(VERTICAL_LINE);
      startMonth = tmp.length - 4 >= 0 ? Integer.parseInt(tmp[tmp.length - 4]) : 1;
      startDay = tmp.length - 3 >= 0 ? Integer.parseInt(tmp[tmp.length - 3]) : 1;
      startHour = tmp.length - 2 >= 0 ? Integer.parseInt(tmp[tmp.length - 2]) : 0;
      startMinute = tmp.length - 1 >= 0 ? Integer.parseInt(tmp[tmp.length - 1]) : 0;
      return this;
    }

    public JdbcSinkWrapper.Builder setUri(String uri) {
      this.uri = uri;
      return this;
    }

    public JdbcSinkWrapper.Builder setInsertType(String insertType) {
      this.insertType = insertType;
      return this;
    }

    public JdbcSinkWrapper.Builder setUsername(String username) {
      this.username = username;
      return this;
    }

    public JdbcSinkWrapper.Builder setAttributeMapper(Map<String, String> attributeMapper) {
      this.attributeMapper = attributeMapper;
      return this;
    }


    public JdbcSinkWrapper.Builder setSchemas(List<String> schemas) {
      this.schemas = schemas;
      return this;
    }

    public JdbcSinkWrapper.Builder setPassword(String password) {
      this.password = password;
      return this;
    }

    public JdbcSinkWrapper.Builder setInsertKey(String insertKey) {
      this.insertKey = insertKey;
      return this;
    }

    public JdbcSinkWrapper.Builder setAccAttrs(List<String> accAttrs) {
      this.accAttrs = accAttrs;
      return this;
    }

    public JdbcSinkWrapper.Builder setReplaceAttrs(List<String> replaceAttrs) {
      this.replaceAttrs = replaceAttrs;
      return this;
    }

    public JdbcSinkWrapper.Builder setReplaceWithNonNullAttrs(
        List<String> replaceWithNonNullAttrs) {
      this.replaceWithNonNullAttrs = replaceWithNonNullAttrs;
      return this;
    }

    public JdbcSinkWrapper.Builder setGreatestAttrs(List<String> greatestAttrs) {
      this.greatestAttrs = greatestAttrs;
      return this;
    }

    public JdbcSinkWrapper.Builder setLeastAttrs(List<String> leastAttrs) {
      this.leastAttrs = leastAttrs;
      return this;
    }

    public JdbcSinkWrapper.Builder setTimeDimension(String timeDimension) {
      this.timeDimension = timeDimension;
      return this;
    }

    public JdbcSinkWrapper.Builder setTimeAttr(RcTimeAttr timeAttr) {
      this.timeAttr = timeAttr;
      return this;
    }

    public JdbcSinkWrapper.Builder setTimeZone(ZoneId timeZone) {
      this.timeZone = timeZone;
      return this;
    }

    public JdbcSinkWrapper.Builder setPriorNodeId(String priorNodeId) {
      this.priorNodeId = priorNodeId;
      return this;
    }

    public JdbcSinkWrapper.Builder setSinkType(SinkTypeEnum sinkType) {
      this.sinkType = sinkType;
      return this;
    }

    public JdbcSinkWrapper.Builder setTable(String table) {
      this.table = table;
      return this;
    }

    public JdbcSinkWrapper.Builder setTimeAggregationFlag(boolean timeAggregationFlag) {
      this.timeAggregationFlag = timeAggregationFlag;
      return this;
    }

    /**
     * build.
     */
    public JdbcSinkWrapper build() {
      this.table = buildTableName();
      this.insertSql = buildInsertSql();
      this.updateSql = buildUpdateSql();
      this.selectSql = buildSelectSql();
      return new JdbcSinkWrapper(this);
    }

    public String buildTableName() {
      return table;
    }


    /**
     * buildInsertSql.
     */
    public String buildInsertSql() {
      StringBuilder builder = new StringBuilder();
      builder.append("insert into ");
      builder.append("\"" + table + "\"");
      builder.append(" (");

      for (int index = 0; index < schemas.size(); index++) {
        String[] unit = schemas.get(index).split(COLUMN_SEPARATOR);
        String field = unit[0];

        if (index > 0) {
          builder.append(COMMA);
        }
        if (!field.startsWith("\"")) {
          if (sinkType.equals(SinkTypeEnum.ORACLE)) {
            builder.append("\"" + field + "\"");
          }
        } else {
          builder.append(field);
        }

        insertFields.add(field);
      }
      builder.append(") values(");
      for (int index = 0; index < schemas.size(); index++) {
        builder.append("?");
        if (index < schemas.size() - 1) {
          builder.append(COMMA);
        }
      }
      builder.append(")");
      return builder.toString();
    }

    /**
     * buildInsertSql.
     */
    public String buildUpdateSql() {
      if (accAttrs.isEmpty() && replaceAttrs.isEmpty() && greatestAttrs.isEmpty() && leastAttrs
          .isEmpty() && replaceWithNonNullAttrs.isEmpty()) {
        return null;
      }
      StringBuilder builder = new StringBuilder();
      builder.append("update ");
      builder.append("\"" + table + "\"");
      builder.append(" set ");

      for (int index = 0; index < accAttrs.size(); index++) {
        String field = accAttrs.get(index);
        if (!field.startsWith("\"")) {
          if (sinkType.equals(SinkTypeEnum.ORACLE)) {
            builder.append("\"" + field + "\"");
          }
        } else {
          builder.append(field);
        }

        builder.append("=");
        //coalesce
        if (SinkTypeEnum.ORACLE.equals(sinkType)) {
          builder.append("NVL(");
        } else if (SinkTypeEnum.PG.equals(sinkType)) {
          builder.append("COALESCE(");
        } else {
          builder.append("IFNULL(");
        }
        if (!field.startsWith("\"")) {
          if (sinkType.equals(SinkTypeEnum.ORACLE)) {
            builder.append("\"" + field + "\"");
          }
        } else {
          builder.append(field);
        }
        builder.append(",0)");
        builder.append("+?");

        if (index < accAttrs.size() - 1) {
          builder.append(COMMA);
        }

        updateFields.add(field);
      }

      if (replaceAttrs != null && !replaceAttrs.isEmpty()) {
        replaceAttrs.removeAll(accAttrs);
        for (int i = 0; i < replaceAttrs.size(); i++) {
          String field = replaceAttrs.get(i);
          if (i > 0 || !accAttrs.isEmpty()) {
            builder.append(COMMA);
          }
          builder.append(field);
          builder.append("=");
          builder.append("?");
          updateFields.add(field);
        }
      }

      if (replaceWithNonNullAttrs != null && !replaceWithNonNullAttrs.isEmpty()) {
        for (int i = 0; i < replaceWithNonNullAttrs.size(); i++) {
          String field = replaceWithNonNullAttrs.get(i);
          if (updateFields.size() > 0) {
            builder.append(COMMA);
          }
          builder.append(field);
          builder.append("=");

          //coalesce
          if (SinkTypeEnum.ORACLE.equals(sinkType)) {
            builder.append("NVL(");
          } else if (SinkTypeEnum.PG.equals(sinkType)) {
            builder.append("COALESCE(");
          } else {
            builder.append("IFNULL(");
          }

          builder.append("?");

          builder.append(",").append(field).append(")");
          updateFields.add(field);
        }
      }

      if (greatestAttrs != null && !greatestAttrs.isEmpty()) {
        greatestAttrs.removeAll(accAttrs);
        greatestAttrs.removeAll(replaceAttrs);
        for (int i = 0; i < greatestAttrs.size(); i++) {
          if (i > 0 || !updateFields.isEmpty()) {
            builder.append(COMMA);
          }
          String field = greatestAttrs.get(i);
          builder.append(field);
          builder.append("=GREATEST(");
          if (SinkTypeEnum.ORACLE.equals(sinkType)) {
            builder.append("NVL(");
          } else if (SinkTypeEnum.PG.equals(sinkType)) {
            builder.append("COALESCE(");
          } else {
            builder.append("IFNULL(");
          }
          if (!field.startsWith("\"")) {
            if (sinkType.equals(SinkTypeEnum.ORACLE)) {
              builder.append("\"" + field + "\"");
            }
          } else {
            builder.append(field);
          }
          builder.append(",?)");
          builder.append(COMMA);
          if (SinkTypeEnum.ORACLE.equals(sinkType)) {
            builder.append("NVL(");
          } else if (SinkTypeEnum.PG.equals(sinkType)) {
            builder.append("COALESCE(");
          } else {
            builder.append("IFNULL(");
          }
          builder.append("? ,");
          if (!field.startsWith("\"")) {
            if (sinkType.equals(SinkTypeEnum.ORACLE)) {
              builder.append("\"" + field + "\"");
            }
          } else {
            builder.append(field);
          }
          builder.append("))");
          updateFields.add(field);
          updateFields.add(field);
        }
      }

      if (leastAttrs != null && !leastAttrs.isEmpty()) {
        leastAttrs.removeAll(accAttrs);
        leastAttrs.removeAll(replaceAttrs);
        leastAttrs.removeAll(greatestAttrs);
        for (int i = 0; i < leastAttrs.size(); i++) {
          if (i > 0 || !updateFields.isEmpty()) {
            builder.append(COMMA);
          }
          String field = leastAttrs.get(i);
          builder.append(field);
          builder.append("=LEAST(");
          if (SinkTypeEnum.ORACLE.equals(sinkType)) {
            builder.append("NVL(");
          } else if (SinkTypeEnum.PG.equals(sinkType)) {
            builder.append("COALESCE(");
          } else {
            builder.append("IFNULL(");
          }
          if (!field.startsWith("\"")) {
            if (sinkType.equals(SinkTypeEnum.ORACLE)) {
              builder.append("\"" + field + "\"");
            }
          } else {
            builder.append(field);
          }
          builder.append(",?)");
          builder.append(COMMA);
          if (SinkTypeEnum.ORACLE.equals(sinkType)) {
            builder.append("NVL(");
          } else if (SinkTypeEnum.PG.equals(sinkType)) {
            builder.append("COALESCE(");
          } else {
            builder.append("IFNULL(");
          }
          builder.append("? ,");
          if (!field.startsWith("\"")) {
            if (sinkType.equals(SinkTypeEnum.ORACLE)) {
              builder.append("\"" + field + "\"");
            }
          } else {
            builder.append(field);
          }
          builder.append("))");
          updateFields.add(field);
          updateFields.add(field);
        }
      }
      if (updateFields.size() == 0) {
        return null;
      }
      builder.append(" where ");

      String[] insetKeyTmp = insertKey.split(COMMA);
      for (int index = 0; index < insetKeyTmp.length; index++) {
        String field = insetKeyTmp[index];
        if (!field.startsWith("\"")) {
          if (sinkType.equals(SinkTypeEnum.ORACLE)) {
            builder.append("\"" + field + "\"");
          }
        } else {
          builder.append(field);
        }
        builder.append("=?");
        if (index < insetKeyTmp.length - 1) {
          builder.append(" and ");
        }
        updateFields.add(field);
      }

      return builder.toString();
    }

    /**
     * buildSelectSql.
     */
    public String buildSelectSql() {
      StringBuilder builder = new StringBuilder();
      builder.append("select * from ");
      builder.append("\"" + table + "\"");
      builder.append(" where ");

      String[] temp = insertKey.split(COMMA);
      for (int index = 0; index < temp.length; index++) {
        String field = temp[index];
        if (index > 0) {
          builder.append(" and ");
        }

        if (!field.startsWith("\"")) {
          if (sinkType.equals(SinkTypeEnum.ORACLE)) {
            builder.append("\"" + field + "\"");
          }
        } else {
          builder.append(field);
        }
        builder.append("=?");

        selectFields.add(field);

      }
      return builder.toString();
    }

  }

  /**
   * buildSql.
   */
  public void buildSql(PreparedStatement statement,
                       RcUnit record,
                       List<String> fieldList,
                       Map<String, String> schema)
      throws SQLException, ParseException {
    for (int index = 0; index < fieldList.size(); index++) {
      String field = fieldList.get(index);
      Object itemValue = record.getAttr(attributeMapper.get(field));
      itemValue = itemValue == null && accAttrs.contains(field) ? 0 : itemValue;

      if (timeAggregationFlag && timeAttr != null && field.equals(timeAttr.getKey())) {
        if (itemValue instanceof String) {
          String convert = timeConverter.convert((String) itemValue, zoneId);
          statement.setObject(index + 1, Timestamp.valueOf(convert));
        } else if (itemValue instanceof Long) {
          String convert = timeConverter.convert((Long) itemValue, zoneId,
              startMonth, startDay, startHour, startMinute);
          statement.setObject(index + 1, Timestamp.valueOf(convert));
        } else {
          Timestamp timestamp = (Timestamp) itemValue;
          String convert = timeConverter.convert(timestamp.getTime(), zoneId,
              startMonth, startDay, startHour, startMinute);
          statement.setObject(index + 1, Timestamp.valueOf(convert));
        }

      } else {
        String fidldName = schema.get(field).toLowerCase();

        if (fidldName.contains("(")) {
          fidldName = schema.get(field).toLowerCase().substring(0,
              schema.get(field).indexOf("("));
        }
        if (fidldName.contains(" ")) {
          fidldName = schema.get(field).toLowerCase().substring(0,
              schema.get(field).indexOf(" "));
        }
        switch (fidldName) {
          case "datetime":
          case "timestamp":
          case "date":
          case "time":
            statement.setObject(index + 1, Timestamp.valueOf(DateUtil.formatNormalDateString(
                itemValue instanceof Timestamp ? ((Timestamp) itemValue).getTime()
                    : (Long) itemValue, "yyyy-MM-dd HH:mm:ss.SSS")));
            continue;
          default:
            statement.setObject(index + 1, itemValue);
            continue;
        }
      }

    }


  }


  @Override
  public void registerOutput(IotWorkRuntimeContext context) {
  }

}
