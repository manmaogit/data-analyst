/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.wrapper.source;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.rootcloud.analysis.common.context.IotWorkRuntimeContext;
import com.rootcloud.analysis.common.entity.RcUnit;
import com.rootcloud.analysis.common.factory.KafkaLogMiddlewareResourceFactory;
import com.rootcloud.analysis.core.constants.GrayLogConstant;
import com.rootcloud.analysis.core.constants.ShortMessageConstant;
import com.rootcloud.analysis.core.enums.GrayLogResultEnum;
import com.rootcloud.analysis.core.enums.MethodEnum;
import com.rootcloud.analysis.core.graylog.LogVo;
import com.rootcloud.analysis.log.loggingservice.LoggingServiceUtil;
import com.rootcloud.analysis.wrapper.WrapperBuilder;
import com.rootcloud.analysis.wrapper.api.RestConnWrapper;
import java.util.Set;
import org.apache.flink.api.common.typeinfo.TypeHint;
import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.source.RichSourceFunction;
import org.apache.flink.streaming.connectors.kafka.internals.FlinkKafkaInternalProducer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * IDF-设备台账-输入节点-实时任务.
 */
public class RestSourceWrapper extends AbstractSourceWrapper {

  private static Logger logger = LoggerFactory.getLogger(RestSourceWrapper.class);
  private final JSONArray headers;
  private final Integer detectPeriod;
  private final String url;
  private final MethodEnum method;
  private final JSONArray parameters;
  private final JSONObject body;
  /**
   * whether to emit the items in the returned dataset one by one. Default to false, which means
   * emitting the dataset as a whole.
   */
  private final boolean emitOnebyOne;

  /**
   * whether this source is sensitive to the changes which may occur in the response by the url.
   */
  private boolean isChangeSensitive = true;

  /**
   * HTTP超时时长（单位：秒）.
   */
  private int timeOutSeconds;

  /**
   * HTTP查询分页大小.
   */
  private int pageLimit;

  protected RestSourceWrapper(Builder builder) {
    super(builder.jobId, builder.jobName, builder.tenantId, builder.nodeId, builder.parallelism,
        builder.nodeName, builder.inputType, builder.schema, builder.logType);
    this.headers = builder.headers;
    this.body = builder.body;
    this.detectPeriod = builder.detectPeriod;
    this.url = builder.url;
    this.method = builder.method;
    this.parameters = builder.parameters;
    this.emitOnebyOne = builder.emitOnebyOne;
    this.isChangeSensitive = builder.isChangeSensitive;
    this.timeOutSeconds = builder.timeOutSeconds;
    this.pageLimit = builder.pageLimit;
  }

  @Override
  public void registerInput(IotWorkRuntimeContext context) {
    StreamExecutionEnvironment env = context.getExecutionEnv();
    RestConnWrapper restConnWrapper = new RestConnWrapper(url, getSchema(), method, headers,
        parameters, body, timeOutSeconds, pageLimit);
    context.putSourceConnToCache(getNodeId(), restConnWrapper);
    SingleOutputStreamOperator sourceStream = env
        .addSource(new RestSourceFunction(restConnWrapper, getKafkaLoggingServiceServers(),
            getKafkaLoggingServiceTopics())).uid(getNodeId())
        .setParallelism(1).name(getNodeName());
    if (emitOnebyOne) {
      sourceStream.returns(RcUnit.class);
    } else {
      sourceStream.returns(TypeInformation.of(new TypeHint<Set<RcUnit>>() {
      }));
    }

    context.putObjectToContainer(getNodeId(), sourceStream);
  }

  public static class Builder extends WrapperBuilder {

    private boolean isChangeSensitive = true;
    private boolean emitOnebyOne;
    private String nodeId;
    private String nodeName;
    private Integer parallelism;
    private String inputType;
    private JSONArray schema;
    private JSONArray headers;
    private JSONObject body;
    private Integer detectPeriod;
    private String url;
    private MethodEnum method;
    private JSONArray parameters;
    private int timeOutSeconds;
    private int pageLimit;

    public Builder setHeaders(JSONArray headers) {
      this.headers = headers;
      return this;
    }

    public Builder setBody(JSONObject body) {
      this.body = body;
      return this;
    }

    public Builder setDetectPeriod(Integer detectPeriod) {
      this.detectPeriod = detectPeriod;
      return this;
    }

    public Builder setUrl(String url) {
      this.url = url;
      return this;
    }

    public Builder setMethod(MethodEnum method) {
      this.method = method;
      return this;
    }

    public Builder setParameters(JSONArray parameters) {
      this.parameters = parameters;
      return this;
    }

    public Builder setNodeName(String nodeName) {
      this.nodeName = nodeName;
      return this;
    }

    public Builder setNodeId(String nodeId) {
      this.nodeId = nodeId;
      return this;
    }

    public Builder setSchema(JSONArray schema) {
      this.schema = schema;
      return this;
    }

    public Builder setParallelism(Integer parallelism) {
      this.parallelism = parallelism;
      return this;
    }

    public Builder setInputType(String inputType) {
      this.inputType = inputType;
      return this;
    }

    public Builder setEmitOnebyOne(boolean emitOnebyOne) {
      this.emitOnebyOne = emitOnebyOne;
      return this;
    }

    public Builder setChangeSensitive(boolean changeSensitive) {
      isChangeSensitive = changeSensitive;
      return this;
    }

    public Builder setTimeOutSeconds(int timeOutSeconds) {
      this.timeOutSeconds = timeOutSeconds;
      return this;
    }

    public Builder setPageLimit(int pageLimit) {
      this.pageLimit = pageLimit;
      return this;
    }

    public RestSourceWrapper build() {
      return new RestSourceWrapper(this);
    }
  }

  public class RestSourceFunction extends RichSourceFunction {

    private RestConnWrapper restConnWrapper;
    private volatile boolean isRunning = true;
    private final String kafkaLoggingServiceServers;
    private final String kafkaLoggingServiceTopics;
    private FlinkKafkaInternalProducer flinkKafkaInternalProducer;

    /**
     * 构造方法.
     */
    public RestSourceFunction(RestConnWrapper restConnWrapper, String kafkaLoggingServiceServers,
                              String kafkaLoggingServiceTopics) {
      this.restConnWrapper = restConnWrapper;
      this.kafkaLoggingServiceServers = kafkaLoggingServiceServers;
      this.kafkaLoggingServiceTopics = kafkaLoggingServiceTopics;
    }

    public boolean isRunning() {
      return isRunning;
    }

    @Override
    public void run(SourceContext ctx) throws Exception {
      if (flinkKafkaInternalProducer == null && kafkaLoggingServiceServers != null) {
        flinkKafkaInternalProducer = KafkaLogMiddlewareResourceFactory.getProducer(
            kafkaLoggingServiceServers,
            this.toString());
      }
      while (isRunning() && !Thread.currentThread().isInterrupted()) {
        try {
          Set<RcUnit> result = null;
          if (!Thread.currentThread().isInterrupted()) {
            result = restConnWrapper.getData();
          }
          if (!Thread.currentThread().isInterrupted() && result != null) {
            if (emitOnebyOne) {
              for (RcUnit rcUnit : result) {
                ctx.collect(rcUnit);
              }
            } else {
              ctx.collect(result);
            }
          }
        } catch (Exception e) {
          LoggingServiceUtil.alarm(LogVo.builder()
              .k8sServiceName(GrayLogConstant.K8S_SERVICE_NAME)
              .module(GrayLogConstant.MODULE_NAME)
              .userName(getTenantId())
              .tenantId(getTenantId())
              .operateObjectName(getJobName())
              .operateObject(getJobId())
              .shortMessage(String.format("Node %s failed to process the result returned by"
                  + " asset API. Asset API URL: %s. Cause: %s",getNodeName(),url,e.toString()))
              .operation(ShortMessageConstant.INVOKE_REST_API)
              .requestId(String.valueOf(System.currentTimeMillis()))
              .result(GrayLogResultEnum.FAIL.getValue())
              .logType(getLogType())
              .kafkaLoggingServiceTopic(kafkaLoggingServiceTopics)
              .kafkaLoggingServiceServers(kafkaLoggingServiceServers)
              .build(), flinkKafkaInternalProducer);
        }

        if (!isChangeSensitive) {
          return;
        }

        try {
          Thread.sleep(detectPeriod * 1000);
        } catch (InterruptedException ex) {
          logger.warn("Thread is interrupted.");
          break;
        } catch (NullPointerException ne) {
          logger.warn("detectPeriod is null.");
          break;
        } catch (Exception e) {
          logger.error(e.toString());
        }
      }
    }


    @Override
    public void cancel() {
      isRunning = false;
    }

    @Override
    public void close() throws Exception {

      logger.info("Rest-api source is closed.");
      LoggingServiceUtil.info(LogVo.builder()
          .k8sServiceName(GrayLogConstant.K8S_SERVICE_NAME)
          .module(GrayLogConstant.MODULE_NAME)
          .userName(getTenantId())
          .userId(getUserId())
          .tenantId(getTenantId())
          .operateObjectName(getJobName())
          .operateObject(getJobId())
          .shortMessage(String.format("Node %s disconnected from asset API service.",getNodeName()))
          .operation(ShortMessageConstant.CLOSE_REST_API)
          .requestId(String.valueOf(System.currentTimeMillis()))
          .result(GrayLogResultEnum.SUCCESS.getValue())
          .logType(getLogType())
          .kafkaLoggingServiceTopic(kafkaLoggingServiceTopics)
          .kafkaLoggingServiceServers(kafkaLoggingServiceServers)
          .build(), flinkKafkaInternalProducer);
      KafkaLogMiddlewareResourceFactory.close(this.toString());
      flinkKafkaInternalProducer = null;
      restConnWrapper.close();
      restConnWrapper = null;
      super.close();
    }

  }
}
