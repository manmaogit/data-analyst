/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.wrapper.api;

import com.alibaba.fastjson.JSONArray;
import com.rootcloud.analysis.common.entity.RcUnit;
import com.rootcloud.analysis.common.jdbc.ConnectionWrapper;
import com.rootcloud.analysis.wrapper.api.IApiWrapper;
import com.rootcloud.analysis.wrapper.jdbc.IJdbcWrapper;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.Set;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class ApiConnWrapper implements IApiWrapper {

  private static final long serialVersionUID = 1L;


  private final JSONArray schema;

  public ApiConnWrapper(JSONArray schema) {
    this.schema = schema;
  }

  /**
   * 获取schema.
   */
  public JSONArray getSchema() {
    return this.schema;
  }

  /**
   * 查询数据.
   */
  @Override
  public abstract Set<RcUnit> getData() throws Exception;


}
