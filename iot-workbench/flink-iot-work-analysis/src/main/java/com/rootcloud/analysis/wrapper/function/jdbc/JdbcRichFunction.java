/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.wrapper.function.jdbc;

import com.rootcloud.analysis.common.factory.KafkaLogMiddlewareResourceFactory;
import com.rootcloud.analysis.wrapper.jdbc.JdbcConnWrapper;
import java.util.concurrent.TimeUnit;
import org.apache.flink.api.common.functions.AbstractRichFunction;
import org.apache.flink.streaming.connectors.kafka.internals.FlinkKafkaInternalProducer;


public abstract class JdbcRichFunction extends AbstractRichFunction {

  private static final long serialVersionUID = 1L;
  public JdbcConnWrapper jdbcConnWrapper;
  public FlinkKafkaInternalProducer flinkKafkaInternalProducer;


  /**
   * 构造方法.
   */
  public JdbcRichFunction(JdbcConnWrapper jdbcConnWrapper) {
    this.jdbcConnWrapper = jdbcConnWrapper;
  }

  /**
   * 关闭 kafka Producer.
   */
  public void closeProducer() {
    if (flinkKafkaInternalProducer != null) {
      KafkaLogMiddlewareResourceFactory.close(this.toString());
      flinkKafkaInternalProducer = null;
    }
  }

  @Override
  public void close() throws Exception {
    jdbcConnWrapper.close();
    closeProducer();
  }

  public abstract void openProducer(String server);

}
