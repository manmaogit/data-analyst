/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.decoder.source.impl;

import com.alibaba.fastjson.JSONObject;
import com.rootcloud.analysis.context.IotWorkRuntimeDecodeContext;
import com.rootcloud.analysis.core.constants.JobConstant;
import com.rootcloud.analysis.core.enums.DataSourceTypeEnum;
import com.rootcloud.analysis.decoder.source.SourceDecoder;
import com.rootcloud.analysis.exception.DecodeSourceException;
import com.rootcloud.analysis.wrapper.source.HiveSourceWrapper;
import com.rootcloud.analysis.wrapper.source.InnerHiveSourceWrapper;
import com.rootcloud.analysis.wrapper.source.InnerHiveSourceWrapper.Builder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SourceDecoder(dataSourceType = DataSourceTypeEnum.INNER_HIVE)
public class InnerHiveSourceDecoder extends HiveSourceDecoder {

  private static Logger logger = LoggerFactory.getLogger(InnerHiveSourceDecoder.class);

  @Override
  protected HiveSourceWrapper decodeHiveSourceParams(JSONObject conf) {
    try {
      InnerHiveSourceWrapper.Builder builder = new Builder();
      builder.setPartitionIdLowerBound(IotWorkRuntimeDecodeContext.getTimeScope().getStartEpochSecond() / 3600);
      builder.setPartitionIdUpperBound(IotWorkRuntimeDecodeContext.getTimeScope().getEndEpochSecond() / 3600);
      HiveSourceWrapper result = builder.build();
      return result;
    } catch (Exception ex) {
      logger
          .error("Exception occurred when decoding inner hive source: {}, error message: {}", conf,
              ex.toString());
      throw new DecodeSourceException(conf.getString(JobConstant.KEY_NODE_ID),
          conf.toJSONString());
    }
  }

}
