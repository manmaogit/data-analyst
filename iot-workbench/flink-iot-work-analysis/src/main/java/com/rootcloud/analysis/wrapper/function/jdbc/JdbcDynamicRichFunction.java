/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.wrapper.function.jdbc;

import com.rootcloud.analysis.common.factory.KafkaLogMiddlewareResourceFactory;
import com.rootcloud.analysis.wrapper.jdbc.JdbcDynamicConnWrapper;
import org.apache.commons.lang3.StringUtils;
import org.apache.flink.api.common.functions.AbstractRichFunction;
import org.apache.flink.streaming.connectors.kafka.internals.FlinkKafkaInternalProducer;


public abstract class JdbcDynamicRichFunction extends AbstractRichFunction {

  private static final long serialVersionUID = 1L;
  public JdbcDynamicConnWrapper jdbcDynamicConnWrapper;
  private FlinkKafkaInternalProducer flinkKafkaInternalProducer;

  public FlinkKafkaInternalProducer getFlinkKafkaInternalProducer() {
    return flinkKafkaInternalProducer;
  }

  /**
   * 构造方法.
   */
  public JdbcDynamicRichFunction(JdbcDynamicConnWrapper jdbcDynamicConnWrapper) {
    this.jdbcDynamicConnWrapper = jdbcDynamicConnWrapper;
  }


  @Override
  public void close() throws Exception {
    jdbcDynamicConnWrapper.close();
    KafkaLogMiddlewareResourceFactory.close(this.toString());
    flinkKafkaInternalProducer = null;
  }

  /**
   * 打开kafkaProducer.
   */
  public void openProducer(String server) {
    if (flinkKafkaInternalProducer == null && !StringUtils.isBlank(server)) {
      flinkKafkaInternalProducer = KafkaLogMiddlewareResourceFactory.getProducer(server, this.toString());
    }
  }


}
