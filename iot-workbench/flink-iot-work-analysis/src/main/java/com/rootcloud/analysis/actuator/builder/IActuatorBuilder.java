/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.actuator.builder;

import com.rootcloud.analysis.core.exec.Actuator;
import com.rootcloud.analysis.core.graph.IGraph;
import com.rootcloud.analysis.wrapper.IWrapper;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

public interface IActuatorBuilder {

  Actuator build(String jobName, String jobType, IGraph<IWrapper> graph, StreamExecutionEnvironment env)
      throws Exception;
}
