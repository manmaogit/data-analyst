/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.wrapper;

import com.rootcloud.analysis.common.entity.RcUnit;

import java.util.Set;

public interface IDataSinkWrapper extends IWrapper {

  /**
   * 开启连接.
   */
  public void open();

  /**
   * 关闭资源.
   */
  public void close();
}
