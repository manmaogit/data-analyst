/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.function.map;

import com.alibaba.fastjson.JSONArray;
import com.google.common.collect.Maps;
import com.rootcloud.analysis.common.entity.RcUnit;
import com.rootcloud.analysis.core.constants.GrayLogConstant;
import com.rootcloud.analysis.core.constants.ShortMessageConstant;
import com.rootcloud.analysis.core.enums.GrayLogResultEnum;
import com.rootcloud.analysis.core.enums.GraylogLogTypeEnum;
import com.rootcloud.analysis.core.graylog.GrayLogUtils;
import com.rootcloud.analysis.core.graylog.GrayLogVo;
import com.rootcloud.analysis.core.graylog.LogVo;
import com.rootcloud.analysis.core.log.LogUtil;
import com.rootcloud.analysis.log.loggingservice.LoggingServiceUtil;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import lombok.AllArgsConstructor;
import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.types.Row;
import org.apache.flink.util.Collector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by 王纯超 on 2021/9/8.
 * This function is used to convert Row to RcUnit.
 */
@AllArgsConstructor
public class Row2RcUnitFlatMap implements FlatMapFunction<Tuple2<Boolean, Row>, RcUnit> {
  private static final Logger LOGGER = LoggerFactory.getLogger(Row2RcUnitFlatMap.class);
  private String jobId;
  private String tenantId;
  private String userId;
  private String jobName;
  private String kafkaLoggingServiceTopics;
  private String kafkaLoggingServiceServers;
  /**
   * The processor node name.
   */
  private String enclosingNode;

  /**
   * The field names of the incoming Row data.
   */
  private final List<String> fieldList;

  /**
   * This object describes the resulting RcUnit instance of this function
   * in terms of name, type etc.
   */
  private final JSONArray schema;

  private final GraylogLogTypeEnum logType;

  /**
   * Flag of whether to keep the retracted rows.
   * This should always be set to false except for a few cases.
   */
  private final boolean keepRetractedRow;

  /**
   * Flag of whether it is a bounded stream.
   * This will affect how a row is converted to a RcUnit.
   */
  private final boolean isOffline;

  @Override
  public void flatMap(Tuple2<Boolean, Row> value, Collector<RcUnit> out) throws Exception {
    if (value == null) {
      return;
    }

    if (!keepRetractedRow && !value.f0) {
      return;
    }

    Map<String, Object> map = Maps.newHashMap();
    try {
      for (int i = 0; i < fieldList.size(); i++) {
        String key = fieldList.get(i);
        Object val = value.f1.getField(i);
        if (val instanceof LocalDateTime) {
          val = Timestamp.valueOf((LocalDateTime) val);
        }
        map.put(key, val);
      }
      RcUnit result = null;
      if (isOffline) {
        result = RcUnit.valueOfDataTypeInfo(map, schema);
      } else {
        result = RcUnit.valueOf(map, schema);
      }
      LOGGER.debug(enclosingNode + " output: {}", result);
      if (!Thread.currentThread().isInterrupted()) {
        out.collect(result);
      }
    } catch (Exception ex) {
      LOGGER.error("Exception occurred in " + enclosingNode + " when processing sql result: {}",
          ex.toString());
      LoggingServiceUtil.error(LogVo.builder()
          .k8sServiceName(GrayLogConstant.K8S_SERVICE_NAME)
          .module(GrayLogConstant.MODULE_NAME)
          .userName(tenantId)
          .userId(userId)
          .tenantId(tenantId)
          .operateObjectName(jobName)
          .operateObject(jobId)
          .shortMessage(String.format("Execution failure with node %s. Cause: %s",
              enclosingNode, ex.toString()))
          .operation(ShortMessageConstant.PROCESS_SQL_RESULT)
          .requestId(String.valueOf(System.currentTimeMillis()))
          .result(GrayLogResultEnum.FAIL.getValue())
          .logType(logType)
          .kafkaLoggingServiceTopic(kafkaLoggingServiceTopics)
          .kafkaLoggingServiceServers(kafkaLoggingServiceServers)
          .build());
    }
  }
}
