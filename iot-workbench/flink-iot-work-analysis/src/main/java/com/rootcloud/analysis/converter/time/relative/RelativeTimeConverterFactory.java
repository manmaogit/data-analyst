/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.converter.time.relative;

import com.rootcloud.analysis.scanner.ClasspathPackageScanner;
import java.io.IOException;
import java.lang.reflect.Modifier;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RelativeTimeConverterFactory {

  private static Logger logger = LoggerFactory.getLogger(RelativeTimeConverterFactory.class);
  private static Map<String, IRelativeTimeConverter> relativeTimeConverterHashMap = new HashMap<>();

  static {
    ClasspathPackageScanner scanner = new ClasspathPackageScanner(
        "com.rootcloud.analysis.converter.time.relative.impl");

    try {
      List<String> classList = scanner.getFullyQualifiedClassNameList();

      for (String className : classList) {
        try {
          Class clazz = RelativeTimeConverterFactory.class.getClassLoader().loadClass(
              className.replace("com/rootcloud/analysis/converter/time/relative/impl/", ""));
          if (Modifier.isAbstract(clazz.getModifiers()) || clazz.isInterface()) {
            continue;
          }
          Object obj = clazz.newInstance();
          if (!(obj instanceof IRelativeTimeConverter)) {
            continue;
          }
          IRelativeTimeConverter converter = (IRelativeTimeConverter) obj;
          RelativeTimeConvert anno = converter.getClass().getAnnotation(RelativeTimeConvert.class);
          if (anno == null) {
            continue;
          }
          relativeTimeConverterHashMap.put(anno.timeUnit(), converter);
        } catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
          logger.warn("Exception occurred when initializing IRelativeTimeConverter instance", e);
        }
      }
    } catch (IOException e) {
      logger.error("RelativeTimeConverterFactory init failed", e);
    }

  }

  /**
   * getConverter.
   */
  public static IRelativeTimeConverter getConverter(String timeUnit) {
    if (relativeTimeConverterHashMap.containsKey(timeUnit)) {
      return relativeTimeConverterHashMap.get(timeUnit);
    }

    logger.error("Relative time converter is not found, time unit: {}", timeUnit);
    return null;
  }

}
