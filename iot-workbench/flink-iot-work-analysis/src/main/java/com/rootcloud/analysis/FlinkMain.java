/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis;

import com.alibaba.fastjson.JSONObject;
import com.mongodb.client.FindIterable;
import com.mongodb.client.model.Filters;
import com.rootcloud.analysis.actuator.builder.ActuatorBuilderFactory;
import com.rootcloud.analysis.actuator.builder.IActuatorBuilder;
import com.rootcloud.analysis.common.context.IotWorkRuntimeContext;
import com.rootcloud.analysis.common.factory.KafkaLogMiddlewareResourceFactory;
import com.rootcloud.analysis.context.IotWorkRuntimeDecodeContext;
import com.rootcloud.analysis.core.constants.GrayLogConstant;
import com.rootcloud.analysis.core.constants.JobConstant;
import com.rootcloud.analysis.core.constants.ShortMessageConstant;
import com.rootcloud.analysis.core.enums.GrayLogResultEnum;
import com.rootcloud.analysis.core.enums.GraylogLogTypeEnum;
import com.rootcloud.analysis.core.exception.RcDbException;
import com.rootcloud.analysis.core.exec.Actuator;
import com.rootcloud.analysis.core.graph.IGraph;
import com.rootcloud.analysis.core.graylog.LogVo;
import com.rootcloud.analysis.core.mongodb.RcMongoTemplate;
import com.rootcloud.analysis.decoder.job.AbstractJobDecoder;
import com.rootcloud.analysis.decoder.job.JobDecoderFactory;
import com.rootcloud.analysis.decoder.time.scope.TimeScopeDecoderFactory;
import com.rootcloud.analysis.log.loggingservice.LoggingServiceUtil;

import java.net.URISyntaxException;
import java.time.Duration;
import java.util.Arrays;

import com.rootcloud.analysis.wrapper.IWrapper;
import org.apache.flink.api.java.utils.ParameterTool;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.connectors.kafka.internals.FlinkKafkaInternalProducer;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Flink Runner Entrance.
 */
public class FlinkMain {

    private static Logger logger = LoggerFactory.getLogger(FlinkMain.class);

    public FlinkKafkaInternalProducer flinkKafkaInternalProducer;

    /**
     * Actuator Entrance.
     *
     * @param args 执行参数
     * @throws Exception 异常
     */
    public static void main(String[] args) throws Exception {
        FlinkMain flinkMain = new FlinkMain();
        try {
            logger.info("Parameters:{}", Arrays.toString(args));
            final ParameterTool params = ParameterTool.fromArgs(args);
            flinkMain.start(params, StreamExecutionEnvironment.getExecutionEnvironment());
        } catch (Exception ex) {
            ex.printStackTrace();
            throw ex;
        } finally {
            IotWorkRuntimeDecodeContext.clean();
            ActuatorBuilderFactory.clean();
            JobDecoderFactory.clean();
            if (flinkMain.flinkKafkaInternalProducer != null) {
                flinkMain.flinkKafkaInternalProducer.close(Duration.ofSeconds(3));
                flinkMain.flinkKafkaInternalProducer = null;
            }
        }
    }

    /**
     * Start flink job.
     */
    public void start(ParameterTool params, StreamExecutionEnvironment env) throws Exception {
        final String jobId = params.get(JobConstant.PARAM_JOB_ID);
        final String mongoUri = params.get(JobConstant.PARAM_MONGO_URI);
        final String mongoDatabase = params.get(JobConstant.PARAM_MONGO_DATABASE);
        final String mongoCollection = params.get(JobConstant.PARAM_MONGO_COLLECTION);
        final String systemDatetime = params.get(JobConstant.PARAM_SYSTEM_DATETIME);
        final String loggingServiceKafkaServers = params.get(JobConstant.LOGGING_SERVICE_KAFKA_SERVERS);
        final String loggingServiceKafkaTopic = params.get(JobConstant.LOGGING_SERVICE_KAFKA_TOPIC);
        flinkKafkaInternalProducer = KafkaLogMiddlewareResourceFactory.create(loggingServiceKafkaServers);
        // 查询mongodb, 读取任务配置
        JSONObject jobConfiguration = getJobConfiguration(mongoUri, mongoDatabase, mongoCollection, jobId);
        if (jobConfiguration == null) {
            logger.error("Failed to get job configuration.");
            return;
        }

        final JSONObject jobParam = jobConfiguration.getJSONObject(JobConstant.KEY_PARAMETERS);
        final String jobType = jobParam.getString(JobConstant.KEY_JOB_TYPE);

        String timeZone = jobParam.getString(JobConstant.KEY_TIMEZONE);
        IotWorkRuntimeContext.setTimezone(timeZone);
        String tenantId = jobParam.getString(JobConstant.TENANT_ID);
        String userId = jobParam.getString(JobConstant.CREATOR_ID);
        String jobName = jobParam.getString(JobConstant.KEY_JOB_NAME);
        String privateKey = getPrivateKey(mongoUri, mongoDatabase, tenantId);

        IotWorkRuntimeDecodeContext.setLoggingServiceKafkaServers(loggingServiceKafkaServers);
        IotWorkRuntimeDecodeContext.setLoggingServiceKafkaTopic(loggingServiceKafkaTopic);
        IotWorkRuntimeDecodeContext.setJobId(jobId);
        IotWorkRuntimeDecodeContext.setJobName(jobName);
        IotWorkRuntimeDecodeContext.setTenantId(tenantId);
        IotWorkRuntimeDecodeContext.setUserId(userId);
        IotWorkRuntimeDecodeContext.setPrivateKey(privateKey);
        IotWorkRuntimeDecodeContext.setSystemDatetime(systemDatetime);
        IotWorkRuntimeDecodeContext.setUdfInfos(jobParam.getJSONArray("udf_use"));
        IotWorkRuntimeDecodeContext.setJobType(GraylogLogTypeEnum.valueOf(jobType));

        // Sets time scope.
        if (jobParam.containsKey(JobConstant.KEY_TIME_SCOPE)) {
            IotWorkRuntimeDecodeContext.setTimeScope(TimeScopeDecoderFactory.getDecoder(jobParam.getJSONObject(JobConstant.KEY_TIME_SCOPE).getString(JobConstant.KEY_TYPE))
                    .decode(jobParam.getJSONObject(JobConstant.KEY_TIME_SCOPE)));
        }
        IGraph<IWrapper> graph;
        try {
            // 1.根据任务类型，获取对应的JobDecoder,比如 RealtimeJobDecoder
            AbstractJobDecoder jobDecoder = JobDecoderFactory.getJobDecoder(jobType);
            if (jobDecoder == null) {
                logger.error("Failed to get job decoder.");
                return;
            }
            // 2. 读取任务配置的"job_desc"字段， 创建Flink 算子的 Wrapper封装对象。
            graph = jobDecoder.decode(jobParam.getJSONObject(JobConstant.KEY_JOB_DESC));

        } catch (Exception e) {
            logger.error("Failed to decode job configuration.", e);
            logger.error("error msg：", e);
            throw e;
        }

        Actuator actuator;
        try {
            IActuatorBuilder actuatorBuilder = ActuatorBuilderFactory.getActuatorBuilder(jobType);
            if (actuatorBuilder == null) {
                logger.error("Failed to get actuator builder.");
                return;
            }
            // 3.根据Flink 算子的 Wrapper封装对象， 组装为DataStream流对象，并且创建StreamExecutionEnvironment，构建Flink 的StreamGraph代码
            actuator = actuatorBuilder.build(JobConstant.IOTWORKS + jobId, jobType, graph, env);
            if (actuator == null) {
                logger.error("Failed to build actuator.");
                return;
            }

            // 4.StreamExecutionEnvironment.execute()... 启动Flink 流
            actuator.execute();
            logger.info("Flink job {} started successfully.", jobId);
            LoggingServiceUtil.info(LogVo.builder()
                    .k8sServiceName(GrayLogConstant.K8S_SERVICE_NAME)
                    .module(GrayLogConstant.MODULE_NAME)
                    .userName(tenantId)
                    .userId(userId)
                    .tenantId(tenantId)
                    .operateObjectName(jobName)
                    .operateObject(jobId)
                    .shortMessage(String.format("Flink job %s has been started successfully.", jobName))
                    .operation(ShortMessageConstant.START_REAL_TIME_JOB)
                    .requestId(String.valueOf(System.currentTimeMillis()))
                    .result(GrayLogResultEnum.SUCCESS.getValue())
                    .logType(IotWorkRuntimeDecodeContext.getJobType())
                    .kafkaLoggingServiceServers(IotWorkRuntimeDecodeContext.getLoggingServiceKafkaServers())
                    .kafkaLoggingServiceTopic(IotWorkRuntimeDecodeContext.getLoggingServiceKafkaTopic())
                    .build(), flinkKafkaInternalProducer);
        } catch (Exception e) {
            logger.error("FlinkMain actuator execute failed", e);
            LoggingServiceUtil.alarm(LogVo.builder()
                    .k8sServiceName(GrayLogConstant.K8S_SERVICE_NAME)
                    .module(GrayLogConstant.MODULE_NAME)
                    .userName(tenantId)
                    .tenantId(tenantId)
                    .operateObjectName(jobName)
                    .operateObject(jobId)
                    .shortMessage(String.format("Flink job %s started failed. msg: %s", jobName, e.toString()))
                    .operation(ShortMessageConstant.START_REAL_TIME_JOB)
                    .requestId(String.valueOf(System.currentTimeMillis()))
                    .result(GrayLogResultEnum.FAIL.getValue())
                    .kafkaLoggingServiceServers(IotWorkRuntimeDecodeContext.getLoggingServiceKafkaServers())
                    .kafkaLoggingServiceTopic(IotWorkRuntimeDecodeContext.getLoggingServiceKafkaTopic())
                    .logType(IotWorkRuntimeDecodeContext.getJobType())
                    .build(), flinkKafkaInternalProducer);
            logger.error("error msg：", e);
            throw e;

        }
    }

    /**
     * 获取job configuration.
     *
     * @param mongoUri        mongoUri
     * @param mongoDatabase   mongoDatabase
     * @param mongoCollection mongoCollection
     * @param jobId           jobId
     * @return job configuration
     */
    private JSONObject getJobConfiguration(String mongoUri,
                                           String mongoDatabase,
                                           String mongoCollection,
                                           String jobId) {
        RcMongoTemplate rcMongoTemplate = null;
        try {
            rcMongoTemplate = new RcMongoTemplate(mongoUri);
            // 读取任务配置
            return (JSONObject) JSONObject.parse(rcMongoTemplate.findById(mongoDatabase, mongoCollection, jobId).toJson());
        } catch (URISyntaxException e) {
            logger.error("Exception occurred when parsing mongo uri: {}", e.getMessage(), e);
        } catch (RcDbException e) {
            logger.error("Exception occurred when getting job configuration from mongo: {}",e.getMessage(), e);
        } finally {
            if (rcMongoTemplate != null) {
                rcMongoTemplate.close();
            }
        }
        return null;
    }


    /**
     * 获取job privateKey.
     */
    private String getPrivateKey(String mongoUri,
                                 String mongoDatabase,
                                 String tenantId) {
        RcMongoTemplate rcMongoTemplate = null;
        try {
            rcMongoTemplate = new RcMongoTemplate(mongoUri);
            Bson eq = Filters.eq(JobConstant.TENANT_ID, tenantId);
            FindIterable<Document> documents = rcMongoTemplate.find(mongoDatabase, JobConstant.MONGO_KEYPAIR_COLLECTION, eq);
            Document document = documents.iterator().next();
            return document.getString(JobConstant.PRIVATE_KEY);
        } catch (URISyntaxException e) {
            logger.error("Exception occurred when parsing mongo uri: {}", e.getMessage(), e);
        } catch (RcDbException e) {
            logger.error("Exception occurred when getting job configuration from mongo: {}", e.getMessage(), e);
        } finally {
            if (rcMongoTemplate != null) {
                rcMongoTemplate.close();
            }
        }
        return null;
    }
}
