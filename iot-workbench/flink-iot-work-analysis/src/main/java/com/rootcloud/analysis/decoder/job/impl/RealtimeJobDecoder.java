/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.decoder.job.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.rootcloud.analysis.context.IotWorkRuntimeBuildContext;
import com.rootcloud.analysis.context.IotWorkRuntimeDecodeContext;
import com.rootcloud.analysis.core.constants.JobConstant;
import com.rootcloud.analysis.core.enums.JobTypeEnum;
import com.rootcloud.analysis.core.enums.ProcessTypeEnum;
import com.rootcloud.analysis.core.graph.IGraph;
import com.rootcloud.analysis.core.graph.RealtimeJobGraph;
import com.rootcloud.analysis.decoder.job.AbstractJobDecoder;
import com.rootcloud.analysis.decoder.job.JobDecoder;
import com.rootcloud.analysis.decoder.processor.AbstractProcessorDecoder;
import com.rootcloud.analysis.decoder.processor.ProcessorDecoderFactory;
import com.rootcloud.analysis.decoder.sink.AbstractSinkDecoder;
import com.rootcloud.analysis.decoder.sink.SinkDecoderFactory;
import com.rootcloud.analysis.decoder.source.AbstractSourceDecoder;
import com.rootcloud.analysis.decoder.source.SourceDecoderFactory;
import com.rootcloud.analysis.wrapper.IWrapper;
import com.rootcloud.analysis.wrapper.processor.AbstractProcessorWrapper;
import com.rootcloud.analysis.wrapper.sink.AbstractSinkWrapper;
import com.rootcloud.analysis.wrapper.source.AbstractSourceWrapper;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import org.apache.commons.compress.utils.Lists;

@JobDecoder(jobType = JobTypeEnum.REALTIME)
public class RealtimeJobDecoder extends AbstractJobDecoder {

  /**
   * Decodes sources.
   */
  private void decodeSources(JSONArray sources, RealtimeJobGraph.Builder builder) {
    List<AbstractSourceWrapper> sourceWrapperList = Lists.newArrayList();
    Set<String> nodeIds = new HashSet<>();
    for (int index = 0; index < sources.size(); index++) {
      JSONObject conf = (JSONObject) sources.get(index);
      String nodeId = conf.getString(JobConstant.KEY_NODE_ID);
      if (!nodeIds.contains(nodeId)) {
        AbstractSourceDecoder decoder = SourceDecoderFactory.getDecoder(conf.getString(JobConstant.KEY_SOURCE_TYPE));
        AbstractSourceWrapper inputWrapper = (AbstractSourceWrapper) decoder.decode(conf);
        sourceWrapperList.add(inputWrapper);

        nodeIds.add(nodeId);
      }
    }
    builder.setSources(sourceWrapperList);
  }

  /**
   * Decodes process nodes.
   */
  private void decodeProcessors(JSONArray processors, RealtimeJobGraph.Builder builder) {
    if (processors == null || processors.isEmpty()) {
      return;
    }
    List<AbstractProcessorWrapper> processorWrapperList = Lists.newArrayList();
    for (int index = 0; index < processors.size(); index++) {
      JSONObject conf = (JSONObject) processors.get(index);

      AbstractProcessorDecoder decoder = ProcessorDecoderFactory.getDecoder(conf.getString(JobConstant.KEY_PROCESSOR_TYPE));

      AbstractProcessorWrapper wrapper = (AbstractProcessorWrapper) decoder.decode(conf);
      processorWrapperList.add(wrapper);
    }

    // Sorts processor nodes
    Map<String, AbstractProcessorWrapper> processorWrapperMap = processorWrapperList.stream()
            .collect(Collectors.toMap(AbstractProcessorWrapper::getNodeId, Function.identity()));

    Set<String> priorNodeIdSet = processorWrapperList.stream()
            .map(AbstractProcessorWrapper::getPriorNodeId)
            .collect(Collectors.toSet());

    List<AbstractProcessorWrapper> maxSequenceProcessorNodeLst = processorWrapperList.stream()
            .filter(processWrapper -> !priorNodeIdSet.contains(processWrapper.getNodeId()))
            .collect(Collectors.toList());

    for (AbstractProcessorWrapper maxSequenceProcessWrapper : maxSequenceProcessorNodeLst) {
      resetProcessorWrapperSequence(maxSequenceProcessWrapper, processorWrapperMap);
    }

    // Caches parallelism for sql processors
    List<AbstractProcessorWrapper> sqlProcessorWrapperLst = processorWrapperList.stream()
            .filter(processWrapper -> processWrapper.getProcessorType()
                    .equals(ProcessTypeEnum.SQL.name())).collect(Collectors.toList());

    if (!sqlProcessorWrapperLst.isEmpty()) {
      IotWorkRuntimeBuildContext.setSqlParallelism(sqlProcessorWrapperLst.stream()
              .max(Comparator.comparing(sqlProcessWrapper -> sqlProcessWrapper
                      .getParallelism())).get().getParallelism());
    }

    builder.setProcessors(processorWrapperMap.values().stream()
            .sorted(Comparator.comparing(processWrapper -> processWrapper.getSequence()))
            .collect(Collectors.toList()));
  }

  private void resetProcessorWrapperSequence(
      AbstractProcessorWrapper processWrapper,
      Map<String, AbstractProcessorWrapper> processorWrapperMap) {
    AbstractProcessorWrapper priorProcessorWrapper = processorWrapperMap
        .get(processWrapper.getPriorNodeId());
    if (priorProcessorWrapper != null) {
      priorProcessorWrapper.setSequence(Math.min(processWrapper.getSequence() - 1,
          priorProcessorWrapper.getSequence()));
      resetProcessorWrapperSequence(priorProcessorWrapper, processorWrapperMap);
    }
  }

  /**
   * Decodes sinks.
   */
  private void decodeSinks(JSONArray sinks, RealtimeJobGraph.Builder builder) {
    List<AbstractSinkWrapper> sinkWrapperList = Lists.newArrayList();
    for (int index = 0; index < sinks.size(); index++) {
      JSONObject conf = (JSONObject) sinks.get(index);
      AbstractSinkDecoder decoder = SinkDecoderFactory.getDecoder(conf.getString(JobConstant.KEY_SINK_TYPE));
      AbstractSinkWrapper wrapper = (AbstractSinkWrapper) decoder.decode(conf);

      sinkWrapperList.add(wrapper);
    }
    builder.setSinks(sinkWrapperList);
  }


  @Override
  public IGraph<IWrapper> decode(JSONObject conf) {
    JSONArray sources = conf.getJSONArray(JobConstant.KEY_SOURCES);
    for (Object nodeConf : sources) {
      IotWorkRuntimeDecodeContext.addNodeConf(((JSONObject) nodeConf).getString(JobConstant.KEY_NODE_ID), (JSONObject) nodeConf);
    }

    JSONArray processors = conf.getJSONArray(JobConstant.KEY_PROCESSORS);
    if (processors != null && !processors.isEmpty()) {
      for (Object nodeConf : processors) {
        IotWorkRuntimeDecodeContext.addNodeConf(((JSONObject) nodeConf).getString(JobConstant.KEY_NODE_ID), (JSONObject) nodeConf);
      }
    }
    JSONArray sinks = conf.getJSONArray(JobConstant.KEY_SINKS);
    for (Object nodeConf : sinks) {
      IotWorkRuntimeDecodeContext.addNodeConf(((JSONObject) nodeConf).getString(JobConstant.KEY_NODE_ID), (JSONObject) nodeConf);
    }
    RealtimeJobGraph.Builder<IWrapper> builder = new RealtimeJobGraph.Builder<>();
    // 根据source配置找到对应的 Source Decoder，创建对应的 source算子 wrapper对象
    decodeSources(sources, builder);
    // 根据process配置找到对应的 Processor Decoder，创建对应的 process算子 wrapper对象
    decodeProcessors(processors, builder);
    // 根据sink配置找到对应的 Sink Decoder，创建对应的 sink算子 wrapper对象
    decodeSinks(sinks, builder);
    return builder.build();
  }
}
