/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.template.impl;

import static com.rootcloud.analysis.core.enums.DataTemplateEnum.HIERARCHY;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import com.rootcloud.analysis.template.IDescriptor;

import com.rootcloud.analysis.template.ServiceTemplate;
import com.rootcloud.analysis.template.domain.DataMap;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Stack;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@ServiceTemplate(template = HIERARCHY)
public class HierarchyDescriptor implements IDescriptor {

  private static Logger logger = LoggerFactory.getLogger(HierarchyDescriptor.class);
  private static final String TAG = "tag";
  private static final String ORG_ID = "org_id";
  private static final String SUB_ORG = "sub_org";
  private static final String TYPE = "type";
  private static final String DEVICE = "device";
  private static final String DEVICE_ID = "device_id";

  private Map<Object, Map<String, Object>> result;

  @Override
  public Map<Object, Map<String, Object>> parse(String content, String keyAttr) {
    Map<Object, Map<String, Object>> contentMap = new HashMap<>();
    JSONObject confObj = JSON.parseObject(content);

    Stack<DataMap> stack = new Stack<>();
    recursive(confObj, stack, contentMap);
    result = contentMap;

    return result;
  }

  private Map<String, Object> getContentMap(String key,
      Map<Object, Map<String, Object>> contentMap) {
    if (!contentMap.containsKey(key)) {
      contentMap.put(key, new HashMap<>());
    }

    return contentMap.get(key);
  }


  private void recursive(JSONObject conf,
      Stack<DataMap> stack,
      Map<Object, Map<String, Object>> contentMap) {
    if (!conf.containsKey(SUB_ORG)) {
      logger.info("conf:{} end", conf);
      return;
    }

    stack.push(DataMap.valueOf(conf.getString(TAG),
        conf.getString(ORG_ID)));

    JSONArray array = conf.getJSONArray(SUB_ORG);
    for (int index = 0; index < array.size(); index++) {
      JSONObject subConf = array.getJSONObject(index);

      if (subConf.getString(TYPE).equals(DEVICE)) {
        Iterator iterator = stack.iterator();
        while (iterator.hasNext()) {
          DataMap dataMap = (DataMap) iterator.next();
          getContentMap(subConf.getString(DEVICE_ID), contentMap)
              .put(dataMap.getTag(), dataMap.getValue());
        }
      } else {
        stack.push(DataMap.valueOf(subConf.getString(TAG),
            subConf.getString(ORG_ID)));
        recursive(subConf, stack, contentMap);
      }
    }
    stack.pop();

  }
}
