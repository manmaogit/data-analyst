/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.wrapper.function.jdbc;

import com.rootcloud.analysis.wrapper.jdbc.JdbcConnWrapper;
import com.rootcloud.analysis.wrapper.jdbc.JdbcDynamicConnWrapper;
import org.apache.flink.streaming.api.functions.source.SourceFunction;

public abstract class JdbcDynamicRichSourceFunction<T> extends JdbcDynamicRichFunction implements SourceFunction<T> {

  private static final long serialVersionUID = 1L;

  private volatile boolean isRunning = true;

  /**
   * 构造方法.
   */
  public JdbcDynamicRichSourceFunction(JdbcDynamicConnWrapper jdbcDynamicConnWrapper) {
    super(jdbcDynamicConnWrapper);
  }

  /**
   * 获取运行状态.
   */
  public boolean isRunning() {
    return isRunning;
  }

  @Override
  public void close() throws Exception {
    isRunning = false;
    super.close();
  }

}
