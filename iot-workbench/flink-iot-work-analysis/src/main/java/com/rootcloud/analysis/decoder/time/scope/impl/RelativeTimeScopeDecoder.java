/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.decoder.time.scope.impl;

import com.alibaba.fastjson.JSONObject;
import com.rootcloud.analysis.common.util.DateUtil;
import com.rootcloud.analysis.context.IotWorkRuntimeDecodeContext;
import com.rootcloud.analysis.converter.time.relative.RelativeTimeConverterFactory;
import com.rootcloud.analysis.core.constants.JobConstant;
import com.rootcloud.analysis.core.enums.TimeScopeTypeEnum;
import com.rootcloud.analysis.core.enums.TimeZoneTypeEnum;
import com.rootcloud.analysis.decoder.time.scope.AbstractTimeScopeDecoder;
import com.rootcloud.analysis.decoder.time.scope.TimeScopeDecoder;
import com.rootcloud.analysis.wrapper.time.scope.AbstractTimeScopeWrapper;
import com.rootcloud.analysis.wrapper.time.scope.RelativeTimeScopeWrapper;
import java.time.OffsetDateTime;
import org.apache.flink.util.Preconditions;

@TimeScopeDecoder(timeScopeTypeEnum = TimeScopeTypeEnum.RELATIVE_TIME)
public class RelativeTimeScopeDecoder extends AbstractTimeScopeDecoder {

  @Override
  public AbstractTimeScopeWrapper decode(JSONObject conf) {
    RelativeTimeScopeWrapper timeScopeWrapper = new RelativeTimeScopeWrapper();
    String systemDatetime = IotWorkRuntimeDecodeContext.getSystemDatetime();
    Preconditions.checkNotNull(systemDatetime, "systemDatetime is null.");
    String regx = "^-\\d+[hdwm]$";
    String startTime = conf.getString(JobConstant.KEY_START_TIME).toLowerCase();
    Preconditions.checkArgument(startTime.matches(regx), "Invalid start time.");
    int startTimeOffset = Integer.parseInt(startTime.substring(0, startTime.length() - 1));
    timeScopeWrapper.setStartEpochSecond(RelativeTimeConverterFactory
        .getConverter(startTime.substring(startTime.length() - 1))
        .convert2EpochSecond(systemDatetime, OffsetDateTime.now().getOffset(), startTimeOffset));

    String endTime = conf.getString(JobConstant.KEY_END_TIME).toLowerCase();
    Preconditions.checkArgument(endTime.matches(regx), "Invalid end time.");
    int endTimeOffset = Integer.parseInt(endTime.substring(0, endTime.length() - 1));
    timeScopeWrapper.setEndEpochSecond(RelativeTimeConverterFactory
        .getConverter(endTime.substring(endTime.length() - 1))
        .convert2EpochSecond(systemDatetime, OffsetDateTime.now().getOffset(), endTimeOffset));

    timeScopeWrapper.setStartTime(
        DateUtil.formatTime(timeScopeWrapper.getStartEpochSecond() * 1000,
            TimeZoneTypeEnum.valueOf(IotWorkRuntimeDecodeContext.getTimezone()).getZoneId(),
            "yyyy-MM-dd HH:mm:ss"));
    timeScopeWrapper.setEndTime(
        DateUtil.formatTime(timeScopeWrapper.getEndEpochSecond() * 1000,
            TimeZoneTypeEnum.valueOf(IotWorkRuntimeDecodeContext.getTimezone()).getZoneId(),
            "yyyy-MM-dd HH:mm:ss"));
    return timeScopeWrapper;
  }
}
