/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.common.factory;

import java.time.Duration;
import java.util.Properties;
import java.util.concurrent.CopyOnWriteArraySet;
import org.apache.commons.lang3.StringUtils;
import org.apache.flink.streaming.connectors.kafka.internals.FlinkKafkaInternalProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.ByteArraySerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class KafkaLogMiddlewareResourceFactory extends LogMiddlewareResourceFactory {

  private static Logger log = LoggerFactory.getLogger(KafkaLogMiddlewareResourceFactory.class);

  private static CopyOnWriteArraySet<String> senderSet = new CopyOnWriteArraySet<>();

  private static FlinkKafkaInternalProducer flinkKafkaInternalProducer = null;

  /**
   * 获取一个新的producer.
   */
  public static FlinkKafkaInternalProducer create(String kafkaServer) {
    return open(kafkaServer);
  }

  /**
   * 注册sender.
   */
  private static void registerSender(String sender) {
    if (senderSet != null) {
      senderSet.add(sender);
      log.info("register sender {} to senderSet and  senderSet is {}", sender, senderSet);
    }
  }

  /**
   * 销毁sender.
   */
  public static void withdrawSender(String sender) {
    if (senderSet != null) {
      senderSet.remove(sender);
      log.info("withdraw sender {} from senderSet and  senderSet is {}", sender, senderSet.toString());
    }
  }

  /**
   * 获取producer.
   */
  public static synchronized FlinkKafkaInternalProducer getProducer(String kafkaServer, String sender) {
    if (senderSet != null) {
      registerSender(sender);
      if (flinkKafkaInternalProducer == null && senderSet != null) {
        flinkKafkaInternalProducer = open(kafkaServer);
      }
    }
    return flinkKafkaInternalProducer;
  }

  /**
   * 返回一个新的producer.
   */
  private static FlinkKafkaInternalProducer open(String kafkaServer) {
    if (StringUtils.isBlank(kafkaServer)) {
      return null;
    }
    FlinkKafkaInternalProducer flinkKafkaInternalProducer = null;
    try {
      Properties p = new Properties();
      p.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG,
          kafkaServer);//kafka地址，多个地址用逗号分割
      p.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, ByteArraySerializer.class);
      p.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, ByteArraySerializer.class);
      p.put(ProducerConfig.ACKS_CONFIG, "1");
      p.put(ProducerConfig.REQUEST_TIMEOUT_MS_CONFIG, 1000);
      p.put(ProducerConfig.CONNECTIONS_MAX_IDLE_MS_CONFIG, 2 * 60 * 1000);
      flinkKafkaInternalProducer = new FlinkKafkaInternalProducer<>(p);
      log.info("The status of KafkaLogMiddlewareResource is running");
    } catch (Exception e) {
      log.error("create kafka producer fail.  msg:{}", e.toString());
      return null;
    }
    return flinkKafkaInternalProducer;
  }

  /**
   * 关闭kafka.
   */
  public static void close(String sender) {
    withdrawSender(sender);
    if (senderSet != null && senderSet.isEmpty() && flinkKafkaInternalProducer != null) {
      log.info("closing KafkaLogMiddlewareResourceFactory kafkaProducer ");
      flinkKafkaInternalProducer.close(Duration.ofSeconds(3));
      flinkKafkaInternalProducer = null;
      senderSet = null;
    }

  }

}
