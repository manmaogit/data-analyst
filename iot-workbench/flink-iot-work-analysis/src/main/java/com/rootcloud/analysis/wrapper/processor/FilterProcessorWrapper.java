/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.wrapper.processor;

import com.rootcloud.analysis.common.context.IotWorkRuntimeContext;
import com.rootcloud.analysis.common.entity.RcUnit;
import com.rootcloud.analysis.wrapper.WrapperBuilder;
import com.rootcloud.analysis.wrapper.function.filter.CompositeFilterWrapper;
import lombok.Getter;
import lombok.ToString;
import org.apache.flink.streaming.api.datastream.DataStream;

@Getter
@ToString(callSuper = true)
public class FilterProcessorWrapper extends AbstractProcessorWrapper {

  private final CompositeFilterWrapper filter;

  private FilterProcessorWrapper(Builder builder) {
    super(builder.jobId, builder.jobName, builder.tenantId, builder.nodeId,
        builder.parallelism, builder.nodeName, builder.priorNodeId, builder.processType,
        builder.logType);
    this.filter = builder.filter;
  }

  @ToString
  public static class Builder extends WrapperBuilder {

    private String nodeId;
    private String nodeName;
    private Integer parallelism;
    private String priorNodeId;
    private String processType;
    private CompositeFilterWrapper filter;

    public Builder setParallelism(Integer parallelism) {
      this.parallelism = parallelism;
      return this;
    }

    public Builder setPriorNodeId(String priorNodeId) {
      this.priorNodeId = priorNodeId;
      return this;
    }

    public Builder setProcessType(String processType) {
      this.processType = processType;
      return this;
    }

    public Builder setFilter(CompositeFilterWrapper filter) {
      this.filter = filter;
      return this;
    }

    public Builder setNodeName(String nodeName) {
      this.nodeName = nodeName;
      return this;
    }

    public FilterProcessorWrapper build() {
      return new FilterProcessorWrapper(this);
    }
  }

  @Override
  public void registerProcess(IotWorkRuntimeContext context) throws Exception {
    DataStream<RcUnit> priorDataStream = context.getObjectFromContainer(getPriorNodeId());
    DataStream<RcUnit> filteredStream = priorDataStream.filter(filter).uid(getNodeId())
        .setParallelism(getParallelism()).name(getNodeName());
    context.putObjectToContainer(getNodeId(), filteredStream);
  }
}
