/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.context;

import com.google.common.collect.Maps;
import com.rootcloud.analysis.common.context.IotWorkRuntimeContext;
import com.rootcloud.analysis.entity.RcTimeAttr;
import java.util.Map;
import org.apache.flink.table.api.Table;

public class IotWorkRuntimeBuildContext extends IotWorkRuntimeContext {

  private static RcTimeAttr timeAttr;
  private static String timeCharacteristic;
  private static Integer sqlParallelism;

  private static Map<String, String> temporaryViewNameMap = Maps.newHashMap();
  private static Map<String, Table> tableObjectMap = Maps.newHashMap();

  public static RcTimeAttr getTimeAttr() {
    return timeAttr;
  }

  public static void setTimeAttr(RcTimeAttr timeAttr) {
    IotWorkRuntimeBuildContext.timeAttr = timeAttr;
  }

  public static String getTimeCharacteristic() {
    return timeCharacteristic;
  }

  public static void setTimeCharacteristic(String timeCharacteristic) {
    IotWorkRuntimeBuildContext.timeCharacteristic = timeCharacteristic;
  }

  public static Integer getSqlParallelism() {
    return sqlParallelism;
  }

  public static void setSqlParallelism(Integer sqlParallelism) {
    IotWorkRuntimeBuildContext.sqlParallelism = sqlParallelism;
  }

  public static void putTemporaryViewName(String nodeId, String temporaryViewName) {
    temporaryViewNameMap.put(nodeId, temporaryViewName);
  }

  public static String getTemporaryViewName(String nodeId) {
    return temporaryViewNameMap.containsKey(nodeId) ? temporaryViewNameMap.get(nodeId) : nodeId;
  }

  public static void putTableObject(String nodeId, Table tableObject) {
    tableObjectMap.put(nodeId, tableObject);
  }

  public static Table getTableObject(String nodeId) {
    return tableObjectMap.get(nodeId);
  }
}
