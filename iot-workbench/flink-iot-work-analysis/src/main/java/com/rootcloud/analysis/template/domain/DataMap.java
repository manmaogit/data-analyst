/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.template.domain;

import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class DataMap implements Serializable {

  private static final long serialVersionUID = -125134852455157961L;
  private String tag;
  private String value;

  /**
   *valueOf.
   */
  public static DataMap valueOf(String tag, String value) {
    DataMap dataMap = new DataMap();
    dataMap.tag = tag;
    dataMap.value = value;
    return dataMap;
  }

}