/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.wrapper.source;

import static com.rootcloud.analysis.core.constants.JobConstant.INFLUXDB_TIME_COLUMN_NAME;
import static com.rootcloud.analysis.core.constants.SchemaConstant.ANCESTORS;
import static com.rootcloud.analysis.core.constants.SchemaConstant.DEVICE_ID;
import static com.rootcloud.analysis.core.constants.SchemaConstant.DEVICE_TYPE_ID;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.rootcloud.analysis.common.entity.DeviceMapping;
import com.rootcloud.analysis.common.entity.RcUnit;
import com.rootcloud.analysis.common.util.DateUtil;
import com.rootcloud.analysis.core.constants.SchemaConstant;
import com.rootcloud.analysis.core.enums.HistorianDataTypeEnum;
import com.rootcloud.analysis.core.enums.TimeZoneTypeEnum;
import com.rootcloud.analysis.entity.InfluxDbQueryResult;
import com.rootcloud.analysis.wrapper.IDataSourceWrapper;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Getter
public class InfluxDbDeviceConnBaseWrapper implements IDataSourceWrapper {

  private static final Logger logger = LoggerFactory.getLogger(InfluxDbDeviceConnBaseWrapper.class);

  private final String url;
  private final String username;
  private final String password;
  private final String database;
  private final String tenantId;
  /**
   * 超时时长（单位：毫秒）.
   */
  private final int timeOutMilliSeconds;
  private String influxdbTimePattern = "yyyy-MM-dd'T'HH:mm:ss";
  private final String timezone;
  private final JSONArray schema;

  // 设备模型ID、抽象模型ID、设备ID集合 的映射关系.
  private final List<DeviceMapping> deviceMappings;

  private final String queryFormat = "%s/query?db=%s&q=%s";

  /**
   * InfluxDbConnBaseWrapper.
   */
  public InfluxDbDeviceConnBaseWrapper(String url, String username,
                                       String password, String database,
                                       String tenantId, int timeOutSeconds,
                                       String timezone, JSONArray schema,
                                       List<DeviceMapping> deviceMappings) {
    this.url = url;
    this.username = username;
    this.password = password;
    this.database = database;
    this.tenantId = tenantId;
    this.timeOutMilliSeconds = timeOutSeconds * 1000;
    this.timezone = timezone;
    influxdbTimePattern =
        influxdbTimePattern + TimeZoneTypeEnum.valueOf(timezone).getTimeZoneValue().substring(3);
    this.schema = schema;
    this.deviceMappings = deviceMappings;
  }

  /**
   * 建立连接.
   */
  protected CloseableHttpResponse invokeHttpGet(CloseableHttpClient httpClient, String url,
                                                String querySql) throws IOException {
    String completeUrl = String.format(queryFormat, url, database,
        URLEncoder.encode(querySql, "utf-8"));
    logger.info("InfluxDb - query completeUrl = {}", completeUrl);
    HttpGet httpGet = new HttpGet(completeUrl);
    // 设置超时时长.
    RequestConfig requestConfig = RequestConfig.custom()
        .setConnectTimeout(timeOutMilliSeconds)
        .setConnectionRequestTimeout(timeOutMilliSeconds)
        .setSocketTimeout(timeOutMilliSeconds)
        .build();
    httpGet.setConfig(requestConfig);
    return httpClient.execute(httpGet);
  }

  /**
   * 查询InfluxDB数据.
   */
  protected List<InfluxDbQueryResult.Series> getInfluxDbData(CloseableHttpClient httpClient,
                                                             String querySql)
      throws IOException {
    CloseableHttpResponse response = null;
    try {
      logger.info("InfluxDb-querySql={}", querySql);
      response = invokeHttpGet(httpClient, url, querySql);
      if (response != null && response.getStatusLine().getStatusCode() == 200) {
        HttpEntity resEntity = response.getEntity();
        String resBody = EntityUtils.toString(resEntity, "UTF-8");
        List<InfluxDbQueryResult.Series> seriesList = new LinkedList<>();
        JSONObject jsonObject = JSON.parseObject(resBody);
        JSONArray results = jsonObject.getJSONArray("results");
        for (Object obj : results) {
          JSONObject result = (JSONObject) obj;
          JSONArray seriesJsonArray = result.getJSONArray("series");
          if (seriesJsonArray != null) {
            for (Object obj2 : seriesJsonArray) {
              JSONObject seriesJsonObj = (JSONObject) obj2;
              InfluxDbQueryResult.Series series = JSON.parseObject(
                  seriesJsonObj.toJSONString(), InfluxDbQueryResult.Series.class);
              seriesList.add(series);
            }
          }
        }
        return seriesList;
      } else {
        throw new RuntimeException("getInfluxDbData error! url = " + url
            + ", querySql = " + querySql);
      }
    } catch (Exception e) {
      throw e;
    } finally {
      if (response != null) {
        response.close();
      }
    }
  }

  /**
   * 针对InfluxDB进行额外的规则检查.
   */
  protected void checkColumnData(String columnKey, Object columnValue, Map data) {
    if (DEVICE_TYPE_ID.equals(columnKey)
        || ANCESTORS.equals(columnKey)) {
      return;
    }
    // ECA InfluxDB会对字段KEY额外进行类型的拼接.
    if (columnKey != null && columnKey.contains("$")) {
      String influxDataType = columnKey.substring(columnKey.indexOf("$") + 1);
      for (Object schemaInfo : schema) {
        JSONObject schemaObject = (JSONObject) schemaInfo;
        String originalColumn = schemaObject.getString(SchemaConstant.ATTR_ORIGINAL_NAME);
        String tempColumnKey = columnKey.substring(0, columnKey.indexOf("$"));
        String dataType = schemaObject.getString(SchemaConstant.ATTR_OLD_DATA_TYPE);
        if (tempColumnKey.startsWith("__location__.")
            || tempColumnKey.startsWith("__online__.")) {
          tempColumnKey = tempColumnKey.replace("__location__.", "")
                                          .replace("__online__.", "");
          dataType = schemaObject.getString(SchemaConstant.ATTR_DATA_TYPE);
        }
        if ((tempColumnKey.equals(originalColumn)
              && HistorianDataTypeEnum.isTypeEquals(influxDataType, dataType)
            ) || (originalColumn.contains(SchemaConstant.DOT)
                && originalColumn.startsWith(tempColumnKey))) {
          columnKey = tempColumnKey;
          break;
        }
      }
    }
    // InfluxDB的时间字段KEY是time，且为零时区格式.
    if (INFLUXDB_TIME_COLUMN_NAME.equals(columnKey)) {
      columnKey = SchemaConstant.TIMESTAMP;
      try {
        columnValue = DateUtil.convertTimeToZoneDate(
            (String) columnValue,
            "yyyy-MM-dd HH:mm:ss.SSS",
            TimeZoneTypeEnum.valueOf(timezone).getZoneId());
      } catch (Exception e) {
        columnKey = null;
        logger.error("InfluxDbConnWrapper-checkColumnData-columnValue={}, timezone={}, "
                + "Exception:{}",
            columnValue, timezone, e);
      }
    }
    if (columnKey != null && !columnKey.equals("")) {
      data.put(columnKey, columnValue);
    }
  }

  /**
   * 补全内置字段的值.
   */
  protected void fillDefaultFieldValue(RcUnit result, DeviceMapping deviceMapping,
                                     String deviceId) {
    // 抽象模型ID.
    String abstractDeviceTypeId = deviceMapping.getAbstractDeviceTypeId();
    if (StringUtils.isNotBlank(abstractDeviceTypeId)) {
      result.setAttr(ANCESTORS, abstractDeviceTypeId);
    }

    for (Object schemaInfo : getSchema()) {
      JSONObject schemaObject = (JSONObject) schemaInfo;
      String originalColumn = schemaObject.getString(SchemaConstant.ATTR_ORIGINAL_NAME);
      String targetName = schemaObject.getString(SchemaConstant.ATTR_NAME);
      switch (originalColumn) {
        // 模型ID.
        case DEVICE_TYPE_ID:
          result.setAttr(targetName, deviceMapping.getDeviceTypeId());
          break;
        // 设备ID.
        case DEVICE_ID:
          if (StringUtils.isNotBlank(deviceId)) {
            result.setAttr(targetName, deviceId);
          }
          break;
        default:
          break;
      }
    }
  }

  @Override
  public Set<RcUnit> getData() throws Exception {
    return null;
  }

  /**
   * 销毁连接.
   */
  @Override
  public void close() {
    logger.info("InfluxDbConnBaseWrapper closed!");
  }
}
