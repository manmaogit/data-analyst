/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.wrapper.sink;

import com.alibaba.fastjson.JSONObject;
import com.rootcloud.analysis.common.util.MysqlDataTypeConverter;
import com.rootcloud.analysis.core.constants.CommonConstant;
import com.rootcloud.analysis.core.constants.SchemaConstant;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString(callSuper = true)
public class MysqlSink03Wrapper extends JdbcSink03Wrapper {

  @Override
  public String buildDdl() {
    // e.g.,
    // CREATE TABLE MyUserTable (
    //  id BIGINT,
    //  name STRING,
    //  age INT,
    //  status BOOLEAN,
    //  PRIMARY KEY (id) NOT ENFORCED
    //) WITH (
    //   'connector' = 'jdbc',
    //   'url' = 'jdbc:mysql://localhost:3306/mydatabase',
    //   'table-name' = 'users'
    //);
    StringBuilder ddlBuilder = new StringBuilder();
    ddlBuilder.append("CREATE TABLE ");
    ddlBuilder.append(getTempTableName());
    ddlBuilder.append(" ( ");
    getSchema().forEach(jsonObj -> {
      ddlBuilder.append("`");
      ddlBuilder.append(((JSONObject) jsonObj).getString(SchemaConstant.ATTR_NAME));
      ddlBuilder.append("`");
      ddlBuilder.append(" ");
      ddlBuilder.append(getFlinkSqlType(
          ((JSONObject) jsonObj).getString(SchemaConstant.ATTR_ORIGINAL_TYPE)));
      ddlBuilder.append(CommonConstant.COMMA);
    });
    ddlBuilder.append("PRIMARY KEY (");
    String[] primaryKeyCols = getPrimaryKey().split(CommonConstant.COMMA);
    for (int i = 0; i < primaryKeyCols.length; i++) {
      if (i > 0) {
        ddlBuilder.append(CommonConstant.COMMA);
      }
      ddlBuilder.append("`");
      ddlBuilder.append(primaryKeyCols[i]);
      ddlBuilder.append("`");
    }
    ddlBuilder.append(") NOT ENFORCED");
    ddlBuilder.append(") WITH ('connector' = 'jdbc',");
    ddlBuilder.append("'url' = '");
    ddlBuilder.append(getUri());
    ddlBuilder.append("',");
    ddlBuilder.append("'username'  = '");
    ddlBuilder.append(getUsername());
    ddlBuilder.append("',");
    ddlBuilder.append("'password'  = '");
    ddlBuilder.append(getPassword());
    ddlBuilder.append("',");
    ddlBuilder.append("'table-name' = '");
    ddlBuilder.append(getTable());
    ddlBuilder.append("'");
    ddlBuilder.append(" )");
    return ddlBuilder.toString();
  }

  @Override
  protected String getFlinkSqlType(String originDbType) {
    return MysqlDataTypeConverter
        .convert2FlinkSqlType(originDbType);
  }

}

