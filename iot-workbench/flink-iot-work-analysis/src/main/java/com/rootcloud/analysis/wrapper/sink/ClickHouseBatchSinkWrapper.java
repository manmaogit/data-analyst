/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.wrapper.sink;

import static com.rootcloud.analysis.core.enums.InsertTypeEnum.INSERT;

import com.rootcloud.analysis.common.context.IotWorkRuntimeContext;
import com.rootcloud.analysis.common.entity.RcUnit;
import com.rootcloud.analysis.context.IotWorkRuntimeDecodeContext;
import com.rootcloud.analysis.core.constants.CommonConstant;
import com.rootcloud.analysis.core.constants.GrayLogConstant;
import com.rootcloud.analysis.core.constants.JobConstant;
import com.rootcloud.analysis.core.constants.ShortMessageConstant;
import com.rootcloud.analysis.core.enums.GrayLogResultEnum;
import com.rootcloud.analysis.core.enums.GraylogLogTypeEnum;
import com.rootcloud.analysis.core.graylog.LogVo;
import com.rootcloud.analysis.exception.SinkWriteDataException;
import com.rootcloud.analysis.log.loggingservice.LoggingServiceUtil;
import com.rootcloud.analysis.wrapper.function.jdbc.JdbcRichSinkFunction;
import com.rootcloud.analysis.wrapper.jdbc.ClickHouseBatchConnWrapper;
import java.sql.PreparedStatement;
import java.util.List;
import java.util.Map;
import lombok.Getter;
import lombok.ToString;
import org.apache.commons.compress.utils.Lists;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.metrics.Counter;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Getter
@ToString(callSuper = true)
public class ClickHouseBatchSinkWrapper extends JdbcBatchSinkWrapper {

  private static final Logger logger = LoggerFactory.getLogger(ClickHouseBatchSinkWrapper.class);

  private String formatField(String field) {
    return String.format("`%s`", field);
  }

  private String buildInsertSql() {
    String table = String.format("`%s`", getTable());

    StringBuilder sqlBuilder = new StringBuilder();
    sqlBuilder.append("INSERT INTO ").append(table).append(" ( ");

    StringBuilder insertValuesBuilder = new StringBuilder();
    for (int i = 0; i < getColumns().size(); i++) {
      String columnName = getColumns().get(i);
      if (i > 0) {
        sqlBuilder.append(CommonConstant.COMMA);
        insertValuesBuilder.append(CommonConstant.COMMA);
      }
      sqlBuilder.append(formatField(columnName));
      insertValuesBuilder.append(" ? ");
    }
    sqlBuilder.append(") VALUES( ").append(insertValuesBuilder).append(")");
    return sqlBuilder.toString();
  }

  @Override
  public void registerOutput(IotWorkRuntimeContext context) {
    DataStream<RcUnit> priorDataStream = getPriorDataStream(context);
    priorDataStream
        .addSink(
            new ClickHouseBatchRichSinkFunction(
                getJobId(),
                getTenantId(),
                getUserId(),
                getUri(),
                getUsername(),
                getPassword(),
                getTable(),
                buildInsertSql(),
                getColumns(),
                getAttributeMapper(),
                IotWorkRuntimeDecodeContext.getJobType(),
                getPriorTableColIndex(),
                getInsertKey(),
                getInsertType(),
                getBatchSize(),
                getUpsertKeyBuilder(),
                getJobName(),
                getNodeName(),
                getKafkaLoggingServiceServers(),
                getKafkaLoggingServiceTopics()))
        .uid(getNodeId())
        .setParallelism(getParallelism())
        .name(getNodeName());
  }

  static class ClickHouseBatchRichSinkFunction extends JdbcRichSinkFunction<RcUnit> {

    private final String jobId;

    private final String jobName;

    private final String nodeName;

    private final String tenantId;

    private final String userId;

    private final String uri;

    private final String table;

    private final String insertSql;

    private final List<String> columns;

    private final Map<String, String> attributeMapper;

    private final GraylogLogTypeEnum logType;

    private final Map<String, Integer> priorTableColIndex;

    private final List<String> insertKey;

    private final String insertType;

    private final int batchSize;

    private final List<RcUnit> records = Lists.newArrayList();

    private final UpsertKeyBuilder upsertKeyBuilder;
    private final String kafkaLoggingServiceServers;
    private final String kafkaLoggingServiceTopics;
    private Counter counterRecordsOut;

    ClickHouseBatchRichSinkFunction(
        String jobId,
        String tenantId,
        String userId,
        String uri,
        String username,
        String password,
        String table,
        String insertSql,
        List<String> columns,
        Map<String, String> attributeMapper,
        GraylogLogTypeEnum logType,
        Map<String, Integer> priorTableColIndex,
        List<String> insertKey,
        String insertType,
        int batchSize,
        UpsertKeyBuilder upsertKeyBuilder,
        String jobName,
        String nodeName,
        String kafkaLoggingServiceServers,
        String kafkaLoggingServiceTopics) {
      super(
          new ClickHouseBatchConnWrapper(
              "com.clickhouse.jdbc.ClickHouseDriver",
              uri,
              username,
              password,
              "select 1",
              table,
              null,
              0,
              null,
              null,
              null));
      this.jobId = jobId;
      this.tenantId = tenantId;
      this.uri = uri;
      this.userId = userId;
      this.table = table;
      this.insertSql = insertSql;
      this.columns = columns;
      this.attributeMapper = attributeMapper;
      this.logType = logType;
      this.priorTableColIndex = priorTableColIndex;
      this.insertKey = insertKey;
      this.insertType = insertType;
      this.batchSize = batchSize;
      this.upsertKeyBuilder = upsertKeyBuilder;
      this.jobName = jobName;
      this.nodeName = nodeName;
      this.kafkaLoggingServiceServers = kafkaLoggingServiceServers;
      this.kafkaLoggingServiceTopics = kafkaLoggingServiceTopics;
    }

    @Override
    public void open(Configuration parameters) throws Exception {
      super.open(parameters);
      counterRecordsOut =
          getRuntimeContext()
              .getMetricGroup()
              .counter(JobConstant.FLINK_METRIC_NUM_RECORDS_OUT_OVERWRITE);
      openProducer(kafkaLoggingServiceServers);
    }

    @Override
    public synchronized void invoke(RcUnit record, Context context) throws Exception {
      logger.debug("ClickHouseBatchSink process element: {}", record);
      if (INSERT.name().equals(insertType)) {
        records.add(record);
        counterRecordsOut.inc();
        if (records.size() >= batchSize) {
          flush();
        }
      } else {
        throw new SinkWriteDataException(
            "Clickhouse " + insertType + " is not support(not implement)");
      }
    }

    private void flush() {
      if (records.size() == 0) {
        return;
      }
      try {
        try (PreparedStatement preparedStatement =
            this.jdbcConnWrapper.getConnectionWrapper().prepareStatement(insertSql)) {
          for (RcUnit record : records) {
            for (int i = 0; i < columns.size(); i++) {
              Object val =
                  attributeMapper.containsKey(columns.get(i))
                      ? record.getAttr(attributeMapper.get(columns.get(i)))
                      : null;
              preparedStatement.setObject(i + 1, val);
            }
            preparedStatement.addBatch();
          }
          preparedStatement.executeBatch();
          logger.debug("successfully execute ClickHouse batch insert sql: {}", preparedStatement);
        }
      } catch (Exception e) {
        logger.error(
            "Exception occurred when try to batch insert/update to {}, exception: {}",
            table,
            e.getMessage(),
            e);
        LoggingServiceUtil.alarm(
            LogVo.builder()
                .k8sServiceName(GrayLogConstant.K8S_SERVICE_NAME)
                .module(GrayLogConstant.MODULE_NAME)
                .userName(tenantId)
                .userId(userId)
                .tenantId(tenantId)
                .operateObjectName(jobName)
                .operateObject(jobId)
                .shortMessage(
                    nodeName
                        + "ClickHouse data table "
                        + table
                        + " Update failed："
                        + e
                        + "，URI："
                        + uri
                        + "，Table："
                        + table)
                .operation(ShortMessageConstant.INSERT_CLICKHOUSE_DATA)
                .requestId(String.valueOf(System.currentTimeMillis()))
                .result(GrayLogResultEnum.FAIL.getValue())
                .logType(logType)
                .kafkaLoggingServiceTopic(kafkaLoggingServiceTopics)
                .kafkaLoggingServiceServers(kafkaLoggingServiceServers)
                .build(),
            flinkKafkaInternalProducer);
      } finally {
        records.clear();
      }
    }

    @Override
    public void close() throws Exception {

      LoggingServiceUtil.info(
          LogVo.builder()
              .k8sServiceName(GrayLogConstant.K8S_SERVICE_NAME)
              .module(GrayLogConstant.MODULE_NAME)
              .userName(tenantId)
              .userId(userId)
              .tenantId(tenantId)
              .operateObjectName(jobName)
              .operateObject(jobId)
              .shortMessage(
                  nodeName
                      + "ClickHouse data source disconnected successfully，"
                      + "URI："
                      + uri
                      + "，Table："
                      + table)
              .operation(ShortMessageConstant.CLOSE_CLICKHOUSE)
              .requestId(String.valueOf(System.currentTimeMillis()))
              .result(GrayLogResultEnum.SUCCESS.getValue())
              .logType(logType)
              .kafkaLoggingServiceTopic(kafkaLoggingServiceTopics)
              .kafkaLoggingServiceServers(kafkaLoggingServiceServers)
              .build(),
          flinkKafkaInternalProducer);
      flush();
      super.close();
      logger.info("ClickHouseBatchSink is closed.");
    }
  }
}
