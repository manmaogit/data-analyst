/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2020 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.service;

import static com.rootcloud.analysis.constants.ExceptionConstant.RUN_FLINK_JAR_EX_CODE;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.rootcloud.analysis.constants.ExceptionConstant;
import com.rootcloud.analysis.core.constants.FlinkConstant;
import com.rootcloud.analysis.core.flink.FlinkJobDetail;
import com.rootcloud.analysis.exception.JobException;
import com.rootcloud.analysis.exception.basic.BadRequestException;
import com.rootcloud.analysis.vo.flink.FlinkJarFileListVo;

import java.io.File;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.annotation.Resource;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

@Component
@Slf4j
public class FlinkRestComponent {

  /*@Autowired
  private TenantResourceService tenantResourceService;*/

    @Resource(name = "flinkRestTemplate")
    private RestTemplate restTemplate;


    @Value("${app.version}")
    private String version;

    public static String JAR_FILE_PATH = "/opt/";

    public static final String JAR_FILE_NAME = "rootcloud-analysis-flink-runner-%s.jar";

    public static final String JAR_ENTRY_CLASS = "com.rootcloud.analysis.FlinkMain";


    /**
     * 根据flink的jid查询Job明细. 注： 调用者需处理返回为空的情况
     *
     * @param flinkJobId flink jid
     */
    public FlinkJobDetail queryFlinkJobDetailByFlinkJobId(List<String> flinkUrls, String flinkJobId) throws Exception {
        int restCount = flinkUrls.size();
        for (String flinkUrl : flinkUrls) {
            String url = flinkUrl + FlinkConstant.JOB_DETAIL_URL;
            try {
                ResponseEntity<String> respEntity = restTemplate.getForEntity(url, String.class, flinkJobId);
                if (respEntity.getStatusCode() == HttpStatus.OK) {
                    return JSON.parseObject(respEntity.getBody(), FlinkJobDetail.class);
                } else {
                    log.info("Flinkurl call failed, error code: {}, exception returned:{}", respEntity.getStatusCode(), respEntity.getBody());
                    restCount--;
                }

            } catch (HttpClientErrorException e) {
                if (e.getStatusCode() == HttpStatus.NOT_FOUND) {
                    log.info("jid does not exist in flink:{},url:{}", flinkJobId, flinkUrl);
                    return null;
                }
            } catch (Exception e) {
                log.info("Flinkurl call failed, calling other nodes", e);
                restCount--;
            }
        }
        if (restCount == 0) {
            throw new Exception("Failed to call all Flink nodes to query");
        }
        return null;
    }

    /**
     * 获取flink-runner的jarId. 逻辑：获取不到则上传，上传后再次获取.
     *
     * @param flinkUrl flink地址
     * @return 返回jarId
     * @throws Exception 异常抛出.
     */
    public String findJarId(String flinkUrl) throws Exception {

        String jarId = getFlinkJarId(flinkUrl);
        if (jarId == null) {
            // 上传jar包文件
            boolean uploadTag = uploadJar(flinkUrl);
            if (!uploadTag) {
                log.error("Flink failed to upload jar package");
                throw new JobException(ExceptionConstant.ERROR_EXCEPTION_CODE, "Flink failed to upload jar package");
            }
        }
        jarId = getFlinkJarId(flinkUrl);
        return jarId;
    }

    /**
     * 启动一个flink job.
     *
     * @param isStream      是否是流计算任务
     * @param tenantId      租户id
     * @param programArgs   入参
     * @param savePointPath savePoint地址
     * @return flink jid
     * @throws Exception 异常
     */
    public String runFlinkJar(Boolean isStream, String tenantId, String programArgs, String savePointPath) throws Exception {
        List<String> flinkUrls = getFlinkUrls(isStream, tenantId);
        if (CollectionUtils.isEmpty(flinkUrls)) {
            throw new JobException(ExceptionConstant.RUN_FLINK_JAR_EX_CODE, "Failed to start flow calculation task Flink");
        }
        String exceptionMsg = null;
        for (String flinkUrl : flinkUrls) {
            try {
                String jarId = findJarId(flinkUrl);
                // 运行 flink jar包
                return executeRunFlinkJar(jarId, programArgs, savePointPath, flinkUrl);
            } catch (HttpClientErrorException e) {
                log.error("Failed to start job at flinkurl:{}", flinkUrl, e);
                exceptionMsg = e.getResponseBodyAsString();
            } catch (Exception e) {
                log.error("Failed to start job at flinkurl:{}", flinkUrl, e);
                exceptionMsg = e.getMessage();
            }
        }
        log.error("Failed to call all Flink addresses to start, tenant ID:{}", tenantId);
        throw new JobException(RUN_FLINK_JAR_EX_CODE, exceptionMsg);
    }

    /**
     * 启动一个flink job.
     *
     * @param isStream      是否是流计算任务
     * @param tenantId      租户id
     * @param programArgs   入参
     * @param savePointPath savePoint地址
     * @param jar           提交作业用的jar
     * @return flink jid
     * @throws Exception 异常
     */
    public String runFlinkJar(Boolean isStream, String tenantId, String programArgs, String savePointPath, File jar, String jobId) throws Exception {
        List<String> flinkUrls = getFlinkUrls(isStream, tenantId);
        if (CollectionUtils.isEmpty(flinkUrls)) {
            throw new JobException(ExceptionConstant.RUN_FLINK_JAR_EX_CODE, "Failed to start flow calculation task Flink");
        }
        String exceptionMsg = null;
        for (String flinkUrl : flinkUrls) {
            try {
                List<String> flinkJarIdContains = getFlinkJarIdContains(flinkUrl, jobId);
                for (String jarId : flinkJarIdContains) {
                    deleteJar(flinkUrl, jarId);
                }

                boolean uploadTag = uploadJar(flinkUrl, jar);
                if (!uploadTag) {
                    log.error("Flink failed to upload jar package");
                    throw new JobException(ExceptionConstant.ERROR_EXCEPTION_CODE, "Flink failed to upload jar package");
                }

                String jarId = getFlinkJarId(flinkUrl, jar.getName());
                return executeRunFlinkJar(jarId, programArgs, savePointPath, flinkUrl);
            } catch (HttpClientErrorException e) {
                log.error("Failed to start job at flinkurl:{}", flinkUrl, e);
                exceptionMsg = e.getResponseBodyAsString();
            } catch (Exception e) {
                log.error("Failed to start job at flinkurl:{}", flinkUrl, e);
                exceptionMsg = e.getMessage();
            }
        }
        log.error("Failed to call all Flink addresses to start, tenant ID:{}", tenantId);
        throw new JobException(RUN_FLINK_JAR_EX_CODE, exceptionMsg);
    }

    private void deleteJar(String flinkUrl, String jarId) {
        String url = flinkUrl + FlinkConstant.FLINK_JARS_URL + "/" + jarId;
        restTemplate.delete(url);
    }

    /**
     * 获取flink-runner的jarId.
     *
     * @param flinkUrl flink地址
     * @return 返回jarId
     * @throws Exception 异常抛出.
     */
    private String getFlinkJarId(String flinkUrl) throws Exception {
        // rootcloud-analysis-flink-runner-%s.jar 包名
        String jarFileName = String.format(JAR_FILE_NAME, version);
        String url = flinkUrl + FlinkConstant.FLINK_JARS_URL;
        ResponseEntity<String> jarListResp = restTemplate.getForEntity(url, String.class);
        if (jarListResp.getStatusCode() != HttpStatus.OK) {
            throw new BadRequestException(jarListResp.getStatusCode().value(), jarListResp.getStatusCode().name());
        }
        JSONObject responseJsonObject = JSON.parseObject(jarListResp.getBody());
        JSONArray files = responseJsonObject.getJSONArray("files");
        if (CollectionUtils.isEmpty(files)) {
            return null;
        }
        for (Object file : files) {
            FlinkJarFileListVo flinkJarFileListVo = JSONObject.parseObject(file.toString(), FlinkJarFileListVo.class);
            if (flinkJarFileListVo.getName().equals(jarFileName)) {
                return flinkJarFileListVo.getId();
            }
        }
        return null;
    }

    /**
     * 获取flink集群中jarFileName的jarId.
     *
     * @param flinkUrl    flink地址
     * @param jarFileName jar name
     * @return 返回jarId列表
     * @throws Exception 异常抛出.
     */
    private String getFlinkJarId(String flinkUrl, String jarFileName) throws Exception {
        String url = flinkUrl + FlinkConstant.FLINK_JARS_URL;
        ResponseEntity<String> jarListResp = restTemplate.getForEntity(url, String.class);
        if (jarListResp.getStatusCode() != HttpStatus.OK) {
            throw new BadRequestException(jarListResp.getStatusCode().value(), jarListResp.getStatusCode().name());
        }
        JSONObject responseJsonObject = JSON.parseObject(jarListResp.getBody());
        JSONArray files = responseJsonObject.getJSONArray("files");

        for (Object file : files) {
            FlinkJarFileListVo flinkJarFileListVo = JSONObject.parseObject(file.toString(), FlinkJarFileListVo.class);
            if (flinkJarFileListVo.getName().equals(jarFileName)) {
                return flinkJarFileListVo.getId();
            }
        }
        return null;
    }

    /**
     * 获取flink集群中名字包含jarFileName的jarId.
     *
     * @param flinkUrl    flink地址
     * @param jarFileName jar name
     * @return 返回jarId列表
     * @throws Exception 异常抛出.
     */
    private List<String> getFlinkJarIdContains(String flinkUrl, String jarFileName) throws Exception {
        List<String> jarIdList = new LinkedList<>();
        String url = flinkUrl + FlinkConstant.FLINK_JARS_URL;
        ResponseEntity<String> jarListResp = restTemplate.getForEntity(url, String.class);
        if (jarListResp.getStatusCode() != HttpStatus.OK) {
            throw new BadRequestException(jarListResp.getStatusCode().value(), jarListResp.getStatusCode().name());
        }
        JSONObject responseJsonObject = JSON.parseObject(jarListResp.getBody());
        JSONArray files = responseJsonObject.getJSONArray("files");

        for (Object file : files) {
            FlinkJarFileListVo flinkJarFileListVo = JSONObject.parseObject(file.toString(), FlinkJarFileListVo.class);
            if (flinkJarFileListVo.getName().contains(jarFileName)) {
                jarIdList.add(flinkJarFileListVo.getId());
            }
        }
        return jarIdList;
    }

    /**
     * 调用 flink rest api启动一个任务. 批注：非静态方法，通过注入方式调用
     *
     * @param jarId         flink已上传的一个jarId
     * @param programArgs   启动类参数
     * @param savePointPath savePoint地址
     * @param flinkUrl      flink地址
     * @return 返回flink jid
     * @throws Exception 异常返回
     */
    private String executeRunFlinkJar(String jarId, String programArgs, String savePointPath, String flinkUrl) throws Exception {

        String url = flinkUrl + FlinkConstant.FLINK_RUN_JAR_URL;
        JSONObject request = new JSONObject();
        request.put("entryClass", JAR_ENTRY_CLASS);
        request.put("programArgs", programArgs);
        request.put("parallelism", null);
        request.put("savepointPath", savePointPath);
        request.put("allowNonRestoredState", null);
        ResponseEntity<String> runJarResp = restTemplate.postForEntity(url, request, String.class, jarId);
        if (runJarResp.getStatusCode() != HttpStatus.OK) {
            throw new BadRequestException(runJarResp.getStatusCode().value(), runJarResp.getStatusCode().name());
        }
        JSONObject runResult = JSON.parseObject(runJarResp.getBody());
        if (runResult.containsKey("errors")) {
            throw new JobException(RUN_FLINK_JAR_EX_CODE, runResult.getString("errors"));
        }
        if (runResult.containsKey("jobid")) {
            return runResult.getString("jobid");
        }
        throw new JobException(RUN_FLINK_JAR_EX_CODE, "Abnormal return value of start flow processing task");
    }

    /**
     * 上传flink-runner的jar包.
     *
     * @param flinkUrl flink地址
     * @return 返回上传结果
     * @throws Exception 异常抛出
     */
    private boolean uploadJar(String flinkUrl) throws Exception {
        String jarFileName = String.format(JAR_FILE_NAME, version);
        //设置请求头
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        //设置请求体，注意是LinkedMultiValueMap
        FileSystemResource fileSystemResource = new FileSystemResource(JAR_FILE_PATH + jarFileName);
        MultiValueMap<String, Object> form = new LinkedMultiValueMap<>();
        form.add("file", fileSystemResource);
        HttpEntity<MultiValueMap<String, Object>> files = new HttpEntity<>(form, headers);
        String url = flinkUrl + FlinkConstant.FLINK_UPLOAD_JAR_URL;
        ResponseEntity<String> uploadResp = restTemplate.postForEntity(url, files, String.class);
        if (uploadResp.getStatusCode() != HttpStatus.OK) {
            throw new BadRequestException(uploadResp.getStatusCode().value(), uploadResp.getStatusCode().name());
        }
        String uploadResultStr = uploadResp.getBody();
        JSONObject uploadResult = JSON.parseObject(uploadResultStr);
        String status = uploadResult.getString("status");
        return status.equals("success");
    }

    /**
     * 上传flink-runner的jar包.
     *
     * @param flinkUrl flink地址
     * @param jar      jar
     * @return 返回上传结果
     * @throws Exception 异常抛出
     */
    private boolean uploadJar(String flinkUrl, File jar) throws Exception {
        //设置请求头
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        //设置请求体，注意是LinkedMultiValueMap
        FileSystemResource fileSystemResource = new FileSystemResource(jar);
        MultiValueMap<String, Object> form = new LinkedMultiValueMap<>();
        form.add("file", fileSystemResource);
        HttpEntity<MultiValueMap<String, Object>> files = new HttpEntity<>(form, headers);
        String url = flinkUrl + FlinkConstant.FLINK_UPLOAD_JAR_URL;
        ResponseEntity<String> uploadResp = restTemplate.postForEntity(url, files, String.class);
        if (uploadResp.getStatusCode() != HttpStatus.OK) {
            throw new BadRequestException(uploadResp.getStatusCode().value(), uploadResp.getStatusCode().name());
        }
        String uploadResultStr = uploadResp.getBody();
        JSONObject uploadResult = JSON.parseObject(uploadResultStr);
        String status = uploadResult.getString("status");
        return status.equals("success");
    }

    /**
     * 所有flink地址都需要通过接口获取，获取flinkurl.
     */
    public List<String> getFlinkUrls(Boolean isStream, String tenantId) throws JobException {
        try {
            // return tenantResourceService.getFlinkUrlFromTenantResource(tenantId, isStream);
            // 根据租户获取Flink Url
            return null;
        } catch (Exception e) {
            throw new JobException(ExceptionConstant.GET_FLINK_URL_FAIL_EX_CODE, e.toString());
        }
    }


    /**
     * 根据flink jid cancel job.
     *
     * @param jid      jid
     * @param isStream 是否是流任务
     * @param tenantId 租户id
     * @throws JobException 异常
     */
    public void cancelFlinkJobByJid(String jid, Boolean isStream, String tenantId) throws JobException {
        List<String> flinkUrls = getFlinkUrls(isStream, tenantId);
        for (String flinkUrl : flinkUrls) {
            boolean cancelFlag = cancelFlinkJobByIdAssignFlinkUrl(flinkUrl, jid);
            if (cancelFlag) {
                return;
            }
        }
        //所有flinkurl都调用失败会触发此异常
        throw new JobException(ExceptionConstant.JOB_FLINK_CANCEL_EX_CODE, "Flinkjob stop failed");
    }

    boolean cancelFlinkJobByIdAssignFlinkUrl(String flinkUrl, String jid) {
        String url = flinkUrl + FlinkConstant.FLINK_CANCEL_JOB_URL;
        String restResult;
        try {
            restResult = restTemplate.patchForObject(url, null, String.class, jid);
            JSONObject jsonObject = JSON.parseObject(restResult);
            if (jsonObject.isEmpty()) {
                return true;
            }
        } catch (HttpClientErrorException e) {
            if (e.getStatusCode() == HttpStatus.NOT_FOUND) {
                return true;
            }
            log.error("Flinkurl:{} call failed, switch to the next node", flinkUrl, e);
        } catch (Exception e) {
            log.error("Flinkurl:{} call failed, switch to the next node", flinkUrl, e);
        }
        return false;
    }

    /**
     * 根据job名称cancel 运行中的job.
     *
     * @param jobName  job名称
     * @param isStream 是否是流任务
     * @param tenantId 租户id
     * @throws JobException 异常
     */
    public void cancelFlinkJobByJobName(String jobName, Boolean isStream, String tenantId) throws JobException {
        List<String> flinkUrls = getFlinkUrls(isStream, tenantId);
        for (String flinkUrl : flinkUrls) {
            try {
                Map<String, Set<String>> runningJobMap = getRunningJobMap(flinkUrl);
                if (runningJobMap.containsKey(jobName)) {
                    Set<String> jidList = runningJobMap.get(jobName);
                    for (String jid : jidList) {
                        cancelFlinkJobByIdAssignFlinkUrl(flinkUrl, jid);
                    }
                }
                return;
            } catch (Exception e) {
                log.error("The flyjob stops abnormally. Try the next node, flyurl: {}", flinkUrl, e);
                continue;
            }
        }
    }

    private Map<String, Set<String>> getRunningJobMap(String flinkUrl) {
        String overviewUrl = flinkUrl + FlinkConstant.FLINK_JOB_OVERVIEW_URL;
        ResponseEntity<String> responseEntity;
        try {
            responseEntity = restTemplate.getForEntity(overviewUrl, String.class);
        } catch (Exception e) {
            log.error("Failed to get the flick job list. Switch to the next node,flinkUrl:{}", flinkUrl);
            throw e;
        }
        Map<String, Set<String>> jobName2JidSetMap = Maps.newHashMap();
        JSONObject resultObject = JSON.parseObject(responseEntity.getBody());
        JSONArray jobs = resultObject.getJSONArray("jobs");
        if (CollectionUtils.isEmpty(jobs)) {
            return jobName2JidSetMap;
        }
        //flink的job列表是运行中的在上面，当检测到没有运行中的就结束遍历
        for (Object job : jobs) {
            JSONObject jobObject = JSONObject.parseObject(JSON.toJSONString(job));
            if (!jobObject.getString("state").equals("RUNNING")) {
                break;
            }
            String name = jobObject.getString("name");
            String jid = jobObject.getString("jid");
            if (jobName2JidSetMap.containsKey(name)) {
                Set<String> jidSet = jobName2JidSetMap.get(name);
                jidSet.add(jid);
                jobName2JidSetMap.put(name, jidSet);
            } else {
                Set<String> jidSet = Sets.newHashSet(jid);
                jobName2JidSetMap.put(name, jidSet);
            }
        }
        for (int i = 0; i < jobs.size(); i++) {

        }
        return jobName2JidSetMap;
    }


}
