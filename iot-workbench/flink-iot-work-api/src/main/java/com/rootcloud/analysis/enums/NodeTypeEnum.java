/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.enums;

public enum NodeTypeEnum {

  // 实时-输入-Kafka实时数据
  REAL_TIME_INPUT,
  // 实时-输入-多组织实时数据
  REAL_TIME_MULTI_ORGANIZATION_INPUT,
  // 实时-输入-物模型
  INNER_INPUT_NODE,
  // 实时-输入-多物模型
  INNER_STREAM_INPUT,

  // 实时-处理-物实例筛选
  REAL_TIME_FILTER,
  // 实时-处理-过滤
  FILTER,
  // 实时-处理-维表关联
  DIMENSION_JOIN,
  // 实时-处理-FlinkSQL
  SQL,
  // 实时-处理-去重
  REALTIME_DEDUP,
  // 实时-处理-资产筛选
  ASSET,
  // 实时-处理-IDF台账关联
  IDF_FILTER,
  //实时-处理-udf 表值聚合
  REALTIME_UDTAGG,

  // 实时-输出-MySQL
  MYSQL_OUTPUT_NODE,
  // 实时-输出-PG
  POSTGRESQL_OUTPUT_NODE,
  // 实时-输出-Kafka
  KAFKA_OUTPUT_NODE,
  // 实时-输出-Oracle
  ORACLE_OUTPUT_NODE,
  // 实时-输出-SQLSERVER
  SQLSERVER_OUTPUT_NODE,
  // 实时-输出-资产
  ASSET_KPI_OUTPUT_NODE,
  // 实时-输出-IDF
  IDF_REALTIME_OUTPUT_NODE,

  // 离线-输入-MySQL
  MYSQL_INPUT,
  // 离线-输入-PG
  POSTGRESQL_INPUT,
  //离线-输入-ORACLE
  ORACLE_02_INPUT,
  //离线-输入-CLICKHOUSE
  CLICKHOUSE_02_INPUT,
  // 离线-输入-Hive
  INNER_HIVE_INPUT,
  // 离线-输入-InfluxDB
  INFLUXDB_INPUT,
  // 离线-输入-IDF
  OFFLINE_IDF_INPUT,
  // 离线-输入-TdEngine
  TD_ENGINE_INPUT,
  //离线-输入-SqlServer
  SQLSERVER_INPUT,
  //离线-输入-clickhouse
  CLICKHOUSE_INPUT,


  // 离线-处理-过滤
  FILTER2,
  // 离线-处理-FlinkSQL
  SQL2,
  // 离线-处理-维表关联
  OFFLINE_JOIN,
  // 离线-处理-去重
  OFFLINE_DEDUP,
  //离线-处理-udf 表值聚合
  OFFLINE_UDTAGG,


  // 离线-输出-MySQL
  MYSQL_02_OUTPUT,
  // 离线-输出-PG
  POSTGRESQL_02_OUTPUT,
  // 离线-输出-SQLSERVER
  SQLSERVER_02_OUTPUT,
  //离线-输出-CLICKHOUSE
  CLICKHOUSE_02_OUTPUT,
  // 离线-输出-IDF
  IDF_OFFLINE_OUTPUT_NODE,

  // 暂无备注
  REST_API,
  ORGANIZATION,
  ORACLE_INPUT;

  /**
   * 判断指定节点类型是否输出节点.
   */
  public static boolean isOutputNode(NodeTypeEnum nodeType) {
    switch (nodeType) {
      case MYSQL_OUTPUT_NODE:
      case POSTGRESQL_OUTPUT_NODE:
      case KAFKA_OUTPUT_NODE:
      case ORACLE_OUTPUT_NODE:
      case SQLSERVER_OUTPUT_NODE:
      case ASSET_KPI_OUTPUT_NODE:
      case MYSQL_02_OUTPUT:
      case POSTGRESQL_02_OUTPUT:
      case SQLSERVER_02_OUTPUT:
      case CLICKHOUSE_02_OUTPUT:
      case IDF_REALTIME_OUTPUT_NODE:
      case IDF_OFFLINE_OUTPUT_NODE:
        return true;
      default:
        return false;
    }
  }
}
