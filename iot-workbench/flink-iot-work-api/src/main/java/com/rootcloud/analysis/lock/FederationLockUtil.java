/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.lock;

public class FederationLockUtil {
  /**
   * 【联邦架构】获取编排的锁.
   * @param tenantId 创建编排的租户
   * @param jobId 编排ID
   * @return
   */
  public static String buildLockKey(String tenantId, String jobId) {
    return tenantId + "-" + jobId + "-" + "distribution";
  }
}
