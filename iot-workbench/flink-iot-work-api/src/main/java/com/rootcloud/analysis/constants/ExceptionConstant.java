/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.constants;

public class ExceptionConstant {

  public static final int ERROR_EXCEPTION_CODE = 500;

  public static final int INTERNAL_SERVER_ERROR_EX_CODE = 51001;

  public static final int DOLP_SCHEDULER_EX_CODE = 51002;


  public static final int NOT_EXIST_EX_CODE = 44001;

  /**
   * 001-100 通用错误.
   */

  public static final int INVALID_TOKEN_EX_CODE = 41001;

  public static final int BAD_REQUEST_EX_CODE = 41002;

  public static final int INVALID_SQL_EX_CODE = 41003;

  public static final int INVALID_SELECT_SQL_MISS_LIMIT_EX_CODE = 41004;

  public static final int INVALID_SELECT_SQL_MISS_ROWNUM_EX_CODE = 41005;

  public static final int INVALID_MAX_ORDERNESS_EX_CODE = 41006;

  public static final int SQL_NOT_EXIST_TIMESTAMP_EX_CODE = 41007;

  public static final int INTERNAL_BAD_REQUEST_EX_CODE = 41008;

  public static final int INVALID_SELECT_SQL_EX_CODE = 41009;

  public static final int EXECUTE_SQL_EX_CODE = 41010;

  public static final int TOKEN_EXPIRED_EX_CODE = 41011;

  public static final int EXECUTE_SQL_DUPLICATION_NAME_EX_CODE = 41012;

  public static final int EXECUTE_SQL_UNIDENTIFICATION_EX_CODE = 41013;

  public static final int EXECUTE_SQL_NOT_RENAME_EX_CODE = 41014;

  public static final int EXECUTE_SQL_ANALYSIS_FAIL_EX_CODE = 41015;

  public static final int GET_FLINK_URL_FAIL_EX_CODE = 41016;

  public static final int COMBINATION_JOB_TEMPLATE_EX_CODE = 41017;

  // 非法的SQL语句：不支持GROUP BY子句
  public static final int INVALID_SQL_WITH_GROUP_BY = 41018;
  // 非法的SQL语句：不支持嵌套子查询和Join关联查询
  public static final int INVALID_SQL_WITH_SUB_SELECT = 41019;
  // 非法/暂不支持的SQL语法
  public static final int INVALID_SQL_GRAMMAR = 41020;
  // 无法获取源表字段
  public static final int GET_SQL_TABLE_SCHEMA = 41021;
  // 非法的SQL语句：where子句字段与源表字段不一致
  public static final int INVALID_SQL_TABLE_SCHEMA_FOR_WHERE = 41022;
  // 非法的SQL语句：order by子句字段源表字段不一致
  public static final int INVALID_SQL_TABLE_SCHEMA_FOR_ORDER_BY = 41023;
  // 非法的SQL语句：limit数量超过上限值
  public static final int INVALID_SQL_LIMIT_OVER_MAX = 41024;

  public static final int IAM_BAD_REQUEST_EX_CODE = 41025;

  public static final int IAM_AUTH_FAIL_EX_CODE = 41026;

  public static final int SET_TENANT_ID_FAIL_EX_CODE = 41027;

  public static final int GET_TENANT_ID_FAIL_EX_CODE = 41028;

  public static final int JOB_TEMPLATE_JOB_TYPE_MISMATCHING_EX_CODE = 41029;

  public static final int JOB_TEMPLATE_MISS_TEMPLATE_EX_CODE = 41030;

  public static final int JOB_TEMPLATE_MISS_PARAMS_EX_CODE = 41031;

  public static final int JOB_TEMPLATE_MISS_SCHEDULE_EX_CODE = 41032;

  public static final int JOB_TEMPLATE_MISS_PARAMS_KEY_DEFINE_EX_CODE = 41033;

  public static final int TENANT_RESOURCE_TENANT_ID_NULL_EX_CODE = 41034;

  public static final int TENANT_RESOURCE_NOT_EXIST_EX_CODE = 41035;

  public static final int TENANT_RESOURCE_GET_RESOURCE_FAIL_EX_CODE = 41036;

  public static final int TENANT_RESOURCE_GET_TENANT_TYPE_FAIL_EX_CODE = 41037;

  public static final int SET_DATA_TENANT_ID_FAIL_EX_CODE = 41038;

  public static final int GET_DATA_TENANT_ID_FAIL_EX_CODE = 41039;
  public static final int INVALID_SELECT_SQL_LENGTH_CODE = 41040;
  public static final int METHOD_ARGUMENT_NOT_VALID_EX_CODE = 41041;

  public static final int FOLDER_LIST_FAIL_EX_CODE = 41042;

  public static final int FOLDER_NAME_EXIST_EX_CODE = 41043;

  public static final int FOLDER_ID_INVALID_EX_CODE = 41044;

  public static final int FOLDER_EXIST_CHILDREN_EX_CODE = 41045;

  public static final int FOLDER_LEVEL_TOO_LARGE_EX_CODE = 41046;

  public static final int FOLDER_EXIST_JOB_EX_CODE = 41047;

  public static final int FOLDER_TYPE_INVALID_EX_CODE = 41048;

  public static final int DOLP_WORKFLOW_EX_CODE = 41049;

  public static final int INVALID_DOL_TENANT_RESOURCE_EX_CODE = 41050;

  public static final int INVALID_DATA_PERMISSION_EX_CODE = 41051;

  public static final int INVALID_JOB_DATA_PERMISSIONS_EX_CODE = 41052;

  public static final int FOLDER_EXIST_JOB_DATA_PERMISSION_EX_CODE = 41053;

  public static final int DATA_PERMISSION_REJECT_EX_CODE = 41054;

  public static final int INVALID_SELECT_SQL_MISS_TOP_EX_CODE = 41055;

  /**
   * 101-200 数据计算服务任务错误.
   */

  public static final int JOB_ALREAY_EXIST_EX_CODE = 41101;

  public static final int JOB_NOT_EXIST_EX_CODE = 41102;

  public static final int DELETE_RELEASE_JOB_EX_CODE = 41103;

  public static final int JOB_ALREAY_RELEASE_EX_CODE = 41104;

  public static final int JOB_NOT_RELEASE_EX_CODE = 41105;

  public static final int INVALID_JOB_STATE_EX_CODE = 41106;

  public static final int JOB_MAXIMUM_RELEASE_BOOK_EX_CODE = 41107;

  public static final int MODIFY_RELEASE_JOB_EX_CODE = 41108;

  public static final int JOB_STARTING_EX_CODE = 41109;

  public static final int JOB_NODE_TYPE_EX_CODE = 41110;

  public static final int JOB_NODE_SQL_EMPTY_EX_CODE = 41111;

  public static final int JOB_RUNNING_EX_CODE = 41112;

  public static final int JOB_STOP_EX_CODE = 41113;

  public static final int JOB_PARAMETER_TYPE_ERROR_EX_CODE = 41114;

  public static final int JOB_NOT_RUNNING_EX_CODE = 41115;

  public static final int QUERY_MODEL_PROPERTY_EX_CODE = 41116;

  public static final int PATH_NOT_FOUND_EX_CODE = 41117;

  public static final int INVALID_SCHEDULE_STATE_EX_CODE = 41118;

  public static final int INVALID_SCHEDULE_PARAMETERS_EX_CODE = 41119;

  public static final int SCHEDULE_NEVER_STARTED = 41120;

  public static final int SCHEDULE_INSTANCE_NOT_EXIST = 41121;

  public static final int SCHEDULE_IS_NOT_ONLINE = 41122;

  public static final int SCHEDULE_IS_ALREADY_EXISTED = 41123;

  public static final int INVALID_SCHEDULE_ID_EX_CODE = 41124;

  public static final int INVALID_INSTANCE_STATE_EX_CODE = 41125;

  public static final int INVALID_COMPLEMENT_PLAN_EX_CODE = 41126;

  public static final int SCHEDULE_COMPLEMENT_DAY_TOO_LARGE_CODE = 41127;

  public static final int RUN_FLINK_JAR_EX_CODE = 41128;

  public static final int GET_FLINK_JAR_ID_EX_CODE = 41129;

  public static final int INVALID_PLAN_DATE_EX_CODE = 41130;

  public static final int PARALLELISM_EXCEEDS_MAX_LIMIT_EX_CODE = 41131;

  public static final int INVALID_JOIN_SOURCE = 41132;

  public static final int SCHEDULE_DEPENDENT_CYCLE_EX_CODE = 41133;

  public static final int INVALID_JOB_TEMPLATE_PARAMETERS_EX_CODE = 41134;

  public static final int JOB_INSTANCE_NOT_EXISTED_EX_CODE = 41135;

  public static final int JOB_FLINK_CANCEL_EX_CODE = 41136;

  public static final int DEPENDENT_JOB_SCHEDULE_NOT_EXIST_CODE = 41137;

  public static final int DEPENDENT_JOB_NOT_RELEASE_EX_CODE = 41138;

  public static final int SCHEDULE_NOT_EXIST = 41139;

  public static final int INVALID_JOB_TEMPLATE_DETAIL_EX_CODE = 41140;

  public static final int NONSUPPORT_JOIN_NODE_FILTER_SQL_EX_CODE = 41141;

  public static final int PARAM_NOT_START_WITH_NODE_ID_EX_CODE = 41142;

  public static final int PARAM_NAME_NOT_EXIST_SCHEMA_EX_CODE = 41143;

  public static final int INVALID_NODE_ID_EX_CODE = 41144;

  public static final int INVALID_TENANT_TYPE_EX_CODE = 41145;

  public static final int DELETE_FOLDER_EX_CODE = 41146;

  public static final int INVALID_CRON_EX_CODE = 41147;

  public static final int INVALID_UPDATE_JOB_STATE_OR_DELETE_EX_CODE = 41148;

  public static final int INVALID_UPDATE_WORKFLOW_STATE_OR_DELETE_EX_CODE = 41149;

  public static final int JOB_FLINK_RUNNING_METRIC_DATA_NOT_FOUND_EX_CODE = 41150;

  public static final int INVALID_JOB_FOLDER_ID_EX_CODE = 41151;
  public static final int INVALID_BATCH_JOB_STATE_EX_CODE = 41152;

  public static final int JOB_NODE_METRIC_DATA_NOT_FOUND_EX_CODE = 41153;

  public static final int INVALID_JOB_STATE_TO_GET_METRIC_EX_CODE = 41154;

  public static final int JOB_METRIC_DATA_NOT_FOUND_EX_CODE = 41155;

  public static final int INVALID_WORKFLOW_RELEASE_REFERENCE_EX_CODE = 41156;

  public static final int DOLPHIN_LOG_QUERY_ERROR_EX_CODE = 41157;

  public static final int NOT_JAR_ERROR_EX_CODE = 41158;
  public static final int JAR_NOT_UPLOAD_EX_CODE = 41159;
  public static final int INVALID_JAR_EX_CODE = 41160;
  public static final int DELETE_UDF_IN_USE_EX_CODE = 41161;
  public static final int FILE_TOO_LARGE_EX_CODE = 41162;
  public static final int UDF_EXIST_EX_CODE = 41163;
  public static final int NO_RESULT_ANNOTATION_EX_CODE = 41164;
  public static final int CLASS_NOT_FOUND_EX_CODE = 41165;
  public static final int METHOD_NOT_FOUND_EX_CODE = 41166;
  public static final int INVALID_DOLPHIN_RES_ID_EX_CODE = 41167;
  public static final int INVALID_UDF_CLASS_EX_CODE = 41168;
  public static final int DOLPHIN_CREATE_RES_EX_CODE = 41169;
  public static final int UDF_NOT_EXIST_ERR_CODE = 41170;

  public static final int NO_INPUT_EX_CODE = 41171;
  public static final int NO_OUTPUT_EX_CODE = 41172;
  public static final int NO_GROUPING_EX_CODE = 41173;

  public static final int NO_DB_TYPE_EX_CODE = 41174;
  public static final int NO_DATASOURCE_EX_CODE = 41175;
  public static final int UNSUPPORTED_DATABASE_EX_CODE = 41176;

  /**
   * 201-300 数据计算服务任务图校验异常.
   */

  public static final int DAG_GRAPH_ISOLATED_NODE_EX_CODE = 41201;

  public static final int DAG_GRAPH_DEPTH_TOO_BIG_EX_CODE = 41202;

  public static final int DAG_GRAPH_TOO_MANY_NODES_EX_CODE = 41203;

  public static final int DAG_GRAPH_NODE_TYPE_IS_EMPTY_EX_CODE = 41204;

  public static final int DAG_EXIST_CLOSED_LOOP_EX_CODE = 41205;

  public static final int DAG_EXIST_INPUT_NODE_EX_CODE = 41206;

  public static final int DAG_EXIST_NULL_POINTER_EX_CODE = 41207;

  public static final int DAG_EXIST_NODEID_REPETITION_EX_CODE = 41208;

  public static final int NODE_FILTER_RULE_REPETITION_EX_CODE = 41209;

  public static final int DAG_MYSQL_OUTPUT_NODE_HAS_FOLLOW_UP_EX_CODE = 41210;

  public static final int INVALID_NODE_TYPE_EX_CODE = 41211;

  public static final int MYSQL_OUTPUT_NODE_MISSING_DB_KEYS_EX_CODE = 41212;

  public static final int INVALID_MYSQL_INSERT_TYPE_EX_CODE = 41213;

  public static final int MISSING_TIME_STAMP_EX_CODE = 41214;

  public static final int KAFKA_OUTPUT_NODE_ATTRNAME_DOES_NOT_MATCH = 41215;

  public static final int DAG_DB_TYPE_MISSMATCH = 41216;

  public static final int PRIMARY_KEY_DOES_NOT_EXIST_EX_CODE = 41228;

  public static final int FLINK_RESOUCE_NOT_ADEQUATE_EX_CODE = 41218;

  public static final int MYSQL_OUTPUT_NODE_ATTR_CONFLICT_EX_CODE = 41219;

  public static final int MISSING_DEVICE_ID_LIST_EX_CODE = 41220;

  public static final int UNSUPPORTED_JDBC_DB_TYPE_EX_CODE = 41221;

  public static final int JOIN_NODE_MISSING_ORIGINAL_NAME = 41222;


  public static final int STRONG_CORRELATION_CHECK_ERROR = 41223;

  public static final int DAG_GRAPH_WIDTH_TOO_BIG_EX_CODE = 41224;

  public static final int DAG_EXIST_LEAF_NODE_PROCESS_EX_CODE = 41225;

  public static final int DAG_EXIST_OUTPUT_NODE_EX_CODE = 41226;

  public static final int PRIMARY_KEY_IS_NOT_MAPPED = 41227;

  public static final int PRIVATE_KEY_DOES_NOT_EXIST = 41217;

  public static final int NODE_FILTER_RULE_UN_SUPPORT_DATA_TYPE_EX_CODE = 41232;

  public static final int INSERT_KEY_IS_NOT_MAPPED_EX_CODE = 41229;

  public static final int NOT_NULL_KEY_IS_NOT_MAPPED_EX_CODE = 41230;

  public static final int INVALID_INSERT_TYPE_EX_CODE = 41231;

  public static final int EMPTY_DUPLICATE_KEY_EX_CODE = 41232;
  public static final int EMPTY_SORT_BY_EX_CODE = 41233;
  public static final int EMPTY_KEEP_EX_CODE = 41234;


  public static final int MULTI_ORGANIZATION_MISSING_PROPERTIES_EX_CODE = 41235;
  public static final int INVALID_WORKFLOW_STATE_EX_CODE = 41236;

  public static final int INVALID_WORKFLOW_NODE_TYPE_EX_CODE = 41237;

  public static final int WORKFLOW_OFFLINE_JOB_NODE_NOT_EXIST_JOB_ID_EX_CODE = 41238;
  public static final int WORKFLOW_OFFLINE_JOB_RETRY_EX_CODE = 41247;
  public static final int WORKFLOW_OFFLINE_JOB_RETRY_INTERVAL_EX_CODE = 41248;
  public static final int WORKFLOW_NODE_PRIOR_NODE_SIZE_EX_CODE = 41239;

  public static final int WORKFLOW_DEPENDENT_NODE_NOT_EXIST_DEPENDENCY_EX_CODE = 41240;
  public static final int WORKFLOW_NODE_EX_CODE = 41241;

  public static final int KAFKA_EXEC_EX_CODE = 41242;

  public static final int WORKFLOW_INVALID_RELEASE_STATE_EX_CODE = 41243;

  public static final int INVALID_INSERT_KEY_EX_CODE = 41244;

  public static final int INVALID_OUTPUT_SCHEMA_INSERT_KEY_EX_CODE = 41245;

  public static final int INVALID_OUTPUT_SCHEMA_NOT_NULL_KEY_EX_CODE = 41246;


  /**
   * 301-400 内部数据源，外部数据源异常.
   */

  public static final int INVALID_DATABASE_PRIMARY_KEY_EX_CODE = 41301;

  public static final int INVALID_INNER_DATASOURCE_EX_CODE = 41302;

  public static final int EXTERNAL_DS_CONN_EX_CODE = 41303;

  public static final int EXTERNAL_DS_DATABASE_NOT_EXIST_EX_CODE = 41304;

  public static final int EXTERNAL_DS_TABLE_NOT_EXIST_EX_CODE = 41305;

  public static final int EXTERNAL_DS_QUERY_EX_CODE = 41306;

  public static final int INVALID_EXTERNAL_DS_TYPE_EX_CODE = 41307;

  public static final int EXTERNAL_DS_ALREAY_EXIST_EX_CODE = 41308;

  public static final int EXTERNAL_DS_QUERY_NUM_EX_CODE = 41309;

  public static final int EXTERNAL_DS_UNSUPPORTED_GET_SCHEMA_EX_CODE = 41310;

  public static final int EXTERNAL_DS_UNSUPPORTED_LIST_TABLE_EX_CODE = 41311;

  public static final int EXTERNAL_DS_UNSUPPORTED_SQL_QUERY_EX_CODE = 41312;

  public static final int EXTERNAL_DS_IOTWORKS_AUTH_EX_CODE = 41313;

  public static final int EXTERNAL_DS_NOT_EXIST_EX_CODE = 41314;

  public static final int INVALID_PROPERTY_NAME_EX_CODE = 41315;

  public static final int EXTERNAL_DS_DATA_TYPE_UNSUPPORTED_EX_CODE = 41316;

  public static final int FIELD_NOT_EXIST_EX_CODE = 41317;

  public static final int INVALID_INNER_DS_TYPE_EX_CODE = 41318;

  public static final int INNER_DS_DATABASE_NOT_EXIST_EX_CODE = 41319;

  public static final int INNER_DS_TABLE_NOT_EXIST_EX_CODE = 41320;

  public static final int INNER_DS_CONN_EX_CODE = 41321;

  public static final int EXTERNAL_DS_KAFKA_INVALID_ADDRESS_FORMAT_EX_CODE = 41322;

  /**
   * 401-500 模板异常.
   */

  public static final int JOB_TEMPLATE_DOES_NOT_EXIST_EX_CODE = 41401;

  public static final int JOB_TEMPLATE_ALREADY_EXIST_EX_CODE = 41402;

  public static final int JOB_TEMPLATE_NAME_EMPTY_EX_CODE = 41403;

  public static final int EDIT_KAFKA_ALERT_EXP = 41404;

  /**
   * 501-600 组件异常.
   */

  public static final int MONGODB_PRIVILEGE_TENANT_RESOURCE_QUERY_EX_CODE = 41501;

  public static final int GET_DISTRIBUTED_LOCK_FAIL_EX_CODE = 41502;

  public static final int JOB_DISTRIBUTED_IN_PROGRESS = 41503;

  public static final int JOB_DISTRIBUTED_INITIALIZING = 41504;

  public static final int JOB_DISTRIBUTED_STOPPING = 41505;

  public static final int REVERSE_PROXY_CALL_FAILURE = 40001;
  public static final int DATA_CENTER_EXEC_ERROR = 40002;
  public static final int DATA_CENTER_UNREACHABLE_ERROR = 40003;
  public static final int DATA_CENTER_SERVICE_UNAVAILABLE = 40004;

  public static final int WORKFLOW_NOT_EXIST_EX_CODE = 41601;
  public static final int WORKFLOW_NAME_EXIST_EX_CODE = 41602;
  public static final int WORKFLOW_DELETE_RELEASE_EX_CODE = 41603;
  public static final int WORKFLOW_UPDATE_RELEASE_EX_CODE = 41604;
  public static final int WORKFLOW_RELEASE_EX_CODE = 41605;
  public static final int WORKFLOW_DELETE_EX_CODE = 41606;
  public static final int WORKFLOW_VALIDATE_JOB_EX_CODE = 41607;
  public static final int WORKFLOW_JOB_NOT_EXIST_EX_CODE = 41608;
  public static final int WORKFLOW_NODE_NOT_EXIST_CONDITION_EX_CODE = 41609;
  public static final int WORKFLOW_NODE_NOT_EXIST_CONDITION_RESULT_EX_CODE = 41610;
  public static final int WORKFLOW_JOB_UN_RELEASE_EX_CODE = 41611;

  public static final int COLLECTION_NAME_EXIST_EX_CODE = FOLDER_NAME_EXIST_EX_CODE;
  public static final int COLLECTION_ID_INVALID_EX_CODE = FOLDER_ID_INVALID_EX_CODE;
  public static final int INVALID_PARENT_COLLECTION_ID = 41701;
}
