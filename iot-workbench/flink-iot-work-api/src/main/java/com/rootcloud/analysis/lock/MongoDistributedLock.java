/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2020 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.lock;

import com.rootcloud.analysis.core.constants.MongoConstants;
import com.rootcloud.analysis.core.lock.DistributedLock;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import lombok.Getter;
import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.mongodb.core.FindAndModifyOptions;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Component
public class MongoDistributedLock implements DistributedLock {
  private static Logger logger = LoggerFactory.getLogger(MongoDistributedLock.class);

  @Autowired
  private MongoTemplate mongoTemplate;


  @Value("${spring.application.lock-time}")
  private Long lockTime;

  /**
   * 返回指定key的数据.
   */
  private List<MongoLock> getByKey(String key) {
    Query query = new Query();
    query.addCriteria(Criteria.where("key").is(key));
    return mongoTemplate.find(query, MongoLock.class, MongoConstants.MONGO_LOCK_COLLECTION);
  }


  /**
   * 指定key自增increment(原子加),并设置过期时间.
   */
  private MongoLock incrByWithExpire(String key, double increment, long expire) {
    //筛选
    Query query = new Query();
    query.addCriteria(new Criteria("key").is(key));

    //更新
    Update update = new Update();
    update.inc("value", increment);
    update.set("expire", expire);
    //可选项
    FindAndModifyOptions options = FindAndModifyOptions.options();
    //没有则新增
    options.upsert(true);
    //返回更新后的值
    options.returnNew(true);
    MongoLock mongoLock = mongoTemplate.findAndModify(query, update, options,
        MongoLock.class, MongoConstants.MONGO_LOCK_COLLECTION);
    return mongoLock;
  }


  /**
   * 根据value删除过期的内容.
   */
  private void remove(String key) {
    Query query = new Query();
    query.addCriteria(Criteria.where("key").is(key));
    query.addCriteria(Criteria.where("expire").lt(System.currentTimeMillis()));
    mongoTemplate.remove(query, MongoConstants.MONGO_LOCK_COLLECTION);
  }

  /**
   * remove.
   */
  private void remove(Map<String, Object> condition) {
    Query query = new Query();
    Set<Map.Entry<String, Object>> set = condition.entrySet();
    int flag = 0;
    for (Map.Entry<String, Object> entry : set) {
      query.addCriteria(Criteria.where(entry.getKey()).is(entry.getValue()));
      flag = flag + 1;
    }
    if (flag == 0) {
      query = null;
    }
    mongoTemplate.remove(query, MongoConstants.MONGO_LOCK_COLLECTION);
  }

  @Override
  @Transactional(propagation = Propagation.NOT_SUPPORTED)
  public boolean tryLock(String key) throws Exception {
    int i = 1;
    boolean result = true;
    while (!lock(key)) {
      result = false;
      Thread.sleep(1000);
      i++;
      logger.info("The data calculation service task has been modified."
          + " It cannot be modified in {} seconds. ID:{}", lockTime / 1000, key);
      if (i >= lockTime / 1000) {
        break;
      }
    }
    return result;
  }

  /**
   * 获得锁的步骤：
   * 1、首先判断锁是否被其他请求获得；如果没被其他请求获得则往下进行；
   * 2、判断锁资源是否过期，如果过期则释放锁资源；
   * 3.1、尝试获得锁资源，如果value=1，那么获得锁资源正常;
   * （在当前请求已经获得锁的前提下，还可能有其他请求尝试去获得锁，此时会导致当前锁的过期时间被延长，
   * 由于延长时间在毫秒级，可以忽略。）
   * 3.2、value>1,则表示当前请求在尝试获取锁资源过程中，其他请求已经获取了锁资源，即当前请求没有获得锁；
   * ！！！注意，不需要锁资源时，及时释放锁资源！！！.
   */
  @Override
  @Transactional(propagation = Propagation.NOT_SUPPORTED)
  public boolean lock(String key) {
    List<MongoLock> mongoLocks = getByKey(key);
    //判断该锁是否被获得,锁已经被其他请求获得，直接返回
    if (mongoLocks.size() > 0 && mongoLocks.get(0).getExpire() >= System.currentTimeMillis()) {
      return false;
    }
    //释放过期的锁
    if (mongoLocks.size() > 0 && mongoLocks.get(0).getExpire() < System.currentTimeMillis()) {
      release(key);
    }
    //！！(在高并发前提下)在当前请求已经获得锁的前提下，还可能有其他请求尝试去获得锁，此时会导致当前锁的过期时间被延长，由于延长时间在毫秒级，可以忽略。
    MongoLock mongoLock = incrByWithExpire(key, 1, System.currentTimeMillis() + lockTime);
    //如果结果是1，代表当前请求获得锁
    if (mongoLock.getValue() == 1) {
      return true;
      //如果结果>1，表示当前请求在获取锁的过程中，锁已被其他请求获得。
    } else if (mongoLock.getValue() > 1) {
      return false;
    }
    return false;
  }

  @Override
  @Transactional(propagation = Propagation.NOT_SUPPORTED)
  public boolean release(String key) {
    boolean result = false;
    try {
      Map<String, Object> condition = new HashMap<>();
      condition.put("key", key);
      remove(condition);
      result = true;
      logger.debug("release lock successed!");
    } catch (Exception e) {
      logger.error("release lock error!", e);
    }
    return result;
  }

  @Getter
  @Setter
  public static class MongoLock {
    private String key;
    private double value;
    private long expire;
  }
}