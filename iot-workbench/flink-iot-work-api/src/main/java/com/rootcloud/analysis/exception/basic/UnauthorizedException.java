/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.exception.basic;

import com.rootcloud.analysis.core.exception.RcException;

public class UnauthorizedException extends RcException {

  public UnauthorizedException(int code, String message) {
    super(code, message);
  }
}