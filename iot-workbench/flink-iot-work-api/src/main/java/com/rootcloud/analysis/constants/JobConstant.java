/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.constants;

import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.temporal.ChronoField;

public class JobConstant {

  public static final String ID = "_id";

  public static final String PARAMS = "params";

  public static final String TEMPLATE = "template";

  public static final String JOB_TYPE = "job_type";

  public static final String JOB_STATE = "job_state";
  public static final String SCHEDULE_STATE = "schedule_state";

  public static final String RUNNING_STATE = "job_running_state";

  public static final String REALTIME = "REALTIME";

  public static final String OFFLINE = "OFFLINE";

  public static final String JOB_NAME = "job_name";

  public static final String JOB_ID = "job_id";

  public static final String NAME = "name";

  public static final String FOLDER_NAME = "name";

  public static final String FOLDER_ID = "folder_id";

  public static final String PARAM_FOLDER_ID = "params.folder_id";

  public static final String FOLDER_PARENT_ID = "parent_id";

  public static final String FOLDER_TYPE = "folder_type";

  public static final String FLINK_JID = "flink_jid";

  public static final String UPDATE_TIME = "update_time";

  public static final String JOB_START_TIMESTAMP = "job_start_timestamp";

  public static final String NO_RESPONSE = "NO_RESPONSE";

  public static final String STATE = "state";

  public static final String TOKEN = "token";

  public static final String DETECT_PERIOD = "detectPeriod";

  public static final String CUSTOM_CONSUMER_GROUP_ID = "customConsumerGroupId";

  public static final String KEY_CUSTOM_CONSUMER_GROUP_ID = "custom_consumer_group_id";

  public static final String RUNNING_STATE_UPDATE_TIME = "running_state_update_time";

  public static final String MAX_OUT_OF_ORDERNESS = "maxOutOfOrderness";

  public static final String ATTRIBUTE_MAPPER_LIST = "attributeMapperList";

  public static final String PERSISTENT_TOKEN = "persistentToken";

  public static final String KEY_KAFKA_DATASOURCE_TYPE = "kafkaDataSourceType";

  public static final String DIMENSION_DATASOURCE_ID = "dimensionDatasourceId";

  public static final String TENANT_GROUP_ID = "tenantGroupId";

  public static final String TENANT_ID = "tenant_id";

  public static final String SOURCE_TENANT_ID = "__tenantId__";

  public static final String SOURCE_MODEL_TYPE = "__modelType__";

  public static final String OUTPUT_TABLE = "outputTable";

  public static final String SOURCE_ABSTRACT_MODEL_TYPE = "__abstractModelType__";

  public static final String TENANT_NAME = "tenant_name";

  public static final String DB_SHARE = "db_share";

  public static final String DEVICE_ID = "__deviceId__";

  public static final String JSON_DEVICE_ID = "__deviceId__";

  public static final String ALL_DEVICE_ID = "__ALL__";

  public static final String NULL_DEVICE_ID = "__NULL__";

  public static final String DEVICE_TYPE_ID = "__deviceTypeId__";

  public static final String LOGICAL_INTERFACE_ID = "__logicalInterfaceId__";

  public static final String ABSTRACT_LOGICAL_INTERFACE_ID = "__ancestors__";

  public static final String LOGICAL_MODEL_TYPE_NAME = "__model_type__";

  public static final String JSON_LOGICAL_INTERFACE_ID = "__logicalInterfaceId__";

  public static final String ATTR_TIME_AGGREGATION = "timeAggregation";

  public static final String ATTR_TIME_AGGREGATION_FLAG = "timeAggregationFlag";

  public static final String ATTR_OUTPUT_SCHEMA = "outputSchema";

  public static final String ATTR_COORDINATE = "coordinate";

  public static final String ATTR_ATTR_SOURCE = "attrSource";

  public static final String ORIGINAL_NAME = "originalName";

  public static final String ORIGINAL_TYPE = "originalType";

  public static final String OLD_DATA_TYPE = "oldDataType";

  public static final String ORIGINAL_DISPLAY_NAME = "originalDisplayName";

  public static final String IOTWORKS = "iotworks-";

  public static final String EXCEPTION_PREFIX = "IOT-";

  public static final String PRIOR_NODE_ID = "priorNodeId";

  public static final String NODE_TYPE = "nodeType";

  public static final String ATTR_SQL = "sql";

  public static final String ATTR_META_TYPE = "metaType";

  public static final int DATASOURCE_QUERY_MAX_NUM = 5000;

  public static final String DATASOURCE_KEY_JDBCURL = "jdbcUrl";
  public static final String DATASOURCE_KEY_USERNAME = "username";
  public static final String DATASOURCE_KEY_PASSWORD = "password";
  public static final String DATASOURCE_KEY_KAFKA_SERVERS = "kafkaBroadcastAddress";

  public static final String DATASOURCE_KEY_TYPE = "type";
  public static final String DATASOURCE_KEY_TYPE_MYSQL = "MYSQL";
  public static final String DATASOURCE_KEY_TYPE_PG = "PG";
  public static final String DATASOURCE_KEY_TYPE_ORACLE = "ORACLE";
  public static final String DATASOURCE_KEY_TYPE_KAFKA = "KAFKA";
  public static final String DATASOURCE_KEY_TYPE_DOLPHIN_SCHEDULER = "DOLPHIN";

  public static final String DELETE_FLAG = "delete_flag";

  public static final String DATA_CENTER_ID = "data_center_id";
  public static final String ORG_ID = "org_id";
  public static final String DISTRIBUTED_JOB_ID_DELIMITER = "_";

  public static final DateTimeFormatter DOLPHIN_RESPONSE_DATE_FORMATTER =
      new DateTimeFormatterBuilder()
          .appendPattern("yyyy-MM-dd'T'HH:mm:ss")
          .appendFraction(ChronoField.MILLI_OF_SECOND, 2, 3, true)
          .appendOffset("+HHMM", "Z")
          .toFormatter();

  // Dolphin的分支和依赖节点默认超时失败分钟数
  public static final int DOLPHIN_DEPENDENT_CONDITIONS_DEFAULT_TIMEOUT_MINUTES = 1;

}
