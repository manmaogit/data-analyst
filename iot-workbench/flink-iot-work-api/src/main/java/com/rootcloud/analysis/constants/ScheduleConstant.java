/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.constants;


public class ScheduleConstant {

  public static final String ID = "id";
  public static final String JOB_ID = "job_id";
  public static final String JOB_NAME = "job_name";
  public static final String PROCESS_DEFINITION_ID = "process_definition_id";
  public static final String SCHEDULE_ID = "schedule_id";
  public static final String TENANT_ID = "tenant_id";
  public static final String TENANT_NAME = "tenant_name";
  public static final String START_TIME = "start_time";
  public static final String END_TIME = "end_time";
  public static final String TIME_ZONE = "time_zone";
  public static final String CRON_TAB = "cron_tab";
  public static final String RELEASE_STATE = "release_state";
  public static final String RUNNING_STATE = "running_state";
  public static final String FAILURE_STRATEGY = "failure_strategy";
  public static final String RELEASE_TIME = "release_time";
  public static final String ONLINE_TIME = "online_time";

  public static final String SCHEDULE_OBJECT = "schedule_object";//调度对象类型。主要为WORKFLOW，兼容批编排时为OFFLINE
}
