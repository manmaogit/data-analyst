/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2020 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.core.graylog;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.rootcloud.analysis.core.constants.ExceptionConstant;
import com.rootcloud.analysis.core.enums.LogLogLevelEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class AlarmGrayLogUtils {

  private static Logger log = LoggerFactory.getLogger(AlarmGrayLogUtils.class);
  private static String PLATFORM_VALUE = "platform-v4";

  /**
   * 打印能够触发告警推送的日志.
   */
  public static void alarm(GrayLogVo grayLogVo) {
    try {
      JSONObject shortMessageJson = new JSONObject();
      shortMessageJson.put("tenantId", grayLogVo.getTenantId());
      shortMessageJson.put("jobName", grayLogVo.getOperateObjectName());
      shortMessageJson.put("jobId", grayLogVo.getOperateObject());
      shortMessageJson.put("message", grayLogVo.getShortMessage());

      AlarmGrayLogVo alarmLog = new AlarmGrayLogVo();
      alarmLog.setK8sServiceName(grayLogVo.getK8sServiceName());
      alarmLog.setModule(grayLogVo.getModule());
      alarmLog.setShortMessage(JSON.toJSONString(shortMessageJson));
      alarmLog.setPlatform(PLATFORM_VALUE);
      alarmLog.setStatusCode(ExceptionConstant.ERROR_EXCEPTION_CODE);
      alarmLog.setModule(grayLogVo.getModule());
      alarmLog.setLogLevel(LogLogLevelEnum.ERROR.getLogLevel());
      log.error(JSON.toJSONString(alarmLog));
    } catch (Exception e) {
      log.error("error msg:",e);
    }
  }
}