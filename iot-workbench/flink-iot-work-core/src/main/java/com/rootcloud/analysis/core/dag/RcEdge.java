/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.core.dag;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * DAG模型的边.
 * 连接2个节点，包含起始与结束节点和节点间的联系规则{@link DispatchRule}
 */
@Data
public class RcEdge {
  /**
   * 起始节点.
   */
  private RcNode startNode;
  /**
   * 结束节点.
   */
  private RcNode endNode;
  /**
   * 节点间的联系规则.
   */
  private DispatchRule dispatchRule;

}
