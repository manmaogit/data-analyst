/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.core.enums;

public enum TaskDependTypeEnum {
  /**
   * 0 run current tasks only
   * 1 run current tasks and previous tasks
   * 2 run current tasks and the other tasks that depend on current tasks.
   */
  TASK_ONLY, TASK_PRE, TASK_POST
}
