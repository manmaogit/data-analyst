/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.core.constants;

public class FilterConstant {

  public static final String ATTRIBUTE = "attribute";

  public static final String VALUE = "value";

  public static final String VALUES = "values";

  public static final String LOGICAL_OPERATOR = "logical_operator";

  public static final String FILTER_RULE = "filter_rule";

  public static final String CONTENTS = "contents";

}
