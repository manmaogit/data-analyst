/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.core.exec;

import com.rootcloud.analysis.core.dag.RcGraph;

/**
 * DAG计划模型执行段.
 *
 * <p>阶段实际也是一个小图，是对大图的切分产生，切分的标准为发生Shuffle操作时，如Join.
 */
public class Stage {
  /**
   * 序号.
   */
  private int stageNo;
  /**
   * 阶段图.
   */
  private RcGraph stageGraph;

  public Stage() {
  }

  public Stage(int stageNo, RcGraph stageGraph) {
    this.stageNo = stageNo;
    this.stageGraph = stageGraph;
  }

  public int getStageNo() {
    return stageNo;
  }

  public void setStageNo(int stageNo) {
    this.stageNo = stageNo;
  }

  public int stageNo() {
    return stageNo;
  }

  public Stage stageNo(int stageNo) {
    this.stageNo = stageNo;
    return this;
  }

  public RcGraph getStageGraph() {
    return stageGraph;
  }

  public void setStageGraph(RcGraph stageGraph) {
    this.stageGraph = stageGraph;
  }

  public RcGraph stageGraph() {
    return stageGraph;
  }

  public Stage stageGraph(RcGraph stageGraph) {
    this.stageGraph = stageGraph;
    return this;
  }

  @Override
  public String toString() {
    final StringBuffer sb = new StringBuffer("Stage{");
    sb.append("stageNo=").append(stageNo);
    sb.append(", stageGraph=").append(stageGraph);
    sb.append('}');
    return sb.toString();
  }
}
