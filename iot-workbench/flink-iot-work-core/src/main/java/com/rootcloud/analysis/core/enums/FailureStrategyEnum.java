/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.core.enums;

public enum FailureStrategyEnum {
  /**
   * 0 ending process when some tasks failed.
   * 1 continue running when some tasks failed.
   **/
  END, CONTINUE
}
