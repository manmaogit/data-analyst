/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.core.enums;

import com.rootcloud.analysis.core.constants.JobConstant;
import com.rootcloud.analysis.core.data.IDataBuilder;
import com.rootcloud.analysis.core.data.ISqlExpressBuilder;
import com.rootcloud.analysis.core.data.ISqlValueExpressBuilder;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import lombok.Getter;

/**
 * @author manmao
 */

@Getter
public enum DataTypeInfoEnum {

  /* tinyint */
  TINYINT(scale -> "BasicTypeInfo.BYTE_TYPE_INFO", obj -> {
    if (obj == null) {
      return null;
    }
    if (obj instanceof Number) {
      return ((Number) obj).byteValue();
    }
    if (obj instanceof String && ((String) obj).matches("^-?\\d+$")) {
      return Byte.parseByte((String) obj);
    }
    if (obj instanceof Boolean) {
      return ((Boolean) obj) ?  Integer.valueOf(1).byteValue() :  Integer.valueOf(0).byteValue();
    }
    throw new IllegalArgumentException("Invalid data: " + obj);
  }, (obj, scale) -> {
    return String.format("( CAST( `%s` AS TINYINT)", obj);
  }, (obj, scale) -> {
    return String.format("%s", obj);
  }),

  SMALLINT(scale -> "BasicTypeInfo.SHORT_TYPE_INFO", obj -> {
    if (obj == null) {
      return null;
    }
    if (obj instanceof Number) {
      return ((Number) obj).shortValue();
    }
    if (obj instanceof String && ((String) obj).matches("^-?\\d+$")) {
      return Short.parseShort((String) obj);
    }
    if (obj instanceof Boolean) {
      return ((Boolean) obj) ?  Integer.valueOf(1).shortValue()  :  Integer.valueOf(0).shortValue();
    }
    throw new IllegalArgumentException("Invalid data: " + obj);
  }, (obj, scale) -> {
    return String.format("(  CAST( `%s` AS SMALLINT)", obj);
  }, (obj, scale) -> {
    return String.format("%s", obj);
  }),

  INT(scale -> "BasicTypeInfo.INT_TYPE_INFO", obj -> {
    if (obj == null) {
      return null;
    }
    if (obj instanceof Number) {
      return ((Number) obj).intValue();
    }
    if (obj instanceof String && ((String) obj).matches("^-?\\d+$")) {
      return Integer.parseInt((String) obj);
    }
    if (obj instanceof Boolean) {
      return ((Boolean) obj) ?  Integer.valueOf(1).intValue()
          : Integer.valueOf(0).intValue();
    }
    throw new IllegalArgumentException("Invalid data: " + obj);
  }, (obj, scale) -> {
    return String.format("(  CAST( `%s` AS INT)", obj);
  }, (obj, scale) -> {
    return String.format("%s", obj);
  }),

  BIGINT(scale -> "BasicTypeInfo.LONG_TYPE_INFO", obj -> {
    if (obj == null) {
      return null;
    }
    if (obj instanceof Long) {
      return obj;
    }
    if (obj instanceof Number) {
      return ((Number) obj).longValue();
    }
    if (obj instanceof String && ((String) obj).matches("^-?\\d+$")) {
      return Long.parseLong((String) obj);
    }
    if (obj instanceof Boolean) {
      return ((Boolean) obj) ? Long.valueOf(1) : Long.valueOf(0);
    }
    throw new IllegalArgumentException("Invalid data: " + obj);
  }, (obj,scale) -> {
    return String.format("(  CAST( `%s` AS BIGINT)", obj);
  }, (obj, scale) -> {
    return String.format("%s", obj);
  }),

  DECIMAL(scale -> String.format("BigDecimalTypeInfo.of(38, %s)", scale), obj -> {
    if (obj == null) {
      return null;
    }
    if (obj instanceof BigDecimal) {
      return obj;
    }
    if (obj instanceof Number) {
      return new BigDecimal(obj.toString());
    }
    if (obj instanceof String && ((String) obj).matches("^-?\\d+(\\.\\d+)?$")) {
      return new BigDecimal((String) obj);
    }
    throw new IllegalArgumentException("Invalid data: " + obj);
  }, (obj, scale) -> {
    return String.format("(  CAST( `%s` AS DECIMAL(38, %s))", obj, scale);
  }, (obj, scale) -> {
    return String.format(" CAST( %s AS DECIMAL(38, %s))", obj, scale);
  }),

  FLOAT(scale -> "BasicTypeInfo.FLOAT_TYPE_INFO", obj -> {
    if (obj == null) {
      return null;
    }
    if (obj instanceof Number) {
      return ((Number) obj).floatValue();
    }
    if (obj instanceof String && ((String) obj).matches("^-?\\d+(\\.\\d+)?$")) {
      return Float.parseFloat((String) obj);
    }
    throw new IllegalArgumentException("Invalid data: " + obj);
  }, (obj, scale) -> {
    return String.format("(  CAST( `%s` AS FLOAT)", obj);
  }, (obj, scale) -> {
    return String.format(" CAST( %s AS FLOAT)", obj);
  }),

  DOUBLE(scale -> "BasicTypeInfo.DOUBLE_TYPE_INFO", obj -> {
    if (obj == null) {
      return null;
    }
    if (obj instanceof Float) {
      return Double.parseDouble(obj.toString());
    }
    if (obj instanceof Number) {
      return ((Number) obj).doubleValue();
    }
    if (obj instanceof String && ((String) obj).matches("^-?\\d+(\\.\\d+)?$")) {
      return Double.parseDouble((String) obj);
    }
    throw new IllegalArgumentException("Invalid data: " + obj);
  }, (obj, scale) -> {
    return String.format("(  CAST( `%s` AS DOUBLE)", obj);
  }, (obj, scale) -> {
    return String.format(" CAST( %s AS DOUBLE)", obj);
  }),

  BOOLEAN(scale -> "BasicTypeInfo.BOOLEAN_TYPE_INFO", obj -> {
    if (obj == null) {
      return null;
    }
    if (obj instanceof Boolean) {
      return obj;
    }
    if (obj instanceof Number) {
      return ((Number) obj).intValue() == 1;
    }
    if (obj instanceof String) {
      if ("1".equals(obj)) {
        return Boolean.TRUE;
      } else if ("0".equals(obj)) {
        return Boolean.FALSE;
      } else {
        return Boolean.parseBoolean((String) obj);
      }
    }
    return Boolean.parseBoolean(obj.toString());
  }, (obj, scale) -> {
    return String.format("(  CAST( `%s` AS BOOLEAN)", obj);
  }, (obj, scale) -> {
    return String.format(" CAST( %s AS BOOLEAN)", obj);
  }),

  DATE(scale -> "SqlTimeTypeInfo.DATE", obj -> {
    if (obj == null) {
      return null;
    }
    if (obj instanceof Date) {
      return obj;
    }
    if (obj instanceof Number) {
      return new Date(((Number) obj).longValue());
    }
    if (obj instanceof String && ((String) obj)
        .matches("^\\d{4}-\\d{1,2}-\\d{1,2}$")) {
      return Date.valueOf(((String) obj));
    }
    throw new IllegalArgumentException("Invalid data: " + obj);
  }, (obj, scale) -> {
    return String.format("(  CAST( `%s` AS DATE)", obj);
  }, (obj, scale) -> {
    return String.format("  CAST( '%s' AS DATE)", obj.toString().trim());
  }),

  TIME(scale -> "SqlTimeTypeInfo.TIME", obj -> {
    if (obj == null) {
      return null;
    }
    if (obj instanceof Time) {
      return obj;
    }
    if (obj instanceof Number) {
      return new Time(((Number) obj).longValue());
    }
    if (obj instanceof String && ((String) obj)
        .matches("^\\d{2}:\\d{2}:\\d{2}$")) {
      return Time.valueOf(((String) obj));
    }
    if (obj instanceof String && ((String) obj)
        .matches("^\\d{2}:\\d{2}$")) {
      return Time.valueOf(((String) obj) + ":00");
    }
    throw new IllegalArgumentException("Invalid data: " + obj);
  }, (obj, scale) -> {
    return String.format("(  CAST( `%s` AS TIME )", obj);
  }, (obj, scale) -> {
    return String.format("  CAST( '%s' AS TIME )", obj.toString().trim());
  }),

  TIMESTAMP(scale -> "SqlTimeTypeInfo.TIMESTAMP", obj -> {
    if (obj == null) {
      return null;
    }
    if (obj instanceof Timestamp) {
      return obj;
    }
    if (obj instanceof Number) {
      return new Timestamp(((Number) obj).longValue());
    }
    if (obj instanceof String && ((String) obj).matches("^\\d{4}-\\d{1,2}-\\d{1,2}.\\d{2}:\\d{2}:\\d{2}(\\.\\d+)*$")) {
      return Timestamp.valueOf(((String) obj).replace("T", " "));
    }
    if (obj instanceof String && ((String) obj).matches("\\d{10,}")) {
      String tsStr = (String) obj;
      if (tsStr.length() == 13) {
        return new Timestamp(Long.valueOf(tsStr));
      } else if (tsStr.length() == 10) {
        return new Timestamp(Long.valueOf(tsStr) * 1000L);
      }
    }
    throw new IllegalArgumentException("Invalid data: " + obj);
  }, (obj, scale) -> {
    return String.format("(  CAST( `%s` AS TIMESTAMP  )", obj);
  }, (obj, scale) -> {
    String formatString = "yyyy-MM-dd HH:mm:ss";
    Date date = new Date(Long.valueOf(String.valueOf(obj)));
    DateFormat formatter = new SimpleDateFormat(formatString);
    LocalDateTime localDateTime = LocalDateTime.parse(formatter.format(date), DateTimeFormatter.ofPattern(formatString));
    String zoneString = TimeZoneTypeEnum.valueOf("UTC").getTimeZoneValue().substring(3);
    return String.format("   CAST( '%s' AS TIMESTAMP  )  ",
        formatter.format(new Date((localDateTime.toEpochSecond(ZoneOffset.of(zoneString)) * 1000))))
        ;
  }),

  STRING(scale -> "BasicTypeInfo.STRING_TYPE_INFO", obj -> {
    if (obj == null) {
      return null;
    }
    return obj.toString();
  }, (obj, scale) -> {
    return String.format("(  CAST( `%s` AS STRING  )", obj);
  }, (obj, scale) -> {
    return String.format("'%s'", obj);
  });

  private ITypeInfoBuilder flinkTypeInfoClass;

  private IDataBuilder dataBuilder;

  private ISqlExpressBuilder expressBuilder;

  private ISqlValueExpressBuilder sqlValueExpressBuilder;

  DataTypeInfoEnum(ITypeInfoBuilder flinkTypeInfoClass,
                   IDataBuilder dataBuilder,
                   ISqlExpressBuilder expressBuilder,
                   ISqlValueExpressBuilder sqlValueExpressBuilder) {
    this.flinkTypeInfoClass = flinkTypeInfoClass;
    this.dataBuilder = dataBuilder;
    this.expressBuilder = expressBuilder;
    this.sqlValueExpressBuilder = sqlValueExpressBuilder;
  }

  /**
   * Get the corresponding flink type information object name of the specified type name with the
   * additional property name. This feature is supported by flink since 1.9.
   *
   * @param name  type name
   * @param scale scale of the type. This parameter may be redundant for some types, such as INT.
   * @return
   */
  public static String getFlinkTypeInfoClass(String name, Integer scale) {
    for (DataTypeInfoEnum dataTypeInfoEnum : DataTypeInfoEnum.values()) {
      if (dataTypeInfoEnum.toString().equals(name)) {
        if (scale != null) {
          return dataTypeInfoEnum.flinkTypeInfoClass.build(scale);
        } else {
          return dataTypeInfoEnum.flinkTypeInfoClass.build(JobConstant.DEFAULT_SCALE);
        }

      }
    }
    throw new IllegalArgumentException("Invalid type information: " + name);
  }
}
