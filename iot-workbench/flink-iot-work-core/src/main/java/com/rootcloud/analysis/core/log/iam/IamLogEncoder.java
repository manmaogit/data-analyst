/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2020 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.core.log.iam;

import ch.qos.logback.classic.PatternLayout;
import ch.qos.logback.classic.spi.ILoggingEvent;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.rootcloud.analysis.core.enums.GrayLogResultEnum;
import com.rootcloud.analysis.core.enums.IamLogTypeEnum;
import com.rootcloud.analysis.core.graylog.LogVo;
import de.siegmar.logbackgelf.GelfEncoder;

import java.nio.charset.StandardCharsets;
import java.util.UUID;
import org.apache.commons.lang3.StringUtils;

public class IamLogEncoder extends GelfEncoder {

  /**
   * Short message format. Default: `"%m%nopex"`.
   */
  private PatternLayout shortPatternLayout;

  @Override
  public PatternLayout getShortPatternLayout() {
    return shortPatternLayout;
  }

  @Override
  public void setShortPatternLayout(PatternLayout shortPatternLayout) {
    this.shortPatternLayout = shortPatternLayout;
  }

  @Override
  public byte[] encode(final ILoggingEvent event) {
    final String shortMessage = shortPatternLayout.doLayout(event);
    IamLogVo iamLogVo = convertGrayLogToIamLogVo(JSONObject.parseObject(shortMessage, LogVo.class));
    return JSON.toJSONString(iamLogVo).getBytes(StandardCharsets.UTF_8);
  }

  /**
   * 转换为IamLogVo.
   */
  public static IamLogVo convertGrayLogToIamLogVo(LogVo logVo) {
    IamLogVo iamLogVo = new IamLogVo();
    iamLogVo.setId(UUID.randomUUID().toString());
    iamLogVo.setType(IamLogTypeEnum.OPERATION.name().toLowerCase());
    iamLogVo.setTimestamp(System.currentTimeMillis());
    IamLogPayloadVo iamLogPayloadVo = new IamLogPayloadVo();
    iamLogPayloadVo.setUserId(logVo.getUserId());
    try {
      iamLogPayloadVo.setOrganizationId(
          StringUtils.isBlank(logVo.getOrganizationId()) ? logVo.getTenantId()
              : logVo.getOrganizationId());
    } catch (Exception e) {
      e.printStackTrace();
    }
    iamLogPayloadVo.setTenantId(logVo.getTenantId());
    iamLogPayloadVo.setClientId(logVo.getClient());
    iamLogPayloadVo.setClientType(null);
    iamLogPayloadVo.setRequestId(logVo.getRequestId());
    iamLogPayloadVo.setStatus(
        logVo.getResult().equals(GrayLogResultEnum.SUCCESS.getValue()) ? 1 : 0);
    iamLogPayloadVo.setServiceProvider(logVo.getModule());
    iamLogPayloadVo.setDescription(logVo.getShortMessage());
    iamLogPayloadVo.setAction(logVo.getAction());
    iamLogPayloadVo.setResourceId(logVo.getOperateObject());
    iamLogPayloadVo.setResourceType(logVo.getOperateObjectType());
    iamLogPayloadVo.setIp(logVo.getIp());
    iamLogVo.setPayload(iamLogPayloadVo);
    return iamLogVo;
  }


}
