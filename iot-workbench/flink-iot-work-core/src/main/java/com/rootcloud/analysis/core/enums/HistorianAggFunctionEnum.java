/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.core.enums;

import org.apache.commons.lang3.StringUtils;

/**
 * 历史数据节点（目前包含InfluxDB和TdEngine）支持的内置聚合函数.
 *
 * @CreatedBy: zexin.huang
 * @Date: 2021/11/9 1:42 下午
 */
public enum HistorianAggFunctionEnum {

  /**
   * 该字段可以忽略，不进行聚合.
   */
  IGNORED,

  /**
   * 返回非空字段值的数目，输入类型为数值/布尔/字符串，输出类型统一为数值.
   */
  COUNT,

  /**
   * 返回字段值的和，输入类型为数值，输出类型为数值.
   */
  SUM,

  /**
   * 返回时间戳最早的值，输入类型为数值/布尔/字符串，输出类型为数值/布尔/字符串.
   */
  FIRST,

  /**
   * 返回时间戳最近的值，输入类型为数值/布尔/字符串，输出类型为数值/布尔/字符串.
   */
  LAST,

  /**
   * 返回最大的字段值，输入类型为数值，输出类型为数值.
   */
  MAX,

  /**
   * 返回最小的字段值，输入类型为数值，输出类型为数值.
   */
  MIN,

  /**
   * 返回字段的平均值，输入类型为数值，输出类型为数值.
   */
  MEAN,

  /**
   * 返回字段的中位数，输入类型为数值，输出类型为数值.
   */
  MEDIAN,

  /**
   * 返回字段中出现频率最高的值，输入类型为数值/布尔/字符串，输出类型为数值/布尔/字符串.
   */
  MODE,

  /**
   * 返回字段中最大和最小值的差值，输入类型为数值，输出类型为数值.
   */
  SPREAD,

  /**
   * 返回字段的标准差，输入类型为数值，输出类型为数值.
   */
  STDDEV,


  // TdEngine额外的函数.
  /**
   * 返回字段的平均值，输入类型为数值，输出类型为数值.
   */
  AVG,
  ;

  /**
   * getAggFunctionEnum.
   */
  public static HistorianAggFunctionEnum getAggFunctionEnum(String aggFunctionStr) {
    for (HistorianAggFunctionEnum aggFunctionEnum : HistorianAggFunctionEnum.values()) {
      if (StringUtils.equals(aggFunctionEnum.name(), aggFunctionStr)) {
        return aggFunctionEnum;
      }
    }
    return IGNORED;
  }
}
