/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.core.enums;

import com.rootcloud.analysis.core.data.IDataBuilder;

/**
 * idf数据类型和iotworks数据类型的映射关系.
 */
public enum IdfDataTypeEnum {
  STRING(DataTypeEnum.STRING, obj -> {
    return String.format("payload|%s|value", obj);
  }, obj -> String.format("%s", obj)),
  //只有数据字典有bool类型，如果其他台账类型添加bool类型需要考虑重新设计originalName
  BOOL(DataTypeEnum.STRING, obj -> {
    return String.format("payload|dictvalue|value");
  }, obj -> String.format("%s", obj)),
  DIM(DataTypeEnum.STRING, obj -> null, obj -> String.format("%s._id", obj)),
  DATE(DataTypeEnum.STRING, obj -> {
    return String.format("payload|%s|value", obj);
  }, obj -> String.format("%s", obj)),
  DATE_TIME(DataTypeEnum.STRING, obj -> {
    return String.format("payload|%s|value", obj);
  }, obj -> String.format("%s", obj)),
  TIME(DataTypeEnum.STRING, obj -> {
    return String.format("payload|%s|value", obj);
  }, obj -> String.format("%s", obj)),
  TIMESTAMP_MS(DataTypeEnum.TIMESTAMP, obj -> {
    return String.format("payload|%s|value", obj);
  }, obj -> String.format("%s", obj)),
  DATE_RANGE(DataTypeEnum.STRING, obj -> {
    return String.format("payload|%s|value", obj);
  }, obj -> String.format("%s", obj)),
  DATE_TIME_RANGE(DataTypeEnum.STRING, obj -> {
    return String.format("payload|%s|value", obj);
  }, obj -> String.format("%s", obj)),
  TIME_RANGE(DataTypeEnum.STRING, obj -> {
    return String.format("payload|%s|value", obj);
  }, obj -> String.format("%s", obj)),
  FLOAT(DataTypeEnum.DOUBLE, obj -> {
    return String.format("payload|%s|value", obj);
  }, obj -> String.format("%s", obj)),
  DOUBLE(DataTypeEnum.DOUBLE, obj -> {
    return String.format("payload|%s|value", obj);
  }, obj -> String.format("%s", obj)),
  ONLINE_ENUM(DataTypeEnum.STRING, obj -> {
    return String.format("payload|%s|value", obj);
  }, obj -> String.format("%s._key", obj)),
  WORKING_STATUS_ENUM(DataTypeEnum.STRING, obj -> String.format("payload|%s|value", obj),
      obj -> String.format("%s._key", obj)),
  ASSET(DataTypeEnum.STRING, obj -> String.format("payload|%s|value", obj),
      obj -> String.format("%s._ids", obj)),
  DIC(DataTypeEnum.STRING, obj -> String.format("payload|%s|value", obj),
      obj -> String.format("%s._ids", obj)),
  BIZ(DataTypeEnum.STRING, obj -> String.format("payload|%s|value", obj),
      obj -> String.format("%s._ids", obj)),
  INT(DataTypeEnum.BIG_INT, obj -> String.format("payload|%s|value", obj),
      obj -> String.format("%s", obj)),
  BIGINT(DataTypeEnum.BIGINT, obj -> String.format("payload|%s|value", obj),
      obj -> String.format("%s", obj)),
  BIG_INT(DataTypeEnum.BIGINT, obj -> String.format("payload|%s|value", obj),
      obj -> String.format("%s", obj)),
  ENUM(DataTypeEnum.STRING, obj -> String.format("payload|%s|value", obj),
      obj -> String.format("%s._key", obj)),
  THING(DataTypeEnum.STRING, obj -> null, obj -> null),
  DECIMAL(DataTypeEnum.DECIMAL, obj -> {
    return String.format("payload|%s|value", obj);
  }, obj -> String.format("%s", obj)),;

  /**
   * iotowrks数据类型.
   */
  private DataTypeEnum iotworksDataType;
  private IDataBuilder originalName;
  private IDataBuilder idfFieldName;

  /**
   * 构造.
   */
  IdfDataTypeEnum(DataTypeEnum string, IDataBuilder originalName, IDataBuilder idfFieldName) {
    this.iotworksDataType = string;
    this.originalName = originalName;
    this.idfFieldName = idfFieldName;
  }

  /**
   * 通过idf类型获取iotworks数据类型.
   */
  public static DataTypeEnum getIotworksDataType(String idfType) {
    for (IdfDataTypeEnum idfDataTypeEnum : IdfDataTypeEnum.values()) {
      if (idfType.equals(idfDataTypeEnum.name())) {
        return idfDataTypeEnum.iotworksDataType;
      }
    }
    return DataTypeEnum.STRING;
  }

  public String getOriginalName(String name) {
    return (String) this.originalName.build(name);

  }

  public String getIdfFieldName(String name) {
    return (String) this.idfFieldName.build(name);

  }
}
