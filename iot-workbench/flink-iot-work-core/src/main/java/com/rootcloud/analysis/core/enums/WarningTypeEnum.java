/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.core.enums;

public enum WarningTypeEnum {
  /**
   * 0 do not send warning;
   * 1 send if process success;
   * 2 send if process failed;
   * 3 send if process ending.
   */
  NONE, SUCCESS, FAILURE, ALL
}
