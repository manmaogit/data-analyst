/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.core.enums;

public enum SchedulerDataTypeEnum {
  /**
   * 0 string
   * 1 integer
   * 2 long
   * 3 float
   * 4 double
   * 5 date, "YYYY-MM-DD"
   * 6 time, "HH:MM:SS"
   * 7 time stamp
   * 8 Boolean.
   */
  VARCHAR,INTEGER,LONG,FLOAT,DOUBLE,DATE,TIME,TIMESTAMP,BOOLEAN
}
