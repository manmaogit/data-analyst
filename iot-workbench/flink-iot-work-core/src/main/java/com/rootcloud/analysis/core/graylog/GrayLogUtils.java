/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2020 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.core.graylog;

import com.alibaba.fastjson.JSON;
import com.rootcloud.analysis.core.enums.LogLogLevelEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class GrayLogUtils {

  private static Logger log = LoggerFactory.getLogger(GrayLogUtils.class);

  /**
   * grayLog 记录info级别的日志.
   *
   * @param grayLogVo grayLogVo
   */
  public static void info(GrayLogVo grayLogVo) {
    grayLogVo.setLogLevel(LogLogLevelEnum.INFO.getLogLevel());
    log.info(JSON.toJSONString(grayLogVo));
  }

  /**
   * grayLog 记录warn级别的日志.
   *
   * @param grayLogVo grayLogVo
   */
  public static void warn(GrayLogVo grayLogVo) {
    grayLogVo.setLogLevel(LogLogLevelEnum.WARN.getLogLevel());
    log.warn(JSON.toJSONString(grayLogVo));
  }

  /**
   * grayLog 记录error级别的日志.
   *
   * @param grayLogVo grayLogVo
   */
  public static void error(GrayLogVo grayLogVo) {
    grayLogVo.setLogLevel(LogLogLevelEnum.ERROR.getLogLevel());
    log.error(JSON.toJSONString(grayLogVo));
  }

  /**
   * grayLog 记录alarm级别的日志,将会触发告警推送.
   *
   * @param grayLogVo grayLogVo
   */
  public static void alarm(GrayLogVo grayLogVo) {
    error(grayLogVo);
    AlarmGrayLogUtils.alarm(grayLogVo);

  }
}