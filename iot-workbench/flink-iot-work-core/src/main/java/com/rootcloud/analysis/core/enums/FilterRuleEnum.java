/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.core.enums;

import com.rootcloud.analysis.core.constants.CommonConstant;
import com.rootcloud.analysis.core.data.IRuleExpressBuilder;
import java.util.List;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;

/**
 * . 过滤器操作符
 */
@Getter
public enum FilterRuleEnum {

  IN_LIST(obj -> {
    String value = "";
    for (String val : (List<String>) obj) {
      if (!StringUtils.isBlank(value)) {
        value = value + CommonConstant.COMMA;
      }
      value = value + val;
    }
    return String.format("IN ( %s ) ) ", value);
  }),   // 在列表   IN (value1,value2)
  NOT_IN_LIST(obj -> {
    String value = "";
    for (String val : (List<String>) obj) {
      if (!StringUtils.isBlank(value)) {
        value = value + CommonConstant.COMMA;
      }
      value = value + val;
    }
    return String.format(" NOT IN ( %s ) ) ", value);
  }),  // 不在列表  NOT IN (value1,value2)
  EQ(obj -> {
    return String.format(" = %s  ) ", obj);
  }), //相等        = value2
  NOT_EQ(obj -> {
    return String.format(" <> %s  ) ", obj);
  }), //不相等     <> value2
  GE(obj -> {
    return String.format(" >= %s  ) ", obj);
  }), //大于等于      >= value2
  GT(obj -> {
    return String.format(" > %s  ) ", obj);
  }), //大于         > value2
  LT(obj -> {
    return String.format(" < %s  ) ", obj);
  }), // 小于等于     <= value2
  LE(obj -> {
    return String.format(" <= %s  ) ", obj);
  }),//小于          < value2
  NULL(obj -> {
    return String.format(" IS NULL  ) ");
  }),//为null      IS NULL
  NOT_NULL(obj -> {
    return String.format(" IS NOT NULL  ) ", obj);
  }),//不为null     IS NOT NULL
  CONTAIN(obj -> {
    return String.format(" LIKE  CONCAT('%%',%s,'%%')  ) ", obj);
  }),//包含         LIKE   CONCAT('%',CONCAT( string2,'%'))   %string2%
  NOT_CONTAIN(obj -> {
    return String.format(" NOT LIKE CONCAT('%%',%s,'%%')  ) ", obj);
  }),//不包含于      NOT LIKE %string2%
  PREFIX(obj -> {
    return String.format("  LIKE CONCAT( %s,'%%') )", obj);
  }),//以开始        LIKE string2%
  NOT_PREFIX(obj -> {
    return String.format(" NOT LIKE CONCAT(%s,'%%')  ) ", obj);
  }),//不以开始   NOT LIKE string2%
  SUFFIX(obj -> {
    return String.format("  LIKE CONCAT('%%',%s) ) ", obj);
  }),//以结束            LIKE %string2
  NOT_SUFFIX(obj -> {
    return String.format(" NOT LIKE CONCAT('%%',%s) ) ", obj);
  }),//不以结束       NOT LIKE %string2
  EMPTY(obj -> {
    return String.format(" = ''  ) ", obj);
  }),//为空字符串          =""
  NOT_EMPTY(obj -> {
    return String.format(" <> ''  ) ", obj);
  }),//不为空字符串     <>""
  IN_RANGE(obj -> {
    String attrName = ((List<String>) obj).get(0);
    String start = ((List<String>) obj).get(1);
    String end = ((List<String>) obj).get(2);
    return String.format(">= %s and %s <= %s  ) ", start, attrName, end);
  }),//在范围内         =>   <=
  NOT_IN_RANGE(obj -> {
    String attrName = ((List<String>) obj).get(0);
    String start = ((List<String>) obj).get(1);
    String end = ((List<String>) obj).get(2);
    return String.format("< %s or %s > %s ) ", start, attrName, end);
  }),//不在范围内     <  ||  >

  //过滤字段是集合
  INTERSECT(obj -> {
    return "";
  });//与指定的值集合存在交集

  private IRuleExpressBuilder ruleExpressBuilder;

  FilterRuleEnum(IRuleExpressBuilder ruleExpressBuilder) {
    this.ruleExpressBuilder = ruleExpressBuilder;
  }


}
