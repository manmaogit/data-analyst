/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.core.enums.dolphin;

import java.util.Arrays;
import java.util.List;

public enum DolphinDependDateValueEnum {
  // 当cycle为hour时
  currentHour, last1Hour, last2Hours, last3Hours, last24Hours,

  // 当cycle为day时
  today, last1Days, last2Days, last3Days, last7Days,

  // 当cycle为week时
  thisWeek, lastWeek,
  lastMonday, lastTuesday, lastWednesday, lastThursday, lastFriday, lastSaturday, lastSunday,

  // 当cycle为month时
  thisMonth, lastMonth, lastMonthBegin, lastMonthEnd
  ;

  /**
   * 根据cycle类型来校验dateValue值是否合法.
   */
  public static boolean validateDateValue(
      DolphinDependItemCycleEnum cycle, DolphinDependDateValueEnum dateValue) {
    switch (cycle) {
      case hour:
        List hourOfCycle =
            Arrays.asList(currentHour, last1Hour, last2Hours, last3Hours, last24Hours);
        return hourOfCycle.contains(dateValue);
      case day:
        List dayOfCycle =
            Arrays.asList(today, last1Days, last2Days, last3Days, last7Days);
        return dayOfCycle.contains(dateValue);
      case week:
        List weekOfCycle =
            Arrays.asList(thisWeek, lastWeek, lastMonday, lastTuesday, lastWednesday,
                          lastThursday, lastFriday, lastSaturday, lastSunday);
        return weekOfCycle.contains(dateValue);
      case month:
        List monthOfCycle =
            Arrays.asList(thisMonth, lastMonth, lastMonthBegin, lastMonthEnd);
        return monthOfCycle.contains(dateValue);
      default:
        return false;
    }
  }
}
