/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.core.enums;

import static com.rootcloud.analysis.core.constants.CommonConstant.DOT;

import lombok.Getter;

/**
 * Historian针对个别字段的存表是不不按json处理，而是打平后按各自类型加后缀.
 *
 * @CreatedBy: zexin.huang
 * @Date: 2022/5/27 10:12
 */
@Getter
public enum HistorianIgnoreTagType {

  State("__state__"),

  Online("__online__"),

  Location("__location__"),

  WorkingStatus("__workingStatus__"),
  ;

  /**
   * Historian使用的字段名称.
   */
  private String historianKey;

  HistorianIgnoreTagType(String historianKey) {
    this.historianKey = historianKey;
  }

  /**
   * 判断某个字段名是否Historian会进行特殊处理的字段.
   */
  public static boolean isHistorianIgnoreTagType(String columnName) {
    for (HistorianIgnoreTagType type : HistorianIgnoreTagType.values()) {
      if (columnName.startsWith(type.getHistorianKey() + DOT)) {
        return true;
      }
    }
    return false;
  }
}
