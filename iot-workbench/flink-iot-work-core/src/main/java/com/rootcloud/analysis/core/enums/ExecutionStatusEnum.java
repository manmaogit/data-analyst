/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.core.enums;

public enum ExecutionStatusEnum {

  /**
   * status：
   * 0 submit success
   * 1 running
   * 2 ready pause
   * 3 pause
   * 4 ready stop
   * 5 stop
   * 6 failure
   * 7 success
   * 8 need fault tolerance
   * 9 kill
   * 10 waiting thread
   * 11 waiting depend node complete.
   */
  SUBMITTED_SUCCESS,
  RUNNING_EXECUTION,
  READY_PAUSE,
  PAUSE,
  READY_STOP,
  STOP,
  FAILURE,
  SUCCESS,
  NEED_FAULT_TOLERANCE,
  KILL,
  WAITTING_THREAD,
  WAITTING_DEPEND;

  /**
   * status is success.
   *
   * @return status
   */
  public boolean typeIsSuccess() {
    return this == SUCCESS;
  }

  /**
   * status is failure.
   *
   * @return status
   */
  public boolean typeIsFailure() {
    return this == FAILURE || this == NEED_FAULT_TOLERANCE;
  }

  /**
   * status is finished.
   *
   * @return status
   */
  public boolean typeIsFinished() {

    return typeIsSuccess() || typeIsFailure() || typeIsCancel() || typeIsPause()
        || typeIsWaittingThread();
  }

  /**
   * status is waiting thread.
   *
   * @return status
   */
  public boolean typeIsWaittingThread() {
    return this == WAITTING_THREAD;
  }

  /**
   * status is pause.
   *
   * @return status
   */
  public boolean typeIsPause() {
    return this == PAUSE;
  }

  /**
   * status is running.
   *
   * @return status
   */
  public boolean typeIsRunning() {
    return this == RUNNING_EXECUTION || this == WAITTING_DEPEND;
  }

  /**
   * status is cancel.
   *
   * @return status
   */
  public boolean typeIsCancel() {
    return this == KILL || this == STOP;
  }


}
