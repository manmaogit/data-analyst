/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.common.util;

import com.google.common.base.Preconditions;
import com.rootcloud.analysis.core.enums.DataTypeEnum;
import com.rootcloud.analysis.core.enums.DataTypeInfoEnum;
import com.rootcloud.analysis.core.enums.PostgreTypeEnum;

public class PostgreSqlDataTypeConverter {

  /**
   * Converts data type to unified data type.
   *
   * @param dataType source data type
   *
   * @return unified data type
   */
  public static DataTypeEnum convert2UnifiedDataType(String dataType) {
    String lowerCaseType = dataType.toLowerCase();
    String upperCaseType = dataType.toUpperCase();
    String timeStampRegex = "^(timestamp|datetime|date|time).*$";
    if (lowerCaseType.matches(timeStampRegex) || upperCaseType.matches(timeStampRegex)) {
      return DataTypeEnum.TIMESTAMP;
    }
    String integerRegex = "^(BIGINT|BIGSERIAL|INTEGER|SMALLINT|SERIAL|INT).*$";
    if (lowerCaseType.matches(integerRegex) || upperCaseType.matches(integerRegex)) {
      return DataTypeEnum.INTEGER;
    }
    String numberRegex = "^(DOUBLE|PRECISION|MONEY|NUMERIC|REAL|double precision|real|float).*$";
    if (lowerCaseType.matches(numberRegex) || upperCaseType.matches(numberRegex)) {
      return DataTypeEnum.NUMBER;
    }
    String booleanRegex = "^(BOOL|boolean).*$";
    if (lowerCaseType.matches(booleanRegex) || upperCaseType.matches(booleanRegex)) {
      return DataTypeEnum.BOOLEAN;
    }
    return DataTypeEnum.STRING;
  }


  /**
   * Converts data type to flink data type.
   *
   * @param dataType source data type
   *
   * @return unified data type
   */
  public static DataTypeInfoEnum convert2TypeInfo(String dataType) {
    String lowerCaseType = dataType.toLowerCase();
    String upperCaseType = dataType.toUpperCase();
    String timeStampRegex = "^(timestamp).*$";
    if (lowerCaseType.matches(timeStampRegex) || upperCaseType.matches(timeStampRegex)) {
      return DataTypeInfoEnum.TIMESTAMP;
    }
    String dateRegex = "^(date).*$";
    if (lowerCaseType.matches(dateRegex) || upperCaseType.matches(dateRegex)) {
      return DataTypeInfoEnum.DATE;
    }
    String timeRegex = "^(time).*$";
    if (lowerCaseType.matches(timeRegex) || upperCaseType.matches(timeRegex)) {
      return DataTypeInfoEnum.TIME;
    }
    String smallIntRegex = "^(INT2).*$";
    if (lowerCaseType.matches(smallIntRegex) || upperCaseType.matches(smallIntRegex)) {
      return DataTypeInfoEnum.SMALLINT;
    }
    String int4Regex = "^(INT4).*$";
    if (lowerCaseType.matches(int4Regex) || upperCaseType.matches(int4Regex)) {
      return DataTypeInfoEnum.INT;
    }
    String int8Regex = "^(INT8).*$";
    if (lowerCaseType.matches(int8Regex) || upperCaseType.matches(int8Regex)) {
      return DataTypeInfoEnum.BIGINT;
    }
    String integerRegex = "^(INTEGER|SERIAL).*$";
    if (lowerCaseType.matches(integerRegex) || upperCaseType.matches(integerRegex)) {
      return DataTypeInfoEnum.INT;
    }
    String bigIntRegex = "^(BIGINT|BIGSERIAL).*$";
    if (lowerCaseType.matches(bigIntRegex) || upperCaseType.matches(bigIntRegex)) {
      return DataTypeInfoEnum.BIGINT;
    }
    String floatRegex = "^(REAL|FLOAT4).*$";
    if (lowerCaseType.matches(floatRegex) || upperCaseType.matches(floatRegex)) {
      return DataTypeInfoEnum.FLOAT;
    }
    String float8Regex = "^(FLOAT8|DOUBLE PRECISION).*$";
    if (lowerCaseType.matches(float8Regex) || upperCaseType.matches(float8Regex)) {
      return DataTypeInfoEnum.DOUBLE;
    }
    String numericRegex = "^(NUMERIC|DECIMAL).*$";
    if (lowerCaseType.matches(numericRegex) || upperCaseType.matches(numericRegex)) {
      return DataTypeInfoEnum.DECIMAL;
    }
    String booleanRegex = "^(BOOLEAN|BOOL)$";
    if (lowerCaseType.matches(booleanRegex) || upperCaseType.matches(booleanRegex)) {
      return DataTypeInfoEnum.BOOLEAN;
    }
    String stringRegex = "^(CHAR|CHARACTER|VARCHAR|VARYING|TEXT|BPCHAR).*$";
    if (lowerCaseType.matches(stringRegex) || upperCaseType.matches(stringRegex)) {
      return DataTypeInfoEnum.STRING;
    }

    return null;
  }

  /**
   * Converts postgresql type to flink sql type.
   */
  public static String convert2FlinkSqlType(String pgType) {
    PostgreTypeEnum postgreTypeEnum = null;
    for (PostgreTypeEnum value : PostgreTypeEnum.values()) {
      if (value.matches(pgType.toUpperCase())) {
        postgreTypeEnum = value;
      }
    }
    Preconditions.checkNotNull(postgreTypeEnum, "Invalid postgresql type.  " + pgType);
    return postgreTypeEnum.getFlinkSqlTypeBuilder().build(pgType);
  }
}