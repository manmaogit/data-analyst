/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.common.entity;

import java.io.Serializable;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * 设备模型ID、抽象模型ID、设备ID集合 的映射关系.
 *
 * @CreatedBy: zexin.huang
 * @Date: 2022/5/11 13:46
 */
@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DeviceMapping implements Serializable {

  /**
   * 抽象设备模型ID.
   */
  private String abstractDeviceTypeId;

  /**
   * 设备模型ID.
   */
  private String deviceTypeId;

  /**
   * 设备模型ID.
   */
  private List<String> deviceIds;
}
