/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.core.constants;

public class TenantResourceConstant {

  public static final String ANALYSIS_CORE_API_KEY = "analysisCoreApi";

  public static final String FLINK_KEY = "flink";

  public static final String DOLPHIN_KEY = "dolphin";

  public static final String DOLPHIN_URL_KEY = "schedulerUrl";

  public static final String FLINK_URL_KEY = "flinkUrl";

}
