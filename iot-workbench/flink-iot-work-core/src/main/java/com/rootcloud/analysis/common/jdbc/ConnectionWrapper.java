/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.common.jdbc;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.concurrent.atomic.AtomicBoolean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ConnectionWrapper implements Serializable {

  private static final long serialVersionUID = 1L;

  private static final Logger logger = LoggerFactory.getLogger(ConnectionWrapper.class);

  private final String driverClassName;

  private final String url;

  private final String username;

  private final String password;

  private final String validationQuery;


  private Connection connection;

  /**
   * Constructor.
   */
  public ConnectionWrapper(String driverClassName, String url, String username,
                           String password, String validationQuery) {
    super();
    this.driverClassName = driverClassName;
    this.url = url;
    this.username = username;
    this.password = password;
    this.validationQuery = validationQuery;
  }

  private void loadDriverClass() {
    synchronized (getClass().getClassLoader()) {
      try {
        Class.forName(driverClassName);
      } catch (ClassNotFoundException e) {
        throw new RuntimeException("Failed to load class " + driverClassName, e);
      }
    }
  }

  /**
   * Create statement.
   */
  public Statement createStatement() throws SQLException {
    return getConnection().createStatement();
  }

  /**
   * prepare statement.
   */
  public PreparedStatement prepareStatement(String sql) throws SQLException {
    return getConnection().prepareStatement(sql);
  }

  /**
   * Get connection.
   */
  public Connection getConnection() {
    if (connection == null || !validateConnection()) {
      reconnect();
    }
    return connection;
  }

  /**
   * Reconnect.
   */
  public void reconnect() {
    disconnect();
    connect();
  }

  /**
   * Disconnect.
   */
  public void disconnect() {
    try {
      if (connection != null && !connection.isClosed()) {
        logger.info("Disconnected to: {}", url);
        connection.close();
      }
      connection = null;
    } catch (SQLException e) {
      logger.warn("Failed to close the connection, url: {}, exception: {}", url, e.toString());
    }
  }

  /**
   * Connect.
   */
  public void connect() {
    try {
      loadDriverClass();
      connection = DriverManager.getConnection(url, username, password);
      logger.info("Connected to: {}", url);
    } catch (SQLException e) {
      logger.error("Exception occurred when creating the connection, url: {},  exception: {}", url,
          e.toString());
      throw new RuntimeException("Failed to connect to database.", e.getCause());
    }
  }

  /**
   * Validates the connection.
   */
  private boolean validateConnection() {
    try {
      if (connection.isClosed()) {
        return false;
      }
      try (Statement statement = connection.createStatement();
          ResultSet resultSet = statement.executeQuery(validationQuery)) {
        return resultSet.next();
      }
    } catch (SQLException e) {
      logger.error(
          "Exception occurred when validating the connection, url: {}, "
              + "validation query: {}, exception: {}",
          url, validationQuery, e.toString());
      return false;
    }
  }

  /**
   * Close.
   */
  public void close() {
    disconnect();
  }

}
