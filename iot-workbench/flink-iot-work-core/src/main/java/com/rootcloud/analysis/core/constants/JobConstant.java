/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.core.constants;

public class JobConstant {

  public static final String KEY_IOTOWRKS = "iotworks";
  public static final String KEY_MAX_OUT_OF_ORDERNESS = "max_out_of_orderness";
  public static final String KEY_PARALLELISM = "parallelism";
  public static final String KEY_WATERMARK_PARALLELISM = "watermark_parallelism";
  public static final String KEY_JOB_TYPE = "job_type";
  public static final String KEY_JOB_NAME = "job_name";
  public static final String KEY_JOB_DESC = "job_desc";
  public static final String KEY_TIMEZONE = "time_zone";
  public static final String KEY_SOURCES = "sources";
  public static final String KEY_SINKS = "sinks";
  public static final String KEY_PROCESSORS = "processors";
  public static final String KEY_INPUT_NODES = "input_nodes";
  public static final String KEY_OUTPUT_NODES = "output_nodes";
  public static final String KEY_PROCESS_NODES = "process_nodes";
  public static final String KEY_SOURCE_TYPE = "source_type";
  public static final String KEY_PROCESSOR_TYPE = "processor_type";
  public static final String KEY_SOURCE_ID = "source_id";
  public static final String KEY_PARAMETERS = "params";
  public static final String KEY_FILTER_SQL = "filter_sql";
  public static final String KEY_BROKER_LIST = "broker_list";
  public static final String KEY_SCHEMA = "schema";
  public static final String KEY_INPUT_SCHEMA = "input_schema";
  public static final String KEY_TIME_ATTR = "time_attribute";
  public static final String KEY_TOPICS = "topics";
  public static final String KEY_OFFSET_STRATEGY = "offset_strategy";
  public static final String KEY_CUSTOM_CONSUMER_GROUP_ID = "custom_consumer_group_id";
  public static final String KEY_KAFKA_DATASOURCE_TYPE = "key_kafka_datasource_type";
  public static final Integer LIMIT_DEFAULT = 100000;
  public static final String KEY_CHANGE_DETECT = "change_detect";
  public static final String KEY_TEMPLATE = "template";
  public static final String KEY_HOST = "host";
  public static final String KEY_PORT = "port";
  public static final String KEY_USERNAME = "username";
  public static final String KEY_PASSWORD = "password";
  public static final String KEY_DATABASE = "database";
  public static final String KEY_TABLE = "table";
  public static final String KEY_SQL = "sql";
  public static final String KEY_COLLECTION = "collection";
  public static final String KEY_PERIOD = "period";
  public static final String KEY_MODE = "mode";
  public static final String KEY_ATTR = "attr";
  public static final String KEY_SINK_TYPE = "sink_type";
  public static final String KEY_SINK_ID = "sink_id";
  public static final String KEY_URI = "uri";
  public static final String KEY_SQL_MODE = "sql_mode";
  public static final String KEY_TABLE_SQL = "tableSql";
  public static final String KEY_TABLE_SQL2 = "table_sql";
  public static final String KEY_LIMIT = "limit";
  public static final String KEY_NODE_TYPE = "node_type";
  public static final String KEY_NODE_ID = "node_id";
  public static final String KEY_NODE_NAME = "node_name";
  public static final String KEY_PRIOR_NODE_ID = "prior_node_id";
  public static final String KEY_DIMENSION_NODE_D = "dimension_node_id";
  public static final String KEY_OPERATORS = "operators";
  public static final String KEY_TYPE = "type";
  public static final String KEY_TOPIC = "topic";
  public static final String KEY_INSERT_KEY = "insert_key";
  public static final String KEY_ACC_ATTR = "acc_attr";
  public static final String KEY_REPLACE_ATTR = "replace_attr";
  public static final String KEY_ATTRIBUTE_MAPPER = "attribute_mapper";
  public static final String KEY_ATTRIBUTE_MAPPER_LIST = "attribute_mapper_list";
  public static final String KEY_OUTPUT_MAPPER = "output_mapper";
  public static final String KEY_DATA_SOURCE = "data_source";
  public static final String KEY_CONF_SOURCE = "conf_source";
  public static final String KEY_SOURCE_ATTRIBUTE = "source_attribute";
  public static final String KEY_TARGET_ATTRIBUTE = "target_attribute";
  public static final String KEY_OUTPUTS = "outputs";
  public static final String KEY_TIME_DIMENSION = "time_dimension";
  public static final String KEY_TIME_AGGREGATION = "time_aggregation";
  public static final String KEY_TIME_AGGREGATION_FLAG = "time_aggregation_flag";
  public static final String KEY_START_TIME = "start_time";
  public static final String KEY_UPDATE_STRATEGY = "update_strategy";
  public static final String KEY_INSERT_TYPE = "insert_type";
  public static final String KEY_DATASOURCE_CATEGORY = "datasource_category";
  public static final String KEY_KAFKA_KEY = "key";
  public static final String KEY_KAFKA_KEY_TYPE = "key_type";
  public static final String KEY_ATTR__NAME = "attr_name";
  public static final String KEY_KAFKA_KEY_VALUE = "key_value";
  public static final String KEY_INSERT_KEY_NAME = "key_name";
  public static final String KEY_INSERT_KEY_TYPE = "key_type";
  public static final String KEY_INSERT_KEY_FIELDS = "key_fields";
  public static final String KEY_INSERT_KEY_FIELD_NAME = "field_name";
  public static final String KEY_SAVEPOINT_LOCATION = "savepoint_location";
  public static final String AUTO_INCREMENT_FLAG = "auto_increment_flag";
  public static final String IS_NULLABLE = "is_nullable";
  public static final String JOIN_TYPE = "join_type";
  public static final String PARAM_JOB_ID = "job_id";
  public static final String PARAM_MONGO_URI = "mongo_uri";
  public static final String PARAM_MONGO_DATABASE = "mongo_database";
  public static final String PARAM_MONGO_COLLECTION = "mongo_collection";
  public static final String PARAM_SYSTEM_DATETIME = "system_datetime";
  public static final String LOGGING_SERVICE_KAFKA_TOPIC = "logging_service_kafka_topic";
  public static final String LOGGING_SERVICE_KAFKA_SERVERS = "logging_service_kafka_servers";
  public static final String KAFKA_ADMIN_EXEC_TIMEOUT = "kafka_admin_exec_timeout";
  public static final String TEMPLATE = "metric_service_aggre";
  public static final String EVENT_TIME = "eventTime";

  public static final String DATA = "data";
  public static final String TOTAL_LIST = "totalList";
  public static final String ID = "_id";
  public static final String X_IOTWORKS_REQUEST_ID = "x-iotworks-request-id";
  public static final String CREATE_TIME = "create_time";
  public static final String UPDATE_TIME = "update_time";
  public static final String PARAMS = "params";
  public static final String DATASOURCE_NAME = "data_source_name";
  public static final String TENANT_ID = "tenant_id";
  public static final String PARAMS_TENANT_ID = "params.tenant_id";

  public static final String CREATED = "created";
  public static final String UPDATED = "updated";
  public static final String PRI_KEY = "PRI";
  public static final String NO_KEY = "NO";

  public static final String IAM_PREFIX = "rc:iotworks:";
  public static final String FLINK_JOB_SLOT_REQUIRED = "slot.required";
  public static final String FLINK_JOB_SLOT_WAIT_SEC = "slot.wait";

  /**
   * 加密公钥/秘钥集合.
   */
  public static final String MONGO_KEYPAIR_COLLECTION = "key_pair";
  public static final String PRIVATE_KEY = "private_key";
  public static final String IOTWORKS = "iotworks-";
  public static final String JOIN_SOURCE_PREFIX = "Source-";

  public static final String KEY_GROUP_ID = "group_id";
  public static final String KEY_GROUP_BASE = "groupbase";
  public static final String KEY_TIMEBASE = "timebase";

  public static final Integer DEFAULT_MAX_OUT_OF_ORDERNESS = 180000;
  /**
   * asset output node constants.
   */
  public static final String ASSET_KEY_ID = "id";
  public static final String ASSET_KEY_GROUP_BASE = "groupbase";
  public static final String ASSET_KEY_GROUP_ID = "groupid";
  public static final String ASSET_KEY_KPIS = "kpis";
  public static final String ASSET_KEY_OBJECT_ID = "objectid";
  public static final String ASSET_KEY_TENANT_ID = "tenantid";
  public static final String ASSET_KEY_TIMEBASE = "timebase";
  public static final String ASSET_KEY_TS = "ts";

  public static final String KEY_CATALOG_NAME = "catalog_name";
  public static final String KEY_DEFAULT_DATABASE = "default_database";
  public static final String KEY_HIVE_CONF_DIR = "hive_conf_dir";
  public static final String KEY_TIME_SCOPE = "time_scope";
  public static final String KEY_END_TIME = "end_time";
  public static final String KEY_VIEW_NAME = "view_name";
  public static final String KEY_PRIOR_NODE_IDS = "prior_node_ids";

  //the overwriting numRecordsOut metric for relational database sink
  public static final String FLINK_METRIC_NUM_RECORDS_OUT_OVERWRITE = "numRecordsOutOverwrite";
  public static final String KEY_PRIMARY_KEY = "primary_key";


  // 批处理Flink SQL内置参数.
  public static final String SYSTEM_TIME_ZONE = "__timeZone__";
  public static final String SYSTEM_ZONE_ID = "__zoneId__";
  public static final String SYS_DATE = "__sysDate__";
  public static final String SYSTEM_DATETIME = "__systemDatetime__";
  public static final String SYSTEM_BIZ_START_DATETIME = "__bizStartDatetime__";
  public static final String SYSTEM_BIZ_END_DATETIME = "__bizEndDatetime__";

  public static final String SYS_DATE_FUNC_KEY = "${__sysDate__}";
  public static final String SYSTEM_DATETIME_FUNC_KEY = "${__systemDatetime__}";
  public static final String SYSTEM_BIZ_START_DATETIME_FUNC_KEY = "${__bizStartDatetime__}";
  public static final String SYSTEM_BIZ_END_DATETIME_FUNC_KEY = "${__bizEndDatetime__}";
  public static final String SYS_DATE_FUNC_KEY_REG = "\\$\\{__sysDate__\\}";
  public static final String SYSTEM_DATETIME_FUNC_KEY_REG = "\\$\\{__systemDatetime__\\}";
  public static final String SYSTEM_BIZ_START_DATETIME_FUNC_KEY_REG =
      "\\$\\{__bizStartDatetime__\\}";
  public static final String SYSTEM_BIZ_END_DATETIME_FUNC_KEY_REG = "\\$\\{__bizEndDatetime__\\}";
  public static final String ROWNUM_KEY = "rownum";


  public static final String CONTAIN_AUTO_INCREMENT_KEY = "contain_auto_increment_key";

  // JDBC节点输入数据最大条数限制.
  public static final int JDBC_INPUT_MAX_LIMIT_NUM = 1000000;

  // Kafka节点默认offset消费策略
  public static final String KAFKA_CONSUMER_OFFSET_STRATEGY_DEFAULT = "GROUP";

  //去重节点
  public static final String KEY_DEDUP_KEY_FIELD_LIST = "keyFieldList";
  public static final String KEY_DEDUP_SORT_BY = "sortBy";
  public static final String KEY_DEDUP_KEEP = "keep";
  public static final String KEY_DEDUP_MAP_MAX_SIZE = "dedupMapMaxSize";
  public static final String KEY_DEDUP_AGG_PRIMARY_KEY = "aggPrimaryKey";

  public static final int DEFAULT_SCALE = 14;

  public static final String DISTRIBUTION_STATE = "distribution_state";
  public static final String DATA_CENTER_NAME = "data_center_name";
  public static final String ORG_NAME = "org_name";
  public static final String REVISION = "revision";
  public static final String DISTRIBUTED_FLAG = "distributed_flag";
  public static final String CREATOR_ID = "creator_id";

  public static final String KEY_JOB_STATE = "jobState";
  public static final String KEY_JOB_RUNNING_STATE = "jobRunningState";

  // InfluxDB单条SQL最大查询次数.
  public static final int INFLUXDB_QUERY_MAX_TIMES = 1;
  // InfluxDB时间字段的默认字段名
  public static final String INFLUXDB_TIME_COLUMN_NAME = "time";

  // TdEngine时间字段的默认字段名
  public static final String TD_ENGINE_TIME_COLUMN_NAME = "ts";

  public static final String KEY_TIMEOUT_SECONDS = "timeout_seconds";
  public static final String KEY_PAGE_LIMIT = "page_limit";

  /**
   * HTTP超时时长（单位：秒）.
   */
  public static final int HTTP_REQUEST_TIMEOUT_SECONDS = 180;

  /**
   * HTTP查询的分页大小.
   */
  public static final int HTTP_REQUEST_PAGE_LIMIT = 1000;

  //IAM
  public static final String DATA_PERMISSIONS_JOB_QUERY_ACTION = "rc:iotworks:Job:Query";
  public static final String DATA_PERMISSIONS_JOB_EDIT_ACTION = "rc:iotworks:Job:Edit";
  public static final String DATA_PERMISSIONS_DISTRIBUTION_JOB_QUERY_ACTION =
      "rc:iotworks:DistributeJob:Query";
  public static final String DATA_PERMISSIONS_DISTRIBUTION_JOB_EDIT_ACTION =
      "rc:iotworks:DistributeJob:Edit";
  public static final String DATA_PERMISSIONS_WORKFLOW_QUERY_ACTION = "rc:iotworks:Workflow:Query";
  public static final String DATA_PERMISSIONS_SCHEDULE_QUERY_ACTION = "rc:iotworks:Schedule:Query";

  public static final String DATA_PERMISSIONS_WORKFLOW_EDIT_ACTION = "rc:iotworks:Workflow:Edit";
  public static final String DATA_PERMISSIONS_SCHEDULE_EDIT_ACTION = "rc:iotworks:Schedule:Edit";
  public static final String DATA_PERMISSIONS_WORKFLOW_SCHEDULE_QUERY_ACTION =
      "rc:iotworks:WorkflowSchedule:Query";
  public static final String DATA_PERMISSIONS_WORKFLOW_SCHEDULE_EDIT_ACTION =
      "rc:iotworks:WorkflowSchedule:Edit";


  public static final String KEY_RETRY = "retry";
  public static final String KEY_RETRY_INTERVAL_MIN = "retry_interval_min";

  public static final String KEY_ABSTRACT_DEVICE_TYPE_IDS = "abstract_device_type_ids";
  public static final String KEY_HUB_SERVICE_URL = "hub_service_url";
  public static final String KEY_DEVICE_MAPPINGS = "device_mappings";


  /**
   * 表值聚合算子.
   */
  public static final String KEY_TABLE_AGG_FUNC_NAME = "tableAggFuncName";
  public static final String KEY_GROUP_BY_KEY = "group_by";

}
