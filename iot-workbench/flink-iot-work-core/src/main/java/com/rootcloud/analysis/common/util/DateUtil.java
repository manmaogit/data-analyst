/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.common.util;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;

import org.apache.commons.lang3.time.DateFormatUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.ISODateTimeFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DateUtil {

  private static final Logger logger = LoggerFactory.getLogger(DateUtil.class);
  public static final String TIME_PATTERN_01 = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";
  public static final String TIME_PATTERN_02 = "yyyy-MM-dd HH:mm:ss.SSS";
  public static final String TIME_PATTERN_03 = "yyyy-MM-dd HH:mm:ss";
  public static final String TIME_PATTERN_04 = "yyyy-MM-dd";
  public static final String TIME_PATTERN_05 = "yyyy-MM";
  public static final String TIME_PATTERN_06 = "yyyyMMddHHmmss";

  /**
   * 生成ISO-8601 规范的时间格式.
   *
   * @param dateStr 格式为yyyy-MM-dd'T'HH:mm:ss.SSSZ的时间字符串.
   */
  public static Date formatIso8601StringToDate(String dateStr) {
    org.joda.time.format.DateTimeFormatter fmt = DateTimeFormat.forPattern(TIME_PATTERN_01);
    return fmt.parseDateTime(dateStr).toDate();
  }

  /**
   * 生成ISO-8601 规范的时间格式.
   *
   * @param dateStr 格式为yyyy-MM-dd'T'HH:mm:ss.SSSZ的时间字符串.
   */
  public static long formatIso8601StringToLong(String dateStr) {
    return formatIso8601StringToDate(dateStr).getTime();
  }

  /**
   * 生成ISO-8601 规范的时间格式.
   * @return  格式为yyyy-MM-dd'T'HH:mm:ss.SSSZ的时间字符串.
   */
  public static String formatIso8601LongToString(long time) {
    return new DateTime(time).toString(TIME_PATTERN_01);
  }

  /**
   * 按指定时间格式Long转String.
   *
   * @param time 时间戳格式
   * @param formatPattern 时间格式
   */
  public static String formatPatternLongToString(long time, String formatPattern) {
    return new DateTime(time).toString(formatPattern);
  }

  /**
   * 按指定时间格式String转Long.
   *
   * @param dateStr 时间戳格式
   * @param formatPattern 时间格式
   */
  public static long formatPatternStringToLong(String dateStr, String formatPattern) {
    org.joda.time.format.DateTimeFormatter fmt = DateTimeFormat.forPattern(formatPattern);
    return fmt.parseDateTime(dateStr).getMillis();
  }

  /**
   * 按指定时间格式Long转Long.
   *
   * @param time 时间戳格式
   * @param sourceFormatPattern 输入的时间格式
   * @param targetFormatPattern 输出的时间格式
   */
  public static long formatPatternLongToLong(long time, String sourceFormatPattern,
                                             String targetFormatPattern) {
    String dateStr = formatPatternLongToString(time, sourceFormatPattern);
    return formatPatternStringToLong(dateStr, targetFormatPattern);
  }

  /**
   * 生成时间戳.
   */
  public static String getTimeStamp(String s) {
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd");
    Date date = null;

    try {
      date = simpleDateFormat.parse(s);
    } catch (ParseException e) {
      logger.error("error msg:", e);
    }
    long ts = Objects.requireNonNull(date).getTime();
    return String.valueOf(ts);
  }

  /**
   * 返回时间戳对应的时间.
   */
  public static Timestamp getDateFromTimeStamp(String s) {
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    long lt = new Long(s);
    Date date1 = new Date(lt);
    return Timestamp.valueOf(simpleDateFormat.format(date1));
  }

  /**
   * 获取原时间戳.
   */
  public static Long recoverReverseTime(Long reverseTime) {
    long longTime = Long.MAX_VALUE - reverseTime;
    return longTime / 1000000;
  }

  /**
   * 生成页面普通展示时间.
   */
  public static String formatNormalDateString(Date date) {
    String pattern = "yyyy-MM-dd HH:mm:ss";
    return DateFormatUtils.format(date, pattern);
  }

  /**
   * 时间转指定格式字符串.
   *
   * @param time    time
   * @param pattern pattern
   *
   * @return string date
   */
  public static String formatNormalDateString(Long time, String pattern) {
    DateTimeFormatter ftf = DateTimeFormatter.ofPattern(pattern);
    return ftf.format(LocalDateTime.ofInstant(Instant.ofEpochMilli(time), ZoneId.systemDefault()));
  }

  /**
   * 将字符串转日期成Long类型的时间戳，格式为：yyyy-MM-dd HH:mm:ss.
   */
  public static Long convertTimeToLong(String time) {
    DateTimeFormatter ftf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    LocalDateTime parse = LocalDateTime.parse(time, ftf);
    return LocalDateTime.from(parse).atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
  }

  /**
   * 将Long类型的时间戳转换成String 类型的时间格式，时间格式为：yyyy-MM-dd HH:mm:ss.
   */
  public static String convertTimeToString(Long time) {
    DateTimeFormatter ftf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    return ftf.format(LocalDateTime.ofInstant(Instant.ofEpochMilli(time), ZoneId.systemDefault()));
  }

  /**
   * 将Long类型的时间戳转换成String 类型的时间格式，时间格式为：yyyy-MM-dd HH:mm:ss.
   */
  public static String convertTimeToStringUtc(Long time) {
    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    return format.format(time);
  }

  /**
   * 将字符串转日期成Long类型的时间戳，格式为：yyyy-MM-dd HH:mm:ss.
   */
  public static Long convertLocalTimeToLongMs(String time) {
    DateTimeFormatter ftf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");
    LocalDateTime parse = LocalDateTime.parse("2018-05-29 13:52:50", ftf);
    return LocalDateTime.from(parse).atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
  }

  /**
   * 将Long类型的时间戳转换成String 类型的时间格式，时间格式为：yyyy-MM-dd HH:mm:ss.
   */
  public static String convertLocalTimeToStringMs(Long time) {
    DateTimeFormatter ftf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");
    return ftf.format(LocalDateTime.ofInstant(Instant.ofEpochMilli(time), ZoneId.systemDefault()));
  }

  /**
   * 将字符串转日期成Long类型的时间戳，格式为：yyyy-MM-dd HH:mm:ss.
   */
  public static Long convertTimeToLongMs(String time) {
    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
    Date date = null;
    try {
      date = format.parse(time);
    } catch (ParseException e) {
      logger.error("error msg:", e);
    }

    return date.getTime();
  }

  /**
   * 将字符串转日期成Long类型的时间戳.
   */
  public static Long convertTimeToLongMs(String time, String pattern) {
    SimpleDateFormat format = new SimpleDateFormat(pattern);
    Date date = null;
    try {
      date = format.parse(time);
    } catch (ParseException e) {
      logger.error("error msg:", e);
    }

    return date.getTime();
  }

  /**
   * 将Long类型的时间戳转换成String 类型的时间格式，时间格式为：yyyy-MM-dd HH:mm:ss.SSS.
   */
  public static String convertTimeToStringMs(Long time) {
    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
    return format.format(time);
  }

  /**
   * 将Long类型的时间戳转换成String 类型的时间格式，时间格式为：yyyy-MM-dd HH:mm:ss.
   */
  public static Date convertTimeToDate(String time) throws ParseException {
    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");

    return format.parse(time);
  }

  /**
   * 将Long类型的时间戳转换成String 类型的时间格式，时间格式为：yyyy-MM-dd HH:mm:ss.
   */
  public static String convertTimeToMonth(String time) {
    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM");
    return format.format(time);
  }

  /**
   * 将Long类型的时间戳精确精确到天.
   */
  public static Long alignMsToDay(Long time) {
    return ((time / 1000 / 3600 / 24) * 1000 * 3600 * 24);
  }

  /**
   * 将Long类型的时间戳精确精确到小时.
   */
  public static Long alignMsToHour(Long time) {
    return ((time / 1000 / 3600) * 1000 * 3600);
  }

  /**
   * 将Long类型的时间戳精确精确到小时.
   */
  public static Date processZoneDate(Date date) {

    Date calcData = date;
    int hour = date.getHours();
    if (hour < 8) {
      calcData = new Date(date.getTime() + 8 * 3600 * 1000);
    }

    return calcData;
  }

  /**
   * 时间戳转化成时间.
   */
  public static Date formatTime(Long time) throws ParseException {
    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    String strTime = format.format(time);
    Date date = format.parse(strTime);
    return date;
  }

  /**
   * 毫秒转为指定时区、指定格式时间.
   *
   * @param timestamp 时间戳
   * @param zoneId    时区
   * @param pattern   格式
   *
   * @return 指定格式时间
   */
  public static String formatTime(long timestamp, ZoneId zoneId,
                                  String pattern) {
    return DateTimeFormatter.ofPattern(pattern).format(LocalDateTime
        .ofInstant(Instant.ofEpochMilli(timestamp), zoneId));
  }

  /**
   * 时间转时区日期.
   *
   * @param time   时间
   * @param zoneId 时区
   *
   * @return 日期
   */
  public static Long convertTimeToZoneDate(Long time, ZoneId zoneId) {
    LocalDateTime localDateTime = LocalDateTime
        .ofInstant(Instant.ofEpochMilli(time), zoneId);
    Instant instant = localDateTime.toInstant(ZoneOffset.UTC);
    return Date.from(instant).getTime();
  }

  /**
   * 时间转时区日期.
   *
   * @param time   时间
   * @param targetPattern   时间格式
   * @param zoneId 时区
   *
   * @return 日期
   */
  public static String convertTimeToZoneDate(long time,
                                             String targetPattern, ZoneId zoneId) {
    LocalDateTime localDateTime = LocalDateTime
        .ofInstant(Instant.ofEpochMilli(time), zoneId);
    return localDateTime.format(DateTimeFormatter.ofPattern(targetPattern));
  }

  /**
   * 时间转时区日期.
   *
   * @param timeStr   时间
   * @param targetPattern   时间格式
   * @param zoneId 时区
   *
   * @return 日期
   */
  public static String convertTimeToZoneDate(String timeStr,
                                             String targetPattern, ZoneId zoneId) {
    DateTime dateTime = ISODateTimeFormat.dateTimeParser().parseDateTime(timeStr);
    LocalDateTime localDateTime = LocalDateTime
        .ofInstant(Instant.ofEpochMilli(dateTime.getMillis()), zoneId);
    return localDateTime.format(DateTimeFormatter.ofPattern(targetPattern));
  }

  /**
   * 按时区ID获取对应的小时数.
   *
   * @param time   时间
   * @param zoneId 时区
   *
   * @return 小时数
   */
  public static int getTimeZoneDateOfHour(long time, ZoneId zoneId) {
    LocalDateTime localDateTime = LocalDateTime
        .ofInstant(Instant.ofEpochMilli(time), zoneId);
    return localDateTime.getHour();
  }

  /**
   * 减时间.
   *
   * @param cal     cal
   * @param months  months
   * @param days    days
   * @param hours   hours
   * @param minutes minutes
   *
   * @return calendar
   */
  public static Calendar minusTime(Calendar cal, int months, int days, int hours, int minutes) {
    cal.add(Calendar.MONTH, -months);
    cal.add(Calendar.DAY_OF_MONTH, -days);
    cal.add(Calendar.HOUR_OF_DAY, -hours);
    cal.add(Calendar.MINUTE, -minutes);
    return cal;
  }

  /**
   * Gets epoch second from datetime.
   */
  public static long getEpochSecond(String datetimeStr, String pattern, String offsetId) {
    LocalDateTime localDateTime = LocalDateTime
        .parse(datetimeStr, DateTimeFormatter.ofPattern(pattern));
    return localDateTime.toEpochSecond(ZoneOffset.of(offsetId));
  }

}
