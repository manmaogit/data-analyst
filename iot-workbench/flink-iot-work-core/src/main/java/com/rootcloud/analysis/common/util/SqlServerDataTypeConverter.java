/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2020 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.common.util;

import com.google.common.base.Preconditions;
import com.rootcloud.analysis.core.enums.DataTypeEnum;
import com.rootcloud.analysis.core.enums.DataTypeInfoEnum;
import com.rootcloud.analysis.core.enums.SqlServerTypeEnum;

public class SqlServerDataTypeConverter {

  /**
   * Converts data type to unified data type.
   *
   * @param dataType source data type
   *
   * @return unified data type
   */
  public static DataTypeEnum convert2UnifiedDataType(String dataType) {
    String lowerCaseType = dataType.toLowerCase();
    String timeStampRegex = "^(datetime|datetimeoffset|smalldatetime|date|time).*$";
    if (lowerCaseType.matches(timeStampRegex)) {
      return DataTypeEnum.TIMESTAMP;
    }
    String integerRegex = "^(tinyint|smallint|int).*$";
    if (lowerCaseType.matches(integerRegex)) {
      return DataTypeEnum.INTEGER;
    }
    String longRegex = "^(bigint).*$";
    if (lowerCaseType.matches(longRegex)) {
      return DataTypeEnum.LONG;
    }
    String numberRegex =
        "^(numeric|smallmoney|money|real|float|decimal).*$";
    if (lowerCaseType.matches(numberRegex)) {
      return DataTypeEnum.NUMBER;
    }
    String booleanRegex = "^(bit).*$";
    if (lowerCaseType.matches(booleanRegex)) {
      return DataTypeEnum.BOOLEAN;
    }
    return DataTypeEnum.STRING;
  }


  /**
   * Converts data type to flink data type.
   *
   * @param dataType source data type
   *
   * @return unified data type
   */
  public static DataTypeInfoEnum convert2TypeInfo(String dataType) {
    SqlServerTypeEnum sqlServerType = null;
    for (SqlServerTypeEnum type : SqlServerTypeEnum.values()) {
      if (type.matches(dataType)) {
        sqlServerType = type;
        break;
      }
    }
    if (sqlServerType == null) {
      return null;
    }
    return sqlServerType.getDataTypeInfoEnumBuilder().build(dataType);
  }


  /**
   * Converts mysql type to flink sql type.
   */
  public static String convert2FlinkSqlType(String sqlServerType) {
    SqlServerTypeEnum sqlServerTypeEnum = null;
    for (SqlServerTypeEnum value : SqlServerTypeEnum.values()) {
      if (value.matches(sqlServerType.toUpperCase())) {
        sqlServerTypeEnum = value;
      }
    }
    Preconditions.checkNotNull(sqlServerTypeEnum, "Invalid SqlServer type." + sqlServerType);
    return sqlServerTypeEnum.getFlinkSqlTypeBuilder().build(sqlServerType);
  }
}