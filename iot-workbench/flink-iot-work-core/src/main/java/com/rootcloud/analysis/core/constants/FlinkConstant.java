/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2020 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.core.constants;

public class FlinkConstant {

  public static final String JOB_DETAIL_URL = "/jobs/{jid}";

  public static final String FLINK_JARS_URL = "/jars";

  public static final String FLINK_RUN_JAR_URL = "/jars/{jarId}/run";

  public static final String FLINK_UPLOAD_JAR_URL = "/jars/upload";

  public static final String FLINK_CANCEL_JOB_URL = "/jobs/{jid}/?mode=cancel";

  public static final String FLINK_JOB_OVERVIEW_URL = "/jobs/overview";


}
