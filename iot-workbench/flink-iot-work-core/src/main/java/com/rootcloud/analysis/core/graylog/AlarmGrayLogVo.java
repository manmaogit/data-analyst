/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2020 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.core.graylog;

import com.alibaba.fastjson.annotation.JSONField;
import java.text.SimpleDateFormat;
import java.util.TimeZone;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AlarmGrayLogVo {

  private static final SimpleDateFormat SDF = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");

  static {
    SDF.setTimeZone(TimeZone.getTimeZone("Asia/Shanghai"));
  }

  /**
   * 时间字段，  iso8601，例如： 2020-05-01T17:30:01.927Z.
   */
  @JSONField(name = "T")
  private String data;
  /**
   * P字段区分具体业务，如云视界、还是平台4.0，相同业务下的所有微服务应使用相同P字段.
   */
  @JSONField(name = "P")
  private String platform;
  /**
   * 注意：审计日志为 Audit-S（审计日志不做告警）.
   * S字段为具体服务，一般搜索日志时，使用对应环境的stream+S字段值进行搜索.
   */
  @JSONField(name = "S")
  private String k8sServiceName;//内部服务名：K8S上服务名称  必填
  /**
   * 注意：审计日志为 Audit-B （审计日志不做告警）.
   * B字段用途主要是用于描述业务模块.
   */
  @JSONField(name = "B")
  private String module;//模块名称：接入与建模、数据计算IoTWorks、应用等  必填
  private Integer statusCode;    //http状态码
  @JSONField(name = "short_message")
  private String shortMessage; //操作详情（detail）必填
  @JSONField(name = "L")
  private String logLevel;//日志级别  必填

  public AlarmGrayLogVo() {
    setData(System.currentTimeMillis());
  }


  public void setData(Long time) {
    this.data = SDF.format(time);
  }

}
