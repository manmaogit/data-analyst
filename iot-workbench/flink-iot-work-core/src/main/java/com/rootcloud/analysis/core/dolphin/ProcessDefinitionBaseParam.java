/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */


package com.rootcloud.analysis.core.dolphin;

import java.util.List;
import java.util.Map;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ProcessDefinitionBaseParam implements ProcessDefinitionTask {

  private String id;

  private String type;

  private String name;

  private String runFlag;

  private String description;

  @Deprecated
  private Integer workerGroupId;

  private String workerGroup;

  private String taskInstancePriority;

  private Timeout timeout;

  private String maxRetryTimes;

  private String retryInterval;

  private List<String> preTasks;

  private Dependence dependence;

  /**
   * dolphin官方就是这么生成task的id,直接拿过来.
   */
  public void randomId() {
    String randomId = "tasks-" + (int)(Math.random() * 100000);
    this.id = randomId;
  }


}
