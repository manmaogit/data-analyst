/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2020 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.core.log.graylog;

import ch.qos.logback.classic.PatternLayout;
import ch.qos.logback.classic.spi.ILoggingEvent;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.rootcloud.analysis.core.constants.GrayLogConstant;
import com.rootcloud.analysis.core.graylog.GrayLogVo;
import com.rootcloud.analysis.core.graylog.LogVo;
import de.siegmar.logbackgelf.GelfEncoder;
import java.nio.charset.StandardCharsets;

public class GrayLogEncoder extends GelfEncoder {

  /**
   * Short message format. Default: `"%m%nopex"`.
   */
  private PatternLayout shortPatternLayout;

  @Override
  public PatternLayout getShortPatternLayout() {
    return shortPatternLayout;
  }

  @Override
  public void setShortPatternLayout(PatternLayout shortPatternLayout) {
    this.shortPatternLayout = shortPatternLayout;
  }

  @Override
  public byte[] encode(final ILoggingEvent event) {
    final String shortMessage = shortPatternLayout.doLayout(event);
    GrayLogVo iamLogVo = convertLogToGrayLogVo(JSONObject.parseObject(shortMessage, LogVo.class));
    return JSON.toJSONString(iamLogVo).getBytes(StandardCharsets.UTF_8);
  }

  /**
   * 转换为GrayLogVo.
   */
  public static GrayLogVo convertLogToGrayLogVo(LogVo logVo) {
    GrayLogVo grayLogVo = GrayLogVo.builder()
        .client(logVo.getClient())
        .userId(logVo.getUserId())
        .ip(logVo.getIp())
        .k8sServiceName(GrayLogConstant.K8S_SERVICE_NAME)
        .shortMessage(logVo.getShortMessage())
        .module(logVo.getModule())
        .operation(logVo.getOperation())
        .action(logVo.getAction())
        .operateObjectName(logVo.getOperateObjectName())
        .operateObject(logVo.getOperateObject())
        .operateObjectType(logVo.getOperateObjectType())
        .userName(logVo.getUserName())
        .tenantId(logVo.getTenantId())
        .requestId(logVo.getRequestId())
        .result(logVo.getResult())
        .logLevel(logVo.getLogLevel())
        .logType(logVo.getLogType())
        .build();
    return grayLogVo;
  }


}
