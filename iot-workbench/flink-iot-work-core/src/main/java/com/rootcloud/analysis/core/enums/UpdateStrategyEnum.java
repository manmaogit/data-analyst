/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.core.enums;

public enum UpdateStrategyEnum {
  /**
   * 总是替换.
   */
  REPLACE,
  ACCUMULATE, GREATEST, LEAST, JSON_ACCUMULATE, JSON_REPLACE, JSON_UNDO,
  /**
   * 非空时才替换.
   */
  REPLACE_WITH_NON_NULL

}
