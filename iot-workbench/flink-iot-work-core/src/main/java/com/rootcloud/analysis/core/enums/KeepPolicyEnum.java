/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.core.enums;

/**
 * Created by 王纯超 on 2021/8/31.
 * Specify which record to keep under given order.
 */
public enum KeepPolicyEnum {
  FIRST,
  LAST
}
