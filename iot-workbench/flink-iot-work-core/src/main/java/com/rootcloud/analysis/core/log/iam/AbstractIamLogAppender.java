/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2020 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.core.log.iam;

import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.Context;
import ch.qos.logback.core.UnsynchronizedAppenderBase;
import ch.qos.logback.core.status.ErrorStatus;
import java.io.IOException;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;


@Getter
@Setter
public abstract class AbstractIamLogAppender extends UnsynchronizedAppenderBase<ILoggingEvent> {


  private IamLogEncoder encoder;


  @Override
  public final void start() {
    if (encoder == null) {
      encoder = new IamLogEncoder();
      encoder.setContext(getContext());
      encoder.start();
    }
    init();
    if (encoder.isAppendNewline()) {
      addError("Newline separator must not be enabled in layout");
      return;
    }

    try {
      startAppender();

      super.start();
    } catch (final Exception e) {
      addError("Couldn't start appender", e);
    }
  }

  /**
   * 用于初始化组件.
   */
  public void init() {
  }


  public void addError(String msg) {
    this.addStatus(new ErrorStatus(msg, this.getDeclaredOrigin()));
  }

  public Context getContext() {
    return this.context;
  }

  protected void startAppender() throws IOException {
  }

  @SuppressWarnings("checkstyle:illegalcatch")
  @Override
  protected void append(final ILoggingEvent event) {
    final byte[] binMessage = encoder.encode(event);

    try {
      appendMessage(binMessage);
    } catch (final Exception e) {
      // Could be IOException or some kind of RuntimeException
      addError("Error sending GELF message", e);
    }
  }

  @Override
  public void stop() {
    super.stop();
    try {
      close();
    } catch (final IOException e) {
      addError("Couldn't close appender", e);
    }
  }

  protected abstract void close() throws IOException;

  protected abstract void appendMessage(byte[] messageToSend) throws IOException;
}
