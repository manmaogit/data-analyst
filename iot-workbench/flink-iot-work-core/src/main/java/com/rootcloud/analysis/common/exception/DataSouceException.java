/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2020 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.common.exception;

import com.rootcloud.analysis.core.exception.RcException;

public class DataSouceException extends RcException {

  public DataSouceException(int code, String message) {
    super(code, message);
  }
}