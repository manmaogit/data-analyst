/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.core.enums;

public enum ExecuteTypeEnum {

  /**
   * 操作类型
   * 1.重跑 2.恢复暂停 3.恢复失败 4.停止 5.暂停.
   */
  NONE,
  REPEAT_RUNNING,
  RECOVER_SUSPENDED_PROCESS,
  START_FAILURE_TASK_PROCESS,
  STOP,
  PAUSE;

  /**
   * getEnum.
   * @param value 1.重跑 2.恢复暂停 3.恢复失败 4.停止 5.暂停.
   * @return
   */
  public static ExecuteTypeEnum getEnum(int value) {
    for (ExecuteTypeEnum e : ExecuteTypeEnum.values()) {
      if (e.ordinal() == value) {
        return e;
      }
    }
    return null;//For values out of enum scope.
  }
}
