/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.core.enums;

import java.util.Arrays;
import java.util.List;
import lombok.Getter;

/**
 * 历史数据节点（目前包含InfluxDB和TdEngine）类型与Flink与类型的映射关系.
 * @CreatedBy: zexin.huang
 * @Date: 2021/10/22 3:51 下午
 */
@Getter
public enum HistorianDataTypeEnum {

  Float("f", "DECIMAL,FLOAT,DOUBLE"),
  Boolean("b", "BOOLEAN"),
  Integer("i", "INT"),
  String("s", "STRING"),
  Json("j", "JSON"),
  ;

  private String historianType;
  private String flinkType;

  HistorianDataTypeEnum(String historianType, String flinkType) {
    this.historianType = historianType;
    this.flinkType = flinkType;
  }

  /**
   * getHistorianType.
   * @param flinkType flink类型.
   * @return
   */
  public static String getHistorianType(String flinkType) {
    for (HistorianDataTypeEnum typeEnum : HistorianDataTypeEnum.values()) {
      List types = Arrays.asList(typeEnum.flinkType.split(","));
      if (types.contains(flinkType)) {
        return typeEnum.historianType;
      }
    }
    return "";
  }

  /**
   * isTypeEquals.
   */
  public static boolean isTypeEquals(String sourceInfluxDbType, String targetFlinkType) {
    for (HistorianDataTypeEnum typeEnum : HistorianDataTypeEnum.values()) {
      if (typeEnum.historianType.equals(sourceInfluxDbType)) {
        List types = Arrays.asList(typeEnum.flinkType.split(","));
        if (types.contains(targetFlinkType)) {
          return true;
        }
      }
    }
    return false;
  }

  /**
   * 判断是否主要的数据类型.
   * 如果是的话，即使是HistorianIgnoreTagType，Historian落库时也会加$类型后缀.
   */
  public static boolean isPrimitiveType(String flinkDataType) {
    for (HistorianDataTypeEnum typeEnum : HistorianDataTypeEnum.values()) {
      if (typeEnum == Float || typeEnum == Boolean
          || typeEnum == Integer || typeEnum == String) {
        List types = Arrays.asList(typeEnum.flinkType.split(","));
        if (types.contains(flinkDataType)) {
          return true;
        }
      }
    }
    return false;
  }
}
