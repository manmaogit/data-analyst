/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2020 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.core.enums;

/**
 * 统一应用对应的英文，模块名称.
 * http://confluence.irootech.com:8090/pages/viewpage.action?pageId=245629352
 */
public enum GragLogModuleEnum {
  CONSOLE("CONSOLE"),
  IAM("IAM"),
  CMS("CMS"),
  IOTWORKS("IOTWORKS"),
  ADS("ADS"),
  OME("OME"),
  ROOTIDE("ROOTTIDE"),
  IOTVIS("IOT VIS"),
  EAM("EAM");

  private String moduleValue;

  private GragLogModuleEnum(String moduleValue) {
    this.moduleValue = moduleValue;
  }

  public String getModuleValue() {
    return moduleValue;
  }

  public void setModuleValue(String moduleValue) {
    this.moduleValue = moduleValue;
  }
}
