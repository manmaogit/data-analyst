/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.core.dag;

import java.util.ArrayList;
import java.util.List;
import lombok.Builder;
import lombok.Data;
import lombok.Singular;

/**
 * RC DAG图.
 *
 * <p>RC DAG图由节点{@link RcNode}和边{@link RcEdge}组合而成.在此提供对DAG图的属性与行为的操作.
 * 创建一个RC DAG图参见{@link RcGraph.Builder}
 *
 * <P>RC DAG图中的节点分为Source Node、Process Node和Sink Node.
 * <pre>
 *    Source Node:加载数据源中的数据
 *    Process Node:转换、处理数据
 *    Sink Node:持久化数据到数据源
 * </pre>
 *
 */
@Data
@Builder
public class RcGraph {
  @Singular private final List<RcNode> sourceNodes = new ArrayList<>(2);
  @Singular private final List<RcNode> processNodes = new ArrayList<>(16);
  @Singular private final List<RcNode> sinkNodes = new ArrayList<>(2);
  @Singular private final List<RcEdge> edges = new ArrayList<>(16);
}
