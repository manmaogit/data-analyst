/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.core.constants;

public class SchedulerConstant {

  public static final String CODE = "code";

  public static final String DATA = "data";

  public static final String MSG = "msg";

  public static final int RELEASE_STATE_CODE = 1;

  public static final String ID = "id";

  public static final String PROCESS_DEFINITION_ID = "processDefinitionId";

  public static final String PROCESS_INSTANCE_ID = "processInstanceId";

  public static final String STATE = "state";

  public static final String JOB_ID = "job_id";

  public static final String JOB_NAME = "job_name";

  public static final String CYCLE_MODE = "cycle_mode";

  public static final String BATCH_PROJECT_PREFIX = "batch_";

  public static final String WORKFLOW_PROJECT_PREFIX = "workflow_";

  public static final String FLINK_LOG_PREFIX = "Job has been submitted with JobID ";
}