/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2020 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.core.graylog;

import com.alibaba.fastjson.annotation.JSONField;
import com.rootcloud.analysis.core.enums.GraylogLogTypeEnum;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class GrayLogVo {

  private String userName;
  private String userRole;
  @JSONField(name = "Audit-S")
  private String k8sServiceName;//内部服务名：K8S上服务名称  必填
  @JSONField(name = "Audit-B")
  private String module;//模块名称：接入与建模、数据计算IoTWorks、应用等  必填
  private String operateObject;//操作对象：IoTWorks任务ID、IoTWorks模板 外部数据源ID
  private String client;//用户使用的客户端类型：（浏览器类型、APP、小程序）
  private String ip;//地址
  private String requestId;//请求唯一id （当前系统时间戳或者UUID）
  private String tenantId;//tenantId 租户id
  private String userId;//userId   用户id
  private String operateObjectName;//operateObjectName   操作对象名称：比如设备名称，模板名，模型名，任务流名等
  private String operation;//operation  操作项
  private String operateObjectType;//操作对象类型
  private String detail;//detail   操作详情
  private String result; //result    操作结果反馈  SUCCESS | FAIL
  private String reserved1;//reserved1  保留字1
  private String reserved2;//reserved2   保留字2
  @JSONField(name = "short_message")
  private String shortMessage; //操作详情（detail）必填
  @JSONField(name = "L")
  private String logLevel;//日志级别  必填
  private GraylogLogTypeEnum logType;
  private String action; //
  private String organizationId;

}
