/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2020 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.core.enums;

import java.time.ZoneId;

import lombok.Getter;

@Getter
public enum TimeZoneTypeEnum {
  //国际换日线
  IDLW("UTC-12:00"),
  //美属萨摩亚标准时间
  SST("UTC-11:00"),
  //夏威夷标准时间
  HST("UTC-10:00"),
  //马克萨斯群岛标准时间
  MIT("UTC-09:30"),
  //阿拉斯加标准时间
  AST("UTC-09:00"),
  //北美太平洋标准时间
  PST("UTC-08:00"),
  //北美山区标准时间
  PNT("UTC-07:00"),
  //西部山脉标准时间
  MST("UTC-07:00"),
  //北美中部标准时间
  CST("UTC-06:00"),
  //东部标准时间
  EST("UTC-05:00"),
  //东部标准时间
  IET("UTC-05:00"),
  //大西洋标准时间
  PRT("UTC-04:00"),
  //纽芬兰标准时间
  CNT("UTC-03:30"),
  //阿根廷标准时间
  AGT("UTC-03:00"),
  //巴西利亚标准时间
  BET("UTC-03:00"),
  //费尔南多·迪诺罗尼亚群岛标准时间
  FNT("UTC-02:00"),
  //佛得角标准时间
  CVT("UTC-01:00"),
  //欧洲西部时区
  UTC("UTC+00:00"),
  //中欧标准时间
  ECT("UTC+01:00"),
  //中部非洲时间
  CAT("UTC+02:00"),
  //东欧标准时间
  ART("UTC+02:00"),
  //东部非洲时间
  EAT("UTC+03:00"),
  //伊朗标准时间
  IRST("UTC+03:30"),
  //亚美尼亚标准时间
  NET("UTC+04:00"),
  //阿富汗标准时间
  AFT("UTC+04:30"),
  //巴基斯坦标准时间
  PLT("UTC+05:00"),
  //印度时间
  IST("UTC+05:30"),
  //尼泊尔标准时间
  NPT("UTC+05:45"),
  //孟加拉标准时间
  BST("UTC+06:00"),
  //缅甸标准时间
  MMT("UTC+06:30"),
  //越南时间
  VST("UTC+07:00"),
  //中国标准时间
  CTT("UTC+08:00"),
  //日本标准时间
  JST("UTC+09:00"),
  //澳大利亚中部标准时间
  ACT("UTC+09:30"),
  //澳大利亚东部标准时间
  AET("UTC+10:00"),
  //豪勋爵群岛标准时间
  LHST("UTC+10:30"),
  //所罗门群岛时间
  VUT("UTC+11:00"),
  //新西兰标准时间
  NST("UTC+12:00"),
  //查塔姆群岛标准时间
  CHAST("UTC+12:45"),
  //菲尼克斯群岛标准时间
  PHOT("UTC+13:00"),
  //莱恩群岛标准时间
  LINT("UTC+14:00");

  private String timeZoneValue;

  TimeZoneTypeEnum(String timeZoneValue) {
    this.timeZoneValue = timeZoneValue;
  }

  public ZoneId getZoneId() {
    return ZoneId.of(getTimeZoneValue());
  }

}

