/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.core.enums;

public enum CommandTypeEnum {

  /**
   * command types
   * 0 start a new process
   * 1 start a new process from current nodes
   * 2  recover tolerance fault work flow
   * 3 start process from paused task nodes
   * 4 start process from failure task nodes
   * 5 complement data
   * 6 start a new process from Scheduler
   * 7 repeat running a work flow
   * 8 pause a process
   * 9 stop a process
   * 10 recover waiting thread.
   */
  START_PROCESS,
  START_CURRENT_TASK_PROCESS,
  RECOVER_TOLERANCE_FAULT_PROCESS,
  RECOVER_SUSPENDED_PROCESS,
  START_FAILURE_TASK_PROCESS,
  COMPLEMENT_DATA,
  SCHEDULER,
  REPEAT_RUNNING,
  PAUSE,
  STOP,
  RECOVER_WAITTING_THREAD
}
