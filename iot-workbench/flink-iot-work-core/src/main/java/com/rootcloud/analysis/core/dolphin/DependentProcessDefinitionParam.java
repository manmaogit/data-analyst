/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.core.dolphin;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import java.util.Map;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DependentProcessDefinitionParam extends ProcessDefinitionBaseParam {

  private ConditionResult conditionResult;

  private Map params;

  /**
   * 设置默认值.
   */
  public void setDefaultValue(String jobId) {
    super.randomId();
    this.setName(jobId + "-依赖节点");
    this.setType("DEPENDENT");
    this.setParams(Maps.newHashMap());
    this.setRunFlag("NORMAL");
    this.setWorkerGroup("default");
    this.setTaskInstancePriority("MEDIUM");
    this.setDescription("");
    ConditionResult conditionResult = new ConditionResult();
    conditionResult.setFailedNode(Lists.newArrayList());
    conditionResult.setSuccessNode(Lists.newArrayList());
    this.setConditionResult(conditionResult);
  }
}
