/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.core.enums;

import com.rootcloud.analysis.core.data.type.ITypeBuilder;
import java.util.regex.Pattern;
import lombok.Getter;

@Getter
public enum PostgreTypeEnum {

  INT(Pattern.compile("^INT[2,4]?\\s*(\\(\\s*\\d+\\s*(,\\s*\\d+\\s*)?\\))?\\s*$"), param -> "INT"),
  BIGINT(Pattern.compile("^INT[8]?\\s*(\\(\\s*\\d+\\s*(,\\s*\\d+\\s*)?\\))?\\s*$"),
      param -> "BIGINT"),
  SERIAL(Pattern.compile("^SERIAL\\s*(\\(\\s*\\d+\\s*(,\\s*\\d+\\s*)?\\))?\\s*$"), param -> "INT"),
  BIGSERIAL(Pattern.compile("^BIGSERIAL\\s*(\\(\\s*\\d+\\s*(,\\s*\\d+\\s*)?\\))?\\s*$"),
      param -> "BIGINT"),
  FLOAT4(Pattern.compile("^FLOAT4\\s*(\\(\\s*\\d+\\s*(,\\s*\\d+\\s*)?\\))?\\s*$"),
      param -> "FLOAT"),
  FLOAT8(Pattern.compile("^FLOAT8\\s*(\\(\\s*\\d+\\s*(,\\s*\\d+\\s*)?\\))?\\s*$"),
      param -> "DOUBLE"),
  NUMERIC(Pattern.compile("^NUMERIC\\s*(\\(\\s*\\d+\\s*(,\\s*\\d+\\s*)?\\))?\\s*$"),
      param -> "DECIMAL" + param.substring(7)),
  BOOL(Pattern.compile("^BOOL\\s*(\\(\\s*\\d+\\s*\\))?\\s*$"), param -> "BOOLEAN"),
  DATE(Pattern.compile("^DATE\\s*(\\(\\s*\\d+\\s*\\))?\\s*$"), param -> "DATE"),
  TIME(Pattern.compile("^TIME\\s*(\\(\\s*\\d+\\s*\\))?\\s*$"), param -> "TIME "),
  TIMESTAMP(Pattern.compile("^TIMESTAMP\\s*(\\(\\s*\\d+\\s*\\))?\\s*$"), param -> "TIMESTAMP "),
  CHARACTER(Pattern.compile("^CHARACTER\\s*(\\(\\s*\\d+\\s*\\))?\\s*$"), param -> "STRING"),
  BPCHAR(Pattern.compile("^BPCHAR\\s*(\\(\\s*\\d+\\s*\\))?\\s*$"), param -> "STRING"),
  VARCHAR(Pattern.compile("^VARCHAR\\s*(\\(\\s*\\d+\\s*\\))?\\s*$"), param -> "STRING"),
  TEXT(Pattern.compile("^TEXT\\s*(\\(\\s*\\d+\\s*\\))?\\s*$"), param -> "STRING"),
  BYTEA(Pattern.compile("^BYTEA\\s*(\\(\\s*\\d+\\s*\\))?\\s*$"), param -> "BYTES");

  private Pattern pattern;

  private ITypeBuilder<String> flinkSqlTypeBuilder;

  PostgreTypeEnum(Pattern pattern, ITypeBuilder flinkSqlTypeBuilder) {
    this.pattern = pattern;
    this.flinkSqlTypeBuilder = flinkSqlTypeBuilder;
  }

  public boolean matches(String input) {
    return pattern.matcher(input.toUpperCase()).matches();
  }


  /**
   * 是否包含以上枚举类型.
   */
  public static Boolean contains(String dataType) {
    for (PostgreTypeEnum type : PostgreTypeEnum.values()) {
      if (type.matches(dataType)) {
        return true;
      }
    }
    return false;
  }

}
