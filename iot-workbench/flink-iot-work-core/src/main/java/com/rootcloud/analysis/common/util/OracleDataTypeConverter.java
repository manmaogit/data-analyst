/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2020 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.common.util;

import com.rootcloud.analysis.core.enums.DataTypeEnum;
import com.rootcloud.analysis.core.enums.DataTypeInfoEnum;

public class OracleDataTypeConverter {

  /**
   * Converts data type to unified data type.
   *
   * @param dataType source data type
   *
   * @return unified data type
   */
  public static DataTypeEnum convert2UnifiedDataType(String dataType) {
    String lowerCaseType = dataType.toLowerCase();
    String upperCaseType = dataType.toUpperCase();
    String timeStampRegex = "^(timestamp|date).*$";
    if (lowerCaseType.matches(timeStampRegex) || upperCaseType.matches(timeStampRegex)) {
      return DataTypeEnum.TIMESTAMP;
    }
    String integerRegex = "^(RAWID|INTEGER|INT|SMALLINT).*$";
    if (lowerCaseType.matches(integerRegex) || upperCaseType.matches(integerRegex)) {
      return DataTypeEnum.INTEGER;
    }
    String numberRegex =
        "^(NUMERIC|DECIMAL|NUMBER|FLOAT|DOUBLE PRECISIOON|REAL|LONG|BINARY_FLOAT|BINARY_DOUBLE).*$";
    if (lowerCaseType.matches(numberRegex) || upperCaseType.matches(numberRegex)) {
      return DataTypeEnum.NUMBER;
    }
    String booleanRegex = "^(BOOL|BIT).*$";
    if (lowerCaseType.matches(booleanRegex) || upperCaseType.matches(booleanRegex)) {
      return DataTypeEnum.BOOLEAN;
    }
    return DataTypeEnum.STRING;
  }


  /**
   * Converts data type to flink data type.
   *
   * @param dataType source data type
   *
   * @return unified data type
   */
  public static DataTypeInfoEnum convert2TypeInfo(String dataType) {
    String upperCaseType = dataType.toUpperCase();

    String tinyIntRegex = "^NUMBER\\s*(\\(\\s*[12]{1}\\s*,\\s*0\\s*\\)){1}\\s*$\"";
    if (upperCaseType.matches(tinyIntRegex)) {
      return DataTypeInfoEnum.TINYINT;
    }

    String boolRegex = "^NUMBER\\s*(\\(\\s*1\\s*,\\s*0\\s*\\)){1}\\s*$|"
        + "^NUMBER\\s*(\\(\\s*1\\s*\\)){1}\\s*$";
    if (upperCaseType.matches(boolRegex)) {
      return DataTypeInfoEnum.BOOLEAN;
    }

    String smallIntRegex = "^NUMBER\\s*(\\(\\s*[34]{1}\\s*,\\s*0\\s*\\)){1}\\s*$|"
        + "^\\s*SMALLINT\\s*$";
    if (upperCaseType.matches(smallIntRegex)) {
      return DataTypeInfoEnum.SMALLINT;
    }
    String intRegex = "^NUMBER\\s*(\\(\\s*"
        + "([56789]{1})\\s*,\\s*0\\s*\\)){1}\\s*$|"
        + "^\\s*INTEGER\\s*$|^\\s*INT\\s*$";
    if (upperCaseType.matches(intRegex)) {
      return DataTypeInfoEnum.INT;
    }

    String bigIntRegex = "^NUMBER\\s*(\\(\\s*((10)|(1[12345678]{1}))\\s*,"
        + "\\s*0\\s*\\)){1}\\s*$|"
        + " ^INTERVAL DAY TO SECOND$|^INTERVAL YEAR TO MONTH$";
    if (upperCaseType.matches(bigIntRegex)) {
      return DataTypeInfoEnum.BIGINT;
    }
    String decimalRegex = "^DECIMAL\\s*(\\(\\s*\\d+\\s*,\\s*\\d\\s*\\))?\\s*$|"
        + "^NUMBER\\s*(\\(\\s*([23]{1}[^9]{1}|29|19)\\s*,\\s*0\\s*\\))?\\s*$|"
        + "^NUMBER\\s*(\\(\\s*\\d+\\s*,\\s*[^0]+\\s*\\))?\\s*$|^DECIMAL$|^REAL$";
    if (upperCaseType.matches(decimalRegex)) {
      return DataTypeInfoEnum.DECIMAL;
    }

    String floatRegex = "^FLOAT\\s*(\\(\\s*\\d+\\s*\\))?\\s*$|^BINARY_FLOAT$";
    if (upperCaseType.matches(floatRegex)) {
      return DataTypeInfoEnum.FLOAT;
    }
    String doubleRegex = "^DOUBLE\\s+PRECISION$|^BINARY_DOUBLE$";
    if (upperCaseType.matches(doubleRegex)) {
      return DataTypeInfoEnum.DOUBLE;
    }

    String timestampRegex = "^TIMESTAMP\\s*(\\(\\s*\\d+\\s*\\))?\\s*$"
            + "|^DATE\\s*(\\(\\s*\\d+\\s*\\))?\\s*$|"
        + "^TIMESTAMP\\s*(\\(\\s*\\d+\\s*\\))?\\s+WITH\\s+LOCAL\\s+TIME\\s+"
        + "ZONE$|^TIMESTAMP\\s*(\\(\\s*\\d+\\s*\\))?\\s+WITH\\s+TIME\\s+ZONE$";
    if (upperCaseType.matches(timestampRegex)) {
      return DataTypeInfoEnum.TIMESTAMP;
    }
    String stringRegex = "^CHAR$|^CHAR\\s+VARYING$|^CHARACTER$|^CHARACTER\\s+VARYING$|"
        + "^VARCHAR$|^VARCHAR2$|^NVARCHAR2$|^LONG$|^LONG\\s+RAW$|^LONG\\s+VARCHAR$|"
        + "^NCHAR$|^NCHAR\\s+VARYING$|^NUMBER\\s*(\\(\\s*\\d{2,}\\s*,\\s*0\\s*\\))\\s*$";
    if (upperCaseType.matches(stringRegex)) {
      return DataTypeInfoEnum.STRING;
    }

    return null;
  }
}