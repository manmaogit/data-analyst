/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2020 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.core.enums;

import com.google.common.base.Preconditions;
import com.rootcloud.analysis.core.constants.CommonConstant;
import com.rootcloud.analysis.core.constants.JobConstant;
import com.rootcloud.analysis.core.data.IDataBuilder;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.Date;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;

@Getter
public enum DataTypeEnum {
  STRING(scale -> "BasicTypeInfo.STRING_TYPE_INFO", "VARCHAR (50)", obj
      -> {
    if (obj == null) {
      return null;
    }
    if (obj instanceof String) {
      return obj;
    } else {
      return obj.toString();
    }
  }
  ),
  CHAR(scale -> "BasicTypeInfo.CHAR_TYPE_INFO", "CHAR(50)", obj -> obj == null ? null : obj),

  VARCHAR(scale -> "BasicTypeInfo.STRING_TYPE_INFO", "VARCHAR (50)", obj
      -> {
    if (obj == null) {
      return null;
    }
    if (obj instanceof String) {
      return obj;
    } else {
      return obj.toString();
    }
  }
  ),
  TEXT(scale -> "BasicTypeInfo.STRING_TYPE_INFO", "TEXT", obj
      -> {
    if (obj == null) {
      return null;
    }
    if (obj instanceof String) {
      return obj;
    } else {
      return obj.toString();
    }
  }
  ),
  TINYTEXT(scale -> "BasicTypeInfo.STRING_TYPE_INFO", "TINYTEXT", obj
      -> {
    if (obj == null) {
      return null;
    }
    if (obj instanceof String) {
      return obj;
    } else {
      return obj.toString();
    }
  }
  ),
  MEDIUMTEXT(scale -> "BasicTypeInfo.STRING_TYPE_INFO", "MEDIUMTEXT", obj
      -> {
    if (obj == null) {
      return null;
    }
    if (obj instanceof String) {
      return obj;
    } else {
      return obj.toString();
    }
  }
  ),
  LONGTEXT(scale -> "BasicTypeInfo.STRING_TYPE_INFO", "LONGTEXT", obj
      -> {
    if (obj == null) {
      return null;
    }
    if (obj instanceof String) {
      return obj;
    } else {
      return obj.toString();
    }
  }
  ),
  BOOLEAN(scale -> "BasicTypeInfo.BOOLEAN_TYPE_INFO", "BOOLEAN", obj -> {
    if (obj == null) {
      return null;
    }
    if (obj instanceof Boolean) {
      return obj;
    }
    if (obj instanceof Number) {
      return ((Number) obj).intValue() == 1;
    }
    if (obj instanceof String) {
      if ("1".equals(obj)) {
        return Boolean.TRUE;
      } else if ("0".equals(obj)) {
        return Boolean.FALSE;
      } else if ("false".equals(obj)) {
        return Boolean.FALSE;
      } else if ("true".equals(obj)) {
        return Boolean.TRUE;
      } else {
        return new Boolean((String) obj);
      }
    }
    return new Boolean(obj.toString());
  }),
  BYTE(scale -> "BasicTypeInfo.BYTE_TYPE_INFO", "TINYINT", obj -> {
    if (obj == null) {
      return null;
    }
    if (obj instanceof Byte) {
      return obj;
    }
    if (obj instanceof Short) {
      return ((Short) obj).byteValue();
    }
    if (obj instanceof Integer) {
      return ((Integer) obj).byteValue();
    }
    if (obj instanceof Long) {
      return ((Long) obj).byteValue();
    }
    if (obj instanceof Float) {
      return ((Float) obj).byteValue();
    }
    if (obj instanceof Double) {
      return ((Double) obj).byteValue();
    }
    if (obj instanceof BigInteger) {
      return ((BigInteger) obj).byteValue();
    }
    if (obj instanceof BigDecimal) {
      return ((BigDecimal) obj).byteValue();
    }
    return obj instanceof String ? Byte.parseByte((String) obj) : Byte.parseByte(obj.toString());
  }),
  TINYINT(scale -> "BasicTypeInfo.BYTE_TYPE_INFO", "TINYINT(1)", obj -> {
    if (obj == null) {
      return null;
    }
    if (obj instanceof Boolean) {
      return obj;
    }
    return obj instanceof String ? new Boolean((String) obj) : new Boolean(obj.toString());
  }),
  SHORT(scale -> "BasicTypeInfo.SHORT_TYPE_INFO", "SMALLINT", obj -> {
    if (obj == null) {
      return null;
    }
    if (obj instanceof Short) {
      return obj;
    }
    if (obj instanceof Byte) {
      return ((Byte) obj).shortValue();
    }
    if (obj instanceof Integer) {
      return ((Integer) obj).shortValue();
    }
    if (obj instanceof Long) {
      return ((Long) obj).shortValue();
    }
    if (obj instanceof Float) {
      return ((Float) obj).shortValue();
    }
    if (obj instanceof Double) {
      return ((Double) obj).shortValue();
    }
    if (obj instanceof BigInteger) {
      return ((BigInteger) obj).shortValue();
    }
    if (obj instanceof BigDecimal) {
      return ((BigDecimal) obj).shortValue();
    }
    return obj instanceof String ? Short.parseShort((String) obj)
        : Short.parseShort(obj.toString());
  }),
  SMALLINT(scale -> "BasicTypeInfo.INT_TYPE_INFO", "SMALLINT", obj -> {
    if (obj == null) {
      return null;
    }
    if (obj instanceof Integer) {
      return obj;
    }
    if (obj instanceof Byte) {
      return ((Byte) obj).intValue();
    }
    if (obj instanceof Short) {
      return ((Short) obj).intValue();
    }
    if (obj instanceof Long) {
      return ((Long) obj).intValue();
    }
    if (obj instanceof Float) {
      return ((Float) obj).intValue();
    }
    if (obj instanceof Double) {
      return ((Double) obj).intValue();
    }
    if (obj instanceof BigInteger) {
      return ((BigInteger) obj).intValue();
    }
    if (obj instanceof BigDecimal) {
      return ((BigDecimal) obj).intValue();
    }
    return obj instanceof String ? Integer.parseInt((String) obj)
        : Integer.parseInt(obj.toString());
  }),
  MEDIUMINT(scale -> "BasicTypeInfo.INT_TYPE_INFO", "MEDIUMINT", obj -> {
    if (obj == null) {
      return null;
    }
    if (obj instanceof Integer) {
      return obj;
    }
    if (obj instanceof Byte) {
      return ((Byte) obj).intValue();
    }
    if (obj instanceof Short) {
      return ((Short) obj).intValue();
    }
    if (obj instanceof Long) {
      return ((Long) obj).intValue();
    }
    if (obj instanceof Float) {
      return ((Float) obj).intValue();
    }
    if (obj instanceof Double) {
      return ((Double) obj).intValue();
    }
    if (obj instanceof BigInteger) {
      return ((BigInteger) obj).intValue();
    }
    if (obj instanceof BigDecimal) {
      return ((BigDecimal) obj).intValue();
    }
    return obj instanceof String ? Integer.parseInt((String) obj)
        : Integer.parseInt(obj.toString());
  }),
  INT(scale -> "BasicTypeInfo.INT_TYPE_INFO", "INT", obj -> {
    if (obj == null) {
      return null;
    }
    if (obj instanceof Integer) {
      return ((Integer) obj).longValue();
    }
    if (obj instanceof Byte) {
      return ((Byte) obj).longValue();
    }
    if (obj instanceof Short) {
      return ((Short) obj).longValue();
    }
    if (obj instanceof Long) {
      return ((Long) obj).longValue();
    }
    if (obj instanceof Float) {
      return ((Float) obj).longValue();
    }
    if (obj instanceof Double) {
      return ((Double) obj).longValue();
    }
    if (obj instanceof BigInteger) {
      return ((BigInteger) obj).longValue();
    }
    if (obj instanceof BigDecimal) {
      return ((BigDecimal) obj).longValue();
    }
    return obj instanceof String ? Long.parseLong((String) obj)
        : Long.parseLong(obj.toString());
  }),

  LONG(scale -> "BasicTypeInfo.LONG_TYPE_INFO", "BIGINT", obj -> {
    if (obj == null) {
      return null;
    }
    if (obj instanceof Long) {
      return obj;
    }
    if (obj instanceof Byte) {
      return ((Byte) obj).longValue();
    }
    if (obj instanceof Short) {
      return ((Short) obj).longValue();
    }
    if (obj instanceof Integer) {
      return ((Integer) obj).longValue();
    }
    if (obj instanceof Float) {
      return ((Float) obj).longValue();
    }
    if (obj instanceof Double) {
      return ((Double) obj).longValue();
    }
    if (obj instanceof BigInteger) {
      return ((BigInteger) obj).longValue();
    }
    if (obj instanceof BigDecimal) {
      return ((BigDecimal) obj).longValue();
    }
    return obj instanceof String ? Long.parseLong((String) obj)
        : Long.parseLong(obj.toString());
  }),
  INTEGER(scale -> "BasicTypeInfo.LONG_TYPE_INFO", "BIGINT", obj -> {
    if (obj == null) {
      return null;
    }
    if (obj instanceof Long) {
      return obj;
    }
    if (obj instanceof Byte) {
      return ((Byte) obj).longValue();
    }
    if (obj instanceof Short) {
      return ((Short) obj).longValue();
    }
    if (obj instanceof Integer) {
      return ((Integer) obj).longValue();
    }
    if (obj instanceof Float) {
      return ((Float) obj).longValue();
    }
    if (obj instanceof Double) {
      return ((Double) obj).longValue();
    }
    if (obj instanceof BigInteger) {
      return ((BigInteger) obj).longValue();
    }
    if (obj instanceof BigDecimal) {
      return ((BigDecimal) obj).longValue();
    }
    if (obj instanceof String) {
      if ("true".equals(obj)) {
        return Long.valueOf(1);
      } else if ("false".equals(obj)) {
        return Long.valueOf(0);
      } else {
        Long.parseLong((String) obj);
      }

    }
    return Long.parseLong(obj.toString());
  }),
  FLOAT(scale -> "BasicTypeInfo.FLOAT_TYPE_INFO", "FLOAT", obj -> {
    if (obj == null) {
      return null;
    }
    if (obj instanceof Float) {
      return obj;
    }
    if (obj instanceof Byte) {
      return ((Byte) obj).floatValue();
    }
    if (obj instanceof Short) {
      return ((Short) obj).floatValue();
    }
    if (obj instanceof Integer) {
      return ((Integer) obj).floatValue();
    }
    if (obj instanceof Long) {
      return ((Long) obj).floatValue();
    }
    if (obj instanceof Double) {
      return ((Double) obj).floatValue();
    }
    if (obj instanceof BigInteger) {
      return ((BigInteger) obj).floatValue();
    }
    if (obj instanceof BigDecimal) {
      return ((BigDecimal) obj).floatValue();
    }
    return obj instanceof String ? Float.parseFloat((String) obj)
        : Float.parseFloat(obj.toString());
  }),
  DOUBLE(scale -> "BasicTypeInfo.DOUBLE_TYPE_INFO", "DOUBLE", obj -> {
    if (obj == null) {
      return null;
    }
    if (obj instanceof Double) {
      return obj;
    }
    if (obj instanceof Byte) {
      return ((Byte) obj).doubleValue();
    }
    if (obj instanceof Short) {
      return ((Short) obj).doubleValue();
    }
    if (obj instanceof Integer) {
      return ((Integer) obj).doubleValue();
    }
    if (obj instanceof Long) {
      return ((Long) obj).doubleValue();
    }
    if (obj instanceof BigInteger) {
      return ((BigInteger) obj).doubleValue();
    }
    if (obj instanceof BigDecimal) {
      return ((BigDecimal) obj).doubleValue();
    }
    return obj instanceof String ? Double.parseDouble((String) obj)
        : Double.parseDouble(obj.toString());
  }),
  DATE(scale -> "BasicTypeInfo.DATE_TYPE_INFO", "DATE", obj -> {
    if (obj instanceof String) { //The code path is never entered. Though it has bug,
      //leave it. What the hell.
      return ((String) obj).matches(CommonConstant.DATETIME_REGX)
          ? ((String) obj).substring(0, 10).replace("/", "-")
          : ((String) obj).matches(CommonConstant.DATE_REGX) ? obj
              : CommonConstant.DEFAULT_DATE_STR;
    }
    if (obj instanceof Long) {
      return new Date((Long) obj);
    }
    if (obj instanceof Date) {
      return obj;
    }
    if (obj instanceof Timestamp) {
      return new Date(((Timestamp) obj).getTime());
    }
    return null;
  }),
  TIME(scale -> "BasicTypeInfo.DATE_TYPE_INFO", "TIME", obj -> {
    if (obj instanceof String) {
      return ((String) obj).matches(CommonConstant.DATETIME_REGX)
          ? ((String) obj).substring(11)
          : ((String) obj).matches(CommonConstant.TIME_REGX) ? obj
              : CommonConstant.DEFAULT_TIME_STR;
    }
    return CommonConstant.DEFAULT_TIME_STR;
  }),
  //DATETIME is a synonym for TIMESTAMP
  DATETIME(scale -> "BasicTypeInfo.LONG_TYPE_INFO", "BIGINT", obj -> {
    if (obj == null) {
      return null;
    }
    if (obj instanceof Timestamp) {
      return ((Timestamp) obj).getTime();
    }
    if (obj instanceof Long) {
      return obj;
    }
    if (obj instanceof Byte) {
      return ((Byte) obj).longValue();
    }
    if (obj instanceof Short) {
      return ((Short) obj).longValue();
    }
    if (obj instanceof Integer) {
      return ((Integer) obj).longValue();
    }
    if (obj instanceof Float) {
      return ((Float) obj).longValue();
    }
    if (obj instanceof Double) {
      return ((Double) obj).longValue();
    }
    if (obj instanceof BigInteger) {
      return ((BigInteger) obj).longValue();
    }
    if (obj instanceof BigDecimal) {
      return ((BigDecimal) obj).longValue();
    }
    if (obj instanceof String) {
      return ((String) obj).matches("^\\d+$") ? Long.parseLong((String) obj)
          : new Long(0);
    }
    return new Long(0);
  }),
  //it should be scale -> "new LegacyTimestampTypeInfo(scale)", but currently it is unnecessary
  //to introduce potential risk. So keep it as it is.
  TIMESTAMP(scale -> "BasicTypeInfo.LONG_TYPE_INFO", "BIGINT", obj -> {
    if (obj == null) {
      return null;
    }
    if (obj instanceof Timestamp) {
      return ((Timestamp) obj).getTime();
    }
    if (obj instanceof Long) {
      return obj;
    }
    if (obj instanceof Byte) {
      return ((Byte) obj).longValue();
    }
    if (obj instanceof Short) {
      return ((Short) obj).longValue();
    }
    if (obj instanceof Integer) {
      return ((Integer) obj).longValue();
    }
    if (obj instanceof Float) {
      return ((Float) obj).longValue();
    }
    if (obj instanceof Double) {
      return ((Double) obj).longValue();
    }
    if (obj instanceof BigInteger) {
      return ((BigInteger) obj).longValue();
    }
    if (obj instanceof BigDecimal) {
      return ((BigDecimal) obj).longValue();
    }
    if (obj instanceof String) {
      return ((String) obj).matches("^\\d+$") ? Long.parseLong((String) obj)
          : new Long(0);
    }
    return new Long(0);
  }),
  BIG_INT(scale -> "BasicTypeInfo.BIG_INT_TYPE_INFO", "BIGINT", obj -> {
    if (obj == null) {
      return null;
    }
    if (obj instanceof BigInteger) {
      return obj;
    }
    if (obj instanceof Float) {
      return new BigInteger(((Integer) ((Float) obj).intValue()).toString());
    }
    if (obj instanceof Double) {
      return new BigInteger(((Long) ((Double) obj).longValue()).toString());
    }
    if (obj instanceof BigDecimal) {
      return new BigInteger(((Long) ((BigDecimal) obj).longValue()).toString());
    }
    return obj instanceof String ? new BigInteger((String) obj) : new BigInteger(obj.toString());
  }),
  BIGINT(scale -> "BasicTypeInfo.BIG_INT_TYPE_INFO", "BIGINT", obj -> {
    if (obj == null) {
      return null;
    }
    if (obj instanceof BigInteger) {
      return obj;
    }
    if (obj instanceof Float) {
      return new BigInteger(((Integer) ((Float) obj).intValue()).toString());
    }
    if (obj instanceof Double) {
      return new BigInteger(((Long) ((Double) obj).longValue()).toString());
    }
    if (obj instanceof BigDecimal) {
      return new BigInteger(((Long) ((BigDecimal) obj).longValue()).toString());
    }
    return obj instanceof String ? new BigInteger((String) obj) : new BigInteger(obj.toString());
  }),
  BIG_DEC(scale -> String.format("BigDecimalTypeInfo.of(38, %s)", scale), "DECIMAL", obj -> {
    if (obj == null) {
      return null;
    }
    if (obj instanceof BigDecimal) {
      return obj;
    }
    return obj instanceof String ? new BigDecimal((String) obj)
        : new BigDecimal(obj.toString());
  }),
  BIGDEC(scale -> String.format("BigDecimalTypeInfo.of(38, %s)", scale), "DECIMAL", obj -> {
    if (obj == null) {
      return null;
    }
    if (obj instanceof BigDecimal) {
      return obj;
    }
    return obj instanceof String ? new BigDecimal((String) obj)
        : new BigDecimal(obj.toString());
  }),
  DECIMAL(scale -> String.format("BigDecimalTypeInfo.of(38, %s)", scale), "DECIMAL", obj -> {
    if (obj == null) {
      return null;
    }
    if (obj instanceof BigDecimal) {
      return obj;
    }
    return obj instanceof String ? new BigDecimal((String) obj)
        : new BigDecimal(obj.toString());
  }),
  NUMBER(scale -> String.format("BigDecimalTypeInfo.of(38, %s)", scale), "DECIMAL", obj -> {
    if (obj == null) {
      return null;
    }
    if (obj instanceof BigDecimal) {
      return obj;
    }
    if (StringUtils.isBlank(obj.toString())) {
      return new BigDecimal(0);
    }
    return obj instanceof String ? new BigDecimal((String) obj)
        : new BigDecimal(obj.toString());


  });

  private ITypeInfoBuilder flinkDataTypeName;
  private String mysqlDataTypeName;
  private IDataBuilder dataBuilder;

  DataTypeEnum(ITypeInfoBuilder flinkDataTypeName,
               String mysqlDataTypeName,
               IDataBuilder dataBuilder) {
    this.flinkDataTypeName = flinkDataTypeName;
    this.mysqlDataTypeName = mysqlDataTypeName;
    this.dataBuilder = dataBuilder;
  }

  /**
   * Get the corresponding flink type information object name of the specified type name with the
   * additional property name. This feature is supported by flink since 1.9.
   *
   * @param name  type name
   * @param scale scale of the type. This parameter may be redundant for some types, such as INT.
   */
  public static String createFlinkDataType(String name, Integer scale) {
    String flinkDataType = null;
    for (DataTypeEnum dataTypeEnum : DataTypeEnum.values()) {
      if (dataTypeEnum.name().equals(name)) {
        if (scale != null) {
          return dataTypeEnum.flinkDataTypeName.build(scale);
        } else {
          return dataTypeEnum.flinkDataTypeName.build(JobConstant.DEFAULT_SCALE);
        }
      }
    }
    Preconditions.checkArgument(flinkDataType != null, String.format("Illegal data type：%s", name));
    return flinkDataType;
  }

  /**
   * 创建mysql data type.
   *
   * @param name data type name
   *
   * @return mysql data type
   */
  @Deprecated
  public static String createMysqlDataType(String name) {
    String mysqlDataType = null;
    for (DataTypeEnum dataTypeEnum : DataTypeEnum.values()) {
      if (dataTypeEnum.name().equals(name)) {
        mysqlDataType = dataTypeEnum.mysqlDataTypeName;
        if (dataTypeEnum == BIG_DEC) {
          if (name.contains("(")) {
            mysqlDataType += "(" + 50 + "," + 2 + ")";
          }
        }
      }
    }
    Preconditions.checkArgument(mysqlDataType != null, String.format("Illegal data type：%s", name));
    return mysqlDataType;
  }

}