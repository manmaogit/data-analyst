/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2020 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.core.graph;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@Getter
@AllArgsConstructor
@ToString
public class RealtimeJobGraph<T> implements IGraph<T> {

  private List<T> sources;

  private List<T> processors;

  private List<T> sinks;

  public static class Builder<T> {

    private List<T> sources;
    private List<T> processors;
    private List<T> sinks;

    public RealtimeJobGraph<T> build() {
      return new RealtimeJobGraph<T>(sources, processors, sinks);
    }

    public Builder<T> setSources(List<T> inputNodes) {
      this.sources = inputNodes;
      return this;
    }

    public Builder<T> setProcessors(List<T> processors) {
      this.processors = processors;
      return this;
    }

    public Builder<T> setSinks(List<T> sinks) {
      this.sinks = sinks;
      return this;
    }
  }

}
