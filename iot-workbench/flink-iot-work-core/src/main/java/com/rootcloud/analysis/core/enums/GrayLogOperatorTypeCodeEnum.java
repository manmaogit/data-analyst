/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2020 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.core.enums;

/**
 * GrayLog操作和message映射.
 */
public enum GrayLogOperatorTypeCodeEnum {
  CONSOLE_PUBLISH("IOT_10000","控制台任务发布"),
  CONSOLE_CANCEL("IOT_10001","控制台任务撤回"),
  CONSOLE_START("IOT_10002","控制台任务启动"),
  CONSOLE_RUNNING("IOT_10003","控制台任务运行"),
  CONSOLE_STOP("IOT_10004","控制台任务撤回"),

  MYSQL_CONNECT_FAIL("IOT_10005","mysql连接异常"),
  MYSQL_CONNECT_TIMEOUT("IOT_10006","mysql超时异常"),

  FLINK_PARSE_SQL_FAIL("IOT_10007","FLINK解析SQL异常"),
  FLINK_EXECUTE_SQL_FAIL("IOT_10007","FLINK运行SQL异常"),

  ;


  private String operatorTypeCode;//操作类型code
  private String operatorDesc;//操作类型描述

  private GrayLogOperatorTypeCodeEnum(String operatorTypeCode, String operatorDesc) {
    this.operatorTypeCode = operatorTypeCode;
    this.operatorDesc = operatorDesc;
  }

  public String getOperatorTypeCode() {
    return operatorTypeCode;
  }

  public void setOperatorTypeCode(String operatorTypeCode) {
    this.operatorTypeCode = operatorTypeCode;
  }

  public String getOperatorDesc() {
    return operatorDesc;
  }

  public void setOperatorDesc(String operatorDesc) {
    this.operatorDesc = operatorDesc;
  }
}