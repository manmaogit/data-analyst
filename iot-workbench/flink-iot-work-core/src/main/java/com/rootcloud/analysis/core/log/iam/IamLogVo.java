/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.core.log.iam;

import com.rootcloud.analysis.core.enums.IamLogTypeEnum;
import com.rootcloud.analysis.core.graylog.LogVo;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Iam对应的kafka消息格式：http://confluence.irootech.com/pages/viewpage.action?pageId=298023239.
 */
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class IamLogVo {

  //string, uuid, optional, 如果没有，就在服务端随机生成一个
  private String id;
  //login | operation, required", 登录或者操作日志
  private String type;
  private Long timestamp;
  private IamLogPayloadVo payload;
  private String ip;
}



