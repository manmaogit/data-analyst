/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.core.mongodb;

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.mongodb.BasicDBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.mongodb.MongoCredential;
import com.mongodb.MongoException;
import com.mongodb.ServerAddress;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.rootcloud.analysis.core.exception.RcDbException;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;

public class RcMongoTemplate {

  public static final String SERVER_ADDR_SEPARATOR = ",";

  public static final String HOST_PORT_SEPARATOR = ":";

  public static final int SERVER_SELECTION_TIMEOUT = 5000;

  public static final String USER_DEFINED_DATABASE = "admin";

  public static final String K_ID = "_id";
  public List<ServerAddress> seeds;
  public MongoClient mongoClient;
  public String username;

  public String password;

  /**
   * Constructor.
   *
   * @param mongoUri mongoUri
   */
  public RcMongoTemplate(String mongoUri) throws URISyntaxException {
    Preconditions.checkArgument(!Strings.isNullOrEmpty(mongoUri), "Mongo uri is null or empty.");

    seeds = new ArrayList<>();
    if (mongoUri.indexOf("@") > 0) {
      String userInfoStr = mongoUri.substring(mongoUri.indexOf("//") + 2, mongoUri.indexOf("@"));
      if (!Strings.isNullOrEmpty(userInfoStr)) {
        String[] userInfo = userInfoStr.split(HOST_PORT_SEPARATOR);
        username = userInfo[0];
        password = userInfo[1];
      }
    }

    String[] mongoUriArr = mongoUri
        .substring(Math.max(mongoUri.indexOf("//") + 2, mongoUri.indexOf("@") + 1))
        .split(SERVER_ADDR_SEPARATOR);

    if (mongoUriArr.length > 0) {
      for (String s : mongoUriArr) {
        String[] hostPortArr = s.split(HOST_PORT_SEPARATOR);
        String host = hostPortArr[0];
        int port;
        if (hostPortArr[1].indexOf("/") > 0) {
          port = Integer.parseInt(hostPortArr[1].substring(0,
              hostPortArr[1].indexOf("/")));
        } else {
          port = Integer.parseInt(hostPortArr[1]);
        }
        seeds.add(new ServerAddress(host, port));
      }
    }
    this.connect();
  }

  /**
   * Connect.
   */
  public void connect() {
    if (Strings.isNullOrEmpty(username) || Strings.isNullOrEmpty(password)) {
      mongoClient = new MongoClient(seeds);
    } else {
      MongoCredential credential = MongoCredential.createCredential(
          username, USER_DEFINED_DATABASE, password.toCharArray());
      MongoClientOptions options = MongoClientOptions.builder()
          .serverSelectionTimeout(SERVER_SELECTION_TIMEOUT)
          .build();
      mongoClient = new MongoClient(seeds, credential, options);
    }
  }

  /**
   * Find by id.
   *
   * @param database   database
   * @param collection collection
   * @param id         id
   *
   * @return document
   */
  public Document findById(String database, String collection, String id) throws RcDbException {
    if (isClosed()) {
      throw new RcDbException("No operations allowed after mongo client closed.");
    }
    MongoCollection<Document> coll = mongoClient.getDatabase(database).getCollection(collection);
    try {
      return coll.find(Filters.eq(K_ID, new ObjectId(id)))
          .first();
    } catch (IllegalArgumentException illegalArgumentException) {
      return coll.find(Filters.eq(K_ID, id))
          .first();
    }
  }

  /**
   * Find.
   *
   * @param database   database
   * @param collection collection
   *
   * @return documents
   *
   * @throws RcDbException throws RcDbException
   */
  public FindIterable<Document> find(String database, String collection) throws RcDbException {
    if (isClosed()) {
      throw new RcDbException("No operations allowed after mongo client closed.");
    }
    MongoCollection<Document> coll = mongoClient.getDatabase(database)
        .getCollection(collection);
    return coll.find();
  }

  /**
   * Find by filter.
   *
   * @param database   database
   * @param collection collection
   * @param filter     filter
   *
   * @return documents
   *
   * @throws RcDbException throws RcDbException
   */
  public FindIterable<Document> find(String database, String collection, Bson filter)
      throws RcDbException {
    if (isClosed()) {
      throw new RcDbException("No operations allowed after mongo client closed.");
    }
    MongoCollection<Document> coll = mongoClient.getDatabase(database)
        .getCollection(collection);
    return coll.find(filter);
  }

  /**
   * Find by filter.
   *
   * @param database   database
   * @param collection collection
   * @param filter     filter
   * @param skip       skip
   * @param limit      limit
   *
   * @return documents
   *
   * @throws RcDbException throws RcDbException
   */
  public FindIterable<Document> find(String database, String collection, Bson filter, int skip,
                                     int limit) throws RcDbException {
    if (isClosed()) {
      throw new RcDbException("No operations allowed after mongo client closed.");
    }
    MongoCollection<Document> coll = mongoClient.getDatabase(database)
        .getCollection(collection);
    return coll.find(filter).skip(skip).limit(limit);
  }

  /**
   * Find.
   *
   * @param database   database
   * @param collection collection
   * @param skip       skip
   * @param limit      limit
   *
   * @return documents
   *
   * @throws RcDbException throws RcDbException
   */
  public FindIterable<Document> find(String database, String collection, int skip, int limit)
      throws RcDbException {
    if (isClosed()) {
      throw new RcDbException("No operations allowed after mongo client closed.");
    }
    MongoCollection<Document> coll = mongoClient.getDatabase(database)
        .getCollection(collection);
    return coll.find().skip(skip).limit(limit);
  }

  /**
   * listCollections.
   */
  public List<String> listCollections(String database) throws RcDbException {
    List<String> names = new ArrayList<>();
    try {
      mongoClient.getDatabase(database).listCollectionNames()
          .forEach((Consumer<String>) s -> names.add(s));
    } catch (MongoException e) {
      throw new RcDbException(e);
    }
    return names;
  }

  /**
   * 校验mongo连通性.
   *
   * @param database database
   */
  public void checkConnect(String database) {
    BasicDBObject queryObject = new BasicDBObject("usersInfo", username);
    MongoDatabase mongoDB = mongoClient.getDatabase(database);
    mongoDB.runCommand(queryObject);
  }

  /**
   * Is closed.
   *
   * @return true/false
   */
  public boolean isClosed() {
    return mongoClient == null;
  }

  /**
   * Close.
   */
  public void close() {
    if (mongoClient != null) {
      mongoClient.close();
      mongoClient = null;
    }
  }

}
