/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.core.exception;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@ToString
public class RcException extends Exception {

  private int code;

  private String message;

  /**
   * Constructor.
   *
   * @param code    code
   * @param message message
   */
  public RcException(int code, String message) {
    super();
    this.code = code;
    this.message = message;
  }

  /**
   * Constructor.
   *
   * @param code    code
   * @param message message
   * @param cause   cause
   */
  public RcException(int code, String message, Throwable cause) {
    super(cause);
    this.code = code;
    this.message = message;
  }

}
