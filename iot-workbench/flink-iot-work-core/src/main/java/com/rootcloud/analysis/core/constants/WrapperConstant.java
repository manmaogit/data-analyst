/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.core.constants;

import com.rootcloud.analysis.core.enums.TimeZoneTypeEnum;
import java.time.ZoneId;

public class WrapperConstant {

  public static final String DEVICE_ID = "device_id";

  public static final String COMMA = CommonConstant.COMMA;

  public static final String SQL_NODE_START_SUFFIX = "_START";

  public static final String SEPARATOR = "^@@^";

  public static final String SPLIT_SEPARATOR = "\\^@@\\^";

  public static final String NULL_IDENTIFICATION = "VAL(`Null`)";

  public static final String DEFAULT_START_TIME = "1|1|0|0";

  public static final ZoneId DEFAULT_ZONE_ID = TimeZoneTypeEnum.CTT.getZoneId();
}
