/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.core.flink.window;


public abstract class AbstractWindow {

  FlinkWindowTypeEnum type;

  public FlinkWindowTypeEnum getType() {
    return type;
  }

  public void setType(FlinkWindowTypeEnum type) {
    this.type = type;
  }
}
