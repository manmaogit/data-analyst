/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.core.exception;

import com.rootcloud.analysis.core.constants.ExceptionConstant;

public class RcDbException extends RcException {

  public RcDbException(String message) {
    super(ExceptionConstant.RC_DB_EX_CODE, message);
  }

  public RcDbException(Throwable cause) {
    super(ExceptionConstant.RC_DB_EX_CODE, cause.getMessage(), cause);
  }

  public RcDbException(String message, Throwable cause) {
    super(ExceptionConstant.RC_DB_EX_CODE, message, cause);
  }

}
