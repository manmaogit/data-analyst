/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.core.constants;

public class CollectionConstant {

  public static final String COLLECTION_TYPE = "collection_type";

  public static final String KEY_COLLECTION_ID = "collection_id";

  public static final String KEY_PARENT_ID = "parent_id";

}
