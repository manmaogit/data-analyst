/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.core.flink;

public enum FlinkStateEnum {

  INITIALIZING, FAILED, FINISHED, SUSPENDED, RUNNING, CANCELLING, CREATED, FAILING, RESTARTING,
  RECONCILING, CANCELED,

  //旧状态 仅用来兼容序列化操作
  FAIL, STARTING, STOPPING, STOP

}
