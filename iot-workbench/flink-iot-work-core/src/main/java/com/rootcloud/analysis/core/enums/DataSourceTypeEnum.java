/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.core.enums;

public enum DataSourceTypeEnum {

  // 离线-输入-Hive-查询旧工况模式
  INNER_HIVE,

  // 离线-输入-Hive-查询新工况模式
  NEO_INNER_HIVE,

  // 离线-输入-MySQL
  // 旧版
  MYSQL_DYNAMIC,
  // 新版
  MYSQL_BATCH,

  // 离线-输入-PG
  // 旧版
  PGSQL_DYNAMIC,
  // 新版
  PGSQL_BATCH,
  //离线-输入-ORACLE
  ORACLE_02_BATCH,

  // 离线-输入-InfluxDB-查询原始数据模式
  // 旧版
  INFLUXDB,
  INFLUXDB_BATCH,
  // 新版
  INFLUXDB_DEVICE,

  // 离线-输入-InfluxDB-查询聚合数据模式
  // 旧版
  INFLUXDB_WITH_AGG,
  INFLUXDB_WITH_AGG_BATCH,
  // 新版
  INFLUXDB_AGG_DEVICE,

  // 离线-输入-IDF
  REST_BATCH,

  // 离线-输入-TdEngine-查询原始数据模式
  TD_ENGINE,

  // 离线-输入-TdEngine-查询聚合数据模式
  TD_ENGINE_WITH_AGG,

  //离线-输入-SqlServer
  SQLSERVER_INPUT,

  //离线-输入-clickhouse
  CLICKHOUSE_INPUT,

  KAFKA,
  MONGO,
  MYSQL,
  HIVE,
  HDFS,
  POSTGRESQL,
  ORACLE,
  REST,
  ASSET,

  MYSQL_02,
  PGSQL_02,
}
