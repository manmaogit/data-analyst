/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.common.context;

import com.alibaba.fastjson.JSONObject;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import com.rootcloud.analysis.core.constants.CommonConstant;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class IotWorkRuntimeContext implements Serializable {

  private static final String CACHE_STREAM = "cacheStream";
  private static final String CACHE_EXECUTION_ENV = "executionEnv";
  private static final String CACHE_TABLE_ENV = "tableEnv";
  private static final String CACHE_STATEMENT_SET = "StatementSet";
  private static final String CACHE_TABLE = "streamTableName";
  private static final String CACHE_BROADCAST_STREAM = "broadcastStream";
  private static final String SOURCE_CONN = "sourceConn";
  private static final long serialVersionUID = 3567537716549970842L;

  private static String timezone;

  private static final Map<String, List<JSONObject>> nextNodesMap = Maps.newHashMap();

  private final Map<String, String> parameters;
  private final Map<String, Object> container;

  public IotWorkRuntimeContext() {
    parameters = Collections.synchronizedMap(new HashMap<String, String>());
    container = Collections.synchronizedMap(new HashMap<String, Object>());
  }

  public IotWorkRuntimeContext(Map<String, String> paramters) {
    this();
    this.putAll(paramters);
  }

  /**
   * 获取参数.
   */
  public Map<String, String> getParameters() {
    synchronized (parameters) {

      return ImmutableMap.copyOf(parameters);
    }
  }

  public void clear() {
    parameters.clear();
  }

  /**
   * 获取参数.
   */
  public Map<String, String> getSubProperties(String prefix) {
    Preconditions.checkArgument(prefix.endsWith("."),
        "The given prefix does not end with a period (" + prefix + ")");
    Map<String, String> result = Maps.newHashMap();
    synchronized (parameters) {
      for (Map.Entry<String, String> entry : parameters.entrySet()) {
        String key = entry.getKey();
        if (key.startsWith(prefix)) {
          String name = key.substring(prefix.length());
          result.put(name, entry.getValue());
        }
      }
    }
    return ImmutableMap.copyOf(result);
  }

  public void putAll(Map<String, String> map) {
    parameters.putAll(map);
  }

  public void put(String key, String value) {
    parameters.put(key, value);
  }

  /**
   * 判断是否包含某个key.
   */
  public boolean containsKey(String key) {
    return parameters.containsKey(key);
  }

  /**
   * 获取bool值.
   */
  public Boolean getBoolean(String key, Boolean defaultValue) {
    String value = get(key);
    if (value != null) {
      return Boolean.valueOf(Boolean.parseBoolean(value.trim()));
    }
    return defaultValue;
  }

  public Boolean getBoolean(String key) {
    return getBoolean(key, null);
  }

  /**
   * 获取Integer参数.
   */
  public Integer getInteger(String key, Integer defaultValue) {
    String value = get(key);
    if (value != null) {
      return Integer.valueOf(Integer.parseInt(value.trim()));
    }
    return defaultValue;
  }

  public Integer getInteger(String key) {
    return getInteger(key, null);
  }

  /**
   * 获取Long数据.
   */
  public Long getLong(String key, Long defaultValue) {
    String value = get(key);
    if (value != null) {
      return Long.valueOf(Long.parseLong(value.trim()));
    }
    return defaultValue;
  }

  public Long getLong(String key) {
    return getLong(key, null);
  }

  public String getString(String key, String defaultValue) {
    return get(key, defaultValue);
  }

  public String getString(String key) {
    return get(key);
  }

  /**
   * 获取float参数.
   */
  public Float getFloat(String key, Float defaultValue) {
    String value = get(key);
    if (value != null) {
      return Float.parseFloat(value.trim());
    }
    return defaultValue;
  }

  public Float getFloat(String key) {
    return getFloat(key, null);
  }

  /**
   * 获取Double数据.
   */
  public Double getDouble(String key, Double defaultValue) {
    String value = get(key);
    if (value != null) {
      return Double.parseDouble(value.trim());
    }
    return defaultValue;
  }

  public Double getDouble(String key) {
    return getDouble(key, null);
  }

  private String get(String key, String defaultValue) {
    String result = parameters.get(key);
    if (result != null) {
      return result;
    }
    return defaultValue;
  }

  private String get(String key) {
    return get(key, null);
  }

  @Override
  public String toString() {
    return "{ parameters:" + parameters + " }";
  }

  /**
   * 获取对象.
   */
  public <T> T getObjectFromContainer(String key) {
    if (container.containsKey(key)) {
      return (T) container.get(key);
    }
    return null;
  }

  public void putObjectToContainer(String key, Object object) {
    container.put(key, object);
  }

  public void putSourceToContainer(String sourceId, Object object) {
    container.put("source:" + sourceId, object);
  }

  /**
   * getSourceFromContainer.
   */
  public <T> T getSourceFromContainer(String sourceId) {
    if (container.containsKey("source:" + sourceId)) {
      return (T) container.get("source:" + sourceId);
    }

    return null;
  }

  public void putExecutionEnv(Object object) {
    container.put(CACHE_EXECUTION_ENV, object);
  }

  /**
   * getExecutionEnv.
   */
  public <T> T getExecutionEnv() {
    if (container.containsKey(CACHE_EXECUTION_ENV)) {
      return (T) container.get(CACHE_EXECUTION_ENV);
    }

    return null;
  }

  /**
   * 缓存流对象.
   */
  public void putStreamToCache(Object object) {
    container.put(CACHE_STREAM, object);
  }

  /**
   * 获取流对象.
   */
  public <T> T getStreamFromCache() {
    if (container.containsKey(CACHE_STREAM)) {
      return (T) container.get(CACHE_STREAM);
    }
    return null;
  }

  /**
   * 缓存流对象.
   */
  public void putTableEnvToCache(Object object) {
    container.put(CACHE_TABLE_ENV, object);
  }

  /**
   * 获取流对象.
   */
  public <T> T getTableEnvFromCache() {
    if (container.containsKey(CACHE_TABLE_ENV)) {
      return (T) container.get(CACHE_TABLE_ENV);
    }
    return null;
  }

  /**
   * Puts statement set to cache.
   */
  public void putStatementSetToCache(Object object) {
    container.put(CACHE_STATEMENT_SET, object);
  }

  /**
   * Gets statement set from cache.
   */
  public <T> T getStatementSetFromCache() {
    if (container.containsKey(CACHE_STATEMENT_SET)) {
      return (T) container.get(CACHE_STATEMENT_SET);
    }
    return null;
  }

  /**
   * 缓存流对象.
   */
  public void putTableToCache(String table) {
    container.put(getTableCacheKey(table), table);
  }

  /**
   * 获取流对象.
   */
  public <T> T getTableFromCache(String table) {
    String key = getTableCacheKey(table);
    if (container.containsKey(key)) {
      return (T) container.get(key);
    }
    return null;
  }

  private String getTableCacheKey(String table) {
    return CACHE_TABLE + CommonConstant.UNDERLINE + table;
  }

  private String getBroadcastStreamCacheKey(String streamId) {
    return CACHE_BROADCAST_STREAM + CommonConstant.UNDERLINE + streamId;
  }

  public void putBroadcastStreamToCache(String streamId, Object object) {
    container.put(getBroadcastStreamCacheKey(streamId), object);
  }

  /**
   * 获取sourceConn.
   */
  public <T> T getSourceConnCacheKey(String key) {
    if (container.containsKey(SOURCE_CONN + CommonConstant.UNDERLINE + key)) {
      return (T) container.get(SOURCE_CONN + CommonConstant.UNDERLINE + key);
    }
    return null;
  }

  /**
   * 放入sourceConn.
   */
  public void putSourceConnToCache(String key, Object object) {
    container.put(SOURCE_CONN + CommonConstant.UNDERLINE + key, object);
  }


  /**
   * 获取缓存的广播流.
   *
   * @param streamId 流ID
   * @param <T>      T
   *
   * @return T
   */
  public <T> T getBroadcastStreamFromCache(String streamId) {
    String key = getBroadcastStreamCacheKey(streamId);
    if (container.containsKey(key)) {
      return (T) container.get(key);
    }
    return null;
  }

  public static void setTimezone(String timezone) {
    IotWorkRuntimeContext.timezone = timezone;
  }

  public static String getTimezone() {
    return timezone;
  }

  /**
   * Puts next nodes to cache.
   */
  public static void putNextNodesToCache(String nodeId, JSONObject object) {
    List<JSONObject> nextNodes = getNextNodesFromCache(nodeId);
    if (nextNodes == null) {
      nextNodes = new ArrayList<>();
      nextNodesMap.put(nodeId, nextNodes);
    }
    nextNodes.add(object);
  }

  /**
   * Gets next nodes from cache.
   */
  public static List<JSONObject> getNextNodesFromCache(String nodeId) {
    if (nextNodesMap.containsKey(nodeId)) {
      return nextNodesMap.get(nodeId);
    }
    return null;
  }

}
