/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.core.log.iam;

import lombok.Data;

@Data
public class IamLogPayloadVo  {

  // 当前用户id，从token中获取, required
  private String userId;
  // 当前组织id，从token中获取, required
  private String organizationId;
  //租户id, 如果type=operation，就是required
  private String tenantId;
  private String clientId;
  private String clientType;
  //当前请求id，从header中获取, optional
  private String requestId;
  //状态，成功了还是失败了， 0, 失败，1, 成功，required
  private Integer status;
  private String errorCode;
  // 请求IP，真实的用户IP，optional
  private String ip;
  // 服务/应用，服务id，required
  private String serviceProvider;
  // 操作描述，optional
  private String description;
  //如果type=operation，就是required",
  private String action;
  //如果type=operation，就是required
  private String resourceId;
  private String resourceType;
  private String grantType;
  //如果没有给值，直接用环境变量DATA_CENTER_ID的
  private String dataCenterId;
  // 如果没有给值，直接用环境变量ZONE_ID的值"
  private String zoneId;
}
