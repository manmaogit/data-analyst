/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.core.dag;

import lombok.Data;

/**
 *  DAG模型的节点.
 *  每个节点的实质是一个算子，所有节点中包含的对应算子的信息，如算子类型、算子的实际Claas文件等等.
 */
@Data
public class RcNode {
  /**
   * 节点名称.
   * 一般为算子名称.
   */
  private String name;
  /**
   *  节点类型.
   *  一般为算子的类型，如：map算子.
   */
  private String type;
  /**
   *  匹配算子Class类型，限定类名.
   *
   */
  private String mappingEntity;

}
