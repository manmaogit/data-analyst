/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.core.constants;

public class ExceptionConstant {

  public static final int RC_DB_EX_CODE = 50001;

  public static final int RSCHEDULER_EX_CODE = 40001;

  public static final int ERROR_EXCEPTION_CODE = 500;

  public static final int BAD_REQUEST_EX_CODE = 41002;

  public static final int INVALID_TENANT_RESOURCE_EX_CODE = 41099;

}
