/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.core.util;

import com.rootcloud.analysis.core.exception.FailedActionException;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.function.Function;

public class RetryActor {
  private ExecutorService es;
  private int tryTimes;

  public RetryActor() {
    this(5);
  }

  public RetryActor(int tryTimes) {
    this.tryTimes = tryTimes;
    es = Executors.newSingleThreadExecutor();
  }

  /**
   * Try the action until {@code successCondition} is met or {@code tryTimes} runs out.
   *
   * @param successCondition the condition when the action succeeds
   * @param action           the action to try
   * @param <T>              the type of the action's result.
   * @throws FailedActionException FailedActionException
   */
  public <T> void tryAction(Function<T, Boolean> successCondition,
                            Callable<T> action)
      throws FailedActionException {
    Exception root = null;
    int time = 0;
    while (time++ < tryTimes) {
      try {
        Future<T> f = es.submit(action);
        T t = f.get();
        if (successCondition.apply(t)) {
          return;
        }
        Thread.sleep(3000);
      } catch (Exception e) {
        root = e;
      }
    }

    throw new FailedActionException(root);
  }
}
