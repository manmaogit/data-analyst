/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.core.enums;

import com.rootcloud.analysis.core.data.type.ITypeBuilder;
import java.util.Arrays;
import java.util.concurrent.atomic.AtomicReference;
import java.util.regex.Pattern;
import lombok.Getter;

@Getter
public enum MysqlTypeEnum {

  TINYINT(Pattern.compile("^TINYINT\\s*(\\(\\s*\\d+\\s*\\))?\\s*$"), param -> {
    if (param.toUpperCase().matches("^TINYINT(\\(1\\))$")) {
      return "BOOLEAN";
    }
    return "TINYINT";
  }),
  SMALLINT(Pattern.compile("^SMALLINT\\s*(\\(\\s*\\d+\\s*\\))?\\s*$"), param -> "SMALLINT"),
  TINYINT_UNSIGNED(Pattern.compile("^TINYINT\\s*(\\(\\s*\\d+\\s*\\))?\\s* UNSIGNED$"),
      param -> "SMALLINT"),
  INT(Pattern.compile("^INT\\s*(\\(\\s*\\d+\\s*\\))?\\s*$"), param -> "INT"),
  MEDIUMINT(Pattern.compile("^MEDIUMINT\\s*(\\(\\s*\\d+\\s*\\))?\\s*$"), param -> "INT"),
  SMALLINT_UNSIGNED(Pattern.compile("^SMALLINT\\s*(\\(\\s*\\d+\\s*\\))?\\s* UNSIGNED$"),
      param -> "INT"),
  BIGINT(Pattern.compile("^BIGINT\\s*(\\(\\s*\\d+\\s*\\))?\\s*$"), param -> "BIGINT"),
  INT_UNSIGNED(Pattern.compile("^INT\\s*(\\(\\s*\\d+\\s*\\))?\\s* UNSIGNED$"), param -> "BIGINT"),
  BIGINT_UNSIGNED(Pattern.compile("^BIGINT\\s*(\\(\\s*\\d+\\s*\\))?\\s* UNSIGNED$"),
      param -> "DECIMAL(20, 0)"),
  FLOAT(Pattern.compile("^FLOAT\\s*(\\(\\s*\\d+\\s*,\\s*\\d+\\s*\\))?\\s*$"), param -> "FLOAT"),
  DOUBLE(Pattern.compile("^DOUBLE\\s*(\\(\\s*\\d+\\s*,\\s*\\d+\\s*\\))?\\s*$"), param -> "DOUBLE"),
  DOUBLE_PRECISION(Pattern.compile("^DOUBLE\\s*(\\(\\s*\\d+\\s*,\\s*\\d+\\s*\\))?\\s* PRECISION$"),
      param -> "DOUBLE"),
  NUMERIC(Pattern.compile("^NUMERIC\\s*(\\(\\s*\\d+\\s*,\\s*\\d+\\s*\\))?\\s*$"),
      param -> "DECIMAL" + param.substring(7)),
  DECIMAL(Pattern.compile("^DECIMAL\\s*(\\(\\s*\\d+\\s*,\\s*\\d+\\s*\\))?\\s*$"), param ->
      param.toUpperCase()),
  BOOLEAN(Pattern.compile("^BOOLEAN$"), param -> "BOOLEAN"),
  DATE(Pattern.compile("^DATE\\s*(\\(\\s*\\d+\\s*\\))?\\s*$"), param -> "DATE"),
  TIME(Pattern.compile("^TIME\\s*(\\(\\s*\\d+\\s*\\))?\\s*$"), param -> param.toUpperCase()),
  DATETIME(Pattern.compile("^DATETIME\\s*(\\(\\s*\\d+\\s*\\))?\\s*$"), param ->
      "TIMESTAMP" + param.substring(8)),
  CHAR(Pattern.compile("^CHAR\\s*(\\(\\s*\\d+\\s*\\))?\\s*$"), param -> "STRING"),
  VARCHAR(Pattern.compile("^VARCHAR\\s*(\\(\\s*\\d+\\s*\\))?\\s*$"), param -> "STRING"),
  TEXT(Pattern.compile("^TEXT\\s*(\\(\\s*\\d+\\s*\\))?\\s*$"), param -> "STRING");

  private Pattern pattern;

  private ITypeBuilder<String> flinkSqlTypeBuilder;

  MysqlTypeEnum(Pattern pattern, ITypeBuilder flinkSqlTypeBuilder) {
    this.pattern = pattern;
    this.flinkSqlTypeBuilder = flinkSqlTypeBuilder;
  }

  public boolean matches(String input) {
    return pattern.matcher(input.toUpperCase()).matches();
  }

  /**
   * 是否包含以上枚举类型.
   */
  public static Boolean contains(String dataType) {
    for (MysqlTypeEnum type : MysqlTypeEnum.values()) {
      if (type.matches(dataType)) {
        return true;
      }
    }
    return false;
  }
}
