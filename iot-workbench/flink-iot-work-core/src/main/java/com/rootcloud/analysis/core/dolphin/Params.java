/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.core.dolphin;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Params {

  private String mainClass;
  private MainJar mainJar;
  private String deployMode;
  private List resourceList;
  private List<LocalParams> localParams;
  private Integer slot;
  private String taskManager;
  private String jobManagerMemory;
  private String taskManagerMemory;
  private Integer executorCores;
  private String mainArgs;
  private String others;
  private String programType;


}
