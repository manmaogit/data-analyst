/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.core.constants;

public class TenantConstant {

  public static final String ID = "_id";

  public static final String TENANT_ID = "tenantId";

  public static final String MAX_JOB_MUM = "maxJobNum";

}