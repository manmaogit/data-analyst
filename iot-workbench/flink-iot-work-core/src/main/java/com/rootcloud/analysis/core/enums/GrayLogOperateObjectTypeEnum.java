/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.core.enums;

public enum GrayLogOperateObjectTypeEnum {
  /**
   * 模型.
   */
  MODEL("MODEL"),
  /**
   * 实例.
   */
  INSTANCE("INSTANCE"),
  /**
   * 模板.
   */
  TEMPLATE("TEMPLATE"),
  /**
   * 任务.
   */
  JOB("JOB"),
  /**
   * 调度任务.
   */
  SCHEDULE("SCHEDULE"),
  /**
   * 指令.
   */
  INSTRUCTION("INSTRUCTION"),
  /**
   * 告警.
   */
  ALARM("ALARM"),
  /**
   * 属性.
   */
  PROPERTY("PROPERTY"),
  /**
   * 外部数据源.
   */
  EXTERNAL_DATASOURCE("EXTERNAL_DATASOURCE"),
  /**
   * 工作流.
   */
  WORKFLOW("WORKFLOW"),

  UDF("UDF")
  ;


  private String value;

  GrayLogOperateObjectTypeEnum(String value) {
    this.value = value;
  }

  public String getValue() {
    return value;
  }

  public void setValue(String value) {
    this.value = value;
  }
}
