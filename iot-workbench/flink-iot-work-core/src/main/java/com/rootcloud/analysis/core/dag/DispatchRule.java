/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.core.dag;

import lombok.Data;

/**
 * 节点间数据分派规则.
 * 引入组的概念：多个结束节点{@link RcNode}为一组分享开始节点数据.各组的分派方式参考{@link Dispatch}
 */
@Data
public class DispatchRule {
  /**
   * 属于组，包含一个及以上的节点.
   */
  private String group;
  /**
   * 分派方式.
   */
  private Dispatch dispatch;

  /**
   * 组或者数据的分派方式，暂时只支持"all"类型，即每组均获取一份完整数据.
   */
  @Data
  public static class Dispatch {
    /**
     * 分派类型。
     * "all":全部的Group都有一份.
     */
    private String type;

  }
}
