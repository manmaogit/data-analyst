/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.core.enums;

public enum AjaxCmdEnum {
  AUTH,
  LOG_DETAIL,
  PROJECTS_CREATE,
  PROJECTS_EXECUTORS_EXECUTE,
  PROJECTS_PROCESS_SAVE,
  PROJECTS_EXECUTORS_START_CHECK,
  PROJECTS_EXECUTORS_START_PROCESS_INSTANCE,
  PROJECTS_LIST_PAGING,
  PROJECTS_PROCESS_RELEASE,
  PROJECTS_PROCESS_VERIFY_NAME,
  PROJECTS_QUERY_BY_ID,
  PROJECTS_SCHEDULE_CREATE,
  PROJECTS_SCHEDULE_PREVIEW,
  PROJECTS_UPDATE,
  QUEUE_LIST,
  QUEUE_CREATE,
  RESOURCES_VERIFY_NAME,
  RESOURCES_CREATE,
  PROJECTS_EXECUTORS_GET_RECEIVER_CC,
  PROJECTS_INSTANCE_LIST,
  PROJECTS_INSTANCE_DELETE,
  PROJECTS_SCHEDULE_LIST,
  PROJECTS_SCHEDULE_ONLINE,
  PROJECTS_SCHEDULE_OFFLINE,
  PROJECTS_PROCESS_DELETE,
  PROJECTS_PROCESS_LIST,
  TASK_INSTANCE_LIST


}
