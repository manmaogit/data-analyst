/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2020 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.core.enums;

public enum ProcessTypeEnum {

  CONNECT,
  FILTER,
  SQL,
  SCHEMA,
  SCHEMA_02,
  TEMPORARY_VIEW,
  FLINK_SQL,
  OFFLINE_DEDUP,
  REALTIME_DEDUP,

  FLINK_SQL_BATCH,
  /**
   * 表值聚合.
   */
  UDT_AGG,

}