/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2020 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.core.enums;

/**
 * 日志告警级别  info warn error.
 * http://confluence.irootech.com:8090/pages/viewpage.action?pageId=78119175.
 */
public enum LogLogLevelEnum {
  INFO("6"),
  WARN("4"),
  ERROR("3");

  private String logLevel;

  private LogLogLevelEnum(String logLevel) {
    this.logLevel = logLevel;
  }

  public String getLogLevel() {
    return logLevel;
  }

  public void setLogLevel(String logLevel) {
    this.logLevel = logLevel;
  }
}