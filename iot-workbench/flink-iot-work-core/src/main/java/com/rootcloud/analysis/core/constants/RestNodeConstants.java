/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.core.constants;

public class RestNodeConstants {

  public static final String URL_KEY = "url";
  public static final String METHOD_KEY = "method";
  public static final String HEADERS_KEY = "headers";
  public static final String BODY_KEY = "body";
  public static final String PARAMETERS_KEY = "parameters";
  public static final String DETECT_PERIOD_KEY = "detect_period";
  public static final String EMIT_ONE_BY_ONE = "emit_one_by_one";
  public static final String IS_CHANGE_SENSITIVE = "is_change_sensitive";
  public static final String REST_API_INPUT_SCHEMA = "rest_api_input_schema";
  public static final String REST_IS_STREAM = "is_stream";
  public static final String REST_INSERT_KEYS = "insert_keys";
  public static final String REST_NOT_NULL_KEYS = "not_null_keys";
  public static final String REST_META_TYPE = "meta_type";
  public static final String REST_STANDING_BOOK_ID = "standing_book_id";
  public static final String REST_BATCH_SIZE = "batch_size";
  public static final String REST_TIME_WINDOW_SECONDS = "time_window_seconds";

}
