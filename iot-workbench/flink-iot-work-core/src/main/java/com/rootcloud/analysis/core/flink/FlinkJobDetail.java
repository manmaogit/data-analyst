/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.core.flink;

import com.alibaba.fastjson.annotation.JSONField;
import java.util.Map;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FlinkJobDetail {

  private String jid;

  private String name;

  private boolean isStoppable;

  private FlinkStateEnum state;

  @JSONField(name = "start-time")
  private Long startTime;

  @JSONField(name = "end-time")
  private Long endTime;

  private Long duration;

  private Long now;

  private Map<String, Long> timestamps;
}
