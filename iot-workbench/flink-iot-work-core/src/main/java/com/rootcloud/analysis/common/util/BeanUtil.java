/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.common.util;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.text.DateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BeanUtil {

  private static Logger logger = LoggerFactory.getLogger(BeanUtil.class);


  /**
   * convert bean to map.
   *
   * @param obj Object
   *
   * @return Map
   *
   */
  public static Map<String, Object> convertBeanToMap(Object obj) {
    if (obj == null) {
      return null;
    }
    Map<String, Object> map = new HashMap<String, Object>();
    try {
      BeanInfo beanInfo = Introspector.getBeanInfo(obj.getClass());
      PropertyDescriptor[] propertyDescriptors = beanInfo.getPropertyDescriptors();
      for (PropertyDescriptor property : propertyDescriptors) {
        String key = property.getName();

        // 过滤class属性
        if (!("class".equals(key))) {
          // 得到property对应的getter方法
          Method getter = property.getReadMethod();
          Object value = getter.invoke(obj);
          if (null == value) {
            map.put(key, "");
          } else {
            map.put(key, value);
          }
        }
      }
    } catch (Exception e) {
      logger.error("convertBeanToMap Error{}", e);
    }
    return map;
  }

  /**
   * convert map to bean.
   *
   * @param clazz Class
   *
   * @param map Map
   *
   * @return T
   *
   */
  public static <T> T convertMapToBean(Map<String, Object> map,Class<T> clazz) {
    T obj = null;
    try {
      BeanInfo beanInfo = Introspector.getBeanInfo(clazz);

      Constructor<?> []constructors = clazz.getConstructors();

      obj = clazz.newInstance();

      // 给 JavaBean 对象的属性赋值
      PropertyDescriptor[] propertyDescriptors = beanInfo.getPropertyDescriptors();
      for (int i = 0; i < propertyDescriptors.length; i++) {
        PropertyDescriptor descriptor = propertyDescriptors[i];
        String propertyName = descriptor.getName();
        if (map.containsKey(propertyName)) {

          // 下面一句可以 try 起来，这样当一个属性赋值失败的时候就不会影响其他属性赋值。
          Object value = map.get(propertyName);
          if ("".equals(value)) {
            value = null;
          }
          Object[] args = new Object[1];
          args[0] = value;
          descriptor.getWriteMethod().invoke(obj, args);
        }
      }
    } catch (IllegalAccessException e) {
      logger.error("convertMapToBean Failed to instantiate JavaBean Error{}", e);
    } catch (IntrospectionException e) {
      logger.error("convertMapToBean Failed to parse class attribute Error{}", e);
    } catch (IllegalArgumentException e) {
      logger.error("convertMapToBean Mapping error Error{}", e);
    } catch (InstantiationException e) {
      logger.error("convertMapToBean instantiation JavaBean fail Error{}", e);
    } catch (InvocationTargetException e) {
      logger.error("Convertmaptobean field mapping failed Error{}", e);
    } catch (Exception e) {
      logger.error("convertMapToBean Error{}", e);
    }
    return (T) obj;
  }

  /**
   * convert map to model.
   *
   * @param map Map
   *
   * @param o Object
   *
   * @return Object
   *
   */
  public static Object convertMapToObjectContent(Map<String, Object> map, Object o)
      throws Exception {
    if (!map.isEmpty()) {
      for (String k : map.keySet()) {
        Object v = null;
        if (!k.isEmpty()) {
          v = map.get(k);
        }
        Field[] fields = null;
        fields = o.getClass().getDeclaredFields();
        String clzName = o.getClass().getSimpleName();
        for (Field field : fields) {
          int mod = field.getModifiers();

          if (field.getName().toUpperCase().equals(k.toUpperCase())) {
            field.setAccessible(true);

            //region--进行类型判断
            String type = field.getType().toString();
            if (type.endsWith("String")) {
              if (v != null) {
                v = v.toString();
              } else {
                v = "";
              }
            }
            if (type.endsWith("Date")) {
              v = DateFormat.getDateInstance().format(v.toString());
            }
            if (type.endsWith("Boolean")) {
              v = Boolean.getBoolean(v.toString());
            }
            if (type.endsWith("int")) {
              v = new Integer(v.toString());
            }
            if (type.endsWith("Long")) {
              v = new Long(v.toString());
            }

            //endregion
            field.set(o, v);
          }
        }
      }
    }
    return o;
  }

  /**
   * convert object to map.
   *
   * @param obj Object
   *
   * @return Map
   *
   */
  public static Map<String, Object> convertObjectToMap(Object obj) {
    Map<String, Object> map = new HashMap<>();
    if (obj == null) {
      return map;
    }
    Class clazz = obj.getClass();
    Field[] fields = clazz.getDeclaredFields();
    try {
      for (Field field : fields) {
        field.setAccessible(true);
        map.put(field.getName(), field.get(obj));
      }
    } catch (Exception e) {
      logger.error("error msg:",e);
    }
    return map;
  }

  /**
   * convert map to object.
   *
   * @param map Map
   *
   * @param clazz Class
   *
   * @return Object
   *
   */
  public static Object convertMap2Object(Map<String, Object> map, Class<?> clazz) {
    if (map == null) {
      return null;
    }
    Object obj = null;
    try {
      obj = clazz.newInstance();

      Field[] fields = obj.getClass().getDeclaredFields();
      for (Field field : fields) {
        int mod = field.getModifiers();
        if (Modifier.isStatic(mod) || Modifier.isFinal(mod)) {
          continue;
        }
        field.setAccessible(true);
        field.set(obj, map.get(field.getName()));
      }
    } catch (Exception e) {
      logger.error("error msg:",e);
    }
    return obj;
  }
}
