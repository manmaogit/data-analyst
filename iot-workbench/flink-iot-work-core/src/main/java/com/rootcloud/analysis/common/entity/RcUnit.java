/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.common.entity;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.google.common.base.Preconditions;

import com.google.common.base.Strings;
import com.google.common.collect.Maps;
import com.rootcloud.analysis.common.util.BeanUtil;
import com.rootcloud.analysis.core.constants.SchemaConstant;
import com.rootcloud.analysis.core.util.DataUtil;

import java.io.Serializable;

import java.util.Map;

import lombok.Getter;
import lombok.ToString;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@ToString
public class RcUnit implements Serializable {

  private static Logger logger = LoggerFactory.getLogger(RcUnit.class);

  private static final long serialVersionUID = 6323348248454679035L;

  @Getter
  private Map data;

  private RcUnit() {
  }

  /**
   * valueOf.
   */
  public static RcUnit valueOf(Object javaBean) {
    RcUnit unit = new RcUnit();
    unit.setData(BeanUtil.convertBeanToMap(javaBean));
    return unit;
  }

  /**
   * valueOf.
   */
  public static RcUnit valueOf(String jsonStr, JSONArray schema) {
    Preconditions.checkArgument(schema != null && !schema.isEmpty(), "Schema is null or empty.");
    JSONObject jsonObject = JSONObject.parseObject(jsonStr);
    Map data = Maps.newHashMap();
    for (Object obj : schema) {
      JSONObject schemaObject = (JSONObject) obj;
      String originalColumn = Strings.isNullOrEmpty(schemaObject.getString(SchemaConstant.ATTR_ORIGINAL_NAME))
              ? schemaObject.getString(SchemaConstant.ATTR_NAME)
              : schemaObject.getString(SchemaConstant.ATTR_ORIGINAL_NAME);
      String column = schemaObject.getString(SchemaConstant.ATTR_NAME);
      if (originalColumn.contains(SchemaConstant.DOT)) {
        String[] split = originalColumn.split("\\.");
        String parentColumn = split[0];
        String childColumn = split[1];
        if (jsonObject.containsKey(parentColumn)) {
          JSONObject childJsonObject = jsonObject.getJSONObject(parentColumn);
          data.put(column, DataUtil.convert(childJsonObject.get(childColumn), schemaObject.getString(SchemaConstant.ATTR_DATA_TYPE)));
        } else {
          continue;
        }
      } else {
        data.put(column, DataUtil.convert(jsonObject.get(originalColumn), schemaObject.getString(SchemaConstant.ATTR_DATA_TYPE)));
      }
    }
    RcUnit unit = new RcUnit();
    unit.setData(data);
    return unit;
  }

  /**
   * valueOf.
   */
  public static RcUnit valueOf(JSONObject jsonObject, JSONArray schema) {
    Preconditions.checkArgument(schema != null && !schema.isEmpty(), "Schema is null or empty.");
    Map data = Maps.newHashMap();
    for (Object obj : schema) {
      JSONObject schemaObject = (JSONObject) obj;
      String originalColumn =
          Strings.isNullOrEmpty(schemaObject.getString(SchemaConstant.ATTR_ORIGINAL_NAME))
              ? schemaObject.getString(SchemaConstant.ATTR_NAME)
              : schemaObject.getString(SchemaConstant.ATTR_ORIGINAL_NAME);
      String column = schemaObject.getString(SchemaConstant.ATTR_NAME);
      if (originalColumn.contains(SchemaConstant.DOT)) {
        String[] split = originalColumn.split("\\.");
        String parentColumn = split[0];
        String childColumn = split[1];
        if (jsonObject.containsKey(parentColumn)) {
          JSONObject childJsonObject = jsonObject.getJSONObject(parentColumn);
          data.put(column, DataUtil.convert(childJsonObject.get(childColumn), schemaObject.getString(SchemaConstant.ATTR_DATA_TYPE)));
        } else {
          continue;
        }
      } else {
        data.put(column, DataUtil.convert(jsonObject.get(originalColumn),
            schemaObject.getString(SchemaConstant.ATTR_DATA_TYPE)));
      }
    }
    RcUnit unit = new RcUnit();
    unit.setData(data);
    return unit;
  }

  /**
   * 流计算转换.
   */
  public static RcUnit valueOf(Map map, JSONArray schema) {
    Preconditions.checkArgument(schema != null && !schema.isEmpty(), "Schema is null or empty.");
    Map data = Maps.newHashMap();
    JSONObject jsonObject = JSONObject.parseObject(JSONObject.toJSONString(map));
    for (Object obj : schema) {
      JSONObject schemaObject = (JSONObject) obj;
      String originalColumn =
          Strings.isNullOrEmpty(schemaObject.getString(SchemaConstant.ATTR_ORIGINAL_NAME))
              ? schemaObject.getString(SchemaConstant.ATTR_NAME)
              : schemaObject.getString(SchemaConstant.ATTR_ORIGINAL_NAME);
      String column = schemaObject.getString(SchemaConstant.ATTR_NAME);
      if (originalColumn.contains(SchemaConstant.DOT)) {
        String[] split = originalColumn.split("\\.");
        String parentColumn = split[0];
        String childColumn = split[1];
        if (jsonObject.containsKey(parentColumn)) {
          JSONObject childJsonObject = jsonObject.getJSONObject(parentColumn);
          data.put(column, DataUtil.convert(childJsonObject.get(childColumn), schemaObject.getString(SchemaConstant.ATTR_DATA_TYPE)));
        } else if (jsonObject.containsKey(childColumn)) {
          Object childObject = jsonObject.get(childColumn);
          data.put(column, DataUtil.convert2DataTypeInfo(childObject, schemaObject.getString(SchemaConstant.ATTR_DATA_TYPE)));
        } else {
          continue;
        }
      } else {
        data.put(column, DataUtil.convert(jsonObject.get(originalColumn), schemaObject.getString(SchemaConstant.ATTR_DATA_TYPE)));
      }
    }
    RcUnit unit = new RcUnit();
    unit.setData(data);
    return unit;
  }

  /**
   * valueOf.
   *
   * @param map map
   *
   * @return rcunit
   */
  public static RcUnit valueOf(Map map) {
    RcUnit unit = new RcUnit();
    unit.setData(map);
    return unit;
  }

  /**
   * 离线计算转换.
   */
  public static RcUnit valueOfDataTypeInfo(Map map, JSONArray schema) {
    Preconditions.checkArgument(schema != null && !schema.isEmpty(), "Schema is null or empty.");
    Map data = Maps.newHashMap();
    JSONObject jsonObject = JSONObject.parseObject(JSONObject.toJSONString(map));
    for (Object obj : schema) {
      JSONObject schemaObject = (JSONObject) obj;
      String originalColumn =
          Strings.isNullOrEmpty(schemaObject.getString(SchemaConstant.ATTR_ORIGINAL_NAME))
              ? schemaObject.getString(SchemaConstant.ATTR_NAME)
              : schemaObject.getString(SchemaConstant.ATTR_ORIGINAL_NAME);
      String column = schemaObject.getString(SchemaConstant.ATTR_NAME);
      if (originalColumn.contains(SchemaConstant.DOT)) {
        String[] split = originalColumn.split("\\.");
        String parentColumn = split[0];
        String childColumn = split[1];
        if (jsonObject.containsKey(parentColumn)) {
          JSONObject childJsonObject = jsonObject.getJSONObject(parentColumn);
          data.put(column, DataUtil.convert2DataTypeInfo(childJsonObject.get(childColumn), schemaObject.getString(SchemaConstant.ATTR_DATA_TYPE)));
        } else if (jsonObject.containsKey(childColumn)) {
          Object childObject = jsonObject.get(childColumn);
          data.put(column, DataUtil.convert2DataTypeInfo(childObject, schemaObject.getString(SchemaConstant.ATTR_DATA_TYPE)));
        } else {
          continue;
        }
      } else {
        //兼容sql转换成大写之后无法匹配小写查询字段
        data.put(column, DataUtil.convert2DataTypeInfo(
            jsonObject.get(originalColumn) == null ? jsonObject.get(originalColumn.toUpperCase()) : jsonObject.get(originalColumn),
            schemaObject.getString(SchemaConstant.ATTR_DATA_TYPE)));
      }
    }
    RcUnit unit = new RcUnit();
    unit.setData(data);
    return unit;
  }

  private void setData(Map map) {
    this.data = map;
  }

  public <T> T getBean(Class<?> clazz) {
    return (T) BeanUtil.convertMapToBean(data, clazz);
  }

  public Object getAttr(String key) {
    return data.get(key);
  }

  public void setAttr(String key, Object obj) {
    data.put(key, obj);
  }

  public String getStringAttr(String key) {
    return (String) data.get(key);
  }

  public Long getLongAttr(String key) {
    return (Long) data.get(key);
  }

  public Integer getIntAttr(String key) {
    return (Integer) data.get(key);
  }

  public Boolean getBoolAttr(String key) {
    return (Boolean) data.get(key);
  }

}
