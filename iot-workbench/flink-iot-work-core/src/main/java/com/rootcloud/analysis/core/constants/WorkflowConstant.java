/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.core.constants;

public class WorkflowConstant {

  public static final String KEY_ID = "id";
  public static final String KEY_COLLECTION_ID = "collection_id";
  public static final String KEY_CONDITION = "condition";
  public static final String KEY_CONDITION_LOGICAL_OPERATOR = "logical_operator";
  public static final String KEY_CONDITION_ITEM_LIST = "condition_item_list";
  public static final String KEY_CONDITION_ITEM_NODE_ID = "node_id";
  public static final String KEY_CONDITION_ITEM_STATUS = "status";
  public static final String KEY_CONDITION_RESULT = "condition_result";
  public static final String KEY_SUCCESS_NODE = "success_node";
  public static final String KEY_FAILED_NODE = "failed_node";
  public static final String KEY_NAME = "name";
  public static final String KEY_DEPENDENCE = "dependence";
  public static final String KEY_DEPENDENCE_LOGICAL_OPERATOR = "logical_operator";
  public static final String KEY_DEPENDENCE_ITEM_LIST = "dependence_item_list";
  public static final String KEY_DEPENDENCE_ITEM_WORKFLOW_ID = "workflow_id";
  public static final String KEY_DEPENDENCE_ITEM_NODE_ID = "node_id";
  public static final String KEY_DEPENDENCE_ITEM_CYCLE = "cycle";
  public static final String KEY_DEPENDENCE_ITEM_DATE_VALUE = "date_value";
  public static final String KEY_PROCESS_DEFINITION_CONNECTS = "process_definition_connects";
  public static final String KEY_PROCESS_DEFINITION_JSON = "process_definition_json";
  public static final String KEY_PROCESS_DEFINITION_LOCATIONS = "process_definition_locations";
  public static final String KEY_PROCESS_DEFINITION_NAME = "process_definition_name";
  public static final String KEY_PROJECT_NAME = "project_name";
  public static final String KEY_NODES = "nodes";
  public static final String KEY_DB_TYPE = "db_type";
  public static final String KEY_DATASOURCE = "datasource";
  public static final String KEY_SQL = "sql";

  /**
   * 工作流-发布状态.
   */
  public static final String RELEASE_STATE = "release_state";

  /**
   * Dolphin中节点NodeId和NodeName的分隔符.
   */
  public static final String DOLPHIN_SEPARATOR_OF_NODE_ID_AND_NAME = "||";
  // 转义后.
  public static final String DOLPHIN_SEPARATOR_OF_NODE_ID_AND_NAME_ESCAPE = "\\|\\|";

  // 默认的未分类目录.
  public static final String DEFAULT_UNCATEGORIZED_COLLECTION_ID = "-";
  public static final String DEFAULT_UNCATEGORIZED_COLLECTION_NAME = "未分类工作流";
  // 离线任务自动转工作流后的离线任务节点的ID前缀.
  public static final String DEFAULT_WORKFLOW_NODE_PREFIX_NAME = "tasks-";

}
