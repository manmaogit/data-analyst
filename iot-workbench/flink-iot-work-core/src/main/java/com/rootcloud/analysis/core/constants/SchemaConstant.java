/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.core.constants;

public class SchemaConstant {

  public static final String COLUMN_SEPARATOR = ":";

  public static final String INDEX_COLUMN_SEPARATOR = ",";

  public static final String ATTR_NAME = "name";

  public static final String ATTR_ORIGINAL_NAME = "original_name";

  public static final String ATTR_DATA_TYPE = "data_type";

  public static final String ATTR_ORIGINAL_TYPE = "original_type";

  public static final String ATTR_SOURCE = "attr_source";

  public static final String ATTR_SCALE = "scale";

  public static final String TIMESTAMP = "__timestamp__";

  public static final String ATTR_OLD_DATA_TYPE = "old_data_type";

  public static final String AGG_FUNCTION = "agg_function";

  public static final String TENANT_ID = "__tenantId__";

  public static final String DEVICE_ID = "__deviceId__";

  public static final String DEVICE_TYPE_ID = "__deviceTypeId__";

  // 抽象模型ID.
  public static final String ANCESTORS = "__ancestors__";

  public static final String DOUBLE_QUOTE = "\"";

  public static final String SINGLE_QUOTE = "'";

  public static final String BACK_QUOTE = "`";

  public static final String DOT = ".";

  public static final String OR = "|";

}
