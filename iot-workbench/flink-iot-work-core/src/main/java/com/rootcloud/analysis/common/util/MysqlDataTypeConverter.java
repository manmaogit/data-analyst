/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.common.util;

import com.google.common.base.Preconditions;
import com.rootcloud.analysis.core.enums.DataTypeEnum;
import com.rootcloud.analysis.core.enums.DataTypeInfoEnum;
import com.rootcloud.analysis.core.enums.MysqlTypeEnum;

public class MysqlDataTypeConverter {

  /**
   * Converts data type to unified data type.
   *
   * @param dataType source data type
   *
   * @return unified data type
   */
  public static DataTypeEnum convert2UnifiedDataType(String dataType) {
    String lowerCaseType = dataType.toLowerCase();
    String timeStampRegex = "^(date|time|datetime|timestamp).*$";
    if (lowerCaseType.matches(timeStampRegex)) {
      return DataTypeEnum.TIMESTAMP;
    }
    String integerRegex = "^(smallint|mediumint|int|bigint).*$";
    if (lowerCaseType.matches(integerRegex)) {
      return DataTypeEnum.INTEGER;
    }
    String boolRegex = "^(tinyint).*$";
    if (lowerCaseType.matches(boolRegex)) {
      return DataTypeEnum.BOOLEAN;
    }
    String numberRegex = "^(float|double|decimal|number).*$";
    if (lowerCaseType.matches(numberRegex)) {
      return DataTypeEnum.NUMBER;
    }
    return DataTypeEnum.STRING;
  }

  /**
   * Converts data type to flink data type.
   *
   * @param dataType source data type
   *
   * @return unified data type
   */
  public static DataTypeInfoEnum convert2TypeInfo(String dataType) {
    String lowerCaseType = dataType.toLowerCase();
    String timeStampRegex = "^(datetime).*$";
    if (lowerCaseType.matches(timeStampRegex)) {
      return DataTypeInfoEnum.TIMESTAMP;
    }
    String dateRegex = "^(date).*$";
    if (lowerCaseType.matches(dateRegex)) {
      return DataTypeInfoEnum.DATE;
    }
    String timeRegex = "^(time\\s*(\\(\\s*\\d+\\s*\\))?\\s*$)";
    if (lowerCaseType.matches(timeRegex)) {
      return DataTypeInfoEnum.TIME;
    }
    String bigintUnsignedRegex = "^(bigint\\s*(\\(\\s*\\d+\\s*\\))?\\s*unsigned).*$";
    if (lowerCaseType.matches(bigintUnsignedRegex)) {
      return DataTypeInfoEnum.DECIMAL;
    }

    String bigintRegex
        = "^(bigint\\s*(\\(\\s*\\d+\\s*\\))?\\s*$|int\\s*(\\(\\s*\\d+\\s*\\))?\\s*unsigned).*$";
    if (lowerCaseType.matches(bigintRegex)) {
      return DataTypeInfoEnum.BIGINT;
    }
    String integerRegex = "^(mediumint|int|smallint\\s*(\\(\\s*\\d+\\s*\\))?\\s*unsigned).*$";
    if (lowerCaseType.matches(integerRegex)) {
      return DataTypeInfoEnum.INT;
    }

    String smallintRegex = "^(smallint\\s*(\\(\\s*\\d+\\s*\\))?).*$";
    if (lowerCaseType.matches(smallintRegex)) {
      return DataTypeInfoEnum.SMALLINT;
    }

    String booleanRegex = "^(boolean|tinyint\\(1\\))$";
    if (lowerCaseType.matches(booleanRegex)) {
      return DataTypeInfoEnum.BOOLEAN;
    }
    String tinyintUnsignedRegex = "^(tinyint\\s*(\\(\\s*\\d+\\s*\\))?\\s*unsigned).*$";
    if (lowerCaseType.matches(tinyintUnsignedRegex)) {
      return DataTypeInfoEnum.SMALLINT;
    }
    String tinyintRegex = "^(tinyint).*$";
    if (lowerCaseType.matches(tinyintRegex)) {
      return DataTypeInfoEnum.TINYINT;
    }
    String floatRegex = "^(float).*$";
    if (lowerCaseType.matches(floatRegex)) {
      return DataTypeInfoEnum.FLOAT;
    }
    String doubleRegex = "^(double|double precision).*$";
    if (lowerCaseType.matches(doubleRegex)) {
      return DataTypeInfoEnum.DOUBLE;
    }
    String numericRegex = "^(numeric|decimal).*$";
    if (lowerCaseType.matches(numericRegex)) {
      return DataTypeInfoEnum.DECIMAL;
    }
    String charRegex = "^(char|varchar|text).*$";
    if (lowerCaseType.matches(charRegex)) {
      return DataTypeInfoEnum.STRING;
    }
    return null;
  }

  /**
   * Converts mysql type to flink sql type.
   */
  public static String convert2FlinkSqlType(String mysqlType) {
    MysqlTypeEnum mysqlTypeEnum = null;
    for (MysqlTypeEnum value : MysqlTypeEnum.values()) {
      if (value.matches(mysqlType.toUpperCase())) {
        mysqlTypeEnum = value;
      }
    }
    Preconditions.checkNotNull(mysqlTypeEnum, "Invalid mysql type." + mysqlType);
    return mysqlTypeEnum.getFlinkSqlTypeBuilder().build(mysqlType);
  }

}
