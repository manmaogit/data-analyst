/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.core.constants;

public class CommonConstant {

  public static final String COMMA = ",";

  public static final String UNDERLINE = "_";

  public static final String DOT = ".";

  public static final String VERTICAL_LINE = "\\|";

  public static final String DATETIME_REGX = "^\\d{4}[-/]\\d{2}[-/]\\d{2}.{1}\\d{2}:\\d{2}:\\d{2}$";

  public static final String DATE_REGX = "^\\d{4}[-/]\\d{2}[-/]\\d{2}$";

  public static final String TIME_REGX = "^\\d{2}:\\d{2}:\\d{2}$";

  public static final String DEFAULT_DATE_STR = "1970-01-01";

  public static final String DEFAULT_TIME_STR = "00:00:00";

  public static final String DEFAULT_DATETIME_STR = "1970-01-01 00:00:00";

  public static final String DOLLAR = "$";

  public static final String BACK_SLASH = "\\";

  public static final String FORWARD_SLASH = "/";

  public static final String BACK_SLASH_QUOTE = "\\\"";

}
