/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.core.enums;

import com.rootcloud.analysis.core.data.type.ITypeBuilder;
import java.util.regex.Pattern;
import lombok.Getter;

@Getter
public enum SqlServerTypeEnum {

  TINYINT(Pattern.compile("^TINYINT$"), param -> "TINYINT", param -> DataTypeInfoEnum.TINYINT),
  SMALLINT(Pattern.compile("^SMALLINT$"), param -> "SMALLINT", param -> DataTypeInfoEnum.SMALLINT),
  INT(Pattern.compile("^INT$"), param -> "INT", param -> DataTypeInfoEnum.INT),
  BIGINT(Pattern.compile("^BIGINT$"), param -> "BIGINT", param -> DataTypeInfoEnum.BIGINT),

  REAL(Pattern.compile("^REAL$"), param -> "FLOAT", param -> DataTypeInfoEnum.FLOAT),
  FLOAT(Pattern.compile("^FLOAT\\s*(\\(\\s*([0-9]|[1-4][0-9]|5[0-3])+\\s*\\))?\\s*$"),
      param -> "DOUBLE", param -> DataTypeInfoEnum.DOUBLE),

  NUMERIC(Pattern.compile("^NUMERIC\\s*(\\(\\s*\\d+\\s*,\\s*\\d+\\s*\\))?\\s*$"),
      param -> "DECIMAL" + param.substring(7), param -> DataTypeInfoEnum.DECIMAL),
  DECIMAL(Pattern.compile("^DECIMAL\\s*(\\(\\s*\\d+\\s*,\\s*\\d+\\s*\\))?\\s*$"), param ->
      param.toUpperCase(), param -> DataTypeInfoEnum.DECIMAL),
  MONEY(Pattern.compile("^MONEY$"),
      param -> "DECIMAL(15,4)", param -> DataTypeInfoEnum.DECIMAL),
  SMALLMONEY(Pattern.compile("^SMALLMONEY$"), param ->
      "DECIMAL(6,4)", param -> DataTypeInfoEnum.DECIMAL),

  BIT(Pattern.compile("^BIT$"), param -> "BOOLEAN", param -> DataTypeInfoEnum.BOOLEAN),

  DATE(Pattern.compile("^DATE$"), param -> "DATE", param -> DataTypeInfoEnum.DATE),
  TIME(Pattern.compile("^TIME\\s*(\\(\\s*[0-7]\\s*\\))?\\s*"), param -> param.toUpperCase(),
      param -> DataTypeInfoEnum.TIME),
  DATETIME(Pattern.compile("^DATETIME$"), param ->
      "TIMESTAMP(3)", param -> DataTypeInfoEnum.TIMESTAMP),
  DATETIME2(Pattern.compile("^DATETIME2$"), param ->
      "TIMESTAMP(7)", param -> DataTypeInfoEnum.TIMESTAMP),
  SMALLDATETIME(Pattern.compile("^SMALLDATETIME$"), param ->
      "TIMESTAMP(0)", param -> DataTypeInfoEnum.TIMESTAMP),

  CHAR(Pattern.compile("^CHAR\\s*(\\(\\s*\\d+\\s*\\))?\\s*$"), param -> "STRING",
      param -> DataTypeInfoEnum.STRING),
  NCHAR(Pattern.compile("^NCHAR\\s*(\\(\\s*\\d+\\s*\\))?\\s*$"), param -> "STRING",
      param -> DataTypeInfoEnum.STRING),
  VARCHAR(Pattern.compile("^VARCHAR\\s*(\\(\\s*\\d+\\s*\\))?\\s*$"), param -> "STRING",
      param -> DataTypeInfoEnum.STRING),
  NVARCHAR(Pattern.compile("^NVARCHAR\\s*(\\(\\s*\\d+\\s*\\))?\\s*$"), param -> "STRING",
      param -> DataTypeInfoEnum.STRING),
  TEXT(Pattern.compile("^TEXT$"), param -> "STRING", param -> DataTypeInfoEnum.STRING),
  NTEXT(Pattern.compile("^NTEXT$"), param -> "STRING", param -> DataTypeInfoEnum.STRING),

  BINARY(Pattern.compile("^BINARY\\s*(\\(\\s*\\d+\\s*\\))?\\s*$"), param -> "STRING",
      param -> DataTypeInfoEnum.STRING),
  VARBINARY(Pattern.compile("^VARBINARY\\s*(\\(\\s*\\d+\\s*\\))?\\s*$"), param -> "STRING",
      param -> DataTypeInfoEnum.STRING),
  ;

  private final Pattern pattern;

  private final ITypeBuilder<String> flinkSqlTypeBuilder;
  private final ITypeBuilder<DataTypeInfoEnum> dataTypeInfoEnumBuilder;


  SqlServerTypeEnum(Pattern pattern, ITypeBuilder flinkSqlTypeBuilder,
                    ITypeBuilder dataTypeInfoEnumBuilder) {
    this.pattern = pattern;
    this.flinkSqlTypeBuilder = flinkSqlTypeBuilder;
    this.dataTypeInfoEnumBuilder = dataTypeInfoEnumBuilder;
  }

  /**
   * 是否包含以上枚举类型.
   */
  public static Boolean contains(String dataType) {
    for (SqlServerTypeEnum type : SqlServerTypeEnum.values()) {
      if (type.matches(dataType)) {
        return true;
      }
    }
    return false;
  }

  public boolean matches(String input) {
    return pattern.matcher(input.toUpperCase()).matches();
  }
}
