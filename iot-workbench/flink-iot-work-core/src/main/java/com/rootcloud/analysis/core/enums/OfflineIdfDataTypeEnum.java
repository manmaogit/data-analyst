/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.core.enums;

/**
 * idf数据类型和iotworks数据类型的映射关系.
 */
public enum OfflineIdfDataTypeEnum {
  STRING(DataTypeEnum.STRING), DIM(DataTypeEnum.STRING), DATE(DataTypeEnum.STRING),
  DATE_TIME(DataTypeEnum.STRING), TIME(DataTypeEnum.STRING),
  DATE_RANGE(DataTypeEnum.STRING), DATE_TIME_RANGE(DataTypeEnum.STRING),
  TIME_RANGE(DataTypeEnum.STRING), FLOAT(DataTypeEnum.DOUBLE),
  DOUBLE(DataTypeEnum.DOUBLE),
  ONLINE_ENUM(DataTypeEnum.STRING),
  WORKING_STATUS_ENUM(DataTypeEnum.STRING), ASSET(DataTypeEnum.STRING),
  INT(DataTypeEnum.BIGINT),BIGINT(DataTypeEnum.BIGINT),
  ENUM(DataTypeEnum.STRING), THING(DataTypeEnum.STRING);

  private DataTypeEnum iotworksDataType;

  /**
   * 构造.
   */
  OfflineIdfDataTypeEnum(DataTypeEnum string) {
    this.iotworksDataType = string;
  }

  /**
   * 通过idf类型获取iotworks数据类型.
   */
  public static DataTypeEnum getIotworksDataType(String idfType) {
    for (OfflineIdfDataTypeEnum idfDataTypeEnum : OfflineIdfDataTypeEnum.values()) {
      if (idfType.equals(idfDataTypeEnum.name())) {
        return idfDataTypeEnum.iotworksDataType;
      }
    }
    return DataTypeEnum.STRING;
  }

}
