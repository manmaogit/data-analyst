/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.common.entity;

import com.rootcloud.analysis.common.util.BeanUtil;

import java.io.Serializable;
import java.util.Map;
import lombok.ToString;

@ToString
public class RcUnitFrame implements Serializable {

  private static final long serialVersionUID = 1134916718975235426L;
  private Map<String, String> header;
  private RcUnit rcu;

  public RcUnitFrame() {
  }

  public RcUnitFrame(RcUnit unit) {
    this.rcu = unit;
  }

  public Map<String, String> getHeader() {
    return header;
  }

  public void setHeader(Map<String, String> header) {
    this.header = header;
  }

  public RcUnit getRcu() {
    return rcu;
  }

  public void setRcu(RcUnit rcu) {
    this.rcu = rcu;
  }

  public Object getObject(Class clazz) {
    return BeanUtil.convertMapToBean(rcu.getData(), clazz);
  }

}

