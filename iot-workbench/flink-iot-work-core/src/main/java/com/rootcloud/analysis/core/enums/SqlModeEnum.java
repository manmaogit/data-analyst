/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.core.enums;

public enum SqlModeEnum {
  TABLE("直接选表"), QUERY("sql查询");
  private String desc;

  SqlModeEnum(String desc) {
    this.desc = desc;
  }
}
