/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.core.dolphin;

import com.rootcloud.analysis.core.enums.dolphin.DolphinDependDateValueEnum;
import com.rootcloud.analysis.core.enums.dolphin.DolphinDependItemCycleEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DependItem implements DolphinProcessDefinitionChildParam {

  private Integer projectId;

  private String projectName;

  private Integer definitionId;

  private String definitionName;

  private String depTasks;

  private String status;

  private DolphinDependItemCycleEnum cycle;

  private DolphinDependDateValueEnum dateValue;
}
