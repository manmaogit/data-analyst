/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.core.exec;

import java.util.ArrayList;
import java.util.List;

/**
 * DAG计划模型执行计划.
 */
public class RcGraphLogicalPlan {
  private String acturator;
  private final List<Stage> stages = new ArrayList<>(16);

  public String getActurator() {
    return acturator;
  }

  public void setActurator(String acturator) {
    this.acturator = acturator;
  }

  public String acturator() {
    return acturator;
  }

  public RcGraphLogicalPlan acturator(String acturator) {
    this.acturator = acturator;
    return this;
  }

}
