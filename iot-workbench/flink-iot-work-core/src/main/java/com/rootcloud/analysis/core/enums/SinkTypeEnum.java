/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.core.enums;

public enum SinkTypeEnum {
  MYSQL, PG, ORACLE, SQLSERVER, MONGO, KAFKA, HIVE, HDFS, POSTGRESQL, REST, ASSET, INNER_HIVE,
  MYSQL_02,
  PGSQL_02,
  PGSQL_03,
  MYSQL_03,

  MYSQL_BATCH,
  PGSQL_BATCH,
  SQLSERVER_BATCH,
  CLICKHOUSE_BATCH

  ;

  /**
   * 根据类型获取enum.
   */
  public static SinkTypeEnum getType(String type) {
    for (SinkTypeEnum typeEnum : SinkTypeEnum.values()) {
      if (typeEnum.toString().equals(type)) {
        return typeEnum;
      }
    }
    return null;
  }

}
