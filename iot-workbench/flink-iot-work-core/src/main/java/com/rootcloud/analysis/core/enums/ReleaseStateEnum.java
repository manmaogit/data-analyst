/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.core.enums;

public enum ReleaseStateEnum {

  /**
   * 0 offline,
   * 1 on line.
   */
  OFFLINE, ONLINE;

  /**
   * getEnum.
   *
   * @param value 0 offline,1 on line.
   * @return
   */
  public static ReleaseStateEnum getEnum(int value) {
    for (ReleaseStateEnum e : ReleaseStateEnum.values()) {
      if (e.ordinal() == value) {
        return e;
      }
    }
    //For values out of enum scope
    return null;
  }
}
