/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.core.enums;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public enum TenantTypeEnum {
  PRIVATE("PRIVATE", "dedicated"), SHARED("SHARED", "shared");

  public String cmsCode;
  /**
   * iotworks租户类型与iam一致.
   */
  public String iamCode;

  /**
   * 使用iamCode获取枚举.
   */
  public static TenantTypeEnum convertFromIam(String code) {
    for (TenantTypeEnum tenantTypeEnum : TenantTypeEnum.values()) {
      if (tenantTypeEnum.iamCode.equals(code)) {
        return tenantTypeEnum;
      }
    }
    return null;
  }


}
