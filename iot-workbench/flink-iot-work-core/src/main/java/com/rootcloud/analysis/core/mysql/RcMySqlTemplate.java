/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.core.mysql;

import com.rootcloud.analysis.core.constants.CommonConstant;
import com.rootcloud.analysis.core.exception.RcDbException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RcMySqlTemplate {

  private static final Logger logger = LoggerFactory.getLogger(RcMySqlTemplate.class);

  private final String url;

  private final String username;

  private final String password;

  private Connection connection;

  /**
   * Constructor.
   *
   * @param uri      uri
   * @param username username
   * @param password password
   * @throws RcDbException throws RdbException
   */
  public RcMySqlTemplate(String uri, String username, String password) throws RcDbException {
    this.url = uri;
    this.username = username;
    this.password = password;
    this.connect();
  }

  /**
   * 创建表.
   *
   * @param ddl ddl
   * @throws RcDbException throws RdbException
   */
  public void createTable(String ddl) throws RcDbException {
    if (isClosed()) {
      throw new RcDbException("No operations allowed after connection closed.");
    }
    Statement state = null;
    try {
      connection.setAutoCommit(true);
      state = connection.createStatement();
      state.execute(ddl);
    } catch (SQLException e) {
      logger.error(e.getMessage(), e);
      throw new RcDbException(e.toString(), e);
    } finally {
      if (state != null) {
        try {
          state.close();
        } catch (SQLException e) {
          logger.error(e.getMessage(), e);
          throw new RcDbException(e.toString(), e);
        }
      }
    }
  }

  /**
   * 断开连接.
   *
   * @throws RcDbException throws RdbException
   */
  public void disconnect() throws RcDbException {
    if (connection != null) {
      try {
        connection.close();
        connection = null;
      } catch (SQLException e) {
        logger.error(e.getMessage(), e);
        throw new RcDbException(e.toString(), e);
      }
    }
  }

  /**
   * 判断是否关闭.
   *
   * @return true/false
   * @throws RcDbException throws RcDbException
   */
  public boolean isClosed() throws RcDbException {
    try {
      return connection == null || connection.isClosed();
    } catch (SQLException e) {
      logger.error(e.getMessage(), e);
      throw new RcDbException(e.toString(), e);
    }
  }

  /**
   * 建立连接.
   *
   * @throws RcDbException throws RdbException
   */
  private void connect() throws RcDbException {
    try {
      Class.forName("com.mysql.cj.jdbc.Driver");
      connection = DriverManager.getConnection(url, username, password);
    } catch (ClassNotFoundException e) {
      logger.error(e.getMessage(), e);
      throw new RcDbException(e.toString(), e);
    } catch (SQLException e) {
      logger.error(e.getMessage(), e);
      throw new RcDbException(e.toString(), e);
    }
  }

  /**
   * 获取MySQL private key.
   */
  public String getPrimaryKey(String tableSchema, String table) throws RcDbException {

    String sql = "SELECT column_name FROM INFORMATION_SCHEMA.`KEY_COLUMN_USAGE` WHERE table_name='"
        + table + "' AND CONSTRAINT_SCHEMA='" + tableSchema + "' AND constraint_name='PRIMARY'";

    if (isClosed()) {
      throw new RcDbException("No operations allowed after connection closed.");
    }
    Statement state = null;
    try {
      connection.setAutoCommit(true);
      state = connection.prepareStatement(sql);
      state.execute(sql);
      ResultSet rs = state.getResultSet();
      String primaryKeys = "";
      while (rs.next()) {
        primaryKeys += rs.getString("COLUMN_NAME") + CommonConstant.COMMA;
      }
      return primaryKeys.substring(0, primaryKeys.length() - 1);
    } catch (SQLException e) {
      logger.error(e.getMessage(), e);
      throw new RcDbException(e.toString(), e);
    } finally {
      if (state != null) {
        try {
          state.close();
        } catch (SQLException e) {
          logger.error(e.getMessage(), e);
          throw new RcDbException(e.toString(), e);
        }
      }
    }
  }

  /**
   * 获取MySQL not IS_NULLABLE key.
   */
  public String getNotIsNullableColumns(String tableSchema, String table) throws RcDbException {
    //https://dev.mysql.com/doc/refman/8.0/en/information-schema-columns-table.html
    String sql = "SELECT column_name FROM INFORMATION_SCHEMA.`COLUMNS` WHERE table_name='"
        + table + "' AND TABLE_SCHEMA='" + tableSchema + "' AND is_nullable='NO'";
    return executeSql(sql);
  }

  /**
   * 获取MySQL primary but not auto increment key.
   */
  public String getPrimaryButNotAutoIncrementKey(String tableSchema, String table)
      throws RcDbException {
    String sql = "SELECT column_name FROM INFORMATION_SCHEMA.`COLUMNS` WHERE table_name='"
        + table + "' AND TABLE_SCHEMA='" + tableSchema
        + "' AND column_key='PRI' AND NOT extra='auto_increment'";
    return executeSql(sql);
  }

  /**
   * 执行sql语句.
   */
  public String executeSql(String sql) throws RcDbException {
    if (isClosed()) {
      throw new RcDbException("No operations allowed after connection closed.");
    }
    Statement state = null;
    try {
      connection.setAutoCommit(true);
      state = connection.prepareStatement(sql);
      state.execute(sql);
      ResultSet rs = state.getResultSet();
      String primaryKeys = "";
      while (rs.next()) {
        primaryKeys += rs.getString("COLUMN_NAME") + CommonConstant.COMMA;
      }

      if (StringUtils.isBlank(primaryKeys)) {
        return primaryKeys;
      }
      return primaryKeys.substring(0, primaryKeys.length() - 1);
    } catch (SQLException e) {
      logger.error(e.getMessage(), e);
      throw new RcDbException(e.toString(), e);
    } finally {
      if (state != null) {
        try {
          state.close();
        } catch (SQLException e) {
          logger.error(e.getMessage(), e);
          throw new RcDbException(e.toString(), e);
        }
      }
    }
  }


}
