/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.core.exception;

public class FailedActionException extends Exception {
  public FailedActionException() {
  }

  public FailedActionException(String message) {
    super(message);
  }

  public FailedActionException(String message, Throwable cause) {
    super(message, cause);
  }

  public FailedActionException(Throwable cause) {
    super(cause);
  }

  public FailedActionException(String message, Throwable cause, boolean enableSuppression,
                               boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }
}
