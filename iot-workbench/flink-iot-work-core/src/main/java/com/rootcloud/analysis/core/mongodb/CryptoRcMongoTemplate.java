/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2020 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.core.mongodb;

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.mongodb.ServerAddress;
import com.rootcloud.analysis.core.util.RsaUtil;

import java.net.URISyntaxException;
import java.util.ArrayList;

public class CryptoRcMongoTemplate extends RcMongoTemplate {

  /**
   * Constructor.
   *
   * @param mongoUri mongoUri
   */
  public CryptoRcMongoTemplate(String mongoUri, String privateKey) throws URISyntaxException {
    super(mongoUri);
    Preconditions.checkArgument(!Strings.isNullOrEmpty(mongoUri), "Mongo uri is null or empty.");

    super.seeds = new ArrayList<>();
    if (mongoUri.indexOf("@") > 0) {
      String userInfoStr = mongoUri.substring(mongoUri.indexOf("//") + 2, mongoUri.indexOf("@"));
      if (!Strings.isNullOrEmpty(userInfoStr)) {
        String[] userInfo = userInfoStr.split(HOST_PORT_SEPARATOR);
        super.username = userInfo[0];
        super.password = RsaUtil.decryptDataOnJava(userInfo[1], privateKey);
      }
    }

    String[] mongoUriArr = mongoUri
        .substring(Math.max(mongoUri.indexOf("//") + 2, mongoUri.indexOf("@") + 1))
        .split(SERVER_ADDR_SEPARATOR);

    if (mongoUriArr.length > 0) {
      for (int i = 0; i < mongoUriArr.length; i++) {
        String[] hostPortArr = mongoUriArr[i].split(HOST_PORT_SEPARATOR);
        String host = hostPortArr[0];
        int port;
        if (hostPortArr[1].indexOf("/") > 0) {
          port = Integer.parseInt(hostPortArr[1].substring(0, hostPortArr[1].indexOf("/")));
        } else {
          port = Integer.parseInt(hostPortArr[1]);
        }
        super.seeds.add(new ServerAddress(host, port));
      }
    }
    super.connect();
  }

}