/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2020 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.core.graph;

import java.util.Iterator;

// 定义一个背包集合，支持泛型，支持迭代
public class Bag<T> implements Iterable<T> {

  private class BagNode<T> {
    T item;
    BagNode next;
  }

  BagNode head;
  int size;

  @Override
  public Iterator<T> iterator() {
    return new Iterator<T>() {
      BagNode node = head;

      @Override
      public boolean hasNext() {
        return node.next != null;
      }

      @Override
      public T next() {
        T item = (T) node.item;
        node = node.next;
        return item;
      }
    };
  }

  public Bag() {
    head = new BagNode();
    size = 0;
  }

  /**
   * 往前插入.
   */
  public void add(T item) {
    BagNode temp = new BagNode();
    // 以下两行代码一定要声明，不可直接使用temp = head，那样temp赋值的是head的引用，对head的所有修改会直接同步到temp，temp就不具备缓存的功能，引发bug。。
    temp.next = head.next;
    temp.item = head.item;
    head.item = item;
    head.next = temp;
    size++;
  }

  public boolean isEmpty() {
    return size == 0;
  }

  public int size() {
    return this.size;
  }

}
