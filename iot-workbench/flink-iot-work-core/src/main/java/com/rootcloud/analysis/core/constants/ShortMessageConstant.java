/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.core.constants;

public class ShortMessageConstant {

  /**
   * 创建定时任务.
   */
  public static final String CREATE_SCHEDULE_TASK = "CREATE_SCHEDULE_TASK";

  /**
   * 删除定时任务.
   */
  public static final String DELETE_SCHEDULE_TASK = "DELETE_SCHEDULE_TASK";

  /**
   * 更新定时任务.
   */
  public static final String UPDATE_SCHEDULE_TASK = "UPDATE_SCHEDULE_TASK";

  /**
   * 实时任务启动.
   */
  public static final String START_REAL_TIME_JOB = "START_REAL_TIME_JOB";

  /**
   * 实时任务停止.
   */
  public static final String STOP_REAL_TIME_JOB = "STOP_REAL_TIME_JOB";

  /**
   * 创建外部数据源.
   */
  public static final String CREATE_EXTERNAL_DATASOURCE = "CREATE_EXTERNAL_DATASOURCE";
  /**
   * 删除外部数据源.
   */
  public static final String DELETE_EXTERNAL_DATASOURCE = "DELETE_EXTERNAL_DATASOURCE";
  /**
   * 更新外部数据源.
   */
  public static final String UPDATE_EXTERNAL_DATASOURCE = "UPDATE_EXTERNAL_DATASOURCE";

  /**
   * 创建job任务.
   */
  public static final String CREATE_JOB = "CREATE_JOB";
  /**
   * 删除job.
   */
  public static final String DELETE_JOB = "DELETE_JOB";
  /**
   * 更新job.
   */
  public static final String UPDATE_JOB = "UPDATE_JOB";
  /**
   * 发布job.
   */
  public static final String RELEASE_JOB = "RELEASE_JOB";
  /**
   * 撤回job.
   */
  public static final String EDITING_JOB = "EDITING_JOB";
  /**
   * 更新job并行度.
   */
  public static final String UPDATE_JOB_PARALLELISM = "UPDATE_JOB_PARALLELISM";
  /**
   * 启动job.
   */
  public static final String START_JOB = "START_JOB";
  /**
   * 停止job.
   */
  public static final String STOP_JOB = "STOP_JOB";
  /**
   * 启动离线任务.
   */
  public static final String START_OFFLINE_JOB = "START_OFFLINE_JOB";
  /**
   * 删除dolphinschecule流程定义.
   */
  public static final String DELETE_SCHEDULER_PROCESS_DEFINITION =
      "DELETE_SCHEDULER_PROCESS_DEFINITION";
  /**
   * 创建job模板.
   */
  public static final String CREATE_JOB_TEMPLATE = "CREATE_JOB_TEMPLATE";
  /**
   * 删除任务模板.
   */
  public static final String DELETE_JOB_TEMPLATE = "DELETE_JOB_TEMPLATE";


  /**
   * 删更新务模板.
   */
  public static final String UPDATE_JOB_TEMPLATE = "UPDATE_JOB_TEMPLATE";


  /**
   * 查询流转数据元素.
   */
  public static final String SEARCH_PROCESS_ELEMENT = "SEARCH_PROCESS_ELEMENT";
  /**
   * 插入mysql数据.
   */
  public static final String INSERT_MYSQL_DATA = "INSERT_MYSQL_DATA";
  /**
   * 关闭mysql数据库连接.
   */
  public static final String CLOSE_MYSQL = "CLOSE_MYSQL";
  /**
   * 插入oracle数据.
   */

  public static final String INSERT_ORACLE_DATA = "INSERT_ORACLE_DATA";
  /**
   * 关闭oracle连接.
   */

  public static final String CLOSE_ORACLE = "CLOSE_ORACLE";

  /**
   * 插入关闭sqlServer连接数据.
   */

  public static final String INSERT_SQLSERVER_DATA = "INSERT_SQLSERVER_DATA";

  /**
   * 关闭sqlServer连接.
   */
  public static final String CLOSE_SQLSERVER = "CLOSE_SQLSERVER";

  /**
   * 插入关闭 ClickHouse 连接数据.
   */

  public static final String INSERT_CLICKHOUSE_DATA = "INSERT_CLICKHOUSE_DATA";

  /**
   * 关闭 ClickHouse 连接.
   */
  public static final String CLOSE_CLICKHOUSE = "CLOSE_CLICKHOUSE";

  /**
   * 插入postgresql数据.
   */

  public static final String INSERT_POSTGRESQL_DATA = "INSERT_POSTGRESQL_DATA";

  public static final String INIT_SCHEMA = "INIT_SCHEMA";

  /**
   * 关闭postgreSql连接.
   */

  public static final String CLOSE_POSTGRESQL = "CLOSE_POSTGRESQL";
  /**
   * 消费kafka数据.
   */

  public static final String READ_KAFKA_DATA = "READ_KAFKA_DATA";
  /**
   * 消费mongo数据.
   */

  public static final String READ_MONGO_DATA = "READ_MONGO_DATA";
  /**
   * 消费mysql数据.
   */

  public static final String READ_MYSQL_DATA = "READ_MYSQL_DATA";
  /**
   * 消费oracle数据.
   */

  public static final String READ_ORACLE_DATA = "READ_ORACLE_DATA";

  /**
   * 消费sqlServer数据.
   */

  public static final String READ_SQL_SERVER_DATA = "READ_SQL_SERVER_DATA";

  /**
   * 读取postgreSql数据.
   */
  public static final String READ_POSTGRESQL_DATA = "READ_POSTGRESQL_DATA";

  /**
   * 读取jdbc数据.
   */
  public static final String READ_JDBC_DATA = "READ_JDBC_DATA";

  /**
   * 读取hive数据.
   */
  public static final String READ_HIVE_DATA = "READ_HIVE_DATA";

  /**
   * 定时扫描任务.
   */

  public static final String SATIC_SCHEDULE_TASK = "SATIC_SCHEDULE_TASK";

  public static final String SEARCH_ATTRIBUTE_CONNECT = "SEARCH_ATTRIBUTE_CONNECT";

  /**
   * 请求 restApi.
   */
  public static final String INVOKE_REST_API = "INVOKE_REST_API";

  /**
   * 获取 restApi 返回结果.
   */
  public static final String READ_REST_API_RESPONSE = "READ_REST_API_RESPONSE";

  public static final String CLOSE_REST_API = "CLOSE_REST_API";

  public static final String PROCESS_BROADCAST_ELEMENT = "PROCESS_BROADCAST_ELEMENT";

  public static final String PROCESS_SQL_RESULT = "PROCESS_SQL_RESULT";

  public static final String FILTER_SCHEMA_DATA = "FILTER_SCHEMA_DATA";

  public static final String EXTRACT_TIMESTAMP = "EXTRACT_TIMESTAMP";


  /**
   * 发布调度.
   */
  public static final String RELEASE_SCHEDULE = "RELEASE_SCHEDULE";
  /**
   * 撤回调度.
   */
  public static final String EDITING_SCHEDULE = "EDITING_SCHEDULE";

  /**
   * 上线调度.
   */
  public static final String ONLINE_SCHEDULE = "ONLINE_SCHEDULE";

  /**
   * 下线调度.
   */
  public static final String OFFLINE_SCHEDULE = "OFFLINE_SCHEDULE";

  /**
   * 删除调度.
   */
  public static final String DELETE_SCHEDULE = "DELETE_SCHEDULE";

  /**
   * 调度补数.
   */
  public static final String COMPLEMENT_DATA = "COMPLEMENT_DATA";

  /**
   * 更新调度.
   */
  public static final String UPDATE_SCHEDULE = "UPDATE_SCHEDULE";

  /**
   * 创建调度.
   */
  public static final String CREATE_SCHEDULE = "CREATE_SCHEDULE";

  /**
   * 停止工作流实例.
   */
  public static final String STOP_INSTANCE = "STOP_INSTANCE";

  /**
   * 重跑工作流实例.
   */
  public static final String REPEAT_RUN_INSTANCE = "REPEAT_RUN_INSTANCE";

  /**
   * 删除工作流实例.
   */
  public static final String DELETE_INSTANCE = "DELETE_INSTANCE";

  /**
   * 消费InfluxDb数据.
   */
  public static final String READ_INFLUXDB_DATA = "READ_INFLUXDB_DATA";

  /**
   * 关闭InfluxDb数据库连接.
   */
  public static final String CLOSE_INFLUXDB = "CLOSE_INFLUXDB";

  /**
   * 创建工作流.
   */
  public static final String CREATE_WORKFLOW = "CREATE_WORKFLOW";
  /**
   * 删除工作流.
   */
  public static final String DELETE_WORKFLOW = "DELETE_WORKFLOW";
  /**
   * 更新工作流.
   */
  public static final String UPDATE_WORKFLOW = "UPDATE_WORKFLOW";

  /**
   * 发布工作流.
   */
  public static final String RELEASE_WORKFLOW = "RELEASE_WORKFLOW";
  /**
   * 撤回工作流.
   */
  public static final String WITHDRAWAL_WORKFLOW = "WITHDRAWAL_WORKFLOW";

  /**
   * 启动/停止已分发数据计算任务成功.
   */
  public static final String OPERATION_DISTRIBUTE_JOB = "OPERATION_DISTRIBUTE_JOB";

  /**
   * 分发数据计算任务成功.
   */
  public static final String RELEASE_DISTRIBUTE_JOB = "RELEASE_DISTRIBUTE_JOB";

  /**
   * 新增或修改IDF数据.
   */
  public static final String UPSERT_REST_DATA = "UPSERT_REST_DATA";

  /**
   * 关闭与IDF通信的HTTP连接.
   */
  public static final String CLOSE_REST_HTTP = "CLOSE_REST_HTTP";

  public static final String ADD_UDF = "ADD_UDF";
}
