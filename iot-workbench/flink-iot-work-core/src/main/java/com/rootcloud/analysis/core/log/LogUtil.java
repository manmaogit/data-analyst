/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.core.log;

import com.alibaba.fastjson.JSON;
import com.rootcloud.analysis.core.enums.LogLogLevelEnum;
import com.rootcloud.analysis.core.graylog.LogVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LogUtil {

  private static Logger log = LoggerFactory.getLogger(LogUtil.class);

  /**
   * grayLog 记录info级别的日志.
   *
   */
  public static void info(LogVo logVo) {
    logVo.setLogLevel(LogLogLevelEnum.INFO.getLogLevel());
    log.info(JSON.toJSONString(logVo));
  }


  /**
   * grayLog 记录warn级别的日志.
   *
   */
  public static void warn(LogVo logVo) {
    logVo.setLogLevel(LogLogLevelEnum.WARN.getLogLevel());
    log.info(JSON.toJSONString(logVo));
  }

  /**
   * grayLog 记录error级别的日志.
   *
   */
  public static void error(LogVo logVo) {
    logVo.setLogLevel(LogLogLevelEnum.ERROR.getLogLevel());
    log.info(JSON.toJSONString(logVo));
  }

  /**
   * grayLog 记录alarm级别的日志,将会触发告警推送.
   *
   */
  public static void alarm(LogVo logVo) {
    error(logVo);
    //AlarmGrayLogUtils.alarm(grayLogVo);

  }
}
