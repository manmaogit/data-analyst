/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.core.enums;

/**
 * 历史数据节点（目前包含InfluxDB和TdEngine）的时间分组单位.
 *
 * @CreatedBy: zexin.huang
 * @Date: 2021/11/10 2:27 下午
 */
public enum HistorianGroupByTimeTypeEnum {

  /**
   * 分钟.
   */
  MIN,

  /**
   * 小时.
   */
  HOUR
  ;
}
