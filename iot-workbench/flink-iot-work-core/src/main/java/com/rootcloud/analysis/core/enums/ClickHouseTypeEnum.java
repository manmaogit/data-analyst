/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.core.enums;

import com.rootcloud.analysis.core.data.type.ITypeBuilder;
import java.util.regex.Pattern;
import lombok.Getter;

@Getter
public enum ClickHouseTypeEnum {
  INT8(
      Pattern.compile("^(NULLABLE\\()?INT8\\)?$"),
          param -> "TINYINT",
          param -> DataTypeInfoEnum.TINYINT),
  INT16(
      Pattern.compile("^(NULLABLE\\()?INT16\\)?$"),
          param -> "SMALLINT",
          param -> DataTypeInfoEnum.SMALLINT),
  INT32(
      Pattern.compile("^(NULLABLE\\()?INT32\\)?$"), param -> "INT", param -> DataTypeInfoEnum.INT),
  INT64(
      Pattern.compile("^(NULLABLE\\()?INT64\\)?$"),
          param -> "BIGINT",
          param -> DataTypeInfoEnum.BIGINT),

  INT128(
      Pattern.compile("^(NULLABLE\\()?INT128\\)?$"),
          param -> "STRING",
          param -> DataTypeInfoEnum.STRING),
  INT256(
      Pattern.compile("^(NULLABLE\\()?INT256"),
          param -> "STRING",
          param -> DataTypeInfoEnum.STRING),

  UINT8(
      Pattern.compile("^(NULLABLE\\()?UINT8\\)?$"),
          param -> "SMALLINT",
          param -> DataTypeInfoEnum.SMALLINT),
  UINT16(
      Pattern.compile("^(NULLABLE\\()?UINT16\\)?$"), param -> "INT", param -> DataTypeInfoEnum.INT),
  UINT32(
      Pattern.compile("^(NULLABLE\\()?UINT32\\)?$"),
          param -> "BIGINT",
          param -> DataTypeInfoEnum.BIGINT),
  UINT64(
      Pattern.compile("^(NULLABLE\\()?UINT64\\)?$"),
          param -> "STRING",
          param -> DataTypeInfoEnum.STRING),
  UINT128(
      Pattern.compile("^(NULLABLE\\()?UINT128\\)?$"),
          param -> "STRING",
          param -> DataTypeInfoEnum.STRING),
  UINT256(
      Pattern.compile("^(NULLABLE\\()?UINT256\\)?$"),
          param -> "STRING",
          param -> DataTypeInfoEnum.STRING),

  FLOAT32(
      Pattern.compile("^(NULLABLE\\()?FLOAT32\\)?$"),
          param -> "FLOAT",
          param -> DataTypeInfoEnum.FLOAT),
  FLOAT64(
      Pattern.compile("^(NULLABLE\\()?FLOAT64\\)?$"),
          param -> "DOUBLE",
          param -> DataTypeInfoEnum.DOUBLE),

  DECIMAL(
      Pattern.compile("^(NULLABLE\\()?DECIMAL\\s*(\\(\\s*\\d+\\s*,\\s*\\d+\\s*\\))?\\s*\\)?$"),
          param -> param.toUpperCase(),
          param -> DataTypeInfoEnum.DECIMAL),

  DECIMAL32(
      Pattern.compile("^(NULLABLE\\()?DECIMAL32\\s*(\\(\\s*\\d+\\s*\\))?\\s*\\)?$"),
          param -> "DECIMAL",
          param -> DataTypeInfoEnum.DECIMAL),
  DECIMAL64(
      Pattern.compile("^(NULLABLE\\()?DECIMAL64\\s*(\\(\\s*\\d+\\s*\\))?\\s*\\)?$"),
          param -> "DECIMAL",
          param -> DataTypeInfoEnum.DECIMAL),
  DECIMAL128(
      Pattern.compile("^(NULLABLE\\()?DECIMAL128\\s*(\\(\\s*\\d+\\s*\\))?\\s*\\)?$"),
          param -> "DECIMAL",
          param -> DataTypeInfoEnum.DECIMAL),
  DECIMAL256(
      Pattern.compile("^(NULLABLE\\()?DECIMAL256\\s*(\\(\\s*\\d+\\s*\\))?\\s*\\)?$"),
          param -> "STRING",
          param -> DataTypeInfoEnum.STRING),

  BOOL(
      Pattern.compile("^(NULLABLE\\()?BOOL\\)?$"),
          param -> "BOOLEAN",
          param -> DataTypeInfoEnum.BOOLEAN),

  STRING(
      Pattern.compile("^(NULLABLE\\()?STRING\\)?$"),
          param -> "STRING",
          param -> DataTypeInfoEnum.STRING),
  UUID(
      Pattern.compile("^(NULLABLE\\()?UUID\\)?$"),
          param -> "STRING",
          param -> DataTypeInfoEnum.STRING),

  ENUM(Pattern.compile("^(NULLABLE\\()?ENUM"), param -> "STRING", param -> DataTypeInfoEnum.STRING),

  FIXEDSTRING(
      Pattern.compile("^(NULLABLE\\()?FIXEDSTRING\\s*(\\(\\s*\\d+\\s*\\))?\\s*\\)?$"),
          param -> "STRING",
          param -> DataTypeInfoEnum.STRING),

  DATE(
      Pattern.compile("^(NULLABLE\\()?(DATE|DATE32)\\)?$"),
          param -> "DATE",
          param -> DataTypeInfoEnum.DATE),
  DATETIME(
      Pattern.compile("^(NULLABLE\\()?DATETIME\\s*(\\(\\s*\\S+\\s*\\))?\\s*\\)?$"),
          param -> "TIMESTAMP",
          param -> DataTypeInfoEnum.TIMESTAMP),

  DATETIME64(
      Pattern.compile(
          "^(NULLABLE\\()?DATETIME64\\s*(\\(\\s*\\d+\\s*(,\\s*\\S+\\s*)?\\))?\\s*\\)?$"),
          param -> "TIMESTAMP",
          param -> DataTypeInfoEnum.TIMESTAMP),
  ;

  private final Pattern pattern;

  private final ITypeBuilder<String> flinkSqlTypeBuilder;
  private final ITypeBuilder<DataTypeInfoEnum> dataTypeInfoEnumBuilder;

  ClickHouseTypeEnum(
      Pattern pattern, ITypeBuilder flinkSqlTypeBuilder, ITypeBuilder dataTypeInfoEnumBuilder) {
    this.pattern = pattern;
    this.flinkSqlTypeBuilder = flinkSqlTypeBuilder;
    this.dataTypeInfoEnumBuilder = dataTypeInfoEnumBuilder;
  }

  /** 是否包含以上枚举类型. */
  public static Boolean contains(String dataType) {
    for (ClickHouseTypeEnum type : ClickHouseTypeEnum.values()) {
      if (type.matches(dataType)) {
        return true;
      }
    }
    return false;
  }

  public boolean matches(String input) {
    return pattern.matcher(input.toUpperCase()).matches();
  }
}
