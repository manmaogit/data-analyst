/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.core.enums;

import com.rootcloud.analysis.core.data.type.ITypeBuilder;
import java.util.regex.Pattern;
import lombok.Getter;

@Getter
public enum OracleTypeEnum {

  TINYINT(Pattern.compile("^NUMBER\\s*(\\(\\s*[12]{1}\\s*,\\s*0\\s*\\))?\\s*$"), param -> {
    return "TINYINT";
  }),
  SMALLINT(
      Pattern.compile("^NUMBER\\s*(\\(\\s*[34]{1}\\s*,\\s*0\\s*\\))?\\s*$|^\\s*SMALLINT\\s*$"),
          param -> "SMALLINT"),
  INT(Pattern.compile(
      "^NUMBER\\s*(\\(\\s*(([56789]{1}))\\s*,\\s*0\\s*\\))?\\s*$|"
          + "^\\s*INTEGER\\s*$|^\\s*INT\\s*$"),
      param -> "INT"),
  BIGINT(
      Pattern.compile(
          "^NUMBER\\s*(\\(\\s*((10)|(1[12345678]{1}))\\s*,\\s*0\\s*\\))?\\s*$|"
              + "^INTERVAL\\s+DAY\\s+TO\\s+SECOND$|^INTERVAL\\s+YEAR\\s+TO\\s+MONTH$"),
          param -> "BIGINT"),
  DECIMAL(Pattern.compile("^DECIMAL$|^REAL$"), param ->
      param.toUpperCase()),
  DECIMALPS(Pattern.compile(
      "^DECIMAL\\s*(\\(\\s*\\d+\\s*,\\s*\\d\\s*\\)){1}\\s*$|"
          + "^NUMBER\\s*(\\(\\s*([23]{1}[^9]{1}|29|19)\\s*,\\s*0\\s*\\))?\\s*$|"
          + "^NUMBER\\s*(\\(\\s*\\d+\\s*,\\s*[^0]+\\s*\\))?\\s*$"),
      param -> "DECIMAL" + param.substring(7)),
  BOOLEAN(Pattern.compile(
      "^NUMBER\\s*(\\(\\s*1\\s*,\\s*0\\s*\\)){1}\\s*$|^NUMBER\\s*(\\(\\s*1\\s*\\)){1}\\s*$"),
      param -> "BOOLEAN"),

  FLOAT(Pattern.compile("^FLOAT\\s*(\\(\\s*\\d+\\s*\\))?\\s*$|^BINARY_FLOAT$"), param -> "FLOAT"),

  DOUBLE(Pattern.compile("^DOUBLE\\s*PRECISION$|^BINARY_DOUBLE$"), param -> "DOUBLE"),

  TIME(Pattern.compile("^TIME\\s*(\\(\\s*\\d+\\s*\\))?\\s*$"), param -> param.toUpperCase()),

  Timestamp(
      Pattern.compile("^TIMESTAMP\\s*(\\(\\s*\\d+\\s*\\))?\\s*$|"
          + "^TIMESTAMP\\s*(\\(\\s*\\d+\\s*\\))?\\s+WITH\\s+LOCAL\\s+TIME\\s+"
          + "ZONE$|^TIMESTAMP\\s*(\\(\\s*\\d+\\s*\\))?\\s+WITH\\s+TIME\\s+ZONE$|"
              + "^DATE\\s*(\\(\\s*\\d+\\s*\\))?\\s*$"),
          param -> "TIMESTAMP"),

  STRING(Pattern.compile(
      "^CHAR\\s*(\\(\\s*\\d+\\s*\\))?\\s*$|^CHAR VARYING$|^CHARACTER$|^CHARACTER VARYING$|"
          + "^VARCHAR\\s*(\\(\\s*\\d+\\s*\\))?\\s*$|^VARCHAR2\\s*(\\(\\s*\\d+\\s*\\))?\\s*$|"
          + "^NVARCHAR2\\s*(\\(\\s*\\d+\\s*\\))?\\s*$|"
          + "^LONG$|^LONG RAW$|^LONG VARCHAR$|"
          + "^NCHAR\\s*(\\(\\s*\\d+\\s*\\))?\\s*$|^NCHAR VARYING$|"
          + "^NUMBER\\s*(\\(\\s*\\d{2,}\\s*,\\s*0\\s*\\))?\\s*$"),
      param -> "STRING");


  private Pattern pattern;

  private ITypeBuilder<String> flinkSqlTypeBuilder;

  OracleTypeEnum(Pattern pattern, ITypeBuilder flinkSqlTypeBuilder) {
    this.pattern = pattern;
    this.flinkSqlTypeBuilder = flinkSqlTypeBuilder;
  }

  public boolean matches(String input) {
    return pattern.matcher(input.toUpperCase()).matches();
  }

  /**
   * 是否包含以上枚举类型.
   */
  public static Boolean contains(String dataType) {
    for (OracleTypeEnum type : OracleTypeEnum.values()) {
      if (type.matches(dataType)) {
        return true;
      }
    }
    return false;
  }
}
