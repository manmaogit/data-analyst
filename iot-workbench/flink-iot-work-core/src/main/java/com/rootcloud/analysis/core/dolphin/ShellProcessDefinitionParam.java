/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.core.dolphin;

import java.util.ArrayList;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ShellProcessDefinitionParam extends ProcessDefinitionBaseParam {

  private ShellParams params;


  /**
   * 通用配置.
   */
  public void buildDefaultParams() {
    this.setRunFlag("NORMAL");
    this.setType("SHELL");
    this.setMaxRetryTimes("0");
    this.setTaskInstancePriority("MEDIUM");
    this.setDependence(new Dependence());
    this.setRetryInterval("1");
    this.setPreTasks(new ArrayList<>());
    this.setWorkerGroup("default");
  }

}
