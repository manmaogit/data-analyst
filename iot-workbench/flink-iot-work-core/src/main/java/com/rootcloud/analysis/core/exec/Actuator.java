/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.core.exec;

/**
 * Actuator.
 *
 * <p>根据逻辑执行计划{@link RcGraphLogicalPlan}启动相应执行引擎的执行器执行.
 */
public interface Actuator {

  void execute() throws Exception;

}
