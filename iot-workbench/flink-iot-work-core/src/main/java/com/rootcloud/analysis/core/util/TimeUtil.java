/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.core.util;

import java.text.SimpleDateFormat;
import java.util.Date;

public class TimeUtil {


  /**
   * formate Date to yyyy-MM-dd HH:mm:ss.
   *
   * @param date date
   *
   * @return string
   */
  public static String formatDateToString(Date date) {
    String pattern = "yyyy-MM-dd HH:mm:ss";
    SimpleDateFormat sdf = new SimpleDateFormat(pattern);
    return sdf.format(date);
  }

}
