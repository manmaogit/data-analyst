/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.core.dolphin;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

@Data
public class ProcessDefinitionLocation {

  private String name;
  /**
   * 前节点id.
   */
  private String targetarr;
  /**
   * 子节点数量.
   */
  private Integer nodenumber;

  @JSONField(name = "x")
  private Integer indexX;
  @JSONField(name = "y")
  private Integer indexY;


}
