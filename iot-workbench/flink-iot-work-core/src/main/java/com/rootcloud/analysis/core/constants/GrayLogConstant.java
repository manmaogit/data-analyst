/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.core.constants;

import com.rootcloud.analysis.core.enums.GragLogModuleEnum;

public class GrayLogConstant {

  public static final String K8S_SERVICE_NAME = "analysis-api";

  //public static final String MODULE_NAME = "数据计算平台-IOTWORKS";
  public static final String MODULE_NAME = GragLogModuleEnum.IOTWORKS.getModuleValue();

}
