/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.common.entity;

import com.alibaba.fastjson.JSONObject;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class RestNodeVo {

  private String originalName;
  private String path;
  private Object value;

  public RestNodeVo(Object value) {
    this.value = value;
  }

  /**
   * 构造方法.
   */
  public RestNodeVo(Object value, String queue) {
    this.value = value;
    this.path = queue;
  }

  /**
   * 构造方法.
   */
  public RestNodeVo(Object value, String queue, String name) {
    this.value = value;
    this.path = queue;
    this.originalName = name;
  }

  /**
   * 构造方法.
   */
  public RestNodeVo() {

  }

  /**
   * 获取值.
   */
  public Object getValueValue(String paramStr) {
    if (value instanceof JSONObject) {
      return ((JSONObject) value).get(paramStr);
    } else {
      return value;
    }
  }
}
