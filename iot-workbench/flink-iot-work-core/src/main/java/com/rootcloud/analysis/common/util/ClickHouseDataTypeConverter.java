/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2020 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.common.util;

import com.google.common.base.Preconditions;
import com.rootcloud.analysis.core.enums.ClickHouseTypeEnum;
import com.rootcloud.analysis.core.enums.DataTypeEnum;
import com.rootcloud.analysis.core.enums.DataTypeInfoEnum;
import com.rootcloud.analysis.core.enums.SqlServerTypeEnum;

public class ClickHouseDataTypeConverter {

  /**
   * Converts data type to unified data type.
   *
   * @param dataType source data type
   *
   * @return unified data type
   */
  public static DataTypeEnum convert2UnifiedDataType(String dataType) {
    String upperCaseType = dataType.toUpperCase();
    String timeStampRegex = "^(DATE|DATE32|DATETIME|DATETIME64).*$";
    if (upperCaseType.matches(timeStampRegex)) {
      return DataTypeEnum.TIMESTAMP;
    }
    String integerRegex = "^(INT8|INT16|INT32|UINT8|UINT16)$";
    if (upperCaseType.matches(integerRegex)) {
      return DataTypeEnum.INTEGER;
    }
    String longRegex = "^(INT64|UINT32)$";
    if (upperCaseType.matches(longRegex)) {
      return DataTypeEnum.LONG;
    }
    String numberRegex =
        "^(FLOAT32|FLOAT64|DECIMAL|DECIMAL32|DECIMAL64|DECIMAL128).*$";
    if (upperCaseType.matches(numberRegex)) {
      return DataTypeEnum.NUMBER;
    }
    String booleanRegex = "^(BOOL).*$";
    if (upperCaseType.matches(booleanRegex)) {
      return DataTypeEnum.BOOLEAN;
    }
    return DataTypeEnum.STRING;
  }


  /**
   * Converts data type to flink data type.
   *
   * @param dataType source data type
   *
   * @return unified data type
   */
  public static DataTypeInfoEnum convert2TypeInfo(String dataType) {
    ClickHouseTypeEnum typeEnum = null;
    for (ClickHouseTypeEnum type : ClickHouseTypeEnum.values()) {
      if (type.matches(dataType)) {
        typeEnum = type;
        break;
      }
    }
    if (typeEnum == null) {
      return null;
    }
    return typeEnum.getDataTypeInfoEnumBuilder().build(dataType);
  }


  /**
   * Converts ClickHouse type to flink sql type.
   */
  public static String convert2FlinkSqlType(String sqlType) {
    ClickHouseTypeEnum typeEnum = null;
    for (ClickHouseTypeEnum value : ClickHouseTypeEnum.values()) {
      if (value.matches(sqlType.toUpperCase())) {
        typeEnum = value;
      }
    }
    Preconditions.checkNotNull(typeEnum, "Invalid ClickHouse type." + sqlType);
    return typeEnum.getFlinkSqlTypeBuilder().build(sqlType);
  }
}