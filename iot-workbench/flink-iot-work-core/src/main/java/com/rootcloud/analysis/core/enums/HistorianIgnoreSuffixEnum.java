/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.core.enums;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import lombok.Getter;

/**
 * Historian不额外加$后缀的内置字段集合.
 *
 * @CreatedBy: zexin.huang
 * @Date: 2022/3/16 10:32 上午
 */
@Getter
public enum HistorianIgnoreSuffixEnum {

  DEVICE_ID("__deviceId__"),
  ASSET_ID("__assetId__"),
  EVENT_TYPE_ID("__eventTypeId__"),
  DEVICE_TYPE_ID("__deviceTypeId__"),
  PHYSICAL_INTERFACE_ID("__physicalInterfaceId__"),
  LOGICAL_INTERFACE_ID("__logicalInterfaceId__"),
  RULE__ID("__ruleId__"),
  TIMESTAMP("__timestamp__"),
  CREATE_TIME("__create_time__"),
  REPORT_TIME("__report_time__"),
  WRITE_TIME("__write_time__"),
  CALCULATE_TIME("__calculate_time__"),
  CLOUD_TIME("__cloud_time__"),
  ALARM_TYPE_ID("__alarmTypeId__"),
  PREVIOUS("__previous__"),
  CURRENT("__current__"),
  EMIT_STATE("__emitState__"),
  SLOT_ID("__slotId__"),
  EVENT_TYPE_NAME("__eventTypeName__"),
  PARTITION_ID("partitionId"),
  TENANT_ID("__tenantId__"),
  ACTIONABLE_TYPE("__actionableType__"),
  Q_BAD("__qBad__"),
  THING_STORAGE_USAGE("__thingStorageUsage__"),
  DEVICE__STORAGE_USAGE("__deviceStorageUsage__"),
  THING_STORAGE_USAGE_RECOUP("__thingStorageUsage_recoup__"),
  DEVICE_STORAGE_USAGE_RECOUP("__deviceStorageUsage_recoup__"),
  BANDWIDTH_USAGE("__bandwidthUsage__"),
  BANDWIDTH_USAGE_RECOUP("__bandwidthUsage_recoup__"),
  DISABLE_ALARM("__disableAlarm__"),
  PREV_ALARM_STATE("__prevAlarmState__"),
  MODEL_TYPE("__modelType__"),
  UPDATE_TYPE("__updateType__"),
  METRICS_TYPE("__metricsType__"),
  RAW_EVENT("__raw_event__"),
  NANO_HASH("__nano_hash__"),

  RESET_RULE("__resetRule__"),
  FLINK_DEADLINE("__flink_deadline__"),
  ANCESTORS("__ancestors__"),
  ABSTRACT_MODEL_TYP("__abstractModelType__")
  ;

  private String fieldName;

  private HistorianIgnoreSuffixEnum(String fieldName) {
    this.fieldName = fieldName;
  }

  /**
   * getAllFieldNames.
   */
  public static List<String> getAllFieldNames() {
    return Arrays.stream(HistorianIgnoreSuffixEnum.values())
                  .map(HistorianIgnoreSuffixEnum::getFieldName)
                  .collect(Collectors.toList());
  }
}
