/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.core.constants;

public class MongoConstants {

  /**
   * mongo数据库.
   */
  public static final String MONGO_DATABASE = "otminer";

  /**
   * job集合.
   */
  public static final String MONGO_JOB_COLLECTION = "job";

  /**
   * job分发集合.
   */
  public static final String MONGO_JOB_DISTRIBUTION_COLLECTION = "job_distribution";

  public static final String MONGO_TENANT_RESOURCE_COLLECTION = "tenant_resource";
  /**
   * tenant集合.
   */
  public static final String MONGO_TENANT_RESOURCE_LIMIT_COLLECTION = "tenant_resource_limit";
  public static final String MONGO_TENANT_RESOURCE_USAGE_COLLECTION = "tenant_resource_usage";

  /**
   * template集合.
   */
  public static final String MONGO_JOB_TEMPLATE_COLLECTION = "job_template";

  /**
   * 外部数据源集合.
   */
  public static final String MONGO_EXTERNAL_DATASOURCE_COLLECTION = "external_datasource";

  /**
   * job metadata 元数据集合.
   */
  public static final String MONGO_JOB_METADATA_COLLECTION = "job_metadata";

  /**
   * job schedule .
   */
  public static final String MONGO_JOB_SCHEDULE_COLLECTION = "job_schedule";
  /**
   * folder 集合.
   */
  public static final String MONGO_FOLDER_COLLECTION = "folder";

  /**
   * lock集合.
   */
  public static final String MONGO_LOCK_COLLECTION = "lock";

  /**
   * 加密公钥/秘钥集合.
   */
  public static final String MONGO_KEYPAIR_COLLECTION = "key_pair";

  /**
   * 工作流集合.
   */
  public static final String MONGO_WORKFLOW_COLLECTION = "workflow";

  /**
   * 工作流集合-发布版本.
   */
  public static final String MONGO_WORKFLOW_RELEASE_COLLECTION = "workflow_release";

  /**
   * 集合对应MongoDB集合.
   */
  public static final String MONGO_COL_COLLECTION = "col";

  /**
   * 工作流metadata集合.
   */
  public static final String MONGO_WORKFLOW_METADATA_COLLECTION = "workflow_metadata";


  /**
   * udf.
   */
  public static final String MONGO_UDF_COLLECTION = "udf";
  public static final String MONGO_UDF_REF_COLLECTION = "udf_ref";
}
