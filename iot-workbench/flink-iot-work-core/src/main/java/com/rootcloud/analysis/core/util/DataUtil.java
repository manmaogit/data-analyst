/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.core.util;

import com.google.common.base.Preconditions;
import com.rootcloud.analysis.core.enums.DataTypeEnum;
import com.rootcloud.analysis.core.enums.DataTypeInfoEnum;

public class DataUtil {

  /**
   * Converts data to specified data type.
   *
   * @param data           data
   * @param targetDataType target data type
   *
   * @return data
   */
  public static Object convert(Object data, String targetDataType) {
    Object result = null;
    DataTypeEnum matchedDataType = null;
    for (DataTypeEnum dataTypeEnum : DataTypeEnum.values()) {
      if (dataTypeEnum.toString().equals(targetDataType)) {
        matchedDataType = dataTypeEnum;
        result = dataTypeEnum.getDataBuilder().build(data);
      }
    }
    Preconditions.checkArgument(matchedDataType != null,
        String.format("Illegal data type：%s", targetDataType));
    return result;
  }

  /**
   * Converts data to specified data type information.
   */
  public static Object convert2DataTypeInfo(Object data, String targetDataType) {
    Object result = null;
    DataTypeInfoEnum matchedDataTypeInfo = null;
    for (DataTypeInfoEnum dataTypeInfoEnum : DataTypeInfoEnum.values()) {
      if (dataTypeInfoEnum.toString().equals(targetDataType)) {
        matchedDataTypeInfo = dataTypeInfoEnum;
        result = dataTypeInfoEnum.getDataBuilder().build(data);
      }
    }
    Preconditions.checkArgument(matchedDataTypeInfo != null,
        String.format("Illegal data type：%s", targetDataType));
    return result;
  }

}
