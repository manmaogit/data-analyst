/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.core.enums;

public enum PriorityEnum {
  /**
   * 0 highest priority
   * 1 higher priority
   * 2 medium priority
   * 3 lower priority
   * 4 lowest priority.
   */
  HIGHEST,HIGH,MEDIUM,LOW,LOWEST
}
