/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.core.constants;

public class UdfConstant {
  public static final String FIELD_UDF_NAME = "name";
  public static final String FIELD_UDF_CHINESE_NAME = "chinese_name";
  public static final String FIELD_UDF_DESC = "desc";
  public static final String FIELD_UDF_JAR = "jar_name";
  public static final String FIELD_UDF_DEF_CLASS = "def_class";
  public static final String FIELD_UDF_TYPE = "type";
  public static final String FIELD_UDF_ARGUMENT = "arguments";
  public static final String FIELD_UDF_RESULT = "result";
  public static final String FIELD_UDF_JAR_ID = "jar_id";
  public static final String FIELD_UDF_JAR_FULL_NAME = "jar_full_name";
  public static final String FIELD_UDF_JAR_SIZE = "jar_size";

  public static final String FIELD_PROP_ORDINAL = "prop_ordinal";
  public static final String FIELD_PROP_TYPE = "prop_type";
  public static final String FIELD_PROP_DESC = "prop_desc";
  public static final String FIELD_PROP_NAME = "prop_name";

  public static final String FIELD_REF_UDF = "udf_id";
  public static final String FIELD_REF_TENANT = "tenant_id";
  public static final String FIELD_REF_JOB_ID = "job_id";
  public static final String FIELD_REF_JOB_NAME = "job_name";
  public static final String FIELD_REF_JOB_TYPE = "job_type";
  public static final String FIELD_REF_JOB_COL_ID = "col_id";
  public static final String FIELD_REF_NODE_ID = "node_id";

  public static final String FIELD_UDF_ACTION_ID = "Udf-Action-Id";

}
