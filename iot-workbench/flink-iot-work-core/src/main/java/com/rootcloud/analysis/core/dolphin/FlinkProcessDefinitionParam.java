/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2019 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.core.dolphin;

import com.google.common.collect.Lists;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;

@Getter
@Setter

public class FlinkProcessDefinitionParam extends ProcessDefinitionBaseParam {

  public static final String FLINK_JOB_SLOT_REQUIRED = "slot.required";
  public static final String FLINK_JOB_SLOT_WAIT_SEC = "slot.wait";

  private Params params;


  /**
   * 构建flink单节点默认实体.
   *
   * @param slotRequired 编排需要的slot数
   * @param slotWaitSecs slot不足时的等待时间
   */
  public void buildDefaultFlinkProcessDefinitionParamObject(int slotRequired, int slotWaitSecs,
                                                            String jobMetadataId) {
    if (StringUtils.isBlank(jobMetadataId)) {
      this.randomId();
    } else {
      this.setId(jobMetadataId);
    }
    this.setType("FLINK");
    Params params = new Params();
    params.setMainClass("com.rootcloud.analysis.FlinkMain");
    params.setMainJar(new MainJar("rootcloud-analysis-flink-runner.jar"));
    params.setDeployMode("local");
    params.setResourceList(Lists.newArrayList());
    LocalParams datetime = new LocalParams();
    datetime.setProp("datetime");
    datetime.setDirect("IN");
    datetime.setType("VARCHAR");
    datetime.setValue("${system.datetime}");

    LocalParams slotRequirement = new LocalParams();
    slotRequirement.setProp(FLINK_JOB_SLOT_REQUIRED);
    slotRequirement.setDirect("IN");
    slotRequirement.setType("INT");
    slotRequirement.setValue(String.valueOf(slotRequired));

    LocalParams slotWaitSecSpec = new LocalParams();
    slotWaitSecSpec.setProp(FLINK_JOB_SLOT_WAIT_SEC);
    slotWaitSecSpec.setDirect("IN");
    slotWaitSecSpec.setType("INT");
    slotWaitSecSpec.setValue(String.valueOf(slotWaitSecs));

    params.setLocalParams(Lists.newArrayList(datetime, slotRequirement, slotWaitSecSpec));
    params.setSlot(1);
    params.setTaskManager("2");
    params.setJobManagerMemory("1G");
    params.setTaskManagerMemory("2G");
    params.setExecutorCores(2);
    params.setProgramType("JAVA");
    this.setParams(params);
    this.setRunFlag("NORMAL");
    this.setMaxRetryTimes("0");
    this.setRetryInterval("1");
    Timeout timeout = new Timeout();
    timeout.setStrategy("");
    timeout.setInterval(0);
    timeout.setEnable(false);
    this.setTimeout(timeout);
    this.setTaskInstancePriority("MEDIUM");
    this.setWorkerGroup("default");
    this.setPreTasks(Lists.newArrayList());
  }

  public FlinkProcessDefinitionParam() {
  }

  /**
   * 构造方法.
   */
  public FlinkProcessDefinitionParam(String id, String type, String name, String runFlag,
                                     String description, Integer workerGroupId,
                                     String workerGroup, String taskInstancePriority,
                                     Timeout timeout, String maxRetryTimes,
                                     String retryInterval, List<String> preTasks,
                                     Dependence dependence,
                                     Params params) {
    super(id, type, name, runFlag, description, workerGroupId, workerGroup, taskInstancePriority,
        timeout, maxRetryTimes, retryInterval, preTasks, dependence);
    this.params = params;
  }
}
