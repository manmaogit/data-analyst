/*
 * Licensed Materials - Property of ROOTCLOUD
 * THIS MODULE IS "RESTRICTED MATERIALS OF ROOTCLOUD"
 * (c) Copyright ROOTCLOUD Inc. 2020 All Rights Reserved
 *
 * The source code for this program is not published or
 * otherwise divested of its trade secrets
 */

package com.rootcloud.analysis.core.graylog;

import ch.qos.logback.classic.PatternLayout;
import ch.qos.logback.classic.spi.ILoggingEvent;
import de.siegmar.logbackgelf.GelfEncoder;
import java.nio.charset.StandardCharsets;

public class RootCloudGelfEncoder extends GelfEncoder {

  /**
   * Short message format. Default: `"%m%nopex"`.
   */
  private PatternLayout shortPatternLayout;

  @Override
  public PatternLayout getShortPatternLayout() {
    return shortPatternLayout;
  }

  @Override
  public void setShortPatternLayout(PatternLayout shortPatternLayout) {
    this.shortPatternLayout = shortPatternLayout;
  }

  @Override
  public byte[] encode(final ILoggingEvent event) {
    final String shortMessage = shortPatternLayout.doLayout(event);
    return shortMessage.getBytes(StandardCharsets.UTF_8);
  }


}
