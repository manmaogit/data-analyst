package com.data.flink.entity;

import java.util.Date;

public  class UserBehavior {

    /**
     * // 用户 ID public long itemId; // 商品 ID
     */
    public Long userId;

    /**
     *  // 商品 ID
     */
    public Long itemId;

    /**
     * // 用户行为, 包括("pv", "buy", "cart", "fav")
     */
    public String behavior;

    /**
     * // 行为发生的时间戳，单位秒
     */
    public Date ts;

    public long getUserId() {
        return userId;
    }

    public long getItemId() {
        return itemId;
    }

    public String getBehavior() {
        return behavior;
    }

    public Date getTimestamp() {
        return ts;
    }
}
