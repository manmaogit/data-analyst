package com.data.flink.entity;

import java.lang.ref.WeakReference;

public class ItemViewCount {

    public long itemId;

    public long windowEnd;

    public long viewCount;


    public static ItemViewCount of(long itemId, long windowEnd, long viewCount) {
        ItemViewCount result = new ItemViewCount();
        result.itemId = itemId;
        result.windowEnd = windowEnd;
        result.viewCount = viewCount;
        return result;
    }

    public long getItemId() {
        return itemId;
    }

    public void setItemId(long itemId) {
        this.itemId = itemId;
    }

    public long getWindowEnd() {
        return windowEnd;
    }

    public void setWindowEnd(long windowEnd) {
        this.windowEnd = windowEnd;
    }

    public long getViewCount() {
        return viewCount;
    }

    public void setViewCount(long viewCount) {
        this.viewCount = viewCount;
    }

    public static void main(String[] args) {
        String a = "12";
        WeakReference<String> weakReference = new WeakReference<String>(a);
        System.gc();
        System.out.println(weakReference.get());
    }
}
