package com.data.hadoop.book.chr4_1;


import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

public class SalaryMapper extends Mapper<LongWritable, Text, Text, Text> {
    private static String TAB_SEPARATOR = "\t";

    /**
     * 调用map解析一行数据，该行的数据存储在value参数中，然后根据\t分隔符，解析出姓名，年龄，性别和工资
     */
    @Override
    public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        // 使用\t,分割数据
        String[] tokens = value.toString().split(TAB_SEPARATOR);

        // 性别
        String gender = tokens[2];
        // 姓名    年龄   工资
        String nameAgeSalary = tokens[0] + TAB_SEPARATOR + tokens[1] + TAB_SEPARATOR + tokens[3];

        // 输出  key=gender  value=name+age+salary
        context.write(new Text(gender), new Text(nameAgeSalary));
    }
}