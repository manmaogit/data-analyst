package com.data.hadoop.book.chr4_1;

import java.io.IOException;


import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;


public class SalaryReducer extends Reducer<Text, Text, Text, Text> {

    private static String TAB_SEPARATOR = "\t";

    private String name = " ";
    private String age = " ";
    private String gender = " ";
    private int maxSalary = Integer.MIN_VALUE;

    @Override
    public void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException {

        int salary = 0;

        // 根据key，迭代values集合，求出最高分
        for (Text val : values) {
            String[] valTokens = val.toString().split(TAB_SEPARATOR);
            salary = Integer.parseInt(valTokens[2]);
            if (salary > maxSalary) {
                name = valTokens[0];
                age = valTokens[1];
                gender = key.toString();
                maxSalary = salary;
            }
        }

    }

    @Override
    protected void cleanup(Context context) throws IOException, InterruptedException {
        context.write(new Text(name), new Text("age：" + age + TAB_SEPARATOR + "gender：" + gender + TAB_SEPARATOR + "salary：" + maxSalary));
    }
}