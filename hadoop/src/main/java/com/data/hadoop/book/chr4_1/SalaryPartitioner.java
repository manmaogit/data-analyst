package com.data.hadoop.book.chr4_1;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Partitioner;

public class SalaryPartitioner extends Partitioner<Text, Text> {

    private static String TAB_SEPARATOR = "\t";

    @Override
    public int getPartition(Text key, Text value, int numReduceTasks) {
        String[] nameAgeSalary= value.toString().split(TAB_SEPARATOR);
        // 年龄
        int age = Integer.parseInt(nameAgeSalary[1]);

        // 默认指定分区 0
        if (numReduceTasks == 0){
            return 0;
        }

        if (age <= 20) {
            // 年龄小于等于20，指定分区0
            return 0;
        } else if (age <= 30) {
            // 年龄大于20，小于等于30，指定分区1
            return 1 % numReduceTasks;
        } else if (age <= 40) {
            // 年龄大于30，小于等于40，指定分区2
            return 2 % numReduceTasks;
        }else {
            // 剩余年龄，指定分区3
            return 3 % numReduceTasks;
        }
    }
}
