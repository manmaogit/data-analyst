package com.data.hadoop.book.chr4_3.chr4_3_4.compant;

import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableComparator;

public class DateTemperatureGroupingComparator extends WritableComparator{

    public DateTemperatureGroupingComparator() {
        super(DateTemperaturePairKey.class,true);
    }

    @SuppressWarnings("rawtypes")
    @Override
    public int compare(WritableComparable a, WritableComparable b) {
        DateTemperaturePairKey pair1=(DateTemperaturePairKey)a;
        DateTemperaturePairKey pair2=(DateTemperaturePairKey)b;
        
        return pair1.getYearMonth().compareTo(pair2.getYearMonth());
    }
    
    
}
