package com.data.hadoop.book.chr4_3;

import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.*;
import org.apache.hadoop.mapreduce.lib.output.FileOutputCommitter;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import java.io.IOException;

public class DBOutputFormat extends OutputFormat<Text, Text> {

    private String outputPathPrefix="/user/maper/";

    @Override
    public RecordWriter<Text, Text> getRecordWriter(TaskAttemptContext taskAttemptContext)
            throws IOException, InterruptedException {
        return new SqlRecordWriter();
    }

    @Override
    public void checkOutputSpecs(JobContext jobContext)
            throws IOException, InterruptedException {

    }

    @Override
    public OutputCommitter getOutputCommitter(TaskAttemptContext context)
            throws IOException, InterruptedException {
        String committerPath=outputPathPrefix+context.getJobName();
        return new FileOutputCommitter (new Path(committerPath), context);
    }
}