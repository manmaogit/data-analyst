package com.data.hadoop.book.chr4_3;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.RecordWriter;
import org.apache.hadoop.mapreduce.TaskAttemptContext;

import java.io.IOException;
import java.sql.SQLException;

public class SqlRecordWriter extends RecordWriter<Text, Text> {

    @Override
    public void write(Text key, Text value) throws IOException, InterruptedException {
        String data=key.toString();
        String  num=value.toString();
        Dao dao=new Dao();
        try {
            // 把结果输出到mysql中
            dao.insertData(data, num);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void close(TaskAttemptContext taskAttemptContext) throws IOException, InterruptedException {
        //关闭数据库连接
    }
}