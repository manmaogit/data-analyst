package com.data.hadoop.book.chr4_2;


import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.InputSplit;
import org.apache.hadoop.mapreduce.RecordReader;
import org.apache.hadoop.mapreduce.TaskAttemptContext;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;
import org.apache.hadoop.util.LineReader;

import java.io.IOException;

public  class CustomReader extends RecordReader<Text ,Text> {

    private LineReader lr ;
    private Text key = new Text();
    private Text value = new Text();
    private long start ;
    private long end;
    private long currentPos;
    private Text line = new Text();

    @Override
    public void initialize(InputSplit inputSplit, TaskAttemptContext taskAttemptContext)
            throws IOException {

        //输入分片的文件对象
        FileSplit split =(FileSplit) inputSplit;
        Configuration conf = taskAttemptContext.getConfiguration();
        Path path = split.getPath();
        FileSystem fs = path.getFileSystem(conf);
        FSDataInputStream is = fs.open(path);
        lr = new LineReader(is,conf);

        //分片，起始位置
        start =split.getStart();
        //分片，终点位置
        end = start + split.getLength();
        is.seek(start);
        if(start != 0){
            //如果不是开始的分片，默认跳过第一行
            start += lr.readLine(new Text(),0,
                    (int)Math.min(Integer.MAX_VALUE, end-start));
        }
        currentPos = start;
    }

    @Override
    public boolean nextKeyValue() throws IOException, InterruptedException {
        //currentPos=end,会继续读取下一行，currentPos > end则返回false
        if(currentPos > end){
            return false;
        }

        // 读取一行数据
        currentPos += lr.readLine(line);
        if(line.getLength()==0){
            return false;
        }
        if(line.toString().startsWith("ignore")){
            currentPos += lr.readLine(line);
        }

        // 转化为word数组
        String [] words = line.toString().split(",");
        // 异常处理
        if(words.length < 2){
            System.err.println("line:"+line.toString()+".");
            return false;
        }
        key.set(words[0]);
        value.set(words[1]);

        return true;
    }

    @Override
    public Text getCurrentKey() throws IOException, InterruptedException {
        return key;
    }

    @Override
    public Text getCurrentValue() throws IOException, InterruptedException {
        return value;
    }

    @Override
    public float getProgress() throws IOException, InterruptedException {
        if (start == end) {
            return 0.0f;
        } else {
            return Math.min(1.0f, (currentPos - start) / (float) (end - start));
        }
    }

    @Override
    public void close() throws IOException {
        lr.close();
    }
}