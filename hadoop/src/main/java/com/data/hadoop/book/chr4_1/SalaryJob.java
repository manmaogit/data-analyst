package com.data.hadoop.book.chr4_1;

import com.data.hadoop.book.chr4_3.DBOutputFormat;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;


public class SalaryJob extends Configured implements Tool {


    @SuppressWarnings("deprecation")
    @Override
    public int run(String[] args) throws Exception {
        // 读取配置文件
        Configuration conf = new Configuration();

        Path mypath = new Path(args[1]);
        FileSystem hdfs = mypath.getFileSystem(conf);
        if (hdfs.isDirectory(mypath)) {
            hdfs.delete(mypath, true);
        }
        
        // 新建一个任务
        Job job = Job.getInstance(conf,"SalaryMaxs");
        // 主类
        job.setJarByClass(SalaryJob.class);
        // Mapper
        job.setMapperClass(SalaryMapper.class);
        // Reducer
        job.setReducerClass(SalaryReducer.class);

        // map 输出key类型
        job.setMapOutputKeyClass(Text.class);
        // map 输出value类型
        job.setMapOutputValueClass(Text.class);
        
        // reduce 输出key类型
        job.setOutputKeyClass(Text.class);
        // reduce 输出value类型
        job.setOutputValueClass(Text.class);



        // 设置Partitioner类
        job.setPartitionerClass(SalaryPartitioner.class);
        // reduce个数设置为3
        job.setNumReduceTasks(3);

        // 输入路径
        FileInputFormat.addInputPath(job, new Path(args[0]));
        // 输出路径
        FileOutputFormat.setOutputPath(job, new Path(args[1]));
        
        // 提交任务
        return job.waitForCompletion(true)?0:1;
    }
    
    public static void main(String[] args) throws Exception {
        String[] args0 = {
                "hdfs://ljc:9000/buaa/gender/gender.txt",
                "hdfs://ljc:9000/buaa/gender/out/"
        };
        int ec = ToolRunner.run(new Configuration(),new SalaryJob(), args0);
        System.exit(ec);
    }
}