package com.data.hadoop.book.chr4_3.chr4_3_4;

import com.data.hadoop.book.chr4_3.chr4_3_4.compant.DateTemperaturePairKey;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

/**
 * Created by Administrator on 2017/8/16.
 */
public class TemperatureMapper extends Mapper<LongWritable,Text, DateTemperaturePairKey, IntWritable> {

    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {

        String line=value.toString();
        String [] tokens=line.split(",");
        String yearMonth=tokens[0]+tokens[1];
        String day=tokens[2];
        int temperature=Integer.parseInt(tokens[3]);

        //prepare reduce key
        DateTemperaturePairKey reduceKey=new DateTemperaturePairKey();
        reduceKey.setYearMonth(new Text(yearMonth));
        reduceKey.setDay(new Text(day));
        reduceKey.setTemperature(new IntWritable(temperature));

        //send it to reduce
        context.write(reduceKey,new IntWritable(temperature));

    }
}
