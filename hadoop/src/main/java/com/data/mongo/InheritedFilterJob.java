package com.data.mongo;


import com.data.analysis.HadoopMain;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;

import com.mongodb.hadoop.MongoInputFormat;
import com.mongodb.hadoop.util.MongoConfigUtil;

/**
 * 
 * 查询检测结果中 罕见病为未检出 的 barcode - 项目 - 结果
 * 
 * 
 * @author Administrator
 *
 */
public class InheritedFilterJob extends Configured implements Tool {
    
    //private Logger logger=LogManager.getLogger(InheritedFilterJob.class);
    
    @Override
    public int run(String[] arg0) throws Exception {
        
        Configuration conf = new Configuration();
        
        conf.addResource("hadoop.xml");
        conf.addResource("mongodb.xml");
        
        MongoConfigUtil.setInputURI(conf,conf.get("mongo.input.uri"));
        //MongoConfigUtil.setOutputURI(conf,conf.get("mongo.output.uri"));

        MongoConfigUtil.setSplitSize(conf, 512); //设置输入分片大小，单位MB
        
        Job job = Job.getInstance(conf);
        job.setJarByClass(HadoopMain.class);
        job.setMapperClass(InheritedMapper.class);
        job.setReducerClass(InheritedReudcer.class);
        job.setNumReduceTasks(4);
        
        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(Text.class);
        
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(Text.class);
        
        job.setInputFormatClass(MongoInputFormat.class);
        job.setOutputFormatClass(TextOutputFormat.class);
        
        
        FileSystem fs = FileSystem.get(conf);
        fs.delete(new Path(arg0[1]), true);
        FileOutputFormat.setOutputPath(job, new Path(arg0[1])); // 设置输出路径
                 
        System.exit(job.waitForCompletion(true) ? 0 : 1);
        return 0;
    }

}
