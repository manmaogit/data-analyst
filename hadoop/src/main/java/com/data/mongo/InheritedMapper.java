package com.data.mongo;

import java.io.IOException;

import org.apache.commons.lang.StringUtils;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.bson.BSONObject;
import org.bson.types.BasicBSONList;

public class InheritedMapper extends Mapper<Object, BSONObject, Text, Text> {
    
    @Override
    protected void setup(Context context)
        throws IOException, InterruptedException {
        
    }
    
    @Override
    protected void map(Object key, BSONObject value, Context context)
        throws IOException, InterruptedException {
        
        BSONObject resultDetail = (BSONObject)value.get("resultDetail");
        BSONObject inheritedResult = (BSONObject)resultDetail.get("inheritedResult");
        String barcode=value.get("barCode").toString();
        String productId=value.get("productId").toString();
        if(inheritedResult!=null) {
            
            BSONObject inheritedResultDetails = (BSONObject)inheritedResult.get("inheritedResultDetails");
            
            if (inheritedResultDetails != null) {
                
                BasicBSONList list = new BasicBSONList();
                list.putAll(inheritedResultDetails);
                
                for (Object object : list) {
                    BSONObject itemBsonObject = (BSONObject)object;
                    String id = itemBsonObject.get("_id").toString();
                    String name = itemBsonObject.get("name").toString();
                    String result = itemBsonObject.get("result").toString();
                    if(StringUtils.isNotBlank(id) 
                        && StringUtils.isNotBlank(name) 
                        && StringUtils.isNotBlank(result) 
                        && result.contains("未检出")) {
                        context.write(new Text(id), new Text(barcode+","+productId+","+name+","+result));
                    }
                }
            }
        }
    }
}
