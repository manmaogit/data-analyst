package com.data.mongo;

import java.io.IOException;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public class InheritedReudcer extends Reducer<Text, Text, Text, Text> {
    @Override
    protected void setup(Context context){
    }
    
    @Override
    protected void reduce(Text key, Iterable<Text> values, Context context)
        throws IOException, InterruptedException {
        for(Text txt:values) {
            context.write(key,txt);
        }
    }
    
    @Override
    protected void cleanup(Context context) {
        
    }
    
    
    
   

}
