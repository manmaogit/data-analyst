package com.data.analysis.sort.compant;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.mapreduce.Partitioner;

public class DateTemperaturePartitioner extends Partitioner<DateTemperaturePairKey, IntWritable>{

    @Override
    public int getPartition(DateTemperaturePairKey key, IntWritable value, int numPartitions) {
        return Math.abs(key.getYearMonth().hashCode()%numPartitions);
    }
    
}
