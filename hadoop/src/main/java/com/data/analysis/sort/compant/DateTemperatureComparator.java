package com.data.analysis.sort.compant;

import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableComparator;

/**
 * 
 * Created by Administrator on 2017/8/16.
 * 
 */
public class DateTemperatureComparator extends WritableComparator {

    protected DateTemperatureComparator(){
        super(DateTemperaturePairKey.class,true);
    }

    @Override
    public int compare(WritableComparable a, WritableComparable b) {
        DateTemperaturePairKey pair1=(DateTemperaturePairKey)a;
        DateTemperaturePairKey pair2=(DateTemperaturePairKey)b;

        int compareValue=pair1.getYearMonth().compareTo(pair2.getYearMonth());
        if(compareValue == 0) {
            compareValue=pair1.getTemperature().compareTo(pair2.getTemperature());
        }
        //return compareValue;  //sort ascending 升序
        return -1*compareValue; //sort descending 降序

    }
}
