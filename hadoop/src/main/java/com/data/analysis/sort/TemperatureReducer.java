package com.data.analysis.sort;

import com.data.analysis.sort.compant.DateTemperaturePairKey;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

/**
 * Created by Administrator on 2017/8/16.
 */
public class TemperatureReducer extends Reducer<DateTemperaturePairKey,IntWritable, Text, Text> {
    @Override
    protected void reduce(DateTemperaturePairKey key, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException {

        StringBuilder sortedTemperatureList=new StringBuilder();

        for(IntWritable temperature:values){
            sortedTemperatureList.append(temperature);
            sortedTemperatureList.append(",");
        }

        context.write(key.getYearMonth(),new Text(sortedTemperatureList.toString()));
    }
}
