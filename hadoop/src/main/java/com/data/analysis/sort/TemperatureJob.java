package com.data.analysis.sort;

import com.data.analysis.HadoopMain;
import com.data.analysis.sort.compant.DateTemperatureComparator;
import com.data.analysis.sort.compant.DateTemperaturePairKey;
import com.data.analysis.sort.compant.DateTemperatureGroupingComparator;
import com.data.analysis.sort.compant.DateTemperaturePartitioner;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;
import org.apache.hadoop.util.Tool;

public class TemperatureJob  extends Configured implements Tool {
    
    @Override
    public int run(String[] arg0) throws Exception {
        
        Configuration conf = new Configuration();
        String[] otherArgs = new GenericOptionsParser(conf, arg0).getRemainingArgs();
        
        conf.addResource("hadoop.xml"); //不能加 classpath
        
        Job job = Job.getInstance(conf);
        job.setJarByClass(HadoopMain.class);
        
        job.setMapperClass(TemperatureMapper.class);
        
        job.setMapOutputKeyClass(DateTemperaturePairKey.class);
        job.setMapOutputValueClass(IntWritable.class);

        job.setReducerClass(TemperatureReducer.class);
        job.setNumReduceTasks(1); //设置一个reducer，全局有序
        
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(Text.class);
        
        job.setInputFormatClass(TextInputFormat.class);
        job.setOutputFormatClass(TextOutputFormat.class);

        //分区方法重定义
        job.setPartitionerClass(DateTemperaturePartitioner.class);
            //Define the comparator that controls which keys are grouped together for a single call to reduce
        job.setGroupingComparatorClass(DateTemperatureGroupingComparator.class);
            //Define the comparator that controls how the keys are sorted before they are passed to the {@link Reducer}.
        job.setSortComparatorClass(DateTemperatureComparator.class);

        FileInputFormat.addInputPath(job, new Path(otherArgs[0])); //设置输入路径
        FileSystem fs = FileSystem.get(conf); 
        fs.delete(new Path(otherArgs[1]), true);
        FileOutputFormat.setOutputPath(job, new Path(otherArgs[1])); //设置输出路径
       
        
        System.exit(job.waitForCompletion(true) ? 0 : 1);


        return 0;
    }
    
}