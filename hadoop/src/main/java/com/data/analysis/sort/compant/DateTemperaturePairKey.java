package com.data.analysis.sort.compant;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.io.WritableComparable;


public class DateTemperaturePairKey implements Writable,WritableComparable<DateTemperaturePairKey>{

    private Text yearMonth=new Text();
    private Text day=new Text();
    private IntWritable temperature=new IntWritable();

    @Override
    public int compareTo(DateTemperaturePairKey pair) { //次排序，排序规则
        int compareValue=this.yearMonth.compareTo(pair.getYearMonth());
        if(compareValue == 0) {
            compareValue=temperature.compareTo(pair.getTemperature());
        }
        //return compareValue;  //sort ascending
        return -1*compareValue; //sort descending
    }

    @Override
    public void write(DataOutput out)throws IOException {
        yearMonth.write(out);
        day.write(out);
        temperature.write(out);
        
        
    }

    @Override
    public void readFields(DataInput in)throws IOException {
        yearMonth.readFields(in);
        day.readFields(in);
        temperature.readFields(in);
        
    }
    
    
    public Text getYearMonth() {
        return yearMonth;
    }

    public void setYearMonth(Text yearMonth) {
        this.yearMonth = yearMonth;
    }

    public Text getDay() {
        return day;
    }

    public void setDay(Text day) {
        this.day = day;
    }

    public IntWritable getTemperature() {
        return temperature;
    }

    public void setTemperature(IntWritable temperature) {
        this.temperature = temperature;
    }
}
