package com.data.analysis.heartbeat;

import com.data.analysis.HadoopMain;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.NLineInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;
import org.apache.hadoop.util.Tool;

public class HeartBeatJob extends Configured implements Tool {

    @Override
    public int run(String[] arg0) throws Exception {

        Configuration conf = new Configuration();
        String[] otherArgs = new GenericOptionsParser(conf, arg0).getRemainingArgs();

        conf.addResource("hadoop.xml"); //不能加 classpath

        Job job = Job.getInstance(conf);
        job.setJarByClass(HadoopMain.class);

        job.setMapperClass(HeartBeatMapper.class);

        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(Text.class);

        //job.setReducerClass(TraitRetioReudcer.class);
        //job.setNumReduceTasks(8);

        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(Text.class);


        job.setInputFormatClass(NLineInputFormat.class);
        job.setOutputFormatClass(TextOutputFormat.class);

        FileInputFormat.addInputPath(job, new Path(otherArgs[0])); //设置输入路径
        FileSystem fs = FileSystem.get(conf);
        fs.delete(new Path(otherArgs[1]), true);
        FileOutputFormat.setOutputPath(job, new Path(otherArgs[1])); //设置输出路径
        NLineInputFormat.setNumLinesPerSplit(job, 1000); //一个分片1000个barCode


        System.exit(job.waitForCompletion(true) ? 0 : 1);

        return 0;
    }

}