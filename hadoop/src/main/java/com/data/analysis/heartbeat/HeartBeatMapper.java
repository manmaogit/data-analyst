package com.data.analysis.heartbeat;

import java.io.IOException;

import com.data.analysis.util.Heartbeat;
import com.data.analysis.util.SpringContextFactory;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.springframework.context.support.AbstractApplicationContext;

public class HeartBeatMapper extends Mapper<LongWritable, Text, Text,Text>{

    private AbstractApplicationContext springContenxt=null;

    private  Heartbeat heartbeat=null;

    @Override
    protected void setup(Context context) throws IOException, InterruptedException {
        heartbeat=Heartbeat.createHeartbeat(context);
        springContenxt= SpringContextFactory.initSpringContext();
    }
    
    @Override
    protected void map(LongWritable key, Text value,Context context)
        throws IOException, InterruptedException {
        
    }
    
    //清空spring缓存
    @Override
    protected void cleanup(Context context) throws IOException, InterruptedException {
        if(heartbeat!=null)
            heartbeat.stopBeating();
        springContenxt.close();
    }
    
    
}