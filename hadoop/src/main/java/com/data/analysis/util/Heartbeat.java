package com.data.analysis.util;

import org.apache.hadoop.mapreduce.TaskAttemptContext;

/**
 * Created by manmao on 2017/7/27.
 */
public class Heartbeat extends Thread {

    private static final int sleepTime = 400;
    private boolean beating = true;
    private TaskAttemptContext context = null;

    private Heartbeat(TaskAttemptContext context) {
        this.context = context;
    }

    @Override
    public void run() {
        super.run();
        while (beating){
            try {
                Thread.sleep(sleepTime*1000);

            }catch (InterruptedException e){}

            context.setStatus(Long.valueOf(System.currentTimeMillis()).toString());
        }
    }

    public  void stopBeating(){
        beating = false;
    }

    public  static Heartbeat createHeartbeat(TaskAttemptContext context){
        Heartbeat heartbeat=new Heartbeat(context);
        Thread heartbeatThread=new Thread(heartbeat);
        heartbeatThread.setPriority(MAX_PRIORITY); //设置为最高优先级
        heartbeatThread.setDaemon(true);
        heartbeatThread.start();
        return  heartbeat;
    }

}
