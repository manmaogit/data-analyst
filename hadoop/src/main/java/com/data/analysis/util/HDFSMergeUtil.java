package com.data.analysis.util;

import java.io.IOException;
import java.net.URI;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.fs.PathFilter;

public class HDFSMergeUtil {
    
    private Path inputPath = null;
    private Path outputPath = null;
    private String filter;
    
    public HDFSMergeUtil(String filter,String input, String output) {
        this.inputPath = new Path(input);
        this.outputPath = new Path(output);
        this.filter=filter;
    }
    
    public void doMerge() throws IOException {
        
        Configuration conf = new Configuration();
        conf.set("fs.defaultFS", "hdfs://master01:8020");
        conf.set("fs.hdfs.impl", "org.apache.hadoop.hdfs.DistributedFileSystem");
        
        FileSystem fsSource = FileSystem.get(URI.create(inputPath.toString()), conf);
        FileSystem fsDest = FileSystem.get(URI.create(outputPath.toString()), conf);
        //获取输入目录下的文件信息
        FileStatus[] sourceStatus = fsSource.listStatus(inputPath,new DefaultPathFilter(filter)); //可以使用过滤器，过滤指定文件 fsSource.listStatus(inputPath, new DefaultPathFilter(".*\\.abc"));
       
        FSDataOutputStream fsOutputStream = fsDest.create(outputPath); //创建输出流
        for (FileStatus fileStatus : sourceStatus) {
            
            FSDataInputStream fsInputStream = fsSource.open(fileStatus.getPath());//打开文件输入流
            
            byte[] data = new byte[2048];
            int read = -1;
            while ((read = fsInputStream.read(data)) > 0) {
                fsOutputStream.write(data, 0 ,read);
            }
            fsInputStream.close();
        }
        fsOutputStream.close();
    }
    
    //java -jar xxx.jar  .*genoTypelog_2017.* srcdir destfile
    public static void merge(String[] args) throws IOException{
        System.out.println(" merge "+args[1]+"  ===>  "+args[2]);
        HDFSMergeUtil merge = new HDFSMergeUtil(args[1],args[2],args[3]);
        merge.doMerge();
        
    }
}


class DefaultPathFilter implements PathFilter {
    
    String reg = null;
    
    public DefaultPathFilter(String reg) {
        this.reg = reg;
    }
    
    public boolean accept(Path path) {
        
        if(path.toString().matches(reg)) {
            return true;
        }
        return false;
    }
}