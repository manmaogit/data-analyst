package com.data.analysis.util;

import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
public class SpringContextFactory {
    
    private static AbstractApplicationContext  sprintContext=null;
    
    public static AbstractApplicationContext  initSpringContext(){
        /*AbstractApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        context.registerShutdownHook();
        
        if(sprintContext == null){
            sprintContext=context;
        }
        return context;*/

        return ApplicationContextHolder.getInstance();
    }
    
    public static AbstractApplicationContext  getSpringContext(){
        if(sprintContext == null){
            initSpringContext();
        }
        return sprintContext;
    }

    private  static   class ApplicationContextHolder{

        private  static  AbstractApplicationContext context=new ClassPathXmlApplicationContext("applicationContext.xml");

        public  static AbstractApplicationContext getInstance(){
            ApplicationContextHolder.context.registerShutdownHook();
            return ApplicationContextHolder.context;
        }
    }

}