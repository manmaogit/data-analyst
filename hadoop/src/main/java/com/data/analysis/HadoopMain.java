package com.data.analysis;

import com.data.analysis.totalSort.TotalOrderJob;
import com.data.normal.distributeCache.MapperJoinJob;
import com.data.normal.join.JoinFilterExampleMRJob;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.util.ToolRunner;

/**
 * Created by Administrator on 2017/7/26.
 */
public class HadoopMain {
    public static void main( String[] args ) throws Exception{
        int res = ToolRunner.run(new Configuration(), new TotalOrderJob(), args);
        System.exit(res);
    }
}
