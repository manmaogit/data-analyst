package com.data.analysis.totalSort;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.*;
import org.apache.hadoop.io.compress.GzipCodec;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.KeyValueTextInputFormat;
import org.apache.hadoop.mapreduce.lib.input.SequenceFileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.mapreduce.lib.partition.InputSampler;
import org.apache.hadoop.mapreduce.lib.partition.TotalOrderPartitioner;
import org.apache.hadoop.util.Tool;

/**
 * Created by manmao on 2017/12/10.
 */
public class TotalOrderJob extends Configured implements Tool {

    @Override
    public int run(String[] args) throws Exception {

        String input1 = args[0];
        String input2 =args[1];
        String partitionFile=args[2];
        String output = args[3];
        int numberOfReducers = Integer.parseInt(args[4]);

        //A
        Job job = Job.getInstance();


        //B
        job.setJarByClass(TotalOrderJob.class);
        job.setJobName("TotalOrderJob");

        //使用符号链接
        //job.addCacheFile(new URI("/user/bar/bar.txt#bar.txt"));

        //C
        Configuration config = job.getConfiguration();

        // D
        job.setInputFormatClass(KeyValueTextInputFormat.class);
        KeyValueTextInputFormat.addInputPath(job, new Path(input1));
        KeyValueTextInputFormat.addInputPath(job, new Path(input2));

        //sequence file inputformat 压缩
        /*SequenceFileOutputFormat.setCompressOutput(job, true);
        SequenceFileOutputFormat.setOutputCompressorClass(job, GzipCodec.class);
        SequenceFileOutputFormat.setOutputCompressionType(job, SequenceFile.CompressionType.BLOCK);
        */


        // E
        job.setOutputFormatClass(TextOutputFormat.class);
        // 删除输出
        FileSystem fs = FileSystem.get(job.getConfiguration());
        fs.delete(new Path(output), true);
        TextOutputFormat.setOutputPath(job, new Path(output));

        // partitioner class设置成TotalOrderPartitioner
        job.setPartitionerClass(TotalOrderPartitioner.class);

        // F
        job.setMapperClass(TotalOrderMapper.class);
        job.setReducerClass(TotalOrderReducer.class);

        // G
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(Text.class);
        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(Text.class);
        //H
        job.setNumReduceTasks(numberOfReducers);

        // RandomSampler第一个参数表示key会被选中的概率，第二个参数是一个选取samples数，第三个参数是最大读取input splits数
        //<Text,Text>类型和InputFormat的类型一致
        InputSampler.RandomSampler<Text, Text> sampler = new InputSampler.RandomSampler<Text, Text>(0.1, 10000, 10);
        //设置partition文件路径
        TotalOrderPartitioner.setPartitionFile(job.getConfiguration(), new Path(partitionFile));
        // 写partition file到mapreduce.totalorderpartitioner.path
        InputSampler.writePartitionFile(job, sampler);

        // I
        job.waitForCompletion(true);
        return 0;
    }

    @Override
    public void setConf(Configuration conf) {
        // TODO Auto-generated method stub

    }

    @Override
    public Configuration getConf() {
        // TODO Auto-generated method stub
        return null;
    }
}
