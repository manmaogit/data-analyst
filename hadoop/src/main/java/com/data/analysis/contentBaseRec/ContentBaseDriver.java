package com.data.analysis.contentBaseRec;

import com.data.analysis.commonFriend.CommonFriendsMapper;
import com.data.analysis.commonFriend.CommonFriendsReducer;
import com.data.analysis.contentBaseRec.model.MovieTupleInfo;
import com.data.analysis.contentBaseRec.model.Tuple2;
import com.data.analysis.contentBaseRec.model.Tuple3;
import com.data.analysis.friendRecommend.FriendRecommendationDriver;
import com.data.analysis.util.HadoopUtil;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.SequenceFileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;
import org.apache.hadoop.util.Tool;
import org.apache.log4j.Logger;

/**
 * Created by Administrator on 2017/11/10.
 * 协同过滤算法
 *      根据物品相似度推荐。
 *
 */
public class ContentBaseDriver extends Configured implements Tool {

    private static Logger theLogger = Logger.getLogger(ContentBaseDriver.class);

    private static  final  String  P1_OUTPUTATH="/tmp/contentbase/ph1";
    private static  final  String  P2_OUTPUTATH="/tmp/contentbase/ph2";
    private static  final  String  P3_OUTPUTATH="/tmp/contentbase/ph3";


    @Override
    public int run(String[] args) throws Exception {

        String[] otherArgs = new GenericOptionsParser(getConf(), args).getRemainingArgs();

        //----------------------------------job 1-------------------------------------------
        Job job1 = Job.getInstance(getConf());
        job1.setJobName("CommonFriendsDriver1");
        job1.setJarByClass(ContentBaseDriver.class);

        job1.setInputFormatClass(TextInputFormat.class);
        job1.setOutputFormatClass(SequenceFileOutputFormat.class);

        job1.setMapOutputKeyClass(Text.class);
        job1.setMapOutputValueClass(Tuple2.class);

        // mapper will generate key as Text (the keys are as (person1,person2))
        job1.setOutputKeyClass(Text.class);
        // mapper will generate value as Text (list of friends)
        job1.setOutputValueClass(Tuple3.class);

        job1.setMapperClass(Phase1Mapper.class);
        job1.setReducerClass(Phase1Reducer.class);
        //input
        FileInputFormat.setInputPaths(job1, new Path(otherArgs[0]));

        //output
        FileSystem fs = FileSystem.get(getConf());
        fs.delete(new Path(P1_OUTPUTATH), true);
        FileOutputFormat.setOutputPath(job1, new Path(P1_OUTPUTATH));

        boolean status1 = job1.waitForCompletion(true);


//----------------------------------job 2-------------------------------------------
        Job job2 = Job.getInstance(getConf());
        job2.setJobName("CommonFriendsDriver2");
        job2.setJarByClass(ContentBaseDriver.class);

        job2.setInputFormatClass(SequenceFileInputFormat.class);
        job2.setOutputFormatClass(SequenceFileOutputFormat.class);


        job2.setMapOutputKeyClass(Text.class);
        job2.setMapOutputValueClass(Tuple3.class);


        job2.setOutputKeyClass(Tuple2.class);          // mapper will generate key as Text (the keys are as (person1,person2))
        job2.setOutputValueClass(MovieTupleInfo.class);        // mapper will generate value as Text (list of friends)


        job2.setMapperClass(Phase2Mapper.class);
        job2.setReducerClass(Phase2Reducer.class);
        //input
        FileInputFormat.setInputPaths(job2, new Path(P1_OUTPUTATH));

        //output
        fs.delete(new Path(P2_OUTPUTATH), true);
        FileOutputFormat.setOutputPath(job2, new Path(P2_OUTPUTATH));

        boolean status2 = job2.waitForCompletion(true);

    //------------------------------job 3------------------------------------
        Job job3 = Job.getInstance(getConf());
        job3.setJobName("CommonFriendsDriver3");
        job3.setJarByClass(ContentBaseDriver.class);

        job3.setInputFormatClass(SequenceFileInputFormat.class);
        job3.setOutputFormatClass(TextOutputFormat.class);


        job3.setOutputKeyClass(Text.class);          // mapper will generate key as Text (the keys are as (person1,person2))
        job3.setOutputValueClass(Text.class);        // mapper will generate value as Text (list of friends)

        job3.setMapOutputKeyClass(Tuple2.class);
        job3.setMapOutputValueClass(MovieTupleInfo.class);

        job3.setMapperClass(Phase3Mapper.class);
        job3.setReducerClass(Phase3Reducer.class);

        //input
        FileInputFormat.setInputPaths(job3, new Path(P2_OUTPUTATH));

        //output
        fs.delete(new Path(P3_OUTPUTATH), true);
        FileOutputFormat.setOutputPath(job3, new Path(P3_OUTPUTATH));

        boolean status3 = job3.waitForCompletion(true);
        return 0;
    }
}
