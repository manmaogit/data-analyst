package com.data.analysis.contentBaseRec;

import com.data.analysis.contentBaseRec.model.DoubleTuple3;
import com.data.analysis.contentBaseRec.model.MovieTupleInfo;
import com.data.analysis.contentBaseRec.model.Tuple2;
import com.data.analysis.contentBaseRec.model.Tuple3;
import com.google.common.collect.Lists;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * key  :    userid
 * value:    List< movie_id,  rate,  numberOfRaters>
 *
 *
 */
public class Phase2Reducer extends Reducer<Text, Tuple3, Tuple2, MovieTupleInfo> {

    @Override
    protected void reduce(Text key, Iterable<Tuple3> values, Context context) throws IOException, InterruptedException {

        //将同一个用户对不同电影的评分，组成两个不同集合列表
        List<DoubleTuple3> list = generateUniqueCombination(values);

        for(DoubleTuple3 doubleTuple:list){

            Tuple3 m1=doubleTuple.getOne(); //
            Tuple3 m2=doubleTuple.getTwo(); //

            Tuple2 reduceKey=new Tuple2(m1.getOne(),m2.getOne()); //movie_id1,movie_id2

            int ratingProduct = Integer.parseInt(m1.getTwo().toString()) * Integer.parseInt(m2.getTwo().toString());
            int rating1Squard = Integer.parseInt(m1.getTwo().toString()) * Integer.parseInt(m1.getTwo().toString());
            int rating2Squard = Integer.parseInt(m2.getTwo().toString()) * Integer.parseInt(m2.getTwo().toString());

            MovieTupleInfo reduceValue=new MovieTupleInfo();

            reduceValue.setM1Rating(new IntWritable(Integer.parseInt(m1.getTwo().toString())));
            reduceValue.setM1numberOfRaters(m1.getThree());

            reduceValue.setM2Rating(new IntWritable(Integer.parseInt(m2.getTwo().toString())));
            reduceValue.setM2numberOfRaters(m2.getThree());

            reduceValue.setRatingProduct(new IntWritable(ratingProduct));
            reduceValue.setRating1Squared(new IntWritable(rating1Squard));
            reduceValue.setRating2Squared(new IntWritable(rating2Squard));

            context.write(reduceKey,reduceValue);

        }

    }

    private List<DoubleTuple3> generateUniqueCombination(Iterable<Tuple3> values){

        List<DoubleTuple3> list=new ArrayList<>();
        //copy 值
        List<Tuple3> valueList=new ArrayList<>();
        for(Tuple3 tmp:values){
            valueList.add(new Tuple3(new Text(tmp.getOne()),new Text(tmp.getTwo()),new LongWritable(tmp.getThree().get())));
        }

        for(int i=0;i<valueList.size()-1;i++){
            for(int j=i+1;j<valueList.size();j++){

                if(valueList.get(i).getOne().compareTo( valueList.get(j).getOne() )<0){
                    list.add(new DoubleTuple3(valueList.get(i),valueList.get(j)));
                }else{
                    list.add(new DoubleTuple3(valueList.get(j),valueList.get(i)));
                }
            }
        }

        return  list;

    }
}
