package com.data.analysis.contentBaseRec.model;

import org.apache.hadoop.io.WritableComparable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

/**
 * Created by Administrator on 2017/11/14.
 */
public class DoubleTuple3 implements WritableComparable<DoubleTuple3> {

    private Tuple3  one;
    private Tuple3  two;

    public  DoubleTuple3(){}

    public  DoubleTuple3(Tuple3  one,Tuple3 two){
        this.one=one;
        this.two=two;
    }

    @Override
    public int compareTo(DoubleTuple3 o) {

        return this.one.compareTo(o.getOne());
    }

    @Override
    public void write(DataOutput dataOutput) throws IOException {
        one.write(dataOutput);
        two.write(dataOutput);
    }

    @Override
    public void readFields(DataInput dataInput) throws IOException {
        one.readFields(dataInput);
        two.readFields(dataInput);
    }

    public Tuple3 getOne() {
        return one;
    }

    public void setOne(Tuple3 one) {
        this.one = one;
    }

    public Tuple3 getTwo() {
        return two;
    }

    public void setTwo(Tuple3 two) {
        this.two = two;
    }
}
