package com.data.analysis.contentBaseRec.model;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.io.WritableComparable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

/**
 * Created by Administrator on 2017/11/10.
 */

public class Tuple2 implements WritableComparable<Tuple2>{

     Text one=new Text();
     Text two=new Text();


    public  Tuple2(){}


    public  Tuple2(Text one, Text two){
        this.one=one;
        this.two=two;
    }

    @Override
    public int compareTo(Tuple2 o) {
        int ret=this.one.compareTo(o.one);
        if(ret==0){
            return this.two.compareTo(o.two);
        }
        return ret;
    }

    @Override
    public void write(DataOutput dataOutput) throws IOException {
        one.write(dataOutput);
        two.write(dataOutput);
    }

    @Override
    public void readFields(DataInput dataInput) throws IOException {
        one.readFields(dataInput);
        two.readFields(dataInput);
    }

    public Text getTwo() {
        return two;
    }

    public void setTwo(Text two) {
        this.two = two;
    }

    public Text getOne() {
        return one;
    }

    public void setOne(Text one) {
        this.one = one;
    }
}
