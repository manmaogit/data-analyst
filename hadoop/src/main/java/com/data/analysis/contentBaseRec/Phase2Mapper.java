package com.data.analysis.contentBaseRec;

import com.data.analysis.contentBaseRec.model.Tuple3;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

/**
 * Created by Administrator on 2017/11/10.
 *
 * key    < user_id >
 * value  < movie_id,  rate,  raters>
 *
 */
public class Phase2Mapper extends Mapper<Text, Tuple3, Text, Tuple3> {

    @Override
    protected void map(Text key, Tuple3 value, Context context) throws IOException, InterruptedException {

        /*String movie=value.getOne().toString();
        String rate= value.getTwo().toString();
        Long numberOfRaters=value.getThree().get();*/

        context.write(key,new Tuple3(value.getOne(),value.getTwo(),value.getThree()));
    }
}
