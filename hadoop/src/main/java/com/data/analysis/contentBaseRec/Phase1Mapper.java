package com.data.analysis.contentBaseRec;

import com.data.analysis.contentBaseRec.model.Tuple2;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

/**
 * Created by Administrator on 2017/11/10.
 *   user id | item id | rating | timestamp
 */
public class Phase1Mapper extends Mapper<LongWritable, Text, Text, Tuple2> {
    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        String[] tokens=value.toString().split("\t");
        context.write(new Text(tokens[1]),new Tuple2(new Text(tokens[0]),new Text(tokens[2])));
    }
}
