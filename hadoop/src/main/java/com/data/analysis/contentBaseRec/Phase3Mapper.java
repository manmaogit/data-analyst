package com.data.analysis.contentBaseRec;

import com.data.analysis.contentBaseRec.model.MovieTupleInfo;
import com.data.analysis.contentBaseRec.model.Tuple2;
import com.data.analysis.contentBaseRec.model.Tuple3;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

/**
 * Created by Administrator on 2017/11/10.
 *
 * key  : Tuple2(<movie1>,<movie2>)
 * value: Tuple7(<rating1>,<numberOfRater1>,
 *               <rating2>,<numberOfRater2>,
 *               <ratingProduct>,<rating1Squared>,<rating2Squared>)
 *
 */
public class Phase3Mapper extends Mapper<Tuple2, MovieTupleInfo, Tuple2, MovieTupleInfo> {

    @Override
    protected void map(Tuple2 key, MovieTupleInfo value, Context context) throws IOException, InterruptedException {
        context.write(key,value);
    }
}
