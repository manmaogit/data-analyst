package com.data.analysis.contentBaseRec.model;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.WritableComparable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

/**
 * Created by Administrator on 2017/11/10.
 */
public class Tuple3 implements WritableComparable<Tuple3>{

    Text one=new Text();
    Text two=new Text();
    LongWritable three=new LongWritable();

    public Tuple3(){}

    public Tuple3(Text one, Text two, LongWritable three){
        this.one=one;
        this.two=two;
        this.three=three;

    }

    @Override
    public int compareTo(Tuple3 o) {
        int ret=this.one.compareTo(o.one);
        if(ret==0){
            return this.two.compareTo(o.two);
        }
        return ret;
    }

    @Override
    public void write(DataOutput dataOutput) throws IOException {
        one.write(dataOutput);
        two.write(dataOutput);
        three.write(dataOutput);
    }

    @Override
    public void readFields(DataInput dataInput) throws IOException {
        one.readFields(dataInput);
        two.readFields(dataInput);
        three.readFields(dataInput);
    }

    public Text getOne() {
        return one;
    }

    public void setOne(Text one) {
        this.one = one;
    }

    public Text getTwo() {
        return two;
    }

    public void setTwo(Text two) {
        this.two = two;
    }

    public LongWritable getThree() {
        return three;
    }

    public void setThree(LongWritable three) {
        this.three = three;
    }

}
