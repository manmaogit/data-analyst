package com.data.analysis.contentBaseRec;

import com.data.analysis.contentBaseRec.model.Tuple2;
import com.data.analysis.contentBaseRec.model.Tuple3;
import com.google.common.collect.Lists;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2017/11/10.
 * key:movie
 * value: List<user,rating>
 *
 */
public class Phase1Reducer extends Reducer<Text, Tuple2, Text, Tuple3> {

    @Override
    protected void reduce(Text key, Iterable<Tuple2> values, Context context) throws IOException, InterruptedException {

        //遍历copy
        List<Tuple2> valueList = new ArrayList<>();
        for(Tuple2 tmp:values){
            valueList.add(new Tuple2(new Text(tmp.getOne()),new Text(tmp.getTwo())));
        }

        long numberOfRater=valueList.size();
        LongWritable numberOfRaters=new LongWritable(numberOfRater);

        for(Tuple2 tuple2:valueList){
            Text K3=tuple2.getOne(); //user_id
            Tuple3 V3=new Tuple3(key,tuple2.getTwo(),numberOfRaters); // movie_id, rate, raters
            context.write(K3,V3);
        }

    }
}
