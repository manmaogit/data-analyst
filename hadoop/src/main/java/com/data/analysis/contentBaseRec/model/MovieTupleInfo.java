package com.data.analysis.contentBaseRec.model;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.WritableComparable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

/**
 * Created by Administrator on 2017/11/14.
 */
public class MovieTupleInfo implements WritableComparable<MovieTupleInfo> {

    private IntWritable m1Rating=new IntWritable();
    private LongWritable m1numberOfRaters=new LongWritable();

    private IntWritable m2Rating=new IntWritable();
    private LongWritable m2numberOfRaters=new LongWritable();

    private  IntWritable ratingProduct=new IntWritable(); //m1Rating * m2Rating
    private  IntWritable rating1Squared=new IntWritable();
    private  IntWritable rating2Squared=new IntWritable();


    public  MovieTupleInfo(){}

    public IntWritable getM1Rating() {
        return m1Rating;
    }

    public void setM1Rating(IntWritable m1Rating) {
        this.m1Rating = m1Rating;
    }

    public LongWritable getM1numberOfRaters() {
        return m1numberOfRaters;
    }

    public void setM1numberOfRaters(LongWritable m1numberOfRaters) {
        this.m1numberOfRaters = m1numberOfRaters;
    }

    public IntWritable getM2Rating() {
        return m2Rating;
    }

    public void setM2Rating(IntWritable m2Rating) {
        this.m2Rating = m2Rating;
    }

    public LongWritable getM2numberOfRaters() {
        return m2numberOfRaters;
    }

    public void setM2numberOfRaters(LongWritable m2numberOfRaters) {
        this.m2numberOfRaters = m2numberOfRaters;
    }

    public IntWritable getRatingProduct() {
        return ratingProduct;
    }

    public void setRatingProduct(IntWritable ratingProduct) {
        this.ratingProduct = ratingProduct;
    }

    public IntWritable getRating1Squared() {
        return rating1Squared;
    }

    public void setRating1Squared(IntWritable rating1Squared) {
        this.rating1Squared = rating1Squared;
    }

    public IntWritable getRating2Squared() {
        return rating2Squared;
    }

    public void setRating2Squared(IntWritable rating2Squared) {
        this.rating2Squared = rating2Squared;
    }

    @Override
    public int compareTo(MovieTupleInfo o) {

        int compare;
        compare=this.getM1Rating().compareTo(o.getM1Rating());
        if(compare==0){
            return this.getM2Rating().compareTo(o.getM2Rating());
        }

        return compare;
    }

    @Override
    public void write(DataOutput dataOutput) throws IOException {
        m1Rating.write(dataOutput);
        m1numberOfRaters.write(dataOutput);
        m2Rating.write(dataOutput);
        m2numberOfRaters.write(dataOutput);

        ratingProduct.write(dataOutput);
        rating1Squared.write(dataOutput);
        rating2Squared.write(dataOutput);

    }

    @Override
    public void readFields(DataInput dataInput) throws IOException {
        m1Rating.readFields(dataInput);
        m1numberOfRaters.readFields(dataInput);
        m2Rating.readFields(dataInput);
        m2numberOfRaters.readFields(dataInput);

        ratingProduct.readFields(dataInput);
        rating1Squared.readFields(dataInput);
        rating2Squared.readFields(dataInput);
    }
}
