package com.data.analysis.contentBaseRec;

import com.data.analysis.contentBaseRec.model.MovieTupleInfo;
import com.data.analysis.contentBaseRec.model.Tuple2;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 *
 * key  : Tuple2( <movie1>,<movie2> )
 * value: List< Tuple7(<rating1>,<numberOfRater1>,
 *               <rating2>,<numberOfRater2>,
 *               <ratingProduct>,<rating1Squared>,<rating2Squared>) >
 *
 */
public class Phase3Reducer extends Reducer<Tuple2, MovieTupleInfo, Text, Text> {

    @Override
    protected void reduce(Tuple2 key, Iterable<MovieTupleInfo> values, Context context) throws IOException, InterruptedException {

        List<MovieTupleInfo> list=copyIterable(values);

        int groupSize=list.size();
        int dotProduct=0;
        int rating1Sum=0;
        int rating2Sum=0;
        int rating1NormSq=0;
        int rating2NormSq=0;
        long maxNumOfnumRaters1=0;
        long maxNumOfnumRaters2=0;

        for(MovieTupleInfo info:list){
            dotProduct += info.getRatingProduct().get();
            rating1Sum += info.getM1Rating().get();
            rating2Sum += info.getM2Rating().get();
            rating1NormSq += info.getRating1Squared().get();
            rating2NormSq += info.getRating2Squared().get();

            if(info.getM1numberOfRaters().get() > maxNumOfnumRaters1){
                maxNumOfnumRaters1 = info.getM1numberOfRaters().get();
            }

            if(info.getM2numberOfRaters().get() > maxNumOfnumRaters2){
                maxNumOfnumRaters2 = info.getM2numberOfRaters().get();
            }
        }

        double pearson=calculatePearsonCorrelation(groupSize,
                dotProduct,
                rating1Sum,
                rating2Sum,
                rating1NormSq,rating2NormSq);

        double cosine=caluateCosinCorrelation(dotProduct,Math.sqrt(rating1NormSq),Math.sqrt(rating2NormSq));

        double jaccard=caluateJaccardCorrelation(list);

        context.write(new Text(key.getOne()+"-"+key.getTwo()),new Text(pearson+","+cosine+","+jaccard));

    }

    /**
     * 计算皮尔逊相关系数
     *
     * @param size
     * @param dotProduct
     * @param rating1Sum
     * @param rating2Sum
     * @param rating1NormSq
     * @param rating2NormSq
     * @return
     */
    private double calculatePearsonCorrelation(
            double size,
            double dotProduct,
            double rating1Sum,
            double rating2Sum,
            double rating1NormSq,
            double rating2NormSq) {

        double numberator=size * dotProduct - rating1Sum * rating2Sum;
        double denominator=Math.sqrt(size * rating1NormSq - rating1Sum*rating1Sum)
                            * Math.sqrt(size * rating2NormSq - rating2Sum*rating2Sum);
        return  numberator/denominator;

    }

    /**
     * 余弦相似度
     * @param dotProduct
     * @param rating1Norm
     * @param rating2Norm
     * @return
     */
    private  double caluateCosinCorrelation(double dotProduct,
                                            double rating1Norm,
                                            double rating2Norm){
        return dotProduct/(rating1Norm*rating2Norm);
    }

    /**
     * Jaccard相似系数
     *
     * @return
     */
    private  double caluateJaccardCorrelation(List<MovieTupleInfo> list){
        Set<Integer> moiverating1Set=new HashSet<>();
        Set<Integer> moiverating2Set=new HashSet<>();
        for(MovieTupleInfo info:list){
            moiverating1Set.add(info.getM1Rating().get());
            moiverating2Set.add(info.getM2Rating().get());
        }

        //统计交集个数
        int count=0;
        for(Integer rat1:moiverating1Set){
            if(moiverating2Set.contains(rat1)){
                count++;
            }
        }

        //统计并集个数
        moiverating1Set.addAll(moiverating2Set);

        return (double) count/moiverating1Set.size();

    }




    private List<MovieTupleInfo> copyIterable(Iterable<MovieTupleInfo> values){

        List<MovieTupleInfo> list=new ArrayList<>();

        for(MovieTupleInfo tmp:values){

            MovieTupleInfo info=new MovieTupleInfo();
            info.setRating1Squared(new IntWritable(tmp.getRating1Squared().get()));
            info.setRating2Squared(new IntWritable(tmp.getRating2Squared().get()));

            info.setRatingProduct(new IntWritable(tmp.getRatingProduct().get()));
            info.setM1numberOfRaters(new LongWritable(tmp.getM1numberOfRaters().get()));
            info.setM2numberOfRaters(new LongWritable(tmp.getM2numberOfRaters().get()));

            info.setM1Rating(new IntWritable(tmp.getM1Rating().get()));
            info.setM2Rating(new IntWritable(tmp.getM2Rating().get()));

            list.add(info);
        }

        return list;

    }

}
