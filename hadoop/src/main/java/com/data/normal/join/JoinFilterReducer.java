package com.data.normal.join;

import org.apache.commons.lang3.StringUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
import sun.misc.Unsafe;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by manmao on 2017/12/10.
 */
public class JoinFilterReducer extends Reducer<Text,Text,NullWritable,Text>{

    int joinValFilter;

    String currentBarId="";

    List<Integer> barBufferList=new ArrayList<>();

    Text newValue=new Text();


    @Override
    protected void setup(Context context) throws IOException, InterruptedException {
        Configuration config=context.getConfiguration();
        joinValFilter=config.getInt(JoinFilterExampleMRJob.JOIN_VAL_MAX_CONF,-1);

    }

    @Override
    protected void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException {

        String keyString = key.toString();

        String barId=keyString.substring(0,keyString.length()-2);
        String sortFlag=keyString.substring(keyString.length()-1);


        if( !currentBarId.equals(barId) ){
            barBufferList.clear();
            currentBarId=barId;
        }

        if(sortFlag.equals(JoinFilterExampleMRJob.BAR_SORT_FLAG)){ //bar
            for(Text value:values){ //
                barBufferList.add(Integer.parseInt(value.toString()));
            }
        }else { //foo
            if(barBufferList.size() > 0){
                for (Text value:values){ //foo
                    for(Integer barValue:barBufferList){ //bar
                        String[] fooCells = StringUtils.split(value.toString(),"|");
                        int fooValue=Integer.parseInt(fooCells[1]);
                        int sumValue=barValue+fooValue;

                        if(sumValue < joinValFilter){  //sumVale小于阈值
                            newValue.set(fooCells[0]+"|"+barId+"|"+sumValue);
                            context.write(NullWritable.get(),newValue);
                        }else {
                            context.getCounter("Custom","joinValueFiltered").increment(1);
                        }
                    }
                }
            }else {
                System.out.println("Matching with nothing");
            }

        }
    }
}
