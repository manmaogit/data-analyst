package com.data.normal.distributeCache;

import com.data.normal.join.JoinFilterMapper;
import com.data.normal.join.JoinFilterPartitioner;
import com.data.normal.join.JoinFilterReducer;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.SequenceFile;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.compress.SnappyCodec;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;

import java.io.FileInputStream;
import java.net.URI;

/**
 * Created by manmao on 2017/12/10.
 */
public class MapperJoinJob extends Configured implements Tool {



    @Override
    public int run(String[] args) throws Exception {

        String inputBarPath=args[0]; //bar.txt
        String inputFoo = args[1];
        String output = args[2];
        int numberOfReducers = Integer.parseInt(args[3]);


        //A
        Job job = Job.getInstance();


        //B
        job.setJarByClass(MapperJoinJob.class);
        job.setJobName("MapperJoinJob");

        //使用符号链接创建分布式缓存
        job.addCacheFile(new URI("/user/bar/bar.txt#bar.txt"));

        //C
        Configuration config = job.getConfiguration();
        config.set("inputBarPath",inputBarPath);

        // D
        job.setInputFormatClass(TextInputFormat.class);
        TextInputFormat.addInputPath(job, new Path(inputFoo));
        //TextInputFormat.addInputPath(job, new Path(inputBar));

        // E
        job.setOutputFormatClass(TextOutputFormat.class);
        //删除输出
        FileSystem fs = FileSystem.get(job.getConfiguration());
        fs.delete(new Path(output), true);
        TextOutputFormat.setOutputPath(job, new Path(output));

        // F
        job.setMapperClass(JoinMapper.class);
        job.setReducerClass(JoinReducer.class);

        // G
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(Text.class);
        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(Text.class);

        //H
        job.setNumReduceTasks(numberOfReducers);

        // I
        job.waitForCompletion(true);
        return 0;
    }

    @Override
    public void setConf(Configuration conf) {
        // TODO Auto-generated method stub

    }

    @Override
    public Configuration getConf() {
        // TODO Auto-generated method stub
        return null;
    }
}
