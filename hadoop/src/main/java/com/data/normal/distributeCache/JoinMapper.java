package com.data.normal.distributeCache;

import org.apache.commons.lang3.StringUtils;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Administrator on 2018/1/6.
 */
public class JoinMapper extends Mapper<LongWritable,Text,Text,Text> {

    private Map<String,String> inputBar=new HashMap<>();

    @Override
    protected void setup(Context context) throws IOException, InterruptedException {

        String inputBarPath=context.getConfiguration().get("inputBarPath");

        File file=new File(inputBarPath);
        if(file.exists()){
            BufferedReader reader=new BufferedReader(new FileReader(file));
            String line=null;
            while (StringUtils.isNoneBlank(line=reader.readLine())){
                String[] arrs=line.split("\t");
                inputBar.put(arrs[2],arrs[0]+","+arrs[1]);
            }
        }



    }

    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        String arrs[]=value.toString().split("\t");
        if(inputBar.get(arrs[2])!=null){
            context.write(new Text(arrs[2]),new Text(inputBar.get(arrs[2])+"#"+arrs[0]+","+arrs[1]));
        }
    }
}
