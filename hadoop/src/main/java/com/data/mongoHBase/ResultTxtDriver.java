package com.data.mongoHBase;


import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.mapreduce.TableMapReduceUtil;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import com.mongodb.hadoop.MongoInputFormat;
import com.mongodb.hadoop.util.MongoConfigUtil;


/**
 * 
 * @author manmao
 * 
 */
public class ResultTxtDriver extends Configured implements Tool {
    
    @Override
    public int run(String[] arg0) throws Exception {
        
        Configuration conf = HBaseConfiguration.create();
        
        conf.addResource("mongodb.xml");
        
        MongoConfigUtil.setInputURI(conf,conf.get("mongo.input.uri"));
        MongoConfigUtil.setSplitSize(conf, 1024); //设置输入分片大小，单位MB
        
        Job job = Job.getInstance(conf);
        job.setJarByClass(ResultTxtDriver.class);
        
        job.setMapperClass(ResultTxtMapper.class);
        
        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(Put.class);
        
        job.setInputFormatClass(MongoInputFormat.class);
        
        job.setNumReduceTasks(16);
        
        job.setPartitionerClass(DefaultPartitioner.class);
        
        // set reducer and output
        /**
         * 设置HBase输出
         */
        TableMapReduceUtil.initTableReducerJob(
           "resultTxt",               // output table
           ResultTxtReducer.class,    // reducer class
           job//
        );
                 
        System.exit(job.waitForCompletion(true) ? 0 : 1);
        
        return 0;
    }
    
    
    public static void main( String[] args ) throws Exception{
        int res = ToolRunner.run(new Configuration(), new ResultTxtDriver(), args);
        System.exit(res);
    }
   
}

    
