package com.mr.test;

import java.io.IOException;

import com.data.normal.join.JoinFilterExampleMRJob;
import com.data.normal.join.JoinFilterMapper;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mrunit.PipelineMapReduceDriver;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.junit.Test;
  
import org.apache.hadoop.io.Text;  

  
public class MaxTemperatureReducerTest {  
    @Test  
    public void returnsMaximumIntegerInValues() throws IOException, InterruptedException {

        MapDriver driver=new MapDriver<LongWritable, Text, Text, Text>()
                .withMapper(new JoinFilterMapper())
                .withInput(new LongWritable(1l),new Text("123|456|789"))
                .withOutput(new Text("789|B"),new Text("123|456"));

        //设置Configuration
        Configuration configuration= driver.getConfiguration();
        configuration.set(JoinFilterExampleMRJob.FOO_VAL_MAX_CONF,"100000");

        driver.runTest();

    }  
}  