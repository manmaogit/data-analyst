package com.mr.test;


import com.data.hadoop.book.chr4_1.SalaryMapper;
import com.data.hadoop.book.chr4_1.SalaryReducer;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.ReduceDriver;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;

public class SalaryMapperTest {
    @Test
    public void testSalaryMapper() throws IOException {

        MapDriver driver=new MapDriver<LongWritable, Text, Text, Text>()
                .withMapper(new SalaryMapper())
                .withInput(new LongWritable(1l),new Text("张三\\t20\\t男\\t8000"))
                .withOutput(new Text("男"),new Text("张三\\t20\\t8000"));

        //设置Configuration
        Configuration configuration= driver.getConfiguration();
        configuration.set("key","value");

        driver.runTest();

    }

    @Test
    public void testSalaryReduce() throws IOException {

        ReduceDriver reduceDriver=new ReduceDriver(new SalaryReducer());

        reduceDriver.withInput(new Text("男"), new ArrayList<Text>(){
            {
                    add(new Text("张三\\t20\\t8000"));
                    add(new Text("李四\\t25\\t10000"));
            }
        });

        reduceDriver.withOutput(new Text("李四"), new Text("25\t男\t10000"));
        reduceDriver.runTest();
    }
}
